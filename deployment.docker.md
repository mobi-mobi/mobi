# Deployment information

The way we deploy an application is far far away from the point of excellence. We had no time to make things shining.

In this document we describe how to deploy mobimobi.

## Setup
- clone mobi repo somewhere where mobi user can access, this repo won't be used to run stuff in PROD, it's going to be used only for deployment
- execute this with sudo user
```
# $1 user to create
# $2 where to deploy mobi folder, absolute path

./setup mobi /home/mobi/mobi
```

This script creates user, install Docker, creates project and configures firewall.

## Deploy

- run it as MOBI_USER and MOBI_DEPLOY_PATH created on previous step, 
TODO: these actions needs to be run from CI
```
# $1 git commit ref or branch name
# $2 Linux user who run the app, created in setup
# $3 absolute path to git repository with the project source code

./deploy master mobi /home/mobi/mobi
```
- stop all legacy services
```
sudo systemctl stop eventapi
sudo systemctl stop recommendationapi
sudo systemctl stop model_builder
sudo /etc/init.d/nginx stop
```
- start not yet dockerized services
```
sudo systemctl start eventapi
sudo systemctl start recommendationapi
sudo systemctl start model_builder
sudo /etc/init.d/nginx start
```

## Operations

- launch only Mongo
```
docker-compose run --service-ports -d mongo
```
- access Mongo from remote server
```
ssh alexey@84.201.180.183 -NfL 2777:localhost:27017
```
- access grafana from remote server
```
ssh alexey@84.201.180.183 -NfL 3000:localhost:3000
```
- setup db
```
docker-compose run setup
```
- test setup and deploy:
    0. run build docker
    ```
    docker run -d --name docker --privileged -v ~/dev/personal/mobimobi/mobi:/mobi docker:dind
    ```
    1. run unbuntu Docker with mobi folder mapped
    ```
    docker run -ti -v ~/dev/personal/mobimobi/mobi:/mobi -w /mobi/docker -v /var/run/docker.sock:/var/run/docker.sock ubuntu:18.04
    ```
    2. run setup and deploy steps from above
