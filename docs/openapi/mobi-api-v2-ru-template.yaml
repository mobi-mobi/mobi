openapi: 3.0.3
info:
  title: Mobimobi.tech API
  description: |
    For English version, please click [here](http://docs.mobimobi.tech/api/docs-en.html).

    Данное техническое руководство предназначено для клиентов компании Mobimobi.tech. Если вы еще не являетесь
    клиентом, то можете более подробно ознакомиться с нашим продуктом на [главном сайте](https://www.mobimobi.tech).

    # Введение
    Поздравляем!

    Вы стали клиентом сервиса умных рекомендаций MobiMobi, а это значит, что в ближайшее время вы сможете
    заметно вырастить свои бизнес-показатели, благодаря более точному таргетированию своих пользователей̆.

    Несмотря на то, что интеграция с нашими сервисами довольно простая, это руководство создано для того,
    чтобы помочь вам и вашей̆ инженерной̆ команде подключиться к ним. Следуйте этой̆ документации и нашим рекомендациям,
    чтобы быстрее достичь желаемых целей̆. А в случае возникновения вопросов – не стесняйтесь обращаться
    к специалистам нашей̆ компании, мы всегда рады вам помочь!

    Обращаем ваше внимание на то, что для доступа к нашему сервису вам необходимо получить специальный̆ «токен»
    и использовать его так, как описано ниже.

    Благодарим за доверие,

    Команда Mobimobi.tech

    # Миграция с предыдущей версии API

    Если вы пользовались предыдущей версией нашего API (v1), для перехода к данной версии (v2):

    ## Используйте только протокол HTTPS

    Чтобы обеспечить безопасность ваших данных, API v2 не поддерживает работу по незашифрованному протоколу HTTP.

    ## Измените способ авторизации

    API v1 поддерживал передачу токена через параметр URL `token` или в заголовке запроса `Auth-Token`.
    API v2 поддерживает авторизацию только при помощи заголовка `Authorization`.

    ## Используйте новые конечные точки и методы

    Адреса конечных точек значительно упрощены:

    | v1                                                                | v2                           |
    |-------------------------------------------------------------------|------------------------------|
    | POST /event/product<br>POST /event/batch/product                  | POST /products               |
    | GET /event/product/{ProductId}                                    | GET /products/{product_id}   |
    | POST /event/product<br>PUT /event/product/{ProductId}/deactivate  | PATCH /products/{product_id} |
    | DELETE /event/product                                             | DELETE /products             |
    | POST /event/event                                                 | POST /events                 |
    | DELETE /event/events                                              | DELETE /events               |
    | GET /recommendation/populations                                   | GET /populations             |
    | GET /recommendation/{UserId}/recommendations                      | GET /recommendations         |
    | GET /recommendation/{UserId}/basket_recommendations               | GET /recommendations/basket  |
    | GET /recommendation/user/{UserId}/product/{ProductId}/similar     | GET /recommendations/similar |
    | GET /user/{UserId}/recommendations/zone/{ZoneId}                  | GET /recommendations/zoned   |
    | GET /recommendation/available_models<br>GET /recommendation/model | GET /models                  |
    | GET /metrics/user/{Metric}                                        | GET /metrics/{metric_name}   |
    | GET /event/status<br>GET /recommendation/status                   | GET /status                  |

    ## Определяйте статус выполнения запроса по коду HTTP

    API v2 использует стандартные коды статуса HTTP (200, 404, 500, и т.д.) для отражения результата исполнения запроса.
    Свойство "status" в ответах более не выдается.

    ## Извлекайте данные из свойств верхнего уровня

    API v2 не обрамляет данные в свойство "data" - они теперь доступны на первом уровне ответа.

  contact:
    name: MobiMobi.Tech support
    email: support@mobimobi.tech
  version: v2
servers:
  - url: https://api.mobimobi.tech/v2
paths:
  /products:
    post:
      tags:
        - Products
      summary: Создать продукт(ы)
      requestBody:
        required: true
        content:
          application/json:
            schema:
              oneOf:
                - $ref: '#/components/schemas/ProductCreateOne'
                - $ref: '#/components/schemas/ProductCreateMany'
      responses:
        '204':
          $ref: '#/components/responses/204'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
    delete:
      tags:
        - Products
      summary: Удалить все продукты
      description: Доступно только для тестовых токенов
      responses:
        '204':
          $ref: '#/components/responses/204'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /products/{product_id}:
    parameters:
      - $ref: '#/components/parameters/product_id_path'
    get:
      tags:
        - Products
      summary: Получить данные о продукте
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadOne'
        '401':
          $ref: '#/components/responses/401'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
    patch:
      tags:
        - Products
      summary: Изменить свойства продукта
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Product'
      responses:
        '204':
          $ref: '#/components/responses/204'
        '401':
          $ref: '#/components/responses/401'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /events:
    post:
      tags:
        - Events
      summary: Создать событие
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Event'
      responses:
        '204':
          $ref: '#/components/responses/204'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
    delete:
      tags:
        - Events
      summary: Удалить все события
      description: Доступно только для тестовых токенов
      responses:
        '204':
          $ref: '#/components/responses/204'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /populations:
    get:
      tags:
        - Populations
      summary: Получить пользовательские популяции
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                type: object
                required:
                  - REFERENCE
                  - TARGET
                  - TEST
                properties:
                  REFERENCE:
                    type: integer
                    example: 70
                  TARGET:
                    type: integer
                    example: 20
                  TEST:
                    type: integer
                    example: 10
        '401':
          $ref: '#/components/responses/401'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendations:
    get:
      tags:
        - Recommendations
      summary: Получить рекомендуемые продукты (общие рекомендации)
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/brands'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendations/basket:
    get:
      tags:
        - Recommendations
      summary: Получить рекомендуемые продукты (для корзины)
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/num'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendations/similar:
    get:
      tags:
        - Recommendations
      summary: Получить рекомендуемые продукты (для продукта)
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/product_id_query'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/same_brand'
        - $ref: '#/components/parameters/brands'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendations/zoned:
    get:
      tags:
        - Recommendations
      summary: Получить рекомендуемые продукты (для зоны)
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/zone_id'
        - $ref: '#/components/parameters/num'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /models:
    get:
      tags:
        - Models
      summary: Получить модели
      parameters:
        - name: active
          in: query
          description: Фильтр по флагу активности
          schema:
            type: integer
            enum:
              - 1
              - 0
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ModelReadMany'
        '401':
          $ref: '#/components/responses/401'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /metrics/{metric_names}:
    get:
      tags:
        - Metrics
      summary: Получить метрики
      parameters:
        - name: metric_names
          in: path
          description: |
            Одна или несколько метрик, разделенные символом `,`
            Формат имени метрики: `<metric_name>[.<tag>.<tag_value>]*[:<alias>]?`
          required: true
          schema:
            type: array
            items:
              type: string
              example:
                - events
                - events.action.create
                - events.type.product_view.action.create:create_events
            minItems: 1
        - name: base
          in: query
          description: 'Шаг (база) метрики: (M)inute / (H)our / (D)ay'
          schema:
            type: string
            enum:
              - M
              - H
              - D
        - name: from
          in: query
          required: true
          description: |
            Указывает, начиная с какой даты необходимо вернуть значения метрик. Поддерживаемые форматы:
            * `%Y-%m-%d %H:%M:%S` - абсолютная дата
            * -NUMBER{M|H|D} (пример: `-2D`, `-10H`) - относительная дата
          schema:
            type: string
            example:
              - 2020-01-01 00:00:00
              - -2D
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                type: object
                properties:
                  metrics:
                    type: array
                    items:
                      type: string
                      example: events.action.create
                  now_ts:
                    type: integer
                    example: 1577306759
                  now:
                    type: string
                    example: '2019-12-25 20:45:59'
                  from_ts:
                    type: integer
                    example: 1577293961
                  from:
                    type: string
                    example: '2019-12-25 17:12:41'
                  points:
                    type: integer
                    example: 4
                  base:
                    type: integer
                    enum:
                      - 60
                      - 3600
                      - 86400
                    example: 3600
                  values:
                    type: object
                    description: |
                      В каждом элементе массива значений метрики находятся:
                      1. Дата, соответствующая точке метрики
                      2. Значение
                    additionalProperties:
                      type: array
                      items:
                        type: array
                        items:
                          oneOf:
                            - type: string
                            - type: integer
                        minItems: 2
                        maxItems: 2
                    example:
                      'events.action.create':
                        - ['2020-04-24 00:00:00', 42]
                        - ['2020-04-25 00:00:00', 43]
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /status:
    get:
      tags:
        - Status
      summary: Получить статус сервиса
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                type: object
                required:
                  - events
                  - recommendations
                properties:
                  events:
                    type: string
                    enum:
                      - OK
                      - ERROR
                    description: Статус модуля обработки пользовательских событий
                  recommendations:
                    type: string
                    enum:
                      - OK
                      - ERROR
                    description: Статус модуля формирования рекомендаций
              example:
                events: OK
                recommendations: OK
        '500':
          $ref: '#/components/responses/500'
components:
  schemas:
    Product:
      type: object
      properties:
        id:
          description: Уникальный идентификатор продукта
          type: string
          example: SKU-145837
        title:
          description: Название продукта
          type: string
          example: Гель для душа
        brand:
          description: Брэнд продукта
          type: string
          example: The Best Company Ever
        in_stock:
          description: Указывает, находится ли продукт в продаже на данный момент или нет
          type: boolean
          default: true
        active:
          description: Указывает, является ли продукт "активным". Неактивный продукт приравнивается к удаленному
          type: boolean
          default: true
        categories:
          description: Список категорий, к которым относится продукт
          type: array
          items:
            type: string
          example:
            - Cosmetics
            - Perfume
        color:
          description: Цвет продукта
          type: string
          example: Black
        is_new:
          description: Указывает, является ли товар "новинкой" или нет
          type: boolean
        is_exclusive:
          description: Указывает, является ли товар "эксклюзивным" или нет
          type: boolean
          example: false
        description:
          description: Описание продукта
          type: string
          example: "Some cool description"
    ProductCreateOne:
      allOf:
        - $ref: '#/components/schemas/Product'
        - type: object
          required:
            - id
            - title
            - brand
            - categories
    ProductCreateMany:
      type: array
      items:
        $ref: '#/components/schemas/ProductCreateOne'
    ProductReadOne:
      allOf:
        - $ref: '#/components/schemas/Product'
        - type: object
          required:
            - id
            - title
            - brand
            - categories
            - in_stock
            - active
    ProductReadMany:
      type: array
      items:
        $ref: '#/components/schemas/ProductReadOne'
    Event:
      type: object
      required:
        - user_id
        - product_id
      properties:
        user_id:
          type: string
          description: Уникальный идентификатор пользователя
          example: USER8833
        product_id:
          type: string
          description: Уникальный идентификатор продукта
          example: SKU-5599
        datetime:
          type: string
          description: 'UTC-время эвента. Ожидаемый формат: `%Y-%m-%d %H:%M:%S`. Значение по-умолчанию - текущее UTC-время'
          example: "2020-05-12 14:12:42"
        event_type:
          type: string
          description: Тип произошедшего события
          enum:
            - UNKNOWN
            - PRODUCT_VIEW
            - PRODUCT_TO_WISHLIST
            - PRODUCT_TO_BASKET
            - PRODUCT_SALE
          example: PRODUCT_VIEW
          default: UNKNOWN
        referer:
          type: string
          description: Реферер события
          enum:
            - UNKNOWN
            - SEARCH
            - RECOMMENDATION
            - CATALOG
            - OTHER
            - SMART_SEARCH
            - SMART_RECOMMENDATION
            - SMART_SIMILAR
            - SMART_OTHER
          example: SMART_RECOMMENDATION
          default: UNKNOWN
    Model:
      type: object
      properties:
        model_id:
          type: string
          description: Уникальный идентификатор модели
          example: 'model_deepwalk_client_1589474587_3162'
        type:
          type: string
          description: Тип модели
          example: 'deepwalk'
        date:
          type: string
          description: Дата создания модели
          example: '2020-05-14T14:23:05.740000'
        status:
          type: string
          description: Текущий статус модели
          enum:
            - 'READY'
            - 'NOT_READY'
            - 'ERROR'
        active:
          type: boolean
          description: Флаг активности модели
    ModelReadOne:
      allOf:
        - $ref: '#/components/schemas/Model'
        - type: object
          required:
            - model_id
            - type
            - date
            - status
    ModelReadMany:
      type: array
      items:
        $ref: '#/components/schemas/ModelReadOne'
    Error:
      type: object
      properties:
        error:
          type: object
          properties:
            code:
              type: integer
              example: 500
            description:
              type: string
              example: Can not parse request body
            message:
              type: string
              example: Can not parse request body
  responses:
    '204':
      description: Запрос обработан успешно
    '400':
      description: Некорректный формат (отсутствуют или неверно заданы параметры)
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    '401':
      description: Токен не указан или не распознан
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    '403':
      description: Исполнение запроса с указанным токеном невозможно
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    '404':
      description: Запрошенный объект не найден
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    '500':
      description: Непредвиденная внутренняя ошибка при обработке запроса
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
  parameters:
    user_id:
      name: user_id
      in: query
      description: Уникальный идентификатор пользователя
      required: true
      schema:
        type: string
        example: USER8234
    product_id_path:
      name: product_id
      in: path
      description: Уникальный идентификатор продукта
      required: true
      schema:
        type: string
        example: SKU-145837
    product_id_query:
      name: product_id
      in: query
      description: Уникальный идентификатор продукта
      required: true
      schema:
        type: string
        example: SKU-145837
    zone_id:
      name: zone_id
      in: query
      description: Уникальный идентификатор зоны
      required: true
      schema:
        type: string
        example: similar-products-zone
    num:
      name: num
      in: query
      description: Количество элементов, которые требуется вернуть в ответе
      schema:
        type: integer
        default: 10
        example: 25
    same_brand:
      name: same_brand
      in: query
      description: |
        Возвращать продукты только того же брэнда, что и заданный.
      schema:
        type: integer
        default: 1
    brands:
      name: brands
      in: query
      description: |
        Возвращать продукты только указанных брэндов.
      schema:
        type: array
        items:
          type: string
          example: The Best Brand Ever
  securitySchemes:
    Token:
      type: http
      scheme: bearer
      description: |
        Токен – это специальный̆ набор символов, выполняющий̆ роль секретного ключа, известного только клиенту,
        с помощью которого API распознаёт, от какого именно клиента пришёл запрос. Поэтому, при каждом запросе к любому
        из API, требуется указать ваш токен. Сделать это можно, добавив заголовок "Authorization" к запросу. Формат:

        ```
        Authorization: Bearer <token>
        ```

        ## Ваш токен

        Скорее всего у вас уже должен быть токен, который вам предоставили сотрудники MobiMobi. Если же это не так,
        или вы сомневаетесь в том, что ваш токен верный – свяжитесь с нами, и мы будем рады помочь.

        ## Тестовый токен

        Для того, чтобы иметь возможность доступа к API не только в продакшене, но и в режиме тестирования и отладки,
        предоставляется дополнительный «тестовый» токен, который можно и нужно использовать для всех задач,
        не связанных с продакшеном. В случае использования одного и того же ключа для всех задач,
        качество предоставляемых рекомендаций может быть ухудшено.
tags:
  - name: Products
    description: |
      Запросы, описанные в данном разделе, позволяют работать с информацией о продуктах в базе данных Mobimobi.tech.
      Данная информация необходима для построения модели машинного обучения, а так же для получения рекомендаций.
  - name: Events
    description: |
      Пользовательские события, которые происходят у вас на сайте или же в вашем мобильном приложении – это то,
      что делает каждого пользователя уникальным, а так же то, что помогает нам обучить модель,
      которая поможет вам строить персональные рекомендации для ваших пользователей. Помимо событий,
      мы так же используем информацию о ваших продуктах, например, таких как цвет продукта или его категория.
      Это позволяет нам сделать модель еще лучше.

      В данной секции описаны способы управления информацией о пользовательских событиях.
      Эти события будут использованы не только для построения модели машинного обучения, но и для того,
      чтобы подсчитать различные метрики, которые будут доступны вам в приложении
      [Web Console](https://console.mobimobi.tech).
  - name: Populations
    description: |
      Далее в документации будут встречаться упоминания пользовательских популяций. Перед тем, как продолжить
      читать этот документ, мы настоятельно рекомендуем ознакомиться с данной частью документации,
      чтобы иметь представление о том, что такое популяции.

      MobiMobi – это продукт, который помогает клиентам персонализировать свои приложения, и тем самым увеличивать
      доход от продаж на своих торговых площадках. Но как измерить размер прироста продаж? Как понять,
      какую пользу получает клиент от сотрудничества с MobiMobi?

      Один из вариантов, который сразу приходит на ум – это измерение пользы, путем отслеживания количества покупок,
      совершенных после перехода пользователя по ссылке из блока рекомендаций. Однако, у этого подхода есть один
      большой недостаток: как понять, совершил бы пользователь ту же саму покупку без помощи блока рекомендаций или нет?
      Возможно, блок рекомендации всего лишь помог пользователю быстрее найти продукт, который этот пользователь
      и так хотел купить.

      Поэтому, чтобы иметь более точное представление о том, какова выгода компании от сотрудничества с MobiMobi,
      мы считаем среднее количество продуктов, покупаемых пользователями в течение одной пользовательской сессии и
      сравниваем получаемые числа для групп пользователей, которым показывают рекомендации, и которым рекомендации
      не показывают. Для этого каждый пользователь случайным образом помещается в одну из двух популяций – REFERENCE
      или TARGET. Все пользователи из популяции TARGET получают персональные рекомендации, тогда как пользователям
      из популяции REFERENCE рекомендации не предоставляются.

      Размеры популяций задаются в конфигурационном файле, и могут быть изменены в любой момент времени по желанию
      клиента.
  - name: Recommendations
    description: |
      Таргетированные рекомендации – информация о том, какой продукт порекомендовать пользователю на главной странице
      или же какие похожие продукты показать на странице просмотра другого продукта.

      Необходимыми условиями работы данного сервиса являются предварительное получение данных о пользовательских
      событиях, а также построение рекомендательной модели, которая использует эти события.

      Mobimobi предоставляет клиентам возможность создать специальную сущность - зоны. Одна такая
      зона соответствует одному блоку на стороне клиента. Каждому блоку может быть сопоставлена одна или множество
      правил поиска и выдачи рекомендаций. API позволяет получать рекомендации с учетом данных правил.
      Настройка параметров зон и правил происходит в специальном приложении -
      [Web Console](https://console.mobimobi.tech), доступ к которой есть только у клиентов Mobimobi.
  - name: Models
    description: |
      В этой секции представлены методы, с помощью которых можно получить дополнительную информацию о состоянии
      рекомендательной системы.
  - name: Metrics
    description: |
      Количественные метрики характеризуют работу системы и её качество. Это вспомогательная информация,
      рекомендуемая для интеграции с другими системами клиента. Для просмотра же данных,
      мы рекомендуем пользоваться приложением [Web Console](https://console.mobimobi.tech).
  - name: Status
    description: |
      В данном разделе описан метод, с помощью которого можно получить статус модулей сервиса.
