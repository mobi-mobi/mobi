openapi: 3.0.3
info:
  title: Mobimobi.tech API
  description: |
    For English version, please click [here](http://docs.mobimobi.tech/api/docs-v2-en.html).

    Данное техническое руководство предназначено для клиентов компании Mobimobi.tech. Если вы еще не являетесь
    клиентом, то можете более подробно ознакомиться с нашим продуктом на [главном сайте](https://www.mobimobi.tech).

    # Введение

    Это руководство создано для того,
    чтобы помочь вам и вашей̆ инженерной̆ команде подключиться к системе умных рекомендаций и поиска Mobimobi.tech.
    Следуйте этой̆ документации и нашим рекомендациям,
    чтобы быстрее достичь желаемых целей̆. А в случае возникновения вопросов – не стесняйтесь обращаться
    к специалистам нашей̆ компании, мы всегда рады вам помочь!

    Обращаем ваше внимание на то, что для доступа к нашему сервису вам необходимо получить специальный̆ «токен»
    и использовать его так, как описано ниже.

    Благодарим за доверие,

    Команда Mobimobi.tech

    # Миграция с предыдущей версии API

    Если вы пользовались предыдущей версией нашего API (v1), для перехода к данной версии (v2):

    ## Используйте только протокол HTTPS

    Чтобы обеспечить безопасность ваших данных, API v2 не поддерживает работу по незашифрованному протоколу HTTP.

    ## Измените способ авторизации

    API v1 поддерживал передачу токена через параметр URL `token` или в заголовке запроса `Auth-Token`.
    API v2 поддерживает авторизацию только при помощи заголовка `Authorization`. Так же обратите внимание на формат передачи токена.

    ## Используйте новые конечные точки и методы

    Адреса конечных точек значительно упрощены:

    | v1                                                            | v2                                        |
    |---------------------------------------------------------------|-------------------------------------------|
    | POST /event/product                                           | POST /catalog/products                    |
    | GET /event/product/{ProductId}                                | GET /catalog/products/{product_id}        |
    | DELETE /event/product                                         | DELETE /catalog/products/{product_id}     |
    | POST /event/event                                             | POST /user/events                         |
    | GET /recommendation/{UserId}/recommendations                  | GET /recommendation/products/personalized |
    | GET /recommendation/{UserId}/basket_recommendations           | GET /recommendation/products/basket       |
    | GET /recommendation/user/{UserId}/product/{ProductId}/similar | GET /recommendation/products/similar      |
    | GET /user/{UserId}/recommendations/zone/{ZoneId}              | GET /recommendation/products/zoned        |

  contact:
    name: Mobimobi.tech support
    email: support@mobimobi.tech
  version: v2
servers:
  - url: https://api.mobimobi.tech/v2
paths:
  /catalog/products:
    post:
      tags:
        - Catalog API
      summary: Создать/заменить продукт
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ProductCreateOne'
              # oneOf:
              #   - $ref: '#/components/schemas/ProductCreateOne'
              #   - $ref: '#/components/schemas/ProductCreateMany'
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /catalog/products/{product_id}:
    parameters:
      - $ref: '#/components/parameters/product_id_path'
    get:
      tags:
        - Catalog API
      summary: Получить данные о продукте
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadOne'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
    patch:
      tags:
        - Catalog API
      summary: Изменить свойства продукта
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Product'
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
    delete:
      tags:
        - Catalog API
      summary: Удалить один продукт
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /catalog/all_products:
    delete:
      tags:
        - Catalog API
      summary: Удалить все продукты
      description: Доступно только для тестовых токенов
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /catalog/status:
    get:
      tags:
        - Catalog API
      summary: Получить статус Catalog API
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '500':
          $ref: '#/components/responses/500'
  /user/events:
    post:
      tags:
        - User API
      summary: Создать событие
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserEvent'
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /user/all_events:
    delete:
      tags:
        - User API
      summary: Удалить все события
      description: Доступно только для тестовых токенов
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /user/status:
    get:
      tags:
        - User API
      summary: Получить статус User API
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '500':
          $ref: '#/components/responses/500'
  /search:
    get:
      tags:
        - Search API
      summary: Поиск по каталогу
      parameters:
        - $ref: '#/components/parameters/search_query'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/user_id_optional'
        - $ref: '#/components/parameters/brands'
        - $ref: '#/components/parameters/full_product_info'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductSearchResultMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /search/status:
    get:
      tags:
        - Search API
      summary: Получить статус Search API
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '500':
          $ref: '#/components/responses/500'
  /recommendation/products/personalized:
    get:
      tags:
        - Recommendations API
      summary: Получить рекомендуемые продукты (общие рекомендации)
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/brands'
        - $ref: '#/components/parameters/categories'
        - $ref: '#/components/parameters/full_product_info'
        - $ref: '#/components/parameters/complete_with_most_popular'
        - $ref: '#/components/parameters/exclude_sold'
        - $ref: '#/components/parameters/exclude_added_to_basket'
        - $ref: '#/components/parameters/exclude_added_to_wishlist'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendation/products/most_popular:
    get:
      tags:
        - Recommendations API
      summary: Получить самые популярные по просмотрам продукты
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/brands'
        - $ref: '#/components/parameters/categories'
        - $ref: '#/components/parameters/full_product_info'
        - $ref: '#/components/parameters/exclude_sold'
        - $ref: '#/components/parameters/exclude_added_to_basket'
        - $ref: '#/components/parameters/exclude_added_to_wishlist'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendation/products/basket:
    get:
      tags:
        - Recommendations API
      summary: Получить рекомендуемые продукты (для корзины)
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/product_ids'
        - $ref: '#/components/parameters/complete_with_most_popular'
        - $ref: '#/components/parameters/full_product_info'
        - $ref: '#/components/parameters/exclude_sold'
        - $ref: '#/components/parameters/exclude_added_to_basket'
        - $ref: '#/components/parameters/exclude_added_to_wishlist'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendation/products/similar:
    get:
      tags:
        - Recommendations API
      summary: Получить рекомендуемые продукты (для продукта)
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/product_id_query'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/same_brand'
        - $ref: '#/components/parameters/brands'
        - $ref: '#/components/parameters/categories'
        - $ref: '#/components/parameters/full_product_info'
        - $ref: '#/components/parameters/exclude_sold'
        - $ref: '#/components/parameters/exclude_added_to_basket'
        - $ref: '#/components/parameters/exclude_added_to_wishlist'
        - $ref: '#/components/parameters/disable_user_product_history_features'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendation/products/zoned:
    get:
      tags:
        - Recommendations API
      summary: Получить рекомендуемые продукты (для зоны)
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/zone_id'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/complete_with_most_popular'
        - $ref: '#/components/parameters/full_product_info'
        - $ref: '#/components/parameters/exclude_sold'
        - $ref: '#/components/parameters/exclude_added_to_basket'
        - $ref: '#/components/parameters/exclude_added_to_wishlist'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendation/products/recently_viewed:
    get:
      tags:
        - Recommendations API
      summary: Получить последние посещённые пользователем продукты
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/num'
        - $ref: '#/components/parameters/full_product_info'
        - $ref: '#/components/parameters/exclude_sold'
        - $ref: '#/components/parameters/exclude_added_to_basket'
        - $ref: '#/components/parameters/exclude_added_to_wishlist'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendation/products/rank:
    get:
      tags:
        - Recommendations API
      summary: Отсортировать продукты в соответствии с предпочтениями пользователя
      parameters:
        - $ref: '#/components/parameters/user_id'
        - $ref: '#/components/parameters/full_product_info'
        - $ref: '#/components/parameters/product_ids'
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductReadMany'
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /recommendation/status:
    get:
      tags:
        - Recommendations API
      summary: Получить статус Recommendations API
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '500':
          $ref: '#/components/responses/500'
  /metrics/{metric_names}:
    get:
      tags:
        - Metrics API
      summary: Получить метрики
      parameters:
        - name: metric_names
          in: path
          description: |
            Одна или несколько метрик, разделенные символом `,`
            Формат имени метрики: `<metric_name>[.<tag>.<tag_value>]*[:<alias>]?`
          required: true
          schema:
            type: array
            items:
              type: string
              example:
                - events
                - events.action.create
                - events.type.product_view.action.create:create_events
            minItems: 1
        - name: base
          in: query
          description: 'Шаг (база) метрики: (M)inute / (H)our / (D)ay'
          schema:
            type: string
            enum:
              - M
              - H
              - D
        - name: from
          in: query
          required: true
          description: |
            Указывает, начиная с какой даты необходимо вернуть значения метрик. Поддерживаемые форматы:
            * `%Y-%m-%d %H:%M:%S` - абсолютная дата
            * -NUMBER{M|H|D} (пример: `-2D`, `-10H`) - относительная дата
          schema:
            type: string
            example:
              - 2020-01-01 00:00:00
              - -2D
      responses:
        '200':
          description: Запрос обработан успешно
          content:
            application/json:
              schema:
                type: object
                properties:
                  metrics:
                    type: array
                    items:
                      type: string
                      example: events.action.create
                  now_ts:
                    type: integer
                    example: 1577306759
                  now:
                    type: string
                    example: '2019-12-25 20:45:59'
                  from_ts:
                    type: integer
                    example: 1577293961
                  from:
                    type: string
                    example: '2019-12-25 17:12:41'
                  points:
                    type: integer
                    example: 4
                  base:
                    type: integer
                    enum:
                      - 60
                      - 3600
                      - 86400
                    example: 3600
                  values:
                    type: object
                    description: |
                      В каждом элементе массива значений метрики находятся:
                      1. Дата, соответствующая точке метрики
                      2. Значение
                    additionalProperties:
                      type: array
                      items:
                        type: array
                        items:
                          oneOf:
                            - type: string
                            - type: integer
                        minItems: 2
                        maxItems: 2
                    example:
                      'events.action.create':
                        - ['2020-04-24 00:00:00', 42]
                        - ['2020-04-25 00:00:00', 43]
        '400':
          $ref: '#/components/responses/400'
        '401':
          $ref: '#/components/responses/401'
        '403':
          $ref: '#/components/responses/403'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
      security:
        - Token: []
  /metrics/status:
    get:
      tags:
        - Metrics API
      summary: Получить статус Metrics API
      responses:
        '200':
          $ref: '#/components/responses/EmptyOK'
        '500':
          $ref: '#/components/responses/500'
components:
  schemas:
    Product:
      type: object
      properties:
        product_id:
          description: Уникальный идентификатор продукта
          type: string
          example: Red T-Shirt XL
          maxLength: 100
        product_group_id:
          description: |
            Уникальный идентификатор группы продукта. Используется, когда несколько разных продуктов являются семейством
            одного и того же продукта и отличаются друг от друга такими параметрами, как размер и/или цвет.
          type: string
          example: T-Shirt
          maxLength: 100
        title:
          description: Название продукта
          type: string
          example: Красная футболка
          maxLength: 150
        brand:
          description: Брэнд продукта
          type: string
          example: Моя компания
          maxLength: 100
        price:
          description: Указывает цену продукта
          type: string
          pattern: '^[0-9]+\.[0-9]{2,2}$'
          example: '12.00'
          maxLength: 20
        currency:
          description: Указывает валюту цены продукта (ISO 4217)
          type: string
          maxLength: 10
          example: 'USD'
        is_searchable:
          description: Помещать ли продукт в поисковый индекс
          type: boolean
          example: true
          default: true
        is_recommendable:
          description: Является ли продукт рекомендуемым. Если false - продукт не будет возвращаться в рекомендациях
          type: boolean
          example: true
          default: true
        categories:
          description: Список категорий, к которым относится продукт
          type: array
          items:
            type: string
            maxLength: 100
          maxItems: 10
          minItems: 0
          example:
            - Cosmetics
            - Perfume
        color:
          description: Цвет продукта
          type: string
          maxLength: 200
          example: Red
        size:
          description: Размер продукта
          type: string
          maxLength: 200
          example: X-Large
        available_from:
          description: Указывает, с какой (UTC) даты продукт становится доступным пользователям. Недоступные
            пользователям продукты не будут отображаться в блоках рекомендаций
          type: string
          pattern: '^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}$'
          example: '2020-05-12 14:12:42'
        available_until:
          description: Указывает, до какой (UTC) даты продукт доступен пользователям. Недоступные пользователям продукты
            не будут отображаться в блоках рекомендаций
          type: string
          pattern: '^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}$'
          example: '2021-05-12 14:12:42'
        description:
          description: Описание продукта
          type: string
          example: 'Some cool description'
          maxLength: 5000
        sale_price:
          description: Указывает цену продукта, размещённого на распродаже
          type: string
          pattern: '^[0-9]+\.[0-9]{2,2}$'
          example: "9.99"
          maxLength: 20
        on_sale_from:
          description: UTC-дата, начиная с которой продукт находится на распродаже
          type: string
          pattern: '^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}$'
          example: '2021-05-12 14:12:42'
        on_sale_until:
          description: UTC-дата, заканчивая которой продукт находится на распродаже
          type: string
          pattern: '^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}$'
          example: '2021-05-12 23:59:59'
        tags:
          description: Список тэгов, соответствующих данному продукту
          type: array
          items:
            type: string
            maxLength: 100
          maxItems: 10
          minItems: 0
          example:
            - exclusive
            - face
        age_group:
          description: Указывает, какой возрастной категории подходит продукт
          type: string
          enum:
            - all_ages
            - adult
            - teen
            - kids
            - newborn
          example: 'adult'
          default: all_ages
        gender:
          description: Указывает, людям какого пола подходит данный продукт
          type: string
          enum:
            - female
            - male
            - unisex
          default: unisex
    ProductCreateOne:
      allOf:
        - $ref: '#/components/schemas/Product'
        - type: object
          required:
            - product_id
            - title
            - brand
            - price
            - currency
#    ProductCreateMany:
#      type: array
#      items:
#        $ref: '#/components/schemas/ProductCreateOne'
    ProductReadOne:
      allOf:
        - $ref: '#/components/schemas/Product'
        - type: object
          required:
            - id
            - title
            - brand
    ProductReadMany:
      type: array
      items:
        $ref: '#/components/schemas/ProductReadOne'
    ProductSearchResultOne:
      type: object
      required:
        - meta
        - product
      properties:
        meta:
          type: object
        product:
          $ref: '#/components/schemas/ProductReadOne'
    ProductSearchResultMany:
      type: array
      items:
        $ref: '#/components/schemas/ProductSearchResultOne'
    UserEvent:
      type: object
      required:
        - user_id
        - event_type
        - event_platform
      properties:
        user_id:
          type: string
          description: Уникальный идентификатор пользователя
          example: USER8833
          maxLength: 100
        event_type:
          type: string
          description: Тип произошедшего события
          enum:
            - PRODUCT_VIEW
            - PRODUCT_TO_WISHLIST
            - PRODUCT_TO_BASKET
            - PRODUCT_SALE
          example: PRODUCT_VIEW
        event_platform:
          type: string
          description: Платформа, на которой произошло событие
          enum:
            - IOS
            - ANDROID
            - WEB
            - OTHER
          example: ANDROID
        product_id:
          type: string
          description: Уникальный идентификатор продукта. Должен быть указан только для событий `PRODUCT_VIEW`, `PROUCT_TO_WISHLIST` и `PRODUCT_TO_BASKET`
          example: SKU-5599
          maxLength: 100
        product_quantity:
          type: integer
          description: Количество продуктов. Должен быть указан только для событий `PRODUCT_TO_BASKET`
          minimum: 1
          maximum: 1000000
        basket_items:
          type: array
          description: Список продуктов. Должен быть указан только для событий `PRODUCT_SALE`
          items:
            type: object
            required:
              - product_id
              - product_quantity
            properties:
              product_id:
                type: string
                description: Уникальный идентификатор продукта
                maxLength: 100
              product_quantity:
                type: integer
                description: Количество продуктов
                minimum: 1
                maximum: 1000000
          maxItems: 1000
          minItems: 1
          example:
            - product_id: "SKU-5599"
              product_quantity: 2
            - product_id: "SKU-9449"
              product_quantity: 6
        date:
          type: string
          description: 'UTC-время эвента. Ожидаемый формат: `%Y-%m-%d %H:%M:%S`. Значение по-умолчанию - текущее UTC-время'
          example: "2020-05-12 14:12:42"
    EmptyOK:
      type: object
      properties:
        status:
          type: string
          example: OK
    BadRequestError:
      type: object
      properties:
        status:
          type: string
          example: ERROR
        error:
          type: object
          properties:
            code:
              type: integer
              example: 400
            description:
              type: string
              example: Can not parse request body.
    UnauthorizedError:
      type: object
      properties:
        status:
          type: string
          example: ERROR
        error:
          type: object
          properties:
            code:
              type: integer
              example: 401
            description:
              type: string
              example: No token provided.
    ForbiddenError:
      type: object
      properties:
        status:
          type: string
          example: ERROR
        error:
          type: object
          properties:
            code:
              type: integer
              example: 403
            description:
              type: string
              example: Token is expired.
    NotFoundError:
      type: object
      properties:
        status:
          type: string
          example: ERROR
        error:
          type: object
          properties:
            code:
              type: integer
              example: 404
            description:
              type: string
              example: Can not find an object.
    InternalError:
      type: object
      properties:
        status:
          type: string
          example: ERROR
        error:
          type: object
          properties:
            code:
              type: integer
              example: 500
            description:
              type: string
              example: Unexpected internal error happened. We have already started digging into the problem.
  responses:
    'EmptyOK':
      description: Запрос обработан успешно
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/EmptyOK'
    '400':
      description: Некорректный формат или отсутствие данных или переданных параметров
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BadRequestError'
    '401':
      description: Токен не указан или не распознан
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/UnauthorizedError'
    '403':
      description: Исполнение запроса с указанным токеном невозможно
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ForbiddenError'
    '404':
      description: Запрошенный или требуемый объект не найден
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/NotFoundError'
    '500':
      description: Непредвиденная внутренняя ошибка при обработке запроса
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/InternalError'
  parameters:
    user_id:
      name: user_id
      in: query
      description: Уникальный идентификатор пользователя
      required: true
      schema:
        type: string
        example: USER8234
    user_id_optional:
      name: user_id
      in: query
      description: Опциональный уникальный идентификатор пользователя
      required: false
      schema:
        type: string
        example: USER8234
    product_id_path:
      name: product_id
      in: path
      description: Уникальный идентификатор продукта
      required: true
      schema:
        type: string
        example: SKU-145837
    product_id_query:
      name: product_id
      in: query
      description: Уникальный идентификатор продукта
      required: true
      schema:
        type: string
        example: SKU-145837
    product_ids:
      name: product_ids
      in: query
      description: Список уникальных идентификаторов продуктов
      schema:
        type: array
        collectionFormat: multi
        items:
          type: string
        example:
          - SKU-145837
          - SKU-7676
    zone_id:
      name: zone_id
      in: query
      description: Уникальный идентификатор зоны
      required: true
      schema:
        type: string
        example: similar-products-zone
    num:
      name: num
      in: query
      description: Количество элементов, которые требуется вернуть в ответе
      schema:
        type: integer
        default: 10
        example: 10
    same_brand:
      name: same_brand
      in: query
      description: |
        Возвращать продукты только того же брэнда, что и заданный.
      schema:
        type: string
        enum:
          - yes
          - no
        default: no
    brands:
      name: brands
      in: query
      description: |
        Возвращать продукты только указанных брэндов.
      schema:
        type: array
        collectionFormat: multi
        items:
          type: string
        example:
          - Some brand
          - Some other brand
    categories:
      name: categories
      in: query
      description: |
        Возвращать продукты только указанных категорий.
      schema:
        type: array
        collectionFormat: multi
        items:
          type: string
        example:
          - Some category
          - Some other category
    search_query:
      name: q
      in: query
      description: Строка поискового запроса
      schema:
        type: string
        example: Red T-Shirt
    full_product_info:
      name: full_product_info
      in: query
      description: Возвращать полную информацию о продуктах - добавляет системные поля в ответ
      schema:
        type: string
        enum:
          - yes
          - no
        default: no
    complete_with_most_popular:
      name: complete_with_most_popular
      in: query
      description: |
        Добавить самые популярные продукты в результат выдачи, если найденное количество продуктов
        меньше теруемого количества
      schema:
        type: string
        enum:
          - yes
          - no
        default: yes
    exclude_sold:
      name: exclude_sold
      in: query
      description: |
        Исключить купленные ранее товары из результатов
      schema:
        type: string
        enum:
          - yes
          - no
        default: yes
    exclude_added_to_basket:
      name: exclude_added_to_basket
      in: query
      description: |
        Исключить добавленные ранее в корзину товары из результатов
      schema:
        type: string
        enum:
          - yes
          - no
        default: no
    exclude_added_to_wishlist:
      name: exclude_added_to_wishlist
      in: query
      description: |
        Исключить добавленные ранее в вишлист товары из результатов
      schema:
        type: string
        enum:
          - yes
          - no
        default: no
    disable_user_product_history_features:
      name: disable_user_product_history_features
      in: query
      description: |
        Не использовать исторические данные о взаимодействии пользователя и конкретных продуктов при
        подготовке рекомендаций. При использовании данного флага в рекомендации могут быть добавлены
        продукты, ранее посещенные пользователем, поэтому используйте этот флаг тогда, когда вы
        хотите показать не только "похожие" продукты.
      schema:
        type: string
        enum:
          - yes
          - no
        default: yes
  securitySchemes:
    Token:
      type: http
      scheme: bearer
      description: |
        Токен – это специальный̆ набор символов, выполняющий̆ роль секретного ключа, известного только клиенту,
        с помощью которого API распознаёт, от какого именно клиента пришёл запрос. Поэтому, при каждом запросе к любому
        из API, требуется указать ваш токен. Сделать это можно, добавив заголовок "Authorization" к запросу. Формат:

        ```
        Authorization: Bearer <token>
        ```

        ## Ваш токен

        Скорее всего у вас уже должен быть токен, который вам предоставили сотрудники MobiMobi. Если же это не так,
        или вы сомневаетесь в том, что ваш токен верный – свяжитесь с нами, и мы будем рады помочь.

        ## Тестовый токен

        Для того, чтобы иметь возможность доступа к API не только в продакшене, но и в режиме тестирования и отладки,
        предоставляется дополнительный «тестовый» токен, который можно и нужно использовать для всех задач,
        не связанных с продакшеном. В случае использования одного и того же ключа для всех задач,
        качество предоставляемых рекомендаций может быть ухудшено.
tags:
  - name: Catalog API
    description: |
      Запросы, описанные в данном разделе, позволяют работать с информацией о продуктах в базе данных Mobimobi.tech.
      Данная информация необходима для построения модели машинного обучения, а так же для получения рекомендаций.
  - name: User API
    description: |
      Пользовательские события, которые происходят у вас на сайте или же в вашем мобильном приложении – это то,
      что делает каждого пользователя уникальным, а так же то, что помогает нам обучить модель,
      которая поможет вам строить персональные рекомендации для ваших пользователей. Помимо событий,
      мы так же используем информацию о ваших продуктах, например, таких как цвет продукта или его категория.
      Это позволяет нам сделать модель еще лучше.

      В данной секции описаны способы управления информацией о пользовательских событиях.
      Эти события будут использованы не только для построения модели машинного обучения, но и для того,
      чтобы подсчитать различные метрики, которые будут доступны вам в приложении
      [Web Console](https://console.mobimobi.tech).
  - name: Search API
    description: |
      TBD
  - name: Recommendations API
    description: |
      Таргетированные рекомендации – информация о том, какой продукт порекомендовать пользователю на главной странице
      или же какие похожие продукты показать на странице просмотра другого продукта.

      Необходимыми условиями работы данного сервиса являются предварительное получение данных о пользовательских
      событиях, а также построение рекомендательной модели, которая использует эти события.

      Mobimobi предоставляет клиентам возможность создать специальную сущность - зоны. Одна такая
      зона соответствует одному блоку на стороне клиента. Каждому блоку может быть сопоставлена одна или множество
      правил поиска и выдачи рекомендаций. API позволяет получать рекомендации с учетом данных правил.
      Настройка параметров зон и правил происходит в специальном приложении -
      [Web Console](https://console.mobimobi.tech), доступ к которой есть только у клиентов Mobimobi.
  - name: Metrics API
    description: |
      Количественные метрики характеризуют работу системы и её качество. Это вспомогательная информация,
      рекомендуемая для интеграции с другими системами клиента. Для просмотра же данных,
      мы рекомендуем пользоваться приложением [Web Console](https://console.mobimobi.tech).
