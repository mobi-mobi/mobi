openapi: 3.0.0
servers:
  - url: https://console.mobimobi.tech/
info:
  description: |
    TBD

  title: Console API
  version: v1

  contact:
    name: Mobimobi.tech support
    email: support@mobimobi.tech

  # x-logo:
    # url: '<logo url>'
    # altText: <logo alt text>

tags:
  - name: user info
    x-displayName: User Info
    description: |
      Информация о пользователе
  - name: catalog
    x-displayName: Catalog Info
    description: |
      Информация о каталоге
  - name: counters
    x-displayName: Counters
    description: |
      Счётчики
  - name: metrics
    x-displayName: Time-series Metrics
    description: |
      Метрики
  - name: zone api
    x-displayName: Zone Management
    description: |
      API для доступа к зонам
  - name: campaign api
    x-displayName: Campaign Management
    description: |
      API для доступа к кампаниям


paths:
  /api/user/info:
    get:
      tags:
        - user info
      summary: Получить информацию о текущем пользователе
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    type: object
                    properties:
                      name:
                        type: string
                        example: John Smith
                      email:
                        type: string
                        example: john@smith.com
                      clients:
                        type: array
                        items:
                          type: string
                        example: ['client', 'testclient']

        '500':
          $ref: '#/components/responses/500'
  /api/{Client}/catalog/info:
    parameters:
      - $ref: '#/components/parameters/Client'
    get:
      tags:
        - catalog
      summary: Получить информацию о каталоге
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    type: object
                    properties:
                      number:
                        type: object
                        properties:
                          number:
                            type: integer
                            example: 20
                            description: Количество уникальных продуктов
  /api/{Client}/catalog/product/{ProductId}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/ProductId'
    get:
      tags:
        - catalog
      summary: Получить информацию о продукте
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    $ref: '#/components/schemas/Product'
  /api/{Client}/catalog/products:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/Limit20'
      - $ref: '#/components/parameters/Offset'
      - $ref: '#/components/parameters/IncludeInactive'
      - $ref: '#/components/parameters/IncludeNotInStock'
      - $ref: '#/components/parameters/Q'
    get:
      tags:
        - catalog
      summary: Поиск по базе продуктов
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    type: object
                    properties:
                      total:
                        type: integer
                        example: 42
                        description: |
                          Количество продуктов, соответствующих поисковому запросу. Если поисковый звпрос не задан, то
                          параметр будет равен Null (если q - пустая строка). Данное число не обязательно равно
                          количеству продуктов, возвращаемых в массиве "products", так как количество продуктов
                          в данном массиве ограничено параметрами limit и offset, а значение "total" соответствует
                          вообще всем продуктам, которые релевантны поисковому запросу.

                          Важно заметить, что при пустом или отсутствующем параметре q, поле total будет установлено в
                          Null, так как это трудоёмкая задача - получить количество продуктов в базе данных.
                      products:
                        type: array
                        description: Соответствующие запросу продукты
                        items:
                          $ref: '#/components/schemas/Product'
  /api/{Client}/catalog/brands:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/Offset'
      - $ref: '#/components/parameters/Limit20'
      - $ref: '#/components/parameters/Q'
    get:
      tags:
        - catalog
      summary: Получить список брэндов
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  meta:
                    type: object
                    properties:
                      source:
                        type: string
                        example: cache
                        enum:
                          - cache
                          - database
                  data:
                    type: object
                    properties:
                      brands:
                        type: array
                        items:
                          type: string
                        example: [brand1, brand2]
  /api/{Client}/catalog/categories:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/Offset'
      - $ref: '#/components/parameters/Limit20'
      - $ref: '#/components/parameters/Q'
    get:
      tags:
        - catalog
      summary: Получить список категорий
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  meta:
                    type: object
                    properties:
                      source:
                        type: string
                        example: cache
                        enum:
                          - cache
                          - database
                  data:
                    type: object
                    properties:
                      categories:
                        type: array
                        items:
                          type: string
                        example: [category1, category2]
  /api/{Client}/catalog/events/count/last/{TimeInterval}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/TimeInterval'
      - $ref: '#/components/parameters/Brand'
      - name: status
        in: query
        description: Статус результата операции
        required: false
        schema:
          type: string
          example: created
          enum:
            - created
            - updated
            - deleted
            - no_effect
    get:
      tags:
        - counters
      summary: Получить количество запросов на изменение каталога товаров
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IntCounter'
  /api/{Client}/user/events/count/last/{TimeInterval}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/TimeInterval'
      - $ref: '#/components/parameters/Brand'
      - name: event_type
        in: query
        description: Тип пользовательского события
        required: false
        schema:
          type: string
          example: product_view
          enum:
            - unknown
            - product_view
            - product_to_wishlist
            - product_to_basket
            - product_sale
    get:
      tags:
        - counters
      summary: Получить количество пользовательских событий
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IntCounter'
  /api/{Client}/recommendation/events/count/last/{TimeInterval}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/TimeInterval'
      - name: recommendation_type
        in: query
        description: Тип рекомендации
        required: false
        schema:
          type: string
          example: similar_products
          enum:
            - most_popular
            - similar_products
            - personal_recommendations
            - smart_search
    get:
      tags:
        - counters
      summary: Получить количество созданных рекомендаций
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IntCounter'
  /api/{Client}/user/unique/metric/from/{TimeInterval}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/TimeInterval'
    get:
      tags:
        - metrics
      summary: Получить данные о количестве уникальных пользователей
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TimeSeriesMetrics'
  /api/{Client}/zone:
    parameters:
      - $ref: '#/components/parameters/Client'
    post:
      tags:
        - zone api
      summary: Создать или заменить зону
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Zone'
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    $ref: '#/components/schemas/Zone'
  /api/{Client}/zone/{ZoneId}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/ZoneId'
    get:
      tags:
        - zone api
      summary: Получить данные о зоне
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    $ref: '#/components/schemas/Zone'
    delete:
      tags:
        - zone api
      summary: Удалить зону
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
  /api/{Client}/zones/num:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/IncludeInactive'
      - $ref: '#/components/parameters/Q'
    get:
      tags:
        - zone api
      summary: Получить количество зон
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IntCounter'
  /api/{Client}/zones:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/Limit20'
      - $ref: '#/components/parameters/Offset'
      - $ref: '#/components/parameters/IncludeInactive'
      - $ref: '#/components/parameters/Q'
    get:
      tags:
        - zone api
      summary: Получить список зон
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    type: array
                    items:
                      $ref: '#/components/schemas/Zone'
  /api/{Client}/campaign:
    parameters:
      - $ref: '#/components/parameters/Client'
    post:
      tags:
        - campaign api
      summary: Создать или заменить кампанию
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Campaign'
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    $ref: '#/components/schemas/Campaign'
  /api/{Client}/campaign/{CampaignId}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/CampaignId'
    get:
      tags:
        - campaign api
      summary: Получить данные о кампании
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    $ref: '#/components/schemas/Campaign'
    delete:
      tags:
        - campaign api
      summary: Удалить кампанию
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
  /api/{Client}/campaigns:
    parameters:
      - $ref: '#/components/parameters/Client'
    get:
      tags:
        - campaign api
      summary: Получить список кампаний
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    type: array
                    items:
                      $ref: '#/components/schemas/Campaign'
  /api/{Client}/zone/{ZoneId}/campaigns:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/ZoneId'
      - $ref: '#/components/parameters/IncludeInactive'
      - name: include_past
        in: query
        description: Указывает, надо ли возвращать кампании, которые уже закончились
        required: false
        schema:
          type: string
          default: no
          example: yes
          enum:
            - yes
            - no
    get:
      tags:
        - campaign api
      summary: Получить список кампаний, подключённых к зоне
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    type: array
                    items:
                      $ref: '#/components/schemas/Campaign'
  /api/{Client}/zone/{ZoneId}/add_campaign/{CampaignId}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/ZoneId'
      - $ref: '#/components/parameters/CampaignId'
    post:
      tags:
        - campaign api
        - zone api
      summary: Добавить компанию к зоне
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
  /api/{Client}/zone/{ZoneId}/remove_campaign/{CampaignId}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/ZoneId'
      - $ref: '#/components/parameters/CampaignId'
    delete:
      tags:
        - campaign api
        - zone api
      summary: Удалить кампанию из зоны
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
  /api/{Client}/zone/{ZoneId}/campaign/closest:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/ZoneId'
    get:
      tags:
        - zone api
      summary: Получить ближайшую по времени кампанию
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    $ref: '#/components/schemas/Campaign'
  /api/{Client}/zone/{ZoneId}/apply_campaign/{CampaignId}:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/ZoneId'
      - $ref: '#/components/parameters/CampaignId'
    get:
      tags:
        - zone api
      summary: Получить параметры зоны с применёнными параметрами кампании
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    $ref: '#/components/schemas/Zone'
  /api/{Client}/zone/{ZoneId}/stats:
    parameters:
      - $ref: '#/components/parameters/Client'
      - $ref: '#/components/parameters/ZoneId'
    get:
      tags:
        - zone api
      summary: Получить информацию о показах и о конверсии
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  status:
                    type: string
                    example: OK
                  data:
                    type: object
                    properties:
                      displays:
                        type: integer
                        example: 10
                      ctr:
                        type: float
                        example: 10.2
                      converted:
                        type: float
                        example: 5.1

components:
  responses:
    '500':
      description: Ошибка при выполнении запроса
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                example: "ERROR"
              error:
                type: object
                properties:
                  code:
                    type: integer
                    example: 500
                  description:
                    type: string
                    example: Can not parse request body
                  message:
                    type: string
                    example: Can not parse request body
  parameters:
    Client:
      name: Client
      in: path
      description: Уникальный идентификатор клиента
      required: true
      schema:
        type: string
        example: testclient
    Q:
      name: q
      in: query
      description: Строка поискового запроса
      required: false
      schema:
        type: string
        example: Some search string
    ProductId:
      name: ProductId
      in: path
      description: Уникальный идентификатор продукта
      required: true
      schema:
        type: string
        example: SKU-145837
    TimeInterval:
      name: TimeInterval
      in: path
      description: Временной интервал
      required: true
      schema:
        type: string
        example: 24h
    Brand:
      name: brand
      in: query
      description: Наменование брэнда
      required: false
      schema:
        type: string
        example: thebestbrandever
    ZoneId:
      name: ZoneId
      in: path
      description: Идентификатор зоны
      required: true
      schema:
        type: string
        example: zoneid1
    CampaignId:
      name: CampaignId
      in: path
      description: Идентификатор кампании
      required: true
      schema:
        type: string
        example: campaignid1
    Offset:
      name: offset
      in: query
      description: Количество элементов, которые следует пропустить при подготовке ответа
      required: false
      schema:
        type: int
        default: 0
        example: 60
    Limit20:
      name: limit
      in: query
      description: Количество элементов, которые надо вернуть
      required: false
      schema:
        type: int
        default: 20
        example: 20
    IncludeInactive:
      name: include_inactive
      in: query
      description: Указывает, требуется ли вернуть неактивные объекты
      required: false
      schema:
        type: string
        example: yes
        default: no
        enum:
          - yes
          - no
    IncludeNotInStock:
      name: include_not_in_stock
      in: query
      description: Указывает, требуется ли вернуть продукты, которые в даннный момент не находятся на продаже
      required: false
      schema:
        type: string
        example: yes
        default: no
        enum:
          - yes
          - no
  schemas:
    IntCounter:
      type: object
      properties:
        status:
          type: string
          example: OK
        data:
          type: object
          properties:
            value:
              type: integer
              example: 20.0
    TimeSeriesMetrics:
      type: object
      properties:
        status:
          type: string
          example: OK
        data:
          type: list
          items:
            oneOf:
              - type: string
              - type: float
          example: [["2020-06-24 20:00:00", 563.0], ["2020-06-24 21:00:00", 498.0], ["2020-06-24 22:00:00", 297.0]]
    ZoneSettings:
      type: object
      required:
        - recommendations_num
      properties:
        recommendations_num:
          description: Количество возвращаемых рекомендаций
          type: integer
          example: 20
        same_brand:
          description: Возвращать ли только продукты того же брэнда?
          type: boolean
          example: true
          default: false
        brands:
          description: Указывает, продукты каких брэндов должны быть рекомендованы
          type: array
          items:
            type: string
          example: [brand1, brand2]
          default: Null
        categories:
          description: Указывает, продукты каких категорий должны быть рекомендованы
          type: array
          items:
            type: string
          example: [category1, category2]
          default: Null
        extra_product_ids:
          description: Указывает идентификаторы продуктов, которые следуюет добавить к списку рекомендуемых продуктов
          type: array
          items:
            type: string
          example: [productid1, productid2]
          default: Null
        extra_products_position:
          description: Указывакт способ добавления экстра-продуктов к рекомендуемым
          type: string
          enum:
            - beginning
            - end
            - random
            - evenly
          example: random
        save_organic_extra_products_positions:
          description: |
            Указывает, оставлять ли рекомендуемый продукт на своём месте, если он так же присутствует в списке
            продуктов, которые надо добавить
          type: boolean
          default: Null
          example: true
        dont_show_organic_products:
          description: |
            Указывает, надо ли оставить рекомендуемые продукты в выдаче, или использовать только экстра-продукты
          type: boolean
          default: Null
          example: true
    Zone:
      type: object
      required:
        - name
        - zone_type
        - zone_settings
      properties:
        zone_id:
          description: индентификатор зоны. Поле должно отсутствовать при создании новой зоны.
          type: string
          example: zone1
        name:
          type: string
          example: My First Zone
        zone_type:
          type: string
          enum:
            - PERSONAL_RECOMMENDATIONS
            - SIMILAR_PRODUCTS
            - BASKET_RECOMMENDATIONS
            - MOST_POPULAR
          example: SIMILAR_PRODUCTS
        description:
          type: string
          description: Описание зоны
          example: My very first zone
        status:
          type: string
          enum:
            - ACTIVE
            - DISABLED
          example: ACTIVE
        campaign_ids:
          type: array
          description: Список кампаний, соответствующих этой зоне
          items:
            type: string
          example: [campaign1, campaign2]
        zone_settings:
          $ref: '#/components/schemas/ZoneSettings'
    Campaign:
      type: object
      required:
        - name
        - zone_settings
      properties:
        campaign_id:
          description: уникальный идентификатор кампании. Поле долдно отсутствовать при создании новой кампании
          type: string
          example: campaign1
        name:
          description: Имя кампании
          type: string
          example: My First Campaign
        description:
          description: Описание кампании
          type: string
          example: My First Campaign
        status:
          description: Статус кампании
          type: string
          example: ACTIVE
          enum:
            - ACTIVE
            - DISABLED
        start_date:
          description: Начало кампании
          type: string
          example: "2020-05-12 14:12:42"
        end_date:
          description: Конец кампании
          type: string
          example: "2020-05-12 14:12:42"
        zone_settings:
          description: Настройки кампании
          $ref: '#/components/schemas/ZoneSettings'
    Product:
      type: object
      required:
        - id
        - title
        - brand
        - categories
      properties:
        id:
          description: Уникальный идентификатор продукта
          type: string
          example: SKU-145837
        title:
          description: Название продукта
          type: string
          example: Гель для душа
        brand:
          description: Брэнд продукта
          type: string
          example: The Best Company Ever
        in_stock:
          description: Указывает, находится ли продукт в продаже на данный момент или нет
          type: boolean
          default: true
          example: true
        active:
          description: Указывает, является ли продукт "активным". Неактивный продукт приравнивается к удаленному
          type: boolean
          default: true
          example: true
        categories:
          description: Список категорий, к которым относится продукт
          type: array
          items:
            type: string
          example: ['Cosmetics', 'Perfume']
        color:
          description: Цвет продукта
          type: string
          example: Black
        is_new:
          description: Указывает, является ли товар "новинкой" или нет
          type: boolean
        is_exclusive:
          description: Указывает, является ли товар "эксклюзивным" или нет
          type: boolean
          example: false
        description:
          description: Описание продукта
          type: string
          example: "Some cool description"
