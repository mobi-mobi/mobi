## Deployment information

The way we deploy an application is far far away from the point of excellence. We had no time to make things shining.

In this document we describe how to deploy mobimobi.

### Setup services

Create a new user:

```
sudo useradd -m -d /home/mobi -s /bin/bash mobi
```

Checkout repository:

```
sudo su - mobi
git clone http://<token>:<password>@gitlab.com/mobi-mobi/mobi.git
exit
```

Here are credentials you can use:

```
Login: mobimobilt
Password: PYHteUcpNHoPRR7S6kCs
```

Install python 3.7

From here: https://jcutrer.com/linux/upgrade-python37-ubuntu1810

```
sudo apt-get install python3.7
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2
sudo update-alternatives --config python3  # Enter 2
```

Install pip3:

```
sudo apt install python3-pip
sudo pip3 install gunicorn
```

Install packages:

```
sudo su - mobi
cd mobi
pip3 install -rrequirements.txt
exit
```

Install and launch MongoDb:

```
wget -qO - https://www.mongodb.org/static/pgp/server-4.0.asc | sudo apt-key add -
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt update
sudo apt-get install -y mongodb-org
sudo service mongod start
```

Setup databases:

```
python3 mobi/dao/setup_scripts/setup_client.py --database testdb mongodb
python3 mobi/dao/setup_scripts/setup_client.py --database sephora mongodb
python3 mobi/dao/setup_scripts/setup_client.py --database sephoratest mongodb
```

#### Configure event API:
```
sudo mkdir -p /var/log/mobi/event_api
sudo chown -R mobi:mobi /var/log/mobi/event_api

sudo touch /var/log/mobi/event_api/stdout.log
sudo chown syslog:adm /var/log/mobi/event_api/stdout.log

cat > /etc/rsyslog.d/event_api.conf << ENDOFFILE
if $programname == 'event_api' then /var/log/mobi/event_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/eventapi.service << ENDOFFILE
[Unit]
Description=Mobi Event Api
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/mobi/event_api.sock "mobi.api.wsgi:event_api_app()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=event_api

[Install]
WantedBy=multi-user.target
ENDOFFILE

sudo systemctl restart rsyslog
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start eventapi service:
```
systemctl start eventapi
```

#### Configure Catalog API:
```
sudo mkdir -p /var/log/mobi/catalog_api
sudo chown -R mobi:mobi /var/log/mobi/catalog_api

sudo touch /var/log/mobi/catalog_api/stdout.log
sudo chown syslog:adm /var/log/mobi/catalog_api/stdout.log

cat > /etc/rsyslog.d/catalog_api.conf << ENDOFFILE
if $programname == 'catalog_api' then /var/log/mobi/catalog_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

sudo cat > /etc/systemd/system/catalogapi.service << ENDOFFILE

[Unit]
Description=Mobi Catalog Api
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/mobi/catalog_api.sock "mobi.api.wsgi:catalog_api_app()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=catalog_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start eventapi service:
```
systemctl start catalogapi
```

#### Configure recommendation API
```
mkdir -p /var/log/mobi/recommendation_api
chown -R mobi:mobi /var/log/mobi/recommendation_api

sudo touch /var/log/mobi/recommendation_api/stdout.log
sudo chown syslog:adm /var/log/mobi/recommendation_api/stdout.log

cat > /etc/rsyslog.d/recommendation_api.conf << ENDOFFILE
if $programname == 'recommendation_api' then /var/log/mobi/recommendation_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/recommendationapi.service << ENDOFFILE
[Unit]
Description=Mobi Recommendation Api
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/mobi/recommendation_api.sock "mobi.api.wsgi:recommendation_api_app()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=recommendation_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start recommendationapi service:
```
systemctl start recommendationapi
```

#### Configure TEST recommendation API
```
sudo mkdir -p /var/log/mobi/test_recommendation_api
chown -R mobi:mobi /var/log/mobi/test_recommendation_api

sudo touch /var/log/mobi/test_recommendation_api/stdout.log
sudo chown syslog:adm /var/log/mobi/test_recommendation_api/stdout.log

cat > /etc/rsyslog.d/test_recommendation_api.conf << ENDOFFILE
if $programname == 'test_recommendation_api' then /var/log/mobi/test_recommendation_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/test_recommendationapi.service << ENDOFFILE
[Unit]
Description=Mobi Test Recommendation Api
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/mobi/test_recommendation_api.sock "mobi.api.wsgi:recommendation_api_app()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=test_recommendation_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start recommendationapi service:
```
systemctl start test_recommendationapi
```

#### Configure metrics API
```
mkdir -p /var/log/mobi/metrics_api
chown -R mobi:mobi /var/log/mobi/metrics_api

sudo touch /var/log/mobi/metrics_api/stdout.log
sudo chown syslog:adm /var/log/mobi/metrics_api/stdout.log

cat > /etc/rsyslog.d/metrics_api.conf << ENDOFFILE
if $programname == 'metrics_api' then /var/log/mobi/metrics_api/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/metricsapi.service << ENDOFFILE
[Unit]
Description=Mobi Metrics Api
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/local/bin/gunicorn --worker-class=sync --worker-connections=100 --workers 5 --bind unix:/home/mobi/metrics_api.sock "mobi.api.wsgi:metrics_api_app()"
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=metrics_api

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start recommendationapi service:
```
systemctl start metricsapi
```


#### Configure Console Web Application
```
mkdir -p /var/log/mobi/console
chown mobi:mobi /var/log/mobi/console

sudo touch /var/log/mobi/console/stdout.log
sudo chown syslog:adm /var/log/mobi/console/stdout.log

cat > /etc/rsyslog.d/console.conf << ENDOFFILE
if $programname == 'console' then /var/log/mobi/console/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/console.service << ENDOFFILE
[Unit]
Description=Mobi Console Web App
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.web.console.console
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=console

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start recommendationapi service:
```
systemctl start console
```


### Configure Model Builder
```
mkdir -p /var/log/mobi/model_builder
chown -R mobi:mobi /var/log/mobi/model_builder

sudo touch /var/log/mobi/model_builder/stdout.log
sudo chown syslog:adm /var/log/mobi/model_builder/stdout.log

cat > /etc/rsyslog.d/model_builder.conf << ENDOFFILE
if $programname == 'model_builder' then /var/log/mobi/model_builder/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/model_builder.service << ENDOFFILE
[Unit]
Description=Mobi Model Builder
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.model_builder
Nice=-10
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=model_builder

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start Model Builder service:
```
systemctl start model_builder
```


### Configure Cache Builder
```
mkdir -p /var/log/mobi/cache_builder
chown -R mobi:mobi /var/log/mobi/cache_builder

sudo touch /var/log/mobi/cache_builder/stdout.log
sudo chown syslog:adm /var/log/mobi/cache_builder/stdout.log

cat > /etc/rsyslog.d/cache_builder.conf << ENDOFFILE
if $programname == 'cache_builder' then /var/log/mobi/cache_builder/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/cache_builder.service << ENDOFFILE
[Unit]
Description=Mobi Cache Builder
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.cache_builder
Nice=-5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=cache_builder

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start Cache Builder service:
```
systemctl start cache_builder
```


### Configure Dynamic Features Builder
```
mkdir -p /var/log/mobi/dynamic_features_builder
chown -R mobi:mobi /var/log/mobi/dynamic_features_builder

sudo touch /var/log/mobi/dynamic_features_builder/stdout.log
sudo chown syslog:adm /var/log/mobi/dynamic_features_builder/stdout.log

cat > /etc/rsyslog.d/dynamic_features_builder.conf << ENDOFFILE
if $programname == 'dynamic_features_builder' then /var/log/mobi/dynamic_features_builder/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/dynamic_features_builder.service << ENDOFFILE
[Unit]
Description=Mobi Dynamic Features Builder Builder
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.dynamic_features_builder
Nice=-5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=dynamic_features_builder

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start Cache Builder service:
```
systemctl start dynamic_features_builder
```


### Configure Search Index Builder
```
mkdir -p /var/log/mobi/search_index_builder
chown -R mobi:mobi /var/log/mobi/search_index_builder

sudo touch /var/log/mobi/search_index_builder/stdout.log
sudo chown syslog:adm /var/log/mobi/search_index_builder/stdout.log

cat > /etc/rsyslog.d/search_index_builder.conf << ENDOFFILE
if $programname == 'search_index_builder' then /var/log/mobi/search_index_builder/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/search_index_builder.service << ENDOFFILE
[Unit]
Description=Mobi Search Index Builder
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.search_index_builder
Nice=-5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=search_index_builder

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
systemctl daemon-reload
```

Start Search Index Builder service:
```
systemctl start search_index_builder
```


### Configure Metrics Builder
```
mkdir -p /var/log/mobi/metrics_builder
chown -R mobi:mobi /var/log/mobi/metrics_builder

sudo touch /var/log/mobi/metrics_builder/stdout.log
sudo chown syslog:adm /var/log/mobi/metrics_builder/stdout.log

cat > /etc/rsyslog.d/metrics_builder.conf << ENDOFFILE
if $programname == 'metrics_builder' then /var/log/mobi/metrics_builder/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/metrics_builder.service << ENDOFFILE
[Unit]
Description=Mobi Metrics Builder
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.metrics_builder
Nice=-5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=metrics_builder

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
sudo systemctl daemon-reload
```

Start Metrics Builder service:
```
systemctl start metrics_builder
```


### Configure Attribution Builder
```
mkdir -p /var/log/mobi/attribution_builder
chown -R mobi:mobi /var/log/mobi/attribution_builder

sudo touch /var/log/mobi/attribution_builder/stdout.log
sudo chown syslog:adm /var/log/mobi/attribution_builder/stdout.log

cat > /etc/rsyslog.d/attribution_builder.conf << ENDOFFILE
if $programname == 'attribution_builder' then /var/log/mobi/attribution_builder/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/attribution_builder.service << ENDOFFILE
[Unit]
Description=Mobi Attribution Builder
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.attribution_builder
Nice=-5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=attribution_builder

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
sudo systemctl daemon-reload
```

Start Attribution Builder service:
```
systemctl start attribution_builder
```


### Configure Email Sender
```
mkdir -p /var/log/mobi/email_sender
chown -R mobi:mobi /var/log/mobi/email_sender

sudo touch /var/log/mobi/email_sender/stdout.log
sudo chown syslog:adm /var/log/mobi/email_sender/stdout.log

cat > /etc/rsyslog.d/email_sender.conf << ENDOFFILE
if $programname == 'email_sender' then /var/log/mobi/email_sender/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/email_sender.service << ENDOFFILE
[Unit]
Description=Mobi Email Sender
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.email_sender
Nice=-5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=email_sender

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
sudo systemctl daemon-reload
```

Start Attribution Builder service:
```
systemctl start email_sender
```


### Configure Retention Cleaner
```
mkdir -p /var/log/mobi/retention_cleaner
chown -R mobi:mobi /var/log/mobi/retention_cleaner

sudo touch /var/log/mobi/retention_cleaner/stdout.log
sudo chown syslog:adm /var/log/mobi/retention_cleaner/stdout.log

cat > /etc/rsyslog.d/retention_cleaner.conf << ENDOFFILE
if $programname == 'retention_cleaner' then /var/log/mobi/retention_cleaner/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/retention_cleaner.service << ENDOFFILE
[Unit]
Description=Mobi Retention Cleaner
After=network.target

[Service]
User=mobi
Group=www-data
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.retention_cleaner
Nice=-5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=retention_cleaner

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needded
```
sudo systemctl daemon-reload
```

Start Retention Cleaner service:
```
systemctl start retention_cleaner
```


### Configure Service Watcher
```
mkdir -p /var/log/mobi/service_watcher
chown -R mobi:mobi /var/log/mobi/service_watcher

sudo touch /var/log/mobi/service_watcher/stdout.log
sudo chown syslog:adm /var/log/mobi/service_watcher/stdout.log

cat > /etc/rsyslog.d/service_watcher.conf << ENDOFFILE
if $programname == 'service_watcher' then /var/log/mobi/service_watcher/stdout.log
& stop
ENDOFFILE

sudo systemctl restart rsyslog

cat > /etc/systemd/system/service_watcher.service << ENDOFFILE
[Unit]
Description=Mobi Service Watcher
After=network.target

[Service]
User=root
Group=root
WorkingDirectory=/home/mobi/mobi
Environment="MOBI_ENV=PROD"
ExecStart=/usr/bin/python3.7 -m mobi.service.service_watcher
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=service_watcher

[Install]
WantedBy=multi-user.target
ENDOFFILE
```

Restart daemon if needed
```
sudo systemctl daemon-reload
```

Start Metrics Service Watcher service:
```
systemctl start service_watcher
```

### Configure logs rotation

```
sudo vim /etc/logrotate.d/rsyslog
```

```
/var/log/mobi/catalog_api/stdout.log
/var/log/mobi/event_api/stdout.log
/var/log/mobi/metrics_api/stdout.log
/var/log/mobi/recommendation_api/stdout.log
/var/log/mobi/test_recommendation_api/stdout.log
/var/log/mobi/console/stdout.log
/var/log/mobi/console/stderr.log
/var/log/mobi/model_builder/stdout.log
/var/log/mobi/cache_builder/stdout.log
/var/log/mobi/dynamic_features_builder/stdout.log
/var/log/mobi/search_index_builder/stdout.log
/var/log/mobi/metrics_builder/stdout.log
/var/log/mobi/attribution_builder/stdout.log
/var/log/mobi/email_sender/stdout.log
/var/log/mobi/retention_cleaner/stdout.log
/var/log/mobi/service_watcher/stdout.log
{
        rotate 4
        weekly
        missingok
        notifempty
        compress
        delaycompress
        sharedscripts
        postrotate
                /usr/lib/rsyslog/rsyslog-rotate
        endscript
}
```

### Setup nginx

Copy certificates
```
sudo cp /home/mobi/mobi/docker/nginx/mobi_* /etc/ssl
```

Install nginx:

```
sudo apt install nginx
```

Setup a firewall:

```
sudo ufw enable
sudo ufw allow 'Nginx HTTP'
sudo ufw allow 'Nginx HTTPS'
sudo ufw allow 'OpenSSH'
sudo ufw status
```

Create api.mobimobi.tech config:

```
sudo vim /etc/nginx/sites-available/api.mobimobi.tech
```

Put the next content there:

```
server {
    listen 80;
    server_name console.mobimobi.tech;

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 80;
    server_name api.mobimobi.tech;

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 80;
    server_name docs.mobimobi.tech;

    location / {
        return 301 http://$host/api$request_uri;
    }

    location /api {
        rewrite ^/api/(.*) /$1 break;
        root /home/mobi/mobi/docs/openapi;
        index /api/redoc-static.html;
        try_files $uri $uri/ =404;
    }
}

server {
    listen 443;
    server_name console.mobimobi.tech;
    charset utf-8;

    ssl on;
    ssl_certificate /etc/ssl/console_bundle.crt;
    ssl_certificate_key /etc/ssl/console_private.key;
    ssl_session_timeout 5m;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers -ALL:EECDH+aRSA+AESGCM:EDH+aRSA+AESGCM:EECDH+aRSA+AES:EDH+aRSA+AES;
    ssl_prefer_server_ciphers on;

    root /home/mobi/mobi/mobi/web/console/react/build;

    location / {
        try_files $uri @backend;
    }

    location @backend {
        proxy_pass http://127.0.0.1:9701;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
    }
}

map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
}

server {
    listen 443;
    server_name api.mobimobi.tech;
    charset utf-8;

    ssl on;
    ssl_certificate /etc/ssl/mobi_bundle.crt;
    ssl_certificate_key /etc/ssl/mobi_private.key;
    ssl_session_timeout 5m;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers -ALL:EECDH+aRSA+AESGCM:EDH+aRSA+AESGCM:EECDH+aRSA+AES:EDH+aRSA+AES;
    ssl_prefer_server_ciphers on;

    location /v2/catalog {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/catalog_api.sock;
    }

    location /v2/user {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/event_api.sock;
    }

    location /v2/recommendation {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/recommendation_api.sock;
    }

    location /v2/metrics {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/metrics_api.sock;
    }

    location /v2/search {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/recommendation_api.sock;
    }

    location /event {
        rewrite ^/event/(.*) /$1 break;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/event_api.sock;
    }

    location /recommendation {
        rewrite ^/recommendation/(.*) /$1 break;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/recommendation_api.sock;
    }
    
    location /test_recommendation {
        rewrite ^/test_recommendation/(.*) /$1 break;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/test_recommendation_api.sock;
    }
    
    location /metrics {
        rewrite ^/metrics/(.*) /$1 break;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP ip_address;
        proxy_pass http://unix:/home/mobi/metrics_api.sock;
    }
    
    location / {
        default_type application/json;
        return 404 '{"status":"ERROR","error":{"code":404,"description":"Not Found"}}';
    }
}
```

Create a symlink:

```
sudo ln -s /etc/nginx/sites-available/api.mobimobi.tech /etc/nginx/sites-enabled/api.mobimobi.tech
```

Restart nginx:

```
sudo /etc/init.d/nginx restart
```

### Update the service
```
sudo systemctl stop mongod
sudo systemctl stop eventapi
sudo systemctl stop recommendationapi
sudo systemctl stop metricsapi
sudo systemctl stop model_builder
sudo /etc/init.d/nginx stop

su - mobi
cd mobi
git reset --hard origin/master
pip3 install -rrequirements.txt
exit

sudo systemctl start mongod
sudo systemctl start eventapi
sudo systemctl start recommendationapi
sudo systemctl start metricsapi
sudo systemctl start model_builder
sudo /etc/init.d/nginx start
```


# Docker deployments
All scripts and docker-compose commands are meant to run from `docker` folder.

## Install

```
# where to deploy mobi folder
export MOBI_DEPLOY_PATH=/home/mobi/mobi
export MOBI_USER=mobi

./setup
```

## Setup DB
- launch only mongo
```
docker-compose run --service-ports -d mongo
```
- setup db
```
docker-compose run setup
```


## Deploy

- run, TODO: these actions needs to be run from CI
```
sudo su - mobi
cd mobi

# git commit ref or branch name
export MOBI_DEPLOY_VERSION=ci
# absolute path to git repository with the project source code
export MOBI_DEPLOY_PATH=~/mobi
# Linux user who run the app
export MOBI_USER=mobi

./deploy
exit

# FIXME dockerize these services
sudo /etc/init.d/nginx restart
```
- be sure to stop all legacy services
```
sudo systemctl stop eventapi
sudo systemctl stop recommendationapi
sudo systemctl stop metricsapi
sudo systemctl stop model_builder
sudo systemctl stop mongod
sudo /etc/init.d/nginx stop
```
- start not yet dockerized services
```
sudo systemctl start mongo
sudo systemctl start eventapi
sudo systemctl start recommendationapi
sudo systemctl start metricsapi
sudo systemctl start model_builder
sudo /etc/init.d/nginx start
```
