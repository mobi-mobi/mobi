# desing notes

1. Online-метрики (то, что не надо считать):

- Общее количество запросов к нашей системе;
- Количество пользовательских эвентов разного вида (У нас есть EventType Enum в коде);
- Количество запросов к Recommendation API с возможностью проекции на тип запроса (похожий продукт / рекомендовано вам / поиск) и популяцию пользователя;

Всё это дело хочется уметь видеть в сумме и в проекции на каждую популяцию пользователя (Population Enum).
Так же, хочется иметь возможность посмотреть "средние" значения на каждый тип пользователя.

Solutions:

1. Prometheus + Graphite + Graphana
Suitable for metrics, applicatives and functionals.
+ Prometheus lib is easy to integrate
+ powerfull aggregation engine
+ Graphana we will need anyway
- we need Graphite or Prometheus for persistence

2. ElasticSearch + Logstach + Kibana
Suitable for logs, but all queries of type how many request we have by "even_type" could be solved with Kibana.
+ we will need it anyway for logs
+ Kibana has a very powerfull query engine
- complex stack
- log format doesn't suit for exact functional metrics
