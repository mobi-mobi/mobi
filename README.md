# Mobi

- build mmh3 on OS X >= Mojave
https://github.com/hajimes/mmh3/issues/20#issuecomment-448580189

- run Docker Mongo
```
docker run --rm -d --name mongodb -p 27017:27017 mongo:4.1
```

- setup Mongo DB
```
python -m mobi.dao.setup --database mongodb mongodb
```

- call the API with token
http://localhost:9090/status?token=testtoken

- run tests from cli
```
pip install -r requirements.txt
. ./test
pytest tests/test_event_api.py
```

- run 1 service with docker
```
docker-compose run -u $(id -u):$(id -g) -v /Users/a.eryshev/dev/personal/mobimobi/mobi/docker:/var/mobi --no-deps event /bin/bash
```

## Gitlab CI
- https://docs.gitlab.com/runner is used to run CI jobs from `gitlab-ci.yml`
- run 1 job=test from CI pipeline locally:
```
gitlab-runner exec docker test
```
- run docker in docker builds
```
gitlab-runner exec docker 
    --docker-privileged \
    --docker-tlsverify \
    --env CONTAINER_TEST_IMAGE=mobi:test \
    build
```
- login to Docker registry
```
docker login registry.gitlab.com/mobi-mobi/mobi -u <Gitlab user> -p <Gitlab password>
```
- pull an image
```
docker pull registry.gitlab.com/mobi-mobi/mobi/mobi:ci
```

## Mongo
- connect to remote Mongo
```
ssh alexey@84.201.180.183 -NfL 2777:localhost:27017
```
