import pathlib

from typing import List

__all__ = [
    "report_full_email",
    "report_row_header",
    "report_row_text",
    "report_row_subtitle",
    "report_row_1_metric_value_and_text",
    "report_row_4_metric_values",
    "report_row_4_metric_values_with_deltas",
    "report_row_img",
    "report_row_title_and_4_cells_table",
    "report_row_footer"
]

_row_header_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows").joinpath("header.html")
with open(_row_header_file, "r") as fp:
    _row_header_template = fp.read()

_row_text_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows").joinpath("text.html")
with open(_row_text_file, "r") as fp:
    _row_text_template = fp.read()

_row_subtitle_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows")\
    .joinpath("subtitle.html")
with open(_row_subtitle_file, "r") as fp:
    _row_subtitle_template = fp.read()

_row_1_metric_value_and_text_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows")\
    .joinpath("1_metric_value_and_text.html")
with open(_row_1_metric_value_and_text_file, "r") as fp:
    _row_1_metric_value_and_text_template = fp.read()

_row_4_metric_values_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows")\
    .joinpath("4_metric_values.html")
with open(_row_4_metric_values_file, "r") as fp:
    _row_4_metric_values_template = fp.read()

_row_4_metric_values_with_deltas_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows")\
    .joinpath("4_metric_values_with_deltas.html")
with open(_row_4_metric_values_with_deltas_file, "r") as fp:
    _row_4_metric_values_with_deltas_template = fp.read()

_row_img_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows").joinpath("image.html")
with open(_row_img_file, "r") as fp:
    _row_img_template = fp.read()

_row_title_and_4_cells_table_row_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows")\
    .joinpath("title_and_4_cells_table_row.html")
with open(_row_title_and_4_cells_table_row_file, "r") as fp:
    _row_title_and_4_cells_table_template = fp.read()

_row_footer_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("rows").joinpath("footer.html")
with open(_row_footer_file, "r") as fp:
    _row_footer_template = fp.read()

_full_email_file = pathlib.Path(__file__).parent.absolute().joinpath("report").joinpath("email.html")
with open(_full_email_file, "r") as fp:
    _full_email_template = fp.read()


def html_special_chars(s: str) -> str:
    return s.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")


# "#6625ab"
def report_full_email(html_content: str, bgcolor: str = "#ffffff") -> str:
    return _full_email_template.replace("%BGCOLOR%", bgcolor).replace("%HTML_CONTENT%", html_content)


def report_row_header(title: str, bgcolor: str = "#ffffff") -> str:
    return _row_header_template \
        .replace("%BGCOLOR%", bgcolor) \
        .replace("%TITLE%", html_special_chars(title))


def report_row_text(text: str, bgcolor: str = "#ffffff") -> str:
    return _row_text_template \
        .replace("%BGCOLOR%", bgcolor) \
        .replace("%TEXT%", html_special_chars(text))


def report_row_subtitle(title: str, bgcolor: str = "#f6edff") -> str:
    return _row_subtitle_template \
        .replace("%BGCOLOR%", bgcolor) \
        .replace("%TITLE%", html_special_chars(title))


def report_row_1_metric_value_and_text(value: str, text: str, bgcolor: str = "#ffffff") -> str:
    return _row_1_metric_value_and_text_template \
        .replace("%BGCOLOR%", bgcolor) \
        .replace("%VALUE%", value) \
        .replace("%TEXT%", text)


def report_row_4_metric_values(titles: List[str], values: List[str], bgcolor: str = "#ffffff") -> str:
    assert len(titles) == 4
    assert len(values) == 4

    result = _row_4_metric_values_template.replace("%BGCOLOR%", bgcolor)

    for pos, (title, value) in enumerate(zip(titles, values)):
        result = result.replace(f"%TITLE{pos+1}%", html_special_chars(title))
        result = result.replace(f"%VALUE{pos+1}%", html_special_chars(value))

    return result


def report_row_4_metric_values_with_deltas(titles: List[str], values: List[str], deltas: List[str],
                                           bgcolor: str = "#ffffff") -> str:
    assert len(titles) == 4
    assert len(values) == 4
    assert len(deltas) == 4

    result = _row_4_metric_values_with_deltas_template.replace("%BGCOLOR%", bgcolor)

    for pos, (title, value, delta) in enumerate(zip(titles, values, deltas)):
        result = result.replace(f"%TITLE{pos+1}%", html_special_chars(title))
        result = result.replace(f"%VALUE{pos+1}%", html_special_chars(value))
        result = result.replace(f"%DELTA{pos+1}%", html_special_chars(delta))

    return result


def report_row_img(src: str, alt: str = "", bgcolor: str = "#ffffff") -> str:
    return _row_img_template \
        .replace("%BGCOLOR%", bgcolor) \
        .replace("%SRC%", src) \
        .replace("%ALT%", html_special_chars(alt))


def report_row_title_and_4_cells_table(title: str, values: List[str], bgcolor: str = "#ffffff") -> str:
    result = _row_title_and_4_cells_table_template \
        .replace("%BGCOLOR%", bgcolor) \
        .replace("%TITLE%", html_special_chars(title))

    for pos, value in enumerate(values):
        result = result.replace(f"%VALUE{pos+1}%", value)

    return result


def report_row_footer(unsubscribe_uri: str, bgcolor: str = "#ffffff") -> str:
    return _row_footer_template.replace("%UNSUBSCRIBE_URI%", unsubscribe_uri).replace("%BGCOLOR%", bgcolor)
