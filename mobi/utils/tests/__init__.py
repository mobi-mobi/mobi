import random
import string

from datetime import datetime, timedelta
from typing import List, Optional

from mobi.core.common.pricetype import PriceType
from mobi.core.models import ApiToken, AttributedEvent, AttributionType, Audience, AudiencesUnion, AudienceSplitTree, \
    AudienceSplit, AudienceSplitType, Event, EventPlatform, EventType, Product, Recommendation, RecommendationSource, \
    RecommendationType, TokenType, TokenPermission, TokenHashAlgo
from mobi.core.models.api_token import hash_token
from mobi.core.models.zone import Campaign, CampaignStatus, Zone, ZoneStatus, ZoneSettings, ZoneType,\
    ZoneExtraProductsPosition
from mobi.core.population import Population


def random_string(length: int = 10):
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(length))


def random_product(product_id: str = None, title: str = None, active: bool = True,
                   brand: str = None, categories: List[str] = None, color: str = None,
                   version: int = None, description: str = None, available_from: datetime = None,
                   available_until: datetime = None) -> Product:
    product_id = product_id or random_string()

    return Product(
        product_id=product_id,
        title=title or f"Product title of {product_id} (r: {random_string()})",
        currency="USD",
        price=PriceType.parse("1.0"),
        active=active,
        brand=brand or f"Product Brand of {product_id} (r: {random_string()})",
        categories=categories or [f"Product Category of {product_id} (r: {random_string()})"],
        color=color or f"Product color of {product_id} (r: {random_string()})",
        version=version,
        description=description,
        available_from=available_from,
        available_until=available_until
    )


def random_event(user_id: str = None, user_population: Population = Population.TARGET, product_id: str = None,
                 event_type: EventType = EventType.PRODUCT_VIEW, date: datetime = None) -> Event:
    return Event(
        user_id=user_id or random_string(),
        user_population=user_population,
        product_id=product_id or random_string(),
        product_version=1,
        event_type=event_type,
        event_platform=EventPlatform.OTHER,
        date=date or datetime.utcnow()
    )


def random_recommendation(user_id: str = None, date: datetime = None,
                          type: RecommendationType = RecommendationType.PERSONAL_RECOMMENDATIONS,
                          source: RecommendationSource = RecommendationSource.MODEL,
                          product_ids: List[str] = None, source_product_id: str = None) -> Recommendation:
    return Recommendation(
        user_id=user_id or random_string(),
        date=date or datetime.utcnow(),
        type=type,
        source=source,
        product_ids=product_ids or [random_string() for _ in range(10)],
        source_product_id=source_product_id
    )


def random_zone_settings(recommendations_num: int = 20, same_brand: bool = False, brands: List[str] = None,
                         categories: List[str] = None, extra_product_ids: List[str] = None,
                         extra_products_position: ZoneExtraProductsPosition = None,
                         save_organic_extra_products_positions: bool = None, dont_show_organic_products: bool = None):
    return ZoneSettings(
        recommendations_num=recommendations_num,
        same_brand=same_brand,
        brands=brands,
        categories=categories,
        extra_product_ids=extra_product_ids,
        extra_products_position=extra_products_position,
        save_organic_extra_products_positions=save_organic_extra_products_positions,
        dont_show_organic_products=dont_show_organic_products
    )


def random_zone(zone_id: str = None, name: str = None, zone_type: ZoneType = ZoneType.PERSONAL_RECOMMENDATIONS,
                description: str = None, status: ZoneStatus = ZoneStatus.ACTIVE, campaign_ids: List[str] = None,
                zone_settings: ZoneSettings = None):
    zone_id = zone_id or random_string()

    return Zone(
        zone_id=zone_id,
        name=name or f"Reco zone with id = {zone_id}",
        zone_type=zone_type,
        description=description,
        status=status,
        campaign_ids=campaign_ids or [],
        zone_settings=zone_settings or random_zone_settings()
    )


def random_campaign(campaign_id: str = None, name: str = None, zone_settings: ZoneSettings = None,
                    description: str = None, status: CampaignStatus = CampaignStatus.ACTIVE,
                    start_date: datetime = None, end_date: datetime = None):
    campaign_id = campaign_id or random_string()

    return Campaign(
        campaign_id=campaign_id,
        name=name or f"Campaign name for campaign_id={campaign_id}",
        zone_settings=zone_settings or random_zone_settings(),
        description=description,
        status=status,
        start_date=start_date,
        end_date=end_date
    )


def random_api_token(client: str = None, token_hash: str = None, token_hash_algo: TokenHashAlgo = None,
                     first_letters: str = None, token_type: TokenType = None, expiration_date: datetime = None,
                     permissions: List[TokenPermission] = None, description: str = None) -> ApiToken:
    random_token_str = random_string()

    return ApiToken(
        client=client or random_string(),
        token_hash=token_hash or hash_token(random_token_str, TokenHashAlgo.SHA256),
        token_hash_algo=token_hash_algo or TokenHashAlgo.SHA256,
        first_letters=first_letters or random_token_str[0:4],
        token_type=token_type or TokenType.TEST,
        expiration_date=expiration_date or datetime.utcnow() + timedelta(days=1),
        permissions=permissions if permissions is not None else [p for p in TokenPermission],
        description=description or random_string()
    )


def random_attributed_event(event: Event = None, attribution_type: AttributionType = AttributionType.EMPIRICAL,
                            attributed_recommendations: List[Recommendation] = None, has_matched_display: bool = True):
    event = event or random_event()
    attributed_recommendations = attributed_recommendations or \
        [random_recommendation(user_id=event.user_id) for _ in range(random.randrange(1, 5))]

    return AttributedEvent(event, attribution_type, attributed_recommendations, has_matched_display)


def random_audience(audience_id: str = None, audience_name: str = None, audience_description: str = None) -> Audience:
    return Audience(
        audience_id=audience_id or random_string(),
        audience_name=audience_name or random_string(),
        audience_description=audience_description or random_string()
    )


def random_audiences_union(audiences_union_id: str = None, audiences_union_name: str = None,
                           audiences_union_description: str = None, audience_ids: List[str] = None) -> AudiencesUnion:
    return AudiencesUnion(
        audiences_union_id=audiences_union_id or random_string(),
        audiences_union_name=audiences_union_name or random_string(),
        audiences_union_description=audiences_union_description or random_string(),
        audience_ids=audience_ids or ["1" + random_string(), "2" + random_string()]
    )


def random_audience_split(split_type: AudienceSplitType = None, split_id: str = None,
                          children_split_ids: List[Optional[str]] = None, percentiles: List[float] = None,
                          quantities: List[int] = None, audience_ids: Optional[List[Optional[List[str]]]] = None,
                          brands: List[str] = None, categories: List[str] = None,
                          product_ids: List[str] = None, session_length_min: int = None) -> AudienceSplit:
    return AudienceSplit(
        split_type=split_type or AudienceSplitType.RANDOM,
        split_id=split_id or random_string(),
        children_split_ids=children_split_ids,
        percentiles=percentiles or [50.0],
        quantities=quantities,
        audience_ids=audience_ids,
        brands=brands,
        categories=categories,
        product_ids=product_ids,
        session_length_min=session_length_min
    )


def random_audiences_split_tree(split_tree_id: str = None, name: str = None, description: str = None,
                                root_split_id: str = None, audience_splits: List[AudienceSplit] = None) \
        -> AudienceSplitTree:
    audience_splits = audience_splits or [random_audience_split()]

    return AudienceSplitTree(
        split_tree_id=split_tree_id or random_string(),
        name=name or random_string(),
        description=description or random_string(),
        root_split_id=root_split_id or audience_splits[0].split_id,
        audience_splits=audience_splits
    )
