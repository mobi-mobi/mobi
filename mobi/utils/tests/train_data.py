import random

from datetime import datetime, timedelta
from typing import List, Tuple

from mobi.core.common.pricetype import PriceType
from mobi.core.models import Event, EventPlatform, EventType, Product
from mobi.core.population import Population


def simple_train_data() -> Tuple[List[Product], List[Event]]:
    products = []

    brand_nike = "Nike"
    brand_adidas = "Adidas"
    brand_asics = "Asics"
    brand_ikea = "IKEA"

    products.append(Product(
        product_id="Red Nike",
        title="Red Nike",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_nike,
        categories=["Shoes", "Snickers"],
        color="Red",
        tags=["red", "nike"],
        enriched_tags=["red", "nike"]
    ))

    products.append(Product(
        product_id="Blue Nike",
        title="Blue Nike",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_nike,
        categories=["Shoes", "Snickers"],
        color="Blue",
        tags=["blue", "nike"],
        enriched_tags=["blue", "nike"]
    ))

    products.append(Product(
        product_id="Black Nike Running",
        title="Black Nike Running",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_nike,
        categories=["Shoes", "Snickers", "Running"],
        color="Black",
        tags=["black", "nike"],
        enriched_tags=["black", "nike", "running"]
    ))

    products.append(Product(
        product_id="Blue Adidas",
        title="Blue Adidas",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_adidas,
        categories=["Shoes", "Snickers"],
        color="Blue",
        tags=["blue", "adidas"],
        enriched_tags=["blue", "adidas"]
    ))

    products.append(Product(
        product_id="White Adidas",
        title="White Adidas",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_adidas,
        categories=["Shoes", "Snickers"],
        color="White",
        tags=["white", "adidas"],
        enriched_tags=["white", "adidas"]
    ))

    products.append(Product(
        product_id="White Round table",
        title="White Round table",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_ikea,
        categories=["Tables"],
        color="White",
        tags=["white", "table"],
        enriched_tags=["white", "round", "table"]
    ))

    products.append(Product(
        product_id="Black Round table",
        title="Black Round table",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_ikea,
        categories=["Tables"],
        color="Black",
        tags=["black", "table"],
        enriched_tags=["black", "round", "table"]
    ))

    products.append(Product(
        product_id="Black Square table",
        title="Black Square table",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_ikea,
        categories=["Tables"],
        color="Black",
        tags=["black", "table"],
        enriched_tags=["black", "square", "table"]
    ))

    products.append(Product(
        product_id="Black Asics Running",
        title="Black Asics Running",
        price=PriceType.parse("1.0"),
        currency="USD",
        active=True,
        brand=brand_asics,
        categories=["Shoes", "Snickers", "Running"],
        color="Black",
        tags=["black", "asics"],
        enriched_tags=["black", "asics", "running"]
    ))

    events = []

    for i in range(30):
        nike_lover_choices = random.choices(
            products,
            [1, 1, 1, 0.1, 0.1, 0.01, 0.01, 0.01, 0.1],
            k=10
        )

        adidas_lover_choices = random.choices(
            products,
            [0.1, 0.1, 0.1, 1, 1, 0.01, 0.01, 0.01, 0.1],
            k=10
        )

        snickers_lover_choices = random.choices(
            products,
            [1, 1, 1, 1, 1, 0.01, 0.01, 0.01, 1],
            k=10
        )

        ikea_lover_choices = random.choices(
            products,
            [0.01, 0.01, 0.01, 0.01, 0.01, 1, 1, 1, 0.01],
            k=10
        )

        for p in nike_lover_choices:
            events.append(
                [f"nike_lover_{i}",
                 p.product_id]
            )

        for p in adidas_lover_choices:
            events.append(
                [f"adidas_lover_{i}",
                 p.product_id]
            )

        for p in snickers_lover_choices:
            events.append(
                [f"snickers_lover_{i}",
                 p.product_id]
            )

        for p in ikea_lover_choices:
            events.append(
                [f"ikea_lover_{i}",
                 p.product_id]
            )

    real_events = []

    initial_date = datetime.utcnow() - timedelta(days=1)
    event_num = 0

    for user_id, product_id in events:
        event_num += 1

        real_events.append(Event(
            user_id=user_id,
            user_population=Population.TARGET,
            product_id=product_id,
            product_version=1,
            event_type=EventType.PRODUCT_VIEW,
            event_platform=EventPlatform.OTHER,
            date=initial_date + timedelta(seconds=30 * event_num)
        ))
        real_events.append(Event(
            user_id=user_id,
            user_population=Population.TARGET,
            product_id=product_id,
            product_version=1,
            event_type=EventType.PRODUCT_TO_BASKET,
            event_platform=EventPlatform.OTHER,
            date=initial_date + timedelta(seconds=30 * event_num)
        ))

    return products, real_events


def simple_train_data_with_null_values() -> Tuple[List[Product], List[Event]]:
    products, real_events = simple_train_data()

    products[0].categories = None
    products[1].categories = []
    products[2].color = None
    products[3].color = ""
    products[4].tags = None
    products[5].tags = []
    products[6].enriched_tags = None
    products[7].enriched_tags = []

    return products, real_events
