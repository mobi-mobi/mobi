from hashlib import sha256
from random import choices
from string import ascii_letters, digits


def generate_salt(k: int):
    return "".join(choices(ascii_letters + digits, k=k))


def generate_hash(password: str, salt: str):
    return sha256((salt + password + salt).encode("utf-8")).hexdigest()


def generate_token(k: int):
    return "".join(choices(ascii_letters + digits, k=k))
