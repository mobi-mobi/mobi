from datetime import datetime, time, timedelta
from typing import Any, Dict

from mobi.config.system.service.search_index_builder import get_search_index_builder_config
from mobi.core.logging import get_logger
from mobi.dao import get_catalog_dao, get_search_dao
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.search.interface import MobiSearchDao
from mobi.service.task.interface import MobiDaemonTask


logger = get_logger()


def catalog_dao() -> MobiCatalogDao:
    return get_catalog_dao(get_search_index_builder_config())


def search_dao() -> MobiSearchDao:
    return get_search_dao(get_search_index_builder_config())


class SearchIndexBuildTask(MobiDaemonTask):
    REFRESH_INTERVAL = timedelta(hours=1)

    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        if client_context.get("last_refresh") is not None \
                and datetime.utcnow() < client_context["last_refresh"] + self.REFRESH_INTERVAL:
            return

        if not search_dao().index_exists(client):
            logger.info(f"Index for the client {client} does not exist. Creating one...")
            search_dao().delete_index(client)
            search_dao().create_index(client)

        logger.info(f"Processing client {client}...")

        dao = search_dao()
        updated_ts = int(datetime.utcnow().timestamp())

        indexed_products = 0
        indexed_brands = 0
        indexed_categories = 0

        for product in catalog_dao().read_all_products(client, include_inactive=True, include_not_in_stock=True):
            dao.index_product(client, product, updated_ts)
            indexed_products += 1

        for brand in catalog_dao().read_all_brands(client):
            dao.index_brand(client, brand, updated_ts)
            indexed_brands += 1

        for category in catalog_dao().read_all_categories(client):
            dao.index_category(client, category, updated_ts)
            indexed_categories += 1

        logger.info(f"Indexed {indexed_products} products, {indexed_brands} brands, {indexed_categories} categories")
        logger.info("Deleting fatigue data...")

        deleted = dao.delete_fatigue_data(client, updated_ts)

        logger.info(f"Deleted {deleted} elements")
        logger.info("Refreshing index...")

        dao.refresh_index(client)

        logger.info("Refreshed")

        logger.info(f"Client {client} processed.")

        client_context["last_refresh"] = datetime.utcnow()
