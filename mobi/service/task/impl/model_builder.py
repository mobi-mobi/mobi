import gc

from datetime import datetime, timedelta
from logging import DEBUG
from traceback import print_exc
from typing import Any, Dict

from mobi.config.client import get_client_config
from mobi.config.system.service.model_builder import get_model_builder_config
from mobi.core.logging import get_logger
from mobi.core.models import EventType, ModelStatus
from mobi.dao import get_catalog_dao, get_event_dao, get_model_dao
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.object import MobiModelDao
from mobi.ml.factory import get_model_factory
from mobi.service.task.interface import MobiDaemonTask


def catalog_dao() -> MobiCatalogDao:
    return get_catalog_dao(get_model_builder_config())


def event_dao() -> MobiEventDao:
    return get_event_dao(get_model_builder_config())


def model_dao() -> MobiModelDao:
    return get_model_dao(get_model_builder_config())


logger = get_logger()
logger.setLevel(DEBUG)


class ModelBuilderTask(MobiDaemonTask):
    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        logger.info(f"Checking client {client}")
        model_infos = model_dao().read_all_model_dumps(client, limit=10)
        try:
            fresh_model = max(model_infos, key=lambda x: x.date)
            max_date = fresh_model.date
            logger.info(f"The latest model was trained on {max_date}")
            logger.info(f"Model info: {fresh_model}")
            del fresh_model
        except ValueError:
            max_date = None
            logger.info(f"No models were ever trained for client {client}")

        if max_date is not None and datetime.utcnow() - max_date < timedelta(hours=1):
            logger.info(f"Last model was trained on {max_date}. Skipping this client...")
            return

        last_attempt = client_context.get("last_attempt")

        if last_attempt is not None and datetime.utcnow() - last_attempt < timedelta(hours=1):
            logger.info(f"Last training attempt was on {last_attempt}. Skipping this "
                        f"client...")
            return

        # TRAINING A NEW MODEL
        try:
            logger.info("Creating a model factory...")
            model_factory = get_model_factory(get_client_config(client), get_model_builder_config())
            logger.info(f"Logger factory created: {model_factory.__class__.__name__}")

            logger.info("Training and compiling a new model...")
            model = model_factory.train(client)

            all_products = list(catalog_dao().read_all_products(client))
            visited_products = {event.product_id for event in
                                event_dao().read_events(client, from_date=datetime.utcnow() - timedelta(days=2))}

            to_basket_products = {event.product_id for event in
                                  event_dao().read_events(client, from_date=datetime.utcnow() - timedelta(days=3))
                                  if event.event_type == EventType.PRODUCT_TO_BASKET}

            model.set_non_recommendable_product_ids(
                [product.product_id for product in all_products
                 if product.product_id not in visited_products or product.product_id not in to_basket_products
                 or not product.active or not product.in_stock])

            model.compile(all_products)
            logger.info(f"Model compiled: {model.meta_info()['model_dump_info']}. Saving...")

            model_dao().save_model_dump(client, model.dump())
            del model
            del model_factory
            del all_products
            del visited_products
            del to_basket_products
            logger.info("Saved!")
        except Exception:
            logger.info(f"There was an exception raised while training a new model for {client}:")
            print_exc()

        client_context["last_attempt"] = datetime.utcnow()

        # DELETING OLD MODELS
        keep_ready_num = 3
        keep_other_num = 3

        model_infos = list(model_dao().read_all_model_dumps(client, limit=100))
        model_infos = sorted(model_infos, key=lambda x: x.date, reverse=True)

        ready_found = 0
        others_found = 0

        for model_info in model_infos:
            to_delete = False

            if model_info.status == ModelStatus.READY.value:
                ready_found += 1
                if ready_found > keep_ready_num:
                    to_delete = True
            else:
                others_found += 1
                if others_found > keep_other_num:
                    to_delete = True

            if to_delete:
                logger.info(f"Deleting model {model_info.model_id}...")
                try:
                    model_dao().delete_model_dump(client, model_info.model_id)
                    logger.info(f"Model deleted.")
                except Exception:
                    logger.info(f"There was an exception raised while deleting old models for {client}:")
                    print_exc()

        del model_info
        del model_infos

        logger.info("Collecting garbage...")
        gc.collect()
