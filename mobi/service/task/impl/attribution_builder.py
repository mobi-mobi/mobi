from datetime import datetime, timedelta
from logging import DEBUG
from random import shuffle
from typing import Any, Dict, Generator, List, Optional, Tuple

from mobi.config.system.service import get_attribution_builder_config
from mobi.core.logging import get_logger
from mobi.dao import get_catalog_dao, get_event_dao, get_metrics_dao, get_recolog_dao
from mobi.core.models import AttributedEvent, AttributedRecommendation, AttributionType, Event, EventType, \
    Recommendation
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.recolog import MobiRecologDao
from mobi.service.task.interface import MobiDaemonTask


def catalog_dao() -> MobiCatalogDao:
    return get_catalog_dao(get_attribution_builder_config())


def event_dao() -> MobiEventDao:
    return get_event_dao(get_attribution_builder_config())


def metrics_dao() -> MobiMetricsDao:
    return get_metrics_dao(get_attribution_builder_config())


def recolog_dao() -> MobiRecologDao:
    return get_recolog_dao(get_attribution_builder_config())


logger = get_logger()
logger.setLevel(DEBUG)


class AttributionBuilderTask(MobiDaemonTask):
    OLDEST_FROM_DATE_DELTA = timedelta(days=28)
    MIN_PROCESSING_WINDOW = timedelta(minutes=15)
    PROCESSING_WINDOW = timedelta(hours=3)
    MIN_TILL_DATE_DELAY = timedelta(minutes=15)

    SUPPORTED_ATTRIBUTION_TYPES = {
        AttributionType.EMPIRICAL
    }

    @staticmethod
    def _next_session(sessions: Generator[List[Event], None, None]) -> Optional[List[Event]]:
        try:
            return next(sessions)
        except StopIteration:
            return None

    @staticmethod
    def _user_recommendations(recommendations: Generator[Recommendation, None, None]) \
            -> Generator[List[Recommendation], None, None]:
        user_recommendations = []

        for recommendation in recommendations:
            if user_recommendations and user_recommendations[0].user_id != recommendation.user_id:
                yield user_recommendations
                user_recommendations = []

            user_recommendations.append(recommendation)

        if user_recommendations:
            yield user_recommendations

    @staticmethod
    def _next_user_recommendations(user_recommendations: Generator[List[Recommendation], None, None]) \
            -> Optional[List[Recommendation]]:
        try:
            return next(user_recommendations)
        except StopIteration:
            return None

    def read_sessions_and_recommendations(self, client: str, from_date: datetime, till_date: datetime,
                                          session_max_time_gap: timedelta = None) \
            -> Generator[Tuple[List[Event], List[Recommendation]], None, None]:
        read_user_sessions_kwargs = {}
        if session_max_time_gap is not None:
            read_user_sessions_kwargs["session_max_time_gap"] = session_max_time_gap

        sessions = event_dao().read_user_sessions(client, from_date=from_date, till_date=till_date,
                                                  **read_user_sessions_kwargs)

        user_recommendations = self._user_recommendations(
            recolog_dao().read_ordered_recommendations(client, from_date=from_date - timedelta(minutes=1),
                                                       till_date=till_date + timedelta(seconds=1))
        )

        current_session = None
        current_user_recommendations = None

        while True:
            if current_session is None:
                current_session = self._next_session(sessions)

            if current_user_recommendations is None:
                current_user_recommendations = self._next_user_recommendations(user_recommendations)

            if current_session is None or current_user_recommendations is None:
                break

            if current_session[0].user_id == current_user_recommendations[0].user_id:
                yield current_session, current_user_recommendations
                current_session = None

            elif current_session[0].user_id < current_user_recommendations[0].user_id:
                current_session = None

            elif current_session[0].user_id > current_user_recommendations[0].user_id:
                current_user_recommendations = None

    @classmethod
    def calc_reference_attributions(cls, session: List[Event], user_recommendations: List[Recommendation]) \
            -> Generator[AttributedEvent, None, None]:
        pass

    @classmethod
    def calc_empirical_attributions(cls, session: List[Event], user_recommendations: List[Recommendation]) \
            -> Generator[AttributedEvent, None, None]:
        def close_dates(date1, date2, eps=timedelta(seconds=5)):
            return date1 - date2 < eps and date2 - date1 < eps

        prev_view_event = None
        viewed_product_ids = set()
        recommended_product_clicks: Dict[str, List[Recommendation]] = dict()

        for event in session:
            attributed_recommendations = None

            if event.event_type in {EventType.PRODUCT_TO_BASKET, EventType.PRODUCT_SALE, EventType.PRODUCT_TO_WISHLIST}:
                if event.event_type == EventType.PRODUCT_SALE:
                    # This is just a copy of the following "if" block
                    for basket_item in event.basket_items:
                        if basket_item.product_id in recommended_product_clicks:
                            for reco in recommended_product_clicks[basket_item.product_id]:
                                if timedelta(seconds=0) <= event.date - reco.date < timedelta(minutes=60):
                                    if attributed_recommendations is None:
                                        attributed_recommendations = []
                                    attributed_recommendations.append(reco)
                    if attributed_recommendations:
                        yield AttributedEvent(event, AttributionType.EMPIRICAL, attributed_recommendations,
                                              has_matched_display=True)
                elif event.product_id in recommended_product_clicks:
                    for reco in recommended_product_clicks[event.product_id]:
                        if timedelta(seconds=0) <= event.date - reco.date < timedelta(minutes=15):
                            if attributed_recommendations is None:
                                attributed_recommendations = []
                            attributed_recommendations.append(reco)
                    if attributed_recommendations:
                        yield AttributedEvent(event, AttributionType.EMPIRICAL, attributed_recommendations,
                                              has_matched_display=True)
                elif event.event_type in {EventType.PRODUCT_TO_BASKET, EventType.PRODUCT_TO_WISHLIST}:
                    # If product_id is not in the recommended_clicks, it still could have been appeared in the basket
                    # or in the wishlist directly from the recommendation. What can tell us if there is a match:
                    # There is a recommendation that satisfies following conditions:
                    # 1. event.date - 3 minute < recommendation.date < event.date
                    # 2. event.product_id in recommendation.product_ids
                    # 3. event.product_id not in previously viewed product ids
                    try:
                        last_reco = max(
                            filter(
                                lambda r: event.date - timedelta(minutes=3) < r.date < event.date
                                and event.product_id in r.product_ids,
                                user_recommendations
                            ),
                            key=lambda r: r.date
                        )
                    except ValueError:
                        last_reco = None

                    if last_reco is not None:
                        if viewed_product_ids is None or event.product_id not in viewed_product_ids:
                            if event.product_id not in recommended_product_clicks:
                                recommended_product_clicks[event.product_id] = []
                            recommended_product_clicks[event.product_id].append(last_reco)
                            yield AttributedEvent(event, AttributionType.EMPIRICAL, [last_reco],
                                                  has_matched_display=False)

            elif event.event_type == EventType.PRODUCT_VIEW:
                viewed_product_ids.add(event.product_id)

                for recommendation in user_recommendations:
                    if recommendation.date > event.date:
                        break
                    if recommendation.date < event.date - timedelta(minutes=5):
                        continue

                    if event.product_id in recommendation.product_ids:
                        no_source_product_match = recommendation.source_product_id is None

                        last_product_source_id_match = \
                            recommendation.source_product_id is not None \
                            and prev_view_event is not None \
                            and prev_view_event.product_id == recommendation.source_product_id \
                            and close_dates(prev_view_event.date, recommendation.date)

                        last_reco_after_last_event_match = \
                            recommendation.source_product_id is not None \
                            and prev_view_event is not None \
                            and prev_view_event.date < recommendation.date < event.date

                        match = no_source_product_match \
                            or last_product_source_id_match \
                            or last_reco_after_last_event_match

                        if match:
                            if event.product_id not in recommended_product_clicks:
                                recommended_product_clicks[event.product_id] = []
                            recommended_product_clicks[event.product_id].append(recommendation)

                            if attributed_recommendations is None:
                                attributed_recommendations = [recommendation]
                            else:
                                attributed_recommendations.append(recommendation)
                if attributed_recommendations:
                    yield AttributedEvent(event, AttributionType.EMPIRICAL, attributed_recommendations,
                                          has_matched_display=True)

                prev_view_event = event

    @classmethod
    def calc_time_window_attributions(cls, session: List[Event], user_recommendations: List[Recommendation]) \
            -> Generator[AttributedEvent, None, None]:
        pass

    @classmethod
    def calc_unmatched_recommendations(cls, user_recommendations: List[Recommendation],
                                       attributed_events: List[AttributedEvent]) -> List[Recommendation]:
        all_ = sorted(user_recommendations, key=lambda r: (r.user_id, r.date))
        pos_ = sorted([r for ae in attributed_events for r in ae.attributed_recommendations],
                      key=lambda r: (r.user_id, r.date))
        pos_len = len(pos_)
        neg_ = []

        while all_ and pos_:
            all_next = (all_[0].user_id, all_[0].date)
            pos_next = (pos_[0].user_id, pos_[0].date)

            if all_next == pos_next:
                all_.pop(0)
                pos_.pop(0)
                continue

            if all_next < pos_next:
                neg_.append(all_.pop(0))
            else:
                pos_.pop(0)

        if all_:
            neg_.extend(all_)

        shuffle(neg_)
        return neg_[0:pos_len]

    @classmethod
    def calc_attributed_events(cls, session: List[Event], user_recommendations: List[Recommendation],
                               attribution_type: AttributionType) \
            -> Generator[AttributedEvent, None, None]:
        if attribution_type == AttributionType.EMPIRICAL:
            return cls.calc_empirical_attributions(session, user_recommendations)

        if attribution_type == AttributionType.REFERENCE:
            return cls.calc_reference_attributions(session, user_recommendations)

        if attribution_type == AttributionType.TIME_WINDOW:
            return cls.calc_time_window_attributions(session, user_recommendations)

        raise ValueError(f"Unknown attribution type given: {str(attribution_type)}")

    @classmethod
    def calc_attributed_recommendations(cls, attributed_events: List[AttributedEvent],
                                        attribution_type: AttributionType) -> List[AttributedRecommendation]:
        # This code is ugly and has to be improved
        reco_reco_map = dict()
        reco_events_map = dict()
        for attributed_event in attributed_events:
            for recommendation in attributed_event.attributed_recommendations:
                reco_str = str(recommendation.as_json_dict())
                if reco_str not in reco_reco_map:
                    reco_reco_map[reco_str] = recommendation
                    reco_events_map[reco_str] = [attributed_event.event]
                else:
                    reco_events_map[reco_str].append(attributed_event.event)

        return [
            AttributedRecommendation(reco, attribution_type, reco_events_map[reco_str])
            for reco_str, reco in reco_reco_map.items()
        ]

    @classmethod
    def add_attributed_event_metrics(cls, metrics_dao_: MobiMetricsDao, client: str, attributed_event: AttributedEvent,
                                     product_id_brand_map: Dict[str, str]):
        last_reco = sorted(attributed_event.attributed_recommendations, key=lambda r: r.date)[-1]
        tags = {
            "event_type": attributed_event.event.event_type.name.lower(),
            "product_id": attributed_event.event.product_id,
            "attribution_type": attributed_event.attribution_type.name.lower(),
            "recommendation_type": last_reco.type.name.lower(),
            "recommendation_source": last_reco.source.name.lower(),
            "has_matched_display": "yes" if attributed_event.has_matched_display else "no",
            "from_zone": "no",
            "from_campaign": "no"
        }

        if attributed_event.event.product_id in product_id_brand_map:
            tags["brand"] = product_id_brand_map[attributed_event.event.product_id]

        if last_reco.zone_id is not None:
            tags["from_zone"] = "yes"
            tags["zone_id"] = last_reco.zone_id
        if last_reco.campaign_id is not None:
            tags["from_campaign"] = "yes"
            tags["campaign_id"] = last_reco.campaign_id

        for base in (60, 60 * 60, 24 * 60 * 60):
            metrics_dao_.inc_metrics_point(client, "attributed_events", tags, base,
                                           int(attributed_event.event.date.timestamp()))

        del tags["product_id"]

        if "brand" in tags:
            for base in (60, 60 * 60, 24 * 60 * 60):
                metrics_dao_.inc_metrics_point(client, "attributed_events__brand_total", tags, base,
                                               int(attributed_event.event.date.timestamp()))

            del tags["brand"]

        for base in (60, 60 * 60, 24 * 60 * 60):
            metrics_dao_.inc_metrics_point(client, "attributed_events__total", tags, base,
                                           int(attributed_event.event.date.timestamp()))

    @classmethod
    def add_unmatched_recommendation_metrics(cls, metrics_dao_: MobiMetricsDao, client: str,
                                             recommendation: Recommendation):
        pass

    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        def last_till_date_name(attribution_type_: AttributionType) -> str:
            return f"last_till_date_{attribution_type_.name.lower()}"

        metrics_dao_ = metrics_dao()
        recolog = recolog_dao()

        product_id_brand_map = dict()

        for product in catalog_dao().read_all_products(client, include_inactive=True, include_not_in_stock=True):
            product_id_brand_map[product.product_id] = product.brand

        for attribution_type in self.SUPPORTED_ATTRIBUTION_TYPES:
            from_date = client_context.get(last_till_date_name(attribution_type))

            if from_date is None:
                latest_attributed_event = recolog.get_latest_attributed_event(client, attribution_type)
                if latest_attributed_event is not None:
                    from_date = latest_attributed_event.event.date

            now = datetime.utcnow()

            if from_date is None:
                from_date = now - self.OLDEST_FROM_DATE_DELTA

            if from_date >= now - self.MIN_TILL_DATE_DELAY:
                return

            till_date = min(from_date + self.PROCESSING_WINDOW, now - self.MIN_TILL_DATE_DELAY)

            if till_date - from_date < self.MIN_PROCESSING_WINDOW:
                return

            attributed_events = []
            unmatched_recommendations = []

            for session, recommendations in self.read_sessions_and_recommendations(
                    client, from_date=from_date - timedelta(minutes=15), till_date=till_date + timedelta(minutes=15)):
                user_attributed_events = []

                for attributed_event in self.calc_attributed_events(session, recommendations, attribution_type):
                    user_attributed_events.append(attributed_event)

                attributed_events.extend(user_attributed_events)

                for unmatched_recommendation in self.calc_unmatched_recommendations(recommendations,
                                                                                    user_attributed_events):
                    unmatched_recommendations.append(unmatched_recommendation)

            attributed_events = [a_e for a_e in attributed_events if from_date <= a_e.event.date < till_date]
            attributed_recommendations = self.calc_attributed_recommendations(attributed_events, attribution_type)
            attributed_recommendations = [a_r for a_r in attributed_recommendations
                                          if from_date <= a_r.recommendation.date < till_date]
            unmatched_recommendations = [u_r for u_r in unmatched_recommendations if from_date <= u_r.date < till_date]

            attributed_events.sort(key=lambda a_e: a_e.event.date)
            attributed_recommendations.sort(key=lambda a_r: a_r.recommendation.date)
            unmatched_recommendations.sort(key=lambda u_r: u_r.date)

            for attributed_event in attributed_events:
                recolog.log_attributed_event(client, attributed_event)
                self.add_attributed_event_metrics(metrics_dao_, client, attributed_event, product_id_brand_map)
                client_context[last_till_date_name(attribution_type)] = \
                    attributed_event.event.date + timedelta(seconds=1)

            client_context[last_till_date_name(attribution_type)] = till_date

            for attributed_recommendation in attributed_recommendations:
                recolog.log_attributed_recommendation(client, attributed_recommendation)

            for recommendation in unmatched_recommendations:
                recolog.log_unmatched_recommendation(client, attribution_type, recommendation)

            del attributed_events
            del attributed_recommendations
            del unmatched_recommendations
