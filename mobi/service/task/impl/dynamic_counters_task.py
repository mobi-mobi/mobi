from collections import Counter, defaultdict
from dataclasses import dataclass
from datetime import datetime, timedelta
from logging import DEBUG
from time import time
from typing import Any, Dict, Generator, Optional, Tuple

from mobi.config.system.api.event_api import get_event_api_config
from mobi.config.system.service.cache_builder import get_cache_builder_config
from mobi.core.models import Event, EventType
from mobi.core.logging import get_logger
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao, MobiDaoException
from mobi.dao.cache import cache_key
from mobi.service.task.interface import MobiDaemonTask


def cache_dao():
    return get_cache_dao(get_cache_builder_config())


def catalog_dao():
    return get_catalog_dao(get_event_api_config())


def event_dao():
    return get_event_dao(get_event_api_config())


logger = get_logger()
logger.setLevel(DEBUG)


@dataclass(frozen=True)
class DynamicCounterSpecs:
    TIME_WINDOW: timedelta
    SUFFIX: str
    REINITIALIZE_EVERY = timedelta(hours=3, minutes=8)


class DynamicCountersTask(MobiDaemonTask):
    SAFE_INTERVAL = timedelta(seconds=20)
    REFRESH_PRODUCT_IDS_TO_BRANDS_MAP_EVERY = timedelta(hours=6, minutes=23)

    COUNTER_SUFFIXES = {
        EventType.PRODUCT_VIEW: "views",
        EventType.PRODUCT_TO_WISHLIST: "to_wishlist",
        EventType.PRODUCT_TO_BASKET: "to_basket",
        EventType.PRODUCT_SALE: "sales",
    }

    COUNTER_SPECS = [
        DynamicCounterSpecs(TIME_WINDOW=timedelta(minutes=30), SUFFIX="30m"),
        DynamicCounterSpecs(TIME_WINDOW=timedelta(hours=1), SUFFIX="1h"),
        DynamicCounterSpecs(TIME_WINDOW=timedelta(hours=24), SUFFIX="24h"),
    ]

    @classmethod
    def initialization_run(cls,  counter_context: Dict[str, Any]) -> bool:
        return counter_context.get("initialization_run", True)

    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        for counter_specs in self.COUNTER_SPECS:
            counter_context = client_context.get(counter_specs.SUFFIX)
            if counter_context is None:
                counter_context = dict()
            client_context[counter_specs.SUFFIX] = counter_context
            self.process_counter(client, client_context, counter_specs, counter_context)

    def process_counter(self, client: str, client_context: Dict[str, Any], counter_specs: DynamicCounterSpecs,
                        counter_context: Dict[str, Any]):
        datetime_now = datetime.utcnow() - self.SAFE_INTERVAL

        last_new_events_till: Optional[datetime] = counter_context.get("last_new_events_till")
        last_initialization_run_date = counter_context.get("last_initialization_run_date")
        should_reinitialize = last_initialization_run_date is not None \
            and datetime_now - last_initialization_run_date > counter_specs.REINITIALIZE_EVERY

        if should_reinitialize:
            counter_context["initialization_run"] = True

        if last_new_events_till is None or should_reinitialize:
            new_events_from = datetime_now - counter_specs.TIME_WINDOW
            new_events_till = datetime_now

            fatigue_events_from = None
            fatigue_events_till = None
        else:
            new_events_from = last_new_events_till
            new_events_till = datetime_now

            fatigue_events_from = last_new_events_till - counter_specs.TIME_WINDOW
            fatigue_events_till = datetime_now - counter_specs.TIME_WINDOW

        self.before_processing(client, counter_context)

        new_events = event_dao().read_events(client, new_events_from, new_events_till)
        logger.debug(f"Dynamic Counters ({counter_specs.SUFFIX}): {client}: Adding events from {new_events_from} "
                     f"until {new_events_till}")
        self.process_new_events(client, new_events, counter_context)

        if fatigue_events_from is not None:
            fatigue_events = event_dao().read_events(client, fatigue_events_from, fatigue_events_till)
            logger.debug(f"Dynamic Counters ({counter_specs.SUFFIX}): {client}: Deleting events "
                         f"from {fatigue_events_from} until {fatigue_events_till}")
            self.process_fatigue_events(client, fatigue_events, counter_context)

        self.after_processing(client, counter_context, counter_specs, client_context)
        logger.debug(f"Dynamic Counters ({counter_specs.SUFFIX}): {client}: Replaced: {counter_context['replaced']}, "
                     f"updated: {counter_context['updated']}. It took {time()-counter_context['start_time']}s")

        counter_context["last_new_events_till"] = datetime_now

        if self.initialization_run(counter_context):
            counter_context["initialization_run"] = False
            counter_context["last_initialization_run_date"] = datetime.utcnow()

    def _product_ids_to_brands_map(self, client: str, client_context: Dict[str, Any]) -> Dict[str, str]:
        last_refresh = client_context.get("product_ids_to_brands_last_refresh_date")
        should_refresh = last_refresh is None \
            or datetime.utcnow() - last_refresh > self.REFRESH_PRODUCT_IDS_TO_BRANDS_MAP_EVERY \
            or client_context.get("product_ids_to_brands_map") is None

        if should_refresh:
            client_context["product_ids_to_brands_map"] = {
                product.product_id: product.brand
                for product in catalog_dao().read_all_products(client, include_inactive=True, include_not_in_stock=True)
            }
            client_context["product_ids_to_brands_last_refresh_date"] = datetime.utcnow()

        return client_context["product_ids_to_brands_map"]

    def brand(self, client: str, client_context: Dict[str, Any], product_id: str) -> Optional[str]:
        product_ids_to_brands_map = self._product_ids_to_brands_map(client, client_context)
        if product_id not in product_ids_to_brands_map:
            product = None
            try:
                product = catalog_dao().read_product(client, product_id)
            except MobiDaoException:
                pass
            if product:
                product_ids_to_brands_map[product_id] = product.brand
        return product_ids_to_brands_map.get(product_id)

    @classmethod
    def dump_cache_data(cls, cache_data: Dict[str, int], cache_ttl: int, replace: bool) -> Tuple[int, int]:
        updated = 0

        if not replace:
            cache_values = cache_dao().get_many_int(list(cache_data.keys()), retries=3)
            for key, cache_value in cache_values.items():
                updated += 1
                cache_data[key] += cache_value

        cache_dao().set_many_int(cache_data, expire=cache_ttl, noreply=True, retries=3)
        return updated, (len(cache_data) - updated)

    def dump_delta(self, client: str, client_context: Dict[str, Any], delta: Dict[str, Dict[EventType, int]],
                   is_init_run: bool, cache_ttl: int, time_suffix: str) -> Tuple[int, int]:
        updated = 0
        replaced = 0

        total_events: Dict[EventType, int] = Counter()

        cache_data: Dict[str, int] = dict()
        brands_delta: Dict[str, Dict[EventType, int]] = defaultdict(Counter)
        for product_id in delta:
            for event_type, value in delta[product_id].items():
                total_events[event_type] += value
                key = cache_key(f"product_{self.COUNTER_SUFFIXES[event_type]}_{time_suffix}",
                                client=client, product_id=product_id)
                cache_data[key] = value
                brand = self.brand(client, client_context, product_id)
                if brand is not None:
                    total_events[event_type] += value
                    brands_delta[brand][event_type] += value
            if len(cache_data) > 50:
                u_, r_ = self.dump_cache_data(cache_data, cache_ttl, replace=is_init_run)
                updated += u_
                replaced += r_
                cache_data = dict()

        for brand in brands_delta:
            for event_type, value in brands_delta[brand].items():
                key = cache_key(f"brand_{self.COUNTER_SUFFIXES[event_type]}_{time_suffix}", client=client,
                                brand=brand)
                cache_data[key] = value
            if len(cache_data) > 50:
                u_, r_ = self.dump_cache_data(cache_data, cache_ttl, replace=is_init_run)
                updated += u_
                replaced += r_
                cache_data = dict()

        for event_type, value in total_events.items():
            key = cache_key(f"product_{self.COUNTER_SUFFIXES[event_type]}_{time_suffix}", client=client)
            cache_data[key] = value
            key = cache_key(f"brand_{self.COUNTER_SUFFIXES[event_type]}_{time_suffix}", client=client)
            cache_data[key] = value

        if cache_data:
            u_, r_ = self.dump_cache_data(cache_data, cache_ttl, replace=is_init_run)
            updated += u_
            replaced += r_

        return updated, replaced

    @classmethod
    def before_processing(cls, _: str, counter_context: Dict[str, Any]):
        counter_context["delta"] = defaultdict(Counter)
        counter_context["start_time"] = time()

    def after_processing(self, client: str, counter_context: Dict[str, Any], counter_specs: DynamicCounterSpecs,
                         client_context: Dict[str, Any]):
        updated, replaced = self.dump_delta(client, client_context, counter_context["delta"],
                                            self.initialization_run(counter_context),
                                            cache_ttl=int(counter_specs.TIME_WINDOW.total_seconds()),
                                            time_suffix=counter_specs.SUFFIX)
        counter_context["updated"] = updated
        counter_context["replaced"] = replaced
        del counter_context["delta"]

    @classmethod
    def process_new_events(cls, client: str, events: Generator[Event, None, None], counter_context: Dict[str, Any]):
        for event in events:
            if event.product_id is not None:
                value = event.product_quantity if event.product_quantity is not None else 1
                counter_context["delta"][event.product_id][event.event_type] += value
            if event.basket_items is not None:
                for basket_item in event.basket_items:
                    counter_context["delta"][basket_item.product_id][event.event_type] += basket_item.product_quantity

    @classmethod
    def process_fatigue_events(cls, client: str, events: Generator[Event, None, None], counter_context: Dict[str, Any]):
        for event in events:
            if event.product_id is not None:
                value = event.product_quantity if event.product_quantity is not None else 1
                counter_context["delta"][event.product_id][event.event_type] -= value
            if event.basket_items is not None:
                for basket_item in event.basket_items:
                    counter_context["delta"][basket_item.product_id][event.event_type] -= basket_item.product_quantity
