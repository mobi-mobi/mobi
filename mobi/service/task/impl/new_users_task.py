from collections import Counter
from datetime import datetime, timedelta
from typing import Any, Dict

from mobi.config.system.service.metrics_builder import get_metrics_builder_config
from mobi.dao import get_event_dao, get_metrics_dao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.service.task.interface import MobiDaemonTask


def events_dao() -> MobiEventDao:
    return get_event_dao(get_metrics_builder_config())


def metrics_dao() -> MobiMetricsDao:
    return get_metrics_dao(get_metrics_builder_config())


class NewUsersTask(MobiDaemonTask):
    METRIC_NAME = "new_users"
    FREQUENCY = {
        60: 3,
        60 * 60: 10 * 60,
        24 * 60 * 60: 30 * 60
    }
    RECALC_LAST_POINTS = {
        60: 30,
        60 * 60: 1,
        24 * 60 * 60: 1
    }
    NEW_USER_AFTER = timedelta(days=35)
    ON_INIT_RECALC_FROM = {
        60: timedelta(days=1),
        60 * 60: timedelta(days=7),
        24 * 60 * 60: timedelta(days=30)
    }

    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        if "wait_until" not in client_context:
            client_context["wait_until"] = dict()

        for base in 60, 60 * 60, 24 * 60 * 60:
            latest_ts_key = f"latest_ts.{base}"

            wait_until = client_context["wait_until"].get(base, datetime.utcnow() - timedelta(hours=1))

            if datetime.utcnow() < wait_until:
                continue

            latest_ts = client_context.get(latest_ts_key)
            if latest_ts is None:
                latest_ts = metrics_dao().get_latest_point_timestamp(client, self.METRIC_NAME, None, base)

            if latest_ts is not None:
                latest_ts -= self.RECALC_LAST_POINTS[base] * base
            else:
                latest_ts = (int((datetime.utcnow() - self.ON_INIT_RECALC_FROM[base]).timestamp()) // base) * base

            current_ts = latest_ts

            old_users = events_dao().read_unique_user_ids(
                client,
                from_date=datetime.utcfromtimestamp(current_ts) - self.NEW_USER_AFTER,
                till_date=datetime.utcfromtimestamp(current_ts),
                sorted=True
            )

            current_users = events_dao().read_unique_user_ids(
                client,
                from_date=datetime.utcfromtimestamp(current_ts),
                till_date=datetime.utcfromtimestamp(current_ts) + timedelta(days=2),
                sorted=True
            )

            new_users = Counter()
            next_old_user = None
            old_user_max_date = None
            next_current_user = None
            current_user_min_date = None

            while True:
                if next_old_user is None:
                    try:
                        next_old_user, _, old_user_max_date = next(old_users)
                    except StopIteration:
                        next_old_user = None
                        old_user_max_date = None

                if next_current_user is None:
                    try:
                        next_current_user, current_user_min_date, _ = next(current_users)
                    except StopIteration:
                        next_current_user = None
                        current_user_min_date = None

                if next_current_user is None:
                    break

                if next_old_user is not None and next_old_user < next_current_user:
                    next_old_user = None
                    continue

                if next_old_user is None:
                    is_new = True
                    next_current_user = None
                else:
                    if next_current_user < next_old_user:
                        is_new = True
                        next_current_user = None
                    else:  # equal
                        if current_user_min_date - old_user_max_date > self.NEW_USER_AFTER:
                            is_new = True
                        else:
                            is_new = False
                        next_current_user = None
                        next_old_user = None

                if is_new:
                    new_users[(int(current_user_min_date.timestamp()) // base) * base] += 1

            del old_users
            del current_users

            timestamps = list(new_users.keys())
            timestamps.sort()

            for ts in timestamps:
                metrics_dao().set_metrics_point(client, self.METRIC_NAME, {}, base, ts, float(new_users[ts]))
                client_context[latest_ts_key] = ts

            del new_users

            client_context["wait_until"][base] = datetime.utcnow() + timedelta(seconds=self.FREQUENCY[base])
