import numpy as np
import statsmodels.stats.api as sms

from collections import Counter, defaultdict
from datetime import datetime, timedelta
from statistics import mean
from typing import Any, Dict, List, Union

from mobi.config.system.service.metrics_builder import get_metrics_builder_config
from mobi.core.models import EventType
from mobi.core.population import Population
from mobi.dao import get_event_dao, get_metrics_dao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.service.task.interface import MobiDaemonTask


def event_dao() -> MobiEventDao:
    return get_event_dao(get_metrics_builder_config())


def metrics_dao() -> MobiMetricsDao:
    return get_metrics_dao(get_metrics_builder_config())


class StatsCalculation(MobiDaemonTask):
    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        from_date = datetime.utcnow() - timedelta(days=28)
        max_allowed_window_min = 30
        target_and_test_pop = "target_and_test"

        data = dict()

        for session in event_dao().read_user_sessions(client, timedelta(minutes=max_allowed_window_min), from_date):
            day_ts = 24 * 60 * 60 * (int(session[0].date.timestamp()) // (24 * 60 * 60))

            if day_ts not in data:
                data[day_ts] = dict()

            pop = session[0].user_population.name.lower()

            if pop not in data[day_ts]:
                data[day_ts][pop] = []

            data[day_ts][pop].append(session)

            # Adding fake target_and_test population

            if session[0].user_population in (Population.TARGET, Population.TEST):
                if target_and_test_pop not in data[day_ts]:
                    data[day_ts][target_and_test_pop] = []

                data[day_ts][target_and_test_pop].append(session)

        def create_abtest_metric(
                metric_data: Dict[str, List[Union[int, float]]],
                metric_name: str,
                day_ts: int,
                extra_tags: Dict[str, str] = None
        ):
            for popA in metric_data:
                for popB in metric_data:
                    if popA == popB:
                        continue

                    if len(metric_data[popA]) < 10 or len(metric_data[popB]) < 10:
                        continue

                    interval = sms.CompareMeans(sms.DescrStatsW(metric_data[popA]),
                                                sms.DescrStatsW(metric_data[popB]))
                    interval = interval.tconfint_diff(usevar='unequal', alpha=0.05, alternative='two-sided')

                    lower = .0 if np.isnan(interval[0]) else float(interval[0])
                    upper = .0 if np.isnan(interval[1]) else float(interval[1])

                    tags = {
                        "popA": popA,
                        "popB": popB,
                        "metric": metric_name
                    }

                    funcs_tags = {
                        "pop": popA,
                        "metric": metric_name
                    }

                    if extra_tags:
                        tags = {
                            **tags,
                            **extra_tags
                        }

                        funcs_tags = {
                            **funcs_tags,
                            **extra_tags
                        }

                    metrics_dao().set_metrics_point(
                        client,
                        "abtest",
                        {**tags, "bound": "lower"},
                        24 * 60 * 60,
                        day_ts,
                        lower
                    )

                    metrics_dao().set_metrics_point(
                        client,
                        "abtest",
                        {**tags, "bound": "upper"},
                        24 * 60 * 60,
                        day_ts,
                        upper
                    )

                    pop_a_mean = float(mean(metric_data[popA]))
                    pop_b_mean = float(mean(metric_data[popB]))

                    pop_a_len = float(len(metric_data[popA]))

                    metrics_dao().set_metrics_point(
                        client,
                        "abtest",
                        {**tags, "bound": "abs_diff"},
                        24 * 60 * 60,
                        day_ts,
                        pop_a_mean - pop_b_mean
                    )

                    metrics_dao().set_metrics_point(
                        client,
                        "abtest",
                        {**tags, "bound": "rel_diff"},
                        24 * 60 * 60,
                        day_ts,
                        0.0 if pop_b_mean < (1 / (10**9)) else ((pop_a_mean - pop_b_mean) / pop_b_mean)
                    )

                    # Setting mean values

                    metrics_dao().set_metrics_point(
                        client,
                        "abtest",
                        {**funcs_tags, "func": "mean"},
                        24 * 60 * 60,
                        day_ts,
                        pop_a_mean
                    )

                    metrics_dao().set_metrics_point(
                        client,
                        "abtest",
                        {**funcs_tags, "func": "len"},
                        24 * 60 * 60,
                        day_ts,
                        pop_a_len
                    )

        total_sessions_len = defaultdict(list)
        total_views_num = defaultdict(list)
        total_purchases_num = defaultdict(list)
        total_to_wishlist_num = defaultdict(list)
        total_to_basket_num = defaultdict(list)
        total_sessions_per_user = defaultdict(list)

        total_sessions_len_7d = defaultdict(list)
        total_views_num_7d = defaultdict(list)
        total_purchases_num_7d = defaultdict(list)
        total_to_wishlist_num_7d = defaultdict(list)
        total_to_basket_num_7d = defaultdict(list)
        total_sessions_per_user_7d = defaultdict(list)

        total_sessions_len_14d = defaultdict(list)
        total_views_num_14d = defaultdict(list)
        total_purchases_num_14d = defaultdict(list)
        total_to_wishlist_num_14d = defaultdict(list)
        total_to_basket_num_14d = defaultdict(list)
        total_sessions_per_user_14d = defaultdict(list)

        datetime_now = datetime.utcnow()

        for day_ts in data:
            daily_sessions_len = defaultdict(list)
            daily_views_num = defaultdict(list)
            daily_purchases_num = defaultdict(list)
            daily_to_wishlist_num = defaultdict(list)
            daily_to_basket_num = defaultdict(list)
            daily_sessions_per_user = defaultdict(list)

            for pop in data[day_ts]:
                user_sessions_num = Counter()

                for session in data[day_ts][pop]:
                    views = len([event for event in session if event.event_type == EventType.PRODUCT_VIEW])
                    purchases = 0
                    for event in session:
                        if event.event_type == EventType.PRODUCT_SALE and event.basket_items is not None:
                            for bi in event.basket_items:
                                purchases += bi.product_quantity
                    to_wishlist = len([event for event in session if event.event_type == EventType.PRODUCT_TO_WISHLIST])
                    to_basket = len([event for event in session if event.event_type == EventType.PRODUCT_TO_BASKET])
                    user_sessions_num[session[0].user_id] += 1

                    total_sessions_len[pop].append(len(session))
                    total_views_num[pop].append(views)
                    total_purchases_num[pop].append(purchases)
                    total_to_wishlist_num[pop].append(to_wishlist)
                    total_to_basket_num[pop].append(to_basket)

                    daily_sessions_len[pop].append(len(session))
                    daily_views_num[pop].append(views)
                    daily_purchases_num[pop].append(purchases)
                    daily_to_wishlist_num[pop].append(to_wishlist)
                    daily_to_basket_num[pop].append(to_basket)

                    if session[0].date > datetime_now - timedelta(days=7):
                        total_sessions_len_7d[pop].append(len(session))
                        total_views_num_7d[pop].append(views)
                        total_purchases_num_7d[pop].append(purchases)
                        total_to_wishlist_num_7d[pop].append(to_wishlist)
                        total_to_basket_num_7d[pop].append(to_basket)

                    if session[0].date > datetime_now - timedelta(days=14):
                        total_sessions_len_14d[pop].append(len(session))
                        total_views_num_14d[pop].append(views)
                        total_purchases_num_14d[pop].append(purchases)
                        total_to_wishlist_num_14d[pop].append(to_wishlist)
                        total_to_basket_num_14d[pop].append(to_basket)

                user_sessions_num_values = list(user_sessions_num.values())
                daily_sessions_per_user[pop] = user_sessions_num_values
                total_sessions_per_user[pop].extend(user_sessions_num_values)
                total_sessions_per_user_7d[pop].extend(user_sessions_num_values)
                total_sessions_per_user_14d[pop].extend(user_sessions_num_values)

            create_abtest_metric(daily_sessions_len, "session_length", day_ts, {"period": "daily"})
            create_abtest_metric(daily_sessions_per_user, "sessions_per_user", day_ts, {"period": "daily"})
            create_abtest_metric(daily_views_num, "views_num", day_ts, {"period": "daily"})
            create_abtest_metric(daily_purchases_num, "purchases_num", day_ts, {"period": "daily"})
            create_abtest_metric(daily_to_wishlist_num, "to_wishlist_num", day_ts, {"period": "daily"})
            create_abtest_metric(daily_to_basket_num, "to_basket_num", day_ts, {"period": "daily"})

        now_ts = 24 * 60 * 60 * (int(datetime_now.timestamp()) // (24 * 60 * 60))

        create_abtest_metric(total_sessions_len_7d, "session_length", now_ts, {"period": "7d"})
        create_abtest_metric(total_sessions_per_user_7d, "sessions_per_user", now_ts, {"period": "7d"})
        create_abtest_metric(total_views_num_7d, "views_num", now_ts, {"period": "7d"})
        create_abtest_metric(total_purchases_num_7d, "purchases_num", now_ts, {"period": "7d"})
        create_abtest_metric(total_to_wishlist_num_7d, "to_wishlist_num", now_ts, {"period": "7d"})
        create_abtest_metric(total_to_basket_num_7d, "to_basket_num", now_ts, {"period": "7d"})

        create_abtest_metric(total_sessions_len_14d, "session_length", now_ts, {"period": "14d"})
        create_abtest_metric(total_sessions_per_user_14d, "sessions_per_user", now_ts, {"period": "14d"})
        create_abtest_metric(total_views_num_14d, "views_num", now_ts, {"period": "14d"})
        create_abtest_metric(total_purchases_num_14d, "purchases_num", now_ts, {"period": "14d"})
        create_abtest_metric(total_to_wishlist_num_14d, "to_wishlist_num", now_ts, {"period": "14d"})
        create_abtest_metric(total_to_basket_num_14d, "to_basket_num", now_ts, {"period": "14d"})

        create_abtest_metric(total_sessions_len, "session_length", now_ts, {"period": "28d"})
        create_abtest_metric(total_sessions_per_user, "sessions_per_user", now_ts, {"period": "28d"})
        create_abtest_metric(total_views_num, "views_num", now_ts, {"period": "28d"})
        create_abtest_metric(total_purchases_num, "purchases_num", now_ts, {"period": "28d"})
        create_abtest_metric(total_to_wishlist_num, "to_wishlist_num", now_ts, {"period": "28d"})
        create_abtest_metric(total_to_basket_num, "to_basket_num", now_ts, {"period": "28d"})
