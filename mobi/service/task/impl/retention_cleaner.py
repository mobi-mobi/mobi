from datetime import datetime, timedelta
from logging import DEBUG
from typing import Any, Dict, Optional

from mobi.config.client import ClientBaseConfig, get_client_config
from mobi.config.system.service.model_builder import get_model_builder_config
from mobi.core.logging import get_logger
from mobi.dao import get_catalog_dao, get_event_dao, get_metrics_dao, get_recolog_dao
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.recolog import MobiRecologDao
from mobi.service.task.interface import MobiDaemonTask


def catalog_dao() -> MobiCatalogDao:
    return get_catalog_dao(get_model_builder_config())


def event_dao() -> MobiEventDao:
    return get_event_dao(get_model_builder_config())


def metrics_dao() -> MobiMetricsDao:
    return get_metrics_dao(get_model_builder_config())


def recolog_dao() -> MobiRecologDao:
    return get_recolog_dao(get_model_builder_config())


logger = get_logger()
logger.setLevel(DEBUG)


class RetentionCleanerTask(MobiDaemonTask):
    @staticmethod
    def delete_old_events(client: str, client_config: Optional[ClientBaseConfig] = None):
        logger.info(f"Deleting old events for client \"{client}\"")
        client_config = client_config or get_client_config(client)
        events_max_days = client_config.EVENTS_RETENTION_DAYS
        deleted = event_dao().delete_fatigue_events(client, datetime.utcnow() - timedelta(days=events_max_days))
        if deleted is None:
            deleted = "<UNKNOWN>"
        logger.info(f"Done. Deleted {deleted} events")

    @staticmethod
    def delete_old_recologs(client: str, client_config: Optional[ClientBaseConfig] = None):
        logger.info(f"Deleting old recologs for client \"{client}\"")
        client_config = client_config or get_client_config(client)
        recologs_max_days = client_config.RECOLOGS_RETENTION_DAYS
        deleted = recolog_dao().delete_fatigue_recologs(client, datetime.utcnow() - timedelta(days=recologs_max_days))
        if deleted is None:
            deleted = "<UNKNOWN>"
        logger.info(f"Done. Deleted {deleted} recologs")

    @staticmethod
    def delete_old_metrics(client: str, client_config: Optional[ClientBaseConfig] = None):
        logger.info(f"Deleting old metrics data for client \"{client}\"")
        client_config = client_config or get_client_config(client)
        now = datetime.utcnow()
        minute_limit = int((now - timedelta(days=client_config.METRICS_MINUTES_RETENTION_DAYS)).timestamp())
        hour_limit = int((now - timedelta(days=client_config.METRICS_HOURS_RETENTION_DAYS)).timestamp())
        day_limit = int((now - timedelta(days=client_config.METRICS_DAILY_RETENTION_DAYS)).timestamp())
        total_deleted = 0
        deleted = metrics_dao().delete_fatigue_data(client, 60, minute_limit)
        if deleted is not None:
            total_deleted += deleted
        deleted = metrics_dao().delete_fatigue_data(client, 60 * 60, hour_limit)
        if deleted is not None:
            total_deleted += deleted
        deleted = metrics_dao().delete_fatigue_data(client, 24 * 60 * 60, day_limit)
        if deleted is not None:
            total_deleted += deleted
        logger.info(f"Done. Deleted {total_deleted} metric records")

    @staticmethod
    def delete_old_products(client: str):
        # TODO: Improve this. We should read min versions directly from database without reading all events
        logger.info(f"Deleting old products for client \"{client}\"")
        product_version_to_remain: Dict[str, int] = dict()

        for product in catalog_dao().read_all_products(client, include_inactive=True, include_not_in_stock=True):
            if product.product_id not in product_version_to_remain:
                product_version_to_remain[product.product_id] = product.version

        for event in event_dao().read_events(client):
            if event.product_id not in product_version_to_remain:
                product_version_to_remain[event.product_id] = event.product_version
            else:
                product_version_to_remain[event.product_id] \
                    = min(event.product_version, product_version_to_remain[event.product_id])

        total_deleted = 0
        for product_id, product_version in product_version_to_remain.items():
            deleted = catalog_dao().delete_fatigue_versions(client, product_id, product_version)
            if deleted is not None:
                total_deleted += deleted
        logger.info(f"Done. Deleted {total_deleted} old unused product versions")

    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        self.delete_old_events(client)
        self.delete_old_recologs(client)
        self.delete_old_metrics(client)
        self.delete_old_products(client)
