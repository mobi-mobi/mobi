import smtplib
from urllib.parse import urlencode

from collections import Counter, defaultdict
from datetime import datetime, timedelta
from hashlib import sha1
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Any, Dict, List, Tuple

from mobi.config.system.service import get_email_sender_config
from mobi.core.logging import get_logger
from mobi.core.models import AttributionType, EventPlatform, EventType, RecommendationType
from mobi.dao import get_auth_dao, get_catalog_dao, get_event_dao, get_metrics_dao, get_recolog_dao, \
    get_user_settings_dao
from mobi.dao.auth import MobiAuthDao
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.recolog import MobiRecologDao
from mobi.dao.user_settings import MobiUserSettingsDao, ReportSubscription
from mobi.service.task.interface import MobiDaemonTask
from mobi.utils.email import *


def auth_dao() -> MobiAuthDao:
    return get_auth_dao(get_email_sender_config())


def catalog_dao() -> MobiCatalogDao:
    return get_catalog_dao(get_email_sender_config())


def event_dao() -> MobiEventDao:
    return get_event_dao(get_email_sender_config())


def metrics_dao() -> MobiMetricsDao:
    return get_metrics_dao(get_email_sender_config())


def recolog_dao() -> MobiRecologDao:
    return get_recolog_dao(get_email_sender_config())


def user_settings_dao() -> MobiUserSettingsDao:
    return get_user_settings_dao(get_email_sender_config())


def nice_float(f: float, preciseness: int = 2) -> str:
    s = f"%.{preciseness}f"
    return s % f


def nice_int(i: int) -> str:
    negative = i < 0
    i = abs(i)
    if i < 1000:
        result = str(i)
    elif i < 1000000:
        result = nice_float(i / 1000, preciseness=1) + "K"
    else:
        result = nice_float(i / 1000000, preciseness=1) + "M"
    if negative:
        result = "-" + result
    return result


def build_report_html(client: str, users: List[Tuple[str, str]], report_subscription: ReportSubscription,
                      from_date: datetime, till_date: datetime, product_id_to_brand_map: Dict[str, str]) -> List[str]:
    get_logger().info(f"Creating {report_subscription.value} report for {client}...")
    # GATHERING DATA
    user_ids = set()
    total_events = Counter()
    per_product_events = defaultdict(Counter)
    per_platform_events = defaultdict(Counter)
    per_brand_events = defaultdict(Counter)

    base = 60 * 60
    new_users_from_ts = (int(from_date.timestamp()) // base) * base
    new_users_till_ts = (int(till_date.timestamp()) // base) * base
    new_users = 0
    for _, val in metrics_dao().get_metrics(client, "new_users", None, base, new_users_from_ts, new_users_till_ts):
        new_users += int(val)

    sessions_num = 0

    for session in event_dao().read_user_sessions(client, from_date=from_date, till_date=till_date):
        sessions_num += 1
        if session:
            user_ids.add(session[0].user_id)

        for event in session:
            if event.event_type in (EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST, EventType.PRODUCT_TO_BASKET):
                total_events[event.event_type] += (event.product_quantity or 1)
                per_product_events[event.event_type][event.product_id] += (event.product_quantity or 1)
                per_platform_events[event.event_type][event.event_platform] += (event.product_quantity or 1)
                if event.product_id in product_id_to_brand_map:
                    per_brand_events[event.event_type][product_id_to_brand_map[event.product_id]] \
                        += (event.product_quantity or 1)
            elif event.event_type == EventType.PRODUCT_SALE:
                for basket_item in event.basket_items:
                    total_events[event.event_type] += basket_item.product_quantity
                    per_product_events[event.event_type][basket_item.product_id] += basket_item.product_quantity
                    per_platform_events[event.event_type][event.event_platform] += basket_item.product_quantity
                    if event.product_id in product_id_to_brand_map:
                        per_brand_events[event.event_type][product_id_to_brand_map[event.product_id]] \
                            += basket_item.product_quantity

    if not sessions_num:
        views_per_session = 0.0
        purchases_per_session = 0.0
    else:
        views_per_session = (total_events[EventType.PRODUCT_VIEW] / sessions_num)
        purchases_per_session = (total_events[EventType.PRODUCT_SALE] / sessions_num)

    per_type_recommendations = Counter()

    for recommendation in recolog_dao().read_recommendations(client, from_date, till_date):
        per_type_recommendations[recommendation.type] += 1

    total_attributed_events = Counter()

    for attributed_event in recolog_dao().read_attributed_events(client, AttributionType.EMPIRICAL, from_date,
                                                                 till_date):
        if attributed_event.event.event_type in (EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST,
                                                 EventType.PRODUCT_TO_BASKET):
            total_attributed_events[attributed_event.event.event_type] += 1
        elif attributed_event.event.event_type == EventType.PRODUCT_SALE:
            for basket_item in attributed_event.event.basket_items:
                total_attributed_events[attributed_event.event.event_type] += basket_item.product_quantity

    # BUILDING HTML

    result = []

    for email, user_name in users:
        html = ""
        header_str = f"{client}: {report_subscription.value} report: {from_date.strftime('%Y-%m-%d')}"
        if report_subscription == ReportSubscription.WEEKLY:
            header_str += " -- " + (from_date + timedelta(days=6)).strftime('%Y-%m-%d')
        html += report_row_header(header_str)
        html += report_row_text(f"Hello, {user_name}! One more report for {client} is ready!")
        html += report_row_subtitle(f"What a {'week' if report_subscription == ReportSubscription.WEEKLY else 'day'}!")
        html += report_row_1_metric_value_and_text(nice_int(total_attributed_events[EventType.PRODUCT_VIEW]),
                                                   "- this is how many products were viewed by your visitors thanks "
                                                   "to Mobimobi recommender system")
        html += report_row_1_metric_value_and_text(nice_int(total_attributed_events[EventType.PRODUCT_TO_BASKET]),
                                                   "- and this is how many products were added to basket!")
        html += report_row_subtitle("Auditory Information")
        html += report_row_4_metric_values(["Active Users", "New Users", "Views Per Session", "Purchases Per Session"],
                                           [nice_int(len(user_ids)), nice_int(new_users),
                                            nice_float(views_per_session), nice_float(purchases_per_session)])

        html += report_row_subtitle("Events Statistics (% of Attributed)")
        event_statistics = [nice_int(total_events[et]) for et in [EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST,
                                                                  EventType.PRODUCT_TO_BASKET, EventType.PRODUCT_SALE]]
        attributed_event_statistics = [nice_int(total_attributed_events[et]) + " ("
                                       + nice_float(0.0 if total_events[et] == 0
                                                    else 100 * total_attributed_events[et] / total_events[et],
                                                    preciseness=1)
                                       + "%)" for et in
                                       [EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST,
                                        EventType.PRODUCT_TO_BASKET, EventType.PRODUCT_SALE]]
        html += report_row_4_metric_values_with_deltas(["Views", "To Wishlist", "To Basket", "Purchased"],
                                                       event_statistics, attributed_event_statistics)

        html += report_row_subtitle("Per Platform Statistics")
        html += report_row_title_and_4_cells_table("", ["Views", "To Wishlist", "To Basket", "Purchased"])

        for platform in (EventPlatform.IOS, EventPlatform.ANDROID, EventPlatform.WEB, EventPlatform.OTHER):

            values = [
                nice_int(per_platform_events[et][platform])
                for et in [EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST,
                           EventType.PRODUCT_TO_BASKET, EventType.PRODUCT_SALE]
            ]
            html += report_row_title_and_4_cells_table(platform.name, values)

        html += report_row_subtitle("Recommendations Generated")
        recommendation_types = [RecommendationType.SIMILAR_PRODUCTS, RecommendationType.PERSONAL_RECOMMENDATIONS,
                                RecommendationType.BASKET_RECOMMENDATIONS, "__SEARCH__TO_DELETE__"]
        html += report_row_4_metric_values(["Similar Products", "Personalized", "In Basket", "Search"],
                                           [nice_int(per_type_recommendations[rt]) for rt in recommendation_types])
        recommendation_types = [RecommendationType.MOST_POPULAR, RecommendationType.RECENTLY_VIEWED,
                                RecommendationType.RANK_PRODUCTS, "__OTHER__"]
        html += report_row_4_metric_values(["Most Popular", "Recently Viewed", "Ranking", "Other"],
                                           [nice_int(per_type_recommendations[rt]) for rt in recommendation_types])

        html += report_row_subtitle("Most Popular Brands")
        html += report_row_title_and_4_cells_table("", ["Views", "To Wishlist", "To Basket", "Purchased"])
        for brand, _ in per_brand_events[EventType.PRODUCT_VIEW].most_common(5):
            values = [nice_int(per_brand_events[et][brand])
                      for et in [EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST,
                                 EventType.PRODUCT_TO_BASKET, EventType.PRODUCT_SALE]]
            html += report_row_title_and_4_cells_table(brand, values)

        html += report_row_subtitle("Most Popular Products")
        html += report_row_title_and_4_cells_table("", ["Views", "To Wishlist", "To Basket", "Purchased"])
        for product_id, _ in per_product_events[EventType.PRODUCT_VIEW].most_common(5):
            values = [nice_int(per_product_events[et][product_id])
                      for et in [EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST, EventType.PRODUCT_TO_BASKET,
                                 EventType.PRODUCT_SALE]
                      ]
            try:
                product_title = catalog_dao().read_product(client, product_id).title
            except:
                continue
            html += report_row_title_and_4_cells_table(product_title, values)

        html += report_row_subtitle("Most Wished Products")
        html += report_row_title_and_4_cells_table("", ["Views", "To Wishlist", "To Basket", "Purchased"])
        for product_id, _ in per_product_events[EventType.PRODUCT_TO_WISHLIST].most_common(5):
            values = [nice_int(per_product_events[et][product_id])
                      for et in [EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST, EventType.PRODUCT_TO_BASKET,
                                 EventType.PRODUCT_SALE]
                      ]
            try:
                product_title = catalog_dao().read_product(client, product_id).title
            except:
                continue
            html += report_row_title_and_4_cells_table(product_title, values)

        unsubscribe_link_checksum_str = "mobimobi" \
                                        + "<user>" + email + "</user>" \
                                        + "<subscription>" + "report" + "</subscription>" \
                                        + "<subscription_type>" + report_subscription.value + "</subscription_type>" \
                                        + f"<subscription_name>{report_subscription.value} report for {client}" \
                                          "</subscription_name>" \
                                        + "<client>" + client + "</client>" \
                                        + "tech"
        unsubscribe_link_checksum = sha1(bytes(unsubscribe_link_checksum_str, "utf-8")).hexdigest()

        url_parameters = urlencode({
            "user": email,
            "client": client,
            "subscription": "report",
            "subscription_type": report_subscription.value,
            "checksum": unsubscribe_link_checksum,
            "subscription_name": f"{report_subscription.value} report for {client}"
        })

        html += report_row_footer(f"unsubscribe?{url_parameters}")
        html = report_full_email(html)
        get_logger().info(f"Report email for {email} created (still need to send it)!")

        result.append(html)

    get_logger().info(f"Report successfully created for all {client} users.")

    return result


def send_email(from_email: str, from_name: str, from_password: str, to_email: str, to_name: str, subject: str,
               html_content: str):
    get_logger().info(f"Sending email to {to_email}...")
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = f"{from_name} <{from_email}>"
    msg['To'] = f"{to_name} <{to_email}>"

    html_part = MIMEText(html_content, 'html')
    msg.attach(html_part)

    mail = smtplib.SMTP('smtp.gmail.com', 587)
    mail.ehlo()
    mail.starttls()
    mail.login(from_email, from_password)
    mail.sendmail(from_email, to_email, msg.as_string())
    mail.quit()
    get_logger().info(f"Message to {to_email} sent!")
    del mail


def get_users_to_submit_report_to(client: str, report_subscription: ReportSubscription,
                                  new_email_id: str) -> List[Tuple[str, str]]:
    result = []
    for email in auth_dao().read_users_for_client(client):
        if report_subscription in user_settings_dao().read_report_subscriptions_list(email, client):
            last_email_id = user_settings_dao().get_last_report_email_id(email, client, report_subscription)
            if last_email_id != new_email_id:
                user = auth_dao().read_user(email)
                result.append((email, user["name"]))
    return result


class ReportSubscriptionSender(MobiDaemonTask):
    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        from_email = "no-reply@mobimobi.tech"
        from_name = "Mobimobi.tech"
        from_password = "vW61l7Uqm1W0bZVpOalk"

        product_id_to_brand_map = {product.product_id: product.brand for product in
                                   catalog_dao().read_all_products(client, include_inactive=True,
                                                                   include_not_in_stock=True)}

        datetime_now = datetime.utcnow() - timedelta(hours=6)

        for report_subscription in ReportSubscription:
            if report_subscription == ReportSubscription.DAILY:
                yesterday = datetime_now - timedelta(days=1)

                email_id_date = yesterday.strftime("%Y_%m_%d")

                from_date = yesterday - timedelta(hours=yesterday.hour, minutes=yesterday.minute,
                                                  seconds=yesterday.second, microseconds=yesterday.microsecond)
                till_date = from_date + timedelta(days=1)
            elif report_subscription == ReportSubscription.WEEKLY:
                today = datetime_now
                last_monday = today - timedelta(days=today.weekday())
                prev_monday = last_monday - timedelta(weeks=1)
                email_id_date = prev_monday.strftime("%Y_%m_%d")

                from_date = prev_monday - timedelta(hours=prev_monday.hour, minutes=prev_monday.minute,
                                                    seconds=prev_monday.second, microseconds=prev_monday.microsecond)
                till_date = from_date + timedelta(days=7)
            else:
                continue

            email_id = f"report_{report_subscription.value}_{email_id_date}"

            users = get_users_to_submit_report_to(client, report_subscription, email_id)

            if users:
                get_logger().info(f"Need to send {report_subscription.value} report to {len(users)} users of {client}")
                for (email, user_name), html in zip(users, build_report_html(client, users, report_subscription,
                                                                             from_date, till_date,
                                                                             product_id_to_brand_map)):
                    send_email(
                        from_email=from_email,
                        from_name=from_name,
                        from_password=from_password,
                        to_email=email,
                        to_name=user_name,
                        subject=f"{client}: Mobimobi.tech {report_subscription.value} report",
                        html_content=html
                    )

                    user_settings_dao().update_last_report_email_id(email, client, report_subscription, email_id)
