from datetime import datetime, timedelta
from logging import DEBUG
from typing import Dict, Any

from mobi.config.system.service.cache_builder import get_cache_builder_config
from mobi.core.logging import get_logger
from mobi.core.models import EventType
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao, get_metrics_dao
from mobi.dao.cache import cache_key, MobiCache
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.service.task.interface import MobiDaemonTask


def cache_dao() -> MobiCache:
    return get_cache_dao(get_cache_builder_config())


def catalog_dao() -> MobiCatalogDao:
    return get_catalog_dao(get_cache_builder_config())


def events_dao() -> MobiEventDao:
    return get_event_dao(get_cache_builder_config())


def metrics_dao() -> MobiMetricsDao:
    return get_metrics_dao(get_cache_builder_config())


logger = get_logger()
logger.setLevel(DEBUG)


class CatalogCacheBuilderTask(MobiDaemonTask):
    @classmethod
    def build_catalog_stats_cache(cls, client: str, client_context: dict):
        stats = dict()
        brand_stats = dict()
        last_stats = client_context.get("last_stats")

        product_id_to_brand_map = dict()

        for product in catalog_dao().read_all_products(client, include_inactive=True, include_not_in_stock=True):
            product_id_to_brand_map[product.product_id] = product.brand
            stats[product.product_id] = {et: 0 for et in EventType}
            brand_stats[product.brand] = {et: 0 for et in EventType}

        from_date = datetime.utcnow() - timedelta(days=1)

        for event in events_dao().read_events(client, from_date=from_date):
            if event.product_id is not None:
                if event.product_id in stats:
                    stats[event.product_id][event.event_type] += 1
                if event.product_id in product_id_to_brand_map \
                        and product_id_to_brand_map[event.product_id] in brand_stats:
                    brand_stats[product_id_to_brand_map[event.product_id]][event.event_type] += 1
            if event.basket_items:
                for basket_item in event.basket_items:
                    if basket_item.product_id in stats:
                        stats[basket_item.product_id][event.event_type] += 1
                    if basket_item.product_id in product_id_to_brand_map \
                            and product_id_to_brand_map[basket_item.product_id] in brand_stats:
                        brand_stats[product_id_to_brand_map[basket_item.product_id]][event.event_type] += 1

        cache = cache_dao()

        for product_id, product_stats in stats.items():
            for event_type in EventType:
                if last_stats is None \
                        or product_id not in last_stats \
                        or event_type not in last_stats[product_id] \
                        or last_stats[product_id][event_type] != 0 \
                        or product_stats[event_type] != 0:

                    key = cache_key("product_events", product_id=product_id, client=client,
                                    event_type=event_type.name.lower(), period="24h")
                    cache.set_int(key, product_stats[event_type], noreply=False, expire=60 * 60, retries=3)

        for brand, brand_stats in brand_stats.items():
            for event_type in EventType:
                key = cache_key("brand_events", brand=brand, client=client, event_type=event_type.name.lower(),
                                period="24h")
                cache.set_int(key, brand_stats[event_type], noreply=False, expire=60 * 60, retries=3)

        client_context["last_stats"] = stats

    @classmethod
    def build_catalog_cache(cls, client: str):
        products_num = 0.0
        products_active = 0.0
        products_in_stock = 0.0
        products_active_and_in_stock = 0.0

        for product in catalog_dao().read_all_products(client, include_inactive=True, include_not_in_stock=True):
            products_num += 1
            if product.active:
                products_active += 1
            if product.in_stock:
                products_in_stock += 1
            if product.active and product.in_stock:
                products_active_and_in_stock += 1

        current_ts = int(datetime.utcnow().timestamp())

        for base in (60, 60 * 60, 24 * 60 * 60):
            metrics_dao().set_metrics_point(
                client,
                "products_num",
                {
                    "active": "yes",
                    "in_stock": "yes"
                },
                base,
                current_ts,
                products_active_and_in_stock
            )

            metrics_dao().set_metrics_point(
                client,
                "products_num",
                {
                    "active": "yes",
                    "in_stock": "no"
                },
                base,
                current_ts,
                products_active - products_active_and_in_stock
            )

            metrics_dao().set_metrics_point(
                client,
                "products_num",
                {
                    "active": "no",
                    "in_stock": "yes"
                },
                base,
                current_ts,
                products_in_stock - products_active_and_in_stock
            )

            metrics_dao().set_metrics_point(
                client,
                "products_num",
                {
                    "active": "no",
                    "in_stock": "no"
                },
                base,
                current_ts,
                products_num - products_active - products_in_stock + products_active_and_in_stock
            )

    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        self.build_catalog_cache(client)
        self.build_catalog_stats_cache(client, client_context)
