from datetime import datetime, timedelta
from logging import DEBUG
from typing import Any, Dict

from mobi.config.system.service.metrics_builder import get_metrics_builder_config
from mobi.core.logging import get_logger
from mobi.dao import get_event_dao, get_metrics_dao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.service.task.interface import MobiDaemonTask


def event_dao() -> MobiEventDao:
    return get_event_dao(get_metrics_builder_config())


def metrics_dao() -> MobiMetricsDao:
    return get_metrics_dao(get_metrics_builder_config())


logger = get_logger()
logger.setLevel(DEBUG)


class ChurnRateTask(MobiDaemonTask):
    MAX_RECALC_TIMEDELTA = timedelta(days=15)

    USER_ACTIVITY_WINDOW = 24 * 60 * 60

    CHURNED_AFTER_CONFIG = {
        "7d": 7 * 24 * 60 * 60,
        "35d": 35 * 24 * 60 * 60
    }

    # CHURNED_AFTER = 28 * 24 * 60 * 60

    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        metrics_base = 24 * 60 * 60

        now_ts = metrics_base * (int(datetime.utcnow().timestamp()) // metrics_base)

        latest_ts = None
        for metric_suffix in ("rel", "abs"):
            for churned_after_suffix in self.CHURNED_AFTER_CONFIG.keys():
                latest_ts_candidate = metrics_dao().get_latest_point_timestamp(
                    client, f"churned_users_w7d_a{churned_after_suffix}_{metric_suffix}", None, 24 * 60 * 60
                )
                if latest_ts_candidate is not None:
                    if latest_ts is None:
                        latest_ts = latest_ts_candidate
                    else:
                        latest_ts = min(latest_ts, latest_ts_candidate)

        if latest_ts is None:
            latest_ts = int(datetime.utcnow().timestamp() - self.MAX_RECALC_TIMEDELTA.total_seconds())
            latest_ts = metrics_base * (latest_ts // metrics_base)

        if latest_ts == now_ts and "latest_calc_date" in client_context:
            if datetime.utcnow() - client_context["latest_calc_date"] < timedelta(hours=1):
                # Basically, we don't want to recalc current day value more often than once in an hour
                return

        for db_metrics_timestamp in list(range(latest_ts, now_ts + 1, metrics_base))[0:2]:
            for churned_after_suffix, churned_after_period_sec in self.CHURNED_AFTER_CONFIG.items():

                active_users_from_timestamp = db_metrics_timestamp - churned_after_period_sec \
                                              - self.USER_ACTIVITY_WINDOW + metrics_base
                active_users_until_timestamp = active_users_from_timestamp + self.USER_ACTIVITY_WINDOW

                active_users = event_dao().read_unique_user_ids(
                    client,
                    datetime.utcfromtimestamp(active_users_from_timestamp),
                    datetime.utcfromtimestamp(active_users_until_timestamp)
                )

                active_users = set(active_user_id for active_user_id, _, _ in active_users)

                returned_users = event_dao().read_unique_user_ids(
                    client,
                    datetime.utcfromtimestamp(active_users_until_timestamp),
                    datetime.utcfromtimestamp(db_metrics_timestamp + metrics_base)
                )

                returned_users = set(returned_user_id for returned_user_id, _, _ in returned_users)
                churned_users_num = len(active_users.difference(returned_users))

                metrics_dao().set_metrics_point(client, f"churned_users_w7d_a{churned_after_suffix}_abs", {},
                                                metrics_base, db_metrics_timestamp, float(churned_users_num))

                rel_value = 0.0 if not active_users else churned_users_num / len(active_users)
                metrics_dao().set_metrics_point(client, f"churned_users_w7d_a{churned_after_suffix}_rel", {},
                                                metrics_base, db_metrics_timestamp, rel_value)

                client_context["latest_calc_date"] = datetime.utcnow()
