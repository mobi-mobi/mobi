import pickle

from collections import Counter
from datetime import datetime, timedelta
from logging import DEBUG
from typing import Any, Dict, List

from mobi.config.system.service.cache_builder import get_cache_builder_config
from mobi.core.logging import get_logger
from mobi.core.models import EventType
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao, MobiDaoException
from mobi.dao.cache import cache_key, MobiCache
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.service.task.interface import MobiDaemonTask


def cache_dao() -> MobiCache:
    return get_cache_dao(get_cache_builder_config())


def catalog_dao() -> MobiCatalogDao:
    return get_catalog_dao(get_cache_builder_config())


def event_dao() -> MobiEventDao:
    return get_event_dao(get_cache_builder_config())


logger = get_logger()
logger.setLevel(DEBUG)

RECALC_EVERY = timedelta(minutes=30)


class CacheBuilderTask(MobiDaemonTask):
    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        counters: Counter = client_context.get("counters", Counter())
        client_context["counters"] = counters

        brand_counters: Dict[str, Counter] = client_context.get("brand_counters", dict())
        client_context["brand_counters"] = brand_counters

        categories_counters: Dict[str, Counter] = client_context.get("categories_counters", dict())
        client_context["categories_counters"] = categories_counters

        brand_categories_counters: Dict[str, Dict[str, Counter]] = client_context.get("brand_categories_counters",
                                                                                      dict())
        client_context["brand_categories_counters"] = brand_categories_counters

        global_brands_counter: Counter = client_context.get("global_brands_counter", Counter())
        client_context["global_brands_counter"] = global_brands_counter

        global_categories_counter: Counter = client_context.get("global_categories_counter", Counter())
        client_context["global_categories_counter"] = global_categories_counter

        product_brand_map: Dict[str, str] = client_context.get("product_brand_map", dict())
        client_context["product_brand_map"] = product_brand_map

        product_categories_map: Dict[str, List[str]] = client_context.get("product_categories_map", dict())
        client_context["product_categories_map"] = product_categories_map

        global RECALC_EVERY

        now = datetime.utcnow()

        if not counters or ("last_recalc" in client_context and now - RECALC_EVERY > client_context["last_recalc"]):
            delete_from = None
            delete_to = None
            add_from = now - timedelta(hours=8)
            add_to = now

            client_context["last_from_date"] = add_from
            client_context["last_to_date"] = now
            client_context["last_recalc"] = now

            counters.clear()
            brand_counters.clear()
            categories_counters.clear()
            global_brands_counter.clear()
            global_categories_counter.clear()
            product_brand_map.clear()
            product_categories_map.clear()
        else:
            delete_from = client_context["last_from_date"]
            delete_to = now - timedelta(hours=8)
            add_from = client_context["last_to_date"]
            add_to = now

            client_context["last_from_date"] = delete_to
            client_context["last_to_date"] = now

        if not product_brand_map or not product_categories_map:
            for product in catalog_dao().read_all_products(client):
                product_brand_map[product.product_id] = product.brand
                product_categories_map[product.product_id] = product.categories

        added = 0
        deleted = 0

        if delete_from is not None:
            for event in event_dao().read_events(client, delete_from, delete_to):
                if event.product_id not in product_brand_map:
                    try:
                        product_brand_map[event.product_id] = catalog_dao().read_product(client, event.product_id).brand
                    except MobiDaoException:
                        pass  # Product does not exist, but there is nothing we can do

                if event.product_id not in product_categories_map:
                    try:
                        product_categories_map[event.product_id] \
                            = catalog_dao().read_product(client, event.product_id).categories
                    except MobiDaoException:
                        pass  # Product does not exist, but there is nothing we can do

                try:
                    brand = product_brand_map[event.product_id]
                    categories = product_categories_map[event.product_id]
                except KeyError:
                    continue

                if brand is not None and brand not in brand_counters:
                    brand_counters[brand] = Counter()

                if brand is not None and brand not in brand_categories_counters:
                    brand_categories_counters[brand] = dict()

                if categories:
                    for category in categories:
                        if category is not None and category not in categories_counters:
                            categories_counters[category] = Counter()
                        if category is not None and category not in brand_categories_counters[brand]:
                            brand_categories_counters[brand][category] = Counter()

                if event.event_type == EventType.PRODUCT_VIEW:
                    counters[event.product_id] -= 1
                    if brand is not None:
                        global_brands_counter[brand] -= 1
                    if brand in brand_counters:
                        brand_counters[brand][event.product_id] -= 1
                    if categories:
                        for category in categories:
                            if category is not None:
                                global_categories_counter[category] -= 1
                            if category in categories_counters:
                                categories_counters[category][event.product_id] -= 1
                    if brand and categories:
                        if brand in brand_categories_counters:
                            for category in categories:
                                if category in brand_categories_counters[brand]:
                                    brand_categories_counters[brand][category][event.product_id] -= 1
                    deleted += 1

        for event in event_dao().read_events(client, add_from, add_to):
            if event.product_id not in product_brand_map:
                try:
                    product_brand_map[event.product_id] = catalog_dao().read_product(client, event.product_id).brand
                except MobiDaoException:
                    pass  # Product does not exist, but there is nothing we can do

            if event.product_id not in product_categories_map:
                try:
                    product_categories_map[event.product_id] \
                        = catalog_dao().read_product(client, event.product_id).categories
                except MobiDaoException:
                    pass  # Product does not exist, but there is nothing we can do

            try:
                brand = product_brand_map[event.product_id]
                categories = product_categories_map[event.product_id]
            except KeyError:
                continue

            if brand is not None and brand not in brand_counters:
                brand_counters[brand] = Counter()

            if brand is not None and brand not in brand_categories_counters:
                brand_categories_counters[brand] = dict()

            if categories:
                for category in categories:
                    if category is not None and category not in categories_counters:
                        categories_counters[category] = Counter()
                    if category is not None and category not in brand_categories_counters[brand]:
                        brand_categories_counters[brand][category] = Counter()

            if event.event_type == EventType.PRODUCT_VIEW:
                counters[event.product_id] += 1
                if brand is not None:
                    global_brands_counter[brand] += 1
                if brand in brand_counters:
                    brand_counters[brand][event.product_id] += 1
                if categories:
                    for category in categories:
                        if category is not None:
                            global_categories_counter[category] += 1
                        if category in categories_counters:
                            categories_counters[category][event.product_id] += 1
                if brand and categories:
                    if brand in brand_categories_counters:
                        for category in categories:
                            if category in brand_categories_counters[brand]:
                                brand_categories_counters[brand][category][event.product_id] += 1
                added += 1

        for product_id, product_to_basket in counters.items():
            cache_dao().set(cache_key("product_to_basket", client=client, product=product_id, interval="8h"),
                            product_to_basket, expire=60 * 60, noreply=False, retries=5)

        for brand in brand_counters.keys():
            cache_dao().set(cache_key("most_popular", client=client, brand=brand, interval="8h"),
                            pickle.dumps(brand_counters[brand].most_common(n=50)), expire=60 * 60,
                            noreply=False, retries=5)

        for category in categories_counters.keys():
            cache_dao().set(cache_key("most_popular", client=client, category=category, interval="8h"),
                            pickle.dumps(categories_counters[category].most_common(n=50)), expire=60 * 60,
                            noreply=False, retries=5)

        for brand in brand_categories_counters.keys():
            for category in brand_categories_counters[brand].keys():
                cache_dao().set(cache_key("most_popular", client=client, brand=brand, category=category, interval="8h"),
                                pickle.dumps(brand_categories_counters[brand][category].most_common(n=50)),
                                expire=60 * 60, noreply=False, retries=5)

        cache_dao().set(cache_key("most_popular", client=client, interval="8h"),
                        pickle.dumps(counters.most_common(n=50)), expire=60 * 60, noreply=False, retries=5)

        cache_dao().set(cache_key("most_popular_brands", client=client, interval="8h"),
                        pickle.dumps(global_brands_counter.most_common(n=50)), expire=60 * 60, noreply=False,
                        retries=5)

        cache_dao().set(cache_key("most_popular_categories", client=client, interval="8h"),
                        pickle.dumps(global_categories_counter.most_common(n=50)), expire=60 * 60, noreply=False,
                        retries=5)

        # Calculating real-time users

        rt_users = set()
        for event in event_dao().read_events(client, from_date=datetime.utcnow() - timedelta(minutes=1)):
            rt_users.add(event.user_id)

        # TODO: This is ugly. We need to improve this solution
        cache_dao().set_int(cache_key("real_time_users_num", client=client), len(rt_users), expire=15 * 60,
                            noreply=False, retries=5)

        logger.debug(f"Adding events from {add_from} to {add_to}")
        logger.info(f"Done for client {client}. Added {added} events. Deleted {deleted}")
        logger.info(f"Most popular products: {counters.most_common(n=3)}")
        logger.info(f"Most popular brands: {global_brands_counter.most_common(n=3)}")
        logger.info(f"Most popular categories: {global_categories_counter.most_common(n=3)}")
        logger.info(f"Active users num: {len(rt_users)}")
