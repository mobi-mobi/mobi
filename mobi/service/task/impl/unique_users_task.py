from collections import defaultdict
from datetime import datetime, timedelta
from typing import Any, Dict

from mobi.config.system.service.metrics_builder import get_metrics_builder_config
from mobi.dao import get_event_dao, get_metrics_dao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.service.task.interface import MobiDaemonTask


def events_dao() -> MobiEventDao:
    return get_event_dao(get_metrics_builder_config())


def metrics_dao() -> MobiMetricsDao:
    return get_metrics_dao(get_metrics_builder_config())


class UniqueUsersTask(MobiDaemonTask):
    FREQUENCY = {
        60: 10,
        60 * 60: 10 * 60,
        24 * 60 * 60: 30 * 60
    }

    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        metric_name = "unique_users"

        if "wait_until" not in client_context:
            client_context["wait_until"] = dict()

        for base in 60, 60 * 60, 24 * 60 * 60:
            latest_ts_key = f"latest_ts.{base}"

            wait_until = client_context["wait_until"].get(base, datetime.utcnow() - timedelta(hours=1))

            if datetime.utcnow() < wait_until:
                continue

            latest_ts = client_context.get(latest_ts_key)
            if latest_ts is None:
                latest_ts = metrics_dao().get_latest_point_timestamp(client, metric_name, None, base)

            if latest_ts is None:
                latest_ts = (int((datetime.utcnow() - timedelta(days=30)).timestamp()) // base) * base

            current_ts = latest_ts

            unique_users = defaultdict(set)

            for event in events_dao().read_events(
                client,
                from_date=datetime.fromtimestamp(current_ts),
                till_date=datetime.fromtimestamp(current_ts) + timedelta(days=2)
            ):
                unique_users[(int(event.date.timestamp()) // base) * base].add(event.user_id)

            timestamps = list(unique_users.keys())
            timestamps.sort()

            for ts in timestamps:
                metrics_dao().set_metrics_point(client, metric_name, {}, base, ts, float(len(unique_users[ts])))
                client_context[latest_ts_key] = ts
                del unique_users[ts]

            del unique_users

            client_context["wait_until"][base] = datetime.utcnow() + timedelta(seconds=self.FREQUENCY[base])
