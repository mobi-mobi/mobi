from dataclasses import dataclass
from time import sleep
from typing import List, Type

from mobi.core.logging import get_logger
from mobi.service.task.interface import MobiDaemonTask

logger = get_logger()
logger.setLevel("INFO")


@dataclass
class TaskInfo:
    CLASS: Type[MobiDaemonTask]
    NAME: str
    CLIENTS: List[str]
    TIME_INTERVAL: int


class MobiTaskRunner:
    def __init__(self, name: str, tasks: List[TaskInfo]):
        self.name = name
        self.tasks: List[MobiDaemonTask] = [
            task.CLASS(task.NAME, task.CLIENTS, task.TIME_INTERVAL) for task in tasks
        ]

    def stop_and_wait(self, interval: int = 10):
        logger.info(f"{self.name}: Exiting from tasks")
        for task in self.tasks:
            task.should_stop = True
        logger.info(f"{self.name}: Giving tasks {interval} seconds to finish...")

        for _ in range(interval):
            sleep(1)
            running = False
            for task in self.tasks:
                if task.is_alive():
                    running = True
            if not running:
                logger.info(f"{self.name}: All tasks successfully finished")
                break

        for task in self.tasks:
            if task.is_alive():
                logger.warning(f"{self.name}: Task {task.name} did not have enough time to finish "
                               f"and will be killed")

    def run(self):
        for task in self.tasks:
            task.start()

        try:
            while True:
                sleep(1)
        except KeyboardInterrupt:
            self.stop_and_wait()
        except SystemExit:
            self.stop_and_wait()
