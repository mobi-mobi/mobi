import gc

from abc import ABC, abstractmethod
from collections import defaultdict
from datetime import datetime, timedelta
from threading import Thread
from time import sleep
from traceback import format_exc
from typing import Any, Dict, List

from mobi.core.logging import get_logger

logger = get_logger()


class MobiDaemonTask(Thread, ABC):
    def __init__(self, name: str, clients: List[str], interval_sec: int = 60):
        if not clients:
            raise ValueError("Clients list is empty")
        if interval_sec < 0:
            raise ValueError("Wring interval_sec value. Should be positive integer")

        super().__init__(name=name, daemon=True)
        self.clients = clients
        self.should_stop = False
        self.interval_sec = interval_sec
        self.global_context = dict()
        self.client_contexts = defaultdict(dict)

    @abstractmethod
    def process_client(self, client: str, global_context: Dict[str, Any], client_context: Dict[str, Any]):
        pass

    def run(self):
        while True:
            if self.should_stop:
                break

            for client in self.clients:
                if not self.should_stop:
                    try:
                        self.process_client(client, self.global_context, self.client_contexts[client])
                    except Exception as e:
                        logger.error(f"{self.name}: Exception raised while processing client {client}: {e}")
                        logger.error(format_exc())
                    finally:
                        gc.collect()

            logger.info(f"{self.name}: Finished processing all clients. "
                        f"Going to sleep for {self.interval_sec} second(s)")
            continue_at = datetime.utcnow() + timedelta(seconds=self.interval_sec)

            while datetime.utcnow() < continue_at and not self.should_stop:
                sleep(1)

    def stop(self):
        self.should_stop = True
