import requests
import subprocess

from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime, timedelta
from enum import Enum
from json.decoder import JSONDecodeError
from time import sleep
from typing import List, Optional, Tuple

from mobi.core.logging import get_logger


class StatusCheckerException(Exception):
    pass


class ServiceStatus(Enum):
    NOT_OK = 0
    OK = 1


class ServiceStatusChecker(ABC):
    def __init__(self):
        self.logger = get_logger()
        self.logger.setLevel(20)

    @abstractmethod
    def check(self) -> Tuple[ServiceStatus, str]:
        pass


class JsonOkStatusChecker(ServiceStatusChecker):
    def __init__(self, url: str):
        super().__init__()
        self.url = url

    def check(self) -> Tuple[ServiceStatus, str]:
        try:
            resp = requests.get(self.url)
        except requests.exceptions.ConnectionError:
            return ServiceStatus.NOT_OK, f"Can not connect to {self.url}"

        if resp.status_code != 200:
            return ServiceStatus.NOT_OK, f"HTTP Code: {resp.status_code}"

        try:
            json = resp.json()
        except JSONDecodeError:
            return ServiceStatus.NOT_OK, f"Can not parse JSON: \"{resp.text[0:20]}\"..."

        if type(json) != dict:
            return ServiceStatus.NOT_OK, f"Json is not a valid dict: \"{json}\""

        if "status" not in json:
            return ServiceStatus.NOT_OK, f"Status field is missing in json: \"{json}\""

        if json["status"] != "OK":
            return ServiceStatus.NOT_OK, f"Status is not OK: \"{json}\""

        return ServiceStatus.OK, "OK"


class PsStatusChecker(ServiceStatusChecker):
    def __init__(self, entities: List[str]):
        super().__init__()
        if not entities:
            raise StatusCheckerException(f"Entities list is empty")
        self.entities = entities

    def check(self) -> Tuple[ServiceStatus, str]:
        try:
            process = subprocess.Popen(['ps', '-ax'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except Exception as e:
            raise StatusCheckerException(f"Can not check process status: {e}") from None

        stdout, stderr = process.communicate()

        if stderr is not None:
            raise StatusCheckerException(f"Can not check process status. Stderr is not None: {stderr[0:20]}...")

        try:
            stdout = stdout.decode("utf-8")
        except Exception:
            raise StatusCheckerException(f"Can not decode stdout: {stdout[0:20]}")

        lines = stdout.split("\n")

        for entity in self.entities:
            lines = [line for line in lines if line.find(entity) != -1]

        # if len(lines) > 1:
        #     ps = ", ".join(["\"" + p + "\"" for p in lines])
        #     self.logger.warning(f"More than one process were found: {ps}")

        if not lines:
            return ServiceStatus.NOT_OK, "Process was not found"

        return ServiceStatus.OK, "OK"


@dataclass
class Process:
    name: str
    checker: ServiceStatusChecker
    launch_command: str
    restart_command: Optional[str] = None
    restart_every: Optional[timedelta] = None


PROCESSES = [
    Process(
        name="mongod",
        checker=PsStatusChecker(["/usr/bin/mongod"]),
        launch_command="systemctl restart mongod"
    ),
    Process(
        name="elasticsearch",
        checker=PsStatusChecker(["/usr/share/elasticsearch/jdk/bin/java"]),
        launch_command="systemctl restart elasticsearch.service"
    ),
    Process(
        name="memcached",
        checker=PsStatusChecker(["/usr/bin/memcached"]),
        launch_command="systemctl restart memcached"
    ),
    Process(
        name="nginx",
        checker=PsStatusChecker(["/usr/sbin/nginx", "master process"]),
        launch_command="/etc/init.d/nginx restart"
    ),
    Process(
        name="metricsapi",
        checker=PsStatusChecker(["gunicorn", "metrics_api_app()"]),
        launch_command="systemctl restart metricsapi"
    ),
    Process(
        name="eventapi",
        checker=PsStatusChecker(["gunicorn", "event_api_app()"]),
        launch_command="systemctl restart eventapi"
    ),
    Process(
        name="recommendationapi",
        checker=PsStatusChecker(["gunicorn", "recommendation_api_app()"]),
        launch_command="systemctl restart recommendationapi"
    ),
    Process(
        name="catalogapi",
        checker=PsStatusChecker(["gunicorn", "catalog_api_app()"]),
        launch_command="systemctl restart catalogapi"
    ),
    Process(
        name="console",
        checker=JsonOkStatusChecker("http://127.0.0.1:9701/status"),
        launch_command="systemctl restart console"
    ),
    Process(
        name="attribution_builder",
        checker=PsStatusChecker(["mobi.service.attribution_builder"]),
        launch_command="systemctl restart attribution_builder"
    ),
    Process(
        name="metrics_builder",
        checker=PsStatusChecker(["mobi.service.metrics_builder"]),
        launch_command="systemctl restart metrics_builder"
    ),
    Process(
        name="model_builder",
        checker=PsStatusChecker(["mobi.service.model_builder"]),
        launch_command="systemctl restart model_builder",
        restart_command="systemctl restart model_builder",
        restart_every=timedelta(hours=5)
    ),
    Process(
        name="cache_builder",
        checker=PsStatusChecker(["mobi.service.cache_builder"]),
        launch_command="systemctl restart cache_builder"
    ),
    Process(
        name="dynamic_features_builder",
        checker=PsStatusChecker(["mobi.service.dynamic_features_builder"]),
        launch_command="systemctl restart dynamic_features_builder"
    ),
    Process(
        name="retention_cleaner",
        checker=PsStatusChecker(["mobi.service.retention_cleaner"]),
        launch_command="systemctl restart retention_cleaner"
    ),
    Process(
        name="email_sender",
        checker=PsStatusChecker(["mobi.service.email_sender"]),
        launch_command="systemctl restart email_sender"
    )
]

if __name__ == "__main__":
    logger = get_logger()
    logger.setLevel(20)
    interval = 60

    restart_times = dict()

    while True:
        for process in PROCESSES:
            logger.debug(f"Checking {process.name}")

            if process.restart_every is not None:
                if process.name not in restart_times:
                    restart_times[process.name] = datetime.utcnow()
                if datetime.utcnow() - restart_times[process.name] > process.restart_every:
                    logger.info(f"Process {process.name} needs to be restarted. Restarting...")
                    cmd = [c for c in process.restart_command.split() if c]
                    try:
                        out, err = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
                        restart_times[process.name] = datetime.utcnow()
                    except Exception as e:
                        logger.error(f"Exception raised while executing the command: {e}")

            try:
                status, err_msg = process.checker.check()
            except Exception as e:
                logger.error(f"Exception raised while checking process {process.name}: {e}")
                continue

            if status == ServiceStatus.OK:
                logger.debug(f"Service is running")
                continue
            else:
                logger.warning(f"Service {process.name} is not running: {err_msg}")

            logger.info(f"Service is not running. Executing \"{process.launch_command}\"...")
            cmd = [c for c in process.launch_command.split() if c]
            try:
                out, err = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
            except Exception as e:
                logger.error(f"Exception raised while executing the command: {e}")
                continue

            if err is not None:
                try:
                    err = err.decode("utf-8")
                except Exception as e:
                    logger.warning(f"Can not decode stderr")
                    continue

                if err:
                    logger.warning(f"Stderr is not empty: {err}")

        logger.info(f"Going to sleep for {interval} seconds...")
        sleep(interval)
