from mobi.service.task import MobiTaskRunner, TaskInfo
from mobi.service.task.impl import ChurnRateTask, NewUsersTask, StatsCalculation, UniqueUsersTask


all_clients = [
    "sephora",
    "sephora-test",
    "testclient"
]

metrics_generator = MobiTaskRunner("Metrics Generator", [
    TaskInfo(ChurnRateTask, "Churn Rate Task", all_clients, 10 * 60),
    TaskInfo(UniqueUsersTask, "Unique Users Task", all_clients, 60),
    TaskInfo(NewUsersTask, "New Users Task", all_clients, 60),
    TaskInfo(StatsCalculation, "Statistics Calculation Task", all_clients, 3 * 60 * 60)  # Every three hour
])


if __name__ == "__main__":
    metrics_generator.run()
