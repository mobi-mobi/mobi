from mobi.service.task import MobiTaskRunner, TaskInfo
from mobi.service.task.impl import AttributionBuilderTask


all_clients = [
    "sephora",
    "sephora-test",
    "testclient"
]

attribution_builder = MobiTaskRunner("Attribution Builder", [
    TaskInfo(AttributionBuilderTask, "Attribution Builder", all_clients, 60),
])


if __name__ == "__main__":
    attribution_builder.run()
