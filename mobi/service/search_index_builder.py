from mobi.service.task import MobiTaskRunner, TaskInfo
from mobi.service.task.impl import SearchIndexBuildTask


all_clients = [
    "sephora",
    "sephora-test",
    "testclient"
]

metrics_generator = MobiTaskRunner("Search Index Builder", [
    TaskInfo(SearchIndexBuildTask, "Search Index Build", all_clients, 5 * 60),
])


if __name__ == "__main__":
    metrics_generator.run()
