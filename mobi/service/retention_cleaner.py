from mobi.service.task import MobiTaskRunner, TaskInfo
from mobi.service.task.impl import RetentionCleanerTask


all_clients = [
    "sephora",
    "sephora-test",
    "testclient"
]

retention_cleaner = MobiTaskRunner("Retention Cleaner", [
    TaskInfo(RetentionCleanerTask, "Retention Cleaner", all_clients, 25 * 60 * 60),  # Every 25 hours
])


if __name__ == "__main__":
    retention_cleaner.run()
