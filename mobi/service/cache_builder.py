from mobi.service.task import MobiTaskRunner, TaskInfo
from mobi.service.task.impl import CacheBuilderTask, CatalogCacheBuilderTask, DynamicCountersTask


all_clients = [
    "sephora",
    "sephora-test",
    "testclient"
]

cache_builder = MobiTaskRunner("Cache Builder", [
    TaskInfo(CacheBuilderTask, "Cache Builder", all_clients, 60),
    TaskInfo(CatalogCacheBuilderTask, "Catalog Cache Builder", all_clients, 5 * 60)
])


if __name__ == "__main__":
    cache_builder.run()
