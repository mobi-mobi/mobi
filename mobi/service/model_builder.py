from mobi.service.task import MobiTaskRunner, TaskInfo
from mobi.service.task.impl import ModelBuilderTask


all_clients = [
    "sephora",
    "sephora-test",
    "testclient"
]

model_builder = MobiTaskRunner("Model Builder", [
    TaskInfo(ModelBuilderTask, "Model Builder", all_clients, 6 * 60 * 60),
])


if __name__ == "__main__":
    model_builder.run()
