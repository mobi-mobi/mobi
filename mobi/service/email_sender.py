from mobi.service.task import MobiTaskRunner, TaskInfo
from mobi.service.task.impl import ReportSubscriptionSender


all_clients = [
    "sephora",
    "sephora-test",
    "testclient"
]

email_sender = MobiTaskRunner("Email Sender", [
    TaskInfo(ReportSubscriptionSender, "Report Subscription Sender", all_clients, 60 * 60),
])


if __name__ == "__main__":
    email_sender.run()
