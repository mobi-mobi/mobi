from mobi.service.task import MobiTaskRunner, TaskInfo
from mobi.service.task.impl import DynamicCountersTask


all_clients = [
    "sephora",
    "sephora-test",
    "testclient"
]

dynamic_features_builder = MobiTaskRunner("Dynamic Features Builder", [
    TaskInfo(DynamicCountersTask, "DynamicCountersTask", all_clients, 60)
])


if __name__ == "__main__":
    dynamic_features_builder.run()
