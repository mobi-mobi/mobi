"""
This is a piece of legacy code that we decided to keep for a while
import argparse

from mobi.dao import get_dao
from mobi.ml import ModelStatus
from mobi.ml.deepwalk import DeepWalkModel


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    parser.add_argument("client", type=str, help=f"Client to build data for")

    return parser.parse_args()


def process_client(client):
    dao = get_dao(client)

    model_infos = dao.read_model_info_list()

    model_info = filter(lambda x: x["status"] == ModelStatus.READY.value, model_infos)
    model_info = max(model_info, key=lambda x: x["date"])

    model = DeepWalkModel(dao, client, model_info["model_id"], model_info["date"])
    model.load()

    with open("embeddings.txt", "w") as embeddings:
        with open("labels.txt", "w") as labels:
            labels.write("Title\tBrand\n")
            for product in dao.read_all_products():
                if product.id in model.embeddings:
                    embeddings.write("\t".join(map(str, model.embeddings[product.id])) + "\n")
                    labels.write(f"{product.title}\t{product.brand}\n")


if __name__ == "__main__":
    args = get_args()
    process_client(args.client)
"""
