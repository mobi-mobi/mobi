import argparse
import random
import requests

from datetime import datetime
from time import sleep
from typing import List

from mobi.core.models import Event, EventType, Product
from mobi.utils.tests import random_string


brand_nike = "Nike"
brand_adidas = "Adidas"
brand_asics = "Asics"
brand_ikea = "IKEA"

PRODUCTS = {
    "p_red_nike": Product(
        product_id="Red Nike",
        title="Red Nike",
        in_stock=True,
        active=True,
        brand=brand_nike,
        categories=["Shoes", "Snickers"],
        color="Red",
        is_new=True,
        is_exclusive=False
    ),

    "p_blue_nike": Product(
        product_id="Blue Nike",
        title="Blue Nike",
        in_stock=True,
        active=True,
        brand=brand_nike,
        categories=["Shoes", "Snickers"],
        color="Blue",
        is_new=True,
        is_exclusive=False
    ),

    "p_black_nike_running": Product(
        product_id="Black Nike Running",
        title="Black Nike Running",
        in_stock=True,
        active=True,
        brand=brand_nike,
        categories=["Shoes", "Snickers", "Running"],
        color="Black",
        is_new=True,
        is_exclusive=False
    ),

    "p_blue_adidas": Product(
        product_id="Blue Adidas",
        title="Blue Adidas",
        in_stock=True,
        active=True,
        brand=brand_adidas,
        categories=["Shoes", "Snickers"],
        color="Blue",
        is_new=True,
        is_exclusive=False
    ),

    "p_white_adidas": Product(
        product_id="White Adidas",
        title="White Adidas",
        in_stock=True,
        active=True,
        brand=brand_adidas,
        categories=["Shoes", "Snickers"],
        color="White",
        is_new=True,
        is_exclusive=False
    ),

    "p_white_round_table": Product(
        product_id="White Round table",
        title="White Round table",
        in_stock=True,
        active=True,
        brand=brand_ikea,
        categories=["Tables"],
        color="White",
        is_new=True,
        is_exclusive=False
    ),

    "p_black_round_table": Product(
        product_id="Black Round table",
        title="Black Round table",
        in_stock=True,
        active=True,
        brand=brand_ikea,
        categories=["Tables"],
        color="Black",
        is_new=True,
        is_exclusive=False
    ),

    "p_square_table": Product(
        product_id="Black Square table",
        title="Black Square table",
        in_stock=True,
        active=True,
        brand=brand_ikea,
        categories=["Tables"],
        color="Black",
        is_new=True,
        is_exclusive=False
    ),

    "p_black_asics_running": Product(
        product_id="Black Asics Running",
        title="Black Asics Running",
        in_stock=True,
        active=True,
        brand=brand_asics,
        categories=["Shoes", "Snickers", "Running"],
        color="Black",
        is_new=True,
        is_exclusive=False
    )
}


def _log(str):
    print(str)


def _delete(url: str):
    _log(f"DELETE: {url}")
    response = requests.delete(url, verify=False)
    _log(f"RESPONSE: {response.content}")
    return response


def _post(url: str, json_data):
    _log(f"POST: {url} DATA: {json_data}")
    response = requests.post(url, json=json_data)
    _log(f"RESPONSE: {response.content}")
    return response


def random_session(user_id: str = None) -> List[Event]:
    user_id = user_id or random_string()

    def nike_lover_choices():
        return random.choices(
            list(PRODUCTS.values()),
            [1, 1, 1, 0.1, 0.1, 0.01, 0.01, 0.01, 0.1],
            k=10
        )

    def adidas_lover_choices():
        return random.choices(
            list(PRODUCTS.values()),
            [0.1, 0.1, 0.1, 1, 1, 0.01, 0.01, 0.01, 0.1],
            k=10
        )

    def snickers_lover_choices():
        return random.choices(
            list(PRODUCTS.values()),
            [1, 1, 1, 1, 1, 0.01, 0.01, 0.01, 1],
            k=10
        )

    def ikea_lover_choices():
        return random.choices(
            list(PRODUCTS.values()),
            [0.01, 0.01, 0.01, 0.01, 0.01, 1, 1, 1, 0.01],
            k=10
        )

    products = random.choice([nike_lover_choices, adidas_lover_choices, snickers_lover_choices, ikea_lover_choices])
    return [Event(user_id, 1, p.product_id, 1, EventType.PRODUCT_VIEW, datetime.utcnow()) for p in products()]


def event_api_url(host: str):
    return f"http://{host}"


def clean_url(host: str):
    return f"{event_api_url(host)}/all"


def new_product_url(host: str):
    return f"{event_api_url(host)}/product"


def new_event_arl(host: str):
    return f"{event_api_url(host)}/event"


def start(host: str, token: str, clean: bool, rps: int):
    if clean:
        _delete(f"{clean_url(host)}?token={token}")
    for product in PRODUCTS.values():
        _post(f"{new_product_url(host)}?token={token}", json_data=product.asdict())

    events = list()
    while True:
        sleep(1.0 / rps)
        if len(events) < 100:
            events_to_add = []
            while len(events_to_add) < 200 - len(events):
                events_to_add.extend(random_session())
            random.shuffle(events_to_add)
            events.extend(events_to_add)
        event = events.pop(0).asdict()
        del event["user_population"]
        event["datetime"] = event["date"].strftime("%Y-%m-%d %H:%M:%S")
        event.pop("product_version")
        event.pop("date")
        _post(f"{new_event_arl(host)}?token={token}", json_data=event)


def get_args() -> argparse.Namespace:
    _default_host = "0.0.0.0:9090"
    # _default_host = "system.mobimobi.tech"
    _default_token = "testtoken"
    _default_clean = False
    _default_rps = 1
    _default_event_api_port = 9090

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"Which host to use. Default: {_default_host}")

    parser.add_argument("-t", "--token", required=False, type=str, default=_default_token,
                        help=f"Token to use. Default: {_default_token}")

    parser.add_argument("--clean", required=False, default=_default_clean,
                        help=f"Delete everything on start up. Default: {_default_clean}")

    parser.add_argument("-r", "--rps", required=False, type=int, default=_default_rps,
                        help=f"Max requests per second. Default: {_default_rps}")

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    start(args.host, args.token, args.clean, args.rps)
