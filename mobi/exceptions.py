class MobiException(Exception):
    pass


class MobiDecodeException(MobiException):
    pass


class MobiSchemaValidationException(MobiException):
    pass
