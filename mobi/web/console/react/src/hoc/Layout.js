import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import LeftSidebar from '../components/base/LeftSidebar';

import { userInfo, getBrandsAndCategories } from '../containers/home/actions';

class Layout extends Component {
  componentDidMount() {
    const { userInfoAction } = this.props;

    userInfoAction();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.client !== this.props.client) {
      this.props.getBrandsAndCategoriesAction(this.props.client);
    }
  }

  render() {
    const { user, children } = this.props;

    if (!user) return null;

    return (
      <div className="layout">
        <LeftSidebar />
        {children}
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.element,
  userInfoAction: PropTypes.func,
  user: PropTypes.object,
  client: PropTypes.string,
};

const mapStateToProps = (state) => {
  return {
    user: state.home.user,
    client: state.home.client,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userInfoAction: bindActionCreators(userInfo, dispatch),
    getBrandsAndCategoriesAction: bindActionCreators(getBrandsAndCategories, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
