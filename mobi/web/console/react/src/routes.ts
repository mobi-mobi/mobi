
import { lazy } from 'react';
import { SemanticICONS } from 'semantic-ui-react';

const Homepage = lazy(() => import('./pages/Homepage'));
const Zones = lazy(() => import('./pages/Zones'));
const Campaigns = lazy(() => import('./pages/Campaigns'));
const Products = lazy(() => import('./pages/Catalog/Products'));
const Brands = lazy(() => import('./pages/Catalog/Brands'));

/** Sandbox pages */
const SandboxSimilarProducts = lazy(() => import('./pages/Sandbox/SimilarProducts'));
const SandboxSearch = lazy(() => import('./pages/Sandbox/Search'));
const SandboxPersonalizedRecommendations = lazy(() => import('./pages/Sandbox/PersonalizedRecommendations'));
const SandboxBasketRecommendations = lazy(() => import('./pages/Sandbox/BasketRecommendations'));

/** Metrics pages */
const MetricsAttributionPage = lazy(() => import('./pages/Metrics/Attribution'));
const MetricsAudiencePage = lazy(() => import('./pages/Metrics/Audience'));
const MetricsIncrementalPage = lazy(() => import('./pages/Metrics/Incremental'));
const MetricsIntegrationPage = lazy(() => import('./pages/Metrics/Integration'));
const MetricsRealTimePage = lazy(() => import('./pages/Metrics/RealTime'));

/** UserSettings pages */
const AccountSettings = lazy(() => import('./pages/UserSettings/AccountSettings'));
const Subscriptions = lazy(() => import('./pages/UserSettings/Subscriptions'));
const Tokens = lazy(() => import('./pages/UserSettings/Tokens'));

/** UserSettings pages */
const CustomAudienceTable = lazy(() => import('./pages/CustomAudiences/Audiences'));
const AbTests = lazy(() => import('./pages/CustomAudiences/AbTests'));
const SplitTrees = lazy(() => import('./pages/CustomAudiences/SplitTrees'));

export type RouteType = {
  path?: string;
  name: string;
  icon?: SemanticICONS;
  sub?: Array<RouteType>;
};

export default [
  {
    name: 'dashboard',
    path: '/home',
    icon: 'clock outline',
    component: Homepage,
  },
  {
    name: 'metrics',
    icon: 'chart bar outline',
    sub: [
      {
        name: 'real_time',
        icon: 'clock outline',
        path: '/metrics/real-time',
        component: MetricsRealTimePage,
      },
      {
        name: 'audience',
        icon: 'group',
        path: '/metrics/audience',
        component: MetricsAudiencePage,
      },
      {
        name: 'attribution',
        icon: 'pin',
        path: '/metrics/attribution',
        component: MetricsAttributionPage,
      },
      {
        name: 'incremental',
        icon: 'chart line',
        path: '/metrics/incremental',
        component: MetricsIncrementalPage,
      },
      {
        name: 'integration',
        icon: 'database',
        path: '/metrics/integration',
        component: MetricsIntegrationPage,
      },
    ],
  },
  {
    name: 'zones',
    path: '/zones',
    icon: 'columns',
    component: Zones,
  },
  {
    name: 'campaigns',
    path: '/campaigns',
    icon: 'flag outline',
    component: Campaigns,
  },
  {
    name: 'catalog',
    icon: 'boxes',
    sub: [
      {
        name: 'products',
        path: '/catalog/products',
        icon: 'shopping bag',
        component: Products,
      },
      {
        name: 'nav_brands',
        path: '/catalog/brands',
        icon: 'gem outline',
        component: Brands,
      },
    ]
  },
  {
    name: 'sandbox',
    icon: 'cubes',
    sub: [
      {
        name: 'similar_products',
        icon: 'dot circle outline',
        path: '/sandbox/similar-products',
        component: SandboxSimilarProducts,
      },
      {
        name: 'search',
        icon: 'search',
        path: '/sandbox/search',
        component: SandboxSearch,
      },
      {
        name: 'personalized_recommendations',
        icon: 'lightbulb outline',
        path: '/sandbox/personalized-recommendations',
        component: SandboxPersonalizedRecommendations,
      },
      {
        name: 'basket_recommendations',
        icon: 'cart',
        path: '/sandbox/basket-recommendations',
        component: SandboxBasketRecommendations,
      },
    ],
  },
  {
    name: 'custom_audiences',
    icon: 'group',
    sub: [
      {
        name: 'audiences',
        icon: 'address card outline',
        path: '/custom-audiences/audiences',
        component: CustomAudienceTable,
      },
      {
        name: 'split_trees',
        icon: 'sitemap',
        path: '/custom-audiences/split-trees',
        component: SplitTrees,
      },
      {
        name: 'ab_tests',
        icon: 'toggle off',
        path: '/custom-audiences/ab-tests',
        component: AbTests,
      },
    ],
  },
  {
    name: 'settings',
    icon: 'settings',
    sub: [
      {
        name: 'account_settings',
        icon: 'user outline',
        path: '/user-settings/account-settings',
        component: AccountSettings,
      },
      {
        name: 'subscriptions',
        icon: 'envelope outline',
        path: '/user-settings/subscriptions',
        component: Subscriptions,
      },
      {
        name: 'tokens',
        icon: 'key',
        path: '/user-settings/tokens',
        component: Tokens,
      },
    ],
  },
];
