import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';

import LoadingSpinner from './components/ui/LoadingSpinner';
import Layout from './hoc/Layout';

import { store } from './store/configureStore';

import 'react-datepicker/dist/react-datepicker.css';
import './assets/styles/app.scss';
import './localization/i18n';

import ExtBrowserRouter from './extBrowserHistory';

import routes from './routes';

const recursiveRoutes = (routes: any) => {
  return routes.map((route: any) =>
    route.sub ? recursiveRoutes(route.sub) : <Route path={route.path} key={route.path} component={route.component} />
  );
};

ReactDOM.render(
  <Provider store={store}>
    <ExtBrowserRouter>
      <Suspense
        fallback={
          <div className="global-preloader">
            <LoadingSpinner />
          </div>
        }
      >
        <Layout>
          <Switch>
            <Route path="/login" />
            <Route path="/logout" />
            {recursiveRoutes(routes)}
            {/* {routes.map((route) => (
              <Route path={route.path} key={route.path} component={route.component} />
            ))} */}
            <Redirect from="/" to="home" />
          </Switch>
        </Layout>
      </Suspense>
    </ExtBrowserRouter>
  </Provider>,
  document.getElementById('root')
);
