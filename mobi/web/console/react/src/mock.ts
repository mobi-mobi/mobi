import { ZoneStatusEnum, ZoneTypeEnum } from './containers/zones/types';

export const zones = [
  {
    zone_id: '1',
    name: 'Some fake zone',
    zone_type: ZoneTypeEnum.BASKET_RECOMMENDATIONS,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Massa tempor nec feugiat nisl pretium. ',
    status: ZoneStatusEnum.ACTIVE,
    campaign_ids: ['c1', 'c2', 'c3'],
    zone_settings: {
      recommendations_num: 20,
      same_brand: true,
      brands: ['brand1', 'brand2'],
      categories: ['category1', 'category2'],
      extra_product_ids: [],
      extra_products_position: 'random',
      save_organic_extra_products_positions: true,
      dont_show_organic_products: true,
    },
  },
];
