import { ZoneSettingsType, ZoneTypeChartsDataType } from '../zones/types';

export enum CampaignStatusEnum {
  ACTIVE = 'ACTIVE',
  DISABLED = 'DISABLED',
}

export type CampaignType = {
  campaign_id?: string;
  name: string;
  description?: string;
  status?: CampaignStatusEnum;
  start_date?: string;
  end_date?: string;
  zone_settings: ZoneSettingsType;
};

export type CampaignTypeFull = {
  campaign: CampaignType;
  metrics_graph: ZoneTypeChartsDataType;
  metrics_single: {
    displays: number;
    clicks: number;
    to_basket: number;
    converted: number;
    ctr: number;
    sales: number;
  };
};

export const ACTION_NAMES = {
  GET_CAMPAIGN_LOAD: 'GET_CAMPAIGN_LOAD',
  GET_CAMPAIGN_SUCCESS: 'GET_CAMPAIGN_SUCCESS',
  GET_CAMPAIGN_FAILURE: 'GET_CAMPAIGN_FAILURE',

  ADD_CAMPAIGN_LOAD: 'ADD_CAMPAIGN_LOAD',
  ADD_CAMPAIGN_SUCCESS: 'ADD_CAMPAIGN_SUCCESS',
  ADD_CAMPAIGN_FAILURE: 'ADD_CAMPAIGN_FAILURE',

  DELETE_CAMPAIGN_LOAD: 'DELETE_ZONES_LOAD',
  DELETE_CAMPAIGN_SUCCESS: 'DELETE_ZONES_SUCCESS',
  DELETE_CAMPAIGN_FAILURE: 'DELETE_ZONES_FAILURE',

  ADD_CAMPAIGN_RESET: 'ADD_CAMPAIGN_RESET',
};

export type CampaignAction = {
  type: string;
  payload?: any;
};
