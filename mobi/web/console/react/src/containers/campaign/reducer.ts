import { ACTION_NAMES as types, CampaignAction, CampaignType } from './types';

type StateTypes = {
  addCampaignIsLoading: boolean;
  success: boolean;
  error: boolean;
  campaigns: Array<CampaignType>;
  campaignsLoading: boolean;
  campaignIsDeleting: boolean;
  campaignIsUpdating: boolean;
};

// eslint-disable-next-line
const initialState = <StateTypes>{
  /** Add campaign state */
  addCampaignIsLoading: false,
  success: false,
  error: false,

  /** Get campaigns */
  campaigns: [],
  campaignsLoading: false,

  /** Delete campaign */
  campaignIsDeleting: false,

  /** Updating campaign */
  campaignIsUpdating: false,
};

export function campaignReducer(state = initialState, action: CampaignAction) {
  switch (action.type) {
    case types.ADD_CAMPAIGN_LOAD:
      return {
        ...state,
        addCampaignIsLoading: true,
      };

    case types.ADD_CAMPAIGN_SUCCESS:
      return {
        ...state,
        addCampaignIsLoading: false,
        success: true,
      };

    case types.ADD_CAMPAIGN_FAILURE:
      return {
        ...state,
        addCampaignIsLoading: false,
        error: true,
      };

    case types.GET_CAMPAIGN_LOAD:
      return {
        ...state,
        campaignsLoading: true,
      };

    case types.GET_CAMPAIGN_SUCCESS:
      return {
        ...state,
        campaigns: action.payload,
        campaignsLoading: false,
      };

    case types.GET_CAMPAIGN_FAILURE:
      return {
        ...state,
        campaignsLoading: false,
      };

    case types.DELETE_CAMPAIGN_LOAD:
      return {
        ...state,
        campaignIsDeleting: true,
      };

    case types.DELETE_CAMPAIGN_SUCCESS:
      const zones = state.campaigns
        ? state.campaigns.filter((c: CampaignType) => c.campaign_id !== action.payload.id)
        : state.campaigns;

      return {
        ...state,
        campaignIsDeleting: false,
        zones,
      };

    case types.DELETE_CAMPAIGN_FAILURE:
      return {
        ...state,
        zoneIsDeleting: false,
      };

    case types.ADD_CAMPAIGN_RESET:
      return {
        ...state,
        addCampaignIsLoading: false,
        error: false,
        success: false,
      };

    default:
      return state;
  }
}
