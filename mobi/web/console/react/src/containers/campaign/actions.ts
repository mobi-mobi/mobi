import { ACTION_NAMES, CampaignType } from './types';

import Api from '../../services/Api';
import config from '../../config';

const api = new Api(config.global.REACT_APP_API_URL);

type CampaignFilters = {
  limit: number;
  q?: string;
  showDisabled?: boolean;
  offset: number;
};

export function postCampaign(campaign: CampaignType, client: string, isAddingNew: boolean) {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.ADD_CAMPAIGN_LOAD });

    return api
      .post(`/${client}/campaign`, { body: JSON.stringify({ ...campaign }) })
      .then(() => {
        dispatch({ type: ACTION_NAMES.ADD_CAMPAIGN_SUCCESS });

        if (isAddingNew) {
          getCampaigns(client, {
            limit: 100,
            offset: 0,
          })(dispatch);
        }
      })
      .catch(() => {
        dispatch({ type: ACTION_NAMES.ADD_CAMPAIGN_FAILURE });
      });
  };
}

export function resetPopup() {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.ADD_CAMPAIGN_RESET });
  };
}

export function deleteCampaign(client: string, id: string) {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.DELETE_CAMPAIGN_LOAD });

    return api
      .delete(`/${client}/campaign/${id}`, {})
      .then(() => {
        dispatch({ type: ACTION_NAMES.DELETE_CAMPAIGN_SUCCESS, payload: { id } });

        getCampaigns(client, {
          limit: 100,
          offset: 0,
        })(dispatch);
      })
      .catch((error) => {
        dispatch({ type: ACTION_NAMES.DELETE_CAMPAIGN_FAILURE });
      });
  };
}

export function getCampaigns(client: string, filters: CampaignFilters) {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.GET_CAMPAIGN_LOAD });

    let url = `/${client}/campaigns/full`;

    if (filters) {
      const keys = Object.keys(filters);
      let params = '?';

      keys.forEach((key) => {
        if (key === 'showDisabled') {
          params += filters.showDisabled ? '&include_inactive=yes' : '';
        } else {
          params += filters[key] ? `&${key}=${filters[key]}` : '';
        }
      });

      url += params;
    }

    return api
      .get(url, {})
      .then((res) => {
        const { data } = res;

        dispatch({ type: ACTION_NAMES.GET_CAMPAIGN_SUCCESS, payload: data });
      })
      .catch(() => {
        dispatch({ type: ACTION_NAMES.GET_CAMPAIGN_FAILURE });
      });
  };
}
