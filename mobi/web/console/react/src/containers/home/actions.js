/* eslint-disable no-console */
import get from 'lodash/get';

import config from '../../config';
import Api from '../../services/Api';

import types from './types';

const api = new Api(config.global.REACT_APP_API_URL);

export function userInfo() {
  return (dispatch) => {
    return api
      .get('/user/info', {})
      .then((response) => {
        const userInfo = get(response, 'data');
        dispatch({
          type: types.GET_USER_INFO,
          userInfo,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
}

export function changeTime(value) {
  return (dispatch) => {
    dispatch({ type: types.SET_TIME_RANGE, timeRange: value });
  };
}

export function changeCompany(value) {
  return (dispatch) => {
    dispatch({ type: types.SET_CLIENT, client: value });
  };
}

export function getBrandsAndCategories(client) {
  return (dispatch) => {
    dispatch({ type: types.GET_BRANDS_AND_CATEGORIES });

    const queries = [
      api.get(`/${client}/catalog/brands`, {}),
      api.get(`/${client}/catalog/categories`, {}),
      api.get(`/${client}/catalog/products`, {}),
    ];

    return Promise.all(queries)
      .then((res) => {
        const [brands, categories, products] = res;

        dispatch({
          type: types.GET_BRANDS_AND_CATEGORIES_SUCCESS,
          payload: [brands.data.brands, categories.data.categories, products.data.products],
        });
      })
      .catch(() => {
        dispatch({ type: types.GET_BRANDS_AND_CATEGORIES_FAILURE });
      });
  };
}
