import types from './types';
import User from '../../classes/user';

const initialState = {
  user: null,
  client: null,
  brands: [],
  categories: [],
  timeRange: '24h',
};

export function homeReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_USER_INFO:
      const user = new User(action.userInfo);
      const selectedClient = localStorage.getItem('selectedClient');

      let client = null;

      if (selectedClient) {
        client = selectedClient;
      } else {
        client = state.client ? state.client : user.clients[0];
      }

      return {
        ...state,
        user: user,
        client: client,
      };

    case types.GET_BRANDS_AND_CATEGORIES_SUCCESS:
      return {
        ...state,
        brands: action.payload[0],
        categories: action.payload[1],
        products: action.payload[2],
      };

    case types.SET_TIME_RANGE:
      return {
        ...state,
        timeRange: action.timeRange,
      };

    case types.SET_CLIENT: {
      localStorage.setItem('selectedClient', action.client);

      return {
        ...state,
        client: action.client,
      };
    }

    default:
      return state;
  }
}
