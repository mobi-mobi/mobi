import { ACTION_NAMES } from './types';

import Api from '../../services/Api';
import config from '../../config';

const api = new Api(config.global.REACT_APP_API_URL);

export const PRODUCT_LIST_LIMIT = 21;

type ProductsFilters = {
  q?: string;
  showDisabled?: boolean;
  showOutStock?: boolean;
  offset: number;
};

export function getProducts(client: string, filters: ProductsFilters) {
  return async (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.PRODUCTS_IS_LOADING });

    let url = `/${client}/pages/catalog`;

    if (filters) {
      const keys = Object.keys(filters);
      let params = `?limit=${PRODUCT_LIST_LIMIT}`;

      keys.forEach((key) => {
        if (key === 'showDisabled') {
          params += filters.showDisabled ? '&include_inactive=yes' : '';
        } else if (key === 'showOutStock') {
          params += filters.showDisabled ? '&include_not_in_stock=yes' : '';
        } else {
          params += filters[key] ? `&${key}=${filters[key]}` : '';
        }
      });

      url += params;
    }

    try {
      const result = await api.get(url, {});
      const { metrics_single: metrics, products, total_found: totalFoundCount } = result.data;

      dispatch({ type: ACTION_NAMES.PRODUCTS_LOADED_SUCCESS, payload: products });
      dispatch({ type: ACTION_NAMES.PRODUCTS_SET_TOTAL_FOUND, payload: totalFoundCount });
      dispatch({
        type: ACTION_NAMES.PRODUCTS_SET_METRICS,
        payload: {
          productsInStock: metrics.active_and_in_stock_num,
          productsNotInStock: metrics.active_and_not_in_stock_num,
          disabledProducts: metrics.disabled_num,
        },
      });
    } catch (error) {
      dispatch({ type: ACTION_NAMES.PRODUCTS_LOADED_FAILURE });
    }
  };
}

export async function getProductsInfinite(client: string, filters: ProductsFilters) {
  let url = `/${client}/pages/catalog`;

  if (filters) {
    const keys = Object.keys(filters);
    let params = `?limit=${PRODUCT_LIST_LIMIT}`;

    keys.forEach((key) => {
      if (key === 'showDisabled') {
        params += filters.showDisabled ? '&include_inactive=yes' : '';
      } else if (key === 'showOutStock') {
        params += filters.showDisabled ? '&include_not_in_stock=yes' : '';
      } else {
        params += filters[key] ? `&${key}=${filters[key]}` : '';
      }
    });

    url += params;
  }

  try {
    const result = await api.get(url, {});
    const { metrics_single: metrics, products, total_found: totalFoundCount } = result.data;

    return { products, totalFoundCount, metrics };
  } catch (error) {}
}
