import { ACTION_NAMES as types, ProductAction, FullProductType } from './types';

type StateTypes = {
  products: Array<FullProductType>;
  productsLoading: boolean;
  metrics: {
    productsInStock: number;
    productsNotInStock: number;
    disabledProducts: number;
  };
  productFound: number | null;
};

// eslint-disable-next-line
const initialState = <StateTypes>{
  products: [],
  productsLoading: false,
  metrics: {
    productsInStock: 0,
    productsNotInStock: 0,
    disabledProducts: 0,
  },
  productFound: null,
};

export function productsReducer(state = initialState, action: ProductAction) {
  switch (action.type) {
    case types.PRODUCTS_IS_LOADING:
      return {
        ...state,
        productsLoading: true,
      };
    case types.PRODUCTS_LOADED_SUCCESS:
      return {
        ...state,
        productsLoading: false,
        products: action.payload,
      };
    case types.PRODUCTS_LOADED_FAILURE:
      return {
        ...state,
        productsLoading: false,
      };
    case types.PRODUCTS_SET_METRICS:
      return {
        ...state,
        metrics: action.payload,
      };
    case types.PRODUCTS_SET_TOTAL_FOUND:
      return {
        ...state,
        productFound: action.payload,
      };

    default:
      return state;
  }
}
