export type ProductType = {
  active: boolean;
  brand: string;
  categories: Array<string>;
  color: string;
  description: string;
  product_id: string;
  in_stock: boolean;
  is_exclusive: boolean;
  is_new: boolean;
  title: string;
  version: number;
};

export type ProductMetricsType = {
  displays: number;
  clicks: number;
  to_basket: number;
  sales: number;
};

export type FullProductType = {
  product: ProductType;
  metrics_single: ProductMetricsType;
};

export const ACTION_NAMES = {
  PRODUCTS_IS_LOADING: 'PRODUCTS_IS_LOADING',
  PRODUCTS_LOADED_SUCCESS: 'PRODUCTS_LOADED_SUCCESS',
  PRODUCTS_LOADED_FAILURE: 'PRODUCTS_LOADED_FAILURE',

  PRODUCTS_SET_METRICS: 'PRODUCTS_SET_METRICS',
  PRODUCTS_SET_TOTAL_FOUND: 'PRODUCTS_SET_TOTAL_FOUND',
};

export type ProductAction = {
  type: string;
  payload?: any;
};
