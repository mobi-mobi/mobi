import { ACTION_NAMES as types, BrandsAction, FullBrandType } from './types';

type StateTypes = {
  brands: Array<FullBrandType>;
  brandsLoading: boolean;
  brandsFound: number | null;
};

// eslint-disable-next-line
const initialState = <StateTypes>{
  brands: [],
  brandsLoading: false,
  brandsFound: null,
};

export function brandsReducer(state = initialState, action: BrandsAction) {
  switch (action.type) {
    case types.BRANDS_IS_LOADING:
      return {
        ...state,
        productsLoading: true,
      };
    case types.BRANDS_LOADED_SUCCESS:
      return {
        ...state,
        productsLoading: false,
        products: action.payload,
      };
    case types.BRANDS_LOADED_FAILURE:
      return {
        ...state,
        productsLoading: false,
      };
    case types.BRANDS_SET_TOTAL_FOUND:
      return {
        ...state,
        productFound: action.payload,
      };

    default:
      return state;
  }
}
