export type BrandType = {
  brand: string;
};

export type BrandMetricsType = {
  displays: number;
  clicks: number;
  to_basket: number;
  sales: number;
};

export type FullBrandType = {
  brand: BrandType;
  metrics_single: BrandMetricsType;
};

export const ACTION_NAMES = {
  BRANDS_IS_LOADING: 'BRANDS_IS_LOADING',
  BRANDS_LOADED_SUCCESS: 'BRANDS_LOADED_SUCCESS',
  BRANDS_LOADED_FAILURE: 'BRANDS_LOADED_FAILURE',

  BRANDS_SET_TOTAL_FOUND: 'BRANDS_SET_TOTAL_FOUND',
};

export type BrandsAction = {
  type: string;
  payload?: any;
};
