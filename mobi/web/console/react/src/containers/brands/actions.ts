import { ACTION_NAMES } from './types';

import Api from '../../services/Api';
import config from '../../config';

const api = new Api(config.global.REACT_APP_API_URL);

export const BRANDS_LIST_LIMIT = 21;

type BrandsFilters = {
  q?: string;
  offset: number;
};

export function getBrands(client: string, filters: BrandsFilters) {
  return async (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.BRANDS_IS_LOADING });

    let url = `/${client}/pages/catalog`;

    if (filters) {
      const keys = Object.keys(filters);
      let params = `?limit=${BRANDS_LIST_LIMIT}`;

      keys.forEach((key) => {
        params += filters[key] ? `&${key}=${filters[key]}` : '';
      });

      url += params;
    }

    try {
      const result = await api.get(url, {});
      const { metrics_single: metrics, products, total_found: totalFoundCount } = result.data;

      dispatch({ type: ACTION_NAMES.BRANDS_LOADED_SUCCESS, payload: products });
      dispatch({ type: ACTION_NAMES.BRANDS_SET_TOTAL_FOUND, payload: totalFoundCount });
    } catch (error) {
      dispatch({ type: ACTION_NAMES.BRANDS_LOADED_FAILURE });
    }
  };
}

export async function getBrandsInfinite(client: string, filters: BrandsFilters) {
  let url = `/${client}/pages/catalog/brands`;

  if (filters) {
    const keys = Object.keys(filters);
    let params = `?limit=${BRANDS_LIST_LIMIT}`;

    keys.forEach((key) => {
      params += filters[key] ? `&${key}=${filters[key]}` : '';
    });

    url += params;
  }

  try {
    const result = await api.get(url, {});
    const { brands, total_found: totalFoundCount } = result.data;

    return { brands, totalFoundCount };
  } catch (error) {}
}
