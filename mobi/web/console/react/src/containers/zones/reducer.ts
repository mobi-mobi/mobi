import { ACTION_NAMES as types, ZoneAction, ZoneType } from './types';

type StateTypes = {
  addZoneIsLoading: boolean;
  success: boolean;
  error: boolean;
  zonesLoading: boolean;
  zoneIsDeleting: boolean;
  zones: Array<ZoneType>;
};

// eslint-disable-next-line
const initialState = <StateTypes>{
  /** Add zone state */
  addZoneIsLoading: false,
  success: false,
  error: false,

  /** Get zones */
  zones: [],
  zonesLoading: false,
  zonesCount: 0,

  /** Delete zone */
  zoneIsDeleting: false,

  /** Updating zone */
  zoneIsUpdating: false,
};

export function zoneReducer(state = initialState, action: ZoneAction) {
  switch (action.type) {
    case types.ADD_ZONE_LOAD:
      return {
        ...state,
        addZoneIsLoading: true,
      };

    case types.ADD_ZONE_SUCCESS:
      return {
        ...state,
        addZoneIsLoading: false,
        success: true,
      };

    case types.ADD_ZONE_FAILURE:
      return {
        ...state,
        addZoneIsLoading: false,
        error: true,
      };

    case types.DELETE_ZONES_LOAD:
      return {
        ...state,
        zoneIsDeleting: true,
      };

    case types.DELETE_ZONES_SUCCESS:
      const zones = state.zones ? state.zones.filter((z: ZoneType) => z.zone_id !== action.payload.id) : state.zones;

      return {
        ...state,
        zoneIsDeleting: false,
        zones,
      };

    case types.DELETE_ZONES_FAILURE:
      return {
        ...state,
        zoneIsDeleting: false,
      };

    case types.GET_ZONES_LOAD:
      return { ...state, zonesLoading: true };

    case types.GET_ZONES_SUCCESS:
      return { ...state, zones: action.payload, zonesLoading: false };

    case types.GET_ZONES_COUNT:
      return { ...state, zonesCount: action.payload, zonesLoading: false };

    case types.UPDATE_ZONE_LOAD:
      return { ...state, zoneIsUpdating: true };

    case types.UPDATE_ZONE_SUCCESS:
      return { ...state, zoneIsUpdating: false };

    case types.UPDATE_ZONE_FAILURE:
      return { ...state, zoneIsUpdating: false };

    case types.ADD_ZONE_RESET:
      return {
        ...state,
        addZoneIsLoading: false,
        error: false,
        success: false,
      };

    default:
      return state;
  }
}
