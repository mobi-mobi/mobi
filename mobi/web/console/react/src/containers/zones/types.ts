import { CampaignType } from '../campaign/types';

export enum ZoneStatusEnum {
  ACTIVE = 'ACTIVE',
  DISABLED = 'DISABLED',
}

export enum ZoneTypeEnum {
  PERSONAL_RECOMMENDATIONS = 'PERSONAL_RECOMMENDATIONS',
  SIMILAR_PRODUCTS = 'SIMILAR_PRODUCTS',
  BASKET_RECOMMENDATIONS = 'BASKET_RECOMMENDATIONS',
  MOST_POPULAR = 'MOST_POPULAR',
}

export enum ExtraProductsPositionEnum {
  beginning = 'beginning',
  end = 'end',
  random = 'random',
  evenly = 'evenly',
}

export type ZoneSettingsType = {
  recommendations_num: number;
  same_brand?: boolean | null;
  brands?: Array<string> | null;
  categories?: Array<string> | null;
  extra_product_ids?: Array<string> | null;
  extra_products_position?: ExtraProductsPositionEnum | null;
  save_organic_extra_products_positions?: boolean | null;
  dont_show_organic_products?: boolean | null;
};

export type ZoneType = {
  zone_id: string;
  name: string;
  zone_type: ZoneTypeEnum;
  description?: string;
  status?: ZoneStatusEnum;
  campaign_ids?: Array<string>;
  zone_settings: ZoneSettingsType;
};

export type ZoneTypeChartsDataType = {
  displays: Array<Array<any>>;
  clicks: Array<Array<any>>;
  to_basket: Array<Array<any>>;
  sales: Array<Array<any>>;
};

export type ZoneTypeFull = {
  active_campaign: CampaignType | null;
  closest_campaign: CampaignType | null;
  metrics_graph: ZoneTypeChartsDataType;
  metrics_single: {
    clicks: number;
    converted: number;
    ctr: number;
    displays: number;
    sales: number;
    to_basket: number;
  };
  next_campaign: object | null;
  ordered_campaigns: [];
  zone: ZoneType;
  zone_settings_effective: object;
};

export const ACTION_NAMES = {
  ADD_ZONE_LOAD: 'ADD_ZONE_LOAD',
  ADD_ZONE_SUCCESS: 'ADD_ZONE_SUCCESS',
  ADD_ZONE_FAILURE: 'ADD_ZONE_FAILURE',
  ADD_ZONE_RESET: 'ADD_ZONE_RESET',
  GET_ZONES_COUNT: 'GET_ZONES_COUNT',

  GET_ZONES_LOAD: 'GET_ZONE_LOAD',
  GET_ZONES_SUCCESS: 'GET_ZONES_SUCCESS',
  GET_ZONES_FAILURE: 'GET_ZONES_FAILURE',

  DELETE_ZONES_LOAD: 'DELETE_ZONES_LOAD',
  DELETE_ZONES_SUCCESS: 'DELETE_ZONES_SUCCESS',
  DELETE_ZONES_FAILURE: 'DELETE_ZONES_FAILURE',

  UPDATE_ZONE_LOAD: 'UPDATE_ZONE_LOAD',
  UPDATE_ZONE_SUCCESS: 'UPDATE_ZONE_SUCCESS',
  UPDATE_ZONE_FAILURE: 'UPDATE_ZONE_FAILURE',
};

export type ZoneAction = {
  type: string;
  payload?: any;
};
