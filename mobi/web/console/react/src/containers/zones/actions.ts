import { ACTION_NAMES, ZoneType } from './types';

import Api from '../../services/Api';
import config from '../../config';

const api = new Api(config.global.REACT_APP_API_URL);

type ZonesFilters = {
  limit: number;
  q?: string;
  showDisabled?: boolean;
  offset: number;
};

export function addZone(zone: ZoneType, client: string) {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.ADD_ZONE_LOAD });

    return api
      .post(`/${client}/zone`, { body: JSON.stringify({ ...zone }) })
      .then(() => {
        dispatch({ type: ACTION_NAMES.ADD_ZONE_SUCCESS });

        getZones(client, {
          offset: 0,
          limit: 10,
        })(dispatch);
      })
      .catch(() => {
        dispatch({ type: ACTION_NAMES.ADD_ZONE_FAILURE });
      });
  };
}

export function editZoneActivity(zone: ZoneType, client: string) {
  return (dispatch: Function) => {
    return api.post(`/${client}/zone`, { body: JSON.stringify({ ...zone }) });
  };
}

export function updateZone(zone: ZoneType, client: string) {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.UPDATE_ZONE_LOAD });

    return api
      .post(`/${client}/zone`, { body: JSON.stringify({ ...zone }) })
      .then(() => {
        dispatch({ type: ACTION_NAMES.UPDATE_ZONE_SUCCESS });
      })
      .catch((error) => {
        dispatch({ type: ACTION_NAMES.UPDATE_ZONE_FAILURE });
      });
  };
}

export function resetPopup() {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.ADD_ZONE_RESET });
  };
}

export function getZones(client: string, filters: ZonesFilters) {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.GET_ZONES_LOAD });

    let url = `/${client}/zones/full`;
    let urlNum = `/${client}/zones/num`;

    if (filters) {
      const keys = Object.keys(filters);
      let params = '?';

      keys.forEach((key) => {
        if (key === 'showDisabled') {
          params += filters.showDisabled ? '&include_inactive=yes' : '';
        } else {
          params += filters[key] ? `&${key}=${filters[key]}` : '';
        }
      });

      url += params;
      urlNum += params;
    }

    return Promise.all([api.get(urlNum, {}), api.get(url, {})])
      .then((res) => {
        const { value } = res[0].data;
        const zones = res[1].data;

        dispatch({ type: ACTION_NAMES.GET_ZONES_COUNT, payload: value });
        dispatch({ type: ACTION_NAMES.GET_ZONES_SUCCESS, payload: zones });
      })
      .catch((error) => {
        dispatch({ type: ACTION_NAMES.GET_ZONES_FAILURE });
      });
  };
}

export function deleteZone(client: string, id: string) {
  return (dispatch: Function) => {
    dispatch({ type: ACTION_NAMES.DELETE_ZONES_LOAD });

    return api
      .delete(`/${client}/zone/${id}`, {})
      .then(() => {
        dispatch({ type: ACTION_NAMES.DELETE_ZONES_SUCCESS, payload: { id } });

        getZones(client, {
          offset: 0,
          limit: 5,
        })(dispatch);
      })
      .catch((error) => {
        dispatch({ type: ACTION_NAMES.DELETE_ZONES_FAILURE });
      });
  };
}

export async function checkIdExist(client: string, id: string) {
  try {
    await api.get(`/${client}/zone/${id}`, {});

    return true;
  } catch (e) {
    return false;
  }
}
