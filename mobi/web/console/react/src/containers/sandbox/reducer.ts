import { ACTION_NAMES as types } from './types';

const initialState = {
  userHistory: null,
  sandboxBrands: null,
};

export type SandboxAction = {
  type: string;
  payload?: any;
};

export function sandboxReducer(state = initialState, action: SandboxAction) {
  switch (action.type) {
    case types.SET_BRANDS:
      return {
        ...state,
        sandboxBrands: action.payload,
      };
    case types.SET_USER_HISTORY:
      return {
        ...state,
        userHistory: action.payload,
      };

    default:
      return state;
  }
}
