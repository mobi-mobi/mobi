export type UserHistoryType = {
  product_id: string;
  event_type: string;
};

export const ACTION_NAMES = {
  SET_USER_HISTORY: 'SET_USER_HISTORY',
  SET_BRANDS: 'SET_BRANDS',
};
