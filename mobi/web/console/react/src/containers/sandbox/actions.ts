import { ACTION_NAMES as types } from './types';

export function setUserHistory(userHistory: any) {
  return async (dispatch: Function) => {
    dispatch({ type: types.SET_USER_HISTORY, payload: userHistory });
  };
}

export function setSandboxBrands(brands: any) {
  return async (dispatch: Function) => {
    dispatch({ type: types.SET_BRANDS, payload: brands });
  };
}
