import React, { ReactNode } from 'react';

import SearchLine from '../SearchLine';
import CustomScroll from '../../ui/CustomScroll';

type Props = {
  children: ReactNode;
  name: string;
};

export default function Page({ children, name }: Props) {
  return (
    <>
      <div className={`page ${name}`}>
        <SearchLine />

        <CustomScroll className="page-container-scroll">
          <div className="page-container">{children}</div>
        </CustomScroll>
      </div>
    </>
  );
}
