import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import Icon from 'react-eva-icons';

import NavLinkCustom from '../NavLink';

import SearchLine from '../SearchLine/SearchLine';
import routes from '../../../routes';

import logo from '../../../assets/images/logo-mmt.svg';

import logoMobile from '../../../assets/images/logo-mmt-mobile.svg';

const LeftSidebar = () => {
  const [toggledHeaderClass, toggleHeaderClass] = useState(false);
  const [hamburgerMenu, toggleHamburger] = useState(true);

  const expanded = toggledHeaderClass ? ' expanded' : '';
  const mobile = hamburgerMenu ? '' : ' mobile';

  useEffect(() => {
    document.body.classList.toggle('expanded');
  }, [expanded]);

  return (
    <header>
      <div className={`header${expanded}${mobile}`} style={{ height: '100vh' }}>
        <div className="header-mobile">
          <div className="header-mobile__hamburger-menu" role="button" onClick={() => toggleHamburger(!hamburgerMenu)}>
            <Icon name="menu" size="large" fill="#feaf55" />
          </div>
          <SearchLine showCompany={false} />
        </div>
        <div className="header-main__menu">
          <div className="header-mobile__hamburger-menu" role="button" onClick={() => toggleHamburger(!hamburgerMenu)}>
            <Icon name="menu" size="large" fill="#feaf55" />
          </div>
          <SearchLine showLoggedUser={false} />
          <NavLink className="header__logo" to={{ pathname: '/home' }} onClick={() => toggleHamburger(!hamburgerMenu)}>
            {!toggledHeaderClass ? <img src={logo} alt="logo" /> : <img src={logoMobile} alt="logo" />}
          </NavLink>

          <nav className="header__navigation">
            {routes.map((route) => (
              <NavLinkCustom
                key={route.name}
                route={{
                  path: route.path,
                  name: route.name,
                  icon: route.icon,
                  sub: route?.sub,
                }}
                hamburgerMenu={hamburgerMenu}
                toggleHamburger={toggleHamburger}
                expanded={expanded}
              />
            ))}
          </nav>
        </div>
        <span
          className={`expand-icon ${toggledHeaderClass ? 'reversed' : ''}`}
          onClick={() => toggleHeaderClass(!toggledHeaderClass)}
        >
          <Icon name="arrowhead-left" size="medium" />
        </span>
      </div>
    </header>
  );
};

export default LeftSidebar;
