import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import LoggedUser from '../../home/LoggedUser';
import CompanyList from '../../home/CompanyList';

class SearchLine extends Component {
  state = {};

  render() {
    const { showCompany = true, showLoggedUser = true } = this.props;

    return (
      <div className="search-line">
        {showCompany && (
          <div className="search-line__search">
            <CompanyList />
          </div>
        )}
        {showLoggedUser && <LoggedUser />}
      </div>
    );
  }
}

SearchLine.propTypes = {
  history: PropTypes.object,
};

export default withRouter(connect()(SearchLine));
