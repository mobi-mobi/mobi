import React, { useState, useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import { Transition, Icon } from 'semantic-ui-react';

import { RouteType } from '../../../routes';

type Props = {
  route: RouteType;
  hamburgerMenu: boolean;
  expanded: string;
  toggleHamburger: Function;
};

const replaceUrlNameSymbols = (name: string) => name.replace(/_/g, '-');

export default function CustomNavLink({ route, hamburgerMenu, expanded, toggleHamburger }: Props) {
  const { pathname } = useLocation();
  const [isExpanded, setIsExpanded] = useState<boolean>(pathname.includes(replaceUrlNameSymbols(route.name)));
  const { t } = useTranslation();

  const handleExpand = useCallback(() => {
    setIsExpanded(!isExpanded);
  }, [isExpanded]);

  useEffect(() => {
    setIsExpanded(pathname.includes(replaceUrlNameSymbols(route.name)));
  }, [route, pathname]);

  return route?.sub?.length ? (
    <div className="header-link--has-submenu">
      <div
        className={`header__link header__link__toggler ${isExpanded ? 'header__link__toggler--expanded' : ''} `}
        onClick={handleExpand}
      >
        {route.icon && <Icon name={route.icon} size="large" />}
        <Transition animation="fade up" duration={500} visible={expanded === ''}>
          <span>{t(route.name)}</span>
        </Transition>
      </div>
      {isExpanded && (
        <div className="header-link__links">
          {route.sub.map((r: RouteType) => {
            return (
              <CustomNavLink
                key={r.path}
                route={{
                  path: r.path,
                  name: r.name,
                  icon: r.icon,
                  sub: r?.sub,
                }}
                hamburgerMenu={hamburgerMenu}
                toggleHamburger={toggleHamburger}
                expanded={expanded}
              />
            );
          })}
        </div>
      )}
    </div>
  ) : (
    <NavLink
      activeClassName="active"
      className="header__link"
      to={{ pathname: route.path }}
      onClick={() => toggleHamburger(!hamburgerMenu)}
    >
      {route.icon && <Icon name={route.icon} size="large" />}
      <Transition animation="fade up" duration={500} visible={expanded === ''}>
        <span>{t(route.name)}</span>
      </Transition>
    </NavLink>
  );
}
