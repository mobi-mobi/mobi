import React, { ReactNode } from 'react';
import Masonry from 'react-masonry-css';

type Props = {
  children: ReactNode;
};


export default function CustomMasonry({ children }: Props) {
  return (
    <Masonry
      breakpointCols={{
        default: 4,
        1100: 3,
        992: 2,
        500: 1,
      }}
      className="my-masonry-grid"
      columnClassName="my-masonry-grid_column"
    >
      {children}
    </Masonry>
  );
}
