import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Card, Icon } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';

import Api from '../../../services/Api';

import config from '../../../config';

type Props = {
  name: string;
  api: string;
  dataPath: string;
  timeRange: string;
};

type DataResponse = {
  value: number;
};

/** TOOD: Singleton for api */
const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function MetricCard({ name, api, dataPath, timeRange }: Props) {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [data, setData] = useState<DataResponse | null>(null);
  const client: string = useSelector((state: any) => state.home.client);
  const { t } = useTranslation();

  useEffect(() => {
    const url = api.replace('<client>', client).replace('<timeRange>', timeRange);
    setIsLoaded(false);

    apiInstance
      .get(url, {})
      .then((response: any) => {
        const { data } = response;

        setIsLoaded(true);
        setData(data);
      })
      .catch((error: any) => {
        console.log(error);
        setIsLoaded(true);

        return NaN;
      });
  }, [api, timeRange, client]);

  return data && isLoaded ? (
    <Card
      header={t(name)}
      description={data.value.toLocaleString('en', {
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
      })}
    />
  ) : (
    <Card
      className="preload-card"
      header={t(name)}
      description={
        <div className="description">
          <Icon loading name="spinner" />
        </div>
      }
    />
  );
}
