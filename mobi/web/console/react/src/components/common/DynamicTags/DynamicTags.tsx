import React, { useState, useCallback, useEffect, useMemo, useRef } from 'react';
import ReactTags from 'react-tag-autocomplete';
import { useSelector } from 'react-redux';

import useDebounce from '../../../hooks/useDebounce';

import LoadingSpinner from '../../ui/LoadingSpinner';

import Api from '../../../services/Api';
import config from '../../../config';

import './styles.scss';

type Props = {
  type: string;
  placeholder: string;
  initialData?: Array<any>;
  onChange: Function;
  mapping?: Function;
};

type Tag = {
  name: string;
  id: number;
};

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function DynamicTags({ type, onChange, initialData, placeholder, mapping }: Props) {
  const client = useSelector((state: any) => state.home.client);
  const [suggestions, setSuggestions] = useState<Array<Tag>>([]);
  const [value, setValue] = useState<Array<any>>(initialData || []);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [search, setSearch] = useState<string>('');
  const isInitialMount = useRef(true);

  const debouncedSearchTerm = useDebounce(search, 300);

  useEffect(() => {
    if (debouncedSearchTerm.length > 0) {
      (async function getItems() {
        setIsLoading(true);
        const { data } = await apiInstance.get(`/${client}/catalog/${type}?q=${debouncedSearchTerm}`, {});

        const formattedData = mapping
          ? data[type].map(mapping)
          : data[type].map((name: string, id: number) => ({ id, name }));

        setSuggestions(formattedData);
        setIsLoading(false);
      })();
    }
  }, [debouncedSearchTerm, type, client, mapping]);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      if (onChange) {
        onChange(
          type,
          value.map(({ name }) => name)
        );
      }
    }
  }, [type, value]);

  const filteredSuggestion = useMemo(() => {
    return suggestions.filter((tag: Tag) => {
      const names = value.map((savedTags: Tag) => savedTags.name);

      return !names.includes(tag.name);
    });
  }, [suggestions]);

  const handleRemoveTags = useCallback(
    (index) => {
      const removed = [...value];

      removed.splice(index, 1);

      setValue(removed);
    },
    [value]
  );

  const handleAddTags = useCallback(
    (tag: Tag) => {
      setValue([...value, tag]);
    },
    [value]
  );

  const handleValidate = useCallback(
    (tag: Tag) => {
      const isDuplicate = Boolean(value.find((el: Tag) => el.name === tag.name));

      return tag.name.length >= 1 && !isDuplicate;
    },
    [value]
  );

  const handleChangeSearchValue = useCallback((value: string) => {
    setSearch(value);
  }, []);

  return (
    <div className="dynamic-tags">
      <ReactTags
        ref={isInitialMount}
        placeholderText={placeholder}
        tags={value}
        suggestions={filteredSuggestion}
        onInput={handleChangeSearchValue}
        onDelete={(index: any) => handleRemoveTags(index)}
        onAddition={(tag: any) => handleAddTags(tag)}
        onValidate={handleValidate}
      />

      {isLoading && <LoadingSpinner size="tiny" />}
    </div>
  );
}
