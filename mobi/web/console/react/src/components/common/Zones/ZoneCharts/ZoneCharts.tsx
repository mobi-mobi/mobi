import React, { useMemo, useState, useCallback } from 'react';
import { Chart } from 'react-google-charts';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import TimeRangeSelector from '../../../home/TimeList';

import { ZoneTypeChartsDataType } from '../../../../containers/zones/types';

import DataFormatter from '../../../../classes/DataFormatter';

import Api from '../../../../services/Api';
import config from '../../../../config';

import './styles.scss';

type Props = {
  id: String | undefined;
  data: ZoneTypeChartsDataType;
  type: String;
};

const timeMap = {
  '1d': '24h',
  '28d': '28d',
  '1h': '60m',
};

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function ZoneCharts({ id, data, type }: Props) {
  const { t } = useTranslation();
  const client = useSelector((state: any) => state.home.client);
  const [timeRange, setTimeRange] = useState('60m');
  const [localData, setLocalData] = useState(data);

  const handleTimeRangeChange = useCallback(
    async (val) => {
      const time = timeMap[val];
      setTimeRange(time);

      const request = await apiInstance.get(`/${client}/${type}/${id}/metrics?metrics_from=${time}`, {});

      setLocalData(request.data.metrics_graph);
    },
    [client, id, type]
  );

  const charts = useMemo(() => {
    const keys = Object.keys(localData);

    return keys.map((key) => {
      const d = localData[key];

      const format = DataFormatter.preformatDataTime([...d], timeRange);

      return (
        <div className="fluid-card-more-content-charts__item" key={key}>
          <div className="fluid-card-more-content-charts__item-title">{t(`zone_chart_item_${key}`)}:</div>
          <Chart
            data={[[t('time'), t(`zone_charts_${key}_value`)], ...format]}
            width="100%"
            height="350px"
            chartType="LineChart"
            options={{
              vAxis: {
                viewWindow: {
                  min: 0,
                },
              },
              theme: 'maximized',
              series: {
                0: { targetAxisIndex: 0, curveType: 'function', pointSize: 2 },
              },
              legend: 'none',
              colors: ['#6625ab', '#feaf55'],
              animation: {
                startup: true,
                easing: 'linear',
                duration: 1000,
              },
            }}
          />
        </div>
      );
    });
  }, [localData, timeRange, t]);

  if (!id) return null;

  return (
    <div className="fluid-card-more-content-charts">
      <div className="fluid-card-more-content-charts-header">
        <TimeRangeSelector
          isLocalChange={true}
          onLocalTimeChange={handleTimeRangeChange}
          customTimeRange={['1h', '1d', '28d']}
          localValue={timeRange}
        />
      </div>

      <div className="fluid-card-more-content-charts-list">{charts}</div>
    </div>
  );
}
