import React, { useState, useCallback, useMemo } from 'react';
import { Button, Popup, Modal } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import Icon from 'react-eva-icons';

import { ZoneType, ZoneTypeFull, ZoneStatusEnum } from '../../../../containers/zones/types';
import { deleteZone, editZoneActivity } from '../../../../containers/zones/actions';
import { CampaignTypeFull } from '../../../../containers/campaign/types';

import ZoneCharts from '../ZoneCharts';
import ZoneSettings from '../Settings';
import CampaignsList from '../CampaignsList';

type Props = {
  fullZone: ZoneTypeFull;
};

type Store = {
  client: string;
  zoneIsDeleting: boolean;
  campaigns: Array<CampaignTypeFull>;
};

export default function ZoneItem({ fullZone }: Props) {
  const { t } = useTranslation();
  const { zone } = fullZone;
  const [expanded, setExpanded] = useState<boolean>(false);
  const [showDeleteModal, setShowDeleteModal] = useState<boolean>(false);
  const [showActivityModal, setShowActivityModal] = useState<boolean>(false);
  const dispatch = useDispatch();
  const store: Store = useSelector((state: any) => ({
    client: state.home.client,
    campaigns: state.campaign.campaigns,
    zoneIsDeleting: state.zones.zoneIsDeleting,
  }));

  const handleChangeActivity = useCallback(
    (status: ZoneStatusEnum) => {
      if (zone && zone.zone_id) {
        const editedZone: ZoneType = {
          ...zone,
          status,
        };

        editZoneActivity(editedZone, store.client)(dispatch);
        setShowActivityModal(false);
      }
    },
    [zone, store, dispatch]
  );

  const handleMoreClick = useCallback(() => {
    setExpanded(!expanded);
  }, [expanded]);

  const handleDeleteZone = useCallback(
    (id: string | undefined) => {
      if (!id) return;

      deleteZone(store.client, id)(dispatch);
    },
    [dispatch, store.client]
  );

  const copyId = useCallback((id) => {
    const input = document.createElement('input');

    input.setAttribute('value', id);
    document.body.appendChild(input);
    input.select();

    document.execCommand('copy');
    document.body.removeChild(input);
  }, []);

  const campaign = useMemo(() => {
    const { active_campaign: activeCampaign, closest_campaign: closestCampaign } = fullZone;

    if (!activeCampaign && !closestCampaign) {
      return (
        <div className="fluid-card-information__next-campaign">
          {t('zone_next_campaign')}: {t('zone_not_scheduled')}
        </div>
      );
    }

    return activeCampaign ? (
      <div className="fluid-card-information__next-campaign">
        {t('zone_current_campaign')}: {activeCampaign.name}
      </div>
    ) : (
      <div className="fluid-card-information__next-campaign">
        {t('zone_next_campaign')}: {closestCampaign?.name}
      </div>
    );
  }, [fullZone, t]);

  if (!zone) return null;

  return (
    <div className="fluid-card">
      <div className="fluid-card-preview">
        <div className="fluid-card-information">
          <div className="fluid-card-information__id">
            <Popup
              content={t('zone_item_copy_id')}
              inverted
              position="right center"
              trigger={
                <span className="id" onClick={() => copyId(zone.zone_id)}>
                  id: {zone.zone_id}
                  <Icon name="copy-outline" size="small" fill="#777" />
                </span>
              }
            />
          </div>
          <div className="fluid-card-information__name" onClick={handleMoreClick}>
            {zone.name}
          </div>
          {campaign}
        </div>
        <div className="fluid-card-statistic">
          <div className="fluid-card-statistic">
            <strong>{t('zone_displays')}:</strong> {fullZone.metrics_single.displays}
          </div>
          <div className="fluid-card-statistic">
            <strong>CTR:</strong> {fullZone.metrics_single.ctr}%
          </div>
          <div className="fluid-card-statistic">
            <strong>{t('zone_converted')}:</strong> {fullZone.metrics_single.converted}%
          </div>
        </div>
        <div className="fluid-card-actions">
          <Button basic primary content={expanded ? t('less') : t('more')} size="tiny" onClick={handleMoreClick} />
        </div>
      </div>
      {expanded && (
        <div className="fluid-card-more-content">
          <div className="fluid-card-more-content-columns">
            <div className="fluid-card-more-content-columns__column">
              <ZoneCharts type="zone" id={fullZone.zone.zone_id} data={fullZone.metrics_graph} />
            </div>
            <div className="fluid-card-more-content-columns__column">
              <div className="fluid-card-more-content__additional-content">
                <div className="fluid-card-more-content__fluid-card-type">
                  <strong>{t('zone_type')}:</strong> {t(zone.zone_type)}
                </div>
                <div className="fluid-card-more-content__description">{zone.description}</div>
                <ZoneSettings zone={zone} />
                <CampaignsList campaigns={store.campaigns} zone={zone} />
              </div>
              <div className="fluid-card-more-actions">
                <div className="fluid-card-more-actions__toggle">
                  <Modal
                    open={showActivityModal}
                    onClose={() => setShowActivityModal(false)}
                    size="mini"
                    trigger={
                      <Button
                        onClick={() => setShowActivityModal(true)}
                        color={zone.status && zone.status === ZoneStatusEnum.ACTIVE ? 'red' : 'green'}
                      >
                        {zone.status && zone.status === ZoneStatusEnum.ACTIVE
                          ? t('zone_toggle_activity_deactivate')
                          : t('zone_toggle_activity_activate')}
                      </Button>
                    }
                  >
                    <Modal.Header>{t('zone_prompt_confirm_action')}</Modal.Header>
                    <Modal.Content>
                      <p
                        dangerouslySetInnerHTML={{
                          __html:
                            zone.status && zone.status === ZoneStatusEnum.ACTIVE
                              ? t('zone_prompt_deactivate_body')
                              : t('zone_prompt_activate_body'),
                        }}
                      />
                    </Modal.Content>
                    <Modal.Actions>
                      <Button onClick={() => setShowActivityModal(false)}>{t('cancel')}</Button>
                      <Button
                        onClick={() =>
                          handleChangeActivity(
                            zone.status === ZoneStatusEnum.ACTIVE ? ZoneStatusEnum.DISABLED : ZoneStatusEnum.ACTIVE
                          )
                        }
                        color={zone.status && zone.status === ZoneStatusEnum.ACTIVE ? 'red' : 'green'}
                      >
                        {zone.status && zone.status === ZoneStatusEnum.ACTIVE
                          ? t('zone_toggle_activity_deactivate')
                          : t('zone_toggle_activity_activate')}
                      </Button>
                    </Modal.Actions>
                  </Modal>
                </div>

                <div className="fluid-card-more-actions__delete">
                  <Modal
                    open={showDeleteModal}
                    onClose={() => setShowDeleteModal(false)}
                    size="mini"
                    trigger={
                      <Button onClick={() => setShowDeleteModal(true)} color="red">
                        {t('zone_delete')}
                      </Button>
                    }
                  >
                    <Modal.Header>{t('zone_prompt_delete_title')}</Modal.Header>
                    <Modal.Content>
                      <p dangerouslySetInnerHTML={{ __html: t('zone_prompt_delete_body') }} />
                    </Modal.Content>
                    <Modal.Actions>
                      <Button onClick={() => setShowDeleteModal(false)}>{t('cancel')}</Button>
                      <Button onClick={() => handleDeleteZone(zone.zone_id)} color="red">
                        {t('zone_prompt_delete_button_text')}
                      </Button>
                    </Modal.Actions>
                  </Modal>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
