import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

import Empty from '../Empty';
import ZoneItem from './ZoneItem';

import LoadingSpinner from '../../ui/LoadingSpinner';

import { ZoneTypeFull } from '../../../containers/zones/types';

type Props = {
  items: Array<ZoneTypeFull>;
  emptyText: string;
};

export default function Zones({ items, emptyText }: Props) {
  const state = useSelector((state: any) => state.zones);

  const renderItems = useMemo(() => {
    if (state.zonesLoading) {
      return <LoadingSpinner relative size="small" />;
    } else if (items && !items.length) {
      return <Empty message={emptyText}></Empty>;
    } else {
      return items && items.map((item: ZoneTypeFull) => <ZoneItem key={item?.zone.zone_id} fullZone={item} />);
    }
  }, [items, state, emptyText]);

  return <div className="fluid-cards-list-items">{renderItems}</div>;
}
