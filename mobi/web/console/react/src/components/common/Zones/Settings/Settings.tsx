import React, { useMemo, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { Checkbox, Input, Select, Button } from 'semantic-ui-react';
import Icon from 'react-eva-icons';

import NumberField from '../../NumberField';
import DynamicTags from '../../DynamicTags';
import SelectSearch from '../../SearchSelect';

import { ZoneType, ExtraProductsPositionEnum, ZoneTypeEnum } from '../../../../containers/zones/types';
import { updateZone } from '../../../../containers/zones/actions';

import './styles.scss';

type Props = {
  zone: ZoneType;
};

type SettingsFieldType = { input: string; type?: string; name?: string; show: Function; mapping: Function };

const SETTINGS_FIELD_MAP = {
  recommendations_num: {
    show: (zone?: ZoneType) => true,
    input: 'text',
    type: 'number',
  },
  same_brand: {
    input: 'checkbox',
    show: (zone?: ZoneType) => zone?.zone_type === ZoneTypeEnum.SIMILAR_PRODUCTS,
  },
  brands: {
    show: (zone?: ZoneType) => true,
    input: 'tags',
    name: 'brands',
  },
  categories: {
    show: (zone?: ZoneType) => true,
    input: 'tags',
    name: 'categories',
  },
  extra_product_ids: {
    show: () => true,
    input: 'select',
    type: 'search',
  },
  extra_products_position: {
    show: () => true,
    input: 'select',
    type: 'default',
  },
  save_organic_extra_products_positions: {
    show: (zone?: ZoneType) => zone?.zone_settings.extra_product_ids?.length,
    input: 'checkbox',
  },
  dont_show_organic_products: {
    show: (zone?: ZoneType) => zone?.zone_settings.extra_product_ids?.length,
    input: 'checkbox',
  },
};

export default function Settings({ zone }: Props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [localZone, setLocalZone] = useState<ZoneType>(zone);
  const [editableFields, setEditableFields] = useState({});
  const { zoneIsUpdating, client } = useSelector((state: any) => ({
    zoneIsUpdating: state.zones.zoneIsUpdating,
    client: state.home.client,
  }));

  useEffect(() => {
    updateZone(localZone, client)(dispatch);
  }, [localZone, client, dispatch]);

  const applyChanges = useCallback(
    (key) => {
      setLocalZone((prevLocalZone) => ({
        ...prevLocalZone,
        zone_settings: {
          ...prevLocalZone.zone_settings,
          [key]: key === 'recommendations_num' ? parseInt(editableFields[key].next, 10) : editableFields[key].next,
        },
      }));

      setEditableFields((prevState) => ({
        ...prevState,
        [key]: null,
      }));
    },
    [editableFields]
  );

  const discardChanges = useCallback((key) => {
    setEditableFields((prevState) => ({
      ...prevState,
      [key]: null,
    }));
  }, []);

  const changeValue = useCallback(
    (key: string, value: any) => {
      setEditableFields((prevState) => ({
        ...prevState,
        [key]: {
          prev: localZone.zone_settings[key],
          next: value,
        },
      }));
    },
    [localZone]
  );

  const handleAddToEdit = useCallback(
    (key) => {
      setEditableFields((prevState) => ({
        ...prevState,
        [key]: {
          prev: localZone.zone_settings[key],
          next: localZone.zone_settings[key],
        },
      }));
    },
    [localZone]
  );

  /** @TODO: move to component */
  const renderSettingField = useCallback(
    (field: SettingsFieldType, key: string) => {
      const value = !Boolean(editableFields[key]?.next) ? '' : editableFields[key].next;
      let label = !Boolean(localZone.zone_settings[key]) ? 'N/A' : localZone.zone_settings[key];

      if (Array.isArray(label)) {
        label = !label.length ? 'N/A' : label.join(', ');
      }

      if (!field.show(zone)) {
        return null;
      }

      if (field.input === 'text') {
        if (field.type === 'number') {
          return (
            <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
              <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
                {label} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <NumberField
                  name={key}
                  InputProps={{
                    value,
                    pattern: '[0-9]+',
                    min: '1',
                  }}
                  onChange={(name: string, val: number) => {
                    changeValue(key, val);
                  }}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          );
        } else {
          return (
            <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
              <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
                {label} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <Input
                  placeholder={t('value')}
                  type={field.type}
                  value={value}
                  onChange={(e) => changeValue(key, e.currentTarget.value)}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          );
        }
      } else if (field.input === 'tags' && field.name) {
        const initialData = localZone.zone_settings[key]?.map((v: string, id: number) => ({ name: v, id }));

        return (
          <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
            <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
              {label} <Icon name="edit-outline" size="small" fill="#777" />
            </div>

            <div className="editable-static-field__input">
              <DynamicTags
                type={field.name}
                placeholder={t('value')}
                initialData={initialData || []}
                onChange={(name: any, value: any) => {
                  changeValue(key, value);
                }}
                mapping={field?.mapping}
              />
              <div className="editable-static-field__input-actions">
                <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                  <Icon name="checkmark-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                </Button>
                <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                  <Icon name="close-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                </Button>
              </div>
            </div>
          </div>
        );
      } else if (field.input === 'select') {
        if (field.type === 'search') {
          return (
            <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
              <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
                {label} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <SelectSearch
                  onChange={(name: string, ids: Array<string>) => changeValue(key, ids)}
                  name={key}
                  placeholder={t(key)}
                  initialValue={value}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          );
        } else {
          return (
            <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
              <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
                {t(label)} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <Select
                  placeholder={t('value')}
                  onChange={(e, data) => changeValue(key, data.value)}
                  options={[
                    {
                      key: ExtraProductsPositionEnum.beginning,
                      value: ExtraProductsPositionEnum.beginning,
                      text: t(ExtraProductsPositionEnum.beginning),
                    },
                    {
                      key: ExtraProductsPositionEnum.end,
                      value: ExtraProductsPositionEnum.end,
                      text: t(ExtraProductsPositionEnum.end),
                    },
                    {
                      key: ExtraProductsPositionEnum.evenly,
                      value: ExtraProductsPositionEnum.evenly,
                      text: t(ExtraProductsPositionEnum.evenly),
                    },
                    {
                      key: ExtraProductsPositionEnum.random,
                      value: ExtraProductsPositionEnum.random,
                      text: t(ExtraProductsPositionEnum.random),
                    },
                  ]}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          );
        }
      } else if (field.input === 'checkbox') {
        return (
          <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
            <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
              {Boolean(localZone.zone_settings[key]) ? t('enabled') : t('disabled')}{' '}
              <Icon name="edit-outline" size="small" fill="#777" />
            </div>
            <div className="editable-static-field__input">
              <Checkbox
                defaultChecked={Boolean(localZone.zone_settings[key])}
                onChange={() => changeValue(key, !Boolean(localZone.zone_settings[key]))}
              />
              <div className="editable-static-field__input-actions">
                <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                  <Icon name="checkmark-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                </Button>
                <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                  <Icon name="close-outline" size="medium" fill="#fff" loading={zoneIsUpdating} />
                </Button>
              </div>
            </div>
          </div>
        );
      }
    },
    [t, localZone, applyChanges, zone, changeValue, discardChanges, zoneIsUpdating, editableFields, handleAddToEdit]
  );

  const zoneSettings = useMemo(() => {
    const keys = Object.keys(zone.zone_settings);

    const field = (key: string) => renderSettingField(SETTINGS_FIELD_MAP[key], key);

    return keys
      .map((key) => {
        const show = field(key);

        return (
          show && (
            <div className="fluid-card-setting" key={key}>
              <div className="fluid-card-setting__name">{t(key)}:</div>
              <div className="fluid-card-setting__value">{field(key)}</div>
            </div>
          )
        );
      })
      .filter(Boolean);
  }, [zone, renderSettingField, t]);

  return (
    <div className="fluid-card-more-content__fluid-card-settings">
      <div className="fluid-card-more-content__fluid-card-settings-container">{zoneSettings}</div>
    </div>
  );
}
