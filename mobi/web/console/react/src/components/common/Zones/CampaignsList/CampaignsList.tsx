import React, { useEffect, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from 'semantic-ui-react';
import Autosuggest from 'react-autosuggest';

import { CampaignStatusEnum, CampaignTypeFull } from '../../../../containers/campaign/types';

import Api from '../../../../services/Api';

import config from '../../../../config';

import { ZoneType } from '../../../../containers/zones/types';
import { getCampaigns } from '../../../../containers/campaign/actions';

import './styles.scss';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

type Props = {
  campaigns: Array<CampaignTypeFull>;
  zone: ZoneType;
};

type Store = {
  client: string;
  campaigns: Array<CampaignTypeFull>;
};

export default function CampaignsList({ campaigns, zone }: Props) {
  const dispatch = useDispatch();
  const [assignedCampaigns, setAssignedCampaigns] = useState<Array<CampaignTypeFull>>([]);
  const [suggestions, setSuggestion] = useState<Array<CampaignTypeFull>>([]);
  const [suggestionsValue, setSuggestionValue] = useState<string>('');
  const [showAddForm, setShowAddForm] = useState<boolean>(false);
  const { t } = useTranslation();
  const store: Store = useSelector((state: any) => ({
    client: state.home.client,
    campaigns: state.campaign.campaigns,
  }));

  const getZoneCampaigns = useCallback(async () => {
    const items = await apiInstance.get(`/${store.client}/zone/${zone.zone_id}/campaigns`, {});

    setAssignedCampaigns(items.data);
  }, [store, zone]);

  useEffect(() => {
    (async function getLocalCampaigns() {
      await getZoneCampaigns();
      await getCampaigns(store.client, {
        offset: 0,
        limit: 100,
      })(dispatch);
    })();
    // eslint-disable-next-line
  }, [store.client, dispatch, zone.zone_id]);

  const handleOpenAddForm = useCallback(() => {
    setShowAddForm(true);
  }, []);

  const handleCloseAddForm = useCallback(() => {
    setShowAddForm(false);
    setSuggestionValue('');
  }, []);

  const escapeRegexCharacters = (str: string) => {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  };

  const getSuggestionValue = (suggestion: CampaignTypeFull) => {
    return suggestion.campaign.name;
  };

  const getSuggestions = (campaigns: Array<CampaignTypeFull>, value: string) => {
    const escapedValue = escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
      return [];
    }

    const regex = new RegExp('^' + escapedValue, 'i');

    return campaigns.filter(
      (campaign) => campaign.campaign.status === CampaignStatusEnum.ACTIVE && regex.test(campaign.campaign.name)
    );
  };

  const handleRemoveCampaign = useCallback(
    async (id: string | undefined) => {
      if (id) {
        await apiInstance
          .delete(`/${store.client}/zone/${zone.zone_id}/remove_campaign/${id}`, {})
          .catch(console.error);

        await getZoneCampaigns();
        setSuggestionValue('');
        setShowAddForm(false);
      }
    },
    [zone, store, getZoneCampaigns]
  );

  const handleAddCampaign = useCallback(async () => {
    const id = store.campaigns.find((campaign: CampaignTypeFull) => campaign.campaign.name === suggestionsValue)
      ?.campaign?.campaign_id;

    if (id) {
      await apiInstance.post(`/${store.client}/zone/${zone.zone_id}/add_campaign/${id}`, {}).catch(console.error);

      setShowAddForm(false);
      await getZoneCampaigns();
    }
  }, [zone, store, suggestionsValue, getZoneCampaigns]);

  const onChange = (event: any, { newValue, method }: any) => {
    setSuggestionValue(newValue);
  };

  const onSuggestionsFetchRequested = ({ value }: any) => {
    setSuggestion(getSuggestions(campaigns, value));
  };

  const onSuggestionsClearRequested = () => {
    setSuggestion([]);
  };

  return (
    <div className="campaign-list">
      <h3 className="campaign-list__title">{t('campaigns')}</h3>
      <div className="campaign-list__items">
        {assignedCampaigns.length ? (
          assignedCampaigns.map((campaign: any) => (
            <div key={campaign.campaign_id} className="campaign-list-item">
              <div className="campaign-list-item__name">{campaign.name}</div>
              <div className="campaign-list-item__action">
                <Button color="red" onClick={() => handleRemoveCampaign(campaign.campaign_id)}>
                  {t('detach_campaign')}
                </Button>
              </div>
            </div>
          ))
        ) : (
          <div>{t('no_assigned_campaigns')}</div>
        )}
      </div>
      <h3 className="campaign-list__add">
        {!showAddForm && (
          <Button onClick={handleOpenAddForm} color="green">
            {t('attach_campaign')}
          </Button>
        )}
        {showAddForm && (
          <div className="campaign-list__add-form">
            <Autosuggest
              suggestions={suggestions}
              onSuggestionsFetchRequested={onSuggestionsFetchRequested}
              onSuggestionsClearRequested={onSuggestionsClearRequested}
              getSuggestionValue={getSuggestionValue}
              renderSuggestion={(suggestion: CampaignTypeFull) => (
                <div className="suggestion">{suggestion.campaign.name}</div>
              )}
              inputProps={{
                placeholder: t('search_campaign'),
                value: suggestionsValue,
                onChange: onChange,
              }}
            />
            <div className="campaign-list__add-form__actions">
              <Button onClick={handleAddCampaign} color="green">
                {t('add')}
              </Button>
              <Button onClick={handleCloseAddForm}>{t('cancel')}</Button>
            </div>
          </div>
        )}
      </h3>
    </div>
  );
}
