import React from 'react';
import Icon from 'react-eva-icons';
import { useTranslation } from 'react-i18next';

import './styles.scss';

type Props = {
  message: string;
};

export default function Empty({ message }: Props) {
  const { t } = useTranslation();

  return (
    <div className="empty">
      <Icon size="xlarge" fill="#6725ea" name="message-circle-outline" />
      <span className="empty__text">{t(message)}</span>
    </div>
  );
}
