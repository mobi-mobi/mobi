import React, { useState, useEffect, useRef, useCallback, useMemo } from 'react';
import { useSelector } from 'react-redux';
import SelectSearch, { SelectSearchOption } from 'react-select-search';

import { ProductType } from '../../../containers/products/types';

type Props = {
  onChange: Function;
  name: string;
  placeholder: string;
  initialValue?: Array<string>;
};

export default function SearchSelect({ onChange, name, placeholder, initialValue }: Props) {
  const [value, setValue] = useState<Array<string>>([]);
  const isInitialMount = useRef(true);
  const products = useSelector((state: any) => state.home.products);

  useEffect(() => {
    if (initialValue) {
      setValue(initialValue);
    }
  }, [initialValue]);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      onChange(name, value);
    }
  }, [name, value]);

  const handleChangeValue = useCallback((val) => {
    setValue(val);
  }, []);

  const productsItems = useMemo((): Array<SelectSearchOption> => {
    return products.map((product: ProductType) => ({ value: product.product_id, name: product.title }));
  }, [products]);

  return (
    <SelectSearch
      options={productsItems}
      value={value}
      onChange={(ids) => handleChangeValue(ids)}
      multiple
      search
      placeholder={placeholder}
      printOptions="on-focus"
    />
  );
}
