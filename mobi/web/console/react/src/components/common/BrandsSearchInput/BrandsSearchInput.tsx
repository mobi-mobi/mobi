import React, { useEffect, useCallback, useRef, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Button, Input } from 'semantic-ui-react';

import { CampaignTypeFull } from '../../../containers/campaign/types';
import useDebounce from '../../../hooks/useDebounce';

import Api from '../../../services/Api';

import config from '../../../config';

import './styles.scss';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

type Store = {
  client: string;
  campaigns: Array<CampaignTypeFull>;
  brands: Array<any>;
};

type Props = {
  onChange: Function;
  max: number;
};

export type SelectedBrandType = {
  value: string;
  id: number;
};

export default function BrandsSearchInput({ max, onChange }: Props) {
  const [showAddForm, setShowAddForm] = useState<boolean>(false);
  const [suggestions, setSuggestions] = useState<Array<string>>([]);
  const [selectedItems, setSelectedItems] = useState<Array<SelectedBrandType>>([]);
  const [value, setValue] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { t } = useTranslation();
  const isInitialMount = useRef(true);
  const store: Store = useSelector((state: any) => ({
    client: state.home.client,
    campaigns: state.campaign.campaigns,
    brands: state.sandbox.sandboxBrands,
  }));

  const debouncedSearchTerm = useDebounce(value, 300);

  useEffect(() => {
    setSelectedItems(store.brands || []);
  }, [store.brands])

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      if (onChange) {
        onChange(selectedItems);
      }
    }
  }, [selectedItems, onChange]);

  useEffect(() => {
    (async function get() {
      if (debouncedSearchTerm) {
        setIsLoading(true);

        const { data } = await apiInstance.get(`/${store.client}/catalog/brands?q=${debouncedSearchTerm}`, {});
        setSuggestions(data.brands);

        setIsLoading(false);
      }
    })();
  }, [store.client, debouncedSearchTerm]);

  const filteredSuggestions = useMemo(() => {
    return suggestions.filter(
      (suggestion: string) => !selectedItems.find((item: SelectedBrandType) => item.value === suggestion)
    );
  }, [suggestions, selectedItems]);

  const handleOpenAddForm = useCallback(() => {
    setShowAddForm(true);
  }, []);

  const handleCloseAddForm = useCallback(() => {
    setShowAddForm(false);
  }, []);

  const handleRemoveItem = useCallback(
    (id: number) => {
      setSelectedItems(selectedItems.filter((selectedItem: SelectedBrandType) => selectedItem.id !== id));
    },
    [selectedItems]
  );

  const handleAddBrand = useCallback(
    async (item) => {
      if (selectedItems.length >= max) return;

      setSelectedItems([...selectedItems, { id: selectedItems.length, value: item }]);
      handleCloseAddForm();
      setSuggestions([]);
      setValue('');
    },
    [selectedItems, handleCloseAddForm, max]
  );

  const handleChangeInputValue = useCallback(async (e) => {
    setValue(e.currentTarget.value);
  }, []);

  return (
    <div className="brands-list">
      <h3 className="brands-list__title">{t('sandbox_brands')}</h3>
      <div className="brands-list__items">
        {selectedItems.length ? (
          selectedItems.map((item: SelectedBrandType) => (
            <div key={item.id + item.value} className="brands-list-item">
              <div className="brands-list-item__name">{item.value}</div>
              <Button
                className="browser-history__remove-button"
                icon="delete"
                color="red"
                size="mini"
                onClick={() => handleRemoveItem(item.id)}
              />
            </div>
          ))
        ) : (
          <div>{t('sandbox_no_added_brands')}</div>
        )}
      </div>
      <div className="brands-list__add">
        {!showAddForm && (
          <Button onClick={handleOpenAddForm} color="green" disabled={selectedItems.length === max}>
            {t('sandbox_add_brand')}
          </Button>
        )}
        {showAddForm && (
          <div className="brands-list__add-form">
            <div className="brands-list__add-form-controls">
              <Input onChange={handleChangeInputValue} loading={isLoading} />
              <div className="brands-list__add-form__actions">
                <Button onClick={handleCloseAddForm}>{t('cancel')}</Button>
              </div>
            </div>

            {filteredSuggestions.length ? (
              <ul className="brands-list-suggestions">
                {filteredSuggestions.map((suggest: string) => (
                  <li key={suggest} onClick={() => handleAddBrand(suggest)} className="suggestion-list-item">
                    {suggest}
                  </li>
                ))}
              </ul>
            ) : null}
          </div>
        )}
      </div>
    </div>
  );
}
