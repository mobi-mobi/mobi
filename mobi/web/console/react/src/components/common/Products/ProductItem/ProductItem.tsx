import React, { useState, useCallback } from 'react';
import { Button } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';

import TimeRanges from '../../../home/TimeList';
import Graphics from '../Graphics';
import { FullProductType } from '../../../../containers/products/types';
import Information from '../Information';

type Props = {
  data: FullProductType;
};

export default function ProductItem({ data }: Props) {
  const { t } = useTranslation();
  const [expanded, setExpanded] = useState<boolean>(false);
  const [timeRange, setTimeRange] = useState<string>('28d');

  const { product, metrics_single } = data;

  const handleMoreClick = useCallback(() => {
    setExpanded(!expanded);
  }, [expanded]);

  const handleTimeRangeChange = useCallback((val: string) => {
    setTimeRange(val);
  }, []);

  return (
    <div className="fluid-card">
      <div className="fluid-card-preview">
        <div className="fluid-card-information">
          <div className="fluid-card-information__id">
            <span className="brand">{product.brand}</span>
          </div>
          <div className="fluid-card-information__name" onClick={handleMoreClick}>
            {product.title}
          </div>
        </div>
        <div className="fluid-card-statistic">
          <div className="fluid-card-statistic">
            <strong>{t('product_metric_displays')}:</strong> {metrics_single.displays}
          </div>
          <div className="fluid-card-statistic">
            <strong>{t('product_metric_add_to_basket')}:</strong> {metrics_single.to_basket}
          </div>
          <div className="fluid-card-statistic">
            <strong>{t('product_metric_sales')}:</strong> {metrics_single.sales}
          </div>
        </div>
        <div className="fluid-card-actions">
          <Button basic primary content={expanded ? t('less') : t('more')} size="tiny" onClick={handleMoreClick} />
        </div>
      </div>

      {expanded && (
        <div className="fluid-card-more-content">
          <div className="fluid-card-more-content-columns">
            <div className="fluid-card-more-content-columns__column">
              <TimeRanges
                isLocalChange={true}
                onLocalTimeChange={handleTimeRangeChange}
                customTimeRange={['60m', '24h', '28d']}
                localValue={timeRange}
              />
              <h2>{t('events')}</h2>
              <Graphics id={product.product_id} attributed={false} timeRange={timeRange} />

              <h2>{t('attributed_events')}</h2>
              <Graphics id={product.product_id} attributed={true} timeRange={timeRange} />
            </div>
            <div className="fluid-card-more-content-columns__column">
              <h3>{t('product_information')}</h3>
              <Information product={product} />
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
