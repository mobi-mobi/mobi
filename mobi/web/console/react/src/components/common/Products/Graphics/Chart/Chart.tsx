import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Chart } from 'react-google-charts';

import DataFormatter from '../../../../../classes/DataFormatter';
import LoadingSpinner from '../../../../../components/ui/LoadingSpinner';

import './styles.scss';

type Props = {
  data: any;
  timeRange: string;
  columns: any;
};

export default function ProductChart({ columns, data, timeRange }: Props) {
  const { t } = useTranslation();

  const format = useMemo(() => DataFormatter.preformatDataTimeShift([...data], timeRange), [data]);

  const percentsIndexes = useMemo(
    () =>
      [
        columns.indexOf(t('rel_product_to_wishlist')),
        columns.indexOf(t('rel_product_to_basket')),
        columns.indexOf(t('rel_product_sale')),
      ].filter((index) => index !== -1 && Boolean(index)),
    [columns]
  );

  const series = useMemo(() => {
    const series = {
      0: { targetAxisIndex: 0, pointSize: 2 },
    };

    percentsIndexes.forEach((index) => {
      series[index] = {
        targetAxisIndex: 1,
        pointSize: 3,
      };
    });

    return series;
  }, [percentsIndexes]);

  const formatters = useMemo(
    () =>
      percentsIndexes.map((index: number) => ({
        column: index,
        type: 'NumberFormat',
        options: {
          suffix: '%',
        },
      })),
    [percentsIndexes]
  );

  return (
    <div className="chart-item">
      {data.length === 0 && (
        <div className="chart-item__overlay">
          <span>{t('not_enough_data_to_display_chart')}</span>
        </div>
      )}

      <Chart
        data={[['time', ...columns], ...format]}
        width="100%"
        height="350px"
        chartType="LineChart"
        loader={<LoadingSpinner relative={true} size="small" />}
        // @ts-ignore
        formatters={formatters}
        options={{
          vAxis: {
            viewWindow: {
              min: 0,
            },
          },
          theme: 'maximized',
          series: series,
          legend: 'none',
          colors: ['#6625ab', '#feaf55', '#6725ea', '#ff6951', '#ff2b74', '#1cc9e8', '#10bc72'],
        }}
      />
    </div>
  );
}
