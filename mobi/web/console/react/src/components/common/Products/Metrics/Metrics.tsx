import React from 'react';
import { Card } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';

type Props = {
  data: any;
};

export default function Metrics({ data }: Props) {
  const { t } = useTranslation();

  return (
    <div className="products-metrics">
      <Card.Group>
        <Card header={t('products_active_in_stock')} description={data.productsInStock} />
        <Card header={t('products_active_not_in_stock')} description={data.productsNotInStock} />
        <Card header={t('products_disabled')} description={data.disabledProducts} />
      </Card.Group>
    </div>
  );
}
