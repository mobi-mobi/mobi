import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { ProductType } from '../../../../containers/products/types';

import './styles.scss';

type Props = {
  product: ProductType;
};

export default function Information({ product }: Props) {
  const { t } = useTranslation();

  const informationItems = useMemo(() => {
    const keys = ['product_id', 'color', 'categories', 'brand', 'description', /* 'in_stock',*/ 'active'];

    return keys.map((key: string) => {
      let value = product[key];

      if (value === null) {
        value = 'N/A';
      } else if (Array.isArray(value)) {
        value = value.join(', ');
      } else if (typeof value === 'boolean') {
        value = value ? t('yes') : t('no');
      }

      return (
        <li key={key} className="product-information__item">
          <strong>{t(`product_information_${key}`)}:</strong> <span>{value}</span>
        </li>
      );
    });
  }, [product, t]);

  return <ul className="product-information">{informationItems}</ul>;
}
