import React, { useEffect, useState, useMemo, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Checkbox } from 'semantic-ui-react';

import Chart from './Chart';
import MetricNameSelector from '../../../buisness/ABCharts/MetricNameSelector';

import Api from '../../../../services/Api';
import config from '../../../../config';

import './styles.scss';

type Props = {
  id: string;
  attributed: Boolean;
  timeRange: string;
};

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function Graphics({ id, attributed, timeRange }: Props) {
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [chartData, setChartData] = useState<any>(null);
  const [chartsKeys, setChartsKeys] = useState<Array<string>>([]);
  const [chartsActive, setChartsActive] = useState<Array<string>>([]);
  const [percentageChartsActive, setPercentageChartsActive] = useState<Array<string>>([]);
  const [metricName, setMetricName] = useState<string>('all');

  useEffect(() => {
    (async function getChartData() {
      const attributedValue = attributed ? 'yes' : 'no';
      const l = timeRange === '1h' ? '60m' : timeRange;

      let url = `/${client}/product/${id}/metrics/events?metrics_from=${l}&attributed=${attributedValue}${
        attributed ? `&recommendation_type=${metricName}` : ''
      }`;

      const result = await apiInstance.get(url, {});

      const relativeKeys = Object.keys(result.data.metrics_graph_relative);
      const relativeActivities = relativeKeys.reduce((acc: any, cur: any) => {
        return cur !== 'unknown'
          ? {
              ...acc,
              [`rel_${cur}`]: false,
            }
          : null;
      }, {});

      const keys = Object.keys(result.data.metrics_graph);
      const activities = keys.reduce((acc: any, cur: any) => {
        return {
          ...acc,
          [cur]: true,
        };
      }, {});

      setChartsKeys(keys);
      setChartsActive(activities);
      setPercentageChartsActive(relativeActivities);
      setChartData(result.data);
    })();
  }, [timeRange, id, attributed, metricName]);

  const handleMetricNameChange = useCallback((val: string) => {
    setMetricName(val);
  }, []);

  const columns = useMemo(() => {
    if (chartData) {
      const cols = Object.keys(chartsActive)
        .map((key) => (chartsActive[key] && key !== 'unknown' ? t(key) : null))
        .filter(Boolean);

      const relCols = Object.keys(percentageChartsActive)
        .map((key) => (percentageChartsActive[key] ? t(key) : null))
        .filter(Boolean);

      return [...cols, ...relCols];
    }

    return [];
  }, [chartsActive, chartData, t, percentageChartsActive]);

  const charts = useMemo(() => {
    if (chartData) {
      const unformatted = Object.keys(chartsActive)
        .map((key) => {
          return chartsActive[key] && key !== 'unknown' ? chartData.metrics_graph[key] : null;
        })
        .filter(Boolean);

      const relUnformatted = Object.keys(percentageChartsActive)
        .map((key) => {
          return percentageChartsActive[key] ? chartData.metrics_graph_relative[key.replace('rel_', '')] : null;
        })
        .filter(Boolean);

      const first = [...unformatted, ...relUnformatted][0];

      if (!first) {
        return [];
      }

      const result = first.map((val: any, index: number) => {
        return first.length && val
          ? [
              val[0],
              chartsActive['product_view'] ? chartData.metrics_graph['product_view'][index][1] : null,
              chartsActive['product_to_wishlist'] ? chartData.metrics_graph['product_to_wishlist'][index][1] : null,
              chartsActive['product_to_basket'] ? chartData.metrics_graph['product_to_basket'][index][1] : null,
              chartsActive['product_sale'] ? chartData.metrics_graph['product_sale'][index][1] : null,
              percentageChartsActive['rel_product_to_wishlist']
                ? chartData.metrics_graph_relative['product_to_wishlist'][index][1] * 100
                : null,
              percentageChartsActive['rel_product_to_basket']
                ? chartData.metrics_graph_relative['product_to_basket'][index][1] * 100
                : null,
              percentageChartsActive['rel_product_sale']
                ? chartData.metrics_graph_relative['product_sale'][index][1] * 100
                : null,
            ].filter((val) => val !== null)
          : null;
      });

      return result;
    }

    return [];
  }, [chartsActive, chartData, percentageChartsActive]);

  const handleChangeChartActivity = useCallback(
    (key: string, isPercentage: boolean) => {
      if (!isPercentage) {
        setChartsActive((prev) => ({
          ...prev,
          [key]: !chartsActive[key],
        }));
      } else {
        setPercentageChartsActive((prev) => ({
          ...prev,
          [key]: !percentageChartsActive[key],
        }));
      }
    },
    [chartsActive, percentageChartsActive]
  );

  const togglers = useMemo(() => {
    return chartsKeys
      .map((key) =>
        key !== 'unknown' ? (
          <Checkbox
            defaultChecked={true}
            onChange={() => handleChangeChartActivity(key, false)}
            label={t(key)}
            key={`na-${key}`}
          />
        ) : null
      )
      .filter(Boolean);
  }, [chartsKeys, handleChangeChartActivity]);

  const togglersInPercentage = useMemo(() => {
    return Object.keys(percentageChartsActive).map((key) => (
      <Checkbox
        defaultChecked={false}
        onChange={() => handleChangeChartActivity(key, true)}
        label={`${t(key)}, %`}
        key={`na-${key}`}
      />
    ));
  }, [percentageChartsActive, handleChangeChartActivity]);

  return chartData ? (
    <div>
      {attributed && (
        <MetricNameSelector
          defaultValue={metricName}
          options={[
            'all',
            'similar_products',
            'personal_recommendations',
            'basket_recommendations',
            'static_recommendations',
            'unknown',
          ]}
          onChange={handleMetricNameChange}
        />
      )}

      <Chart columns={columns} data={charts} timeRange={timeRange} />
      <div className="chart-togglers">
        <div className="chart-togglers--left">{togglers}</div>
        <div className="chart-togglers--right">{togglersInPercentage}</div>
      </div>
    </div>
  ) : null;
}
