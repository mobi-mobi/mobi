import React from 'react';

import ProductItem from './ProductItem';

import { FullProductType } from '../../../containers/products/types';

type Props = {
  items: Array<FullProductType>;
};

export default function Products({ items }: Props) {
  return (
    <div>
      {items.map((data: FullProductType) => (
        <ProductItem key={`${data.product.title}-${data.product.product_id}`} data={data} />
      ))}
    </div>
  );
}
