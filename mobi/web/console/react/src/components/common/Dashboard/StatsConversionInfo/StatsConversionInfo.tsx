import React, { useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Card, Icon } from 'semantic-ui-react';

import InfoBlock from '../InfoBlock';
import SalesFunnel from '../../SalesFunnel';
import Masonry from '../../../base/Masonry';

import Api from '../../../../services/Api';

import config from '../../../../config';
import { IStatsConversionInfoData } from './interfaces';

const apiInstance = new Api(config.global.REACT_APP_API_URL);


export default function StatsConversionInfo() {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [conversionData, setConversionData] = useState<IStatsConversionInfoData>();
  const client: string = useSelector((state: any) => state.home.client);
  const { t } = useTranslation();

  useEffect(() => {
    (async function getConversionData() {
      setIsLoading(true);

      const { data } = await apiInstance.get(`/${client}/pages/main/conversion-stats`, {});

      setConversionData(data);

      setIsLoading(false);
    })();
  }, [client]);

  return (
    <InfoBlock
      title={t('dashboard_conversion_stats')}
      link={{
        label: t('dashboard_go_to_displays_conversion'),
        url: '/metrics/attribution',
      }}
    >
      <div className="ui cards">
        <Masonry>
          <Card
            className="preload-card"
            header={t('similar_products')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : conversionData ? (
                <SalesFunnel data={conversionData.similar_products} />
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('personalized_recommendations')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : conversionData ? (
                <SalesFunnel data={conversionData.personal_recommendations} />
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('basket_recommendations')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : conversionData ? (
                <SalesFunnel data={conversionData.basket_recommendations} />
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('organic')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : conversionData ? (
                <SalesFunnel data={conversionData.organic} />
              ) : null
            }
          />
        </Masonry>
      </div>
    </InfoBlock>
  );
}
