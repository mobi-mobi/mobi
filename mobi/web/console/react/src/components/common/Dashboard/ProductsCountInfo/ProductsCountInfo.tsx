import React, { useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import InfoBlock from '../InfoBlock';
import Masonry from '../../../base/Masonry';

import Api from '../../../../services/Api';

import config from '../../../../config';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function ProductsCountInfo() {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const client: string = useSelector((state: any) => state.home.client);
  const { t } = useTranslation();

  useEffect(() => {
    const queries = [
      apiInstance.get(`/${client}/catalog/brands`, {}),
      apiInstance.get(`/${client}/catalog/products`, {}),
    ];

    Promise.all(queries).then((res) => {
      console.log(res);
    });
  }, [client]);

  return (
    <InfoBlock title={t('dashboard_catalog_state')}>
      <div className="ui cards">
        {/* <Masonry>
        <Card
          className="preload-card"
          header={t('dashboard_products_count')}
          description={
            isLoading ? (
              <div className="description">
                <Icon loading name="spinner" />
              </div>
            ) : integrationData ? (
              integrationData.user_events_received
            ) : null
          }
        />
        <Card
          className="preload-card"
          header={t('dashboard_brands_count')}
          description={
            isLoading ? (
              <div className="description">
                <Icon loading name="spinner" />
              </div>
            ) : integrationData ? (
              integrationData.catalog_updates_received
            ) : null
          }
        />
        </Masonry>*/}
      </div>
    </InfoBlock>
  );
}
