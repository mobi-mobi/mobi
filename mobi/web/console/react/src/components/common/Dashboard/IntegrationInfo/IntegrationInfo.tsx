import React, { useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Card, Icon } from 'semantic-ui-react';

import InfoBlock from '../InfoBlock';
import Masonry from '../../../base/Masonry';

import Api from '../../../../services/Api';

import config from '../../../../config';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

interface IIntegrationInfoData {
  user_events_received: number;
  catalog_updates_received: number;
  recommendations_returned: number;
  search_results_returned: number;
}

export default function IntegrationInfo() {
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [integrationData, setIntegrationData] = useState<IIntegrationInfoData>();

  useEffect(() => {
    (async function getIntegrationData() {
      setIsLoading(true);

      const { data } = await apiInstance.get(`/${client}/pages/main/integration-stats`, {});
      setIntegrationData(data);

      setIsLoading(false);
    })();
  }, [client]);

  return (
    <InfoBlock
      title={t('dashboard_integration')}
      link={{
        label: t('dashboard_go_to_integration'),
        url: '/metrics/integration',
      }}
    >
      <div className="ui cards">
        <Masonry>
          <Card
            className="preload-card"
            header={t('dashboard_user_events_received')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : integrationData ? (
                integrationData.user_events_received
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_catalog_updates_received')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : integrationData ? (
                integrationData.catalog_updates_received
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_recommendations_returned')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : integrationData ? (
                integrationData.recommendations_returned
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_search_results_returned')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : integrationData ? (
                integrationData.search_results_returned
              ) : null
            }
          />
        </Masonry>
      </div>
    </InfoBlock>
  );
}
