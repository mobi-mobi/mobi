import React, { useEffect, useState, useMemo, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Card, Icon } from 'semantic-ui-react';
import Chart from 'react-google-charts';

import InfoBlock from '../InfoBlock';

import Api from '../../../../services/Api';

import config from '../../../../config';

import { ITrafficSourceData, ITrafficSourceInfoData } from './interfaces';

import './styles.scss';
import DataFormatter from '../../../../classes/DataFormatter';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function TrafficSourceInfo() {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [trafficSourceData, setTrafficSourceData] = useState<ITrafficSourceData>();
  const [trafficSourceInfoData, setTrafficSourceInfoData] = useState<ITrafficSourceInfoData>();
  const client: string = useSelector((state: any) => state.home.client);
  const { t } = useTranslation();

  useEffect(() => {
    (async function getIntegrationData() {
      setIsLoading(true);

      const { data: trafficSource } = await apiInstance.get(`/${client}/pages/main/traffic-source`, {});
      const { data: trafficSourceInfo } = await apiInstance.get(`/${client}/pages/main/traffic-info`, {});

      setTrafficSourceData(trafficSource);
      setTrafficSourceInfoData(trafficSourceInfo);

      setIsLoading(false);
    })();
  }, [client]);

  const trafficSourceEventData = useCallback(
    (type: string) => {
      if (!trafficSourceInfoData) return [];

      const views = DataFormatter.preformatDataTime(trafficSourceInfoData[type].product_view, '24h', 'YYYY-MM-DD');
      const toWishlists = DataFormatter.preformatDataTime(trafficSourceInfoData[type].product_to_wishlist, '24h', 'YYYY-MM-DD');
      const toBasket = DataFormatter.preformatDataTime(trafficSourceInfoData[type].product_to_basket, '24h', 'YYYY-MM-DD');
      const sale = DataFormatter.preformatDataTime(trafficSourceInfoData[type].product_sale, '24h', 'YYYY-MM-DD');

      return sale.map((val: any, index: number) => [
        ...val,
        toBasket[index][1],
        toWishlists[index][1],
        views[index][1],
      ]);
    },
    [trafficSourceInfoData]
  );

  const pieCharts = useMemo(() => {
    if (!trafficSourceData) return;

    const keys = Object.keys(trafficSourceData);

    return keys.map((key: string, index: number) => {
      const chartKeys = Object.keys(trafficSourceData[key]);
      const chartValues = Object.values(trafficSourceData[key]);
      const titles = chartKeys.map(t);

      const chartData = titles.map((title: any, index: number) => {
        return (chartValues[index] as number) > 0
          ? ['', { v: chartValues[index], f: `${title} — ${chartValues[index]}` }]
          : ['', { v: 1, f: `${title} — 0` }];
      });

      return (
        <Card
          key={`pie-chart-${key}-${index}`}
          className="preload-card hide-gridlines"
          header={t(key)}
          description={
            <Chart
              height="180px"
              chartType="PieChart"
              loader={<div>Loading Chart</div>}
              data={[[t('type'), t('value')], ...chartData]}
              options={{
                legend: 'none',
                theme: 'maximized',
                sliceVisibilityThreshold: 0,
                colors: ['#ff6951', '#feaf55', '#6725ea', '#51c3c5'],
              }}
            />
          }
        />
      );
    });
  }, [trafficSourceData]);

  const legend = useMemo(() => {
    const labels = ['similar_products', 'personal_recommendations', 'basket_recommendations', 'organic'];

    return labels.map((label: string) => (
      <div key={label} className={`custom-legend-item custom-legend-item--${label}`}>
        <div className="custom-legend-item__color"></div>
        <div className="custom-legend-item__label">{t(label)}</div>
      </div>
    ));
  }, []);

  return (
    <>
      <InfoBlock title={t('dashboard_traffic_info')}>
        <div className="ui cards">
          <Card
            className="preload-card hide-gridlines-y"
            header={t('dashboard_user_events')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : trafficSourceInfoData ? (
                <Chart
                  height="150px"
                  chartType="AreaChart"
                  loader={<div>Loading Chart</div>}
                  data={[
                    [
                      t('date'),
                      t('graph_title_product_sale'),
                      t('graph_title_product_to_basket'),
                      t('graph_title_product_to_wishlist'),
                      t('graph_title_product_view'),
                    ],
                    ...trafficSourceEventData('events'),
                  ]}
                  options={{
                    isStacked: true,
                    legend: 'none',
                    theme: 'maximized',
                    colors: ['#e7564e', '#fcb559', '#51c3c5', '#1775b7'],
                    vAxis: { minValue: 0 },
                    lineWidth: 0,
                    hAxis: {
                      textPosition: 'none',
                    },
                    series: {
                      0: { pointSize: 1 },
                      1: { pointSize: 1 },
                      2: { pointSize: 1 },
                      3: { pointSize: 1 },
                    },
                  }}
                />
              ) : null
            }
          />{' '}
          <Card
            className="preload-card hide-gridlines-y"
            header={t('dashboard_user_attributed_events')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : trafficSourceInfoData ? (
                <Chart
                  height="150px"
                  chartType="AreaChart"
                  loader={<div>Loading Chart</div>}
                  data={[
                    [
                      t('date'),
                      t('graph_title_product_sale'),
                      t('graph_title_product_to_basket'),
                      t('graph_title_product_to_wishlist'),
                      t('graph_title_product_view'),
                    ],
                    ...trafficSourceEventData('attributed_events'),
                  ]}
                  options={{
                    isStacked: true,
                    legend: 'none',
                    theme: 'maximized',
                    colors: ['#e7564e', '#fcb559', '#51c3c5', '#1775b7'],
                    vAxis: { minValue: 0 },
                    lineWidth: 0,
                    hAxis: {
                      textPosition: 'none',
                    },
                    series: {
                      0: { pointSize: 1 },
                      1: { pointSize: 1 },
                      2: { pointSize: 1 },
                      3: { pointSize: 1 },
                    },
                  }}
                />
              ) : null
            }
          />
        </div>
      </InfoBlock>
      <InfoBlock
        title={t('dashboard_traffic')}
        link={{
          label: t('dashboard_go_to_traffic'),
          url: '/metrics/attribution',
        }}
      >
        <div className="custom-legend">{legend}</div>
        <div className="ui cards">{pieCharts}</div>
      </InfoBlock>
    </>
  );
}
