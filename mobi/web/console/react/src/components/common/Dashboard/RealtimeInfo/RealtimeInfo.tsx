import React, { useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Card, Icon } from 'semantic-ui-react';

import InfoBlock from '../InfoBlock';
import MostPopularItems from '../MostPopularItems';
import Masonry from '../../../base/Masonry';

import { ProductType } from '../../../../containers/products/types';

import Api from '../../../../services/Api';

import config from '../../../../config';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

interface IRealTimeData {
  active_users_num: Number;
  most_popular_brands: Array<Array<string | number>>;
  most_popular_categories: Array<Array<string | number>>;
  most_popular_products: Array<{
    product: ProductType;
    count: Number;
  }>;
}

export default function RealtimeInfo() {
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [realTimeData, setRealTimeData] = useState<IRealTimeData>();

  useEffect(() => {
    (async function getRealTimeInfoData() {
      setIsLoading(true);

      const { data } = await apiInstance.get(`/${client}/pages/main/real-time`, {});
      setRealTimeData(data);

      setIsLoading(false);
    })();
  }, [client]);

  const mostPopularProductsParsed = useMemo(() => {
    return realTimeData?.most_popular_products.map((value) => [value.product.title, value.count]);
  }, [realTimeData?.most_popular_products]);

  return (
    <InfoBlock
      title={t('dashboard_real_time_information')}
      link={{
        label: t('dashboard_more_real_time_information'),
        url: '/metrics/real-time',
      }}
    >
      <div className="ui cards">
        <Masonry>
          <Card
            className="preload-card"
            header={t('dashboard_active_users_now')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : (
                realTimeData?.active_users_num
              )
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_most_popular_product')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : (
                <MostPopularItems items={mostPopularProductsParsed} />
              )
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_most_popular_brands')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : (
                <MostPopularItems items={realTimeData?.most_popular_brands} />
              )
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_most_popular_categories')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : (
                <MostPopularItems items={realTimeData?.most_popular_categories} />
              )
            }
          />
        </Masonry>
      </div>
    </InfoBlock>
  );
}
