import React, { useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Card, Icon } from 'semantic-ui-react';

import InfoBlock from '../InfoBlock';
import Masonry from '../../../base/Masonry';

import Api from '../../../../services/Api';

import config from '../../../../config';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

interface IIncrementalInfoData {
  product_view: number;
  product_to_wishlist: number;
  product_to_basket: number;
  product_sale: number;
}

export default function IncrementalInfo() {
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [incrementalData, setIncrementalData] = useState<IIncrementalInfoData>();

  useEffect(() => {
    (async function getIncrementalData() {
      setIsLoading(true);

      const { data } = await apiInstance.get(`/${client}/pages/main/incremental-stats`, {});
      setIncrementalData(data);

      setIsLoading(false);
    })();
  }, [client]);

  const checkIsPositive = (value: string) =>
    value[0] === '-' ? (
      <div className="description negative">{value}%</div>
    ) : (
      <div className="description positive">{`+${value}`}%</div>
    );

  return (
    <InfoBlock
      title={t('dashboard_incremental')}
      link={{
        label: t('dashboard_go_to_incremental'),
        url: '/metrics/incremental',
      }}
    >
      <div className="ui cards">
        <Masonry>
          <Card
            className="preload-card"
            header={t('dashboard_displays')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : incrementalData ? (
                checkIsPositive((incrementalData.product_view * 100).toFixed(2))
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_to_wishlist')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : incrementalData ? (
                checkIsPositive((incrementalData.product_to_wishlist * 100).toFixed(2))
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_to_basket')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : incrementalData ? (
                checkIsPositive((incrementalData.product_to_basket * 100).toFixed(2))
              ) : null
            }
          />
          <Card
            className="preload-card"
            header={t('dashboard_sales')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : incrementalData ? (
                checkIsPositive((incrementalData.product_sale * 100).toFixed(2))
              ) : null
            }
          />
        </Masonry>
      </div>
    </InfoBlock>
  );
}
