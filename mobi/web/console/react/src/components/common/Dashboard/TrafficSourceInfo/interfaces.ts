export interface ITrafficSourceData {
  product_view: {
    similar_products: number;
    personal_recommendations: number;
    basket_recommendations: number;
    organic: number;
  };
  product_to_wishlist: {
    similar_products: number;
    personal_recommendations: number;
    basket_recommendations: number;
    organic: number;
  };
  product_to_basket: {
    similar_products: number;
    personal_recommendations: number;
    basket_recommendations: number;
    organic: number;
  };
  product_sale: {
    similar_products: number;
    personal_recommendations: number;
    basket_recommendations: number;
    organic: number;
  };
}

export interface ITrafficSourceInfoData {
  attributed_events: {
    product_sale: Array<Array<Array<number | string>>>;
    product_to_basket: Array<Array<Array<number | string>>>;
    product_to_wishlist: Array<Array<Array<number | string>>>;
    product_view: Array<Array<Array<number | string>>>;
  };
  events: {
    product_sale: Array<Array<Array<number | string>>>;
    product_to_basket: Array<Array<Array<number | string>>>;
    product_to_wishlist: Array<Array<Array<number | string>>>;
    product_view: Array<Array<Array<number | string>>>;
  };
}
