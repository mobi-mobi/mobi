import React, { useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Card, Icon } from 'semantic-ui-react';
import Chart from 'react-google-charts';

import InfoBlock from '../InfoBlock';
import Masonry from '../../../base/Masonry';

import DataFormatter from '../../../../classes/DataFormatter';

import Api from '../../../../services/Api';

import config from '../../../../config';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

interface IAudienceInfoData {
  new_users_share: number;
  new_vs_returning: {
    new: Array<Array<number | string>>;
    returning: Array<Array<number | string>>;
  };
  active_week_to_week: Array<Array<Array<number | string>>>;
  user_churns: {
    churned_users_w7d_a7d_abs: Array<Array<Array<number | string>>>;
    churned_users_w7d_a7d_rel: Array<Array<Array<number | string>>>;
    churned_users_w7d_a35d_abs: Array<Array<Array<number | string>>>;
    churned_users_w7d_a35d_rel: Array<Array<Array<number | string>>>;
  };
}

export default function AudienceInfo() {
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [audienceData, setAudienceData] = useState<IAudienceInfoData>();

  useEffect(() => {
    (async function getIntegrationData() {
      setIsLoading(true);

      const { data } = await apiInstance.get(`/${client}/pages/main/audience-info`, {});
      setAudienceData(data);

      setIsLoading(false);
    })();
  }, [client]);

  const newReturningUsersData = useMemo(() => {
    if (!audienceData) return [];

    const newUsers = DataFormatter.preformatDataTimeShift(audienceData.new_vs_returning.new, '24h');
    const returnedUsers = DataFormatter.preformatDataTimeShift(audienceData.new_vs_returning.returning, '24h');

    return newUsers.map((val: any, index: number) => [...val, returnedUsers[index][1]]);
  }, [audienceData]);

  const usersChurnData = useMemo(() => {
    if (!audienceData) return [];

    const churnedUsers7d = DataFormatter.preformatDataTime(
      audienceData.user_churns.churned_users_w7d_a7d_rel,
      '7d',
      'YYYY-MM-DD'
    );
    const churnedUsers35d = DataFormatter.preformatDataTime(
      audienceData.user_churns.churned_users_w7d_a35d_rel,
      '35d',
      'YYYY-MM-DD'
    );

    return churnedUsers7d.map((val: any, index: number) => [val[0], val[1] * 100, churnedUsers35d[index][1] * 100]);
  }, [audienceData]);

  const weeksChartData = useMemo(() => {
    if (!audienceData) return [];

    const { active_week_to_week: data } = audienceData;

    const firstWeek = data[0];

    const combinedData = firstWeek.map((val: any, index: number) => [
      val[0],
      data[3][index][1],
      data[2][index][1],
      data[1][index][1],
      val[1],
    ]);

    const formatted = DataFormatter.preformatDataTimeShift(combinedData, '7d', true);

    return formatted.map((val: any, index: number) => {
      const dayOfWeek = t(val.shift());

      return [dayOfWeek, ...val];
    });
  }, [audienceData, t]);

  return (
    <InfoBlock
      title={t('dashboard_audience')}
      link={{
        label: t('dashboard_go_to_audience'),
        url: '/metrics/audience',
      }}
    >
      <div className="ui cards">
        <Masonry>
          <Card
            className="preload-card"
            header={t('dashboard_new_users_share')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : audienceData ? (
                `${(audienceData.new_users_share * 100).toFixed(2)}%`
              ) : null
            }
          />
          <Card
            className="preload-card hide-gridlines-y"
            header={t('dashboard_new_vs_returning_users')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : audienceData ? (
                <Chart
                  height="150px"
                  chartType="AreaChart"
                  loader={<div>Loading Chart</div>}
                  data={[[t('date'), t('new'), t('returning')], ...newReturningUsersData]}
                  options={{
                    isStacked: true,
                    legend: 'none',
                    theme: 'maximized',
                    colors: ['#feaf55', '#6625ab'],
                    vAxis: { minValue: 0 },
                    lineWidth: 0,
                    hAxis: {
                      textPosition: 'none',
                    },
                    series: {
                      0: { pointSize: 1 },
                      1: { pointSize: 1 },
                    },
                  }}
                />
              ) : null
            }
          />
          <Card
            className="preload-card hide-gridlines-y"
            header={t('dashboard_audience_active_users')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : audienceData ? (
                <Chart
                  height={'150px'}
                  chartType="LineChart"
                  loader={<div>Loading Chart</div>}
                  data={[
                    [
                      t('date'),
                      t('active_user_week_4'),
                      t('active_user_week_3'),
                      t('active_user_week_2'),
                      t('active_user_week_1'),
                    ],
                    ...weeksChartData,
                  ]}
                  options={{
                    legend: 'none',
                    vAxis: { minValue: 0 },
                    theme: 'maximized',
                    colors: ['#caabec', '#9657da', '#6324a7', '#feaf55'],
                  }}
                />
              ) : null
            }
          />
          <Card
            className="preload-card hide-gridlines-y"
            header={t('dashboard_users_churns')}
            description={
              isLoading ? (
                <div className="description">
                  <Icon loading name="spinner" />
                </div>
              ) : audienceData ? (
                <Chart
                  height="150px"
                  chartType="LineChart"
                  loader={<div>Loading Chart</div>}
                  data={[
                    [t('date'), t('dashboard_users_churns_7d'), t('dashboard_users_churns_35d')],
                    ...usersChurnData,
                  ]}
                  formatters={[
                    {
                      column: 1,
                      type: 'NumberFormat',
                      options: {
                        suffix: '%',
                      },
                    },
                    {
                      column: 2,
                      type: 'NumberFormat',
                      options: {
                        suffix: '%',
                      },
                    },
                  ]}
                  options={{
                    isStacked: true,
                    legend: 'none',
                    theme: 'maximized',
                    colors: ['#feaf55', '#6324a7'],
                    vAxis: { minValue: 0 },
                    hAxis: {
                      textPosition: 'none',
                    },
                    series: {
                      0: { pointSize: 0 },
                      1: { pointSize: 0 },
                    },
                  }}
                />
              ) : null
            }
          />
        </Masonry>
      </div>
    </InfoBlock>
  );
}
