export interface IStatsConversionInfoDataItem {
  product_view: number;
  product_to_wishlist: number;
  product_to_basket: number;
  product_sale: number;
}

export interface IStatsConversionInfoData {
  basket_recommendations: IStatsConversionInfoDataItem;
  organic: IStatsConversionInfoDataItem;
  personal_recommendations: IStatsConversionInfoDataItem;
  similar_products: IStatsConversionInfoDataItem;
}
