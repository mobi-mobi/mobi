import React, { ReactNode } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'semantic-ui-react';

import './styles.scss';

type Props = {
  children: ReactNode;
  title: string;
  link?: {
    url: string;
    label: string;
  };
};

export default function InfoBlock({ children, title, link }: Props) {
  return (
    <div className="info-block">
      <h3 className="info-block__title">{title}</h3>
      <div className="info-block__content">{children}</div>
      {link && (
        <Link to={link.url} className="info-block__link">
          <Button primary>{link.label}</Button>
        </Link>
      )}
    </div>
  );
}
