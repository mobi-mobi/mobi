import React from 'react';

import './styles.scss';

type Props = {
  items: Array<Array<string | Number>> | undefined;
};

export default function MostPopularItems({ items }: Props) {
  if (!items) return null;

  return (
    <ul className="most-popular-items">
      {items.slice(0, 5).map((val) => {
        const [title, count] = val;

        return (
          <li key={`${title}-${count}`} className="most-popular-items-item">
            <span className="most-popular-items-item__title">{title}</span>
            <span className="most-popular-items-item__count">{count}</span>
          </li>
        );
      })}
    </ul>
  );
}
