import React, { useState, useRef, useEffect, useCallback, useMemo } from 'react';
import ReactTags from 'react-tag-autocomplete';

import './styles.scss';

type Props = {
  name: string | undefined;
  onChange?: Function;
  onRemove?: Function;
  onAdd?: Function;
  data: Array<any>;
  initialData?: Array<any>;
};

export default function Tags({ name, onChange, data, initialData, onAdd, onRemove }: Props) {
  const [localData, setLocalData] = useState<Array<any>>(initialData || []);
  const isInitialMount = useRef(true);
  const parsed = useMemo(() => data.map((name, id) => ({ name, id })), [data]);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      if (onChange) {
        onChange(
          name,
          localData.map(({ name }) => name)
        );
      }
    }
    // eslint-disable-next-line
  }, [name, localData]);

  const handleRemoveTags = useCallback(
    (index) => {
      const tags = [...localData];

      if (onRemove) {
        onRemove(tags.find((v, i) => index === i));
      }

      tags.splice(index, 1);

      setLocalData(tags);
    },
    [localData, onRemove]
  );

  const handleAddTags = useCallback(
    (tag) => {
      const tags = [...localData, tag];

      if (onAdd) {
        onAdd(tag);
      }

      setLocalData(tags);
    },
    [localData, onAdd]
  );

  return (
    <ReactTags
      tags={localData}
      suggestions={parsed}
      onDelete={(index: any) => handleRemoveTags(index)}
      onAddition={(tag: any) => handleAddTags(tag)}
      onValidate={(tag: { name: string; id: number }) => tag.name.length >= 1}
    />
  );
}
