import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Header, Modal, Divider, Input, Dropdown } from 'semantic-ui-react';
import { useSelector } from 'react-redux';

import Api from '../../../../services/Api';
import config from '../../../../config';

import { IAudienceResponse, IAudienceUnion } from '../../../../pages/CustomAudiences/Audiences/interfaces';
import { IResponseError } from '../../../../types/api';

import './styles.scss';

interface IAddAudienceUnionProps {
  isOpened?: string | null;
  onClose?(): void;
  onOpen?(): void;
  onSubmit?(): void;
  edit?: IAudienceUnion;
  audiences?: IAudienceResponse[];
}

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function AddAudienceUnion(props: IAddAudienceUnionProps) {
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [modalOpen, setModalOpen] = useState<boolean>();
  const [result, setResult] = useState<string>('');
  const [error, setError] = useState<IResponseError | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [id, setId] = useState<string>(props.edit ? props.edit.audiences_union_id : '');
  const [name, setName] = useState<string>(props.edit ? props.edit.audiences_union_name : '');
  const [description, setDescription] = useState<string>(props.edit ? props.edit.audiences_union_description : '');
  const [union, setUnion] = useState<string[]>(props.edit ? props.edit.audience_ids : []);

  const handleOpen = useCallback(() => {
    setModalOpen(true);

    if (props.edit) {
      setId(props.edit.audiences_union_id);
      setName(props.edit.audiences_union_name);
      setDescription(props.edit.audiences_union_description);
      setUnion(props.edit.audience_ids);
    }

    if (props.onOpen) {
      props.onOpen();
    }
  }, [props.edit, props.onOpen]);

  useEffect(() => {
    if (props.isOpened !== undefined && props.edit?.audiences_union_id === props.isOpened) {
      handleOpen();
    }
  }, [props.isOpened, handleOpen])

  const handleClose = useCallback(() => {
    setModalOpen(false);
    setDescription('');
    setName('');
    setId('');
    setResult('');
    setError(null);

    if (props.onSubmit) {
      props.onSubmit();
    }

    if (props.onClose) {
      props.onClose();
    }
  }, [result, props.onSubmit]);

  const options = useMemo(() => {
    if (props.audiences) {
      return props.audiences.map((val) => ({
        key: val.audience.audience_id,
        text: val.audience.audience_name,
        value: val.audience.audience_id,
      }));
    }

    return [];
  }, [props.audiences]);

  const handleSubmit = useCallback(() => {
    setIsLoading(true);
    setError(null);

    const url = `/${client}/audiences_unions`;
    const body = JSON.stringify({
      audiences_union_id: id,
      audiences_union_name: name,
      audiences_union_description: description,
      audience_ids: union,
    });

    const request = props.edit
      ? apiInstance.put(url, {
          body,
        })
      : apiInstance.post(url, {
          body,
        });

    request
      .then((res) => {
        handleClose();
      })
      .catch((e) => {
        setError(e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [client, name, id, description, props.edit, handleClose, union]);

  const onDescriptionChange = useCallback((e, val) => {
    setDescription(val.value);
  }, []);

  const onNameChange = useCallback((e, val) => {
    setName(val.value);
  }, []);

  const onIdChange = useCallback((e, val) => {
    setId(val.value);
  }, []);

  const onUnionChange = useCallback((e, val) => {
    setUnion(val.value);
  }, []);

  return (
    <Modal
      centered={false}
      trigger={
        <Button basic={!!props.edit} primary size="tiny" onClick={handleOpen}>
          {props.edit ? t('edit') : t('add_audiences_union')}
        </Button>
      }
      open={modalOpen}
      onClose={handleClose}
      size="tiny"
    >
      <Header content={t('audience_union_modal_title')} />
      <Modal.Content>
        {!!error && <div className="error-message">{error.error.description}</div>}
        <div className="add-token-modal">
          <Input
            loading={isLoading}
            disabled={!!result.length || Boolean(props.edit)}
            value={id}
            onChange={onIdChange}
            placeholder={t('id')}
          />
          <Divider />
          <Input
            loading={isLoading}
            disabled={!!result.length}
            value={name}
            onChange={onNameChange}
            placeholder={t('name')}
          />
          <Divider />
          <Input
            loading={isLoading}
            disabled={!!result.length}
            value={description}
            onChange={onDescriptionChange}
            placeholder={t('description')}
          />
          <Divider />
          <Dropdown
            onChange={onUnionChange}
            value={union}
            placeholder={t('audiences')}
            fluid
            multiple
            selection
            options={options}
          />
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button color="green" onClick={!!result.length ? handleClose : handleSubmit} loading={isLoading}>
          {props.edit ? t('edit_union_audience') : t('add_union_audience')}
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
