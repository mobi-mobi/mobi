import React, { useState, useCallback, useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Header, Modal, Divider, Message, Select, Checkbox, Input } from 'semantic-ui-react';
import { useSelector } from 'react-redux';

import Api from '../../../../services/Api';
import config from '../../../../config';

import './styles.scss';

type TokenRequestStatus = {
  isLoading: boolean;
  success: boolean;
  error: boolean;
};

interface IAddTokenProps {
  onSubmit?(): void;
}

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function AddTokenModal(props: IAddTokenProps) {
  const { t } = useTranslation();
  const clients: string[] = useSelector((state: any) => state.home.user.clients);
  const [modalOpen, setModalOpen] = useState<boolean>();
  const [result, setResult] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [description, setDescription] = useState<string>('');
  const [isTest, setIsTest] = useState<boolean | undefined>(false);
  const [client, setClient] = useState<string | number | boolean | (string | number | boolean)[] | undefined>('');
  const [permissions, setPermissions] = useState({
    get_recommendations: false,
    read_metrics: false,
    read_catalog: false,
    search: false,
    update_catalog: false,
    send_events: false,
    test_recommendations: false,
  });

  const handleOpen = useCallback(() => {
    setModalOpen(true);
  }, []);

  const handleClose = useCallback(() => {
    setModalOpen(false);
    setClient('');
    setDescription('');
    setIsTest(false);
    setPermissions({
      get_recommendations: false,
      read_metrics: false,
      read_catalog: false,
      search: false,
      update_catalog: false,
      send_events: false,
      test_recommendations: false,
    });

    if (!!result.length && props.onSubmit) {
      props.onSubmit();
      setResult('');
    }
  }, [result, props.onSubmit]);

  const handleSubmit = useCallback(() => {
    const keys = Object.keys(permissions);
    const active = keys.filter((key) => permissions[key]);

    setIsLoading(true);

    apiInstance
      .post(`/user/settings/tokens?client=${client}`, {
        body: JSON.stringify({
          token_type: isTest ? 'test' : 'production',
          description,
          token_permissions: active,
        }),
      })
      .then((res) => {
        setResult(res.data.token_str);
      })
      .catch((e) => {
        alert(e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [client, permissions, description, isTest]);

  const handleChangePermissions = useCallback((permission, value) => {
    setPermissions((prevState) => ({
      ...prevState,
      [permission]: value,
    }));
  }, []);

  const onDescriptionChange = useCallback((e, val) => {
    setDescription(val.value);
  }, []);

  return (
    <Modal
      centered={false}
      trigger={
        <Button primary onClick={handleOpen}>
          {t('add_token')}
        </Button>
      }
      open={modalOpen}
      onClose={handleClose}
      size="tiny"
    >
      <Header content={t('add_token_modal_title')} />
      <Modal.Content>
        {!Boolean(result.length) && (
          <div className="add-token-modal">
            <Select
              placeholder={t('client')}
              loading={isLoading}
              onChange={(e, data) => setClient(data.value)}
              disabled={!!result.length}
              options={clients.map((val) => ({
                key: val,
                value: val,
                text: val,
              }))}
            />
            <Divider />
            <Checkbox
              disabled={isLoading || !!result.length}
              label={t('add_token_for_tests')}
              onChange={(e, data) => setIsTest(data.checked)}
            />
            <Divider />
            <Input
              loading={isLoading}
              disabled={!!result.length}
              value={description}
              onChange={onDescriptionChange}
              placeholder={t('description')}
            />
            <Divider />
            <p>{t('permissions')}:</p>
            <Checkbox
              disabled={isLoading || !!result.length}
              label={t('permission_get_recommendations')}
              onChange={(e, data) => handleChangePermissions('get_recommendations', data.checked)}
            />
            <br />
            <Checkbox
              disabled={isLoading || !!result.length}
              label={t('permission_read_metrics')}
              onChange={(e, data) => handleChangePermissions('read_metrics', data.checked)}
            />
            <br />
            <Checkbox
              disabled={isLoading || !!result.length}
              label={t('permission_read_catalog')}
              onChange={(e, data) => handleChangePermissions('read_catalog', data.checked)}
            />
            <br />
            <Checkbox
              disabled={isLoading || !!result.length}
              label={t('permission_search')}
              onChange={(e, data) => handleChangePermissions('search', data.checked)}
            />
            <br />
            <Checkbox
              disabled={isLoading || !!result.length}
              label={t('permission_update_catalog')}
              onChange={(e, data) => handleChangePermissions('update_catalog', data.checked)}
            />
            <br />
            <Checkbox
              disabled={isLoading || !!result.length}
              label={t('permission_send_events')}
              onChange={(e, data) => handleChangePermissions('send_events', data.checked)}
            />
            <br />
            <Checkbox
              disabled={isLoading || !!result.length}
              label={t('permission_test_recommendations')}
              onChange={(e, data) => handleChangePermissions('test_recommendations', data.checked)}
            />
          </div>
        )}

        {!!result.length && (
          <div style={{ textAlign: 'center' }}>
            {t('add_token_modal_success_heading')}: <br /> <b style={{ padding: 10 }}>{result}</b>
          </div>
        )}
      </Modal.Content>
      <Modal.Actions>
        <Button color="green" onClick={!!result.length ? handleClose : handleSubmit} loading={isLoading}>
          {!!result.length ? t('close') : t('add_token')}
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
