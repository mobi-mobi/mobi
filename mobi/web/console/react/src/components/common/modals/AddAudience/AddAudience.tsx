import React, { useState, useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Header, Modal, Divider, Input } from 'semantic-ui-react';
import { useSelector } from 'react-redux';

import Api from '../../../../services/Api';
import config from '../../../../config';

import { IAudience } from '../../../../pages/CustomAudiences/Audiences/interfaces';
import { IResponseError } from '../../../../types/api';

import './styles.scss';

interface IAddAudienceProps {
  isOpened?: string | null;
  onSubmit?(): void;
  onClose?(): void;
  onOpen?(): void;
  edit?: IAudience;
}

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function AddAudience(props: IAddAudienceProps) {
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [modalOpen, setModalOpen] = useState<boolean>();
  const [result, setResult] = useState<string>('');
  const [error, setError] = useState<IResponseError | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [id, setId] = useState<string>(props.edit ? props.edit.audience_id : '');
  const [name, setName] = useState<string>(props.edit ? props.edit.audience_name : '');
  const [description, setDescription] = useState<string>(props.edit ? props.edit.audience_description : '');

  const handleOpen = useCallback(() => {
    setModalOpen(true);

    if (props.edit) {
      setId(props.edit.audience_id);
      setName(props.edit.audience_name);
      setDescription(props.edit.audience_description);
    }

    if (props.onOpen) {
      props.onOpen();
    }
  }, [props.edit, props.onOpen]);

  useEffect(() => {
    if (props.isOpened && props.isOpened !== undefined && props.edit?.audience_id === props.isOpened) {
      handleOpen();
    }
  }, [props.isOpened, handleOpen])

  const handleClose = useCallback(() => {
    setModalOpen(false);
    setDescription('');
    setName('');
    setId('');
    setResult('');
    setError(null);

    if (props.onSubmit) {
      props.onSubmit();
    }

    if (props.onClose) {
      props.onClose();
    }
  }, [result, props.onSubmit]);

  const handleDelete = useCallback(() => {
    if (!props.edit) return;

    setIsLoading(true);

    apiInstance
      .delete(`/${client}/audiences?audience_id=${props.edit.audience_id}`, {})
      .then((res) => {
        if (res.status === 'OK') {
          handleClose();
        }
      })
      .catch((e) => {
        alert(e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [client, props.edit]);

  const handleSubmit = useCallback(() => {
    setIsLoading(true);
    setError(null);

    const url = `/${client}/audiences`;
    const body = JSON.stringify({
      audience_id: id,
      audience_name: name,
      audience_description: description,
    });

    const request = props.edit
      ? apiInstance.put(url, {
          body,
        })
      : apiInstance.post(url, {
          body,
        });

    request
      .then((res) => {
        handleClose();
      })
      .catch((e) => {
        setError(e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [client, name, id, description, props.edit, handleClose]);

  const onDescriptionChange = useCallback((e, val) => {
    setDescription(val.value);
  }, []);

  const onNameChange = useCallback((e, val) => {
    setName(val.value);
  }, []);

  const onIdChange = useCallback((e, val) => {
    setId(val.value);
  }, []);

  return (
    <Modal
      centered={false}
      trigger={
        <Button basic={!!props.edit} primary size="tiny" onClick={handleOpen}>
          {props.edit ? t('edit') : t('add_audience')}
        </Button>
      }
      open={modalOpen}
      onClose={handleClose}
      size="tiny"
    >
      <Header content={t('audience_modal_title')} />
      <Modal.Content>
        {!!error && <div className="error-message">{error.error.description}</div>}
        <div className="add-token-modal">
          <Input
            loading={isLoading}
            disabled={!!result.length || Boolean(props.edit)}
            value={id}
            onChange={onIdChange}
            placeholder={t('id')}
          />
          <Divider />
          <Input
            loading={isLoading}
            disabled={!!result.length}
            value={name}
            onChange={onNameChange}
            placeholder={t('name')}
          />
          <Divider />
          <Input
            loading={isLoading}
            disabled={!!result.length}
            value={description}
            onChange={onDescriptionChange}
            placeholder={t('description')}
          />
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button color="green" onClick={!!result.length ? handleClose : handleSubmit} loading={isLoading}>
          {props.edit ? t('edit_audience') : t('add_audience')}
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
