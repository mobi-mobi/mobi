import React, { useEffect, useState, useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Header, Modal, Input, Select, Message } from 'semantic-ui-react';
import { useSelector, useDispatch } from 'react-redux';

import { ZoneType, ZoneTypeEnum } from '../../../../containers/zones/types';
import { addZone, resetPopup, checkIdExist } from '../../../../containers/zones/actions';

import useDebounce from '../../../../hooks/useDebounce';
import NumberField from '../../../../components/common/NumberField';

import ZoneSettingsFields from './ZoneSettingsFields';

import StringProcessor from '../../../../classes/StringProcessor';

import './styles.scss';

type ZoneRequestStatus = {
  isLoading: boolean;
  success: boolean;
  error: boolean;
};

export default function AddZoneModal() {
  const { t } = useTranslation();
  const [modalOpen, setModalOpen] = useState<boolean>();
  const [fieldIdIsChanged, setFieldIdIsChanged] = useState<boolean>(false);

  const [idIsValid, setIdIsValid] = useState<boolean>(false);
  const [idIsValidating, setIdIsValidating] = useState<boolean>(false);

  const dispatch = useDispatch();
  const client: string = useSelector((state: any) => state.home.client);

  const zoneRequestStatus: ZoneRequestStatus = useSelector((state: any) => ({
    isLoading: state.zones.addZoneIsLoading,
    success: state.zones.success,
    error: state.zones.success,
  }));

  const [newZoneData, setNewZoneData] = useState<ZoneType>({
    zone_id: '',
    name: '',
    zone_type: ZoneTypeEnum.BASKET_RECOMMENDATIONS,
    description: '',
    zone_settings: {
      recommendations_num: 10,
    },
  });

  const debouncedSearchTerm = useDebounce(newZoneData.zone_id, 300);

  const handleOpen = useCallback(() => setModalOpen(true), []);

  const handleClose = useCallback(() => {
    setNewZoneData({
      zone_id: '',
      name: '',
      zone_type: ZoneTypeEnum.BASKET_RECOMMENDATIONS,
      description: '',
      zone_settings: {
        recommendations_num: 10,
      },
    });

    setModalOpen(false);
  }, []);

  useEffect(() => {
    (async function check() {
      if (debouncedSearchTerm) {
        setIdIsValidating(true);

        const val = await checkIdExist(client, debouncedSearchTerm);

        setIdIsValid(false);
        setIdIsValidating(false);

        setIdIsValid(val);
      }
    })();
  }, [debouncedSearchTerm, client]);

  useEffect(() => {
    if (zoneRequestStatus.success) {
      setTimeout(() => {
        resetPopup()(dispatch);
        handleClose();
      }, 1500);
    }
  }, [zoneRequestStatus.success, dispatch, handleClose]);

  const handleChangeField = useCallback(
    (name, value) => {
      if (name === 'zone_id') {
        setFieldIdIsChanged(true);

        setNewZoneData((s) => ({
          ...s,
          zone_id: StringProcessor.urlReplacer(value),
        }));
      } else if (name === 'name' && !fieldIdIsChanged) {
        setNewZoneData((s) => ({
          ...s,
          zone_id: StringProcessor.urlReplacer(value),
          name: value,
        }));
      } else {
        setNewZoneData((s) => ({
          ...s,
          [name]: value,
        }));
      }
    },
    [fieldIdIsChanged]
  );

  const handleChangeSettings = useCallback((name, field, isNumber) => {
    if (field === '' || (/^[0-9\b]+$/.test(field) && isNumber)) {
      setNewZoneData((s) => ({
        ...s,
        zone_settings: {
          ...s.zone_settings,
          [name]: parseInt(field, 10),
        },
      }));
    } else {
      setNewZoneData((s) => ({
        ...s,
        zone_settings: {
          ...s.zone_settings,
          [name]: field,
        },
      }));
    }
  }, []);

  const handleSubmit = useCallback(() => {
    addZone(newZoneData, client)(dispatch);
  }, [newZoneData, dispatch, client]);

  const buttonValidation = useMemo(() => {
    return (
      (newZoneData.zone_id.length &&
        newZoneData.name.length &&
        newZoneData.zone_settings.recommendations_num &&
        newZoneData.zone_type.length &&
        newZoneData?.description?.length) ||
      zoneRequestStatus.success
    );
  }, [newZoneData, zoneRequestStatus.success]);

  return (
    <Modal
      centered={false}
      trigger={
        <Button primary onClick={handleOpen}>
          {t('zone_create')}
        </Button>
      }
      open={modalOpen}
      onClose={handleClose}
      size="tiny"
    >
      <Header content={t('add_zone_modal_title')} />
      <Modal.Content>
        <div className="add-fluid-card-modal-content">
          <label>{t('zone_input_name_label')}</label>
          <Input
            loading={zoneRequestStatus.isLoading}
            disabled={zoneRequestStatus.success}
            value={newZoneData.name}
            onChange={(e) => handleChangeField('name', e.currentTarget.value)}
          />

          <label>{t('zone_input_description_label')}</label>
          <Input
            loading={zoneRequestStatus.isLoading}
            disabled={zoneRequestStatus.success}
            value={newZoneData.description}
            onChange={(e) => handleChangeField('description', e.currentTarget.value)}
          />

          <label>{t('zone_input_id_label')}</label>
          <Input
            loading={zoneRequestStatus.isLoading || idIsValidating}
            disabled={zoneRequestStatus.success || idIsValidating}
            value={newZoneData.zone_id}
            error={idIsValid}
            onChange={(e) => handleChangeField('zone_id', e.currentTarget.value)}
          />
          {idIsValid && <label className="label-error">{t('zone_input_id_is_exists')}</label>}

          <label>{t('zone_input_type_label')}</label>
          <Select
            loading={zoneRequestStatus.isLoading}
            disabled={zoneRequestStatus.success}
            value={newZoneData.zone_type}
            onChange={(e, data) => handleChangeField('zone_type', data.value)}
            options={[
              {
                key: ZoneTypeEnum.BASKET_RECOMMENDATIONS,
                value: ZoneTypeEnum.BASKET_RECOMMENDATIONS,
                text: t(ZoneTypeEnum.BASKET_RECOMMENDATIONS),
              },
              {
                key: ZoneTypeEnum.MOST_POPULAR,
                value: ZoneTypeEnum.MOST_POPULAR,
                text: t(ZoneTypeEnum.MOST_POPULAR),
              },
              {
                key: ZoneTypeEnum.PERSONAL_RECOMMENDATIONS,
                value: ZoneTypeEnum.PERSONAL_RECOMMENDATIONS,
                text: t(ZoneTypeEnum.PERSONAL_RECOMMENDATIONS),
              },
              {
                key: ZoneTypeEnum.SIMILAR_PRODUCTS,
                value: ZoneTypeEnum.SIMILAR_PRODUCTS,
                text: t(ZoneTypeEnum.SIMILAR_PRODUCTS),
              },
            ]}
          />

          <NumberField
            label={t('zone_input_rec_number_label')}
            name="recommendations_num"
            InputProps={{
              loading: zoneRequestStatus.isLoading,
              disabled: zoneRequestStatus.success,
              value: newZoneData.zone_settings.recommendations_num,
              pattern: '[0-9]+',
              min: '1',
            }}
            onChange={(name: string, val: number) => {
              handleChangeSettings('recommendations_num', val, true);
            }}
          />
        </div>

        <hr />

        <ZoneSettingsFields
          zoneRequestStatus={zoneRequestStatus}
          settings={newZoneData.zone_settings}
          onChange={handleChangeSettings}
          zone={newZoneData}
        />

        {zoneRequestStatus.success && (
          <Message positive>
            <Message.Header>{t('add_zone_modal_success_heading')}</Message.Header>
          </Message>
        )}
      </Modal.Content>
      <Modal.Actions className="flex-end">
        <Button onClick={handleClose} disabled={zoneRequestStatus.isLoading} loading={zoneRequestStatus.isLoading}>
          {t('cancel')}
        </Button>
        <Button
          color="green"
          onClick={handleSubmit}
          disabled={!buttonValidation || idIsValid}
          loading={zoneRequestStatus.isLoading}
        >
          {t('zone_create')}
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
