import React, { useState, useCallback, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { SelectSearchOption } from 'react-select-search';
import { Select, Accordion, Menu, Checkbox } from 'semantic-ui-react';

import DynamicTags from '../../../DynamicTags';
import SelectSearch from '../../../SearchSelect';

import {
  ExtraProductsPositionEnum,
  ZoneSettingsType,
  ZoneType,
  ZoneTypeEnum,
} from '../../../../../containers/zones/types';

import { ProductType } from '../../../../../containers/products/types';

import './styles.scss';

type Props = {
  onChange: Function;
  zone?: ZoneType;
  zoneRequestStatus: {
    isLoading: boolean;
    success: boolean;
  };
  settings: ZoneSettingsType;
};

export default function ZoneSettingsFields({ onChange, zone, zoneRequestStatus, settings }: Props) {
  const [activeIndex, setActiveIndex] = useState(-1);
  const { t } = useTranslation();
  const [products] = useSelector((state: any) => [
    state.home.products,
  ]);

  const handleClick = useCallback(
    (e, titleProps) => {
      const { index } = titleProps;
      const newIndex = activeIndex === index ? -1 : index;

      setActiveIndex(newIndex);
    },
    [activeIndex]
  );

  const isLoading = useMemo(() => Boolean(zoneRequestStatus.isLoading), [zoneRequestStatus]);
  const isSuccess = useMemo(() => Boolean(zoneRequestStatus.success), [zoneRequestStatus]);
  const productsItems = useMemo((): Array<SelectSearchOption> => {
    return products.map((product: ProductType) => ({ value: product.product_id, name: product.title }));
  }, [products]);

  const fields = useMemo(() => {
    const initialDataBrands = settings.brands ? settings.brands.map((v: string, id: number) => ({ name: v, id })) : [];
    const initialDataCategories = settings.categories
      ? settings.categories.map((v: string, id: number) => ({ name: v, id }))
      : [];

    return (
      <div className="additional-settings">
        <div
          className={`additional-settings__field
          ${zone?.zone_type !== ZoneTypeEnum.SIMILAR_PRODUCTS ? 'additional-settings__field--disabled' : ''}`}
        >
          <label htmlFor="brands">{t('same_brand')}</label>
          <Checkbox
            id="brands"
            onChange={(e) => onChange('same_brand', !Boolean(settings.same_brand))}
            disabled={isSuccess || isLoading || zone?.zone_type !== ZoneTypeEnum.SIMILAR_PRODUCTS}
            defaultChecked={Boolean(settings.same_brand)}
          />
        </div>

        <div className="additional-settings__field">
          <label htmlFor="brands">{t('brands')}</label>
          <DynamicTags
            placeholder={t('value')}
            type="brands"
            onChange={(name: string, value: Array<any>) => {
              onChange(name, value, false);
            }}
            initialData={initialDataBrands}
          />
        </div>

        <div className="additional-settings__field">
          <label htmlFor="categories">{t('categories')}</label>
          <DynamicTags
            placeholder={t('value')}
            type="categories"
            initialData={initialDataCategories}
            onChange={(name: string, value: Array<any>) => {
              onChange(name, value, false);
            }}
          />
        </div>

        <div className="additional-settings__field">
          <label htmlFor="extra_product_ids">{t('extra_product_ids')}</label>
          <SelectSearch
            onChange={(name: string, ids: Array<string>) => onChange('extra_product_ids', ids)}
            name="extra_product_ids"
            placeholder={t('extra_product_ids')}
          />
        </div>

        {settings.extra_product_ids?.length && (
          <div className="additional-settings__field">
            <label>{t('extra_products_position')}</label>
            <Select
              placeholder={t('extra_products_position')}
              onChange={(e, data) => onChange('extra_products_position', data.value, false)}
              defaultValue={settings.extra_products_position?.toLowerCase() || undefined}
              options={[
                {
                  key: ExtraProductsPositionEnum.beginning,
                  value: ExtraProductsPositionEnum.beginning,
                  text: t(ExtraProductsPositionEnum.beginning),
                },
                {
                  key: ExtraProductsPositionEnum.end,
                  value: ExtraProductsPositionEnum.end,
                  text: t(ExtraProductsPositionEnum.end),
                },
                {
                  key: ExtraProductsPositionEnum.evenly,
                  value: ExtraProductsPositionEnum.evenly,
                  text: t(ExtraProductsPositionEnum.evenly),
                },
                {
                  key: ExtraProductsPositionEnum.random,
                  value: ExtraProductsPositionEnum.random,
                  text: t(ExtraProductsPositionEnum.random),
                },
              ]}
            />
          </div>
        )}

        <div
          className={`additional-settings__field
          ${!settings.extra_product_ids?.length ? 'additional-settings__field--disabled' : ''}`}
        >
          <label htmlFor="save_organic_extra_products_positions">{t('save_organic_extra_products_positions')}</label>
          <Checkbox
            id="save_organic_extra_products_positions"
            onChange={(e) =>
              onChange(
                'save_organic_extra_products_positions',
                !Boolean(settings.save_organic_extra_products_positions),
                false
              )
            }
            defaultChecked={Boolean(settings.save_organic_extra_products_positions)}
            disabled={isSuccess || isLoading || !settings.extra_product_ids?.length}
          />
        </div>

        <div
          className={`additional-settings__field
          ${!settings.extra_product_ids?.length ? 'additional-settings__field--disabled' : ''}`}
        >
          <label htmlFor="dont_show_organic_products">{t('dont_show_organic_products')}</label>
          <Checkbox
            id="dont_show_organic_products"
            defaultChecked={Boolean(settings.dont_show_organic_products)}
            onChange={(e) =>
              onChange('dont_show_organic_products', !Boolean(settings.dont_show_organic_products), false)
            }
            disabled={isSuccess || isLoading || !settings.extra_product_ids?.length}
          />
        </div>
      </div>
    );
  }, [isLoading, isSuccess, settings, zone, onChange, t]);

  return (
    <Accordion as={Menu} fluid vertical>
      <Menu.Item>
        <Accordion.Title
          active={activeIndex === 0}
          content={t('zone_add_modal_additional_settings')}
          index={0}
          onClick={handleClick}
        />
        <Accordion.Content active={activeIndex === 0} content={fields} />
      </Menu.Item>
    </Accordion>
  );
}
