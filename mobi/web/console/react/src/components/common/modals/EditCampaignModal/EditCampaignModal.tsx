import React, { useState, useCallback, useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Header, Modal, Input, Message } from 'semantic-ui-react';
import { useSelector, useDispatch } from 'react-redux';
import DatePicker from 'react-datepicker';
import dayjs from 'dayjs';

import ZoneSettingsFields from '../AddZone/ZoneSettingsFields';

import { CampaignType } from '../../../../containers/campaign/types';
import { postCampaign, deleteCampaign } from '../../../../containers/campaign/actions';

type CampaignRequestStatus = {
  isLoading: boolean;
  success: boolean;
  error: boolean;
};

type Props = {
  campaign: CampaignType;
};

export default function EditCampaignModal({ campaign }: Props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [newCampaign, setNewCampaign] = useState<CampaignType>(campaign);
  const [modalOpen, setModalOpen] = useState<boolean>();
  const [dateRange, setDateRange] = useState<any>({
    start: campaign.start_date ? dayjs(campaign.start_date).toDate() : new Date(),
    end: campaign.end_date ? dayjs(campaign.end_date).toDate() : dayjs(new Date()).add(1, 'day').toDate(),
  });

  // eslint-disable-next-line
  const client: string = useSelector((state: any) => state.home.client);

  const campaignRequestStatus: CampaignRequestStatus = useSelector((state: any) => ({
    isLoading: state.campaign.addZoneIsLoading,
    success: state.campaign.success,
    error: state.campaign.success,
  }));

  useEffect(() => {
    const end_date = dayjs(dateRange.end).format('YYYY-MM-DD HH:mm:ss');
    const start_date = dayjs(dateRange.start).format('YYYY-MM-DD HH:mm:ss');

    setNewCampaign((s: any) => ({
      ...s,
      end_date,
      start_date,
    }));
  }, [dateRange]);

  const onChangeEnd = useCallback((date: any) => {
    setDateRange((dates: any) => ({
      ...dates,
      end: date,
    }));
  }, []);

  const onChangeStart = useCallback((date: any) => {
    setDateRange((dates: any) => ({
      ...dates,
      start: date,
    }));
  }, []);

  const handleClose = useCallback(() => {
    setNewCampaign({
      name: '',
      campaign_id: '',
      start_date: '',
      end_date: '',
      zone_settings: {
        recommendations_num: 10,
      },
    });

    setModalOpen(false);
  }, []);

  const handleChangeField = useCallback((name, value) => {
    setNewCampaign((s: any) => ({
      ...s,
      [name]: value,
    }));
  }, []);

  const handleChangeSettings = useCallback((name, field, isNumber) => {
    if (field === '' || (/^[0-9\b]+$/.test(field) && isNumber)) {
      setNewCampaign((s: any) => ({
        ...s,
        zone_settings: {
          ...s.zone_settings,
          [name]: parseInt(field, 10),
        },
      }));
    } else {
      setNewCampaign((s: any) => ({
        ...s,
        zone_settings: {
          ...s.zone_settings,
          [name]: field,
        },
      }));
    }
  }, []);

  const handleDelete = useCallback(() => {
    if (newCampaign && newCampaign.campaign_id) {
      deleteCampaign(client, newCampaign.campaign_id)(dispatch);
    }
  }, [dispatch, newCampaign, client]);

  const handleSubmit = useCallback(() => {
    postCampaign(newCampaign, client, false)(dispatch);
  }, [dispatch, newCampaign, client]);

  useEffect(() => {
    if (campaignRequestStatus.success) {
      setTimeout(() => {
        handleClose();
      }, 1500);
    }
  }, [campaignRequestStatus.success, dispatch, handleClose]);

  const buttonValidation = useMemo(() => !newCampaign.name.length, [newCampaign]);

  return (
    <Modal
      centered={false}
      trigger={
        <Button primary compact>
          {t('campaign_listing_edit')}
        </Button>
      }
      open={modalOpen}
      onClose={handleClose}
      size="tiny"
    >
      <Header content={t('edit_campaign_modal_title')} />
      <Modal.Content>
        <div className="add-fluid-card-modal-content">
          <label>{t('campaign_input_name_label')}</label>
          <Input
            placeholder={t('add_campaign_modal_name')}
            onChange={(e) => handleChangeField('name', e.currentTarget.value)}
            value={newCampaign.name}
          />

          <label>{t('campaign_input_description_label')}</label>
          <Input
            placeholder={t('add_campaign_modal_description')}
            onChange={(e) => handleChangeField('description', e.currentTarget.value)}
            value={newCampaign.description}
          />

          <div className="timerange-select">
            <label className="timerange-select__headline">{t('campaign_input_date_range_label')}</label>

            <label>{t('campaign_input_start_date_range_label')}</label>
            <DatePicker
              selected={dateRange.start}
              onChange={onChangeStart}
              timeInputLabel="Time:"
              dateFormat="yyyy-MM-dd HH:mm:ss"
              showTimeInput
            />

            <label>{t('campaign_input_end_date_range_label')}</label>
            <DatePicker
              selected={dateRange.end}
              onChange={onChangeEnd}
              minDate={dateRange.end}
              timeInputLabel="Time:"
              dateFormat="yyyy-MM-dd HH:mm:ss"
              showTimeInput
            />
          </div>

          <hr />

          <ZoneSettingsFields
            zoneRequestStatus={campaignRequestStatus}
            settings={newCampaign.zone_settings}
            onChange={(name: any, value: any) => handleChangeSettings(name, value, false)}
          />
        </div>

        {campaignRequestStatus.success && (
          <Message positive>
            <Message.Header>{t('add_campaign_modal_success_heading')}</Message.Header>
          </Message>
        )}
      </Modal.Content>
      <Modal.Actions className="flex-jc">
        <Button color="red" onClick={handleDelete} disabled={buttonValidation}>
          {t('campaign_delete')}
        </Button>
        <Button color="green" onClick={handleSubmit} disabled={buttonValidation}>
          {t('campaign_save')}
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
