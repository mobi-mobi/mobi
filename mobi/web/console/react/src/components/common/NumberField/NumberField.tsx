import React, { useState, useRef, useCallback, useEffect } from 'react';
import { Input } from 'semantic-ui-react';

type Props = {
  InputProps?: any;
  onChange: Function;
  name: string;
  label?: string;
  defaultValue?: number;
};

export default function NumberField({ InputProps, name, onChange, defaultValue, label }: Props) {
  const [val, setVal] = useState(defaultValue || 1);
  const isInitialMount = useRef(true);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      onChange(name, val, true);
    }
  }, [name, val]);

  const handleKeyPress = useCallback((e: any) => {
    if (e.key === '0' && !Boolean(e?.currentTarget?.value)) {
      e.preventDefault();
    }
  }, []);

  const handleOnBlur = useCallback((e) => {
    if (!Boolean(e?.currentTarget?.value)) {
      setVal(1);
    }
  }, []);

  const localOnChange = useCallback(
    (e) => {
      const { value } = e.currentTarget;

      if (value === 0) {
        e.preventDefault();
      } else {
        setVal(e.currentTarget.value);
      }
    },
    []
  );

  return (
    <>
      {label && <label>{label}</label>}
      <Input
        onKeyPress={handleKeyPress}
        onChange={localOnChange}
        onBlur={handleOnBlur}
        {...InputProps}
        min="1"
        type="number"
      />
    </>
  );
}
