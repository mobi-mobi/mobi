import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

import Empty from '../Empty';
import CampaignItem from './CampaignItem';

import LoadingSpinner from '../../ui/LoadingSpinner';

import { CampaignTypeFull } from '../../../containers/campaign/types';

type Props = {
  items: Array<CampaignTypeFull>;
  emptyText: string;
};

export default function Campaigns({ items, emptyText }: Props) {
  const campaignsLoading: boolean = useSelector((state: any) => state.campaign.campaignsLoading);

  const renderItems = useMemo(() => {
    if (campaignsLoading) {
      return <LoadingSpinner relative size="small" />;
    } else if (items && !items.length) {
      return <Empty message={emptyText} />;
    } else {
      return (
        items &&
        items.map((item: CampaignTypeFull) => <CampaignItem key={item?.campaign.campaign_id} fullCampaign={item} />)
      );
    }
  }, [items, campaignsLoading, emptyText]);

  return <div className="fluid-cards-list-items">{renderItems}</div>;
}
