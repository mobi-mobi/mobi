/** @TODO: Refactor this component, make special component for edit field, combine it with zone settings component  */
import React, { useMemo, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { Checkbox, Input, Select, Button } from 'semantic-ui-react';
import { SelectSearchOption } from 'react-select-search';
import Icon from 'react-eva-icons';
import DatePicker from 'react-datepicker';
import dayjs from 'dayjs';

import SelectSearch from '../../SearchSelect';
import NumberField from '../../NumberField';
import DynamicTags from '../../DynamicTags';

import { ExtraProductsPositionEnum } from '../../../../containers/zones/types';
import { postCampaign } from '../../../../containers/campaign/actions';
import { ProductType } from '../../../../containers/products/types';
import { CampaignType } from '../../../../containers/campaign/types';

import './styles.scss';

type Props = {
  campaign: CampaignType;
};

type SettingsFieldType = { input: string; type?: string; name?: string; show: Function; mapping?: Function };

const SETTINGS_FIELD_MAP = {
  recommendations_num: {
    show: () => true,
    input: 'text',
    type: 'number',
  },
  same_brand: {
    input: 'checkbox',
    show: () => true,
  },
  brands: {
    show: () => true,
    input: 'tags',
    name: 'brands',
  },
  categories: {
    show: () => true,
    input: 'tags',
    name: 'categories',
  },
  extra_product_ids: {
    show: () => true,
    input: 'select',
    type: 'search',
  },
  extra_products_position: {
    show: () => true,
    input: 'select',
    type: 'default',
  },
  save_organic_extra_products_positions: {
    show: () => true,
    input: 'checkbox',
  },
  dont_show_organic_products: {
    show: () => true,
    input: 'checkbox',
  },
};

export default function Settings({ campaign }: Props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [localCampaign, setLocalCampaign] = useState<CampaignType>(campaign);
  const [editableFields, setEditableFields] = useState({});
  const { campaignIsUpdating, client, products } = useSelector((state: any) => ({
    campaignIsUpdating: state.campaign.campaignIsUpdating,
    client: state.home.client,
    products: state.home.products,
  }));

  useEffect(() => {
    postCampaign(localCampaign, client, false)(dispatch);
  }, [localCampaign, client, dispatch]);

  const applyChanges = useCallback(
    (key: string, isZoneSettings: boolean = true) => {
      if (isZoneSettings) {
        setLocalCampaign((prev) => ({
          ...prev,
          zone_settings: {
            ...prev.zone_settings,
            [key]: key === 'recommendations_num' ? parseInt(editableFields[key].next, 10) : editableFields[key].next,
          },
        }));
      } else {
        setLocalCampaign((prev) => ({
          ...prev,
          [key]: editableFields[key].next,
          zone_settings: {
            ...prev.zone_settings,
          },
        }));
      }

      setEditableFields((prevState) => ({
        ...prevState,
        [key]: null,
      }));
    },
    [editableFields]
  );

  const discardChanges = useCallback((key) => {
    setEditableFields((prevState) => ({
      ...prevState,
      [key]: null,
    }));
  }, []);

  const changeValue = useCallback(
    (key: string, value: any, isZoneSettings: boolean = true) => {
      setEditableFields((prevState) => ({
        ...prevState,
        [key]: {
          prev: isZoneSettings ? localCampaign.zone_settings[key] : localCampaign[key],
          next: value,
        },
      }));
    },
    [localCampaign]
  );

  const handleAddToEdit = useCallback(
    (key: string, isZoneSettings: boolean = true) => {
      setEditableFields((prevState) => ({
        ...prevState,
        [key]: {
          prev: isZoneSettings ? localCampaign.zone_settings[key] : localCampaign[key],
          next: isZoneSettings ? localCampaign.zone_settings[key] : localCampaign[key],
        },
      }));
    },
    [localCampaign]
  );

  const productsItems = useMemo((): Array<SelectSearchOption> => {
    return products ? products.map((product: ProductType) => ({ value: product.product_id, name: product.title })) : [];
  }, [products]);

  /** @TODO: move to component */
  const renderSettingField = useCallback(
    (field: SettingsFieldType, key: string) => {
      const value = !Boolean(editableFields[key]?.next) ? '' : editableFields[key].next;
      let label = !Boolean(localCampaign.zone_settings[key]) ? 'N/A' : localCampaign.zone_settings[key];

      if (Array.isArray(label)) {
        label = !label.length ? 'N/A' : label.join(', ');
      }

      if (field.input === 'text') {
        if (field.type === 'number') {
          return (
            <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
              <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
                {label} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <NumberField
                  name={key}
                  InputProps={{
                    value: value,
                    pattern: '[0-9]+',
                    min: '1',
                  }}
                  onChange={(name: string, val: number) => {
                    changeValue(key, val);
                  }}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          );
        } else {
          return (
            <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
              <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
                {label} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <Input
                  placeholder={t('value')}
                  type={field.type}
                  value={value}
                  onChange={(e) => changeValue(key, e.currentTarget.value)}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          );
        }
      } else if (field.input === 'tags' && field.name) {
        const initialData = localCampaign.zone_settings[key]?.map((v: string, id: number) => ({ name: v, id }));

        return (
          <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
            <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
              {label} <Icon name="edit-outline" size="small" fill="#777" />
            </div>

            <div className="editable-static-field__input">
              <DynamicTags
                placeholder={t('value')}
                type={field.name}
                initialData={initialData || []}
                onChange={(name: string, value: Array<any>) => {
                  changeValue(key, value);
                }}
                mapping={field?.mapping}
              />
              <div className="editable-static-field__input-actions">
                <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                  <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                </Button>
                <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                  <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                </Button>
              </div>
            </div>
          </div>
        );
      } else if (field.input === 'select') {
        if (field.type === 'search') {
          return (
            <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
              <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
                {label} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <SelectSearch
                  onChange={(name: string, ids: Array<string>) => changeValue(key, ids)}
                  name={key}
                  placeholder={t(key)}
                  initialValue={value}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          );
        } else {
          return (
            <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
              <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
                {t(label)} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <Select
                  placeholder={t('value')}
                  onChange={(e, data) => changeValue(key, data.value)}
                  options={[
                    {
                      key: ExtraProductsPositionEnum.beginning,
                      value: ExtraProductsPositionEnum.beginning,
                      text: t(ExtraProductsPositionEnum.beginning),
                    },
                    {
                      key: ExtraProductsPositionEnum.end,
                      value: ExtraProductsPositionEnum.end,
                      text: t(ExtraProductsPositionEnum.end),
                    },
                    {
                      key: ExtraProductsPositionEnum.evenly,
                      value: ExtraProductsPositionEnum.evenly,
                      text: t(ExtraProductsPositionEnum.evenly),
                    },
                    {
                      key: ExtraProductsPositionEnum.random,
                      value: ExtraProductsPositionEnum.random,
                      text: t(ExtraProductsPositionEnum.random),
                    },
                  ]}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          );
        }
      } else if (field.input === 'checkbox') {
        return (
          <div className={`editable-static-field ${editableFields[key] ? 'editable-static-field--active' : ''}`}>
            <div className="editable-static-field__value" onClick={() => handleAddToEdit(key)}>
              {Boolean(localCampaign.zone_settings[key]) ? t('enabled') : t('disabled')}{' '}
              <Icon name="edit-outline" size="small" fill="#777" />
            </div>
            <div className="editable-static-field__input">
              <Checkbox
                defaultChecked={Boolean(localCampaign.zone_settings[key])}
                onChange={() => changeValue(key, !Boolean(localCampaign.zone_settings[key]))}
              />
              <div className="editable-static-field__input-actions">
                <Button size="mini" color="green" onClick={() => applyChanges(key)}>
                  <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                </Button>
                <Button size="mini" color="grey" onClick={() => discardChanges(key)}>
                  <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                </Button>
              </div>
            </div>
          </div>
        );
      }
    },
    [
      t,
      localCampaign,
      applyChanges,
      changeValue,
      discardChanges,
      campaignIsUpdating,
      editableFields,
      handleAddToEdit,
      productsItems,
    ]
  );

  const changeDate = useCallback(
    (key, date) => {
      const actualDate = dayjs(date).format('YYYY-MM-DD HH:mm:ss');

      changeValue(key, actualDate, false);
    },
    [changeValue]
  );

  const zoneSettings = useMemo(() => {
    const keys = Object.keys(campaign.zone_settings);

    const field = (key: string) => renderSettingField(SETTINGS_FIELD_MAP[key], key);

    return keys
      .map((key) => {
        const show = field(key);

        return (
          show && (
            <div className="fluid-card-setting" key={key}>
              <div className="fluid-card-setting__name">{t(key)}:</div>
              <div className="fluid-card-setting__value">{field(key)}</div>
            </div>
          )
        );
      })
      .filter(Boolean);
  }, [campaign, renderSettingField, t]);

  return (
    <div className="fluid-card-more-content__fluid-card-settings">
      <div className="fluid-card-more-content__main-settings-container">
        <div className="fluid-card-setting">
          <div className="fluid-card-setting__name">{t('description')}:</div>
          <div className="fluid-card-setting__value">
            <div
              className={`editable-static-field ${
                editableFields['description'] ? 'editable-static-field--active' : ''
              }`}
            >
              <div className="editable-static-field__value" onClick={() => handleAddToEdit('description', false)}>
                {localCampaign.description} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <Input
                  placeholder={t('value')}
                  type="text"
                  value={!Boolean(editableFields['description']?.next) ? '' : editableFields['description'].next}
                  onChange={(e) => changeValue('description', e.currentTarget.value, false)}
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges('description', false)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges('description')}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="fluid-card-setting">
          <div className="fluid-card-setting__name">{t('start_date')}:</div>
          <div className="fluid-card-setting__value">
            <div
              className={`editable-static-field ${editableFields['start_date'] ? 'editable-static-field--active' : ''}`}
            >
              <div className="editable-static-field__value" onClick={() => handleAddToEdit('start_date', false)}>
                {localCampaign.start_date} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <DatePicker
                  selected={
                    !Boolean(editableFields['start_date']?.next)
                      ? ''
                      : dayjs(editableFields['start_date'].next).toDate()
                  }
                  onChange={(date: Date) => changeDate('start_date', date)}
                  timeInputLabel="Time:"
                  dateFormat="yyyy-MM-dd HH:mm:ss"
                  showTimeInput
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges('start_date', false)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges('start_date')}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="fluid-card-setting">
          <div className="fluid-card-setting__name">{t('end_date')}:</div>
          <div className="fluid-card-setting__value">
            <div
              className={`editable-static-field ${editableFields['end_date'] ? 'editable-static-field--active' : ''}`}
            >
              <div className="editable-static-field__value" onClick={() => handleAddToEdit('end_date', false)}>
                {localCampaign.end_date} <Icon name="edit-outline" size="small" fill="#777" />
              </div>

              <div className="editable-static-field__input">
                <DatePicker
                  selected={
                    !Boolean(editableFields['end_date']?.next) ? '' : dayjs(editableFields['end_date'].next).toDate()
                  }
                  onChange={(date: Date) => changeDate('end_date', date)}
                  timeInputLabel="Time:"
                  dateFormat="yyyy-MM-dd HH:mm:ss"
                  showTimeInput
                />
                <div className="editable-static-field__input-actions">
                  <Button size="mini" color="green" onClick={() => applyChanges('end_date', false)}>
                    <Icon name="checkmark-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                  <Button size="mini" color="grey" onClick={() => discardChanges('end_date')}>
                    <Icon name="close-outline" size="medium" fill="#fff" loading={campaignIsUpdating} />
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="fluid-card-more-content__fluid-card-settings-container">
        <h3>{t('campaign_zone_settings')}:</h3>
        {zoneSettings}
      </div>
    </div>
  );
}
