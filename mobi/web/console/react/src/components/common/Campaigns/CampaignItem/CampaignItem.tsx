import React, { useState, useCallback } from 'react';
import { Button, Modal } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { deleteCampaign, postCampaign } from '../../../../containers/campaign/actions';
import { CampaignStatusEnum, CampaignTypeFull } from '../../../../containers/campaign/types';

import ZoneCharts from '../../Zones/ZoneCharts';
import CampaignSettings from '../../Campaigns/Settings';

type Props = {
  fullCampaign: CampaignTypeFull;
};

type Store = {
  client: string;
  campaignIsDeleting: boolean;
};

export default function CampaignItem({ fullCampaign }: Props) {
  const { campaign } = fullCampaign;
  const [localFullCampaign, setLocalFullCampaign] = useState<CampaignTypeFull>(fullCampaign);
  const [expanded, setExpanded] = useState<boolean>(false);
  const [showDeleteModal, setShowDeleteModal] = useState<boolean>(false);
  const [showActivityModal, setShowActivityModal] = useState<boolean>(false);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const store: Store = useSelector((state: any) => ({
    client: state.home.client,
    campaigns: state.campaign.campaigns,
    campaignIsDeleting: state.campaign.campaignIsDeleting,
  }));
  const client: string = useSelector((state: any) => state.home.client);

  const handleChangeActivity = useCallback(
    (status) => {
      const updatedCampaign = {
        ...campaign,
        status,
      };

      postCampaign(updatedCampaign, client, false)(dispatch);
      setShowActivityModal(false);
      setLocalFullCampaign((prev) => ({
        ...prev,
        campaign: updatedCampaign,
      }));
    },
    [campaign, client, dispatch]
  );

  const handleMoreClick = useCallback(() => {
    setExpanded(!expanded);
  }, [expanded]);

  const handleDeleteCampaign = useCallback(
    (id: string | undefined) => {
      if (!id) return;

      deleteCampaign(store.client, id)(dispatch);
    },
    [dispatch, store.client]
  );

  if (!campaign) return null;

  return (
    <div className="fluid-card">
      <div className="fluid-card-preview">
        <div className="fluid-card-information">
          <div className="fluid-card-information__name" onClick={handleMoreClick}>
            {campaign.name}
          </div>
        </div>
        <div className="fluid-card-statistic">
          <div className="fluid-card-statistic">
            <strong>{t('zone_displays')}:</strong> {fullCampaign.metrics_single.displays}
          </div>
          <div className="fluid-card-statistic">
            <strong>CTR:</strong> {fullCampaign.metrics_single.ctr}%
          </div>
          <div className="fluid-card-statistic">
            <strong>{t('zone_converted')}:</strong> {fullCampaign.metrics_single.converted}%
          </div>
        </div>
        <div className="fluid-card-actions">
          <Button basic primary content={expanded ? t('less') : t('more')} size="tiny" onClick={handleMoreClick} />
        </div>
      </div>
      {expanded && (
        <div className="fluid-card-more-content">
          <div className="fluid-card-more-content-columns">
            <div className="fluid-card-more-content-columns__column">
              <ZoneCharts type="campaign" id={fullCampaign.campaign.campaign_id} data={fullCampaign.metrics_graph} />
            </div>
            <div className="fluid-card-more-content-columns__column">
              <div className="fluid-card-more-content__additional-content">
                <div className="fluid-card-more-content__description">{campaign.description}</div>
                <CampaignSettings campaign={campaign} />
              </div>
              <div className="fluid-card-more-actions">
                <div className="fluid-card-more-actions__toggle">
                  <Modal
                    open={showActivityModal}
                    onClose={() => setShowActivityModal(false)}
                    size="mini"
                    trigger={
                      <Button
                        onClick={() => setShowActivityModal(true)}
                        color={
                          localFullCampaign.campaign.status &&
                          localFullCampaign.campaign.status === CampaignStatusEnum.ACTIVE
                            ? 'red'
                            : 'green'
                        }
                      >
                        {localFullCampaign.campaign.status &&
                        localFullCampaign.campaign.status === CampaignStatusEnum.ACTIVE
                          ? t('campaign_toggle_activity_deactivate')
                          : t('campaign_toggle_activity_activate')}
                      </Button>
                    }
                  >
                    <Modal.Header>{t('campaign_prompt_confirm_action')}</Modal.Header>
                    <Modal.Content>
                      <p
                        dangerouslySetInnerHTML={{
                          __html:
                            campaign.status && campaign.status === CampaignStatusEnum.ACTIVE
                              ? t('campaign_prompt_deactivate_body')
                              : t('campaign_prompt_activate_body'),
                        }}
                      />
                    </Modal.Content>
                    <Modal.Actions>
                      <Button onClick={() => setShowActivityModal(false)}>{t('cancel')}</Button>
                      <Button
                        onClick={() =>
                          handleChangeActivity(
                            localFullCampaign.campaign.status === CampaignStatusEnum.ACTIVE
                              ? CampaignStatusEnum.DISABLED
                              : CampaignStatusEnum.ACTIVE
                          )
                        }
                        color={
                          localFullCampaign.campaign.status &&
                          localFullCampaign.campaign.status === CampaignStatusEnum.ACTIVE
                            ? 'red'
                            : 'green'
                        }
                      >
                        {localFullCampaign.campaign.status &&
                        localFullCampaign.campaign.status === CampaignStatusEnum.ACTIVE
                          ? t('campaign_toggle_activity_deactivate')
                          : t('campaign_toggle_activity_activate')}
                      </Button>
                    </Modal.Actions>
                  </Modal>
                </div>

                <div className="fluid-card-more-actions__delete">
                  <Modal
                    open={showDeleteModal}
                    onClose={() => setShowDeleteModal(false)}
                    size="mini"
                    trigger={
                      <Button onClick={() => setShowDeleteModal(true)} color="red">
                        {t('campaign_delete')}
                      </Button>
                    }
                  >
                    <Modal.Header>{t('campaign_prompt_delete_title')}</Modal.Header>
                    <Modal.Content>
                      <p dangerouslySetInnerHTML={{ __html: t('campaign_prompt_delete_body') }} />
                    </Modal.Content>
                    <Modal.Actions>
                      <Button onClick={() => setShowDeleteModal(false)}>{t('cancel')}</Button>
                      <Button onClick={() => handleDeleteCampaign(campaign.campaign_id)} color="red">
                        {t('campaign_prompt_delete_button_text')}
                      </Button>
                    </Modal.Actions>
                  </Modal>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
