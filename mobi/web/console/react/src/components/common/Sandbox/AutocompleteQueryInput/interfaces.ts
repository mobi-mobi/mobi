import { ProductType } from '../../../../containers/products/types';

export interface IAutocompleteQueryInputProps {
  onChange: Function;
  max: number;
  texts: {
    noResults: string;
    button: string;
  };
}

export interface SelectedProductType {
  value: ProductType;
  id: number;
}
