import React, { useState, useCallback } from 'react';
import { Button } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import BrandsSearchInput from '../../BrandsSearchInput';
import BrowserHistorySearchInput from '../BrowserHistorySelect';
import { SelectedProductType } from '../BrowserHistorySelect/BrowserHistorySelect';

import { setSandboxBrands, setUserHistory } from '../../../../containers/sandbox/actions';

type Props = {
  actions?: Array<any>;
};

export default function SandboxSettings({ actions }: Props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [showSettings, setShowSettings] = useState<boolean>(false);
  const count: number = useSelector(
    (state: any) => [state.sandbox.sandboxBrands?.length, state.sandbox.userHistory?.length].filter(Boolean).length
  );

  const handleChangeBrowserHistory = useCallback(
    (value: Array<SelectedProductType>) => {
      setUserHistory(value)(dispatch);
    },
    [dispatch]
  );

  const handleBrandsChange = useCallback(
    (name) => {
      setSandboxBrands(name)(dispatch);
    },
    [dispatch]
  );

  const handleChangeSettingsVisibility = useCallback(() => {
    setShowSettings(!showSettings);
  }, [showSettings]);

  return (
    <div className="sandbox-settings">
      <div className="sandbox-settings__actions">
        {actions}
        <Button onClick={handleChangeSettingsVisibility} primary>
          {t('settings')} {count > 0 ? `(${count})` : null}
        </Button>
      </div>

      {showSettings ? (
        <div className="sandbox-settings__panes">
          <div className="sandbox-recommendation">
            <div className="sandbox-recommendation-pane sandbox-recommendation-pane--settings">
              <div className="sandbox-recommendation-settings__brands">
                <BrandsSearchInput max={100} onChange={handleBrandsChange} />
              </div>
            </div>

            <div className="sandbox-recommendation-pane sandbox-recommendation-pane--history">
              <h3>{t('user_browser_history')}</h3>
              <BrowserHistorySearchInput max={10} onChange={handleChangeBrowserHistory} />
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
}
