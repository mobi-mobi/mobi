import React, { useEffect, useCallback, useRef, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Button, Input, Dropdown, Icon } from 'semantic-ui-react';

import { ProductType } from '../../../../containers/products/types';
import useDebounce from '../../../../hooks/useDebounce';

import Api from '../../../../services/Api';

import config from '../../../../config';

import './styles.scss';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

type Store = {
  client: string;
  userHistory: Array<any>;
};

type Props = {
  onChange: Function;
  max: number;
};

export type SelectedProductType = {
  eventType: string;
  value: ProductType;
  id: number;
};

const EVENT_ICONS = {
  product_view: 'eye',
  to_wishlist: 'list',
  to_basket: 'shopping basket',
  sale: 'tag',
};

export default function BrowserHistorySelect({ onChange, max }: Props) {
  const [showAddForm, setShowAddForm] = useState<boolean>(false);
  const [eventType, setEventType] = useState<any>('product_view');
  const [suggestions, setSuggestions] = useState<Array<ProductType>>([]);
  const [selectedItems, setSelectedItems] = useState<Array<SelectedProductType>>([]);
  const [value, setValue] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { t } = useTranslation();
  const isInitialMount = useRef(true);
  const store: Store = useSelector((state: any) => ({
    client: state.home.client,
    userHistory: state.sandbox.userHistory,
  }));

  const debouncedSearchTerm = useDebounce(value, 300);

  useEffect(() => {
    setSelectedItems(store.userHistory || []);
  }, [store.userHistory])

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      if (onChange) {
        onChange(selectedItems);
      }
    }
  }, [selectedItems, onChange]);

  useEffect(() => {
    (async function get() {
      if (debouncedSearchTerm) {
        setIsLoading(true);

        const { data } = await apiInstance.get(`/${store.client}/catalog/products?q=${debouncedSearchTerm}`, {});
        setSuggestions(data.products);

        setIsLoading(false);
      }
    })();
  }, [store.client, debouncedSearchTerm]);

  const filteredSuggestions = useMemo(() => {
    return suggestions.filter(
      (suggestion: ProductType) =>
        !selectedItems.find((item: SelectedProductType) => item.value.title === suggestion.title)
    );
  }, [suggestions, selectedItems]);

  const handleOpenAddForm = useCallback(() => {
    setShowAddForm(true);
  }, []);

  const handleCloseAddForm = useCallback(() => {
    setShowAddForm(false);
  }, []);

  const handleRemoveItem = useCallback(
    (id: number) => {
      setSelectedItems(selectedItems.filter((selectedItem: SelectedProductType) => selectedItem.id !== id));
    },
    [selectedItems]
  );

  const handleChangeEventType = useCallback((e: React.SyntheticEvent<HTMLElement>, { value }: any) => {
    setEventType(value);
  }, []);

  const handleAddBrowserHistory = useCallback(
    async (item: ProductType) => {
      if (selectedItems.length >= max) return;

      setSelectedItems([...selectedItems, { id: selectedItems.length, value: item, eventType: eventType }]);
      handleCloseAddForm();
      setValue('');
      setSuggestions([]);
    },
    [selectedItems, handleCloseAddForm, eventType, max]
  );

  const handleChangeInputValue = useCallback(async (e) => {
    setValue(e.currentTarget.value);
  }, []);

  const options = useMemo(
    () => [
      { key: 'product_view', value: 'product_view', text: t('product_view'), icon: 'eye' },
      { key: 'to_wishlist', value: 'to_wishlist', text: t('to_wishlist'), icon: 'heart' },
      { key: 'to_basket', value: 'to_basket', text: t('to_basket'), icon: 'shopping basket' },
      { key: 'product_sale', value: 'product_sale', text: t('sale'), icon: 'credit card' },
    ],
    [t]
  );

  return (
    <div className="browser-history-list">
      <div className="browser-history-list__items">
        {selectedItems.length ? (
          selectedItems.map((item: SelectedProductType) => (
            <div key={item.value.product_id} className="browser-history-list-item">
              <div className="browser-history__event-icon">
                <Icon name={EVENT_ICONS[item.eventType]} />
              </div>
              <div className="browser-history__text">
                <span>{item.value.brand}</span>
                {item.value.title}
              </div>
              <Button
                className="browser-history__remove-button"
                icon="delete"
                color="red"
                size="mini"
                onClick={() => handleRemoveItem(item.id)}
              />
            </div>
          ))
        ) : (
          <div>{t('sandbox_no_added_browser_history')}</div>
        )}
      </div>
      <div className="browser-history__add">
        {!showAddForm && (
          <Button onClick={handleOpenAddForm} color="green" disabled={selectedItems.length === max}>
            {t('sandbox_add_browser_history')}
          </Button>
        )}
        {showAddForm && (
          <div className="browser-history__add-form">
            <div className="browser-history-controls">
              <div className="browser-history-controls__event-select">
                <Dropdown
                  text={t(eventType)}
                  icon={EVENT_ICONS[eventType]}
                  floating
                  onChange={handleChangeEventType}
                  labeled
                  button
                  className="icon"
                  options={options}
                  defaultValue={eventType}
                />
              </div>
              <div className="browser-history-controls__input">
                <Input onChange={handleChangeInputValue} loading={isLoading} />
              </div>

              <div className="browser-history-controls__actions">
                <Button onClick={handleCloseAddForm}>{t('cancel')}</Button>
              </div>
            </div>

            {filteredSuggestions.length ? (
              <ul className="bh-list-suggestions">
                {filteredSuggestions.map((suggest: ProductType) => (
                  <li key={suggest.product_id} onClick={() => handleAddBrowserHistory(suggest)} className="suggestion-list-item">
                    <span>{suggest.brand}</span>
                    {suggest.title}
                  </li>
                ))}
              </ul>
            ) : null}
          </div>
        )}
      </div>
    </div>
  );
}
