import React, { useEffect, useCallback, useRef, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Input } from 'semantic-ui-react';

import { ProductType } from '../../../../containers/products/types';
import useDebounce from '../../../../hooks/useDebounce';

import Api from '../../../../services/Api';

import config from '../../../../config';

import './styles.scss';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

type Props = {
  onChange: Function;
};

export default function SimilarProductSelect({ onChange }: Props) {
  const { t } = useTranslation();
  const [suggestions, setSuggestions] = useState<Array<ProductType>>([]);
  const [selectedItem, setSelectedItem] = useState<ProductType | null>(null);
  const [value, setValue] = useState<string>('');
  const [visibleValue, setVisibleValue] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const isInitialMount = useRef(true);
  const client: string = useSelector((state: any) => state.home.client);

  const debouncedSearchTerm = useDebounce(value, 300);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      if (onChange) {
        onChange(selectedItem);
      }
    }
  }, [selectedItem, onChange]);

  useEffect(() => {
    (async function get() {
      if (debouncedSearchTerm) {
        setIsLoading(true);

        const { data } = await apiInstance.get(`/${client}/catalog/products?q=${debouncedSearchTerm}`, {});
        setSuggestions(data.products);

        setIsLoading(false);
      }
    })();
  }, [client, debouncedSearchTerm]);

  const filteredSuggestions = useMemo(() => {
    return suggestions.filter((suggestion: ProductType) => selectedItem?.title !== suggestion.title);
  }, [suggestions, selectedItem]);

  const handleSelectProduct = useCallback(async (item: ProductType) => {
    setSelectedItem(item);
    setVisibleValue(item.title);
    setSuggestions([]);
  }, []);

  const handleChangeInputValue = useCallback(async (e) => {
    setValue(e.currentTarget.value);
    setVisibleValue(e.currentTarget.value);
  }, []);

  return (
    <div className="similar-product-list">
      <div className="similar-product__add">
        <div className="similar-product__add-form">
          <div className="similar-product-controls">
            <div className="similar-product-controls__input">
              <Input
                onChange={handleChangeInputValue}
                value={visibleValue}
                placeholder={t('select_a_product')}
                loading={isLoading}
              />
            </div>
          </div>

          {filteredSuggestions.length ? (
            <ul className="similar-products-suggestions">
              {filteredSuggestions.map((suggest: ProductType) => (
                <li key={suggest.product_id} onClick={() => handleSelectProduct(suggest)} className="suggestion-list-item">
                  <span>{suggest.brand}</span>
                  {suggest.title}
                </li>
              ))}
            </ul>
          ) : null}
        </div>
      </div>
    </div>
  );
}
