import React, { useEffect, useCallback, useRef, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Button, Input } from 'semantic-ui-react';

import { ProductType } from '../../../../containers/products/types';
import useDebounce from '../../../../hooks/useDebounce';

import Api from '../../../../services/Api';

import config from '../../../../config';

import './styles.scss';

import { IAutocompleteQueryInputProps, SelectedProductType } from './interfaces';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function AutocompleteQueryInput({ onChange, texts, max }: IAutocompleteQueryInputProps) {
  const { t } = useTranslation();
  const [showAddForm, setShowAddForm] = useState<boolean>(false);
  const [suggestions, setSuggestions] = useState<Array<ProductType>>([]);
  const [selectedItems, setSelectedItems] = useState<Array<SelectedProductType>>([]);
  const [value, setValue] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const isInitialMount = useRef(true);
  const client: string = useSelector((state: any) => state.home.client);

  const debouncedSearchTerm = useDebounce(value, 300);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      if (onChange) {
        onChange(selectedItems);
      }
    }
  }, [selectedItems, onChange]);

  useEffect(() => {
    (async function get() {
      if (debouncedSearchTerm) {
        setIsLoading(true);

        const { data } = await apiInstance.get(`/${client}/catalog/products?q=${debouncedSearchTerm}`, {});
        setSuggestions(data.products);

        setIsLoading(false);
      }
    })();
  }, [client, debouncedSearchTerm]);

  const filteredSuggestions = useMemo(() => {
    return suggestions.filter(
      (suggestion: ProductType) =>
        !selectedItems.find((item: SelectedProductType) => item.value.title === suggestion.title)
    );
  }, [suggestions, selectedItems]);

  const handleOpenAddForm = useCallback(() => {
    setShowAddForm(true);
  }, []);

  const handleCloseAddForm = useCallback(() => {
    setShowAddForm(false);
  }, []);

  const handleRemoveItem = useCallback(
    (id: number) => {
      setSelectedItems(selectedItems.filter((selectedItem: SelectedProductType) => selectedItem.id !== id));
    },
    [selectedItems]
  );

  const handleSelectProduct = useCallback(
    async (item: ProductType) => {
      if (selectedItems.length >= max) return;

      setSelectedItems([...selectedItems, { id: selectedItems.length, value: item }]);
      handleCloseAddForm();
      setValue('');
      setSuggestions([]);
    },
    [selectedItems, handleCloseAddForm, max]
  );

  const handleChangeInputValue = useCallback(async (e) => {
    setValue(e.currentTarget.value);
  }, []);

  return (
    <div className="similar-product-list">
      <div className="similar-product-list__items">
        {selectedItems.length ? (
          selectedItems.map((item: SelectedProductType) => (
            <div key={item.value.product_id} className="similar-product-list-item">
              <div className="similar-product__text">
                <span>{item.value.brand}</span>
                {item.value.title}
              </div>
              <Button
                className="similar-product__remove-button"
                icon="delete"
                color="red"
                size="mini"
                onClick={() => handleRemoveItem(item.id)}
              />
            </div>
          ))
        ) : (
          <div>{texts.noResults}</div>
        )}
      </div>
      <div className="similar-product__add">
        {!showAddForm && (
          <Button onClick={handleOpenAddForm} color="green" disabled={selectedItems.length === max}>
            {t(texts.button)}
          </Button>
        )}
        {showAddForm && (
          <div className="similar-product__add-form">
            <div className="similar-product-controls">
              <div className="similar-product-controls__input">
                <Input onChange={handleChangeInputValue} loading={isLoading} />
              </div>

              <div className="similar-product-controls__actions">
                <Button onClick={handleCloseAddForm}>{t('cancel')}</Button>
              </div>
            </div>

            {filteredSuggestions.length ? (
              <ul className="similar-products-suggestions">
                {filteredSuggestions.map((suggest: ProductType) => (
                  <li key={suggest.product_id} onClick={() => handleSelectProduct(suggest)} className="suggestion-list-item">
                    <span>{suggest.brand}</span>
                    {suggest.title}
                  </li>
                ))}
              </ul>
            ) : null}
          </div>
        )}
      </div>
    </div>
  );
}
