import React from 'react';
import { useTranslation } from 'react-i18next';
import { Icon } from 'semantic-ui-react';

import { IStatsConversionInfoDataItem } from '../Dashboard/StatsConversionInfo/interfaces';

import './styles.scss';

interface ISalesFunnelProps {
  data: IStatsConversionInfoDataItem;
}

export default function SalesFunnel({ data }: ISalesFunnelProps) {
  const { t } = useTranslation();
  const {
    product_to_basket: productToBasket,
    product_to_wishlist: productToWishlist,
    product_view: productView,
    product_sale: productSale,
  } = data;

  const format = (val: number, val2: number) => `${((val / val2) * 100).toFixed(2)}%`;

  return (
    <div className="sales-funnel">
      <div className="sales-funnel-item">
        <div className="sales-funnel-item__icon">
          <Icon name="eye" />
        </div>
        <div className="sales-funnel-item__bar">
          <div className="sales-funnel-item__bar-inner" style={{ width: '100%' }}>
            <span className="sales-funnel-value">{data.product_view}</span>
            <span className="sales-funnel-label">{t('product_view')}</span>
          </div>
        </div>
        <div className="sales-funnel-item__percent">100%</div>
      </div>
      <div className="sales-funnel-item">
        <div className="sales-funnel-item__icon">
          <Icon name="heart" />
        </div>
        <div className="sales-funnel-item__bar">
          <div className="sales-funnel-item__bar-inner" style={{ width: format(productToWishlist, productView) }}>
            <span className="sales-funnel-label">{t('to_wishlist')}</span>
          </div>
        </div>
        <div className="sales-funnel-item__percent">{format(productToWishlist, productView)}</div>
      </div>
      <div className="sales-funnel-item">
        <div className="sales-funnel-item__icon">
          <Icon name="shopping basket" />
        </div>
        <div className="sales-funnel-item__bar">
          <div className="sales-funnel-item__bar-inner" style={{ width: format(productToBasket, productView) }}>
            <span className="sales-funnel-label">{t('to_basket')}</span>
          </div>
        </div>
        <div className="sales-funnel-item__percent">{format(productToBasket, productView)}</div>
      </div>
      <div className="sales-funnel-item">
        <div className="sales-funnel-item__icon">
          <Icon name="credit card" />
        </div>
        <div className="sales-funnel-item__bar">
          <div className="sales-funnel-item__bar-inner" style={{ width: format(productSale, productView) }}>
            <span className="sales-funnel-label">{t('product_sale')}</span>
          </div>
        </div>
        <div className="sales-funnel-item__percent">{format(productSale, productView)}</div>
      </div>
    </div>
  );
}
