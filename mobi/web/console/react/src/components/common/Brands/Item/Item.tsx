import React, { useState, useCallback } from 'react';
import { Button } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';

import { FullBrandType } from '../../../../containers/brands/types';

type Props = {
  data: FullBrandType;
};

export default function ProductItem({ data }: Props) {
  const { t } = useTranslation();
  const [expanded, setExpanded] = useState<boolean>(false);

  const { brand, metrics_single } = data;

  const handleMoreClick = useCallback(() => {
    setExpanded(!expanded);
  }, [expanded]);

  return (
    <div className="fluid-card">
      <div className="fluid-card-preview">
        <div className="fluid-card-information">
          <div className="fluid-card-information__name" onClick={handleMoreClick}>
            {brand}
          </div>
        </div>
        <div className="fluid-card-statistic">
          <div className="fluid-card-statistic">
            <strong>{t('product_metric_displays')}:</strong> {metrics_single.displays}
          </div>
          <div className="fluid-card-statistic">
            <strong>{t('product_metric_add_to_basket')}:</strong> {metrics_single.to_basket}
          </div>
          <div className="fluid-card-statistic">
            <strong>{t('product_metric_sales')}:</strong> {metrics_single.sales}
          </div>
        </div>
        <div className="fluid-card-actions">
          <Button basic primary content={expanded ? t('less') : t('more')} size="tiny" onClick={handleMoreClick} />
        </div>
      </div>

      {expanded && (
        <div className="fluid-card-more-content">
          <div className="fluid-card-more-content-columns">
            TBD
          </div>
        </div>
      )}
    </div>
  );
}
