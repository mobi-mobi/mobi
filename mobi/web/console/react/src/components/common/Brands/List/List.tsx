import React from 'react';

import BrandItem from '../Item';

import { FullBrandType } from '../../../../containers/brands/types';

type Props = {
  items: Array<FullBrandType>;
};

export default function Brands({ items }: Props) {
  return (
    <div>
      {items.map((data: FullBrandType) => (
        <BrandItem key={`${data.brand}`} data={data} />
      ))}
    </div>
  );
}
