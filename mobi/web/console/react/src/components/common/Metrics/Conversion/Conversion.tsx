import React, { useState, useCallback } from 'react';
import { Card as SCard } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';
import TimeRangeSelector from '../../../home/TimeList';

import Card from '../../../base/MetricCard';

const CONVERSION_CARD_MODEL = [
  {
    name: 'count_views_b',
    api: '/<client>/user/events/count/last/<timeRange>?event_type=product_view',
    dataPath: 'value',
  },
  {
    name: 'count_wishlist_add_b',
    api: '/<client>/user/events/count/last/<timeRange>?event_type=product_to_wishlist',
    dataPath: 'value',
  },
  {
    name: 'count_basket_add_b',
    api: '/<client>/user/events/count/last/<timeRange>?event_type=product_to_basket',
    dataPath: 'value',
  },
  {
    name: 'count_sales_b',
    api: '/<client>/user/events/count/last/<timeRange>?event_type=product_sale',
    dataPath: 'value',
  },
];

export default function Conversion() {
  const [timeRange, setTimeRange] = useState<string>('1h');
  const { t } = useTranslation();

  const handleTimeRangeChange = useCallback((val) => {
    setTimeRange(val);
  }, []);

  return (
    <div className="metric-cards">
      <div className="part-header part-header--actions">
        <span>{t('conversion')}</span>
        <TimeRangeSelector
          isLocalChange={true}
          onLocalTimeChange={handleTimeRangeChange}
          customTimeRange={['1h', '1d', '7d']}
          localValue={timeRange}
        />
      </div>

      <SCard.Group>
        {CONVERSION_CARD_MODEL.map((card) => (
          <Card key={card.name} {...card} timeRange={timeRange} />
        ))}
      </SCard.Group>
    </div>
  );
}
