import React, { Component } from 'react';
import { connect } from 'react-redux';
import User from '../../classes/user';
import { Dropdown, DropdownProps } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { changeCompany } from '../../containers/home/actions';

interface CompanyListProps {
  user: User;
  client: string;
  changeCompanyAction: Function;
}

class CompanyList extends Component<CompanyListProps, any> {
  handleChange = (e: React.SyntheticEvent<HTMLElement>, { value }: DropdownProps) => {
    const { changeCompanyAction } = this.props;
    changeCompanyAction(value);
  };

  render() {
    const { user, client } = this.props;
    const options =
      user?.clients?.map((el) => {
        return { key: el, value: el, content: el, text: el };
      }) || [];

    return (
      <div className="logged-user__container">
        <div className="logged-user__name">
          <Dropdown
            onChange={this.handleChange}
            inline
            icon="chevron down"
            options={options}
            defaultValue={client}
            pointing="top right"
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    user: state.home.user,
    client: state.home.client,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    changeCompanyAction: bindActionCreators(changeCompany, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CompanyList);
