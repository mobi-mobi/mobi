import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dropdown, DropdownProps, Responsive } from 'semantic-ui-react';
import { withTranslation, WithTranslation } from 'react-i18next';
import { bindActionCreators } from 'redux';

import { changeTime } from '../../containers/home/actions';

interface RangeListProps {
  timeRange: string;
  changeTimeAction: Function;
  onLocalTimeChange?: Function;
  isLocalChange?: boolean;
  customTimeRange?: string[];
  localValue?: string;
  removeLast?: boolean;
}

class RangeList extends Component<WithTranslation & RangeListProps, any> {
  timeRangeFill = () => {
    return ['10m', '1h', '3h', '24h', '7d', '28d'];
  };

  handleChange = (e: React.SyntheticEvent<HTMLElement>, { value }: DropdownProps) => {
    const { changeTimeAction, onLocalTimeChange, isLocalChange } = this.props;

    if (isLocalChange && onLocalTimeChange) {
      onLocalTimeChange(value);
    } else {
      changeTimeAction(value);
    }
  };

  render() {
    const { timeRange, t, customTimeRange, localValue, removeLast } = this.props;
    const ranges = customTimeRange || this.timeRangeFill();
    const defaultTimeRange = customTimeRange && localValue ? localValue : timeRange;
    const text = customTimeRange && localValue ? t(localValue + '_sh') : t(timeRange + '_sh');
    const lastText =
      customTimeRange && localValue
        ? removeLast
          ? t(localValue)
          : t('last_time') + t(localValue)
        : removeLast
        ? t(timeRange)
        : t('last_time') + t(timeRange);

    const options = ranges.map((el: string) => {
      return { key: el, value: el, content: t(el), text: t(el) };
    });

    return (
      <div>
        <Responsive maxWidth={720}>
          <Dropdown
            text={text}
            icon="clock outline"
            onChange={this.handleChange}
            floating
            labeled
            button
            className="icon"
            options={options}
            defaultValue={defaultTimeRange}
          />
        </Responsive>
        <Responsive minWidth={721}>
          <Dropdown
            text={lastText}
            icon="clock outline"
            onChange={this.handleChange}
            floating
            labeled
            button
            className="icon"
            options={options}
            defaultValue={defaultTimeRange}
          />
        </Responsive>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    timeRange: state.home.timeRange,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    changeTimeAction: bindActionCreators(changeTime, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(RangeList));
