import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dropdown, DropdownProps } from 'semantic-ui-react';
import { withTranslation, WithTranslation } from 'react-i18next';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import User from '../../classes/user';

interface LoggedUserProps {
  user: User;
}

class LoggedUser extends Component<RouteComponentProps & WithTranslation & LoggedUserProps, any> {
  handleChange = (e: React.SyntheticEvent<HTMLElement>, { value }: DropdownProps) => {
    const changeLanguage = (lng: any) => {
      this.props.i18n.changeLanguage(lng);
    };

    if (value === 'en' || value === 'ru') {
      changeLanguage(value);
    }

    if (value === 'logout') {
      window.location.href = 'https://console.mobimobi.tech/logout';
    }
  };

  render() {
    const { user, t } = this.props;
    const logout = t('logout');
    const options = [
      { key: 'lang-en', value: 'en', text: 'English' },
      { key: 'lang-ru', value: 'ru', text: 'Русский' },
      { key: 'logout', value: 'logout', text: logout },
    ];

    return (
      <div className="logged-user__container">
        <div className="logged-user__name">
          <Dropdown
            onChange={this.handleChange}
            inline
            icon={'angle down'}
            options={options}
            text={user.displayName}
            pointing="top right"
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    user: state.home.user,
  };
};

export default connect(mapStateToProps)(withTranslation()(withRouter(LoggedUser)));
