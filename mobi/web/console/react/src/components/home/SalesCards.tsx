import React, { Component } from 'react';
import { withTranslation, WithTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { InfoCards } from '../ui/InfoCard';
import TimeList from './TimeList';

interface SalesCardsProps {
  timeRange: string;
}

class SalesCards extends Component<WithTranslation & SalesCardsProps, any> {
  cards1 = [
    { name: 'count_p_catalog', api: '/<client>/catalog/info', dataPath: 'number.active' },
    {
      name: 'count_views',
      api: '/<client>/user/events/count/last/<timeRange>?event_type=product_view',
      dataPath: 'value',
    },
    {
      name: 'count_adds',
      api: '/<client>/user/events/count/last/<timeRange>?event_type=product_to_basket',
      dataPath: 'value',
    },
    {
      name: 'count_sales',
      api: '/<client>/user/events/count/last/<timeRange>?event_type=product_sale',
      dataPath: 'value',
    },
  ];
  cards2 = [
    {
      name: 'count_p_create',
      api: '/<client>/catalog/events/count/last/<timeRange>?status=created',
      dataPath: 'value',
    },
    { name: 'count_p_edits', api: '/<client>/catalog/events/count/last/<timeRange>?status=updated', dataPath: 'value' },
    { name: 'count_removes', api: '/<client>/catalog/events/count/last/<timeRange>?status=deleted', dataPath: 'value' },
    { name: 'count_u_events', api: '/<client>/catalog/events/count/last/<timeRange>', dataPath: 'value' },
  ];
  cards3 = [
    { name: 'count_to_service', api: '/<client>/recommendation/events/count/last/<timeRange>', dataPath: 'value' },
    {
      name: 'count_r_personal',
      api: '/<client>/recommendation/events/count/last/<timeRange>?recommendation_type=personal_recommendations',
      dataPath: 'value',
    },
    {
      name: 'count_analogs',
      api: '/<client>/recommendation/events/count/last/<timeRange>?recommendation_type=similar_products',
      dataPath: 'value',
    },
    {
      name: 'count_smart_search',
      api: '/<client>/recommendation/events/count/last/<timeRange>?recommendation_type=smart_search',
      dataPath: 'value',
    },
  ];

  render() {
    const { timeRange, t } = this.props;
    return (
      <div>
        <div className="part-header part-header--actions">
          {t('part1')}
          <div className="time-range">
            <TimeList />
          </div>
        </div>
        <InfoCards data={this.cards1} timeRange={timeRange} />
        <div className="part-header">{t('part2')}</div>
        <InfoCards data={this.cards2} timeRange={timeRange} />
        <div className="part-header">{t('part3')}</div>
        <InfoCards data={this.cards3} timeRange={timeRange} />
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    timeRange: state.home.timeRange,
  };
};

export default connect(mapStateToProps)(withTranslation()(SalesCards));
