import React, { Component } from 'react';
import { withTranslation, WithTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { InfoCards } from '../ui/InfoCard';

interface MetricsCardsProps {
  timeRange: string;
}

class MetricsCards extends Component<WithTranslation & MetricsCardsProps, any> {
  timeRange = '1h';
  cards = [
    {
      name: 'count_views_b',
      api: '/<client>/user/events/count/last/<timeRange>?event_type=product_view',
      dataPath: 'value',
    },
    {
      name: 'count_wishlist_add_b',
      api: '/<client>/user/events/count/last/<timeRange>?event_type=product_to_wishlist',
      dataPath: 'value',
    },
    {
      name: 'count_basket_add_b',
      api: '/<client>/user/events/count/last/<timeRange>?event_type=product_to_basket',
      dataPath: 'value',
    },
    {
      name: 'count_sales_b',
      api: '/<client>/user/events/count/last/<timeRange>?event_type=product_sale',
      dataPath: 'value',
    },
  ];

  render() {
    const { t } = this.props;

    return (
      <div>
        <div className="part-header">{t('conversion_hour')}</div>
        <InfoCards data={this.cards} timeRange={'1h'} />

        <div className="part-header">{t('conversion_day')}</div>
        <InfoCards data={this.cards} timeRange={'1d'} />

        <div className="part-header">{t('conversion_week')}</div>
        <InfoCards data={this.cards} timeRange={'7d'} />
      </div>
    );
  }
}

export default withTranslation()(MetricsCards);
