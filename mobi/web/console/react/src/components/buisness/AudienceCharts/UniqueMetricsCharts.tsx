import React, { Component } from 'react';
import { withTranslation, WithTranslation } from 'react-i18next';

import ChartLine from '../../ui/ChartCard';
import TimeRange from '../../home/TimeList';
import ChartData from '../../../classes/chartData';

class UniqueMetricsCharts extends Component<WithTranslation, any> {
  chart = new ChartData({ name: 'visitors', api: '/<client>/user/unique/metric/from/<timeRange>', dataPath: '' });

  state = {
    timeRange: '1h',
  };

  handleTimeRangeChange = (val: any) => {
    this.setState({ timeRange: val });
  };

  render() {
    const { t } = this.props;
    const { timeRange } = this.state;

    return (
      <div>
        <div className="part-header part-header--actions">
          {t('unique_visitors')}

          <TimeRange
            isLocalChange={true}
            onLocalTimeChange={this.handleTimeRangeChange}
            customTimeRange={['1h', '24h', '28d']}
            localValue={timeRange}
          />
        </div>

        <ChartLine chartData={this.chart} timeRange={timeRange} />
      </div>
    );
  }
}

export default withTranslation()(UniqueMetricsCharts);
