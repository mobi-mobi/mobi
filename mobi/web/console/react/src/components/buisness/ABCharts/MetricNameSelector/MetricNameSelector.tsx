import React, { useState, useMemo, useCallback, useRef, useEffect } from 'react';
import { Dropdown } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';

type Props = {
  onChange: Function;
  defaultValue: string;
  options: Array<string>;
};

export default function MetricNameSelector({ onChange, defaultValue, options }: Props) {
  const [selectedName, setSelectedName] = useState<string>(defaultValue);
  const { t } = useTranslation();

  const isInitialMount = useRef(true);
  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      onChange(selectedName);
    }
  }, [selectedName]);

  const handleChange = useCallback(
    (e: React.SyntheticEvent<HTMLElement>, { value }: any) => {
      setSelectedName(value);
    },
    []
  );

  const selectOptions = useMemo(
    () =>
      options.map((el: string) => {
        return { key: el, value: el, content: t(el), text: t(el) };
      }),
    [options]
  );

  return (
    <Dropdown
      text={t(selectedName)}
      onChange={handleChange}
      floating
      labeled
      button
      className="icon"
      options={selectOptions}
      defaultValue={selectedName}
    />
  );
}
