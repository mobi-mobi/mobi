import React, { useState, useCallback, useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import Chart from 'react-google-charts';
import { Checkbox } from 'semantic-ui-react';

import TimeRangeSelector from '../../home/TimeList';
import LoadingSpinner from '../../ui/LoadingSpinner';
import MetricNameSelector from './MetricNameSelector';

import Api from '../../../services/Api';

import config from '../../../config';

import DataFormatter from '../../../classes/DataFormatter';

import './styles.scss';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

type PopulationType = {
  pop_a: string;
  pop_b: string;
};

type PopulationTypeData = {
  mean_values: Array<Array<any>>;
  points_num: Array<Array<any>>;
};

type AbResultsTypeData = {
  absolute: {
    lower: Array<Array<any>>;
    middle: Array<Array<any>>;
    upper: Array<Array<any>>;
  };
  relative: {
    lower: Array<Array<any>>;
    middle: Array<Array<any>>;
    upper: Array<Array<any>>;
  };
};

export default function ABCharts() {
  const [showAbsValues, setShowAbsValues] = useState<boolean>(false);
  const [timeRange, setTimeRange] = useState<string>('14d');
  const [metricName, setMetricName] = useState<string>('to_basket_num');
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [abResults, setAbResults] = useState<AbResultsTypeData>();
  const [popLeftChartData, setPopLeftChartData] = useState<PopulationTypeData>({
    mean_values: [[]],
    points_num: [[]],
  });

  const [popRightChartData, setPopRightChartData] = useState<PopulationTypeData>({
    mean_values: [[]],
    points_num: [[]],
  });

  const [populations, setPopulations] = useState<PopulationType>({
    pop_a: 'target_and_test',
    pop_b: 'reference',
  });

  useEffect(() => {
    (async function getMetricsData() {
      const {
        data: { pop_a, pop_b, test_results },
      } = await apiInstance.get(
        `/${client}/abtest/metric/${metricName}?period=${timeRange}&pop_a=${populations.pop_a}&pop_b=${populations.pop_b}&days=14`,
        {}
      );

      setPopLeftChartData(pop_a);
      setPopRightChartData(pop_b);

      setAbResults({
        absolute: {
          lower: DataFormatter.preformatDataTime(test_results.abs_diff_lower_bound, timeRange),
          upper: DataFormatter.preformatDataTime(test_results.abs_diff_upper_bound, timeRange),
          middle: DataFormatter.preformatDataTime(test_results.abs_diff, timeRange),
        },
        relative: {
          lower: DataFormatter.preformatDataTime(test_results.rel_diff_lower_bound, timeRange),
          upper: DataFormatter.preformatDataTime(test_results.rel_diff_upper_bound, timeRange),
          middle: DataFormatter.preformatDataTime(test_results.rel_diff, timeRange),
        },
      });
    })();
  }, [timeRange, metricName, client, populations]);

  const preformatData = useCallback(
    (metricData) => {
      if (metricData) {
        return DataFormatter.preformatDataTime(metricData, timeRange);
      }

      return [[]];
    },
    [timeRange]
  );

  const handleToggleShowAbsValues = useCallback(() => {
    setShowAbsValues(!showAbsValues);
  }, [showAbsValues]);

  const combineChartData = useCallback(
    (meanValues, pointsNum) => {
      const preformattedMeanValues = preformatData(meanValues);
      const preformattedPointsNum = preformatData(pointsNum);

      return preformattedMeanValues.map((val: any, index: number) => [...val, preformattedPointsNum[index][1]]);
    },
    [preformatData]
  );

  const leftChartData = useMemo(() => combineChartData(popLeftChartData.mean_values, popLeftChartData.points_num), [
    popLeftChartData,
    combineChartData,
  ]);

  const rightChartData = useMemo(() => combineChartData(popRightChartData.mean_values, popRightChartData.points_num), [
    popRightChartData,
    combineChartData,
  ]);

  const floatAndFixed = useCallback((n) => parseFloat(n.toFixed(2)), []);

  const popABChartData = useMemo(() => {
    return leftChartData.map((data, i) => [...data, rightChartData[i]?.[1], rightChartData[i]?.[2]]);
  }, [leftChartData, rightChartData]);

  const combinedChartHeader = [
    t('date'),
    metricName !== 'sessions_per_user' ? `${t(metricName)} (Pop. A)` : `${t('number_of_unique_users')} (Pop. A)`,
    t('ab_charts_population_a_label'),
    metricName !== 'sessions_per_user' ? `${t(metricName)} (Pop. B)` : `${t('number_of_unique_users')} (Pop. B)`,
    t('ab_charts_population_b_label'),
  ];

  const resultChartData = useMemo(() => {
    const data = showAbsValues ? abResults?.absolute : abResults?.relative;

    if (data) {
      return data.upper.map((val, index) =>
        showAbsValues
          ? [val[0], floatAndFixed(val[1]), floatAndFixed(data.middle[index][1]), floatAndFixed(data.lower[index][1])]
          : [
              val[0],
              floatAndFixed(val[1] * 100),
              floatAndFixed(data.middle[index][1] * 100),
              floatAndFixed(data.lower[index][1] * 100),
            ]
      );
    }

    return null;
  }, [abResults, showAbsValues, floatAndFixed]);

  const handleTimeRangeChange = useCallback((val: string) => {
    setTimeRange(val === '1d' ? 'daily' : val);
  }, []);

  const handleMetricNameChange = useCallback((val: string) => {
    setMetricName(val);
  }, []);

  const handlePopulationChange = useCallback((type: string, val: string) => {
    setPopulations((prev) => ({ ...prev, [type]: val }));
  }, []);

  const resultsChartsColumnNames = useMemo(
    () =>
      showAbsValues
        ? [t('abs_diff_upper_bound'), t('abs_diff'), t('abs_diff_lower_bound')]
        : [t('rel_diff_upper_bound'), t('rel_diff'), t('rel_diff_lower_bound')],
    [showAbsValues, t]
  );

  return (
    <div className="ab-charts">
      <div className="ab-charts__title">{t('ab_charts_title')}</div>

      <div className="ab-charts__filters">
        <div className="row-label">
          <div className="row-label__text">{t('ab_charts_chart_name')}: </div>
          <MetricNameSelector
            defaultValue={metricName}
            options={['to_basket_num', 'to_wishlist_num', 'views_num', 'purchases_num', 'sessions_per_user']}
            onChange={handleMetricNameChange}
          />
        </div>

        <div className="row-label">
          <div className="row-label__text">{t('ab_charts_period')}: </div>

          <TimeRangeSelector
            isLocalChange={true}
            removeLast={true}
            onLocalTimeChange={handleTimeRangeChange}
            customTimeRange={['1d', '7d', '14d', '28d']}
            localValue={timeRange}
          />
        </div>
      </div>

      <div className="ab-charts__filters">
        <div className="ab-charts-filter">
          <div className="row-label">
            <div className="row-label__text">{t('ab_charts_population_a_label')}: </div>
            <MetricNameSelector
              defaultValue={populations.pop_a}
              options={['reference', 'target', 'test', 'target_and_test']}
              onChange={(val: string) => handlePopulationChange('pop_a', val)}
            />
          </div>
        </div>
        <div className="ab-charts-filter">
          <div className="row-label">
            <div className="row-label__text">{t('ab_charts_population_b_label')}: </div>
            <MetricNameSelector
              defaultValue={populations.pop_b}
              options={['reference', 'target', 'test', 'target_and_test']}
              onChange={(val: string) => handlePopulationChange('pop_b', val)}
            />
          </div>
        </div>
      </div>

      <div className="ab-charts-charts">
        {popABChartData[1] ? (
          <Chart
            width="100%"
            height="400px"
            chartType="ComboChart"
            loader={<LoadingSpinner relative size="small" />}
            data={[[...combinedChartHeader], ...popABChartData]}
            options={{
              title: 'AB Test Chart',
              legend: 'none',
              series: {
                0: {
                  targetAxisIndex: 0,
                },
                1: {
                  targetAxisIndex: 1,
                  type: 'bars',
                },
                2: {
                  targetAxisIndex: 0,
                },
                3: {
                  targetAxisIndex: 1,
                  type: 'bars',
                },
              },
              colors: ['#ff6951', '#feaf55', '#6725ea', '#a2e81c'],
              theme: 'maximized',
            }}
          />
        ) : null}
      </div>

      <div className="ab-charts-results">
        {resultChartData ? (
          <>
            <div className="ab-charts-results__title">
              <h3>{t('ab_results')}</h3>
              <Checkbox
                onChange={handleToggleShowAbsValues}
                defaultChecked={showAbsValues}
                label={t('show_absolute_values')}
              />
            </div>
            <Chart
              width="100%"
              height="400px"
              formatters={
                !showAbsValues
                  ? [
                      {
                        column: 1,
                        type: 'NumberFormat',
                        options: {
                          suffix: '%',
                        },
                      },
                      {
                        column: 2,
                        type: 'NumberFormat',
                        options: {
                          suffix: '%',
                        },
                      },
                      {
                        column: 3,
                        type: 'NumberFormat',
                        options: {
                          suffix: '%',
                        },
                      },
                    ]
                  : []
              }
              chartType="LineChart"
              loader={<LoadingSpinner relative size="small" />}
              data={[[t('date'), ...resultsChartsColumnNames], ...resultChartData]}
              options={{
                theme: 'maximized',
                hAxis: {
                  minValue: 0,
                },
                title: t('results'),
                legend: 'none',
                series: { 1: { type: 'line' }, 2: { type: 'line' }, 3: { type: 'line' } },
                colors: ['#777', '#feaf55', '#777'],
              }}
            />
          </>
        ) : (
          <LoadingSpinner relative size="small" />
        )}
      </div>
    </div>
  );
}
