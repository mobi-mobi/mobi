import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

class CustomScroll extends Component {
  renderThumb = () => {
    return <div className="custom-scroll-horizontal" />;
  };

  render() {
    const { children } = this.props;
    return (
      <Scrollbars
        {...this.props}
        autoHide
        renderTrackHorizontal={() => <div style={{ display: 'none' }} />}
        renderThumbVertical={this.renderThumb}
      >
        {children}
      </Scrollbars>
    );
  }
}

CustomScroll.propTypes = {
  children: PropTypes.any,
};

export default CustomScroll;
