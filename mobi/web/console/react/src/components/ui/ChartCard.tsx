import { Chart } from 'react-google-charts';
import React, { useState, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

import { getDataFromApi } from '../../services/ApiConnectors';
import ChartData from '../../classes/chartData';

interface IChartLine {
  chartData: ChartData;
  timeRange: string;
}

const preformatDataTime = (data: Array<Array<any>>, timeRange: string) => {
  let format = 'MM.DD';

  if (timeRange.includes('h') || timeRange.includes('m')) {
    format = 'HH:mm';
  }

  return data.map((el) => [moment(el[0]).format(format), el[1]]);
};

export const ChartLine = ({ chartData, timeRange }: IChartLine) => {
  const [data, setData] = useState([]);
  const client: string = useSelector((state: any) => state.home.client);
  const { t } = useTranslation();
  const [myTimeRange, setMyTimeRange] = useState(timeRange);

  myTimeRange !== timeRange && setMyTimeRange(timeRange);

  useMemo(() => {
    const timePoint = myTimeRange === '1h' ? '60m' : myTimeRange;

    getDataFromApi(chartData.api, timePoint, client).then((data: any) => setData(data));
  }, [myTimeRange, client, chartData.api]);

  return (
    <div className={'my-pretty-chart-container'}>
      {data.length > 0 ? (
        <Chart
          data={[['Time', 'Value'], ...preformatDataTime([...data], myTimeRange)]}
          width="100%"
          height="400px"
          chartType="LineChart"
          options={{
            theme: 'maximized',
            series: {
              0: { targetAxisIndex: 0, curveType: 'function', pointSize: 4 },
            },
            title: t(chartData.name),
            legend: 'none',
            colors: ['#6625ab', '#feaf55'],
            animation: {
              startup: true,
              easing: 'linear',
              duration: 1000,
            },
          }}
        />
      ) : (
        <div />
      )}
    </div>
  );
};

export default ChartLine;
