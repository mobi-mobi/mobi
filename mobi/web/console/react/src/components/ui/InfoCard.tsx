import React, { useState, useMemo } from 'react';
import { Card, Icon } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import get from 'lodash/get';

import CardData from '../../classes/cardData';
import Api from '../../services/Api';

import config from '../../config';

const api = new Api(config.global.REACT_APP_API_URL);

export interface InfoCardsProps {
  data: CardData[];
  timeRange: string;
}

const getDataFromApi = (apiPath: string, timeRange: string, client: string) => {
  const apiPathReady = apiPath.replace('<client>', client).replace('<timeRange>', timeRange);

  return api
    .get(apiPathReady, {})
    .then((response: any) => {
      const data = get(response, 'data');

      return data;
    })
    .catch((error: any) => {
      console.log(error);

      return NaN;
    });
};

const Meta = ({ timeRange, metaData }: any) => {
  const { t } = useTranslation();

  return metaData ? (
    <div className="meta">
      <Icon name="clock outline" /> <span>{t(timeRange)}</span>
    </div>
  ) : (
    <div />
  );
};

const CountCard = (cardData: CardData, index: number, timeRange: string) => {
  const { t } = useTranslation();
  const [count, setCount] = useState(-1);
  const client: string = useSelector((state: any) => state.home.client);
  const [myTimeRange, setMytimeRange] = useState(timeRange);

  myTimeRange !== timeRange && setMytimeRange(timeRange);

  useMemo(() => {
    setCount(-1);
    getDataFromApi(cardData.api, myTimeRange, client).then((data: any) => setCount(get(data, cardData.dataPath, '0')));
  }, [myTimeRange, client, cardData.api, cardData.dataPath]);

  return count !== -1 ? (
    <Card
      key={index}
      header={t(cardData.name)}
      meta={<Meta timeRange={timeRange} metaData={cardData.metaData} />}
      description={count.toLocaleString('en', {
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
      })}
    />
  ) : (
    <Card
      key={index}
      className="preload-card"
      header={t(cardData.name)}
      description={
        <div className="description">
          <Icon loading name="spinner" />
        </div>
      }
    />
  );
};

export const InfoCards = ({ data, timeRange }: InfoCardsProps) => (
  <Card.Group className="cards-group">
    {data.map((cardData, index) => CountCard(cardData, index, timeRange))}
  </Card.Group>
);
