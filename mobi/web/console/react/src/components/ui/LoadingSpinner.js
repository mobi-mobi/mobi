import React from 'react';

const LoadingSpinner = (props) => {
  const { relative, size } = props;

  return (
    <div
      className={`loading-spinner ${relative ? 'loading-spinner--relative' : ''} ${
        size ? `loading-spinner--${size}` : ''
      }`}
    >
      <div />
      <div />
      <div />
      <div />
    </div>
  );
};

export default LoadingSpinner;
