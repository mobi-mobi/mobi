export interface IResponseError {
  status: string;
  error: {
    code: number;
    description: string;
    message: string;
  };
}
