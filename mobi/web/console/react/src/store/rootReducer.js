import { combineReducers } from 'redux';
import { homeReducer } from '../containers/home/reducer';
import { zoneReducer } from '../containers/zones/reducer';
import { campaignReducer } from '../containers/campaign/reducer';
import { productsReducer } from '../containers/products/reducer';
import { brandsReducer } from '../containers/brands/reducer';
import { sandboxReducer } from '../containers/sandbox/reducer';

export const rootReducer = combineReducers({
  home: homeReducer,
  zones: zoneReducer,
  campaign: campaignReducer,
  products: productsReducer,
  sandbox: sandboxReducer,
  brands: brandsReducer,
});
