let config = {};
const env = process.env;
const NODE_ENV = env.NODE_ENV;

config.global = {
  m: console.log(env) ? "s" : "ss",
  REACT_APP_API_URL:
    NODE_ENV === "development"
      ? env.REACT_APP_API_URL
      : env.REACT_APP_API_PROD_URL,
};

module.exports = config;
