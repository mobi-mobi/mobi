import React, { useState, useCallback, useRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import Page from '../../../components/base/Page';
import SimilarProductSelect from '../../../components/common/Sandbox/SimilarProductSelect';
import SandboxSettings from '../../../components/common/Sandbox/SandboxSettings';

import Api from '../../../services/Api';
import config from '../../../config';

import { ProductType } from '../../../containers/products/types';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function SimilarProducts() {
  const { t } = useTranslation();
  const { brands, client, userHistory } = useSelector((state: any) => ({
    client: state.home.client,
    brands: state.sandbox.sandboxBrands,
    userHistory: state.sandbox.userHistory,
  }));

  const [error, setError] = useState<string | null>(null);
  const [similarProduct, setSimilarProduct] = useState<ProductType | null>(null);
  const [results, setResults] = useState<Array<ProductType>>([]);
  const [requested, setRequested] = useState<boolean>(false);
  const isInitialMounted = useRef(true);

  useEffect(() => {
    if (isInitialMounted.current) {
      isInitialMounted.current = false;
    } else {
      if (!similarProduct || !client) {
        return;
      }

      let url = `/${client}/test/recommendation?recommendation_type=similar_products&product_id=${similarProduct.product_id}`;

      if (brands) {
        url += brands.map((brand: any) => `&brands=${brand.value}`).join('&');
      }

      if (userHistory) {
        const mapped = userHistory.map((selectedProduct: any) => ({
          product_id: selectedProduct.value.product_id,
          event_type: selectedProduct.eventType,
        }));

        const history = encodeURIComponent(JSON.stringify(mapped));
        url += `&browsing_history=${history}`;
      } else {
        url += `&browsing_history=[]`;
      }

      url += `&num=20`;

      apiInstance
        .get(url, {})
        .then((res) => {
          setError(null);
          setResults(res.data);
          setRequested(true);
        })
        .catch((error) => {
          setRequested(false);
          setError(error.error.description);
        });
    }
  }, [brands, userHistory, client, similarProduct]);

  const handleSimilarProductChange = useCallback((product) => {
    setSimilarProduct(product);
  }, []);

  return (
    <Page name="page--sandbox-recommendation">
      <h1>
        {t('sandbox')} / {t('similar_products')}
      </h1>

      <div className="page-content">
        <div className="sandbox-similar-product__input">
          <SimilarProductSelect onChange={handleSimilarProductChange} />
        </div>

        <SandboxSettings />

        {error && <div className="sandbox-recommendation-results__error">{JSON.stringify(error)}</div>}
        {Boolean(results.length) && (
          <div className="sandbox-recommendation-results">
            <h3>
              {t('products_similar_to')} "{similarProduct?.title}"
            </h3>

            {results.map((product: ProductType) => (
              <div key={product.product_id} className="sandbox-recommendation-results__item">
                <i>{product.brand}</i>
                <div className="sandbox-recommendation-results__item-title">{product.title}</div>
              </div>
            ))}
          </div>
        )}

        {results.length === 0 && requested ? <h3>{t('no_results')}</h3> : null}
      </div>
    </Page>
  );
}
