import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import Page from '../../../components/base/Page';
import SandboxSettings from '../../../components/common/Sandbox/SandboxSettings';

import Api from '../../../services/Api';
import config from '../../../config';

import { ProductType } from '../../../containers/products/types';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function PersonalizedRecommendations() {
  const { t } = useTranslation();
  const { brands, client, userHistory } = useSelector((state: any) => ({
    client: state.home.client,
    brands: state.sandbox.sandboxBrands,
    userHistory: state.sandbox.userHistory,
  }));

  const [error, setError] = useState<string | null>(null);
  const [results, setResults] = useState<Array<ProductType>>([]);
  const [requested, setRequested] = useState<boolean>(false);

  useEffect(() => {
    let url = `/${client}/test/recommendation?recommendation_type=personal_recommendations`;

    if (brands) {
      url += brands.map((brand: any) => `&brands=${brand.value}`).join('&');
    }

    if (userHistory) {
      const mapped = userHistory.map((selectedProduct: any) => ({
        product_id: selectedProduct.value.product_id,
        event_type: selectedProduct.eventType,
      }));

      const history = encodeURIComponent(JSON.stringify(mapped));
      url += `&browsing_history=${history}`;
    } else {
      url += `&browsing_history=[]`;
    }

    url += `&num=20`;

    apiInstance
      .get(url, {})
      .then((res) => {
        setError(null);
        setResults(res.data);
        setRequested(true);
      })
      .catch((error) => {
        setRequested(false);
        setError(error.error.description);
      });
  }, [brands, userHistory, client]);

  return (
    <Page name="page--sandbox-recommendation">
      <h1>
        {t('sandbox')} / {t('personalized_recommendations')}
      </h1>

      <div className="page-content">
        <SandboxSettings />

        {error && <div className="sandbox-recommendation-results__error">{JSON.stringify(error)}</div>}
        {Boolean(results.length) && (
          <div className="sandbox-recommendation-results">
            <h3>{t('products_that_recommended_for_you')}</h3>

            {results.map((product: ProductType) => {
              return (
                <div key={product.product_id} className="sandbox-recommendation-results__item">
                  <i>{product.brand}</i>
                  <div className="sandbox-recommendation-results__item-title">{product.title}</div>
                </div>
              );
            })}
          </div>
        )}
        {results.length === 0 && requested ? <h3>{t('no_results')}</h3> : null}
      </div>
    </Page>
  );
}
