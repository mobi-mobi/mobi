import React, { useState, useCallback, useRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import Page from '../../../components/base/Page';
import SandboxSettings from '../../../components/common/Sandbox/SandboxSettings';

import useDebounce from '../../../hooks/useDebounce';

import Api from '../../../services/Api';
import config from '../../../config';

import { ProductType } from '../../../containers/products/types';
import { Input, Button } from 'semantic-ui-react';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function Search() {
  const { t } = useTranslation();
  const { brands, client, userHistory } = useSelector((state: any) => ({
    client: state.home.client,
    brands: state.sandbox.sandboxBrands,
    userHistory: state.sandbox.userHistory,
  }));

  const isInitialMounted = useRef(true);
  const [error, setError] = useState<string | null>(null);
  const [query, setQuery] = useState<string | null>(null);
  const [results, setResults] = useState<Array<ProductType>>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [requested, setRequested] = useState<boolean>(false);

  const debouncedSearchTerm = useDebounce(query, 300);

  const search = useCallback(async () => {
    if (!debouncedSearchTerm) {
      return;
    }

    setIsLoading(true);

    let url = `/${client}/test/search?q=${debouncedSearchTerm}`;

    if (brands) {
      url += brands.map((brand: any) => `&brands=${brand.value}`).join('&');
    }

    if (userHistory) {
      const mapped = userHistory.map((selectedProduct: any) => ({
        product_id: selectedProduct.value.product_id,
        event_type: selectedProduct.eventType,
      }));

      const history = encodeURIComponent(JSON.stringify(mapped));

      url += `&browsing_history=${history}`;
    } else {
      url += `&browsing_history=[]`;
    }

    url += `&num=20`;

    apiInstance
      .get(url, {})
      .then((res) => {
        const data = res.data.map((fullProduct: any) => fullProduct.product);

        setError(null);
        setResults(data);
        setRequested(true);
      })
      .catch((error) => {
        setError(error.error.description);
        setRequested(false);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [brands, userHistory, client, debouncedSearchTerm]);

  useEffect(() => {
    if (isInitialMounted.current) {
      isInitialMounted.current = false;
    } else {
      search();
    }
  }, [search]);

  const handleSearchClick = useCallback(
    async () => {
      await search();
    },
    [search]
  );

  const handleChangeQuery = useCallback(async (e) => {
    setQuery(e.currentTarget.value);
  }, []);

  return (
    <Page name="page--sandbox-recommendation">
      <h1>
        {t('sandbox')} / {t('search')}
      </h1>

      <div className="page-content">
        <div className="sandbox-similar-product__input">
          <Input onChange={handleChangeQuery} loading={isLoading} placeholder={t('search_for_products')} />
        </div>

        <SandboxSettings
          actions={[
            <Button key="sandbox-search-button" color="green" onClick={handleSearchClick}>
              {t('search')}
            </Button>,
          ]}
        />

        {error && <div className="sandbox-recommendation-results__error">{JSON.stringify(error)}</div>}
        {Boolean(results.length) && (
          <div className="sandbox-recommendation-results">
            <h3>
              {t('search_results_to')} {query}
            </h3>

            {results.map((product: ProductType) => (
              <div key={product.product_id} className="sandbox-recommendation-results__item">
                <i>{product.brand}</i>
                <div className="sandbox-recommendation-results__item-title">{product.title}</div>
              </div>
            ))}
          </div>
        )}

        {results.length === 0 && requested ? <h3>{t('no_results')}</h3> : null}
      </div>
    </Page>
  );
}
