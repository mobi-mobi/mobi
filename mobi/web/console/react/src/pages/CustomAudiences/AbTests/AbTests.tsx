import * as React from 'react';
import { useTranslation } from 'react-i18next';

import Page from '../../../components/base/Page';

export default function AbTests() {
  const { t } = useTranslation();

  return (
    <Page name="page--brands">
      <h1>{t('ab_tests')}</h1>

      <div className="page-content">
        AbTests
      </div>
    </Page>
  );
}
