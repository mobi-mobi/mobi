export interface IAudience {
  audience_id: string;
  audience_name: string;
  audience_description: string;
}

export interface IAudienceUnion {
  audiences_union_id: string;
  audiences_union_name: string;
  audiences_union_description: string;
  audience_ids: string[];
  presented_in_split_trees: string[];
}

export interface IAudienceResponse {
  audience: IAudience;
  presented_in_audiences_unions: any[];
  presented_in_split_trees: any[];
}

export interface IAudienceUnionResponse {
  audiences_union: IAudienceUnion;
}

export interface IAudienceTableGetResponse {
  status: string;
  data: {
    audiences: IAudienceResponse[];
    audiences_unions: IAudienceUnionResponse[];
    split_trees: any[];
  };
}
