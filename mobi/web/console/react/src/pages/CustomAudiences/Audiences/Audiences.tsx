import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Icon, Button, Confirm, Popup } from 'semantic-ui-react';

import Page from '../../../components/base/Page';
import AddAudienceModal from '../../../components/common/modals/AddAudience';
import AddAudienceUnionModal from '../../../components/common/modals/AddAudienceUnion';

import config from '../../../config';
import Api from '../../../services/Api';

import { IAudienceTableGetResponse } from './interfaces';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

interface IDeletingAudience {
  type: 'audiences' | 'audiences_unions';
  id: string;
}

export default function Audiences() {
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const [data, setData] = useState<IAudienceTableGetResponse | null>(null);
  const [isOpened, setIsOpened] = useState<string | null>(null);
  const [isDeleting, setIsDeleting] = useState<IDeletingAudience | null>(null);

  const fetchTables = useCallback(() => {
    apiInstance.get(`/${client}/pages/audiences`, {}).then((res: IAudienceTableGetResponse) => {
      setData(res);
    });
  }, [client]);

  useEffect(() => {
    fetchTables();
  }, [fetchTables]);

  const confirmDelete = (deleteSetting: IDeletingAudience) => {
    setIsDeleting(deleteSetting);
  };

  const handleDeleteConfirm = () => {
    if (!isDeleting) {
      return;
    }

    apiInstance
      .delete(`/${client}/${isDeleting.type}?audience_id=${isDeleting.id}`, {})
      .then(() => {
        fetchTables();
      })
      .catch((e) => {
        alert(e);
      })
      .finally(() => {
        setIsDeleting(null);
      });
  };

  return (
    <Page name="page--brands">
      <h1>
        {t('custom_audiences')} / {t('audiences_table')}
      </h1>

      <div className="page-content">
        <div className="audience-groups">
          <div className="audience-groups-group">
            <div className="audience-groups-group-header">
              <div className="audience-groups-group-header__title">{t('audiences')}</div>
              <div className="audience-groups-group-header__actions">
                <AddAudienceModal onSubmit={fetchTables} />
              </div>
            </div>

            <div className="audience-groups-group-body">
              {data &&
                data.data.audiences.map((audience) => {
                  return (
                    <div key={audience.audience.audience_id} className="fluid-card">
                      <div className="fluid-card-preview">
                        <div className="fluid-card-information">
                          <div className="fluid-card-information__id">
                            <span className="id">ID: {audience.audience.audience_id}</span>
                          </div>
                          <div
                            className="fluid-card-information__name fluid-card-information__name--audience"
                            onClick={() => setIsOpened(audience.audience.audience_id)}
                          >
                            {audience.audience.audience_name}
                            {audience.presented_in_split_trees.length === 0 && (
                              <div className="audience-not-assigned">
                                <Popup
                                  wide="very"
                                  trigger={<Icon color="yellow" size="tiny" name="warning sign" />}
                                  content={t('audience_is_not_assigned_to_split')}
                                  position="right center"
                                />
                              </div>
                            )}
                          </div>
                          <div className="fluid-card-information__next-campaign">
                            {audience.audience.audience_description}
                          </div>
                        </div>

                        <div className="fluid-card-actions">
                          <AddAudienceModal
                            onSubmit={fetchTables}
                            onClose={() => {
                              setIsOpened(null);
                            }}
                            isOpened={isOpened}
                            edit={audience.audience}
                          />
                          <Button
                            onClick={() =>
                              confirmDelete({
                                type: 'audiences',
                                id: audience.audience.audience_id,
                              })
                            }
                            basic
                            size="tiny"
                            color="red"
                          >
                            {t('delete')}
                          </Button>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
          <div className="audience-groups-group">
            <div className="audience-groups-group-header">
              <div className="audience-groups-group-header__title">{t('audiences_union')}</div>
              <div className="audience-groups-group-header__actions">
                <AddAudienceUnionModal onSubmit={fetchTables} audiences={data?.data.audiences} />
              </div>
            </div>
            <div className="audience-groups-group-body">
              {data &&
                data.data.audiences_unions.map((union) => {
                  return (
                    <div key={union.audiences_union.audiences_union_id} className="fluid-card">
                      <div className="fluid-card-preview">
                        <div className="fluid-card-information">
                          <div className="fluid-card-information__id">
                            <span className="id">ID: {union.audiences_union.audiences_union_id}</span>
                          </div>
                          <div
                            className="fluid-card-information__name"
                            onClick={() => setIsOpened(union.audiences_union.audiences_union_id)}
                          >
                            {union.audiences_union.audiences_union_name}
                          </div>
                          <div className="fluid-card-information__next-campaign">
                            {union.audiences_union.audiences_union_description}
                          </div>
                        </div>

                        <div className="fluid-card-statistic">
                          <div className="fluid-card-statistic">
                            <strong>{t('audience_union_ids')}:</strong>{' '}
                            {union.audiences_union.audience_ids.map((id: string) => {
                              const audience = data.data.audiences.find((val) => val.audience.audience_id === id);

                              return (
                                <div key={audience?.audience.audience_name || id}>
                                  {audience?.audience.audience_name || id}
                                </div>
                              );
                            })}
                          </div>
                        </div>
                        <div className="fluid-card-actions">
                          <AddAudienceUnionModal
                            onSubmit={fetchTables}
                            audiences={data?.data.audiences}
                            edit={union.audiences_union}
                            onClose={() => {
                              setIsOpened(null);
                            }}
                            isOpened={isOpened}
                          />
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>

      <Confirm
        open={!!isDeleting}
        content={t('delete_audience_message')}
        onCancel={() => setIsDeleting(null)}
        onConfirm={handleDeleteConfirm}
      />
    </Page>
  );
}
