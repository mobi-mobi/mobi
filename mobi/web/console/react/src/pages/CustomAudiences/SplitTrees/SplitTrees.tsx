import * as React from 'react';
import { useTranslation } from 'react-i18next';

import Page from '../../../components/base/Page';

export default function SplitTrees() {
  const { t } = useTranslation();

  return (
    <Page name="page--brands">
      <h1>{t('split_trees')}</h1>

      <div className="page-content">
        SplitTrees
      </div>
    </Page>
  );
}
