import React, { useEffect, useState, useCallback, useRef, useMemo, SyntheticEvent } from 'react';
import { Input, Button } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import useDebounce from '../../../hooks/useDebounce';

import Page from '../../../components/base/Page';
import BrandList from '../../../components/common/Brands/List';

import { getBrandsInfinite, BRANDS_LIST_LIMIT } from '../../../containers/brands/actions';
import LoadingSpinner from '../../../components/ui/LoadingSpinner';
import { FullBrandType, ACTION_NAMES } from '../../../containers/brands/types';

const offsetLimit = BRANDS_LIST_LIMIT - 1;

export default function Brands() {
  const { t } = useTranslation();
  const [showDisabled, setShowDisabled] = useState<boolean>(false);
  const [showOutOfStock, setShowOutOfStock] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [query, setQuery] = useState<string>('');
  const [brands, setBrands] = useState<FullBrandType[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [brandFound, setBrandFound] = useState<number | null>(null);
  const isInitialMount = useRef(true);
  const dispatch = useDispatch();
  const { client } = useSelector((store: any) => ({
    client: store.home.client,
  }));

  const debouncedSearchTerm = useDebounce(query, 300);

  useEffect(() => {
    (async function get() {
      if (debouncedSearchTerm || isInitialMount.current) {
        isInitialMount.current = false;
        await loadBrands();
      }
    })();
  }, [client, query, showDisabled, showOutOfStock, debouncedSearchTerm, dispatch]);

  useEffect(() => {
    (async function get() {
      await loadBrands();
    })();
  }, [client]);

  useEffect(() => {
    setCurrentPage(1);
    setBrands([]);
  }, [showDisabled, showOutOfStock, query]);

  async function loadBrands() {
    setIsLoading(true);

    const response = await getBrandsInfinite(client, {
      q: query,
      offset: currentPage === 1 ? 0 : offsetLimit * currentPage,
    });

    const receivedBrands = [...response?.brands];

    setBrands(
      receivedBrands.length < 21
        ? [...receivedBrands]
        : receivedBrands.filter((p: any, id: number) => id !== receivedBrands.length - 1)
    );
    setBrandFound(response?.totalFoundCount);
    setIsLoading(false);
  }

  const handleQueryOnChange = useCallback((e: SyntheticEvent<HTMLInputElement>) => {
    setQuery(e.currentTarget.value);
  }, []);

  const loadMore = useCallback(async () => {
    setIsLoading(true);

    const response = await getBrandsInfinite(client, {
      q: query,
      offset: offsetLimit * currentPage,
    });

    const receivedBrands = [...response?.brands];
    const withoutLatestItem =
      receivedBrands.length < 21
        ? receivedBrands
        : receivedBrands.filter((p: any, id: number) => id !== receivedBrands.length - 1);

    setBrands((prevState) => [...prevState, ...withoutLatestItem]);
    setBrandFound(response?.totalFoundCount);
    setIsLoading(false);
  }, [client, query, showDisabled, showOutOfStock, currentPage]);

  const handleOnLoadMore = useCallback(async () => {
    setCurrentPage(currentPage + 1);

    await loadMore();
  }, [currentPage, loadMore]);

  const totalPages = useMemo(() => (brandFound ? Math.ceil(brandFound / BRANDS_LIST_LIMIT) : 0), [brandFound]);

  return (
    <Page name="page--brands">
      <h1>{t('nav_brands')}</h1>

      <div className="page-header">
        <div className="fluid-card-filters">
          <Input
            placeholder={t('brands_filter')}
            loading={Boolean(query) && isLoading}
            onChange={handleQueryOnChange}
          />
        </div>
      </div>

      <div className="page-content">
        {brandFound !== null ? (
          <h3 className="total-product-found">
            {t('total_brands_found')}: {brandFound}
          </h3>
        ) : null}

        <BrandList items={brands} />

        {isLoading ? <LoadingSpinner relative size="small" /> : null}

        <div className="load-more">
          {brands.length ? (
            <Button onClick={handleOnLoadMore} primary disabled={currentPage === totalPages || isLoading}>
              {t('load_more')}
            </Button>
          ) : null}
        </div>
      </div>
    </Page>
  );
}
