import React, { useEffect, useState, useCallback, useRef, useMemo, SyntheticEvent } from 'react';
import { Input, Checkbox, Button } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import useDebounce from '../../../hooks/useDebounce';

import Page from '../../../components/base/Page';
import ProductsList from '../../../components/common/Products';
import Metrics from '../../../components/common/Products/Metrics';

import { getProductsInfinite, PRODUCT_LIST_LIMIT } from '../../../containers/products/actions';
import LoadingSpinner from '../../../components/ui/LoadingSpinner';
import { FullProductType, ACTION_NAMES } from '../../../containers/products/types';

const offsetLimit = PRODUCT_LIST_LIMIT - 1;

export default function Products() {
  const { t } = useTranslation();
  const [showDisabled, setShowDisabled] = useState<boolean>(false);
  const [showOutOfStock, setShowOutOfStock] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const dispatch = useDispatch();
  const [query, setQuery] = useState<string>('');
  const [products, setProducts] = useState<Array<FullProductType>>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [productFound, setProductFound] = useState<number | null>(null);
  const isInitialMount = useRef(true);
  const { client, metrics } = useSelector((store: any) => ({
    client: store.home.client,
    metrics: store.products.metrics,
  }));

  const debouncedSearchTerm = useDebounce(query, 300);

  useEffect(() => {
    (async function get() {
      if (debouncedSearchTerm || isInitialMount.current) {
        isInitialMount.current = false;
        await loadProducts();
      }
    })();
  }, [client, query, showDisabled, showOutOfStock, debouncedSearchTerm, dispatch]);

  useEffect(() => {
    (async function get() {
      await loadProducts();
    })();
  }, [client]);

  useEffect(() => {
    setCurrentPage(1);
    setProducts([]);
  }, [showDisabled, showOutOfStock, query]);

  async function loadProducts() {
    setIsLoading(true);

    const response = await getProductsInfinite(client, {
      q: query,
      offset: currentPage === 1 ? 0 : offsetLimit * currentPage,
      showDisabled,
      showOutStock: showOutOfStock,
    });

    const receivedProducts = [...response?.products];

    setProducts(
      receivedProducts.length < 21
        ? [...receivedProducts]
        : receivedProducts.filter((p: any, id: number) => id !== receivedProducts.length - 1)
    );
    setProductFound(response?.totalFoundCount);
    setIsLoading(false);

    if (response && response.metrics) {
      dispatch({
        type: ACTION_NAMES.PRODUCTS_SET_METRICS,
        payload: {
          productsInStock: response?.metrics.active_and_in_stock_num,
          productsNotInStock: response?.metrics.active_and_not_in_stock_num,
          disabledProducts: response?.metrics.disabled_num,
        },
      });
    }
  }

  const handleQueryOnChange = useCallback((e: SyntheticEvent<HTMLInputElement>) => {
    setQuery(e.currentTarget.value);
  }, []);

  const handleChangeShowDisabled = useCallback(() => {
    setShowDisabled(!showDisabled);
  }, [showDisabled]);

  const handleChangeOutOfStock = useCallback(() => {
    setShowOutOfStock(!showOutOfStock);
  }, [showOutOfStock]);

  const loadMore = useCallback(async () => {
    setIsLoading(true);

    const response = await getProductsInfinite(client, {
      q: query,
      offset: offsetLimit * currentPage,
      showDisabled,
      showOutStock: showOutOfStock,
    });

    const receivedProducts = [...response?.products];
    const withoutLatestItem =
      receivedProducts.length < 21
        ? receivedProducts
        : receivedProducts.filter((p: any, id: number) => id !== receivedProducts.length - 1);

    setProducts((prevState) => [...prevState, ...withoutLatestItem]);
    setProductFound(response?.totalFoundCount);
    setIsLoading(false);
  }, [client, query, showDisabled, showOutOfStock, currentPage]);

  const handleOnLoadMore = useCallback(async () => {
    setCurrentPage(currentPage + 1);

    await loadMore();
  }, [currentPage, loadMore]);

  const totalPages = useMemo(() => {
    let count = productFound ? productFound : metrics.productsInStock;

    if (showOutOfStock) {
      count += metrics.productsNotInStock;
    }

    if (showDisabled) {
      count += metrics.disabledProducts;
    }

    return Math.ceil(count / PRODUCT_LIST_LIMIT);
  }, [
    metrics.productsInStock,
    metrics.disabledProducts,
    metrics.productsNotInStock,
    showDisabled,
    showOutOfStock,
    productFound,
  ]);

  return (
    <Page name="page--products">
      <h1>{t('products')}</h1>

      <div className="page-metrics">
        <Metrics data={metrics} />
      </div>

      <div className="page-header">
        <div className="fluid-card-filters">
          <Input
            placeholder={t('products_filter')}
            loading={Boolean(query) && isLoading}
            onChange={handleQueryOnChange}
          />
        </div>
        <div className="fluid-card-show-disabled">
          <Checkbox label={t('products_show_disabled')} onChange={handleChangeShowDisabled} />
          <Checkbox label={t('products_show_empty_stock')} onChange={handleChangeOutOfStock} />
        </div>
      </div>

      <div className="page-content">
        {productFound !== null ? (
          <h3 className="total-product-found">
            {t('total_products_found')}: {productFound}
          </h3>
        ) : null}

        <ProductsList items={products} />

        {isLoading ? <LoadingSpinner relative size="small" /> : null}

        <div className="load-more">
          {products.length ? (
            <Button onClick={handleOnLoadMore} primary disabled={currentPage === totalPages || isLoading}>
              {t('load_more')}
            </Button>
          ) : null}
        </div>
      </div>
    </Page>
  );
}
