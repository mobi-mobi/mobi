export type TTokenPermissions =
  | 'get_recommendations'
  | 'get_model_info'
  | 'read_metrics'
  | 'read_catalog'
  | 'search'
  | 'update_catalog'
  | 'send_events'
  | 'test_recommendations';

export interface IToken {
  description: string;
  expiration_date: string;
  first_letters: string;
  permissions: TTokenPermissions[];
  token_hash: string;
  token_type: string;
}

export interface ITokenParameters {
  [key: string]: Array<IToken>;
}

export interface ITokenDataResponse {
  status: string;
  data: ITokenParameters;
}
