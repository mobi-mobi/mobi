import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Table, Button, Confirm } from 'semantic-ui-react';

import AddTokenModal from '../../../components/common/modals/AddToken';
import Page from '../../../components/base/Page';

import Api from '../../../services/Api';
import config from '../../../config';

import LoadingSpinner from '../../../components/ui/LoadingSpinner';

import { ITokenParameters, ITokenDataResponse, IToken } from './interfaces';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

interface IDeleteParams {
  client: string;
  hash: string;
}

export default function Tokens() {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isDeleting, setIsDeleting] = useState<IDeleteParams | null>(null);
  const [tokens, setTokens] = useState<ITokenParameters | null>(null);

  const getTokens = () => {
    setIsLoading(true);

    apiInstance.get('/user/settings/tokens', {}).then((res: ITokenDataResponse) => {
      if (res.status === 'OK') {
        setIsLoading(false);
        setTokens(res.data);
      }
    });
  };

  useEffect(() => {
    getTokens();
  }, []);

  const handleDelete = (params: IDeleteParams) => {
    setIsDeleting(params);
  };

  const handleCloseAddingModal = () => {
    getTokens();
  };

  const handleDeleteConfirm = () => {
    if (isDeleting) {
      setIsDeleting(null);

      apiInstance
        .delete(`/user/settings/tokens?client=${isDeleting.client}&token_hash=${isDeleting.hash}`, {})
        .then((res) => {
          if (res.status === 'OK') {
            getTokens();
          }
        });
    }
  };

  const renderTokens = useMemo(() => {
    if (!tokens) {
      return null;
    }

    const clients = Object.keys(tokens);

    return clients.map((client: string) => {
      return tokens[client].length ? (
        <div className="tokens__client" key={client}>
          <h3>
            {t('client')}: {client}
          </h3>

          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>{t('token_exp_data')}</Table.HeaderCell>
                <Table.HeaderCell>{t('token_description')}</Table.HeaderCell>
                <Table.HeaderCell>{t('token_is_test')}</Table.HeaderCell>
                <Table.HeaderCell>{t('token_token')}</Table.HeaderCell>
                <Table.HeaderCell>{t('token_permissions')}</Table.HeaderCell>
                <Table.HeaderCell>{t('token_delete')}</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {tokens[client].map((token: IToken) => {
                return (
                  <Table.Row key={`${client}-${token.token_hash}`}>
                    <Table.Cell>{token.expiration_date}</Table.Cell>
                    <Table.Cell>{token.description}</Table.Cell>
                    <Table.Cell>{token.token_type === 'production' ? t('no') : t('yes')}</Table.Cell>
                    <Table.Cell>{token.first_letters}*****</Table.Cell>
                    <Table.Cell>
                      <ul>
                        {token.permissions.map((val: string) => (
                          <li key={`${client}-${val}`}>{t(`permission_${val}`)}</li>
                        ))}
                      </ul>
                    </Table.Cell>
                    <Table.Cell>
                      <Button
                        color="red"
                        onClick={() =>
                          handleDelete({
                            client,
                            hash: token.token_hash,
                          })
                        }
                      >
                        {t('delete')}
                      </Button>
                    </Table.Cell>
                  </Table.Row>
                );
              })}
            </Table.Body>
          </Table>
        </div>
      ) : null;
    });
  }, [t, tokens]);

  return (
    <Page name="page--tokens">
      <h1 className="title title--extended">
        {t('user_settings')} / {t('user_settings_tokens')}
        <AddTokenModal onSubmit={handleCloseAddingModal} />
      </h1>

      <div className="page-content">{isLoading ? <LoadingSpinner relative size="small" /> : <>{renderTokens}</>}</div>

      <Confirm
        open={!!isDeleting}
        content={t('delete_token_message')}
        onCancel={() => setIsDeleting(null)}
        onConfirm={handleDeleteConfirm}
      />
    </Page>
  );
}
