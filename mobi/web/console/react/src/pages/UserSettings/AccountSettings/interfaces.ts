export interface IAccountSettingData {
  email: string;
  name: string;
}

export interface IAccountSettingPassword {
  new_password: string;
  new_password_repeated: string;
}

export interface IAccountSettingResponse {
  status: string;
  data: IAccountSettingData
}
