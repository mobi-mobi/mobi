import React, { useState, useEffect, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Input } from 'semantic-ui-react';

import Page from '../../../components/base/Page';

import Api from '../../../services/Api';
import config from '../../../config';

import { IAccountSettingResponse, IAccountSettingData, IAccountSettingPassword } from './interfaces';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

export default function AccountSettings() {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const [accountSettingData, setAccountSettingData] = useState<IAccountSettingData>({
    email: '',
    name: '',
  });

  const [password, setPassword] = useState<IAccountSettingPassword>({
    new_password: '',
    new_password_repeated: '',
  });

  useEffect(() => {
    setIsLoading(true);

    apiInstance.get('/user/settings/account', {}).then((res: IAccountSettingResponse) => {
      if (res.status === 'OK') {
        setAccountSettingData(res.data);
        setIsLoading(false);
      }
    });
  }, []);

  const changeValue = useCallback((key: string, value: any) => {
    setAccountSettingData((prevState) => ({
      ...prevState,
      [key]: value,
    }));
  }, []);

  const changePasswordValue = useCallback((key: string, value: any) => {
    setPassword((prevState) => ({
      ...prevState,
      [key]: value,
    }));
  }, []);

  const handleUpdateGeneralInformation = useCallback(() => {
    setIsLoading(true);

    apiInstance.put('/user/settings/account', {
      body: JSON.stringify({
        name: accountSettingData.name
      }),
    }).finally(() => {
      setIsLoading(false);
    });
  }, [accountSettingData]);

  const handleUpdatePassword = useCallback(() => {
    setIsLoading(true);

    apiInstance.put('/user/settings/password', {
      body: JSON.stringify(password),
    }).then(() => {
      setPassword({ new_password: '', new_password_repeated: '' });

      /** @TODO notifications and translations */
      alert('Password successfully changed');
    }).catch(err => {
      alert(err);
    }).finally(() => {
      setIsLoading(false);
    });
  }, [password]);

  return (
    <Page name="page--account-settings">
      <h1>
        {t('user_settings')} / {t('account_settings')}
      </h1>

      <div className="page-content">
        <h3>{t('account_settings_details')}</h3>
        <div className="user-settings-fields">
          <div className="user-settings-fields__item">
            <Input
              label={t('user_name')}
              placeholder={t('user_name')}
              type="text"
              value={accountSettingData.name}
              loading={isLoading}
              onChange={(e) => changeValue('name', e.currentTarget.value)}
            />
          </div>
          <div className="user-settings-fields__item">
            <Input
              loading={isLoading}
              value={accountSettingData.email}
              label={t('user_email')}
              placeholder={t('user_email')}
              disabled
              type="text"
            />
          </div>
          <div className="user-settings__actions">
            <Button primary onClick={handleUpdateGeneralInformation} loading={isLoading}>
              {t('update')}
            </Button>
          </div>
        </div>
        <h3>{t('account_settings_edit_password')}</h3>
        <div className="user-settings-fields">
          <div className="user-settings-fields__item">
            <Input
              label={t('new_password')}
              placeholder={t('new_password')}
              type="password"
              loading={isLoading}
              value={password.new_password}
              onChange={(e) => changePasswordValue('new_password', e.currentTarget.value)}
            />
          </div>
          <div className="user-settings-fields__item">
            <Input
              label={t('new_password_repeat')}
              placeholder={t('new_password_repeat')}
              type="password"
              value={password.new_password_repeated}
              loading={isLoading}
              onChange={(e) => changePasswordValue('new_password_repeated', e.currentTarget.value)}
            />
          </div>
          <div className="user-settings__actions">
            <Button primary onClick={handleUpdatePassword} loading={isLoading}>
              {t('update')}
            </Button>
          </div>
        </div>
      </div>
    </Page>
  );
}
