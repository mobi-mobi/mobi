import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Input } from 'semantic-ui-react';

import Page from '../../../components/base/Page';

import Api from '../../../services/Api';
import config from '../../../config';

import {
  ISubscribeParameters,
  ISubscribeParametersData,
  ISubscriptionsDataResponse,
  IUnsubscribeParameters,
} from './interfaces';
import LoadingSpinner from '../../../components/ui/LoadingSpinner';

const apiInstance = new Api(config.global.REACT_APP_API_URL);

const convertUrl = (obj: object) =>
  Object.entries(obj)
    .map(([key, val]) => `${key}=${val}`)
    .join('&');

export default function Subscriptions() {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [canSubscriptions, setCanSubscriptions] = useState<ISubscribeParameters | null>();
  const [currentSubscriptions, setCurrentSubscriptions] = useState<IUnsubscribeParameters | null>();

  const getSubData = () => {
    apiInstance.get('/user/settings/subscriptions', {}).then((res: ISubscriptionsDataResponse) => {
      if (res.status === 'OK') {
        setCanSubscriptions(res.data.can_subscribe_to);
        setCurrentSubscriptions(res.data.current_subscriptions);

        setIsLoading(false);
      }
    });
  };

  useEffect(() => {
    setIsLoading(true);
    getSubData();
  }, []);

  const handleControlSubscribe = useCallback((params: ISubscribeParametersData, isDeleting: boolean) => {
    const urlParam = convertUrl(params);

    const request = isDeleting
      ? apiInstance.delete(`/user/settings/subscriptions?${urlParam}`, {})
      : apiInstance.post(`/user/settings/subscriptions?${urlParam}`, {});

    request.then((res) => {
      if (res.status === 'OK') {
        getSubData();
      }
    });
  }, []);

  const canSubscriptionsRender = useMemo(() => {
    if (!canSubscriptions) {
      return null;
    }

    const keys = Object.keys(canSubscriptions);

    if (keys.length === 0) {
      return <p>{t('you_have_no_active_subscriptions')}</p>;
    }

    return keys.map((key: string, i: number) => {
      return canSubscriptions[keys[i]].length ? (
        <div key={key} className="user-settings-fields">
          <p>
            {t('client')}: {key}
          </p>
          {canSubscriptions[keys[i]].map((val) => (
            <div key={val.title} className="user-settings-fields__item user-settings-fields__item--subscribe">
              <Button onClick={() => handleControlSubscribe(val.subscribe_parameters, false)} primary>
                {t('subscribe')}
              </Button>
              <div>{t(`s_${val.title.replace(/ /g, '_')}`)}</div>
            </div>
          ))}
        </div>
      ) : null;
    });
  }, [t, canSubscriptions]);

  const currentSubscriptionsRender = useMemo(() => {
    if (!currentSubscriptions) {
      return null;
    }

    const keys = Object.keys(currentSubscriptions);

    const elements = keys
      .map((key: string, i: number) => {
        return currentSubscriptions[keys[i]].length ? (
          <div key={key} className="user-settings-fields">
            <p>
              {t('client')}: {key}
            </p>
            {currentSubscriptions[keys[i]].map((val) => (
              <div key={val.title} className="user-settings-fields__item user-settings-fields__item--subscribe">
                <Button onClick={() => handleControlSubscribe(val.unsubscribe_parameters, true)} color="red">
                  {t('unsubscribe')}
                </Button>
                <div>{t(`s_${val.title.replace(/ /g, '_')}`)}</div>
              </div>
            ))}
          </div>
        ) : null;
      })
      .filter((v) => !!v);

    return !elements.length ? t('no_subscriptions') : elements;
  }, [t, currentSubscriptions]);

  return (
    <Page name="page--account-settings">
      <h1>
        {t('user_settings')} / {t('subscriptions')}
      </h1>

      <div className="page-content">
        {isLoading ? (
          <LoadingSpinner relative size="small" />
        ) : (
          <>
            <h3>{t('current_subscriptions')}</h3>
            {currentSubscriptionsRender}
            <h3>{t('available_subscriptions')}</h3>
            {canSubscriptionsRender}
          </>
        )}
      </div>
    </Page>
  );
}
