export interface ISubscribeParametersData {
  client: string;
  report_subscription: string;
  type: string;
}

export interface IUnsubscribeParametersData {
  client: string;
  report_subscription: string;
  type: string;
}

export interface ISubscribeParameters {
  [key: string]: Array<{ title: string; subscribe_parameters: ISubscribeParametersData }>;
}

export interface IUnsubscribeParameters {
  [key: string]: Array<{ title: string; unsubscribe_parameters: ISubscribeParametersData }>;
}

export interface ISubscriptionsDataResponse {
  status: string;
  data: {
    can_subscribe_to: ISubscribeParameters;
    current_subscriptions: IUnsubscribeParameters;
  };
}
