import { ZoneTypeFull } from "../../containers/zones/types";

export interface IZonesStore {
  client: string;
  zones: Array<ZoneTypeFull>;
  zonesCount: number;
}
