import React, { useEffect, useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { Input, Checkbox, Pagination } from 'semantic-ui-react';

import Page from '../../components/base/Page';
import ZonesList from '../../components/common/Zones';
import AddZoneModal from '../../components/common/modals/AddZone';

import { getZones } from '../../containers/zones/actions';

import { IZonesStore } from './interfaces';

export default function Zones() {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const store: IZonesStore = useSelector((state: any) => ({
    client: state.home.client,
    zones: state.zones.zones,
    zonesCount: state.zones.zonesCount,
  }));

  const [query, setQuery] = useState('');
  const [showDisabled, setShowDisabled] = useState(false);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [elementsOnPage] = useState(10);

  const handleChangeDisabledFilter = useCallback(() => {
    setShowDisabled(!showDisabled);
  }, [showDisabled]);

  const handleChangeFilterByName = useCallback((e) => {
    setQuery(e.currentTarget.value);
  }, []);

  useEffect(() => {
    setTotalPages(Math.ceil(store.zonesCount / elementsOnPage));
  }, [store.zonesCount, elementsOnPage]);

  useEffect(() => {
    getZones(store.client, {
      limit: elementsOnPage,
      offset: 0,
    })(dispatch);
  }, [store.client, dispatch, elementsOnPage]);

  useEffect(() => {
    getZones(store.client, {
      limit: elementsOnPage,
      showDisabled,
      q: query,
      offset: elementsOnPage * (currentPage - 1),
    })(dispatch);
  }, [showDisabled, query, currentPage, elementsOnPage, store.client, dispatch]);

  const handlePaginationChange = useCallback((e, { activePage }) => {
    setCurrentPage(activePage);
  }, []);

  return (
    <Page name="page--zone">
      <h1>{t('zones_page_title')}</h1>

      <div className="page-header">
        <div className="fluid-card-filters">
          <Input placeholder={t('zone_filter')} onChange={handleChangeFilterByName} />
        </div>
        <div className="fluid-card-actions">
          <AddZoneModal />
        </div>
        <div className="fluid-card-show-disabled">
          <Checkbox label={t('zone_show_disabled')} onChange={handleChangeDisabledFilter} />
        </div>
      </div>

      <div className="fluid-card-page-body">
        <ZonesList emptyText="zone_empty_text" items={store.zones} />

        {totalPages > 1 && store.zones.length ? (
          <Pagination activePage={currentPage} onPageChange={handlePaginationChange} totalPages={totalPages} />
        ) : null}
      </div>
    </Page>
  );
}
