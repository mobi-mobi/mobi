import React from 'react';
import { useTranslation } from 'react-i18next';

import Page from '../../../components/base/Page';
import Conversion from '../../../components/common/Metrics/Conversion';

export default function MetricsIntegrationPage() {
  const { t } = useTranslation();

  return (
    <Page name="page--metrics-integration">
      <h1>
        {t('metrics')} / {t('integration')}
      </h1>

      <div className="page-content">
        <Conversion />
      </div>
    </Page>
  );
}
