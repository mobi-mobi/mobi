import React from 'react';
import { useTranslation } from 'react-i18next';

import Page from '../../../components/base/Page';

export default function MetricsAttributionPage() {
  const { t } = useTranslation();

  return (
    <Page name="page--metrics-attribution">
      <h1>
        {t('metrics')} / {t('attribution')}
      </h1>

      <div className="page-content">TBD</div>
    </Page>
  );
}
