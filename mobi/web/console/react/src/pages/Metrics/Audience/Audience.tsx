import React from 'react';
import { useTranslation } from 'react-i18next';

import Page from '../../../components/base/Page';
import UniqueMetricsCharts from '../../../components/buisness/AudienceCharts/UniqueMetricsCharts';

export default function MetricsAudiencePage() {
  const { t } = useTranslation();

  return (
    <Page name="page--metrics-audience">
      <h1>
        {t('metrics')} / {t('audience')}
      </h1>

      <div className="page-content">
        <div className="metrics">
          <UniqueMetricsCharts />
        </div>
      </div>
    </Page>
  );
}
