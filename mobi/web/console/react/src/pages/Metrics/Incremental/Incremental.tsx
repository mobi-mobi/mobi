import React from 'react';
import { useTranslation } from 'react-i18next';

import Page from '../../../components/base/Page';
import ABCharts from '../../../components/buisness/ABCharts';

export default function MetricsIncrementalPage() {
  const { t } = useTranslation();

  return (
    <Page name="page--metrics-incremental">
      <h1>
        {t('metrics')} / {t('incremental')}
      </h1>

      <div className="page-content">
        <div className="metrics">
          <ABCharts />
        </div>
      </div>
    </Page>
  );
}
