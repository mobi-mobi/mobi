import React from 'react';
import { useTranslation } from 'react-i18next';

import Page from '../../../components/base/Page';

export default function MetricsRealTimePage() {
  const { t } = useTranslation();

  return (
    <Page name="page--metrics-real-time">
      <h1>
        {t('metrics')} / {t('real_time')}
      </h1>

      <div className="page-content">TBD</div>
    </Page>
  );
}
