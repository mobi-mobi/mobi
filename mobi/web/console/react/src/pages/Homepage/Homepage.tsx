import React from 'react';

import Page from '../../components/base/Page';

import RealTimeInfo from '../../components/common/Dashboard/RealtimeInfo';
import StatsConversionInfo from '../../components/common/Dashboard/StatsConversionInfo';
import IncrementalInfo from '../../components/common/Dashboard/IncrementalInfo';
import IntegrationInfo from '../../components/common/Dashboard/IntegrationInfo';
import AudienceInfo from '../../components/common/Dashboard/AudienceInfo';
import TrafficSourceInfo from '../../components/common/Dashboard/TrafficSourceInfo';

export default function Homepage() {
  return (
    <Page name="home">
      <RealTimeInfo />
      <AudienceInfo />
      <TrafficSourceInfo />
      <StatsConversionInfo />
      <IncrementalInfo />
      <IntegrationInfo />
    </Page>
  );
}
