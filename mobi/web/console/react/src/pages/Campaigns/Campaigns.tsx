import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Input, Checkbox } from 'semantic-ui-react';
import { useTranslation } from 'react-i18next';

import Page from '../../components/base/Page';
import AddCampaign from '../../components/common/modals/AddCampaign';
import CampaignsList from '../../components/common/Campaigns';

import { getCampaigns } from '../../containers/campaign/actions';
import { CampaignTypeFull } from '../../containers/campaign/types';

export default function Campaigns() {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const client: string = useSelector((state: any) => state.home.client);
  const campaigns: Array<CampaignTypeFull> = useSelector((state: any) => state.campaign.campaigns);

  const [query, setQuery] = useState('');
  const [showDisabled, setShowDisabled] = useState(false);
  const [elementsOnPage] = useState(10);

  const handleChangeDisabledFilter = useCallback(() => {
    setShowDisabled(!showDisabled);
  }, [showDisabled]);

  const handleChangeFilterByName = useCallback((e) => {
    setQuery(e.currentTarget.value);
  }, []);

  useEffect(() => {
    getCampaigns(client, {
      offset: 0,
      limit: 100,
    })(dispatch);
  }, [client, dispatch]);

  useEffect(() => {
    getCampaigns(client, {
      limit: 100,
      offset: 0,
      showDisabled,
      q: query,
    })(dispatch);
  }, [showDisabled, query, elementsOnPage, client, dispatch]);

  return (
    <Page name="page--campaigns">
      <h1>{t('campaigns')}</h1>

      <div className="page-header">
        <div className="fluid-card-filters">
          <Input placeholder={t('campaign_filter')} onChange={handleChangeFilterByName} />
        </div>
        <div className="fluid-card-actions">
          <AddCampaign />
        </div>
        <div className="fluid-card-show-disabled">
          <Checkbox label={t('campaign_show_disabled')} onChange={handleChangeDisabledFilter} />
        </div>
      </div>

      <div className="page-content">
        <CampaignsList items={campaigns} emptyText={t('no_campaigns_found')} />
      </div>
    </Page>
  );
}
