const proxy = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    '/api/',
    proxy({
      target: 'https://console.mobimobi.tech',
      changeOrigin: true,
    })
  );
};
