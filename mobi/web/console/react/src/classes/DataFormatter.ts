import moment from 'moment';
import dayjs from 'dayjs';
import isoWeek from 'dayjs/plugin/isoWeek';

dayjs.extend(isoWeek);

export default class DataFormatter {
  static preformatDataTime = (data: Array<Array<any>>, timeRange: string, dateFormat?: string) => {
    let format = dateFormat || 'MM.DD';

    if ((timeRange.includes('h') || timeRange.includes('m')) && !dateFormat) {
      format = 'HH:mm';
    }

    return data.map((el) => [moment(el[0]).format(format), el[1]]);
  };

  static preformatDataTimeShift = (data: Array<Array<any>>, timeRange: string, showWeekday?: boolean) => {
    let format = 'MM.DD';

    if (timeRange.includes('h') || timeRange.includes('m')) {
      format = 'HH:mm';
    }

    const res = data.map((el, i) => {
      const arr = [...data[i]];
      const date = showWeekday ? DataFormatter.getWeekDay(arr.shift()) : moment(arr.shift()).format(format);

      return [date, ...arr];
    });

    return res;
  };

  static getWeekdayName = (index: number) => ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'][index];

  static getWeekDay = (date: string) => {
    return DataFormatter.getWeekdayName(dayjs(date).isoWeekday() - 1);
  };
}
