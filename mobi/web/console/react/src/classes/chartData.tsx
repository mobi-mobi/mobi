export default class ChartData {
  constructor(chartData: ChartData) {
    this.name = chartData.name;
    this.api = chartData.api;
    this.dataPath = chartData.dataPath;
    this.metaData = chartData.metaData || '';
  }

  name: string;
  api: string;
  dataPath: string;
  metaData?: string;
}
