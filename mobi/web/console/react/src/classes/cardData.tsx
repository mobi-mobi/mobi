export default class CardData {
  constructor(cardData: CardData) {
    this.name = cardData.name;
    this.api = cardData.api;
    this.dataPath = cardData.dataPath;
    this.metaData = cardData.metaData || '';
  }

  name: string;
  api: string;
  dataPath: string;
  metaData?: string;
}
