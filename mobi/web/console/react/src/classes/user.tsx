export default class User {
  constructor(user: User) {
    this.name = user.name;
    this.email = user.email;
    this.clients = user.clients;
  }

  name: string;
  email: string;
  clients: Array<string>;

  get displayName(): string {
    return this.name;
  }
}
