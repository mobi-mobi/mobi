/* eslint-disable */

export default class StringProcessor {
  static stripTags = (str: string): string | null =>
    str ? str.replace(/<\/?(?!span(?:\s|>))[a-z][^>]*(>|$)/gi, '').trim() : null;

  static urlReplacer = (name: string): string => {
    const url = name
      .toLowerCase()
      .trim()
      .replace(/[.,\/#!$?%\^&\*;:{}=\_`~()]/g, '')
      .replace(/\s{2,}/g, '')
      .replace(/ /g, '-');

    const stripped = StringProcessor.stripTags(url);

    return stripped ? stripped.trim() : url;
  };
}
