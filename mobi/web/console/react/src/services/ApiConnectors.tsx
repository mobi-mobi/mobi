import get from 'lodash/get';
import config from '../config';
import Api from './Api';

const api = new Api(config.global.REACT_APP_API_URL);

export const getDataFromApi = (apiPath: string, timeRange: string, client: string) => {
  const apiPathReady = apiPath.replace('<client>', client).replace('<timeRange>', timeRange);

  return api
    .get(apiPathReady, {})
    .then((response: any) => {
      const data = get(response, 'data');
      return data;
    })
    .catch((error: any) => {
      console.log(error);
      return NaN;
    });
};


// <ChartCard
// <InfoCard
