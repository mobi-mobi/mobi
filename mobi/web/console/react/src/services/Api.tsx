export default class Api {
  constructor(url: any) {
    this.url = url;
    this.token = 'ljGlkgLLYlajSjZOfW6bJooYIoYOA91g';
  }

  url: string;
  token: string;

  _getToken() {
    return this.token;
  }

  _request = async (path = '', params: any = {}) => {
    params.headers = new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...params.headers,
    });

    params.credentials = 'include';

    try {
      const response = await fetch(this.url + path, params);
      const json = await response.json();

      // if (response.status > 400 && response.status < 500) {
      //   window.location.href = 'https://console.mobimobi.tech/login';
      // }

      if (response.status < 200 || response.status > 300) {
        const error = new Error(json?.error?.description);

        Object.assign(error, json);

        throw error;
      }

      return json;
    } catch (error) {
      console.log(error);

      throw error;
    }
  };

  get(path: string, params: any) {
    return this._request(path, {
      ...params,
      method: 'GET',
    });
  }

  post(path: string, params: any) {
    return this._request(path, {
      ...params,
      method: 'POST',
    });
  }

  put(path: string, params: any) {
    return this._request(path, {
      ...params,
      method: 'PUT',
    });
  }

  delete(path: string, params: any) {
    return this._request(path, {
      ...params,
      method: 'DELETE',
    });
  }
}
