import argparse
import pickle
import requests
import werkzeug

from collections import defaultdict
from datetime import datetime, timedelta
from flask import g, redirect, render_template, request, url_for
from flask_cors import CORS
import json
from hashlib import sha1
from math import floor
from random import choices
from string import ascii_lowercase, digits
from typing import List, Optional, Tuple

from mobi.api.helpers.zone import calc_active_zone_campaign, calc_closest_zone_campaign, calc_next_zone_campaign,\
    validate_campaign_json, validate_zone_json
from mobi.api.parsers import uri
from mobi.api.parsers.model import parse_audience, parse_audiences_union, parse_audience_split_tree
from mobi.config.system.web._base import WebBaseConfig
from mobi.config.system.web.console import get_console_web_config
from mobi.core.models import AudienceSplitTree, ApiToken, AttributionType, EventType, RecommendationType, TokenType,\
    TokenPermission, TokenHashAlgo
from mobi.core.models.api_token import hash_token
from mobi.core.models.zone import Campaign, Zone
from mobi.core.population import Population
from mobi.dao import MobiDaoException
from mobi.dao.cache import cache_key
from mobi.dao.enums import MobiObjectCreationStatus
from mobi.dao.user_settings import ReportSubscription
from mobi.web import MobiWebApplication, MobiWebException, random_string, special_chars


class ConsoleApplication(MobiWebApplication):
    def __init__(self, application: str, host: str, port: int):
        super().__init__(application, host, port)
        cors_origins = [
            'http://console.mobimobi.tech',
            'https://console.mobimobi.tech',
            'localhost:3000',
            'http://localhost',
            'https://localhost',
            'http://localhost:3000',
            'https://localhost:3000',
        ]
        CORS(self.app, supports_credentials=True, origins=cors_origins)

        # System methods

        self.app.add_url_rule("/unsubscribe", "unsubscribe", self.unsubscribe_user, methods=["GET"])

        # Pages

        self.app.add_url_rule("/api/<client>/zones/full", "legacy_get_zones_full", self.get_zones_page, methods=["GET"])
        self.app.add_url_rule("/api/<client>/pages/zones", "get_zones_page", self.get_zones_page, methods=["GET"])

        self.app.add_url_rule("/api/<client>/campaigns/full", "legacy_get_campaigns_full", self.get_campaigns_page,
                              methods=["GET"])
        self.app.add_url_rule("/api/<client>/pages/campaigns", "get_campaigns_page", self.get_campaigns_page,
                              methods=["GET"])
        self.app.add_url_rule("/api/<client>/pages/catalog/products", "get_catalog_products_page",
                              self.get_catalog_products_page, methods=["GET"])
        self.app.add_url_rule("/api/<client>/pages/catalog/brands", "get_catalog_brands_page",
                              self.get_catalog_brands_page, methods=["GET"])

        self.app.add_url_rule("/api/<client>/catalog/products/full", "legacy_products_list_full",
                              self.get_catalog_products_page, methods=["GET"])
        # TODO: Delete the next line
        self.app.add_url_rule("/api/<client>/pages/catalog", "get_catalog_page_legacy", self.get_catalog_products_page,
                              methods=["GET"])

        # Main page

        self.app.add_url_rule("/api/<client>/pages/main/real-time", "main_page_real_time", self.get_main_page_real_time)
        self.app.add_url_rule("/api/<client>/pages/main/audience-info", "main_page_audience_info",
                              self.get_main_page_audience_info)
        self.app.add_url_rule("/api/<client>/pages/main/traffic-info", "main_page_traffic_info",
                              self.get_main_page_traffic_info)
        self.app.add_url_rule("/api/<client>/pages/main/traffic-source", "main_page_traffic_source",
                              self.get_main_page_traffic_source)
        self.app.add_url_rule("/api/<client>/pages/main/conversion-stats",
                              "main_page_conversion_stats", self.get_main_page_conversion_stats)
        self.app.add_url_rule("/api/<client>/pages/main/recommendations-conversion",
                              "main_page_recommendations_conversion", self.get_main_page_recommendations_conversion)
        self.app.add_url_rule("/api/<client>/pages/main/incremental-stats", "main_page_incremental_stats",
                              self.get_main_page_incremental_stats)
        self.app.add_url_rule("/api/<client>/pages/main/integration-stats", "main_page_integration_stats",
                              self.get_main_page_integration_stats)

        # Audiences pages
        self.app.add_url_rule("/api/<client>/pages/audiences", "get_audiences_page", self.get_audiences_page)
        self.app.add_url_rule("/api/<client>/pages/split_trees", "get_split_trees_page",
                              self.get_split_trees_page)

        # User helpers
        
        self.app.add_url_rule("/api/user/info", "user_info", self.user_info, methods=["GET"])

        # User Settings Pages

        self.app.add_url_rule("/api/user/settings/account", "get_update_account_details",
                              self.get_update_account_details, methods=["GET", "PUT"])
        self.app.add_url_rule("/api/user/settings/password", "update_password", self.update_password,
                              methods=["PUT"])

        self.app.add_url_rule("/api/user/settings/subscriptions", "get_delete_add_subscriptions",
                              self.get_delete_add_subscriptions, methods=["GET", "POST", "DELETE"])

        self.app.add_url_rule("/api/user/settings/tokens", "get_delete_add_tokens", self.get_delete_add_tokens,
                              methods=["GET", "POST", "DELETE"])

        # Helpers

        self.app.add_url_rule("/status", "status", self.status, methods=["GET"])

        # Metrics

        self.app.add_url_rule("/api/<client>/catalog/events/count/last/<time_interval>", "product_events_num",
                              self.product_events_num, methods=["GET"])
        self.app.add_url_rule("/api/<client>/user/events/count/last/<time_interval>", "user_events_num",
                              self.user_events_num, methods=["GET"])
        self.app.add_url_rule("/api/<client>/recommendation/events/count/last/<time_interval>",
                              "recommendation_events_num", self.recommendation_events_num, methods=["GET"])

        self.app.add_url_rule("/api/<client>/user/unique/metric/from/<time_interval>", "unique_users_metric",
                              self.unique_users_metric, methods=["GET"])

        # Zones management

        self.app.add_url_rule("/api/<client>/zone", "create_update_zone", self.create_update_zone, methods=["POST"])
        self.app.add_url_rule("/api/<client>/zone/<zone_id>", "get_delete_zone", self.get_delete_zone,
                              methods=["GET", "DELETE"])
        self.app.add_url_rule("/api/<client>/zone/<zone_id>/metrics", "get_zone_metrics", self.get_zone_metrics,
                              methods=["GET"])
        self.app.add_url_rule("/api/<client>/zones", "get_zones", self.get_zones, methods=["GET"])
        self.app.add_url_rule("/api/<client>/zones/num", "get_zones_num", self.get_zones_num, methods=["GET"])
        self.app.add_url_rule("/api/<client>/zone/<zone_id>/campaign/closest", "get_closest_zone_campaign",
                              self.get_closest_zone_campaign, methods=["GET"])
        self.app.add_url_rule("/api/<client>/zone/<zone_id>/apply_campaign/<campaign_id>",
                              "get_zone_applied_settings", self.get_zone_applied_settings, methods=["GET"])
        self.app.add_url_rule("/api/<client>/zone/<zone_id>/stats", "get_zone_stats", self.get_zone_stats,
                              methods=["GET"])

        # Campaigns management

        self.app.add_url_rule("/api/<client>/campaign", "create_update_campaigns", self.create_update_campaigns,
                              methods=["POST"])
        self.app.add_url_rule("/api/<client>/campaign/<campaign_id>", "get_delete_campaign",
                              self.get_delete_campaign, methods=["GET", "DELETE"])
        self.app.add_url_rule("/api/<client>/campaigns", "get_campaigns", self.get_campaigns, methods=["GET"])
        self.app.add_url_rule("/api/<client>/campaign/<campaign_id>/metrics", "get_campaign_metrics",
                              self.get_campaign_metrics, methods=["GET"])

        # Zone-campaigns management

        self.app.add_url_rule("/api/<client>/zone/<zone_id>/campaigns", "get_zone_campaigns", self.get_zone_campaigns,
                              methods=["GET"])
        self.app.add_url_rule("/api/<client>/zone/<zone_id>/add_campaign/<campaign_id>", "attach_campaign_to_zone",
                              self.attach_campaign_to_zone, methods=["POST"])
        self.app.add_url_rule("/api/<client>/zone/<zone_id>/remove_campaign/<campaign_id>", "remove_campaign_from_zone",
                              self.remove_campaign_from_zone, methods=["DELETE"])

        # Catalog management

        self.app.add_url_rule("/api/<client>/catalog/info", "catalog_info", self.catalog_info, methods=["GET"])

        self.app.add_url_rule("/api/<client>/catalog/products", "products_list",
                              self.products_list, methods=["GET"])
        self.app.add_url_rule("/api/<client>/catalog/product/<product_id>", "product_info",
                              self.product_info, methods=["GET", "PUT", "DELETE"])

        self.app.add_url_rule("/api/<client>/catalog/brands", "catalog_brands", self.catalog_brands, methods=["GET"])
        self.app.add_url_rule("/api/<client>/catalog/categories", "catalog_categories",
                              self.catalog_categories, methods=["GET"])

        # Product metrics

        self.app.add_url_rule("/api/<client>/product/<product_id>/metrics/events", "product_metrics_events",
                              self.get_product_metrics_events, methods=["GET"])

        # AB-Test metrics

        self.app.add_url_rule("/api/<client>/abtest/metric/<metric_name>", "get_abtest_metrics",
                              self.get_abtest_metrics, methods=["GET"])

        # Audiences

        self.app.add_url_rule("/api/<client>/audiences", "get_create_update_audience", self.get_create_update_audience,
                              methods=["GET", "POST", "PUT", "DELETE"])
        self.app.add_url_rule("/api/<client>/audiences/all", "get_all_audiences", self.get_all_audiences)

        self.app.add_url_rule("/api/<client>/audiences_unions", "get_create_update_audiences_union",
                              self.get_create_update_audiences_union, methods=["GET", "POST", "PUT", "DELETE"])
        self.app.add_url_rule("/api/<client>/audiences_unions/all", "get_all_audiences_unions",
                              self.get_all_audiences_unions)

        self.app.add_url_rule("/api/<client>/audience_split_trees", "get_create_update_audience_split_tree",
                              self.get_create_update_audience_split_tree, methods=["GET", "POST", "PUT", "DELETE"])
        self.app.add_url_rule("/api/<client>/audience_split_trees/all", "get_all_audience_spit_tree",
                              self.get_all_audience_spit_tree)

        # Test search & recommendations

        self.app.add_url_rule("/api/<client>/test/recommendation", "test_recommendation", self.get_test_recommendation,
                              methods=["GET"])
        self.app.add_url_rule("/api/<client>/test/search", "test_search", self.get_test_search, methods=["GET"])

    @property
    def config(self) -> WebBaseConfig:
        return get_console_web_config()

    def status(self):
        return self.format_result()

    @staticmethod
    def _parse_time_interval(s: str) -> int:
        if not s or type(s) != str or len(s) < 2 or s[-1] not in ('m', 'h', 'd'):
            raise MobiWebException(f"Bad time interval value: {s}")
        int_val = s[0:-1]
        try:
            int_val = int(int_val)
        except ValueError:
            raise MobiWebException(f"Bad time interval value: {s}")
        result = int(floor(datetime.utcnow().timestamp()))
        if s[-1] == "m":
            result -= 60 * int_val
        if s[-1] == "h":
            result -= 60 * 60 * int_val
        if s[-1] == "d":
            result -= 24 * 60 * 60 * int_val
        return result

    def assert_has_access_to_client(self, client: str):
        token = self.token

        if client and token:
            user = self.auth_dao.read_user_by_token(token)
            if user.get("clients") and client in user["clients"]:
                return

        raise MobiWebException(f"User does not have an access to the client {special_chars(client)}")

    def index(self):
        return render_template("index.html")

    def login(self):
        url = request.args.get("redirect_url", url_for("index"))

        if self.is_user_authenticated():
            return redirect(url, 302)

        msg = request.args.get("msg")
        if msg == "wrong_credentials":
            g.error_message = "Wrong user credentials. Either we don't know you or you use wrong email and/or password"
        if msg == "not_authenticated":
            g.error_message = "Please, login to start using MobiMobi.Tech console"
        if msg == "byebye":
            g.error_message = "Bye-bye :)"
        return render_template("login.html")

    def render_error_page(self, exception: MobiWebException):
        if exception.code == 404:
            g.error_message = "Unknown URL"
        elif exception.code // 100 == 5:
            g.error_message = f"Internal error: {special_chars(str(exception))}"
        else:
            g.error_message = f"Error: {special_chars(str(exception))}"
        return render_template("error.html")

    def user_info(self):
        user = self.auth_dao.read_user_by_token(self.token)

        if not user:
            raise MobiWebException("User not found")

        result = {
            "name": user.get("name"),
            "email": user.get("email"),
            "clients": user.get("clients")
        }
        return self.format_result(result)

    # System methods
    def unsubscribe_user(self):
        user = uri.read_str("user", required=False, default=None)
        subscription_name = uri.read_str("subscription_name", required=False, default="")
        subscription = uri.read_str("subscription", required=False, default=None)
        subscription_type = uri.read_str("subscription_type", required=False, default=None)
        client = uri.read_str("client", required=False, default=None)
        checksum = uri.read_str("checksum", required=False, default=None)

        if None not in [user, subscription, subscription_type, client, checksum]:
            checksum_str = "mobimobi" \
                           + "<user>" + user + "</user>" \
                           + "<subscription>" + subscription + "</subscription>" \
                           + "<subscription_type>" + subscription_type + "</subscription_type>" \
                           + "<subscription_name>" + subscription_name + "</subscription_name>" \
                           + "<client>" + client + "</client>" \
                           + "tech"

            try:
                checksum_original = sha1(bytes(checksum_str, "utf-8")).hexdigest()
            except:
                checksum_original = ""
            if checksum_original != checksum:
                g.error_message = "\"Unsubscribe\" link is not valid or expired"
                return render_template("error.html")
            else:
                if subscription == "report":
                    report_subscription = None

                    try:
                        report_subscription = ReportSubscription(subscription_type)
                    except ValueError:
                        pass

                    if report_subscription is not None:
                        self.user_settings_dao.remove_report_subscription(user=user, client=client,
                                                                          report_subscription=report_subscription)

        g.subscription_name = special_chars(subscription_name or "")
        return render_template("unsubscribe.html")

    # User settings API

    def get_update_account_details(self):
        user = self.auth_dao.read_user_by_token(self.token)

        if request.method == "GET":
            result = {
                "name": user["name"],
                "email": user["email"]
            }
            return self.format_result(result)
        if request.method == "PUT":
            try:
                data = json.loads(request.data)
            except:
                raise MobiWebException(description="Can not parse post data")

            if not isinstance(data, dict):
                raise MobiWebException(description="Can not parse post data")

            name = data.get("name")

            if name is None or not len(name.strip()) or len(name.strip()) > 100:
                raise MobiWebException(description="Name should not be empty and should contain no more than "
                                                   "100 symbols")

            try:
                self.auth_dao.update_user_name(email=user["email"], name=name.strip())
            except MobiDaoException:
                raise MobiWebException(description="Use does not exist")
            return self.format_result()

    def update_password(self):
        user = self.auth_dao.read_user_by_token(self.token)

        try:
            data = json.loads(request.data)
        except:
            raise MobiWebException(description="Can not parse post data")

        if not isinstance(data, dict):
            raise MobiWebException(description="Can not parse post data")

        new_password = data.get("new_password")
        new_password_repeated = data.get("new_password_repeated")

        if not new_password:
            raise MobiWebException(description="New password is not set")

        if new_password != new_password_repeated:
            raise MobiWebException(description="New passwords should match")

        if len(new_password) < 8:
            raise MobiWebException(description="New password should be at least 8 symbols long")

        if len(new_password) > 100:
            raise MobiWebException(description="New password should be at most 100 symbols long")

        self.auth_dao.update_user_password(user["email"], new_password)

        return self.format_result()

    def get_delete_add_subscriptions(self):
        user = self.auth_dao.read_user_by_token(self.token)
        clients = user["clients"]

        if request.method == "GET":
            result = {}
            current_subscriptions = {}
            can_subscribe_to = {}

            for client in clients:
                current_subscriptions[client] = []
                can_subscribe_to[client] = []
                subscribed_to = self.user_settings_dao.read_report_subscriptions_list(user["email"], client)
                for report in ReportSubscription:
                    title = report.name.lower() + " report"
                    subscription_parameters = {
                        "type": "report",
                        "client": client,
                        "report_subscription": report.name.lower()
                    }
                    if report in subscribed_to:
                        current_subscriptions[client].append({
                            "title": title,
                            "unsubscribe_parameters": subscription_parameters
                        })
                    else:
                        can_subscribe_to[client].append({
                            "title": title,
                            "subscribe_parameters": subscription_parameters
                        })

            result["current_subscriptions"] = current_subscriptions
            result["can_subscribe_to"] = can_subscribe_to

            return self.format_result(result)

        if request.method == "DELETE":
            subscription_type = uri.read_str("type", required=True, min_length=1)
            client = uri.read_str("client", required=True, min_length=1)
            self.assert_has_access_to_client(client)

            if subscription_type == "report":
                report_subscription_str = uri.read_str("report_subscription", required=True)
                try:
                    report_subscription = ReportSubscription[report_subscription_str.upper()]
                except KeyError:
                    raise MobiWebException(f"Unknown report type: {report_subscription_str}")
                self.user_settings_dao.remove_report_subscription(user["email"], client, report_subscription)
            else:
                raise MobiWebException(f"Unknown subscription type: {subscription_type}")

            return self.format_result()

        if request.method == "POST":
            subscription_type = uri.read_str("type", required=True, min_length=1)
            client = uri.read_str("client", required=True, min_length=1)
            self.assert_has_access_to_client(client)

            if subscription_type == "report":
                report_subscription_str = uri.read_str("report_subscription", required=True)
                try:
                    report_subscription = ReportSubscription[report_subscription_str.upper()]
                except KeyError:
                    raise MobiWebException(f"Unknown report type: {report_subscription_str}")

                self.user_settings_dao.add_report_subscription(user["email"], client, report_subscription)
            else:
                raise MobiWebException(f"Unknown subscription type: {subscription_type}")

            return self.format_result()

    def get_delete_add_tokens(self):
        user = self.auth_dao.read_user_by_token(self.token)
        clients = user["clients"]

        if request.method == "GET":
            result = {}
            for client in clients:
                result[client] = []
                for token in self.settings_dao.read_tokens(client):
                    result[client].append({
                        "token_hash": token.token_hash,
                        "first_letters": token.first_letters,
                        "token_type": token.token_type.name.lower(),
                        "expiration_date": token.expiration_date.strftime("%Y-%m-%d %H:%M:%S"),
                        "permissions": [p.name.lower() for p in token.permissions],
                        "description": "" if token.description is None else token.description
                    })
            return self.format_result(result)
        if request.method == "DELETE":
            client = uri.read_str("client", required=True, min_length=1)
            self.assert_has_access_to_client(client)
            token_hash = uri.read_str("token_hash", required=True, min_length=1)
            self.settings_dao.delete_token(client, token_hash)
            return self.format_result()
        if request.method == "POST":
            client = uri.read_str("client", required=True, min_length=1)
            self.assert_has_access_to_client(client)

            try:
                data = json.loads(request.data)
            except:
                raise MobiWebException(description="Can not parse post data")

            if not isinstance(data, dict):
                raise MobiWebException(description="Can not parse post data")

            try:
                token_type = TokenType.parse(data.get("token_type"))
            except Exception:
                raise MobiWebException(description="Can not parse token type") from None

            try:
                token_permissions = [TokenPermission.parse(p) for p in data.get("token_permissions")]
            except Exception:
                raise MobiWebException(description="Can not parse token permissions") from None

            token_description = data.get("description")
            if token_description is not None:
                if not isinstance(token_description, str):
                    raise MobiWebException("Token description should be a string")
                if len(token_description) > 200:
                    raise MobiWebException(description="Max token description length is 200 symbols")

            retries = 3
            while retries > 0:
                retries -= 1
                token_str = "".join(choices(ascii_lowercase + digits, k=32))
                token = ApiToken(
                    client=client,
                    token_hash=hash_token(token_str, TokenHashAlgo.SHA256),
                    token_hash_algo=TokenHashAlgo.SHA256,
                    first_letters=token_str[0:4],
                    token_type=token_type,
                    expiration_date=datetime.utcnow() + timedelta(days=367),
                    permissions=token_permissions,
                    description=token_description
                )
                try:
                    # Just in case if we were lucky to generate exactly same cache (which is nearly impossible)
                    self.settings_dao.create_api_token(token)
                    return self.format_result({
                        "token_str": token_str
                    })
                except Exception:
                    pass

            raise MobiWebException(description="Can not create token. Something has happen on the backend side")

    # Main Page - real-time

    def get_main_page_real_time(self, client):
        self.assert_has_access_to_client(client)

        most_popular_products = self.cache_dao.get(cache_key("most_popular", client=client, interval="8h"), retries=3)
        if most_popular_products:
            most_popular_products = pickle.loads(most_popular_products)
            most_popular_products = most_popular_products[:20]
            products = self.catalog_dao.read_products(client, [product_id for product_id, _ in most_popular_products])
            products = {product.product_id: product for product in products}
            most_popular_products = [{"product": products[product_id].as_json_dict(), "count": count}
                                     for product_id, count in most_popular_products if product_id in products]
        else:
            most_popular_products = []

        most_popular_brands = self.cache_dao.get(cache_key("most_popular_brands", client=client, interval="8h"),
                                                 retries=3)
        if most_popular_brands:
            most_popular_brands = pickle.loads(most_popular_brands)
            most_popular_brands = most_popular_brands[:20]
        else:
            most_popular_brands = []

        most_popular_categories = self.cache_dao.get(cache_key("most_popular_categories", client=client, interval="8h"),
                                                     retries=3)
        if most_popular_categories:
            most_popular_categories = pickle.loads(most_popular_categories)
            most_popular_categories = most_popular_categories[:20]
        else:
            most_popular_categories = []

        result = {
            "active_users_num": self.cache_dao.get_int(cache_key("real_time_users_num", client=client),
                                                       default=0, retries=3),
            "most_popular_products": most_popular_products,
            "most_popular_brands": most_popular_brands,
            "most_popular_categories": most_popular_categories
        }

        return self.format_result(result)

    # Main Page - audience

    def get_main_page_audience_info(self, client):
        self.assert_has_access_to_client(client)

        now = datetime.utcnow()
        skip_last = timedelta(minutes=15)
        metrics_from = now - timedelta(days=1) - skip_last

        new_users_24h = int(self.metrics_dao.get_metrics_last(client, "new_users", None,
                                                              timestamp_from=int(metrics_from.timestamp()),
                                                              bases=[60, 60 * 60]))
        new_users_24h -= int(self.metrics_dao.get_metrics_last(client, "new_users", None,
                                                               timestamp_from=int((now - skip_last).timestamp()),
                                                               bases=[60, 60 * 60]))
        all_users_24h = int(self.metrics_dao.get_metrics_last(client, "unique_users", None,
                                                              timestamp_from=int(metrics_from.timestamp()),
                                                              bases=[60, 60 * 60]))
        all_users_24h -= int(self.metrics_dao.get_metrics_last(client, "unique_users", None,
                                                               timestamp_from=int((now-skip_last).timestamp()),
                                                               bases=[60, 60 * 60]))

        metrics_base = 60 * 60
        db_from_timestamp = metrics_base * (int(metrics_from.timestamp()) // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(now.timestamp()) // metrics_base) + metrics_base

        def sub_metrics(m1, m2):
            result = []
            for v1, v2 in zip(m1, m2):
                date, i1 = v1
                _, i2 = v2
                result.append((date, max(0, i1 - i2)))
            return result

        new_users_metric = self.fetch_metric(client, "new_users", None, db_from_timestamp, db_until_timestamp, 60 * 60)
        unique_users_metric = self.fetch_metric(client, "unique_users", None, db_from_timestamp, db_until_timestamp,
                                                60 * 60)

        metrics_base = 24 * 60 * 60
        metrics_from = now - timedelta(days=4 * 7)
        db_from_timestamp = metrics_base * (int(metrics_from.timestamp()) // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(now.timestamp()) // metrics_base) + metrics_base

        w_2_w_users = self.fetch_metric(client, "unique_users", None, db_from_timestamp, db_until_timestamp,
                                        24 * 60 * 60)
        week_to_week = []
        for _ in range(4):
            week_to_week.append(w_2_w_users[-7:])
            w_2_w_users = w_2_w_users[:-7]

        metrics_base = 24 * 60 * 60
        metrics_from = now - timedelta(days=15)
        db_from_timestamp = metrics_base * (int(metrics_from.timestamp()) // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(now.timestamp()) // metrics_base) + metrics_base

        user_churns_metrics = dict()
        user_churns_metric_names = ("churned_users_w7d_a7d_abs", "churned_users_w7d_a7d_rel",
                                    "churned_users_w7d_a35d_abs", "churned_users_w7d_a35d_rel")

        for user_churns_metric_name in user_churns_metric_names:
            user_churns_metrics[user_churns_metric_name] = self.fetch_metric(
                client, user_churns_metric_name, None, db_from_timestamp, db_until_timestamp, 24 * 60 * 60
            )

        result = {
            "new_users_share": 0.0 if not all_users_24h else new_users_24h / all_users_24h,
            "new_vs_returning": {
                "new": new_users_metric,
                "returning": sub_metrics(unique_users_metric, new_users_metric)
            },
            "active_week_to_week": week_to_week,
            "user_churns": user_churns_metrics,
        }

        return self.format_result(result)

    def get_main_page_traffic_info(self, client):
        self.assert_has_access_to_client(client)

        result = {
            "events": {},
            "attributed_events": {}
        }

        now = datetime.utcnow()
        metrics_from = now - timedelta(days=15)
        metrics_base = 24 * 60 * 60
        db_from_timestamp = metrics_base * (int(metrics_from.timestamp()) // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(now.timestamp()) // metrics_base) + metrics_base

        for event_type in (EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST, EventType.PRODUCT_TO_BASKET,
                           EventType.PRODUCT_SALE):
            result["events"][event_type.name.lower()] = self.sum_fetched_metrics(
                [
                    self.fetch_metric(
                        client,
                        "events__total",
                        tags={"type": event_type.name.lower()},
                        from_ts=db_from_timestamp,
                        until_ts=db_until_timestamp,
                        base_int=24 * 60 * 60
                    ),
                    self.fetch_metric(
                        client,
                        "events_created__total",
                        tags={"event_type": event_type.name.lower()},
                        from_ts=db_from_timestamp,
                        until_ts=db_until_timestamp,
                        base_int=24 * 60 * 60
                    )
                ]
            )

            result["attributed_events"][event_type.name.lower()] = self.fetch_metric(
                client,
                "attributed_events__total",
                tags={"event_type": event_type.name.lower()},
                from_ts=db_from_timestamp,
                until_ts=db_until_timestamp,
                base_int=24 * 60 * 60
            )

        return self.format_result(result)

    def get_main_page_traffic_source(self, client):
        self.assert_has_access_to_client(client)
        exclude_last = timedelta(minutes=15)

        result = {}

        now = datetime.utcnow()
        metrics_from = now - timedelta(days=1) - exclude_last
        metrics_base = 60 * 60
        db_from_timestamp = metrics_base * (int(metrics_from.timestamp()) // metrics_base) + metrics_base

        for event_type in (EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST, EventType.PRODUCT_TO_BASKET,
                           EventType.PRODUCT_SALE):
            result[event_type.name.lower()] = dict()

            total_events = self.metrics_dao.get_metrics_last(client, "events__total", {"type": event_type.name.lower()},
                                                             db_from_timestamp)
            total_events += self.metrics_dao.get_metrics_last(client, "events_created__total",
                                                              {"event_type": event_type.name.lower()},
                                                              db_from_timestamp)
            total_events -= self.metrics_dao.get_metrics_last(client, "events__total",
                                                              {"type": event_type.name.lower()},
                                                              int((now - exclude_last).timestamp()))
            total_events -= self.metrics_dao.get_metrics_last(client, "events_created__total",
                                                              {"event_type": event_type.name.lower()},
                                                              int((now - exclude_last).timestamp()))

            for reco_type in (RecommendationType.SIMILAR_PRODUCTS, RecommendationType.PERSONAL_RECOMMENDATIONS,
                              RecommendationType.BASKET_RECOMMENDATIONS):
                tags = {
                    "event_type": event_type.name.lower(),
                    "attribution_type": AttributionType.EMPIRICAL.name.lower(),
                    "recommendation_type": reco_type.name.lower()
                }
                matched_events = self.metrics_dao.get_metrics_last(client, "attributed_events__total", tags,
                                                                   db_from_timestamp)
                matched_events -= self.metrics_dao.get_metrics_last(client, "attributed_events__total", tags,
                                                                    int((now - exclude_last).timestamp()))
                result[event_type.name.lower()][reco_type.name.lower()] = matched_events
                total_events -= matched_events
            else:
                result[event_type.name.lower()]["organic"] = max(0, total_events)

        return self.format_result(result)

    def get_main_page_conversion_stats(self, client):
        self.assert_has_access_to_client(client)

        now = datetime.utcnow()
        exclude_last = timedelta(minutes=15)
        metrics_from = now - timedelta(days=1) - exclude_last
        metrics_base = 60 * 60
        db_from_timestamp = metrics_base * (int(metrics_from.timestamp()) // metrics_base) + metrics_base

        result = dict()

        event_types_to_fetch = (
            EventType.PRODUCT_VIEW,
            EventType.PRODUCT_TO_WISHLIST,
            EventType.PRODUCT_TO_BASKET,
            EventType.PRODUCT_SALE
        )

        # Computing organic values

        result["organic"] = dict()

        for event_type in event_types_to_fetch:
            events_num = int(self.metrics_dao.get_metrics_last(
                client, "events__total", {"type": event_type.name.lower()}, db_from_timestamp
            ))
            events_num += int(self.metrics_dao.get_metrics_last(
                client, "events_created__total", {"event_type": event_type.name.lower()}, db_from_timestamp
            ))
            events_num -= int(self.metrics_dao.get_metrics_last(
                client, "events__total",
                {"type": event_type.name.lower()}, int((now - exclude_last).timestamp())
            ))
            events_num -= int(self.metrics_dao.get_metrics_last(
                client, "events_created__total",
                {"event_type": event_type.name.lower()}, int((now - exclude_last).timestamp())
            ))

            result["organic"][event_type.name.lower()] = events_num

        # Computing recommendations values

        recommendation_types_to_fetch = (
            RecommendationType.SIMILAR_PRODUCTS,
            RecommendationType.PERSONAL_RECOMMENDATIONS,
            RecommendationType.BASKET_RECOMMENDATIONS
        )

        for recommendation_type in recommendation_types_to_fetch:
            result[recommendation_type.name.lower()] = dict()

            for event_type in event_types_to_fetch:
                attributed_recos_tags = {
                    "attribution_type": AttributionType.EMPIRICAL.name.lower(),
                    "event_type": event_type.name.lower(),
                    "recommendation_type": recommendation_type.name.lower()
                }

                events_num = int(self.metrics_dao.get_metrics_last(
                    client, "attributed_events__total", attributed_recos_tags, db_from_timestamp, bases=[60, 60 * 60]
                ))
                events_num -= int(self.metrics_dao.get_metrics_last(
                    client, "attributed_events__total", attributed_recos_tags, int((now - exclude_last).timestamp()),
                    bases=[60, 60 * 60]
                ))

                result["organic"][event_type.name.lower()] -= events_num
                result[recommendation_type.name.lower()][event_type.name.lower()] = events_num

        return self.format_result(result)

    def get_main_page_recommendations_conversion(self, client):
        self.assert_has_access_to_client(client)

        now = datetime.utcnow()
        exclude_last = timedelta(minutes=15)
        metrics_from = now - timedelta(days=1) - exclude_last
        metrics_base = 60 * 60
        db_from_timestamp = metrics_base * (int(metrics_from.timestamp()) // metrics_base) + metrics_base

        result = {}

        # Computing organic values

        total_displays = int(self.metrics_dao.get_metrics_last(
            client, "events__total", {"type": EventType.PRODUCT_VIEW.name.lower()}, db_from_timestamp
        ))
        total_displays -= int(self.metrics_dao.get_metrics_last(
            client, "events__total",
            {"type": EventType.PRODUCT_VIEW.name.lower()}, int((now - exclude_last).timestamp())
        ))
        total_to_basket = int(self.metrics_dao.get_metrics_last(
            client, "events__total", {"type": EventType.PRODUCT_TO_BASKET.name.lower()}, db_from_timestamp
        ))
        total_to_basket -= int(self.metrics_dao.get_metrics_last(
            client, "events__total",
            {"type": EventType.PRODUCT_TO_BASKET.name.lower()}, int((now - exclude_last).timestamp())
        ))

        # Computing recommendations conversion

        for reco_type in (RecommendationType.SIMILAR_PRODUCTS, RecommendationType.PERSONAL_RECOMMENDATIONS,
                          RecommendationType.BASKET_RECOMMENDATIONS):
            # Displays

            attributed_recos_tags = {
                "attribution_type": AttributionType.EMPIRICAL.name.lower(),
                "event_type": EventType.PRODUCT_VIEW.name.lower(),
                "recommendation_type": reco_type.name.lower()
            }
            reco_displays = int(self.metrics_dao.get_metrics_last(
                client, "attributed_events__total", attributed_recos_tags, db_from_timestamp, bases=[60, 60 * 60]
            ))
            reco_displays -= int(self.metrics_dao.get_metrics_last(
                client, "attributed_events__total", attributed_recos_tags, int((now - exclude_last).timestamp()),
                bases=[60, 60 * 60]
            ))

            # To basket

            attributed_recos_tags = {
                "attribution_type": AttributionType.EMPIRICAL.name.lower(),
                "event_type": EventType.PRODUCT_TO_BASKET.name.lower(),
                "recommendation_type": reco_type.name.lower()
            }
            reco_to_basket = int(self.metrics_dao.get_metrics_last(
                client, "attributed_events__total", attributed_recos_tags, db_from_timestamp, bases=[60, 60 * 60]
            ))
            reco_to_basket -= int(self.metrics_dao.get_metrics_last(
                client, "attributed_events__total", attributed_recos_tags, int((now - exclude_last).timestamp()),
                bases=[60, 60 * 60]
            ))

            result[reco_type.name.lower()] = 0.0 if not reco_displays else reco_to_basket / reco_displays

            total_displays -= reco_displays
            total_to_basket -= reco_to_basket

        result["organic"] = 0.0 if not total_displays else total_to_basket / total_displays

        return self.format_result(result)

    def get_main_page_incremental_stats(self, client):
        self.assert_has_access_to_client(client)

        tags = {
            "popA": "target_and_test",
            "popB": Population.REFERENCE.name.lower(),
            "bound": "rel_diff",
            "period": "28d"
        }

        sources = (
            (EventType.PRODUCT_VIEW, "views_num"),
            (EventType.PRODUCT_TO_WISHLIST, "to_wishlist_num"),
            (EventType.PRODUCT_TO_BASKET, "to_basket_num"),
            (EventType.PRODUCT_SALE, "purchases_num")
        )

        result = {}

        for event_type, metric_name in sources:
            tags["metric"] = metric_name
            value = self.metrics_dao.get_latest_point_value(client, "abtest", tags, 24 * 60 * 60) or 0.0
            result[event_type.name.lower()] = value

        return self.format_result(result)

    def get_main_page_integration_stats(self, client):
        self.assert_has_access_to_client(client)

        now = datetime.utcnow()
        metrics_from = now - timedelta(days=1)
        metrics_base = 60 * 60
        db_from_timestamp = metrics_base * (int(metrics_from.timestamp()) // metrics_base) + metrics_base

        effective_statuses = (
            MobiObjectCreationStatus.CREATED,
            MobiObjectCreationStatus.DELETED,
            MobiObjectCreationStatus.UPDATED
        )

        updates = 0

        for status in effective_statuses:
            updates += int(self.metrics_dao.get_metrics_last(
                client, "products", {"status": status.name.lower()}, db_from_timestamp
            ))

        recommendations = 0

        recommendation_types = (
            RecommendationType.SIMILAR_PRODUCTS,
            RecommendationType.PERSONAL_RECOMMENDATIONS,
            RecommendationType.BASKET_RECOMMENDATIONS,
        )

        for reco_type in recommendation_types:
            recommendations += int(self.metrics_dao.get_metrics_last(
                client, "recommendations", {"method": reco_type.name.lower()}, db_from_timestamp
            ))

        result = {
            "user_events_received": int(self.metrics_dao.get_metrics_last(
                client, "events__total", {}, db_from_timestamp
            )),
            "catalog_updates_received": updates,
            "recommendations_returned": recommendations,
            "search_results_returned": int(self.metrics_dao.get_metrics_last(
                client, "recommendations", {"method": "smart_search"}, db_from_timestamp
            ))
        }

        return self.format_result(result)

    # AUDIENCES PAGES

    def get_audiences_page(self, client):
        self.assert_has_access_to_client(client)

        audiences = self.audience_dao.read_all_audiences(client)
        audiences_unions = self.audience_dao.read_all_audiences_unions(client)
        split_trees = self.audience_dao.read_all_audience_split_trees(client)

        audiences_to_audiences_unions_map = defaultdict(set)
        audiences_to_split_trees_map = defaultdict(set)

        for audiences_union in audiences_unions:
            for audience_id in audiences_union.audience_ids:
                audiences_to_audiences_unions_map[audience_id].add(audiences_union.audiences_union_id)
        for split_tree in split_trees:
            for audience_split in split_tree.audience_splits:
                if audience_split.audience_ids is not None:
                    for audience_ids in audience_split.audience_ids:
                        for audience_id in audience_ids:
                            audiences_to_split_trees_map[audience_id].add(split_tree.split_tree_id)

        result = {
            "audiences": [{
                "audience": audience.as_json_dict(),
                "presented_in_audiences_unions": list(audiences_to_audiences_unions_map.get(audience.audience_id, [])),
                "presented_in_split_trees": list(audiences_to_split_trees_map.get(audience.audience_id, []))
            } for audience in audiences],

            "audiences_unions": [{
                "audiences_union": audiences_union.as_json_dict()
            } for audiences_union in audiences_unions],

            "split_trees": [{
                "split_tree": split_tree.as_json_dict()
            } for split_tree in split_trees]
        }

        return self.format_result(result)

    def get_split_trees_page(self, client):
        def _defines_audiences(split_tree: AudienceSplitTree) -> List[str]:
            _result = []
            for audience_split in split_tree.audience_splits or []:
                for audience_ids in audience_split.audience_ids or []:
                    for audience_id in audience_ids or []:
                        if audience_id is not None:
                            _result.append(audience_id)
            return sorted(_result)

        self.assert_has_access_to_client(client)

        result = {
            "split_trees": [{
                "split_tree": split_tree.as_json_dict(),
                "defines_audiences": _defines_audiences(split_tree)
            } for split_tree in self.audience_dao.read_all_audience_split_trees(client)],
            "audiences": [{
                "audience": audience.as_json_dict()
            } for audience in self.audience_dao.read_all_audiences(client)]
        }

        return self.format_result(result)

    def catalog_info(self, client):
        self.assert_has_access_to_client(client)

        active = self.catalog_dao.get_products_number(client)

        result = {
            "number": {
                "active": active
            }
        }

        return self.format_result(result)

    def products_list(self, client):
        self.assert_has_access_to_client(client)
        kwargs = {}

        limit = request.args.get("limit", 20)
        try:
            limit = int(limit)
        except ValueError:
            raise MobiWebException("Can not parse limit parameter") from None
        if limit < 1:
            raise MobiWebException("Limit should be a positive integer")
        if limit > 100:
            raise MobiWebException("Max number of products that can be fetched is 100")
        kwargs["limit"] = limit

        offset = request.args.get("offset", 0)
        try:
            offset = int(offset)
        except ValueError:
            raise MobiWebException("Can not parse offset parameter") from None
        if offset < 0:
            raise MobiWebException("Offset should be larger or equal to zero")
        if offset > 0:
            kwargs["offset"] = offset

        include_inactive = request.args.get("include_inactive", "no") == "yes"
        include_not_in_stock = request.args.get("include_not_in_stock", "no") == "yes"

        total_found = None

        q = request.args.get("q", "").strip()
        if q:
            total_found, found_products = self.search_dao.search_product(client, q, offset=offset, limit=limit,
                                                                         include_inactive=include_inactive,
                                                                         include_not_in_stock=include_not_in_stock)
            product_ids = [found_product.product_id for found_product in found_products]
            products = list(self.catalog_dao.read_products(client, product_ids, respect_order=True))
        else:
            products = list(self.catalog_dao.read_all_products(client, include_inactive=include_inactive,
                                                               include_not_in_stock=include_not_in_stock, **kwargs))

        products_as_json = [product.as_json_dict() for product in products]

        return self.format_result({"total": total_found, "products": products_as_json})

    def get_catalog_brands_page(self, client):
        self.assert_has_access_to_client(client)
        kwargs = {}

        limit = request.args.get("limit", 20)
        try:
            limit = int(limit)
        except ValueError:
            raise MobiWebException("Can not parse limit parameter") from None
        if limit < 1:
            raise MobiWebException("Limit should be a positive integer")
        if limit > 100:
            raise MobiWebException("Max number of products that can be fetched is 100")
        kwargs["limit"] = limit

        offset = request.args.get("offset", 0)
        try:
            offset = int(offset)
        except ValueError:
            raise MobiWebException("Can not parse offset parameter") from None
        if offset < 0:
            raise MobiWebException("Offset should be larger or equal to zero")
        if offset > 0:
            kwargs["offset"] = offset

        total_found = None

        q = request.args.get("q", "").strip()
        if q:
            total_found, found_brands = self.search_dao.search_brand(client, q, offset=offset, limit=limit)
            brands = [found_brand.brand for found_brand in found_brands]
        else:
            brands = list(self.search_dao.read_brands(client, limit=limit, offset=offset))
            total_found, brands = len(brands), brands[0:limit]

        results = {
            "total_found": total_found,
            "metrics_single": {
            },
            "brands": []
        }

        def get_cache_val(event_type: EventType, brand: str, period="24h") -> int:
            key = cache_key("brand_events", client=client, brand=brand, event_type=event_type.name.lower(),
                            period=period)
            try:
                return self.cache_dao.get_int(key, default=0, retries=3)
            except Exception:
                return 0

        for brand in brands:
            result = {
                "brand": brand,
                "metrics_single": {
                    "displays": get_cache_val(EventType.PRODUCT_VIEW, brand),
                    "clicks": 0,
                    "to_wishlist": get_cache_val(EventType.PRODUCT_TO_WISHLIST, brand),
                    "to_basket": get_cache_val(EventType.PRODUCT_TO_BASKET, brand),
                    "sales": get_cache_val(EventType.PRODUCT_SALE, brand)
                }
            }

            results["brands"].append(result)

        return self.format_result(results)

    def get_catalog_products_page(self, client):
        self.assert_has_access_to_client(client)
        kwargs = {}

        limit = request.args.get("limit", 20)
        try:
            limit = int(limit)
        except ValueError:
            raise MobiWebException("Can not parse limit parameter") from None
        if limit < 1:
            raise MobiWebException("Limit should be a positive integer")
        if limit > 100:
            raise MobiWebException("Max number of products that can be fetched is 100")
        kwargs["limit"] = limit

        offset = request.args.get("offset", 0)
        try:
            offset = int(offset)
        except ValueError:
            raise MobiWebException("Can not parse offset parameter") from None
        if offset < 0:
            raise MobiWebException("Offset should be larger or equal to zero")
        if offset > 0:
            kwargs["offset"] = offset

        include_inactive = request.args.get("include_inactive", "no") == "yes"
        include_not_in_stock = request.args.get("include_not_in_stock", "no") == "yes"

        total_found = None

        q = request.args.get("q", "").strip()
        if q:
            total_found, found_products = self.search_dao.search_product(client, q, offset=offset, limit=limit,
                                                                         include_inactive=include_inactive,
                                                                         include_not_in_stock=include_not_in_stock)
            product_ids = [found_product.product_id for found_product in found_products]
            products = list(self.catalog_dao.read_products(client, product_ids, respect_order=True))
        else:
            products = list(self.catalog_dao.read_all_products(client, include_inactive=include_inactive,
                                                               include_not_in_stock=include_not_in_stock, **kwargs))

        active_and_in_stock_num = self.metrics_dao.get_latest_point_value(
            client, "products_num", {"active": "yes", "in_stock": "yes"}, 24 * 60 * 60
        ) or 0.0

        active_and_not_in_stock_num = self.metrics_dao.get_latest_point_value(
            client, "products_num", {"active": "yes", "in_stock": "no"}, 24 * 60 * 60
        ) or 0.0

        disabled_num = self.metrics_dao.get_latest_point_value(
            client, "products_num", {"active": "no"}, 24 * 60 * 60
        ) or 0.0

        results = {
            "total_found": total_found,
            "metrics_single": {
                "active_and_in_stock_num": int(active_and_in_stock_num),
                "active_and_not_in_stock_num": int(active_and_not_in_stock_num),
                "disabled_num": int(disabled_num)
            },
            "products": []
        }

        def get_cache_val(event_type: EventType, product_id: str, period="24h") -> int:
            key = cache_key("product_events", client=client, product_id=product_id,
                            event_type=event_type.name.lower(), period=period)
            try:
                return self.cache_dao.get_int(key, default=0, retries=3)
            except Exception:
                return 0

        for product in products:
            result = {
                "product": product.as_json_dict(),
                "metrics_single": {
                    "displays": get_cache_val(EventType.PRODUCT_VIEW, product.product_id),
                    "clicks": 0,
                    "to_wishlist": get_cache_val(EventType.PRODUCT_TO_WISHLIST, product.product_id),
                    "to_basket": get_cache_val(EventType.PRODUCT_TO_BASKET, product.product_id),
                    "sales": get_cache_val(EventType.PRODUCT_SALE, product.product_id)
                }
            }

            results["products"].append(result)

        return self.format_result(results)

    def product_info(self, client, product_id):
        self.assert_has_access_to_client(client)

        try:
            result = self.catalog_dao.read_product(client, product_id).as_json_dict()
        except MobiDaoException:
            raise MobiWebException(f"Can not read product {product_id}") from None

        return self.format_result(result)

    def get_product_metrics_events(self, client, product_id):
        metrics_from_raw = request.args.get("metrics_from", "24h")
        metrics_from = self._parse_time_interval(metrics_from_raw)

        metrics_base = None
        if metrics_from_raw[-1] == "m":
            metrics_base = 60
        elif metrics_from_raw[-1] == "h":
            metrics_base = 60 * 60
        elif metrics_from_raw[-1] == "d":
            metrics_base = 24 * 60 * 60

        db_from_timestamp = metrics_base * (metrics_from // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(datetime.utcnow().timestamp()) // metrics_base) + metrics_base

        self.assert_has_access_to_client(client)
        try:
            _ = self.catalog_dao.read_product(client, product_id)
        except MobiDaoException as e:
            raise MobiWebException(f"Can not read product: {str(e)}")

        result = {
            "metrics_graph": dict(),
            "metrics_graph_relative": dict()
        }

        recommendation_type = None
        metric_name = "events"

        if request.args.get("attributed", "no") == "yes":
            metric_name = "attributed_events"

            recommendation_type = request.args.get("recommendation_type", "all")
            if recommendation_type != "all":
                try:
                    recommendation_type_enum = RecommendationType.parse(recommendation_type)
                except Exception:
                    raise MobiWebException(f"Can not parse recommendation_type \"{recommendation_type}\"") from None
                recommendation_type = recommendation_type_enum.name.lower()
            else:
                recommendation_type = None

        for event_type in EventType:
            tags = {
                "product_id": product_id
            }

            if metric_name == "attributed_events":
                tags["event_type"] = event_type.name.lower()
            else:
                tags["type"] = event_type.name.lower()

            if recommendation_type is not None:
                tags["recommendation_type"] = recommendation_type

            result["metrics_graph"][event_type.name.lower()] = self.fetch_metric(
                client, metric_name, tags, db_from_timestamp, db_until_timestamp, metrics_base
            )

        for event_type in EventType:
            if event_type == EventType.PRODUCT_VIEW:
                continue

            result["metrics_graph_relative"][event_type.name.lower()] = []

            for m, m_d in zip(result["metrics_graph"][event_type.name.lower()],
                              result["metrics_graph"][EventType.PRODUCT_VIEW.name.lower()]):
                date, _ = m
                _, events_num = m
                _, displays_num = m_d

                if abs(displays_num) < 0.01:
                    result["metrics_graph_relative"][event_type.name.lower()].append((date, 0.0))
                else:
                    result["metrics_graph_relative"][event_type.name.lower()].append((date, events_num/displays_num))

        return self.format_result(result)

    def product_events_num(self, client, time_interval):
        self.assert_has_access_to_client(client)

        status = request.args.get("status")
        timestamp_from = self._parse_time_interval(time_interval)
        brand = request.args.get("brand")

        tags = {
            "action": "create"
        }

        if status is not None:
            if status.lower() in ("created", "updated", "deleted", "no_effect"):
                tags["status"] = status.lower()
            else:
                raise MobiWebException(f"Wrong status value: {status}")

        if brand:
            tags["brand"] = brand

        result = {
            "value": self.metrics_dao.get_metrics_last(client, "products", tags, timestamp_from)
        }

        return self.format_result(result)

    def user_events_num(self, client, time_interval):
        self.assert_has_access_to_client(client)

        event_type = request.args.get("event_type")
        timestamp_from = self._parse_time_interval(time_interval)
        brand = request.args.get("brand")

        tags = {}

        if event_type:
            tags["type"] = EventType.parse(event_type).name.lower()

        if brand:
            tags["brand"] = brand

        result = {
            "value": self.metrics_dao.get_metrics_last(client, "events__total", tags, timestamp_from)
            + self.metrics_dao.get_metrics_last(client, "events_created__total", tags, timestamp_from)
        }

        return self.format_result(result)

    def recommendation_events_num(self, client, time_interval):
        self.assert_has_access_to_client(client)

        recommendation_type = request.args.get("recommendation_type")
        timestamp_from = self._parse_time_interval(time_interval)

        tags = {}

        if recommendation_type:
            if recommendation_type not in ("most_popular", "similar_products",
                                           "personal_recommendations", "smart_search"):
                raise MobiWebException(f"Wrong recommendation type: {recommendation_type}")
            tags["method"] = recommendation_type

        result = {
            "value": self.metrics_dao.get_metrics_last(client, "recommendations", tags, timestamp_from)
        }

        return self.format_result(result)

    def catalog_brands(self, client):
        self.assert_has_access_to_client(client)

        kwargs = {}

        limit = request.args.get("limit", 20)
        try:
            limit = int(limit)
        except ValueError:
            raise MobiWebException("Can not parse limit parameter") from None
        if limit < 1:
            raise MobiWebException("Limit should be a positive integer")
        if limit > 100:
            raise MobiWebException("Max number of brands that can be fetched is 100")
        kwargs["limit"] = limit

        offset = request.args.get("offset", 0)
        try:
            offset = int(offset)
        except ValueError:
            raise MobiWebException("Can not parse offset parameter") from None
        if offset < 0:
            raise MobiWebException("Offset should be larger or equal to zero")
        if offset > 0:
            kwargs["offset"] = offset

        source = "database"
        q = request.args.get("q", "").strip()

        if q:
            _, brands = self.search_dao.search_brand(client, q, **kwargs)
            brands = [item.brand for item in brands]
        else:
            key = cache_key("brands", client=client, limit=limit, offset=offset)
            brands_dump = self.cache_dao.get(key)

            if brands_dump is not None:
                source = "cache"
                brands = pickle.loads(brands_dump)
            else:
                # This is bad and should be improved
                brands = list(self.search_dao.read_brands(client, limit=limit, offset=offset))
                self.cache_dao.set(key, pickle.dumps(brands), expire=60)

        result = {
            "brands": brands
        }

        return self.format_result(result, meta={"source": source})

    def catalog_categories(self, client):
        self.assert_has_access_to_client(client)

        kwargs = {}

        limit = request.args.get("limit", 20)
        try:
            limit = int(limit)
        except ValueError:
            raise MobiWebException("Can not parse limit parameter") from None
        if limit < 1:
            raise MobiWebException("Limit should be a positive integer")
        if limit > 100:
            raise MobiWebException("Max number of categories that can be fetched is 100")
        kwargs["limit"] = limit

        offset = request.args.get("offset", 0)
        try:
            offset = int(offset)
        except ValueError:
            raise MobiWebException("Can not parse offset parameter") from None
        if offset < 0:
            raise MobiWebException("Offset should be larger or equal to zero")
        if offset > 0:
            kwargs["offset"] = offset

        source = "database"
        q = request.args.get("q", "").strip()

        if q:
            _, categories = self.search_dao.search_category(client, q, **kwargs)
            categories = [item.category for item in categories]
        else:
            key = cache_key("categories", client=client, limit=limit, offset=offset)
            categories_dump = self.cache_dao.get(key)

            if categories_dump is not None:
                source = "cache"
                categories = pickle.loads(categories_dump)
            else:
                # This is bad and should be improved
                categories = list(self.search_dao.read_categories(client, limit=limit, offset=offset))
                self.cache_dao.set(key, pickle.dumps(categories), expire=60)

        result = {
            "categories": categories
        }

        return self.format_result(result, meta={"source": source})

    @classmethod
    def _format_metric_str(cls, ts: int, base: int) -> str:
        return datetime.fromtimestamp((ts // base) * base).strftime("%Y-%m-%d %H:%M:%S")

    def fetch_metric(self, client, name, tags, from_ts: int, until_ts: int, base_int: int) -> List[Tuple[str, float]]:
        self.assert_has_access_to_client(client)

        values = []
        for ts, value in self.metrics_dao.get_metrics(client, name, tags, base_int, from_ts, until_ts):
            values.append((ts, value))

        full_metrics = []
        for ts in range(from_ts, until_ts, base_int):
            val = 0.0
            try:
                if values and values[0][0] == ts:
                    val = values[0][1]
                    values.pop(0)
            except IndexError:
                raise MobiWebException(f"Internal error. Bad data received from database: \"{values[0]}\"")

            ts = self._format_metric_str(ts, base_int)
            full_metrics.append((ts, val))

        return full_metrics

    @classmethod
    def sum_fetched_metrics(cls, fetched_metrics: List[List[Tuple[str, float]]]) -> List[Tuple[str, float]]:
        result = defaultdict(lambda: 0.0)
        for metrics in fetched_metrics:
            for ts, value in metrics:
                result[ts] += value
        return [(ts, result[ts]) for ts, _ in fetched_metrics[0]]

    def unique_users_metric(self, client, time_interval):
        self.assert_has_access_to_client(client)
        timestamp_from = self._parse_time_interval(time_interval)

        base = None
        if time_interval[-1] == "m":
            base = 60
        elif time_interval[-1] == "h":
            base = 60 * 60
        elif time_interval[-1] == "d":
            base = 24 * 60 * 60

        db_from_timestamp = base * (timestamp_from // base) + base
        db_until_timestamp = base * (int(datetime.utcnow().timestamp()) // base) + base

        return self.format_result(self.fetch_metric(client, "unique_users", None, db_from_timestamp,
                                                    db_until_timestamp, base))

    def create_update_zone(self, client):
        self.assert_has_access_to_client(client)

        json = request.get_json(force=True, silent=True)
        if json is None:
            raise MobiWebException(f"Wrong request format. Ensure you pass a valid json object")

        try:
            validate_zone_json(json)
        except ValueError as e:
            raise MobiWebException(f"Can not parse zone object: {str(e)}")

        try:
            if "zone_id" not in json or not json["zone_id"]:
                json["zone_id"] = str(int(datetime.utcnow().timestamp())) + random_string(10)
            zone = Zone.from_dict(json)
        except ValueError as e:
            raise MobiWebException(f"The data is incorrect: {str(e)}") from None

        if zone.zone_settings.brands:
            brands_key = cache_key("brands", client=client)
            self.cache_dao.delete(brands_key, noreply=False, retries=10)

        if zone.zone_settings.categories:
            categories_key = cache_key("categories", client=client)
            self.cache_dao.delete(categories_key, noreply=False, retries=10)

        try:
            self.settings_dao.create_zone(client, zone)
        except Exception as e:
            raise MobiWebException(f"Can not create zone: {str(e)}") from None
        return self.format_result(self.settings_dao.read_zone(client, zone.zone_id).as_json_dict())

    def get_delete_zone(self, client, zone_id):
        self.assert_has_access_to_client(client)

        if not zone_id:
            raise MobiWebException(f"Zone id is not provided")

        if request.method == "GET":
            try:
                zone = self.settings_dao.read_zone(client, zone_id)
            except Exception as e:
                raise MobiWebException(f"Can not retrieve zone: {str(e)}") from None

            return self.format_result(zone.as_json_dict())

        if request.method == "DELETE":
            try:
                self.settings_dao.delete_zone(client, zone_id)
            except Exception as e:
                raise MobiWebException(f"Can not delete zone: {str(e)}") from None

            return self.format_result()

    def get_zone_metrics(self, client, zone_id):
        self.assert_has_access_to_client(client)

        if not zone_id:
            raise MobiWebException(f"Zone id is not provided")

        try:
            _ = self.settings_dao.read_zone(client, zone_id)
        except Exception as e:
            raise MobiWebException(f"Can not read zone {zone_id}: {str(e)}")

        metrics_from_raw = request.args.get("metrics_from", "24h")
        metrics_from = self._parse_time_interval(metrics_from_raw)

        metrics_base = None
        if metrics_from_raw[-1] == "m":
            metrics_base = 60
        elif metrics_from_raw[-1] == "h":
            metrics_base = 60 * 60
        elif metrics_from_raw[-1] == "d":
            metrics_base = 24 * 60 * 60

        db_from_timestamp = metrics_base * (metrics_from // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(datetime.utcnow().timestamp()) // metrics_base) + metrics_base

        result = dict()

        result["metrics_single"] = {
            "displays": self.metrics_dao.get_metrics_last(client, "recommendations", {"zone": zone_id},
                                                          timestamp_from=metrics_from),
            "clicks": self.metrics_dao.get_metrics_last(client, "recommendations", {"zone": zone_id},
                                                        timestamp_from=metrics_from),
            "to_basket": self.metrics_dao.get_metrics_last(client, "recommendations", {"zone": zone_id},
                                                           timestamp_from=metrics_from),
            "sales": self.metrics_dao.get_metrics_last(client, "recommendations", {"zone": zone_id},
                                                       timestamp_from=metrics_from),
            "ctr": 5.2,
            "converted": 4.2
        }

        result["metrics_graph"] = {
            "displays": self.fetch_metric(client, "recommendations", {"zone": zone_id},
                                          db_from_timestamp, db_until_timestamp, metrics_base),
            "clicks": self.fetch_metric(client, "recommendations", {"zone": zone_id},
                                        db_from_timestamp, db_until_timestamp, metrics_base),
            "to_basket": self.fetch_metric(client, "recommendations", {"zone": zone_id},
                                           db_from_timestamp, db_until_timestamp, metrics_base),
            "sales": self.fetch_metric(client, "recommendations", {"zone": zone_id},
                                       db_from_timestamp, db_until_timestamp, metrics_base),
        }

        return self.format_result(result)

    def get_campaign_metrics(self, client, campaign_id):
        self.assert_has_access_to_client(client)

        if not campaign_id:
            raise MobiWebException(f"Campaign id is not provided")

        try:
            _ = self.settings_dao.read_campaign(client, campaign_id)
        except Exception as e:
            raise MobiWebException(f"Can not read campaign {campaign_id}: {str(e)}")

        metrics_from_raw = request.args.get("metrics_from", "24h")
        metrics_from = self._parse_time_interval(metrics_from_raw)

        metrics_base = None
        if metrics_from_raw[-1] == "m":
            metrics_base = 60
        elif metrics_from_raw[-1] == "h":
            metrics_base = 60 * 60
        elif metrics_from_raw[-1] == "d":
            metrics_base = 24 * 60 * 60

        db_from_timestamp = metrics_base * (metrics_from // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(datetime.utcnow().timestamp()) // metrics_base) + metrics_base

        result = dict()

        result["metrics_single"] = {
            "displays": self.metrics_dao.get_metrics_last(client, "recommendations", {"campaign": campaign_id},
                                                          timestamp_from=metrics_from),
            "clicks": self.metrics_dao.get_metrics_last(client, "recommendations", {"campaign": campaign_id},
                                                        timestamp_from=metrics_from),
            "to_basket": self.metrics_dao.get_metrics_last(client, "recommendations", {"campaign": campaign_id},
                                                           timestamp_from=metrics_from),
            "sales": self.metrics_dao.get_metrics_last(client, "recommendations", {"campaign": campaign_id},
                                                       timestamp_from=metrics_from),
            "ctr": 5.2,
            "converted": 4.2
        }

        result["metrics_graph"] = {
            "displays": self.fetch_metric(client, "recommendations", {"campaign": campaign_id},
                                          db_from_timestamp, db_until_timestamp, metrics_base),
            "clicks": self.fetch_metric(client, "recommendations", {"campaign": campaign_id},
                                        db_from_timestamp, db_until_timestamp, metrics_base),
            "to_basket": self.fetch_metric(client, "recommendations", {"campaign": campaign_id},
                                           db_from_timestamp, db_until_timestamp, metrics_base),
            "sales": self.fetch_metric(client, "recommendations", {"campaign": campaign_id},
                                       db_from_timestamp, db_until_timestamp, metrics_base),
        }

        return self.format_result(result)

    def get_zones_num(self, client):
        self.assert_has_access_to_client(client)

        kwargs = {}

        if request.args.get("include_inactive", "no") == "yes":
            kwargs["include_inactive"] = True

        if request.args.get("q", "").strip():
            kwargs["text"] = request.args.get("q", "").strip()

        zones_count = self.settings_dao.count_all_zones(client, **kwargs)
        return self.format_result({"value": zones_count})

    def get_closest_zone_campaign(self, client, zone_id):
        self.assert_has_access_to_client(client)

        try:
            self.settings_dao.read_zone(client, zone_id)
        except KeyError:
            raise MobiWebException(f"Zone \"{zone_id}\" does not exist") from None

        campaigns = list(self.settings_dao.read_zone_campaigns(client, zone_id, include_inactive=False))
        campaign = calc_closest_zone_campaign(campaigns)

        if campaign is None:
            raise MobiWebException(f"Zone \"{zone_id}\" does not have active campaigns")

        return self.format_result(campaign.as_json_dict())

    def get_zone_applied_settings(self, client, zone_id, campaign_id):
        self.assert_has_access_to_client(client)

        try:
            zone = self.settings_dao.read_zone(client, zone_id)
        except KeyError:
            raise MobiWebException(f"Zone \"{zone_id}\" does not exist") from None

        try:
            campaign = self.settings_dao.read_campaign(client, campaign_id)
        except KeyError:
            raise MobiWebException(f"Campaign \"{campaign_id}\" does not exist") from None

        zone.zone_settings.apply_other(campaign.zone_settings)

        return self.format_result(zone.as_json_dict())

    def get_zone_stats(self, client, zone_id):
        self.assert_has_access_to_client(client)

        try:
            _ = self.settings_dao.read_zone(client, zone_id)
        except KeyError:
            raise MobiWebException(f"Zone \"{zone_id}\" does not exist") from None

        result = {
            "displays": 10,
            "ctr": 4.3,
            "converted": 55.1
        }

        return self.format_result(result)

    def get_zones(self, client):
        self.assert_has_access_to_client(client)
        kwargs = {}

        try:
            kwargs["offset"] = int(request.args.get("offset", 0))
        except ValueError:
            raise MobiWebException(f"Can not parse offset variable: {request.args.get('offset')}")

        try:
            kwargs["limit"] = int(request.args.get("limit", 20))
        except ValueError:
            raise MobiWebException(f"Can not parse limit variable: {request.args.get('limit')}")

        if request.args.get("include_inactive", "no") == "yes":
            kwargs["include_inactive"] = True

        if request.args.get("q", "").strip():
            kwargs["text"] = request.args.get("q", "").strip()

        try:
            zones = self.settings_dao.read_all_zones(client, **kwargs)
        except Exception as e:
            raise MobiWebException(f"Can not read zones: {str(e)}") from None

        return self.format_result([zone.as_json_dict() for zone in zones])

    def get_zones_page(self, client):
        self.assert_has_access_to_client(client)

        metrics_from_raw = request.args.get("metrics_from", "24h")
        metrics_from = self._parse_time_interval(metrics_from_raw)

        metrics_base = None
        if metrics_from_raw[-1] == "m":
            metrics_base = 60
        elif metrics_from_raw[-1] == "h":
            metrics_base = 60 * 60
        elif metrics_from_raw[-1] == "d":
            metrics_base = 24 * 60 * 60

        db_from_timestamp = metrics_base * (metrics_from // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(datetime.utcnow().timestamp()) // metrics_base) + metrics_base

        kwargs = {}

        try:
            kwargs["offset"] = int(request.args.get("offset", 0))
        except ValueError:
            raise MobiWebException(f"Can not parse offset variable: {request.args.get('offset')}")

        try:
            kwargs["limit"] = int(request.args.get("limit", 20))
        except ValueError:
            raise MobiWebException(f"Can not parse limit variable: {request.args.get('limit')}")

        if request.args.get("include_inactive", "no") == "yes":
            kwargs["include_inactive"] = True

        if request.args.get("q", "").strip():
            kwargs["text"] = request.args.get("q", "").strip()

        try:
            zones = list(self.settings_dao.read_all_zones(client, **kwargs))
        except Exception as e:
            raise MobiWebException(f"Can not read zones: {str(e)}") from None

        results = []
        campaigns_cache = {}

        for zone in zones:
            if zone.campaign_ids:
                for campaign_id in zone.campaign_ids:
                    if campaign_id not in campaigns_cache:
                        try:
                            campaigns_cache[campaign_id] = self.settings_dao.read_campaign(client, campaign_id)
                        except KeyError:
                            pass
                        except Exception as e:
                            raise MobiWebException(f"Can not read campaign \"{campaign_id}\": {str(e)}") from None

        def _jsonify_campaign_or_none(campaign: Optional[Campaign]):
            if campaign is not None:
                campaign = campaign.as_json_dict()
            return campaign

        for zone in zones:
            result = dict()
            result["zone"] = zone.as_json_dict()
            ordered_campaigns = [campaigns_cache.get(campaign_id)
                                 for campaign_id in zone.campaign_ids if campaign_id in campaigns_cache]
            result["ordered_campaigns"] = [campaign.as_json_dict() for campaign in ordered_campaigns]

            active_campaign = calc_active_zone_campaign(ordered_campaigns)
            result["active_campaign"] = _jsonify_campaign_or_none(active_campaign)

            result["zone_settings_effective"] = None
            if active_campaign is not None:
                zone_settings = zone.zone_settings
                zone_settings.apply_other(active_campaign.zone_settings)
                result["zone_settings_effective"] = zone_settings.as_json_dict()
            result["next_campaign"] = _jsonify_campaign_or_none(calc_next_zone_campaign(ordered_campaigns))
            result["closest_campaign"] = _jsonify_campaign_or_none(calc_closest_zone_campaign(ordered_campaigns))
            result["metrics_single"] = {
                "displays": self.metrics_dao.get_metrics_last(client, "recommendations", {"zone": zone.zone_id},
                                                              timestamp_from=metrics_from),
                "clicks": self.metrics_dao.get_metrics_last(client, "recommendations", {"zone": zone.zone_id},
                                                            timestamp_from=metrics_from),
                "to_basket": self.metrics_dao.get_metrics_last(client, "recommendations", {"zone": zone.zone_id},
                                                               timestamp_from=metrics_from),
                "sales": self.metrics_dao.get_metrics_last(client, "recommendations", {"zone": zone.zone_id},
                                                           timestamp_from=metrics_from),
                "ctr": 5.2,
                "converted": 4.2
            }
            result["metrics_graph"] = {
                "displays": self.fetch_metric(client, "recommendations", {"zone": zone.zone_id},
                                              db_from_timestamp, db_until_timestamp, metrics_base),
                "clicks": self.fetch_metric(client, "recommendations", {"zone": zone.zone_id},
                                            db_from_timestamp, db_until_timestamp, metrics_base),
                "to_basket": self.fetch_metric(client, "recommendations", {"zone": zone.zone_id},
                                               db_from_timestamp, db_until_timestamp, metrics_base),
                "sales": self.fetch_metric(client, "recommendations", {"zone": zone.zone_id},
                                           db_from_timestamp, db_until_timestamp, metrics_base),
            }
            results.append(result)

        return self.format_result(results)

    def get_campaigns_page(self, client):
        self.assert_has_access_to_client(client)

        metrics_from_raw = request.args.get("metrics_from", "24h")
        metrics_from = self._parse_time_interval(metrics_from_raw)

        metrics_base = None
        if metrics_from_raw[-1] == "m":
            metrics_base = 60
        elif metrics_from_raw[-1] == "h":
            metrics_base = 60 * 60
        elif metrics_from_raw[-1] == "d":
            metrics_base = 24 * 60 * 60

        db_from_timestamp = metrics_base * (metrics_from // metrics_base) + metrics_base
        db_until_timestamp = metrics_base * (int(datetime.utcnow().timestamp()) // metrics_base) + metrics_base

        kwargs = {}

        try:
            kwargs["offset"] = int(request.args.get("offset", 0))
        except ValueError:
            raise MobiWebException(f"Can not parse offset variable: {request.args.get('offset')}")

        try:
            kwargs["limit"] = int(request.args.get("limit", 20))
        except ValueError:
            raise MobiWebException(f"Can not parse limit variable: {request.args.get('limit')}")

        if request.args.get("include_inactive", "no") == "yes":
            kwargs["include_inactive"] = True

        if request.args.get("q", "").strip():
            kwargs["text"] = request.args.get("q", "").strip()

        results = []

        try:
            campaigns = list(self.settings_dao.read_all_campaigns(client, **kwargs))
        except Exception as e:
            raise MobiWebException(f"Can not read zones: {str(e)}") from None

        for campaign in campaigns:
            result = dict()
            result["campaign"] = campaign.as_json_dict()

            result["metrics_single"] = {
                "displays": self.metrics_dao.get_metrics_last(client, "recommendations",
                                                              {"campaign": campaign.campaign_id},
                                                              timestamp_from=metrics_from),
                "clicks": self.metrics_dao.get_metrics_last(client, "recommendations",
                                                            {"campaign": campaign.campaign_id},
                                                            timestamp_from=metrics_from),
                "to_basket": self.metrics_dao.get_metrics_last(client, "recommendations",
                                                               {"campaign": campaign.campaign_id},
                                                               timestamp_from=metrics_from),
                "sales": self.metrics_dao.get_metrics_last(client, "recommendations",
                                                           {"campaign": campaign.campaign_id},
                                                           timestamp_from=metrics_from),
                "ctr": 5.2,
                "converted": 4.2
            }
            result["metrics_graph"] = {
                "displays": self.fetch_metric(client, "recommendations", {"campaign": campaign.campaign_id},
                                              db_from_timestamp, db_until_timestamp, metrics_base),
                "clicks": self.fetch_metric(client, "recommendations", {"campaign": campaign.campaign_id},
                                            db_from_timestamp, db_until_timestamp, metrics_base),
                "to_basket": self.fetch_metric(client, "recommendations", {"campaign": campaign.campaign_id},
                                               db_from_timestamp, db_until_timestamp, metrics_base),
                "sales": self.fetch_metric(client, "recommendations", {"campaign": campaign.campaign_id},
                                           db_from_timestamp, db_until_timestamp, metrics_base),
            }
            results.append(result)

        return self.format_result(results)

    def create_update_campaigns(self, client):
        self.assert_has_access_to_client(client)

        json = request.get_json(force=True, silent=True)
        if json is None:
            raise MobiWebException(f"Wrong request format. Ensure you pass a valid json object")

        try:
            validate_campaign_json(json)
        except ValueError as e:
            raise MobiWebException(f"Can not parse campaign object: {str(e)}")

        try:
            if "campaign_id" not in json or not json["campaign_id"]:
                json["campaign_id"] = str(int(datetime.utcnow().timestamp())) + random_string(10)
            campaign = Campaign.from_dict(json)
        except ValueError as e:
            raise MobiWebException(f"The data is incorrect: {str(e)}") from None

        if campaign.zone_settings.brands:
            brands_key = cache_key("brands", client=client)
            self.cache_dao.delete(brands_key, noreply=False, retries=10)

        if campaign.zone_settings.categories:
            categories_key = cache_key("categories", client=client)
            self.cache_dao.delete(categories_key, noreply=False, retries=10)

        try:
            self.settings_dao.create_campaign(client, campaign)
        except Exception as e:
            raise MobiWebException(f"Can not create campaign: {str(e)}") from None
        return self.format_result(self.settings_dao.read_campaign(client, campaign.campaign_id).as_json_dict())

    def get_delete_campaign(self, client, campaign_id):
        self.assert_has_access_to_client(client)

        if not campaign_id:
            raise MobiWebException(f"Campaign id is not provided")

        if request.method == "GET":
            try:
                campaign = self.settings_dao.read_campaign(client, campaign_id)
            except Exception as e:
                raise MobiWebException(f"Can not retrieve campaign: {str(e)}") from None

            return self.format_result(campaign.as_json_dict())

        if request.method == "DELETE":
            try:
                self.settings_dao.delete_campaign(client, campaign_id)
            except Exception as e:
                raise MobiWebException(f"Can not delete campaign: {str(e)}") from None

            return self.format_result()

    def get_campaigns(self, client):
        self.assert_has_access_to_client(client)

        campaigns = self.settings_dao.read_all_campaigns(client, include_inactive=True)
        return self.format_result([campaign.as_json_dict() for campaign in campaigns])

    def get_zone_campaigns(self, client, zone_id):
        self.assert_has_access_to_client(client)

        if not zone_id:
            raise MobiWebException(f"Zone id is not provided")

        include_inactive = request.args.get("include_inactive", "no") == "yes"
        include_past = request.args.get("include_past", "no") == "yes"

        campaigns = self.settings_dao.read_zone_campaigns(client, zone_id, include_inactive=include_inactive,
                                                          include_past=include_past)
        return self.format_result([campaign.as_json_dict() for campaign in campaigns])

    def attach_campaign_to_zone(self, client, zone_id, campaign_id):
        self.assert_has_access_to_client(client)

        if not zone_id:
            raise MobiWebException(f"Zone id is not provided")

        if not campaign_id:
            raise MobiWebException(f"Campaign id is not provided")

        try:
            self.settings_dao.add_campaign_to_zone(client, zone_id, campaign_id)
        except Exception as e:
            raise MobiWebException(f"Can not attach campaign to zone: {str(e)}") from None

        return self.format_result()

    def remove_campaign_from_zone(self, client, zone_id, campaign_id):
        self.assert_has_access_to_client(client)

        if not zone_id:
            raise MobiWebException(f"Zone id is not provided")

        if not campaign_id:
            raise MobiWebException(f"Campaign id is not provided")

        try:
            self.settings_dao.remove_campaign_from_zone(client, zone_id, campaign_id)
        except Exception as e:
            raise MobiWebException(f"Can not remove campaign from zone: {str(e)}") from None

        return self.format_result()

    def get_abtest_metrics(self, client, metric_name):
        self.assert_has_access_to_client(client)

        if metric_name not in ("to_basket_num", "to_wishlist_num", "views_num", "session_length", "sessions_per_user",
                               "purchases_num"):
            raise MobiWebException(f"Wrong metric name: {str(metric_name)}")

        period = request.args.get("period", "daily")
        if period not in ("daily", "7d", "14d", "28d"):
            raise MobiWebException(f"Wrong period parameter: {str(period)}")

        populations_str = [p.name.lower() for p in Population] + ["target_and_test"]

        pop_a = request.args.get("pop_a")
        if pop_a not in populations_str:
            raise MobiWebException(f"Bad population A: {str(pop_a)}")

        pop_b = request.args.get("pop_b")
        if pop_b not in populations_str:
            raise MobiWebException(f"Bad population B: {str(pop_b)}")

        days = request.args.get("days", 7)
        try:
            days = int(days)
        except ValueError:
            raise MobiWebException(f"Can not parse \"days\" parameter: {str(days)}") from None

        if days < 1:
            raise MobiWebException(f"Days should be a positive integer")
        if days > 31:
            raise MobiWebException(f"Too many days requested: {str(days)}")

        tags = {
            "metric": metric_name,
            "period": period
        }

        base = 24 * 60 * 60
        ts_now = int(datetime.utcnow().timestamp())

        ts_until = base * (ts_now // base) + base
        ts_from = ts_until - (base * days)

        kwargs = {
            "from_ts": ts_from,
            "until_ts": ts_until,
            "base_int": base
        }

        pop_b_mean_values = self.fetch_metric(client, "abtest", {"pop": pop_b, "func": "mean", **tags}, **kwargs)
        abs_diff_lower_values = self.fetch_metric(
            client, "abtest", {"popA": pop_a, "popB": pop_b, "bound": "lower", **tags}, **kwargs
        )
        abs_diff_upper_values = self.fetch_metric(
            client, "abtest", {"popA": pop_a, "popB": pop_b, "bound": "upper", **tags}, **kwargs
        )

        def div_metrics(m1, m2):
            result = []
            for v1, v2 in zip(m1, m2):
                date, i1 = v1
                _, i2 = v2
                result.append((date, 0.0 if (i2 < 1 / (10 ** 9)) else i1 / i2))
            return result

        rel_diff_lower_values = div_metrics(abs_diff_lower_values, pop_b_mean_values)
        rel_diff_upper_values = div_metrics(abs_diff_upper_values, pop_b_mean_values)

        results = {
            "pop_a": {
                "population": pop_a,
                "points_num": self.fetch_metric(client, "abtest", {"pop": pop_a, "func": "len", **tags}, **kwargs),
                "mean_values": self.fetch_metric(client, "abtest", {"pop": pop_a, "func": "mean", **tags}, **kwargs),
            },
            "pop_b": {
                "population": pop_b,
                "points_num": self.fetch_metric(client, "abtest", {"pop": pop_b, "func": "len", **tags}, **kwargs),
                "mean_values": pop_b_mean_values,
            },
            "test_results": {
                "abs_diff": self.fetch_metric(client, "abtest", {"popA": pop_a, "popB": pop_b, "bound": "abs_diff",
                                                                 **tags}, **kwargs),
                "abs_diff_lower_bound": abs_diff_lower_values,
                "abs_diff_upper_bound": abs_diff_upper_values,
                
                "rel_diff": self.fetch_metric(client, "abtest", {"popA": pop_a, "popB": pop_b, "bound": "rel_diff",
                                                                 **tags}, **kwargs),
                "rel_diff_lower_bound": rel_diff_lower_values,
                "rel_diff_upper_bound": rel_diff_upper_values,
            }
        }
        return self.format_result(results)

    def get_create_update_audience(self, client):
        self.assert_has_access_to_client(client)

        if request.method == "POST":
            audience = parse_audience(request.data)
            if self.audience_dao.read_audience(client, audience.audience_id) is not None:
                raise MobiWebException(f"Audience with id \"{audience.audience_id}\" already exists")
            self.audience_dao.create_audience(client, audience)
            return self.format_result()
        if request.method == "PUT":
            audience = parse_audience(request.data)
            if not self.audience_dao.read_audience(client, audience.audience_id):
                raise MobiWebException(f"Audience with id \"{audience.audience_id}\" does not exist")
            self.audience_dao.create_audience(client, audience)
            return self.format_result()
        if request.method == "GET":
            audience_id = uri.read_str("audience_id", required=True, min_length=1, max_length=100)
            audience = self.audience_dao.read_audience(client, audience_id)
            if audience is None:
                raise MobiWebException(f"Audience \"{audience_id}\" does not exist")
            return self.format_result(audience.as_json_dict())
        if request.method == "DELETE":
            audience_id = uri.read_str("audience_id", required=True, min_length=1, max_length=100)
            for audiences_union in self.audience_dao.read_all_audiences_unions(client):
                if audience_id in audiences_union.audience_ids:
                    audiences_union.audience_ids = [a_id for a_id in audiences_union.audience_ids
                                                    if a_id != audience_id]
                    self.audience_dao.create_audiences_union(client, audiences_union)
            for audience_split_tree in self.audience_dao.read_all_audience_split_trees(client):
                should_update = False
                if audience_split_tree.audience_splits:
                    for split in audience_split_tree.audience_splits:
                        if split.audience_ids and audience_id in split.audience_ids:
                            split.audience_ids = [a_id for a_id in split.audience_ids if a_id != audience_id]
                            should_update = True
                if should_update:
                    self.audience_dao.create_audience_split_tree(client, audience_split_tree)
            self.audience_dao.delete_audience(client, audience_id)
            return self.format_result()

    def get_all_audiences(self, client):
        self.assert_has_access_to_client(client)
        return self.format_result([a.as_json_dict() for a in self.audience_dao.read_all_audiences(client)])

    def get_create_update_audiences_union(self, client):
        self.assert_has_access_to_client(client)

        if request.method == "POST":
            audiences_union = parse_audiences_union(request.data)
            if self.audience_dao.read_audiences_union(client, audiences_union.audiences_union_id) is not None:
                raise MobiWebException(f"Audiences Union with id \"{audiences_union.audiences_union_id}\" "
                                       f"already exists")
            self.audience_dao.create_audiences_union(client, audiences_union)
            return self.format_result()
        if request.method == "PUT":
            audiences_union = parse_audiences_union(request.data)
            if not self.audience_dao.read_audiences_union(client, audiences_union.audiences_union_id):
                raise MobiWebException(f"Audiences Union with id \"{audiences_union.audiences_union_id}\" "
                                       f"does not exist")
            self.audience_dao.create_audiences_union(client, audiences_union)
            return self.format_result()
        if request.method == "GET":
            audiences_union_id = uri.read_str("audiences_union_id", required=True, min_length=1, max_length=100)
            audiences_union = self.audience_dao.read_audiences_union(client, audiences_union_id)
            if audiences_union is None:
                raise MobiWebException(f"Audiences Union \"{audiences_union_id}\" does not exist")
            return self.format_result(audiences_union.as_json_dict())
        if request.method == "DELETE":
            audiences_union_id = uri.read_str("audiences_union_id", required=True, min_length=1, max_length=100)
            self.audience_dao.delete_audiences_union(client, audiences_union_id)
            return self.format_result()

    def get_all_audiences_unions(self, client):
        self.assert_has_access_to_client(client)
        return self.format_result([au.as_json_dict() for au in self.audience_dao.read_all_audiences_unions(client)])

    def get_create_update_audience_split_tree(self, client):
        self.assert_has_access_to_client(client)

        if request.method == "POST":
            audience_split_tree = parse_audience_split_tree(request.data)
            if self.audience_dao.read_audience_split_tree(client, audience_split_tree.split_tree_id) is not None:
                raise MobiWebException(f"Audience Split Tree with id \"{audience_split_tree.split_tree_id}\" "
                                       f"already exists")
            self.audience_dao.create_audience_split_tree(client, audience_split_tree)
            return self.format_result()
        if request.method == "PUT":
            audience_split_tree = parse_audience_split_tree(request.data)
            if not self.audience_dao.read_audience_split_tree(client, audience_split_tree.split_tree_id):
                raise MobiWebException(f"Audience Split Tree with id \"{audience_split_tree.split_tree_id}\" "
                                       f"does not exist")
            self.audience_dao.create_audience_split_tree(client, audience_split_tree)
            return self.format_result()
        if request.method == "GET":
            split_tree_id = uri.read_str("split_tree_id", required=True, min_length=1, max_length=100)
            audience_split_tree = self.audience_dao.read_audience_split_tree(client, split_tree_id)
            if audience_split_tree is None:
                raise MobiWebException(f"Audience Split Tree \"{split_tree_id}\" does not exist")
            return self.format_result(audience_split_tree.as_json_dict())
        if request.method == "DELETE":
            split_tree_id = uri.read_str("split_tree_id", required=True, min_length=1, max_length=100)
            self.audience_dao.delete_audience_split_tree(client, split_tree_id)
            return self.format_result()

    def get_all_audience_spit_tree(self, client):
        self.assert_has_access_to_client(client)
        return self.format_result([st.as_json_dict() for st in self.audience_dao.read_all_audience_split_trees(client)])

    def get_test_recommendation(self, client):
        self.assert_has_access_to_client(client)

        recommendation_type_raw = request.args.get("recommendation_type")
        if not recommendation_type_raw:
            raise MobiWebException("recommendation_type is not provided")

        try:
            recommendation_type = RecommendationType.parse(recommendation_type_raw)
        except Exception:
            raise MobiWebException(f"Bad recommendation type: \"{recommendation_type_raw}\"") from None

        if recommendation_type == RecommendationType.UNKNOWN:
            raise MobiWebException(f"Unknown recommendation type: \"{recommendation_type_raw}\"")

        if recommendation_type not in {RecommendationType.SIMILAR_PRODUCTS, RecommendationType.PERSONAL_RECOMMENDATIONS,
                                       RecommendationType.BASKET_RECOMMENDATIONS}:
            raise MobiWebException(f"Unsupported recommendation type provided: \"{recommendation_type_raw}\"")

        if not request.args.get("browsing_history"):
            raise MobiWebException(f"Browsing history is not defined")

        allowed_query_params = {
            "recommendation_type",
            "browsing_history",
            "num",
            "product_id"
        }

        allowed_multi_query_params = {
            "basket_product_ids",
            "brands"
        }

        query_string = "&".join(f"{werkzeug.urls.url_quote(param)}={werkzeug.urls.url_quote(value)}"
                                for param, value in request.args.items() if param in allowed_query_params)

        for param in allowed_multi_query_params:
            values = request.args.getlist(param)
            if values:
                values_str = "&".join(f"{werkzeug.urls.url_quote(param)}"
                                      f"={werkzeug.urls.url_quote(value)}" for value in values)
                if query_string:
                    query_string += "&"
                query_string += values_str

        # This is a dirty hack that has to be refactored in future

        token = f"8d636fbbc20fce016c4a4b31910a8c596799af69-console-token-for-test-recommendations-{client}"

        try:
            response_raw = requests.get(f"https://api.mobimobi.tech/v2/recommendation/products/test"
                                        f"?recommendation_type={recommendation_type.name.lower()}&{query_string}"
                                        f"&token={token}")
        except Exception as e:
            raise MobiWebException(f"Can not send request to the recommendation service: {str(e)}")

        try:
            response = response_raw.json()

            if not isinstance(response, dict):
                raise ValueError()
        except ValueError:
            raise MobiWebException(f"Can not parse a response from recommendation api") from None

        if "status" not in response:
            raise MobiWebException(f"Wrong response format received from recommendation api")

        if response["status"] != "OK":
            try:
                message = response["error"]["description"]
            except Exception:
                message = "<can not retrieve error description from the response>"
            raise MobiWebException(f"Error occurred while calling recommendation API: {message}")

        if "data" not in response:
            raise MobiWebException(f"Response from recommendation api has no data field")

        return self.format_result(response["data"])

    def get_test_search(self, client):
        self.assert_has_access_to_client(client)

        if not request.args.get("browsing_history"):
            raise MobiWebException(f"Browsing history is not defined")

        allowed_query_params = {
            "browsing_history",
            "num",
            "q"
        }

        allowed_multi_query_params = {
            "brands"
        }

        query_string = "&".join(f"{werkzeug.urls.url_quote(param)}={werkzeug.urls.url_quote(value)}"
                                for param, value in request.args.items() if param in allowed_query_params)

        for param in allowed_multi_query_params:
            values = request.args.getlist(param)
            if values:
                values_str = "&".join(f"{werkzeug.urls.url_quote(param)}"
                                      f"={werkzeug.urls.url_quote(value)}" for value in values)
                if query_string:
                    query_string += "&"
                query_string += values_str

        # This is a dirty hack that has to be refactored in future

        token = f"8d636fbbc20fce016c4a4b31910a8c596799af69-console-token-for-test-recommendations-{client}"

        try:
            response_raw = requests.get(f"https://api.mobimobi.tech/v2/search/test?{query_string}"
                                        f"&token={token}")
        except Exception as e:
            raise MobiWebException(f"Can not send request to the recommendation service: {str(e)}")

        try:
            response = response_raw.json()

            if not isinstance(response, dict):
                raise ValueError()
        except ValueError:
            raise MobiWebException(f"Can not parse a response from recommendation api") from None

        if "status" not in response:
            raise MobiWebException(f"Wrong response format received from recommendation api")

        if response["status"] != "OK":
            try:
                message = response["error"]["description"]
            except Exception:
                message = "<can not retrieve error description from the response>"
            raise MobiWebException(f"Error occurred while calling recommendation API: {message}")

        if "data" not in response:
            raise MobiWebException(f"Response from recommendation api has no data field")

        return self.format_result(response["data"])


def get_args() -> argparse.Namespace:
    _default_host = "0.0.0.0"
    _default_port = 9701

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"host listen address. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"defines a port to listen. Default: {_default_port}", type=int)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    console_app = ConsoleApplication("console", args.host, args.port)
    console_app.run()
