import argparse
import functools
import google.oauth2.credentials
import googleapiclient.discovery
import json

from authlib.integrations.requests_client import OAuth2Session
from flask import Flask, make_response, redirect, request, session, url_for
from base64 import decodebytes, encodebytes
from random import choices
from string import ascii_letters, digits
from typing import Optional
from urllib.parse import urlparse
from waitress import serve

from mobi.config.env import Env, get_env


ACCESS_TOKEN_URI = "https://www.googleapis.com/oauth2/v4/token"
AUTHORIZATION_URL = "https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&prompt=consent"
AUTHORIZATION_SCOPE = "email"

AUTH_REDIRECT_URI = "http://localhost:9701/auth/google"

BASE_URI = "http://localhost:8050"
CLIENT_ID = "624911210133-o7ov7l9p5cthtt57lts41k0e7i2uia6m.apps.googleusercontent.com"
CLIENT_SECRET = "yApxa9HdKwXYspEnF2tl3lbP"

AUTH_TOKEN_KEY = 'auth_token'
AUTH_STATE_KEY = 'auth_state'

PRIVACY_POLICY = '<a href="https://www.iubenda.com/privacy-policy/57726776" ' \
                 'class="iubenda-white iubenda-embed" title="Privacy Policy ">Privacy Policy</a>' \
                 '<script type="text/javascript">(function (w,d) {var loader = function () ' \
                 '{var s = d.createElement("script"), ' \
                 'tag = d.getElementsByTagName("script")[0]; ' \
                 's.src="https://cdn.iubenda.com/iubenda.js"; ' \
                 'tag.parentNode.insertBefore(s,tag);}; ' \
                 'if(w.addEventListener){w.addEventListener("load", loader, false);}' \
                 'else if(w.attachEvent){w.attachEvent("onload", loader);}' \
                 'else{w.onload = loader;}}' \
                 ')(window, document);</script>'

LOGIN_FORM = """
<html>
<head>
<title>MobiMobi.Tech Login Form</title>
</head>
<body>
<div class="id-form">
    <div class="error-message">%ERROR_MESSAGE%</div>
    <div class="google-oauth-form">
        <div class="google-oauth-title"></div>
        <div class="google-oauth-button">
            <a href="%GOOGLE_OAUTH_LINK%">
                <img src="%GOOGLE_OAUTH_IMG%"></img>
            </a>
        </div>
    </div>
    <div class="email-login-form">
        <form method="post" action="/login/email">
            <div class="or-line">OR Log in with your email
            </div>
            <div class="login-form-email">Email: <input type="text" name="email"></input></div>
            <div class="login-form-password">Password: <input type="password" name="password"></input></div>
            <div class="login-form-submit"><input type="submit"></input></div>
        </form>
    </div>
</div>
</body>
</html>
"""


EXCEPTION_STR = """
<html>
<head><title>MobiMobi.Tech Error</title></head>
<body>
<div class="exception">
    <div class="exception-title">%EXCEPTION_TITLE%</div>
    <div class="exception-body">%EXCEPTION_BODY%</div>
</div>
</body>
</html>
"""


def special_chars(s: str):
    return s.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")


def no_cache(view):
    @functools.wraps(view)
    def no_cache_impl(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers["Cache-Control"] = "no-store, no-cache, must-revalidate, max-age=0"
        response.headers["Pragma"] = "no-cache"
        response.headers["Expires"] = "-1"
        return response

    return functools.update_wrapper(no_cache_impl, view)


def random_string(k: int = 16) -> str:
    return "".join(choices(ascii_letters + digits, k=k))


def encode_state(state: str) -> str:
    return encodebytes((random_string() + "_" + state).encode("utf-8")).decode("utf-8")


def decode_state(state: str) -> Optional[str]:
    state_bytes: bytes = decodebytes(state.encode())
    state_arr = state_bytes.decode("utf-8").split("_")
    if len(state_arr) != 2:
        return None
    return state_arr[1]


class MobiLoginApplication:
    def __init__(self, host, port):
        self.host = host
        self.port = port

        self.app = Flask("MobiMobi Login Application", static_url_path="/static",
                         static_folder="static")
        self.app.secret_key = "befbb3a56871a76f7267c0018cc36c9f0c59eb1c"

        self.app.add_url_rule("/", "index", self.index, methods=["GET"])
        self.app.add_url_rule("/login", "login", self.login, methods=["GET"])
        # self.app.add_url_rule("/", "index", self.index, methods=["GET"])
        self.app.add_url_rule("/login/email", "login_email", self.login_email, methods=["POST"])

        self.app.add_url_rule("/google/login", "google_login", self.google_login, methods=["GET"])
        self.app.add_url_rule("/auth/google", "google_auth", self.google_auth, methods=["GET"])
        self.app.add_url_rule("/google/logout", "google_logout",
                              self.google_logout, methods=["GET"])

        # self.app.register_error_handler(Exception, self.error_handler)

    @staticmethod
    def is_logged_in():
        return AUTH_TOKEN_KEY in session

    def error_handler(self, e: Exception):
        return EXCEPTION_STR.replace("%EXCEPTION_TITLE%", "Error")\
            .replace("%EXCEPTION_BODY%", special_chars(str(e))), 500

    def index(self):
        if self.is_logged_in():
            user_info = self.get_user_info()
            state = decode_state(session[AUTH_STATE_KEY])
            return '<div>State: ' + state + 'You are currently logged in as ' + user_info['email'] + '<div><pre>' + json.dumps(
                user_info, indent=4) + "</pre>"

        return 'You are not currently logged in.'

    def login(self):
        url = request.args.get("u")
        if not url:
            raise Exception("No redirect URL provided")

        url_details = urlparse(url)
        if not any([url_details.scheme, url_details.netloc]):
            raise Exception("Wrong URL format")

        msg = special_chars(request.args.get("msg", ""))

        if self.is_logged_in():
            return redirect(url, 302)

        return LOGIN_FORM\
            .replace("%ERROR_MESSAGE%", msg)\
            .replace("%GOOGLE_OAUTH_LINK%", url_for("google_login"))\
            .replace("%GOOGLE_OAUTH_IMG%", "/static/img/common/google/1x/btn_google_signin_dark_focus_web.png")

    def login_email(self):
        request.form.get("")
        return "OK"

    def build_credentials(self):
        if not self.is_logged_in():
            raise Exception("User must be logged in")

        oauth2_tokens = session[AUTH_TOKEN_KEY]

        return google.oauth2.credentials.Credentials(
            oauth2_tokens["access_token"],
            refresh_token=oauth2_tokens["refresh_token"],
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            token_uri=ACCESS_TOKEN_URI
        )

    def get_user_info(self):
        credentials = self.build_credentials()

        oauth2_client = googleapiclient.discovery.build(
            "oauth2", "v2", credentials=credentials, cache_discovery=False
        )

        return oauth2_client.userinfo().get().execute()

    @no_cache
    def google_login(self):
        oauth_session = OAuth2Session(CLIENT_ID, CLIENT_SECRET,
                                      scope=AUTHORIZATION_SCOPE,
                                      redirect_uri=AUTH_REDIRECT_URI)

        state = encode_state("MY STATE")

        uri, state = oauth_session.create_authorization_url(AUTHORIZATION_URL, state=state)

        session[AUTH_STATE_KEY] = state
        session.permanent = True

        return redirect(uri, code=302)

    @no_cache
    def google_auth(self):
        req_state = request.args.get("state", default=None, type=None)

        if req_state != session[AUTH_STATE_KEY]:
            response = make_response("Invalid state perimeter", 401)
            return response

        oauth_session = OAuth2Session(CLIENT_ID, CLIENT_SECRET,
                                      scope=AUTHORIZATION_SCOPE,
                                      state=session[AUTH_STATE_KEY],
                                      redirect_uri=AUTH_REDIRECT_URI)

        oauth_tokens = oauth_session.fetch_access_token(
            ACCESS_TOKEN_URI,
            authorization_response=request.url
        )

        session[AUTH_TOKEN_KEY] = oauth_tokens

        return redirect(BASE_URI, code=302)

    @no_cache
    def google_logout(self):
        session.pop(AUTH_TOKEN_KEY, None)
        session.pop(AUTH_STATE_KEY, None)

        return redirect(BASE_URI, code=302)

    def privacy_policy(self):
        return f"<html><body>{PRIVACY_POLICY}</body></html>"

    def run(self):
        if get_env() in (Env.PROD, Env.PREPROD, Env.GITLAB):
            serve(self.app, host=self.host, port=self.port, threads=8,
                  max_request_header_size=10*1024, max_request_body_size=10*1024,
                  channel_timeout=5, cleanup_interval=3, connection_limit=1024,
                  ident=self.__class__.__name__)
        else:
            self.app.run(self.host, self.port)


def get_args() -> argparse.Namespace:
    _default_host = "0.0.0.0"
    _default_port = 8050

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"host listen address. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"defines a port to listen. Default: {_default_port}", type=int)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    login_app = MobiLoginApplication(args.host, args.port)
    login_app.run()
