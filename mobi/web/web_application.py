import functools
import google.oauth2.credentials
import googleapiclient.discovery
import json
import os
import sys

from abc import ABC, abstractmethod
from authlib.integrations.requests_client import OAuth2Session
from dataclasses import dataclass
from datetime import datetime, timedelta
from flask import Flask, g, make_response, redirect, request, session, url_for
from base64 import decodebytes, encodebytes
from random import choices
from string import ascii_letters, digits
from typing import Dict, Optional, Union
from waitress import serve
from werkzeug.exceptions import HTTPException

from mobi.config.env import Env, get_env
from mobi.config.system.web._base import WebBaseConfig
from mobi.dao import get_audience_dao, get_auth_dao, get_cache_dao, get_catalog_dao, get_metrics_dao, get_search_dao,\
    get_settings_dao, get_user_settings_dao
from mobi.dao.audience import MobiAudienceDao
from mobi.dao.auth import MobiAuthDao
from mobi.dao.cache import MobiCache
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.search import MobiSearchDao
from mobi.dao.settings import MobiSettingsDao
from mobi.dao.user_settings import MobiUserSettingsDao
from mobi.exceptions import MobiException


ACCESS_TOKEN_URI = "https://www.googleapis.com/oauth2/v4/token"
AUTHORIZATION_URL = "https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&prompt=consent"
AUTHORIZATION_SCOPE = "email"

CLIENT_ID = "624911210133-o7ov7l9p5cthtt57lts41k0e7i2uia6m.apps.googleusercontent.com"
CLIENT_SECRET = "yApxa9HdKwXYspEnF2tl3lbP"

if get_env() == Env.PROD:
    CLIENT_ID = "557551456790-rf5ook3440b6dn4r39p9n4otiiq9osb2.apps.googleusercontent.com"
    CLIENT_SECRET = "BwkU3tHEB8K92MGo6xrwbXg0"


AUTH_TOKEN_KEY = 'auth_token'
AUTH_STATE_KEY = 'auth_state'

PRIVACY_POLICY = '<a href="https://www.iubenda.com/privacy-policy/57726776" ' \
                 'class="iubenda-white iubenda-embed" title="Privacy Policy ">Privacy Policy</a>' \
                 '<script type="text/javascript">(function (w,d) {var loader = function () ' \
                 '{var s = d.createElement("script"), ' \
                 'tag = d.getElementsByTagName("script")[0]; ' \
                 's.src="https://cdn.iubenda.com/iubenda.js"; ' \
                 'tag.parentNode.insertBefore(s,tag);}; ' \
                 'if(w.addEventListener){w.addEventListener("load", loader, false);}' \
                 'else if(w.attachEvent){w.attachEvent("onload", loader);}' \
                 'else{w.onload = loader;}}' \
                 ')(window, document);</script>'

LOGIN_FORM = """
<html>
<head>
<title>MobiMobi.Tech Login Form</title>
</head>
<body>
<div class="id-form">
    <div class="error-message">%ERROR_MESSAGE%</div>
    <div class="google-oauth-form">
        <div class="google-oauth-title"></div>
        <div class="google-oauth-button">
            <a href="%GOOGLE_OAUTH_LINK%">
                <img src="%GOOGLE_OAUTH_IMG%"></img>
            </a>
        </div>
    </div>
    <div class="email-login-form">
        <form method="post" action="/login/email">
            <div class="or-line">OR Log in with your email
            </div>
            <div class="login-form-email">Email: <input type="text" name="email"></input></div>
            <div class="login-form-password">Password: <input type="password" name="password"></input></div>
            <div class="login-form-submit"><input type="submit"></input></div>
        </form>
    </div>
</div>
</body>
</html>
"""


EXCEPTION_STR = """
<html>
<head><title>MobiMobi.Tech Error</title></head>
<body>
<div class="exception">
    <div class="exception-title">%EXCEPTION_TITLE%</div>
    <div class="exception-body">%EXCEPTION_BODY%</div>
</div>
</body>
</html>
"""


def special_chars(s: str):
    return s.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")


def no_cache(view):
    @functools.wraps(view)
    def no_cache_impl(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers["Cache-Control"] = "no-store, no-cache, must-revalidate, max-age=0"
        response.headers["Pragma"] = "no-cache"
        response.headers["Expires"] = "-1"
        return response

    return functools.update_wrapper(no_cache_impl, view)


def random_string(k: int = 16) -> str:
    return "".join(choices(ascii_letters + digits, k=k))


def encode_state(state: str) -> str:
    return encodebytes((random_string() + "_" + state).encode("utf-8")).decode("utf-8")


def decode_state(state: str) -> Optional[str]:
    state_bytes: bytes = decodebytes(state.encode())
    state_arr = state_bytes.decode("utf-8").split("_")
    if len(state_arr) != 2:
        return None
    return state_arr[1]


class MobiWebException(HTTPException, MobiException):
    code = 500
    description = "Unknown Mobi Event Api Exception"


class MobiWebWrongCredentials(MobiWebException):
    code = 403
    description = "Wrong credentials"


class NotAuthenticatedException(MobiWebException):
    code = 401
    description = "User is not authenticated"


@dataclass
class Cookie:
    key: str
    value: str
    max_age: int = 24 * 60 * 60  # 24 hours
    expires: Optional[Union[datetime, int]] = None
    secure: bool = False
    include_subdomains: bool = True


class MobiWebApplication(ABC):
    def __init__(self, application: str, host: str, port: int):
        self.name = application
        self.host = host
        self.port = port

        app_folder = os.path.dirname(os.path.realpath(sys.modules[self.__class__.__module__].__file__))
        template_folder = os.path.join(app_folder, "templates")

        self.app = Flask(f"MobiMobi {application} Application", static_url_path="/public",
                         template_folder=template_folder)
        self.app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0  # Disabling cache for static files

        self.app.secret_key = "befbb3a56871a76f7267c0018cc36c9f0c59eb1c"

        self.app.before_request(self.check_auth)
        self.app.after_request(self.add_cookies_to_response)
        self.app.add_url_rule("/status", "status", self.status, methods=["GET"])

        self.app.add_url_rule("/", "index", self.index, methods=["GET"])
        self.app.add_url_rule("/login", "login", self.login, methods=["GET"])
        self.app.add_url_rule("/logout", "logout", self.logout, methods=["GET"])

        # self.app.add_url_rule("/login/email", "login_email", self.login_email, methods=["POST"])

        self.app.add_url_rule("/login/google", "login_google", self.login_google, methods=["GET"])
        self.app.add_url_rule("/auth/google", "auth_google", self.auth_google, methods=["GET"])

        self.app.add_url_rule("/login/auth", "login_auth", self.login_auth, methods=["POST"])

        # self.app.add_url_rule("/auth/google", "google_auth", self.google_auth, methods=["GET"])
        # self.app.add_url_rule("/google/logout", "google_logout",
        #                       self.google_logout, methods=["GET"])

        self.app.register_error_handler(MobiWebException, self.error_handler)
        self.app.register_error_handler(HTTPException, self.http_exception_handler)

        # What an ugly way to store cache :( Should be refactored in future
        self.token_renewed_at: Dict[str, datetime] = dict()
        self.renew_interval = 5 * 60

    @abstractmethod
    def index(self):
        pass

    def set_cookie(self, cookie: Cookie):
        g.setdefault("cookies", []).append(cookie)

    def add_cookies_to_response(self, response):
        for cookie in g.get("cookies", []):
            # domain is not supported yet
            response.set_cookie(
                key=cookie.key,
                value=cookie.value,
                max_age=cookie.max_age,
                secure=cookie.secure
            )
        return response

    def is_user_authenticated(self):
        if self.token is None or not self.auth_dao.check_session_token(self.token):
            return False
        return True

    def run(self):
        if get_env() in (Env.PROD, Env.PREPROD, Env.GITLAB):
            serve(self.app, host=self.host, port=self.port, threads=32,
                  max_request_header_size=10*1024, max_request_body_size=10*1024,
                  channel_timeout=5, cleanup_interval=3, connection_limit=1024,
                  ident=self.name)
        else:
            self.app.run(self.host, self.port)

    def login(self):
        url = request.args.get("redirect_url", url_for("index"))

        msg = special_chars(request.args.get("msg", ""))

        # Not sure if we really need this. Replaced with is_user_authenticated check
        # if AUTH_TOKEN_KEY in session:
        #     return redirect(url, 302)

        if self.is_user_authenticated():
            return redirect(url, 302)

        return LOGIN_FORM\
            .replace("%ERROR_MESSAGE%", msg)\
            .replace("%GOOGLE_OAUTH_LINK%", url_for("login_google"))\
            .replace("%GOOGLE_OAUTH_IMG%", "/static/img/common/google/1x/btn_google_signin_dark_focus_web.png")

    @no_cache
    def login_auth(self):
        email = request.form.get("email")
        password = request.form.get("password")

        if not self.auth_dao.check_user_password(email, password):
            raise MobiWebWrongCredentials()

        token = self.auth_dao.create_session_token(email, datetime.utcnow() + timedelta(hours=24))

        self.set_cookie(Cookie(key="token", value=token))

        return redirect(url_for("index"), code=302)

    @no_cache
    def login_google(self):
        redirect_url = request.url_root + "auth/google"
        if get_env() == Env.PROD:
            redirect_url = redirect_url.replace("http://", "https://")

        oauth_session = OAuth2Session(CLIENT_ID, CLIENT_SECRET,
                                      scope=AUTHORIZATION_SCOPE,
                                      redirect_uri=redirect_url)

        state = encode_state("MY STATE")

        uri, state = oauth_session.create_authorization_url(AUTHORIZATION_URL, state=state)

        session[AUTH_STATE_KEY] = state
        session.permanent = True

        return redirect(uri, code=302)

    @no_cache
    def auth_google(self):
        req_state = request.args.get("state", default=None, type=None)

        if req_state != session[AUTH_STATE_KEY]:
            response = make_response("Invalid state perimeter", 401)
            return response

        redirect_url = request.url_root + "auth/google"
        if get_env() == Env.PROD:
            redirect_url = redirect_url.replace("http://", "https://")

        oauth_session = OAuth2Session(CLIENT_ID, CLIENT_SECRET,
                                      scope=AUTHORIZATION_SCOPE,
                                      state=session[AUTH_STATE_KEY],
                                      redirect_uri=redirect_url)

        oauth_tokens = oauth_session.fetch_access_token(
            ACCESS_TOKEN_URI,
            authorization_response=request.url
        )

        session[AUTH_TOKEN_KEY] = oauth_tokens

        # Fetching user info and saving session info in DAO and cookies
        credentials = google.oauth2.credentials.Credentials(
            oauth_tokens["access_token"],
            refresh_token=oauth_tokens["refresh_token"],
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            token_uri=ACCESS_TOKEN_URI
        )

        oauth2_client = googleapiclient.discovery.build(
            "oauth2", "v2", credentials=credentials, cache_discovery=False
        )

        user_info = oauth2_client.userinfo().get().execute()

        if not self.auth_dao.check_user_exists(user_info["email"]):
            raise MobiWebWrongCredentials()

        token = self.auth_dao.create_session_token(user_info["email"], datetime.utcnow() + timedelta(hours=24))

        self.set_cookie(Cookie(key="token", value=token))

        return redirect(url_for("index"), code=302)

    def delete_cookie_if_exists(self, key):
        if key in request.cookies:
            self.set_cookie(Cookie(key=key, value="", expires=0, max_age=0))

    @no_cache
    def logout(self):
        session.pop(AUTH_TOKEN_KEY, None)
        session.pop(AUTH_STATE_KEY, None)
        if self.token:
            try:
                self.auth_dao.delete_session_token(self.token)
            except Exception:
                pass

        self.delete_cookie_if_exists("token")

        return redirect(url_for("login", msg="byebye"), code=302)

    def check_auth(self):
        allow_access_to = (
            "/status",
            "/login",
            "/auth/",
            "/logout",
            "/public/img/google/",  # Looks ugly
            "/unsubscribe"
        )

        for url_prefix in allow_access_to:
            if request.path.startswith(url_prefix):
                return

        if not self.is_user_authenticated():
            raise NotAuthenticatedException()

        token = self.token

        renewed_at = self.token_renewed_at.get(token)
        if renewed_at is None or renewed_at < datetime.utcnow() - timedelta(seconds=self.renew_interval):
            self.auth_dao.renew_session_token(token, datetime.utcnow() + timedelta(hours=24))
            self.token_renewed_at[token] = datetime.utcnow()
            self.set_cookie(Cookie(
                key="token",
                value=token,
                max_age=24 * 60 * 60,  # 24 hours
                secure=False
            ))

    def status(self):
        return self.format_result()

    @property
    @abstractmethod
    def config(self) -> WebBaseConfig:
        pass

    @property
    def audience_dao(self) -> MobiAudienceDao:
        return get_audience_dao(self.config)

    @property
    def auth_dao(self) -> MobiAuthDao:
        return get_auth_dao(self.config)

    @property
    def cache_dao(self) -> MobiCache:
        return get_cache_dao(self.config)

    @property
    def catalog_dao(self) -> MobiCatalogDao:
        return get_catalog_dao(self.config)

    @property
    def metrics_dao(self) -> MobiMetricsDao:
        return get_metrics_dao(self.config)

    @property
    def search_dao(self) -> MobiSearchDao:
        return get_search_dao(self.config)

    @property
    def settings_dao(self) -> MobiSettingsDao:
        return get_settings_dao(self.config)

    @property
    def user_settings_dao(self) -> MobiUserSettingsDao:
        return get_user_settings_dao(self.config)

    @property
    def token(self) -> Optional[str]:
        return request.headers.get("Auth-Token") or request.cookies.get("token")

    def error_handler(self, e):
        return self.format_result(None, None, e)

    def http_exception_handler(self, e: HTTPException):
        return self.format_result(None, None, e)

    @abstractmethod
    def render_error_page(self, exception: MobiWebException):
        pass

    def format_result(self, data: Optional[Union[dict, list]] = None,
                      meta: Optional[Union[dict, list]] = None,
                      exception: Optional[MobiWebException] = None):
        if not request.path.startswith("/api/") and exception:
            # Redirect to login page with error message
            if type(exception) == MobiWebWrongCredentials:
                return redirect(url_for("login", msg="wrong_credentials"), code=302)

            if type(exception) == NotAuthenticatedException:
                return redirect(url_for("login", msg="not_authenticated"), code=302)

            return self.render_error_page(exception)

        code = 200
        body = {
            "status": "OK"
        }
        if meta:
            body["meta"] = meta
        if data is not None:
            body["data"] = data
        if exception:
            body["status"] = "ERROR"
            body["error"] = {
                "code": exception.code,
                "description": exception.description,
                "message": str(exception)
            }
            code = exception.code

        return json.dumps(body), code, {'Content-Type': 'application/json; charset=utf-8'}
