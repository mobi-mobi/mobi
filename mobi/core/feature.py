from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
from typing import Any, List


class FeatureSource(Enum):
    PRODUCT = 0
    EVENT = 1


class FeatureType(Enum):
    STATIC = 0
    DYNAMIC = 1


class FeatureMode(Enum):
    TRAINING = 0
    INFERENCE = 1


@dataclass
class FeatureSpec:
    name: str
    feature_source: FeatureSource
    feature_type: FeatureType


class Feature(ABC):
    def __init__(self, name: str, multi: bool = False, feature_mode: FeatureMode = FeatureMode.TRAINING,
                 multi_length: int = None):

        if not isinstance(name, str) or not name:
            raise ValueError("Name parameter should be a non-empty string")
        if not isinstance(multi, bool):
            raise ValueError("multi parameter should be boolean")
        if not isinstance(feature_mode, FeatureMode):
            raise ValueError("feature_mode parameter should be a valid FeatureMode instance")

        self.name = name
        self.multi = multi
        self.feature_mode = feature_mode
        self.multi_length = multi_length

        if multi:
            if not isinstance(multi_length, int) or multi_length < 1:
                raise ValueError(f"Feature {self.name} has to have multi_length parameter defined")
        else:
            if multi_length is not None:
                raise ValueError(f"Feature {self.name} is not multi-value. multi_length parameter should be None")

    def set_mode(self, feature_mode: FeatureMode):
        self.feature_mode = feature_mode

    @abstractmethod
    def _parse_single(self, value: Any) -> Any:
        pass

    def _limit_multi(self, value: List[Any]) -> List[Any]:
        if len(value) > self.multi_length:
            return value[0:self.multi_length]
        elif len(value) == self.multi_length:
            return value
        else:
            return value + [None] * (self.multi_length - len(value))

    def parse(self, value: Any) -> Any:
        value = self.data_field.parse(value)

        if self.data_field.multi:
            return [self._parse_single(item) for item in self._limit_multi(value)]
        else:
            return self._parse_single(value)


def build_feature(spec: FeatureSpec) -> Feature:
    pass


class CategoricalFeature(Feature):
    def __init__(self, name: str, data_field: DataField, feature_mode: FeatureMode = FeatureMode.TRAINING,
                 multi_length: int = None, build_inverted_vocab: bool = False):
        super().__init__(name, data_field, feature_mode, multi_length)

        self.vocab = dict()
        self.inverted_vocab = None
        if build_inverted_vocab:
            self.inverted_vocab = dict()

    def _parse_single(self, value: Any) -> int:
        # Not thread safe

        if value is None:
            return 0

        if value not in self.vocab:
            if self.feature_mode != FeatureMode.TRAINING:
                raise ValueError(f"Key \"{value}\" was not found in feature \"{self.name}\"")

            k = len(self.vocab) + 1

            self.vocab[value] = k
            if self.inverted_vocab is not None:
                self.inverted_vocab[k] = value

        return self.vocab[value]

    def original_value(self, feature_value: int):
        if self.inverted_vocab is None:
            raise ValueError(f"Feature \"{self.name}\" does not support retrieving original value")

        if feature_value is None:
            return 0

        return self.inverted_vocab[feature_value]