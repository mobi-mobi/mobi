from enum import Enum
from typing import Optional, Union


class MobiEnum(Enum):
    @classmethod
    def _unknown_value(cls) -> Optional['MobiEnum']:
        return None

    @classmethod
    def parse(cls, value: Union[int, str, 'MobiEnum']):
        def return_unknown() -> Optional['MobiEnum']:
            if cls._unknown_value() is None:
                raise ValueError(f"Can not parse value \"{value}\"") from None
            return cls._unknown_value()

        def enum_from_int(value_int: int) -> 'MobiEnum':
            try:
                inst = cls
                inst.__init__(value)
                return cls(value_int)
            except ValueError:
                return return_unknown()

        if isinstance(value, MobiEnum):
            return value
        if isinstance(value, int):
            return enum_from_int(value)
        if isinstance(value, str):
            try:
                value_int = int(value)
                if value == str(value_int):
                    return enum_from_int(value_int)
            except ValueError:
                pass

            try:
                return cls[value.upper()]
            except KeyError:
                return return_unknown()

        return return_unknown()
