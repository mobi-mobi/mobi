class PriceType:
    def __init__(self, real: int, fractional: int = 0):
        self.real = real
        self.fractional = fractional

    @classmethod
    def _fractional_to_str(cls, fractional: int) -> str:
        s = str(fractional)
        if len(s) < 2:
            return "0" + s
        if len(s) > 2:
            return s[0:2]
        return s

    def __str__(self):
        return f"{self.real}.{self._fractional_to_str(self.fractional)}"

    def __repr__(self):
        return f"<PriceType: {str(self)}>"

    def __eq__(self, other):
        return str(self) == str(other)

    @classmethod
    def parse(cls, price: str) -> 'PriceType':
        try:
            real, fractional = price.split(".")
        except Exception:
            raise ValueError(f"Can not parse price \"{price}\"") from None

        if len(fractional) < 2:
            fractional += "0"
        try:
            if int(real) < 0 or int(fractional) < 0:
                raise ValueError()
        except ValueError:
            raise ValueError(f"Can not parse price \"{price}\"") from None

        return PriceType(int(real), int(fractional))
