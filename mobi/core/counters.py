import pickle

from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Dict, Hashable, List, Optional, Type, Union

from mobi.core.models import EventType


@dataclass(frozen=True)
class DynamicCounterSpecs:
    name: str
    time_window: timedelta
    preciseness: timedelta
    cache_for: Optional[timedelta] = None

    def __post_init__(self):
        assert isinstance(self.name, str) and self.name

        assert isinstance(self.time_window, timedelta)
        assert int(self.time_window.total_seconds()) > 0

        assert isinstance(self.preciseness, timedelta)
        assert int(self.preciseness.total_seconds()) > 0

        assert int(self.time_window.total_seconds()) % int(self.preciseness.total_seconds()) == 0

        if self.cache_for is not None:
            assert isinstance(self.cache_for, timedelta)
            assert int(self.cache_for.total_seconds()) > 0


class DynamicCounter:
    def __init__(self, counter_specs: DynamicCounterSpecs):
        self.name = counter_specs.name
        self.time_window = int(counter_specs.time_window.total_seconds())
        self.preciseness = int(counter_specs.preciseness.total_seconds())

        self.cache_for: Optional[timedelta] = None
        if counter_specs.cache_for is not None:
            self.cache_for = int(counter_specs.cache_for.total_seconds())

        # values list will contain pairs (timestamp, value). Sorted by timestamp in ascending order
        self.values: List[List[int, Union[float, int]]] = []

        self.last_value_ts = None
        self.last_value = None  # Used as a cache
        self.current_sum = 0

    @classmethod
    def _now_ts_from_now_dt(cls, now_dt: Optional[Union[datetime, int]]) -> int:
        if now_dt is None:
            return int(datetime.utcnow().timestamp())
        else:
            return now_dt if isinstance(now_dt, int) else int(now_dt.timestamp())

    def cached_value(self, now_dt: Optional[Union[datetime, int]] = None) -> Optional[Union[float, int]]:
        now_ts = self._now_ts_from_now_dt(now_dt)
        if self.last_value is not None and self.cache_for is not None and now_ts - self.last_value_ts < self.cache_for:
            return self.last_value

        return None

    def value(self, now_dt: Optional[Union[datetime, int]] = None) -> Union[float, int]:
        now_ts = self._now_ts_from_now_dt(now_dt)

        cached_value = self.cached_value(now_ts)
        if cached_value is not None:
            return cached_value

        while self.values and self.values[0][0] <= now_ts - self.time_window - self.preciseness:
            _, val = self.values.pop(0)
            self.current_sum -= val

        value = self.current_sum

        if self.values:
            latest_ts, latest_val = self.values[0]
            if latest_ts < now_ts - self.time_window and latest_val > 0:
                is_int = isinstance(value, int)
                value -= latest_val * ((now_ts - self.time_window - latest_ts) / self.preciseness)
                if is_int:
                    value = int(value)

        if self.cache_for is not None:
            self.last_value = value
            self.last_value_ts = now_ts

        return value

    def inc(self, value: Union[float, int] = 1, dt: Optional[Union[datetime, int]] = None):
        assert isinstance(value, int) and value > 0

        if dt is None:
            ts = int(datetime.utcnow().timestamp())
        else:
            ts = dt if isinstance(dt, int) else int(dt.timestamp())

        ts = ts - (ts % self.preciseness)

        if self.values and self.values[-1][0] >= ts:
            self.values[-1][1] += value
        else:
            self.values.append([ts, value])

        self.current_sum += value

        while self.values and self.values[0][0] <= ts - self.time_window - self.preciseness:
            self.values.pop(0)

    def as_bytes(self) -> bytes:
        return pickle.dumps({
            "vv": self.values,
            "cs": self.current_sum
        })

    def refresh_from_bytes(self, bb: bytes):
        dd = pickle.loads(bb)
        self.values, self.current_sum = dd["vv"], dd["cs"]

        self.last_value_ts = None
        self.last_value = None


class ComplexDynamicCounter:
    DEFAULT_COUNTER_CLASS: Type[DynamicCounter] = DynamicCounter

    def __init__(self, counter_specs: DynamicCounterSpecs, counter_class: Type[DynamicCounter] = None):
        self.counter_class = counter_class or self.DEFAULT_COUNTER_CLASS
        self.counter_specs = counter_specs
        self.counters: Dict[Hashable, DynamicCounter] = dict()

    def value(self, key: Hashable, now_dt: Optional[Union[datetime, int]] = None) -> Union[float, int]:
        if key not in self.counters:
            return 0
        else:
            return self.counters[key].value(now_dt)

    def inc(self, key: Hashable, value: Union[float, int], dt: Optional[Union[datetime, int]] = None):
        if key not in self.counters:
            self.counters[key] = self.counter_class(self.counter_specs)
        self.counters[key].inc(value, dt)

    def as_bytes(self, key: Hashable) -> Optional[bytes]:
        if key not in self.counters:
            return None

        return self.counters[key].as_bytes()

    def refresh_from_bytes(self, key: Hashable, bb: Optional[bytes]):
        if bb is not None:
            if key not in self.counters:
                self.counters[key] = self.counter_class(self.counter_specs)
            self.counters[key].refresh_from_bytes(bb)


PRODUCT_EVENTS_COUNTERS_SPECS = {
    EventType.PRODUCT_VIEW: {
        "product_views_1h": DynamicCounterSpecs(name="product_views_1h", time_window=timedelta(hours=1),
                                                preciseness=timedelta(minutes=2)),
        "product_views_24h": DynamicCounterSpecs(name="product_views_24h", time_window=timedelta(hours=24),
                                                 preciseness=timedelta(minutes=10)),
    },
    EventType.PRODUCT_TO_BASKET: {
        "product_to_basket_1h": DynamicCounterSpecs(name="product_to_basket_1h", time_window=timedelta(hours=1),
                                                    preciseness=timedelta(minutes=2)),
        "product_to_basket_24h": DynamicCounterSpecs(name="product_to_basket_24h", time_window=timedelta(hours=24),
                                                     preciseness=timedelta(minutes=10)),
    },
    EventType.PRODUCT_TO_WISHLIST: {
        "product_to_wishlist_1h": DynamicCounterSpecs(name="product_to_wishlist_1h", time_window=timedelta(hours=1),
                                                      preciseness=timedelta(minutes=2)),
        "product_to_wishlist_24h": DynamicCounterSpecs(name="product_to_wishlist_24h", time_window=timedelta(hours=24),
                                                       preciseness=timedelta(minutes=10)),
    },
}

BRAND_EVENTS_COUNTERS_SPECS = {
    EventType.PRODUCT_VIEW: {
        "brand_views_1h": DynamicCounterSpecs(name="brand_views_1h", time_window=timedelta(hours=1),
                                              preciseness=timedelta(minutes=2)),
        "brand_views_24h": DynamicCounterSpecs(name="brand_views_24h", time_window=timedelta(hours=24),
                                               preciseness=timedelta(minutes=10)),
    },
    EventType.PRODUCT_TO_BASKET: {
        "brand_to_basket_1h": DynamicCounterSpecs(name="brand_to_basket_1h", time_window=timedelta(hours=1),
                                                  preciseness=timedelta(minutes=2)),
        "brand_to_basket_24h": DynamicCounterSpecs(name="brand_to_basket_24h", time_window=timedelta(hours=24),
                                                   preciseness=timedelta(minutes=10)),
    },
    EventType.PRODUCT_TO_WISHLIST: {
        "brand_to_wishlist_1h": DynamicCounterSpecs(name="brand_to_wishlist_1h", time_window=timedelta(hours=1),
                                                    preciseness=timedelta(minutes=2)),
        "brand_to_wishlist_24h": DynamicCounterSpecs(name="brand_to_wishlist_24h", time_window=timedelta(hours=24),
                                                     preciseness=timedelta(minutes=10)),
    }
}


def create_counters(counter_specs: Dict[EventType, Dict[str, DynamicCounterSpecs]]) \
        -> Dict[EventType, Dict[str, ComplexDynamicCounter]]:
    result = {}
    for event_type, counters_dict in counter_specs.items():
        result[event_type] = {
            counter_name: ComplexDynamicCounter(counter_specs_)
            for counter_name, counter_specs_ in counters_dict.items()
        }

    return result


def create_total_counters(counter_specs: Dict[EventType, Dict[str, DynamicCounterSpecs]]) \
        -> Dict[EventType, Dict[str, DynamicCounter]]:
    result = {}
    for event_type, counters_dict in counter_specs.items():
        result[event_type] = {
            counter_name: DynamicCounter(counter_specs_)
            for counter_name, counter_specs_ in counters_dict.items()
        }

    return result


def create_product_counters():
    return create_counters(PRODUCT_EVENTS_COUNTERS_SPECS)


def create_product_total_counters():
    return create_total_counters(PRODUCT_EVENTS_COUNTERS_SPECS)


def create_brand_counters():
    return create_counters(BRAND_EVENTS_COUNTERS_SPECS)


def create_brand_total_counters():
    return create_total_counters(BRAND_EVENTS_COUNTERS_SPECS)
