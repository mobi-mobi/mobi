from hashlib import sha1
from typing import Any, Dict, Optional, Union

from mobi.core.common import MobiEnum


class Population(MobiEnum):
    UNKNOWN = 0

    REFERENCE = 1
    TARGET = 2
    TEST = 3

    @classmethod
    def _unknown_value(cls) -> Optional['Population']:
        return Population.UNKNOWN


def _user_space_size() -> int:
    return 10 ** 9


def user_hash(uid: Any) -> int:
    return int(sha1(bytes("mobimobi" + str(uid) + "tech", "utf-8")).hexdigest(), 16) % _user_space_size()


class PopulationSetup:
    def __init__(self, populations: Dict[Population, Union[int, float]]):
        self.populations = {}
        self.pop_intervals = []
        self.total_size = 0.0

        self.update_populations(populations)

    def update_populations(self, populations: Dict[Population, Union[int, float]]):
        self._assert_populations(populations)

        self.populations = {population: float(size)
                            for population, size in populations.items() if size > 0}

        self.total_size = sum(self.populations.values())

        self._init_intervals()

    @staticmethod
    def _assert_populations(populations: Dict[Population, Union[int, float]]):
        if len(populations) < 1:
            raise ValueError("At least one population is expected")

        if [p for p in populations if populations[p] < 0]:
            raise ValueError("Can't process negative population")

        if not [population for population, size in populations.items() if size > 0]:
            raise ValueError("At least one non-empty population is expected")

    def _init_intervals(self) -> None:
        scale = float(_user_space_size()) / sum([size for _, size in self.populations.items()])
        pop_intervals = []
        current_start = 0.0
        for population in sorted(self.populations, key=lambda p: p.value):
            pop_intervals.append([current_start, population])
            current_start += scale * self.populations[population]

        self.pop_intervals = pop_intervals

    def population(self, uid: Any) -> Population:
        _user_hash = user_hash(uid)
        for starts_from, population in reversed(self.pop_intervals):
            if _user_hash >= starts_from:
                return population
        return self.pop_intervals[-1][1]

    def __getitem__(self, uid: Any) -> Population:
        return self.population(uid)
