import logging


logging.basicConfig(format="%(asctime)-15s %(levelname)s : %(message)s")


def get_logger() -> logging.Logger:
    return logging.getLogger("mobi")
