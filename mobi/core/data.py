from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import Any, Dict, List, Union


class DataType(Enum):
    # Simple types
    INT = 1
    FLOAT = 2
    STR = 3
    BOOL = 4
    DATE = 5


@dataclass(frozen=True)
class DataFieldSpec:
    data_type: DataType
    optional: bool = False
    default: Any = None
    multi: bool = False

    def __post_init__(self):
        if not isinstance(self.data_type, DataType):
            raise ValueError(f"DataField data type must be an instance of DataType class")
        if not isinstance(self.optional, bool):
            raise ValueError("\"optional\" argument must be a boolean")
        if not isinstance(self.multi, bool):
            raise ValueError(f"\"multi\" argument must be a boolean")
        if self.multi and self.default is not None:
            raise ValueError(f"\"multi\" data field can not have default value")
        if self.optional and self.default:
            raise ValueError(f"\"optional\" feature can not have default value")


@dataclass(frozen=True)
class DataObjectSpec:
    data_fields: Dict[str, DataFieldSpec]

    def __post_init__(self):
        if not isinstance(self.data_fields, dict) or not self.data_fields:
            raise ValueError("data_fields must be a non-empty dict")
        for name, df in self.data_fields.items():
            if not isinstance(name, str) or not name.strip() or name != name.strip():
                raise ValueError("Field names must be non-empty strings")
            if not isinstance(df, DataFieldSpec):
                raise ValueError("data_fields elements must me instances of DataFieldSpec class")


def _parse_single_data_field(spec: DataFieldSpec, value: Any) -> Any:
    def _raise(msg: str):
        raise ValueError(f"Can not parse data field: {msg}")

    if spec.data_type == DataType.INT:
        if not isinstance(value, int):
            _raise(f"int value expected, but \"{type(value)}\" was provided")
        return value

    elif spec.data_type == DataType.FLOAT:
        if not isinstance(value, float) and not isinstance(value, int):
            _raise(f"int or float types were expected, but \"{type(value)}\" was provided")
        return value

    elif spec.data_type == DataType.STR:
        if not isinstance(value, str):
            _raise(f"string value was expected, but \"{type(value)}\" was provided")
        return value

    elif spec.data_type == DataType.BOOL:
        if not isinstance(value, bool):
            _raise(f"boolean value was expected, but \"{type(value)}\" was provided")
        return value

    elif spec.data_type == DataType.DATE:
        if isinstance(value, datetime):
            return value
        elif isinstance(value, str):
            supported_formats = ("%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M", "%Y-%m-%d")
            for _format in supported_formats:
                try:
                    value = datetime.strptime(value, _format)
                    return value
                except ValueError:
                    pass
            _raise(f"can not parse date \"{value}\". Unsupported format. "
                   f"Please, use one of these formats: {supported_formats}")
        else:
            _raise(f"date field should be either string or datetime, but \"{type(value)}\" was provided")

    else:
        raise ValueError(f"Unknown data type: {spec.data_type}")


def parse_data_field(spec: DataFieldSpec, value: Any) -> Any:
    if value is None:
        if spec.default is not None:
            value = spec.default

    if value is None or (spec.multi and not value):
        if not spec.optional:
            raise ValueError(f"Data Field is not optional. Some non-empty value should be passed")
        return None

    if spec.multi:
        if not isinstance(value, list):
            raise ValueError(f"Field is multi-value. A list of values was expected.")
        return [_parse_single_data_field(spec, item) for item in value]
    else:
        return _parse_single_data_field(spec, value)


def parse_data_object(data_object: DataObjectSpec, value: Dict[str, Any]) -> Dict[str, Any]:
    if not isinstance(value, dict):
        raise ValueError(f"Data Object must be a dictionary. \"{type(value)}\" provided.")

    result = {}

    unknown_fields = set(value.keys()).difference(data_object.data_fields.keys())
    if unknown_fields:
        raise ValueError(f"Unknown field(s): \"{unknown_fields}\"")

    for key, data_field in data_object.data_fields.items():
        try:
            result[key] = parse_data_field(data_field, value.get(key))
        except ValueError as e:
            raise ValueError(f"Can not parse field \"{key}\": {str(e)}") from None

    return result


class FeatureMode(Enum):
    TRAINING = 0
    INFERENCE = 1


class Feature(ABC):
    def __init__(self, name: str, multi: bool = False, feature_mode: FeatureMode = FeatureMode.TRAINING,
                 multi_length: int = None):

        if not isinstance(name, str) or not name:
            raise ValueError("Name parameter should be a non-empty string")
        if not isinstance(multi, bool):
            raise ValueError("multi parameter should be boolean")
        if not isinstance(feature_mode, FeatureMode):
            raise ValueError("feature_mode parameter should be a valid FeatureMode instance")

        self.name = name
        self.multi = multi
        self.feature_mode = feature_mode
        self.multi_length = multi_length

        if multi:
            if not isinstance(multi_length, int) or multi_length < 1:
                raise ValueError(f"Feature {self.name} has to have multi_length parameter defined")
        else:
            if multi_length is not None:
                raise ValueError(f"Feature {self.name} is not multi-value. multi_length parameter should be None")

    def set_mode(self, feature_mode: FeatureMode):
        self.feature_mode = feature_mode

    @abstractmethod
    def _parse_single(self, value: Any) -> Any:
        pass

    def _limit_multi(self, value: List[Any]) -> List[Any]:
        if len(value) > self.multi_length:
            return value[0:self.multi_length]
        elif len(value) == self.multi_length:
            return value
        else:
            return value + [None] * (self.multi_length - len(value))

    def parse(self, value: Any) -> Any:
        value = self.data_field.parse(value)

        if self.data_field.multi:
            return [self._parse_single(item) for item in self._limit_multi(value)]
        else:
            return self._parse_single(value)


class CategoricalFeature(Feature):
    def __init__(self, name: str, data_field: DataField, feature_mode: FeatureMode = FeatureMode.TRAINING,
                 multi_length: int = None, build_inverted_vocab: bool = False):
        super().__init__(name, data_field, feature_mode, multi_length)

        self.vocab = dict()
        self.inverted_vocab = None
        if build_inverted_vocab:
            self.inverted_vocab = dict()

    def _parse_single(self, value: Any) -> int:
        # Not thread safe

        if value is None:
            return 0

        if value not in self.vocab:
            if self.feature_mode != FeatureMode.TRAINING:
                raise ValueError(f"Key \"{value}\" was not found in feature \"{self.name}\"")

            k = len(self.vocab) + 1

            self.vocab[value] = k
            if self.inverted_vocab is not None:
                self.inverted_vocab[k] = value

        return self.vocab[value]

    def original_value(self, feature_value: int):
        if self.inverted_vocab is None:
            raise ValueError(f"Feature \"{self.name}\" does not support retrieving original value")

        if feature_value is None:
            return 0

        return self.inverted_vocab[feature_value]


class BoundedCategoricalFeature(CategoricalFeature):
    def __init__(self, name: str, data_field: DataField, bounds: List[Union[int, float]], multi_length: int = None):
        super().__init__(name, data_field, FeatureMode.TRAINING, multi_length, build_inverted_vocab=False)

        if not isinstance(bounds, list):
            raise ValueError("\"bounds\" parameter should be a list")
        if any(map(lambda x: not(isinstance(x, int) or isinstance(x, float)), bounds)):
            raise ValueError("One of the bounds is not a numeric value")

        self.bounds = list(sorted(set(bounds)))

        # Initiate ordered int values
        for i in range(0, len(self.bounds) + 1):
            super(BoundedCategoricalFeature, self)._parse_single(i)

        self.feature_mode = FeatureMode.INFERENCE

    def _parse_single(self, value: Any) -> int:
        if value is not None:
            for i, bound in enumerate(self.bounds):
                if bound > value:
                    value = i
                    break
            else:
                value = len(self.bounds)

        return super(BoundedCategoricalFeature, self)._parse_single(value)
