import random

from typing import Any, List

from mobi.core.models import Product
from mobi.core.models.zone import ZoneExtraProductsPosition


def join_evenly(organic: List[Any], extra: List[Any]) -> List[Any]:
    result = []
    while organic and extra:
        extra_to_take = max(1, int(len(extra) / (len(organic) + 1)))
        for _ in range(extra_to_take):
            if extra:
                result.append(extra.pop(0))
        result.append(organic.pop(0))
    if organic:
        result.extend(organic)
    if extra:
        result.extend(extra)
    return result


def join_randomly(organic: List[Any], extra: List[Any]) -> List[Any]:
    random.shuffle(extra)
    return join_evenly(organic, extra)


def merge_organic_and_extra_products(organic_products: List[Product], extra_products: List[Product],
                                     recommendations_num: int, save_organic_extra_products_positions: bool,
                                     extra_products_position: ZoneExtraProductsPosition) -> List[Product]:

    extra_products = [product for product in extra_products if product.active and product.in_stock]
    extra_product_ids = {product.product_id for product in extra_products}

    _organic_products = []
    unique_organic_products_num = 0
    for product in organic_products:
        if unique_organic_products_num + len(extra_products) >= recommendations_num:
            break
        if not product.active or not product.in_stock:
            continue
        if product.product_id not in extra_product_ids:
            unique_organic_products_num += 1
        _organic_products.append(product)
    organic_products = _organic_products
    organic_product_ids = {product.product_id for product in organic_products}

    if save_organic_extra_products_positions:
        extra_products = [product for product in extra_products if product.product_id not in organic_product_ids]
    else:
        organic_products = [product for product in organic_products if product.product_id not in extra_product_ids]

    extra_products = extra_products[0:recommendations_num]

    if extra_products_position == ZoneExtraProductsPosition.BEGINNING:
        return (extra_products + organic_products)[0:recommendations_num]
    elif extra_products_position == ZoneExtraProductsPosition.END:
        return (organic_products + extra_products)[0:recommendations_num]
    elif extra_products_position == ZoneExtraProductsPosition.RANDOM:
        return join_randomly(organic_products, extra_products)
    elif extra_products_position == ZoneExtraProductsPosition.EVENLY:
        return join_evenly(organic_products, extra_products)
    else:
        raise ValueError(f"Unknown merge rule: \"{extra_products_position}\"")
