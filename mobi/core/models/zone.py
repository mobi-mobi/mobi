from datetime import datetime
from typing import Any, Dict, List, Optional

from mobi.core.common.mobienum import MobiEnum
from mobi.core.models._base import MobiModel


class ZoneExtraProductsPosition(MobiEnum):
    BEGINNING = 1
    END = 2
    RANDOM = 3
    EVENLY = 4


class ZoneStatus(MobiEnum):
    ACTIVE = 1
    DISABLED = 2


class ZoneType(MobiEnum):
    PERSONAL_RECOMMENDATIONS = 1
    SIMILAR_PRODUCTS = 2
    BASKET_RECOMMENDATIONS = 3
    MOST_POPULAR = 4


class CampaignStatus(MobiEnum):
    ACTIVE = 1
    DISABLED = 2


class ZoneSettings(MobiModel):
    def __init__(self, recommendations_num: Optional[int] = None, same_brand: Optional[bool] = None,
                 brands: Optional[List[str]] = None, categories: Optional[List[str]] = None,
                 extra_product_ids: Optional[List[str]] = None,
                 extra_products_position: Optional[ZoneExtraProductsPosition] = None,
                 save_organic_extra_products_positions: Optional[bool] = None,
                 dont_show_organic_products: Optional[bool] = None):
        if recommendations_num is not None:
            if not isinstance(recommendations_num, int) or recommendations_num < 1:
                raise ValueError("A number of recommendations should be a positive integer")
        self.recommendations_num = recommendations_num

        self.same_brand = same_brand
        self.brands = brands
        self.categories = categories
        self.extra_product_ids = extra_product_ids
        self.extra_products_position = None if extra_products_position is None \
            else ZoneExtraProductsPosition.parse(extra_products_position)
        self.save_organic_extra_products_positions = save_organic_extra_products_positions
        self.dont_show_organic_products = dont_show_organic_products

    def apply_other(self, zone: 'ZoneSettings'):
        if zone.recommendations_num is not None:
            self.recommendations_num = zone.recommendations_num

        if zone.same_brand is not None:
            self.same_brand = zone.same_brand

        if zone.brands is not None:
            self.brands = zone.brands

        if zone.categories is not None:
            self.categories = zone.categories

        if zone.extra_product_ids is not None:
            self.extra_product_ids = zone.extra_product_ids

        if zone.extra_products_position is not None:
            self.extra_products_position = zone.extra_products_position

        if zone.save_organic_extra_products_positions is not None:
            self.save_organic_extra_products_positions = zone.save_organic_extra_products_positions

        if zone.dont_show_organic_products is not None:
            self.dont_show_organic_products = zone.dont_show_organic_products

    def as_dict(self) -> Dict[str, Any]:
        return {
            "recommendations_num": self.recommendations_num,
            "same_brand": self.same_brand,
            "brands": self.brands,
            "categories": self.categories,
            "extra_product_ids": self.extra_product_ids,
            "extra_products_position": self.extra_products_position,
            "save_organic_extra_products_positions": self.save_organic_extra_products_positions,
            "dont_show_organic_products": self.dont_show_organic_products
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'ZoneSettings':
        inst = super(ZoneSettings, cls).__new__(cls)
        inst.__init__(
            recommendations_num=d.get("recommendations_num"),
            same_brand=d.get("same_brand"),
            brands=d.get("brands"),
            categories=d.get("categories"),
            extra_product_ids=d.get("extra_product_ids"),
            extra_products_position=d.get("extra_products_position"),
            save_organic_extra_products_positions=d.get("save_organic_extra_products_positions"),
            dont_show_organic_products=d.get("dont_show_organic_products")
        )
        return inst


class Campaign(MobiModel):
    def __init__(self, campaign_id: str, name: str, zone_settings: ZoneSettings, description: Optional[str] = None,
                 status: CampaignStatus = CampaignStatus.ACTIVE, start_date: Optional[datetime] = None,
                 end_date: Optional[datetime] = None):
        if not campaign_id or not isinstance(campaign_id, str):
            raise ValueError("Invalid campaign id value")
        if not name or not isinstance(name, str):
            raise ValueError("Invalid name value")
        if not isinstance(zone_settings, ZoneSettings):
            raise ValueError("Invalid zone settings value")

        self.campaign_id = campaign_id
        self.name = name
        self.zone_settings = zone_settings
        self.description = description
        self.status = status
        self.start_date = start_date
        self.end_date = end_date

    def as_dict(self) -> Dict[str, Any]:
        return {
            "campaign_id": self.campaign_id,
            "name": self.name,
            "zone_settings": self.zone_settings.as_dict(),
            "description": self.description,
            "status": self.status,
            "start_date": self.start_date,
            "end_date": self.end_date,
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Campaign':
        inst = super(Campaign, cls).__new__(cls)
        start_date = d.get("start_date")
        if start_date is not None:
            start_date = start_date if isinstance(start_date, datetime) else cls._parse_datetime(start_date)
        end_date = d.get("end_date")
        if end_date is not None:
            end_date = end_date if isinstance(end_date, datetime) else cls._parse_datetime(end_date)
        inst.__init__(
            campaign_id=d["campaign_id"],
            name=d["name"],
            description=d.get("description"),
            status=CampaignStatus.parse(d.get("status", CampaignStatus.ACTIVE)),
            start_date=start_date,
            end_date=end_date,
            zone_settings=ZoneSettings.from_dict(d["zone_settings"])
        )
        return inst


class Zone(MobiModel):
    def __init__(self, zone_id: str, name: str, zone_type: ZoneType, zone_settings: ZoneSettings,
                 description: Optional[str] = None, status: ZoneStatus = ZoneStatus.ACTIVE,
                 campaign_ids: Optional[List[str]] = None):
        if not zone_id or not isinstance(zone_id, str):
            raise ValueError("Zone id must be a non-empty string string")
        if not name or not isinstance(name, str):
            raise ValueError("Name must be a non-empty string")
        if not isinstance(zone_type, ZoneType):
            raise ValueError("Invalid zone type value")
        if not isinstance(zone_settings, ZoneSettings):
            raise ValueError("Invalid zone settings value")
        if not isinstance(zone_settings.recommendations_num, int) or zone_settings.recommendations_num < 1:
            raise ValueError("A number of recommendations to return should be a positive integer")

        self.zone_id = zone_id
        self.name = name
        self.zone_type = ZoneType.parse(zone_type)
        self.description = description
        self.status = ZoneStatus.parse(status)
        self.campaign_ids = campaign_ids or []
        self.zone_settings = zone_settings

    def as_dict(self) -> Dict[str, Any]:
        return {
            "zone_id": self.zone_id,
            "name": self.name,
            "zone_type": self.zone_type,
            "description": self.description,
            "status": self.status,
            "campaign_ids": self.campaign_ids,
            "zone_settings": self.zone_settings.as_dict()
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Zone':
        inst = super(Zone, cls).__new__(cls)
        inst.__init__(
            zone_id=d["zone_id"],
            name=d["name"],
            zone_type=ZoneType.parse(d["zone_type"]),
            description=d.get("description"),
            status=ZoneStatus.parse(d.get("status", ZoneStatus.ACTIVE)),
            campaign_ids=d.get("campaign_ids"),
            zone_settings=ZoneSettings.from_dict(d["zone_settings"])
        )
        return inst
