from datetime import datetime
from typing import Any, Dict, List, Optional

from mobi.core.common.mobienum import MobiEnum
from mobi.core.common.pricetype import PriceType
from mobi.core.models._base import MobiModel


class ProductAgeGroup(MobiEnum):
    ALL_AGES = 0
    ADULT = 1
    TEEN = 2
    KIDS = 3
    NEWBORN = 4


class ProductGender(MobiEnum):
    UNISEX = 0
    FEMALE = 1
    MALE = 2


class Product(MobiModel):
    NOT_COMPARABLE_FIELD_NAMES = {"version", "enriched_tags"}

    IGNORED_API_JSON_FIELDS = {
        "export_to_facebook",
        "export_to_criteo",
        "facebook_properties",
        "criteo_properties",
        "active",
        "version",
        "enriched_tags"
    }

    def __init__(
            self,
            product_id: str,
            title: str,
            brand: str,
            price: PriceType,
            currency: str,

            product_group_id: Optional[str] = None,
            categories: Optional[List[str]] = None,
            color: Optional[str] = None,
            size: Optional[str] = None,
            available_from: Optional[datetime] = None,
            available_until: Optional[datetime] = None,
            description: Optional[str] = None,
            sale_price: Optional[PriceType] = None,
            on_sale_from: Optional[datetime] = None,
            on_sale_until: Optional[datetime] = None,
            tags: Optional[List[str]] = None,
            age_group: ProductAgeGroup = ProductAgeGroup.ALL_AGES,
            gender: ProductGender = ProductGender.UNISEX,

            is_searchable: bool = True,
            is_recommendable: bool = True,

            export_to_facebook: bool = True,
            export_to_criteo: bool = True,
            facebook_properties: Optional[dict] = None,
            criteo_properties: Optional[dict] = None,

            # System attributes
            active: bool = True,
            version: int = None,

            # Enriched attributes
            enriched_tags: Optional[List[str]] = None
    ):
        self.product_id = product_id
        self.title = title
        self.brand = brand
        self.price = price
        self.currency = currency

        self.product_group_id = product_group_id
        self.categories = categories
        self.color = color
        self.size = size
        self.available_from = available_from
        self.available_until = available_until
        self.description = description
        self.sale_price = sale_price
        self.on_sale_from = on_sale_from
        self.on_sale_until = on_sale_until
        self.tags = tags
        self.age_group = age_group
        self.gender = gender

        self.is_searchable = is_searchable
        self.is_recommendable = is_recommendable

        self.export_to_facebook = export_to_facebook
        self.export_to_criteo = export_to_criteo
        self.facebook_properties = facebook_properties
        self.criteo_properties = criteo_properties

        self.active = active
        self.version = version

        self.enriched_tags = enriched_tags

    @property
    def in_stock(self) -> bool:
        if self.available_from is not None and self.available_from > datetime.utcnow():
            return False
        if self.available_until is not None and self.available_until < datetime.utcnow():
            return False
        return True

    @property
    def on_sale(self) -> bool:
        if self.on_sale_from is not None and self.on_sale_from > datetime.utcnow():
            return False
        if self.on_sale_until is not None and self.on_sale_until < datetime.utcnow():
            return False
        return True

    def as_dict(self) -> Dict[str, Any]:
        """
        return {
            "id": self.product_id,
            "title": self.title,
            "in_stock": self.in_stock,
            "active": self.active,
            "brand": self.brand,
            "categories": self.categories,
            "color": self.color,
            "is_new": False,
            "is_exclusive": False,
            "version": self.version,
            "description": self.description
        }
        """
        return {
            "product_id": self.product_id,
            "title": self.title,
            "brand": self.brand,
            "price": self.price,
            "currency": self.currency,

            "product_group_id": self.product_group_id,
            "categories": self.categories,
            "color": self.color,
            "size": self.size,
            "available_from": self.available_from,
            "available_until": self.available_until,
            "description": self.description,
            "sale_price": self.sale_price,
            "on_sale_from": self.on_sale_from,
            "on_sale_until": self.on_sale_until,
            "tags": self.tags,
            "age_group": self.age_group,
            "gender": self.gender,

            "is_searchable": self.is_searchable,
            "is_recommendable": self.is_recommendable,

            "export_to_facebook": self.export_to_facebook,
            "export_to_criteo": self.export_to_criteo,
            "facebook_properties": self.facebook_properties,
            "criteo_properties": self.criteo_properties,

            "active": self.active,
            "version": self.version,

            "enriched_tags": self.enriched_tags
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Product':
        inst = super(Product, cls).__new__(cls)
        inst.__init__(
            product_id=d.get("product_id", d.get("id")),
            title=d.get("title"),
            brand=d.get("brand"),
            price=cls._parse_price_type(d.get("price", "0.0")),
            currency=d.get("currency", "USD"),

            product_group_id=d.get("product_group_id"),
            categories=d.get("categories"),
            color=d.get("color"),
            size=d.get("size"),
            available_from=cls._parse_datetime(d.get("available_from")),
            available_until=cls._parse_datetime(d.get("available_until")),
            description=d.get("description"),
            sale_price=cls._parse_price_type(d.get("sale_price")),
            on_sale_from=cls._parse_datetime(d.get("on_sale_from")),
            on_sale_until=cls._parse_datetime(d.get("on_sale_until")),
            tags=d.get("tags"),
            age_group=ProductAgeGroup.parse(d.get("age_group", ProductAgeGroup.ALL_AGES)),
            gender=ProductGender.parse(d.get("gender", ProductGender.UNISEX)),

            is_searchable=d.get("is_searchable", True),
            is_recommendable=d.get("is_recommendable", True),

            export_to_facebook=d.get("export_to_facebook", True),
            export_to_criteo=d.get("export_to_criteo", True),
            facebook_properties=d.get("facebook_properties"),
            criteo_properties=d.get("criteo_properties"),

            active=d.get("active", True),
            version=d.get("version"),

            enriched_tags=d.get("enriched_tags")
        )
        return inst
