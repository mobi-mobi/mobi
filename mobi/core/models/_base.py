import pickle

from abc import ABC, abstractmethod
from datetime import datetime
from typing import Any, Dict, Optional

from mobi.core.common import MobiEnum
from mobi.core.common.pricetype import PriceType


class MobiModel(ABC):
    NOT_COMPARABLE_FIELD_NAMES = set()
    DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

    IGNORED_API_JSON_FIELDS = set()

    @classmethod
    def _parse_datetime(cls, value: Any) -> Optional[datetime]:
        if value is None:
            return None
        if isinstance(value, datetime):
            return value
        try:
            return datetime.strptime(value, cls.DATE_FORMAT)
        except Exception:
            raise ValueError(f"Can not parse datetime {value}") from None

    @classmethod
    def _parse_price_type(cls, value: Any):
        if value is None:
            return None
        if isinstance(value, PriceType):
            return PriceType.parse(str(value))
        return PriceType.parse(value)

    @classmethod
    def _datetime_to_str(cls, d: datetime) -> str:
        return d.strftime(cls.DATE_FORMAT)

    @abstractmethod
    def as_dict(self) -> Dict[str, Any]:
        pass

    @classmethod
    def _jsonify_value(cls, v) -> Any:
        if isinstance(v, MobiModel):
            return cls._jsonify_value(v.as_dict())
        elif isinstance(v, MobiEnum):
            return v.name.lower()
        elif isinstance(v, PriceType):
            return str(v)
        elif isinstance(v, dict):
            return {k: cls._jsonify_value(v) for k, v in v.items()}
        elif isinstance(v, list):
            return [cls._jsonify_value(item) for item in v]
        elif isinstance(v, datetime):
            return cls._datetime_to_str(v)
        elif v is None or isinstance(v, str) or isinstance(v, int) or isinstance(v, float):
            return v
        raise ValueError(f"Impossible to jsonify a value of type {type(v)}")

    def as_json_dict(self, exclude_ignored_fields: bool = False) -> Dict[str, Any]:
        json = self._jsonify_value(self.as_dict())
        if exclude_ignored_fields:
            for field in self.IGNORED_API_JSON_FIELDS:
                if field in json:
                    del json[field]
        return json

    @classmethod
    def _bsonify_value(cls, v) -> Any:
        if isinstance(v, MobiModel):
            return cls._bsonify_value(v.as_dict())
        elif isinstance(v, MobiEnum):
            return v.value
        elif isinstance(v, PriceType):
            return str(v)
        elif isinstance(v, dict):
            return {k: cls._bsonify_value(v) for k, v in v.items()}
        elif isinstance(v, list):
            return [cls._bsonify_value(item) for item in v]
        elif v is None or isinstance(v, str) or isinstance(v, int) or isinstance(v, float) or isinstance(v, datetime):
            return v
        raise ValueError(f"Impossible to bsonify a value of type {type(v)}")

    def as_bson_dict(self) -> Dict[str, Any]:
        return self._bsonify_value(self.as_dict())

    def as_bytes(self) -> bytes:
        return pickle.dumps(self.as_bson_dict())

    @classmethod
    @abstractmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'MobiModel':
        pass

    @classmethod
    def from_bytes(cls, b: bytes) -> 'MobiModel':
        return cls.from_dict(pickle.loads(b))

    def __str__(self):
        return str(self.as_json_dict())

    def __repr__(self):
        return str(self.as_json_dict())

    def __eq__(self, other):
        self_as_dict = self.as_dict()
        other_as_dict = other.as_dict()
        for key in set(self_as_dict.keys()).union(other_as_dict.keys()):
            if key in self.NOT_COMPARABLE_FIELD_NAMES:
                continue

            if key not in self_as_dict or key not in other_as_dict:
                return False

            if type(self_as_dict[key]) == list:
                if type(other_as_dict[key]) != list:
                    return False

                if len(self_as_dict[key]) != len(other_as_dict[key]):
                    return False

                try:
                    if sorted(self_as_dict[key]) != sorted(other_as_dict[key]):
                        return False
                except TypeError:
                    # Not comparable => not sortable
                    for pos in range(len(self_as_dict[key])):
                        if self_as_dict[key][pos] != other_as_dict[key][pos]:
                            return False

            else:
                if isinstance(self_as_dict[key], datetime):
                    if not isinstance(other_as_dict[key], datetime):
                        return False
                    if self_as_dict[key].strftime("%Y-%m-%d %H:%M:%S") != \
                            other_as_dict[key].strftime("%Y-%m-%d %H:%M:%S"):
                        return False

                elif self_as_dict[key] != other_as_dict[key]:
                    return False

        return True

    def __ne__(self, other):
        return not self.__eq__(other)
