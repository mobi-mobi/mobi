from typing import List, Dict, Any

from mobi.core.common.mobienum import MobiEnum
from mobi.core.models._base import MobiModel
from mobi.core.models.event import Event
from mobi.core.models.recommendation import Recommendation


class AttributionType(MobiEnum):
    @classmethod
    def _unknown_value(cls) -> 'AttributionType':
        return cls.UNKNOWN

    REFERENCE = 1  # Uses reference id - the most precise

    EMPIRICAL = 2  # Uses matching algorithms

    TIME_WINDOW = 3  # Uses time frame windows - tow least precise

    UNKNOWN = 1001


class AttributedEvent(MobiModel):
    def __init__(self, event: Event, attribution_type: AttributionType,
                 attributed_recommendations: List[Recommendation], has_matched_display: bool):
        self.event = event
        self.attribution_type = attribution_type
        self.attributed_recommendations = attributed_recommendations
        self.has_matched_display = has_matched_display

    def as_dict(self) -> Dict[str, Any]:
        return {
            "event": self.event,
            "attribution_type": self.attribution_type,
            "attributed_recommendations": self.attributed_recommendations,
            "has_matched_display": self.has_matched_display
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'AttributedEvent':
        inst = super(AttributedEvent, cls).__new__(cls)
        inst.__init__(
            event=Event.from_dict(d["event"]),
            attribution_type=AttributionType.parse(d["attribution_type"]),
            attributed_recommendations=[Recommendation.from_dict(r) for r in d["attributed_recommendations"]],
            has_matched_display=d.get("has_matched_display", True)  # As this flag was added later
        )
        return inst

    def __eq__(self, other: 'AttributedEvent'):
        return self.event == other.event \
               and self.attribution_type == other.attribution_type \
               and self.has_matched_display == other.has_matched_display \
               and len(self.attributed_recommendations) == len(other.attributed_recommendations) \
               and sorted([str(r.as_json_dict()) for r in self.attributed_recommendations]) \
               == sorted([str(r.as_json_dict()) for r in other.attributed_recommendations])


class AttributedRecommendation(MobiModel):
    def __init__(self, recommendation: Recommendation, attribution_type: AttributionType,
                 attributed_events: List[Event]):
        self.recommendation = recommendation
        self.attribution_type = attribution_type
        self.attributed_events = attributed_events

    def as_dict(self) -> Dict[str, Any]:
        return {
            "recommendation": self.recommendation.as_dict(),
            "attribution_type": self.attribution_type,
            "attributed_events": self.attributed_events
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'AttributedRecommendation':
        inst = super(AttributedRecommendation, cls).__new__(cls)
        inst.__init__(
            recommendation=Recommendation.from_dict(d["recommendation"]),
            attribution_type=AttributionType.parse(d["attribution_type"]),
            attributed_events=[Event.from_dict(e) for e in d["attributed_events"]]
        )
        return inst

    def __eq__(self, other: 'AttributedRecommendation'):
        return self.recommendation == other.recommendation \
               and self.attribution_type == other.attribution_type \
               and len(self.attributed_events) == len(other.attributed_events) \
               and sorted([str(e.as_json_dict()) for e in self.attributed_events]) \
               == sorted([str(e.as_json_dict()) for e in other.attributed_events])
