from typing import Any, Dict, List, Optional

from mobi.core.common.mobienum import MobiEnum
from mobi.core.models._base import MobiModel


class AudienceSplitType(MobiEnum):
    RANDOM = 1

    # TIMEDELTA - BASED SPLITS
    CUSTOMER_SINCE = 10
    NO_ACTIVITY_SINCE = 11
    NO_PURCHASES_SINCE = 12

    # LAST SESSION - BASED SPLITS
    VISITED_SPECIFIC_BRANDS = 20
    VISITED_SPECIFIC_CATEGORIES = 21
    VISITED_SPECIFIC_PRODUCTS = 22

    # ML-SPLITS
    BRANDS_LOVERS = 30
    CATEGORIES_LOVERS = 31
    PRODUCT_IDS_LOVERS = 32


class Audience(MobiModel):
    def __init__(self, audience_id: str, audience_name: str, audience_description: str):
        self.audience_id = audience_id
        self.audience_name = audience_name
        self.audience_description = audience_description

    def as_dict(self) -> Dict[str, Any]:
        return {
            "audience_id": self.audience_id,
            "audience_name": self.audience_name,
            "audience_description": self.audience_description
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Audience':
        return Audience(
            audience_id=d["audience_id"],
            audience_name=d["audience_name"],
            audience_description=d["audience_description"]
        )


class AudiencesUnion(MobiModel):
    def __init__(self, audiences_union_id: str, audiences_union_name: str, audiences_union_description: str,
                 audience_ids: List[str]):
        self.audiences_union_id = audiences_union_id
        self.audiences_union_name = audiences_union_name
        self.audiences_union_description = audiences_union_description
        self.audience_ids = audience_ids

    def as_dict(self) -> Dict[str, Any]:
        return {
            "audiences_union_id": self.audiences_union_id,
            "audiences_union_name": self.audiences_union_name,
            "audiences_union_description": self.audiences_union_description,
            "audience_ids": self.audience_ids
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'AudiencesUnion':
        return AudiencesUnion(
            audiences_union_id=d["audiences_union_id"],
            audiences_union_name=d["audiences_union_name"],
            audiences_union_description=d["audiences_union_description"],
            audience_ids=d["audience_ids"]
        )


class AudienceSplit(MobiModel):
    def __init__(self, split_type: AudienceSplitType, split_id: str, children_split_ids: Optional[List[Optional[str]]],
                 percentiles: Optional[List[float]], quantities: Optional[List[int]],
                 audience_ids: Optional[List[Optional[List[str]]]], brands: Optional[List[str]],
                 categories: Optional[List[str]], product_ids: Optional[List[str]],
                 session_length_min: Optional[int]):
        self.split_type = split_type
        self.split_id = split_id
        self.children_split_ids = children_split_ids
        self.percentiles = percentiles
        self.quantities = quantities
        self.audience_ids = audience_ids
        self.brands = brands
        self.categories = categories
        self.product_ids = product_ids
        self.session_length_min = session_length_min

    def assert_audience_split(self):
        if self.split_type in (AudienceSplitType.RANDOM, AudienceSplitType.BRANDS_LOVERS,
                               AudienceSplitType.CATEGORIES_LOVERS):
            if self.percentiles is None or not self.percentiles:
                raise ValueError("Number of percentile-related parameters should not be zero")
            if self.audience_ids is not None:
                if len(self.percentiles) + 1 != len(self.audience_ids):
                    raise ValueError("Number of percentile-related parameters should be less than number "
                                     "of audiences by 1")

            if any(filter(lambda x: x < 0 or x > 100, self.percentiles)):
                raise ValueError("Percentile-related values should be positive and less or equal to 100")
            if any(filter(lambda p: p[0] > p[1], zip(self.percentiles, self.percentiles[1:]))):
                raise ValueError("Percentile-related values should be sorted")

            if self.quantities is not None:
                raise ValueError("Quantities-related parameter should be empty")

            if self.split_type == AudienceSplitType.BRANDS_LOVERS:
                if self.brands is None or not self.brands:
                    raise ValueError("At least one brand should be defined")
            if self.split_type == AudienceSplitType.CATEGORIES_LOVERS:
                if self.categories is None or not self.categories:
                    raise ValueError("At least one category should be defined")
            if self.split_type == AudienceSplitType.PRODUCT_IDS_LOVERS:
                if self.product_ids is None or not self.product_ids:
                    raise ValueError("At least one product_id should be defined")

        if self.split_type in (AudienceSplitType.CUSTOMER_SINCE, AudienceSplitType.NO_ACTIVITY_SINCE,
                               AudienceSplitType.NO_PURCHASES_SINCE):
            if self.quantities is None or not self.quantities:
                raise ValueError("Number of quantity-related parameters should not be zero")

            if self.audience_ids is not None:
                if len(self.quantities) + 1 != len(self.audience_ids):
                    raise ValueError("Number of quantity-related parameters should be less than number "
                                     "of audiences by 1")

            if any(filter(lambda x: x < 0, self.quantities)):
                raise ValueError("Quantity-related values should be positive or equal to 0")
            if any(filter(lambda a, b: a > b, *zip(self.quantities, self.quantities[1:]))):
                raise ValueError("Quantity-related values should be sorted")

            if self.percentiles is not None:
                raise ValueError("Percentile-related parameter should be empty")

        if self.split_type in (AudienceSplitType.VISITED_SPECIFIC_BRANDS, AudienceSplitType.VISITED_SPECIFIC_CATEGORIES,
                               AudienceSplitType.VISITED_SPECIFIC_PRODUCTS):
            if self.session_length_min is None or self.session_length_min <= 0:
                raise ValueError("Session length should be positive")

            if self.brands is None or not self.brands:
                raise ValueError("At least one brand should be provided")
            if self.categories is None or not self.categories:
                raise ValueError("At least one category should be provided")
            if self.product_ids is None or not self.product_ids:
                raise ValueError("At least one product_id should be provided")

    def as_dict(self) -> Dict[str, Any]:
        return {
            "split_type": self.split_type,
            "split_id": self.split_id,
            "children_split_ids": self.children_split_ids,
            "percentiles": self.percentiles,
            "quantities": self.quantities,
            "audience_ids": self.audience_ids,
            "brands": self.brands,
            "categories": self.categories,
            "product_ids": self.product_ids,
            "session_length_min": self.session_length_min
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'AudienceSplit':
        return AudienceSplit(
            split_type=AudienceSplitType.parse(d["split_type"]),
            split_id=d["split_id"],
            children_split_ids=d.get("children_split_ids"),
            percentiles=d.get("percentiles"),
            quantities=d.get("quantities"),
            audience_ids=d.get("audience_ids"),
            brands=d.get("brands"),
            categories=d.get("categories"),
            product_ids=d.get("product_ids"),
            session_length_min=d.get("session_length_min")
        )


class AudienceSplitTree(MobiModel):
    def __init__(self, split_tree_id: str, name: str, description: str, root_split_id: Optional[str],
                 audience_splits: List[AudienceSplit]):
        self.split_tree_id = split_tree_id
        self.name = name
        self.description = description
        self.root_split_id = root_split_id
        self.audience_splits = audience_splits

    def assert_audience_split_tree(self):
        if self.audience_splits:
            split_ids = set(a.split_id for a in self.audience_splits)
            if self.root_split_id is None or self.root_split_id not in split_ids:
                raise ValueError("Root split is is not defined or wrong")
            if len(split_ids) != len(list(a.split_id for a in self.audience_splits)):
                raise ValueError("There are duplicates in auditory split ids")
            for split in self.audience_splits:
                split.assert_audience_split()

            # Checking the structure
            splits: Dict[str, AudienceSplit] = {a.split_id: a for a in self.audience_splits}
            visited_splits = set()
            splits_to_visit = [self.root_split_id]

            while splits_to_visit:
                next_split = splits_to_visit.pop(0)
                if next_split in visited_splits:
                    raise ValueError(f"Tree structure is wrong: split {next_split} is presented at least two times")
                visited_splits.add(next_split)
                if next_split not in splits:
                    raise ValueError(f"Unknown children split id found: {next_split}")
                if splits[next_split].children_split_ids:
                    for children_split_id in splits[next_split].children_split_ids:
                        if children_split_id is not None:
                            splits_to_visit.append(children_split_id)

            not_visited_splits = set(splits.keys()).difference(visited_splits)
            if not_visited_splits:
                raise ValueError(f"Can not reach {', '.join(map(str, not_visited_splits))} split ids - make sure you "
                                 f"pass a valid tree")

        else:
            if self.root_split_id:
                raise ValueError("Root split id is defined, but splits set is empty")

    def as_dict(self) -> Dict[str, Any]:
        return {
            "split_tree_id": self.split_tree_id,
            "name": self.name,
            "description": self.description,
            "root_split_id": self.root_split_id,
            "audience_splits": self.audience_splits
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'AudienceSplitTree':
        return AudienceSplitTree(
            split_tree_id=d["split_tree_id"],
            name=d["name"],
            description=d["description"],
            root_split_id=d["root_split_id"],
            audience_splits=[AudienceSplit.from_dict(a) for a in d["audience_splits"]]
        )
