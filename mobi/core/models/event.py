from datetime import datetime
from typing import Any, Dict, List, Optional, Union

from mobi.core.common.mobienum import MobiEnum
from mobi.core.models._base import MobiModel
from mobi.core.population import Population


class EventType(MobiEnum):
    # Unknown event
    UNKNOWN = 0

    # User has opened a product page
    PRODUCT_VIEW = 1

    # User has added a product to they wishlist
    PRODUCT_TO_WISHLIST = 2

    # User has added a product to a basket
    PRODUCT_TO_BASKET = 3

    # User has bought a product
    PRODUCT_SALE = 1001

    @classmethod
    def _unknown_value(cls) -> Optional['MobiEnum']:
        return EventType.UNKNOWN


class EventPlatform(MobiEnum):
    OTHER = 0
    IOS = 1
    ANDROID = 2
    WEB = 3


class EventBasketItem(MobiModel):
    def __init__(self, product_id: str, product_version: Optional[int], product_quantity: int):
        self.product_id = product_id
        self.product_version = product_version
        self.product_quantity = product_quantity

    def as_dict(self) -> Dict[str, Any]:
        return {
            "product_id": self.product_id,
            "product_version": self.product_version,
            "product_quantity": self.product_quantity
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'EventBasketItem':
        inst = super(EventBasketItem, cls).__new__(cls)
        inst.__init__(
            product_id=d["product_id"],
            product_version=d.get("product_version"),
            product_quantity=d["product_quantity"]
        )
        return inst


class Event(MobiModel):
    def __init__(self, user_id: str, user_population: Union[str, int, Population],
                 event_type: Union[str, int, EventType], event_platform: Union[str, int, EventPlatform],
                 date: datetime, product_id: Optional[str] = None, product_version: Optional[int] = None,
                 product_quantity: Optional[int] = None, basket_items: Optional[List[EventBasketItem]] = None):
        self.user_id = user_id
        self.user_population = Population.parse(user_population)
        self.event_type = EventType.parse(event_type)
        self.event_platform = EventPlatform.parse(event_platform)
        self.product_id = product_id
        self.product_version = product_version
        self.product_quantity = product_quantity
        self.date = date
        self.basket_items = basket_items

    def as_dict(self) -> Dict[str, Any]:
        return {
            "user_id": self.user_id,
            "user_population": self.user_population,
            "event_type": self.event_type,
            "event_platform": self.event_platform,
            "product_id": self.product_id,
            "product_version": self.product_version,
            "product_quantity": self.product_quantity,
            "date": self.date,
            "basket_items": self.basket_items
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Event':
        product_quantity = d.get("product_quantity")
        if product_quantity is None and EventType.parse(d["event_type"]) == EventType.PRODUCT_TO_BASKET:
            # As we added it later
            product_quantity = 1

        inst = super(Event, cls).__new__(cls)
        inst.__init__(
            user_id=d["user_id"],
            user_population=d.get("user_population"),  # .get(), because this field was added
            # after we started producing data
            event_type=d["event_type"],
            event_platform=d.get("event_platform", EventPlatform.OTHER),  # As we added it later
            product_id=d.get("product_id"),  # As it's nullable now
            product_version=d.get("product_version"),
            product_quantity=product_quantity,
            date=cls._parse_datetime(d.get("date") or datetime.utcnow()),
            basket_items=None if d.get("basket_items") is None \
            else [EventBasketItem.from_dict(bi) for bi in d.get("basket_items")]  # As we added it later
        )
        return inst
