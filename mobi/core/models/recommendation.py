from datetime import datetime
from typing import Any, Dict, List, Optional, Union

from mobi.core.common.mobienum import MobiEnum
from mobi.core.models._base import MobiModel
from mobi.core.models.product import Product


class RecommendationType(MobiEnum):
    UNKNOWN = 0
    SIMILAR_PRODUCTS = 1
    PERSONAL_RECOMMENDATIONS = 2
    BASKET_RECOMMENDATIONS = 3
    STATIC_RECOMMENDATIONS = 4  # We recommend predefined list of products
    RECENTLY_VIEWED = 5
    MOST_POPULAR = 6
    RANK_PRODUCTS = 7


class RecommendationSource(MobiEnum):
    UNKNOWN = 0
    MODEL = 1
    MOST_POPULAR_PRODUCTS = 2
    PREDEFINED = 3


class Recommendation(MobiModel):
    def __init__(self, user_id: str, date: datetime, type: Union[int, str, RecommendationType],
                 source: Union[int, str, RecommendationSource], product_ids: List[str],
                 source_product_id: Optional[str] = None, zone_id: Optional[str] = None,
                 campaign_id: Optional[str] = None):
        self.user_id = user_id
        self.date = date
        self.type = RecommendationType.parse(type)
        self.source = RecommendationSource.parse(source)
        self.product_ids = product_ids
        self.source_product_id = source_product_id
        self.zone_id = zone_id
        self.campaign_id = campaign_id

    def as_dict(self) -> dict:
        return {
            "user_id": self.user_id,
            "date": self.date,
            "type": self.type,
            "source": self.source,
            "product_ids": self.product_ids,
            "source_product_id": self.source_product_id,
            "zone_id": self.zone_id,
            "campaign_id": self.campaign_id
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'Recommendation':
        inst = super(Recommendation, cls).__new__(cls)
        inst.__init__(
            user_id=d["user_id"],
            date=d["date"],
            type=d["type"],
            source=d["source"],
            product_ids=d["product_ids"],
            source_product_id=d.get("source_product_id"),
            zone_id=d.get("zone_id"),
            campaign_id=d.get("campaign_id")
        )
        return inst

