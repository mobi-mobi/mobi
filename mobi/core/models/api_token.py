from datetime import datetime
from hashlib import sha256
from typing import Iterable, Dict, Any

from mobi.core.models._base import MobiModel, MobiEnum


class TokenType(MobiEnum):
    PRODUCTION = 1
    TEST = 2


class TokenPermission(MobiEnum):
    # Catalog API

    READ_CATALOG = 1
    UPDATE_CATALOG = 2

    # Events API

    SEND_EVENTS = 11

    # Recommendation API

    GET_MODEL_INFO = 21

    GET_RECOMMENDATIONS = 31
    SEARCH = 32

    TEST_RECOMMENDATIONS = 33

    # Metrics API

    READ_METRICS = 41


class TokenHashAlgo(MobiEnum):
    SHA256 = 0


def _hash_token_sha256(token: str) -> str:
    return sha256(token.encode()).hexdigest()


def hash_token(token: str, token_hash_algo: TokenHashAlgo) -> str:
    if token_hash_algo == TokenHashAlgo.SHA256:
        return _hash_token_sha256(token)
    raise ValueError(f"unknown hash algo: {token_hash_algo}")


class ApiToken(MobiModel):
    def __init__(self, client: str, token_hash: str, token_hash_algo: TokenHashAlgo, first_letters: str,
                 token_type: TokenType, expiration_date: datetime, permissions: Iterable[TokenPermission],
                 description: str = None):
        self.client = client
        self.token_hash = token_hash
        self.token_hash_algo = TokenHashAlgo.parse(token_hash_algo)
        self.first_letters = first_letters
        self.token_type = TokenType.parse(token_type)
        self.expiration_date: datetime = expiration_date
        self.permissions = list(set(TokenPermission.parse(permission) for permission in permissions))
        self.description = description

    def expired(self) -> bool:
        return self.expiration_date < datetime.utcnow()

    def as_dict(self) -> Dict[str, Any]:
        return {
            "client": self.client,
            "token_hash": self.token_hash,
            "token_hash_algo": self.token_hash_algo,
            "first_letters": self.first_letters,
            "token_type": self.token_type,
            "expiration_date": self.expiration_date,
            "permissions": self.permissions,
            "description": self.description
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'ApiToken':
        return ApiToken(
            client=d["client"],
            token_hash=d["token_hash"],
            token_hash_algo=TokenHashAlgo.parse(d["token_hash_algo"]),
            first_letters=d["first_letters"],
            token_type=TokenType.parse(d["token_type"]),
            expiration_date=d["expiration_date"],
            permissions=list(set(TokenPermission.parse(v) for v in d["permissions"])),
            description=d.get("description")
        )
