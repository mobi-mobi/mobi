from dataclasses import dataclass
from datetime import datetime

from typing import Any, Dict

from mobi.core.common.mobienum import MobiEnum
from mobi.core.models._base import MobiModel


class ModelStatus(MobiEnum):
    NOT_READY = 0
    ERROR = 1
    READY = 2


class ModelDumpInfo(MobiModel):
    def __init__(self, model_id: str, model_type: str, date: datetime, status: ModelStatus):
        self.model_id = model_id
        self.model_type = model_type
        self.date = date
        self.status = status

    def as_dict(self) -> Dict[str, Any]:
        return {
            "model_id": self.model_id,
            "model_type": self.model_type,
            "date": self.date,
            "status": self.status
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'ModelDumpInfo':
        inst = super(ModelDumpInfo, cls).__new__(cls)
        inst.__init__(
            model_id=d.get("model_id"),
            model_type=d.get("model_type"),
            date=d.get("date"),
            status=ModelStatus.parse(d.get("status"))
        )
        return inst


@dataclass
class ModelDump:
    info: ModelDumpInfo
    binary: bytes
