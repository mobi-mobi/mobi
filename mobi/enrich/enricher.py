from abc import ABC, abstractmethod
from typing import Optional

from mobi.core.models import Product


class ProductEnricher(ABC):
    @abstractmethod
    def enrich_product(self, product: Product):
        pass
