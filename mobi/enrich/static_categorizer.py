from typing import List

from mobi.enrich.categorizer import Categorizer


_title_to_categories = {
    "eyes": ["eyes"],
    "глаз": ["eyes"],

    "scrub": ["scrub"],
    "скраб": ["scrub"],

    "mask": ["mask"],
    "маски": ["mask"],
    "маска": ["mask"],
    "масок": ["mask"],

    "hands": ["hands"],
    "для рук": ["hands"],

    "для тела": ["body"],
    "for body": ["body"],

    "для ног": ["foot"],
    "for legs": ["foot"],

    "soap": ["soap"],
    "мыло": ["soap"],

    "fluid": ["fluid"],
    "флюид": ["fluid"],

    "skin": ["skin"],
    "кожа": ["skin"],
    "кожи": ["skin"],

    "cleansing": ["cleanser"],
    "cleanser": ["cleanser"],
    "очищен": ["cleanser"],
    "очищающ": ["cleanser"],

    "powder": ["powder"],
    "пудр": ["powder"],

    "gel": ["gel"],
    "гель": ["gel"],
    "гель для": ["gel_for"],
    "gel for": ["gel_for"],

    "matting": ["matting"],
    "матирующ": ["matting"],

    "detox": ["detox"],
    "repair": ["repair"],

    "oil": ["oil"],
    "масло": ["oil"],

    "tonic": ["tonic"],
    "тоник": ["tonic"],

    "sea": ["sea"],
    "морск": ["sea"],

    "seaweed": ["seaweed"],
    "водорослями": ["seaweed"],

    "nail polish remover": ["nails", "nail_polish_remover"],
    "жидкость для снятия лака": ["nails", "nail_polish_remover"],
    "фиксатор лака": ["nails", "varnish_fixer"],

    "crayon": ["crayon"],
    "карандаш": ["crayon"],

    "elixir": ["elixir"],
    "эликсир": ["skin", "face", "elixir"],

    "spray": ["spray"],
    "спрей": ["spray"],

    "body": ["body"],
    "тела": ["body"],
    "телу": ["body"],
    "телом": ["body"],

    "anti aging": ["anti_aging"],
    "anti-aging": ["anti_aging"],
    "antiaging": ["anti_aging"],
    "антивозрастн": ["anti_aging"],

    "day cream": ["day_cream"],
    "дневной крем": ["day_cream"],

    "conditioner": ["conditioner"],
    "кондиционер": ["conditioner"],

    "herbal": ["herbal"],
    "трав": ["herbal"],

    "beauty": ["beauty"],
    "бьюти": ["beauty"],

    "disposable": ["disposable"],
    "однораз": ["disposable"],

    "nutritious": ["nutrient"],
    "nutrient": ["nutrient"],
    "питательн": ["nutrient"],

    "lipstick": ["lipstick"],
    "помада": ["lipstick"],

    "concealer": ["concealer"],
    "консилер": ["concealer"],

    "свеча": ["candle"],
    "candle": ["candle"],

    "помещен": ["building"],

    "straightening": ["straightening"],
    "выпрямляющ": ["straightening"],

    "head": ["head"],
    "голов": ["head"],

    "ink": ["ink"],
    "тушь": ["ink"],

    "primer": ["primer"],
    "праймер": ["primer"],

    "for lips": ["for_lips"],
    "для губ": ["for_lips"],

    "cream": ["cream"],
    "крем": ["cream"],

    "eyelash": ["eyelashes"],
    "ресниц": ["eyelashes"],

    "lip_gloss": ["lips", "lip_gloss"],
    "блеск для губ": ["lips", "lip_gloss"],

    "peeling": ["peeling"],
    "пилинг": ["peeling"],

    "rinse_aid": ["rinse_aid"],
    "ополаскиватель": ["rinse_aid"],

    "mouth": ["mouth"],
    "полости рта": ["mouth"],

    "antibacterial": ["antibacterial"],
    "антибактериальн": ["antibacterial"],

    "lotion": ["lotion"],
    "лосьон": ["lotion"],

    "face": ["face"],
    "для лица": ["face"],

    "bath salt": ["bath", "salt", "bath_salt"],
    "соль для ванн": ["bath", "salt", "bath_salt"],

    "shower gel": ["shower", "shower_gel"],
    "гель для душа": ["shower", "shower_gel"],

    "lip balm": ["lip_balm", "lips"],
    "бальзам для губ": ["lip_balm", "lips"],

    "shaving": ["shaving"],
    "shaving cream": ["shaving", "shaving_cream"],
    "крем для бритья": ["shaving", "shaving_cream"],

    "бритья": ["shaving"],
    "after shave": ["shaving", "after_shave"],
    "после бритья": ["shaving", "after_shave"],
    "after shave emulsion": ["shaving", "after_shave", "after_shave_emulsion"],
    "эмульсия после бритья": ["shaving", "after_shave", "after_shave_emulsion"],

    "deodorant": ["deodorant"],
    "дезодорант": ["deodorant"],

    "body scrub": ["body", "scrub", "body_scrub"],
    "скраб для тела": ["body", "scrub", "body_scrub"],

    "liquid soap": ["soap", "liquid_soap"],
    "жидкое мыло": ["soap", "liquid_soap"],

    "shampoo": ["shampoo"],
    "шампунь": ["shampoo"],

    "одеколон": ["cologne"],
    "cologne": ["cologne"],

    "hair balm": ["hair", "hair_balm"],
    "бальзам для волос": ["hair", "hair_balm"],

    "face tonic": ["face", "face_tonic"],
    "тоник для лица": ["face", "face_tonic"],

    "дорожном формате": ["road_format"],

    "toilet water": ["perfume", "eau_de_toilette"],
    "eau de toilette": ["perfume", "eau_de_toilette"],
    "туалетная вода": ["perfume", "eau_de_toilette"],

    "highlighter": ["highlighter"],
    "хайлайтер": ["highlighter"],
    "хайлайтер для лица и глаз": ["face", "eyes", "highlighter", "highlighter_for_face_and_eyes"],

    "liquid lipstick": ["lips", "lipstick", "liquid_lipstick"],
    "cream lipstick": ["lips", "lipstick", "cream_lipstick"],
    "жидкая помада для губ": ["lipstick", "liquid_lipstick"],
    "кремовая помада для губ": ["lipstick", "cream_lipstick"],

    "makeup brush set": ["makeup", "makeup_brush_set"],
    "набор кистей для макияжа": ["makeup", "makeup_brush_set"],

    "palette of shadows": ["eyes", "makeup", "shadow", "palette", "palette_of_shadows"],
    "палетка теней": ["eyes", "makeup", "shadow", "palette", "palette_of_shadows"],

    "face oil": ["face", "face_oil"],
    "масло для лица": ["face", "face_oil"],

    "hand cream": ["hands", "hand_cream"],
    "hands cream": ["hands", "hand_cream"],
    "крем для рук": ["hands", "hand_cream"],

    "foot cream": ["foot", "foot_cream"],
    "крем для ног": ["foot", "foot_cream"],

    "foundation": ["makeup", "foundation"],
    "тональная основа": ["makeup", "foundation"],
    "тональное средство": ["makeup", "foundation"],

    "parfum": ["perfume"],
    "perfume": ["perfume"],
    "eau de parfum": ["perfume", "eau_de_parfum"],
    "парфюмерная вода": ["perfume", "eau_de_parfum"],

    "eyeliner": ["eyes", "eyeliner"],
    "карандаш для глаз": ["eyes", "eyeliner"],

    "face serum": ["face", "face_serum"],
    "сыворотка для лица": ["face", "face_serum"],

    "eye balm": ["eyes", "eye_balm"],
    "бальзам для глаз": ["eyes", "eye_balm"],

    "sunscreen": ["sunscreen"],
    "солнцезащитн": ["sunscreen"],

    "face care": ["face", "face_care"],
    "уход за лицом": ["face", "face_care"],
    "уходу за лицом": ["face", "face_care"],

    "hydrating": ["moisturizing", "hydrating"],
    "увлажн": ["moisturizing", "hydrating"],

    "mask for the face": ["mask_for_the_face"],
    "маска для лица": ["mask_for_the_face"],

    "face cream": ["face", "face_cream"],
    "крем для лица": ["face", "face_cream"],

    "гель для умывания": ["face", "face_washing", "washing_gel"],
    "крем для умывания": ["face", "face_washing", "washing_creme"],

    "makeup remover": ["makeup", "makeup_remover"],
    "снятия макияжа": ["makeup", "makeup_remover"],
    "снятие макияжа": ["makeup", "makeup_remover"],

    "moisturizing mask": ["hydrating", "moisturizing_mask"],
    "маска увлажняющая": ["hydrating", "moisturizing_mask"],

    "духи": ["ru_duhi", "perfume"],

    "парфюмерное мыло": ["perfume_soap"],

    "blonde": ["blonde"],
    "блонд": ["blonde"],

    "маска-филлер": ["filler_mask"],

    "serum": ["serum"],
    "сыворотка": ["serum"],

    "shine water": ["shine_water"],
    "вода для блеска": ["shine_water"],

    "cleaning powder": ["cleanser_powder"],
    "cleanser powder": ["cleanser_powder"],
    "пудра для умывания": ["cleanser_powder"],

    "body cream": ["body", "body_cream"],
    "крем для тела": ["body", "body_cream"],

    "маска-перчатки": ["mask_gloves"],

    "night mask": ["face", "mask", "night", "night_mask"],
    "ночная маска": ["face", "mask", "night", "night_mask"],
    "маска ночная": ["face", "mask", "night", "night_mask"],
}


_word_prefixes_to_categories = {
    "мужчин": ["for_men"],
    "мужск": ["for_men"],

    "kit": ["kit"],
    "набор": ["kit"],

    "жидк": ["liquid"],
    "кремо": ["cream_like"],

    "макияж": ["makeup"],

    "дорожн": ["for_road"],

    "подарочн": ["gift"],

    "тени": ["eyes", "makeup", "shadow"],
    "теней": ["eyes", "makeup", "shadow"],

    "box": ["box"],

    "recovery": ["recovery"],
    "восстанавл": ["recovery"],

    "контур": ["contouring"],
    "contouring": ["contouring"],

    "бронз": ["bronze"],

    "жирн": ["fat"],

    "мусс": ["mousse"],
    "mousse": ["mousse"],

    "hair": ["hairs"],
    "волос": ["hairs"],

    "бров": ["eyebrows"],

    "nail": ["nails"],
    "ногт": ["nails"],

    "moisturizing": ["hydrating", "moisturizing"],
    "увлажняющ": ["hydrating", "moisturizing"],

    "night": ["night"],
    "ночн": ["night"],
}

_exact_words_to_categories = {
    "men": ["for_men"],
    "homme": ["for_men"],

    "3d": ["3d"],
}


class StaticCategorizer(Categorizer):
    def categorize(self, s: str) -> List[str]:
        s_lower = s.lower()
        enriched_categories = set()
        for keyword, categories in _title_to_categories.items():
            if s_lower.find(keyword.lower()) != -1:
                enriched_categories.update(categories)
        title_words = {s_lower}
        split_by = [" ", "-", ".", ",", "'", "`", "’", ":", ";"]
        for split_param in split_by:
            title_words = {word for word_to_split in title_words for word in word_to_split.split(split_param)
                           if word}

        for word_prefix, categories in _word_prefixes_to_categories.items():
            for title_word in title_words:
                if title_word.startswith(word_prefix):
                    enriched_categories.update(categories)

        for title_word in title_words:
            if title_word in _exact_words_to_categories:
                enriched_categories.update(_exact_words_to_categories[title_word])

        return list(enriched_categories)
