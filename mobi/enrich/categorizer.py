from abc import abstractmethod
from typing import List, Optional

from mobi.core.models import Product
from mobi.enrich.enricher import ProductEnricher


class Categorizer(ProductEnricher):
    @abstractmethod
    def categorize(self, s: str) -> List[str]:
        pass

    def enrich_product(self, product: Product):
        enriched_tags = set(self.categorize(product.title))

        product.enriched_tags = list(set(product.enriched_tags or []).union(enriched_tags))
