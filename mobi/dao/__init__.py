from mobi.dao.common import MobiDaoException
from mobi.dao.factory import get_audience_dao, get_auth_dao, get_cache_dao, get_catalog_dao, get_event_dao,\
    get_message_dao, get_metrics_dao, get_model_dao, get_object_dao, get_recolog_dao, get_search_dao, get_settings_dao,\
    get_user_settings_dao
