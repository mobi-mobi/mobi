import argparse

from pymongo import MongoClient
from pymongo.database import Database

from mobi.dao.event.impl.mongodb import MongoEventDao


def create_events_collection(db: Database, events_collection_name: str):
    db.create_collection(events_collection_name)


def create_indexes(db, events_collection_name: str):
    events = db[events_collection_name]

    events.create_index([("client", 1), ("user_id", 1)], unique=False)
    events.create_index([("client", 1), ("date", 1)], unique=False)
    events.create_index([("client", 1), ("user_id", 1), ("date", 1)], unique=False)


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_events_collection = MongoEventDao.EVENTS_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--events_collection", required=False, default=_default_events_collection,
                        help=f"Events collection. Default: {_default_events_collection}", type=str)

    return parser.parse_args()


def setup_mongodb_event_dao(host, port, dbname, events_collection_name):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_events_collection(db, events_collection_name)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, events_collection_name)
    print("Done.")

    client.close()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_event_dao(args.host, args.port, args.database, args.events_collection)
