import argparse

from pymongo import MongoClient
from pymongo.database import Database

from mobi.dao.catalog.impl.mongodb import MongoCatalogDao


def create_product_collection(db: Database, products_collection: str):
    db.create_collection(products_collection)


def create_indexes(db, products_collection: str):
    products = db[products_collection]

    products.create_index([("client", 1), ("id", 1), ("version", 1)], unique=True)
    products.create_index([("client", 1), ("id", 1)], unique=False)


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_products_collection = MongoCatalogDao.PRODUCTS_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--products_collection", required=False, default=_default_products_collection,
                        help=f"Products collection. Default: {_default_products_collection}", type=str)

    return parser.parse_args()


def setup_mongodb_catalog_dao(host, port, dbname, products_collection):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_product_collection(db, products_collection)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, products_collection)
    print("Done.")

    client.close()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_catalog_dao(args.host, args.port, args.database, args.products_collection)
