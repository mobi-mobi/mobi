import argparse

from pymongo import MongoClient, TEXT
from pymongo.database import Database

from mobi.dao.settings.impl.mongodb import MongoSettingsDao


def create_tokens_collection(db: Database, tokens_collection: str):
    db.create_collection(tokens_collection)


def create_zones_collection(db: Database, zones_collection: str):
    db.create_collection(zones_collection)


def create_campaigns_collection(db: Database, campaigns_collection: str):
    db.create_collection(campaigns_collection)


def create_indexes(db, zones_collection, campaigns_collection, tokens_collection):
    zones = db[zones_collection]

    zones.create_index("client", unique=False)
    zones.create_index([("client", 1), ("zone_id", 1)], unique=True)
    zones.create_index([("client", 1), ("name", TEXT)], default_language="english", unique=False)

    campaigns = db[campaigns_collection]

    campaigns.create_index("client", unique=False)
    campaigns.create_index([("client", 1), ("campaign_id", 1)], unique=True)
    campaigns.create_index([("client", 1), ("name", TEXT)], default_language="english", unique=False)

    tokens = db[tokens_collection]

    tokens.create_index("token_hash", unique=True)


def setup_mongodb_settings_dao(host, port, dbname, zones_collection, campaigns_collection, tokens_collection):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_zones_collection(db, zones_collection)
    create_campaigns_collection(db, campaigns_collection)
    create_tokens_collection(db, tokens_collection)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, zones_collection, campaigns_collection, tokens_collection)
    print("Done.")

    client.close()


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_zones_collection = MongoSettingsDao.ZONES_COLLECTION_NAME
    _default_campaigns_collection = MongoSettingsDao.CAMPAIGNS_COLLECTION_NAME
    _default_tokens_collection = MongoSettingsDao.TOKENS_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--zones_collection", required=False, default=_default_zones_collection,
                        help=f"Zones collection. Default: {_default_zones_collection}", type=str)

    parser.add_argument("--campaigns_collection", required=False, default=_default_campaigns_collection,
                        help=f"Campaigns collection. Default: {_default_campaigns_collection}", type=str)

    parser.add_argument("--tokens_collection", required=False, default=_default_tokens_collection,
                        help=f"API Tokens collection. Default: {_default_tokens_collection}", type=str)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_settings_dao(args.host, args.port, args.database, args.zones_collection, args.campaigns_collection,
                               args.tokens_collection)
