import argparse

from pymongo import MongoClient
from pymongo.database import Database

from mobi.dao.metrics.impl.mongodb import MongoMetricsDao


def create_metrics_collection(db: Database, metrics_collection_name: str):
    db.create_collection(metrics_collection_name)


def create_indexes(db, metrics_collection_name: str):
    _tags_to_index = [
        "product_id",
        "brand"
    ]

    metrics = db[metrics_collection_name]

    fields = [("client", 1), ("name", 1), ("base", 1), ("timestamp", 1)]

    for tag in _tags_to_index:
        fields.append((f"tags.{tag}", 1))

    metrics.create_index(fields, unique=False)


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_metrics_collection = MongoMetricsDao.METRICS_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--metrics_collection", required=False, default=_default_metrics_collection,
                        help=f"Metrics collection. Default: {_default_metrics_collection}", type=str)

    return parser.parse_args()


def setup_mongodb_metrics_dao(host, port, dbname, metrics_collection_name):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_metrics_collection(db, metrics_collection_name)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, metrics_collection_name)
    print("Done.")

    client.close()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_metrics_dao(args.host, args.port, args.database, args.metrics_collection)
