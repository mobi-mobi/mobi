import argparse

from collections import OrderedDict
from pymongo import MongoClient
from pymongo.database import Database


def create_product_collection(db: Database):
    db.create_collection("products")

    vexpr = {
        "$jsonSchema": {
            "bsonType": "object",
            "required": ["client", "id", "title", "in_stock", "active", "brand", "features"],
            "properties": {
                "client": {
                    "bsonType": "string"
                },
                "id": {
                    "bsonType": "string",
                    "description": "An external product id. Also used as an index. Required."
                },
                "title": {
                    "bsonType": "string"
                },
                "in_stock": {
                    "bsonType": "bool"
                },
                "active": {
                    "bsonType": "bool"
                },
                "brand": {
                    "bsonType": "string"
                },
                "features": {
                    "bsonType": "object",
                },
            }
        }
    }

    query = [
        ("collMod", "products"),
        ("validator", vexpr),
        ("validationLevel", "moderate")
    ]

    db.command(OrderedDict(query))


def create_events_collection(db: Database):
    db.create_collection("events")

    vexpr = {
        "$jsonSchema": {
            "bsonType": "object",
            "required": ["client", "user_id", "user_population", "product_id", "event_type", "date"],
            "properties": {
                "client": {
                    "bsonType": "string"
                },
                "user_id": {
                    "bsonType": "string"
                },
                "user_population": {
                    "bsonType": "int"
                },
                "product_id": {
                    "bsonType": "string"
                },
                "product_version": {
                    "bsonType": "int"
                },
                "event_type": {
                    "bsonType": "int"
                },
                "date": {
                    "bsonType": "date"
                }
            }
        }
    }

    query = [
        ("collMod", "events"),
        ("validator", vexpr),
        ("validationLevel", "moderate")
    ]

    db.command(OrderedDict(query))


def create_models_collection(db: Database):
    db.create_collection("models")

    vexpr = {
        "$jsonSchema": {
            "bsonType": "object",
            "required": ["client", "model_id", "type", "status", "date"],
            "properties": {
                "client": {
                    "bsonType": "string"
                },
                "model_id": {
                    "bsonType": "string"
                },
                "type": {
                    "bsonType": "string"
                },
                "status": {
                    "bsonType": "int"
                },
                "date": {
                    "bsonType": "date"
                }
            }
        }
    }

    query = [
        ("collMod", "models"),
        ("validator", vexpr),
        ("validationLevel", "moderate")
    ]

    db.command(OrderedDict(query))


def create_indexes(db: Database):
    products = db["products"]

    products.create_index([("client", 1), ("id", 1)], unique=True)

    events = db["events"]

    events.create_index([("client", 1), ("user_id", 1)], unique=False)
    events.create_index([("client", 1), ("date", 1)], unique=False)
    events.create_index([("client", 1), ("user_id", 1), ("date", 1)], unique=False)

    metrics = db["metrics"]

    metrics.create_index([("client", 1), ("name", 1), ("base", 1), ("timestamp", 1), ("tags", 1)], unique=True)

    models = db["models"]

    models.create_index([("client", 1), ("model_id", 1)], unique=True)
    models.create_index([("client", 1), ("date", 1)], unique=False)


def setup_mongodb(host: str, port: int, database: str):
    client = MongoClient(host, port)
    db = client[database]

    create_product_collection(db)
    create_events_collection(db)
    create_models_collection(db)

    create_indexes(db)


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb(args.host, args.port, args.database)
