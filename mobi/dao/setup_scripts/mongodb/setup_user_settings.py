import argparse

from pymongo import MongoClient, TEXT
from pymongo.database import Database

from mobi.dao.user_settings.impl.mongodb import MongoUserSettingsDao


def create_user_settings_collection(db: Database, user_settings_collection: str):
    db.create_collection(user_settings_collection)


def create_indexes(db, user_settings_collection: str):
    user_settings = db[user_settings_collection]
    user_settings.create_index("user", unique=True)


def setup_mongodb_user_settings_dao(host, port, dbname, user_settings_collection):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_user_settings_collection(db, user_settings_collection)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, user_settings_collection)
    print("Done.")

    client.close()


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_user_settings_collection = MongoUserSettingsDao.USER_SETTINGS_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--user_settings_collection", required=False, default=_default_user_settings_collection,
                        help=f"User Settings collection. Default: {_default_user_settings_collection}", type=str)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_user_settings_dao(args.host, args.port, args.database, args.user_settings_collection)
