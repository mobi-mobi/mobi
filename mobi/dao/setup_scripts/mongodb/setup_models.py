import argparse

from pymongo import MongoClient
from pymongo.database import Database

from mobi.dao.object.impl.mongodb import MongoModelDao


def create_models_collection(db: Database, models_collection_name: str):
    db.create_collection(models_collection_name)


def create_indexes(db, models_collection_name: str):
    models = db[models_collection_name]

    models.create_index([("client", 1), ("date", 1)], unique=False)
    models.create_index([("client", 1), ("model_id", 1)], unique=True)


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_models_collection = MongoModelDao.MODELS_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--models_collection", required=False, default=_default_models_collection,
                        help=f"Models collection. Default: {_default_models_collection}", type=str)

    return parser.parse_args()


def setup_mongodb_models_dao(host, port, dbname, models_collection_name):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_models_collection(db, models_collection_name)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, models_collection_name)
    print("Done.")

    client.close()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_models_dao(args.host, args.port, args.database, args.models_collection)
