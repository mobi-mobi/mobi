import argparse

from pymongo import MongoClient
from pymongo.database import Database

from mobi.dao.recolog.impl.mongodb import MongoRecologDao


def create_recologs_collection(db: Database, recologs_collection_name: str):
    db.create_collection(recologs_collection_name)


def create_attributed_events_collection(db: Database, attributed_events_collection_name: str):
    db.create_collection(attributed_events_collection_name)


def create_attributed_recommendations_collection(db: Database, attributed_recommendations_collection_name: str):
    db.create_collection(attributed_recommendations_collection_name)


def create_unmatched_recommendations_collection(db: Database, unmatched_recommendations_collection: str):
    db.create_collection(unmatched_recommendations_collection)


def create_collections(db: Database, recologs_collection_name: str, attributed_events_collection_name: str,
                       attributed_recommendations_collection_name: str, unmatched_recommendations_collection: str):
    create_recologs_collection(db, recologs_collection_name)
    create_attributed_events_collection(db, attributed_events_collection_name)
    create_attributed_recommendations_collection(db, attributed_recommendations_collection_name)
    create_unmatched_recommendations_collection(db, unmatched_recommendations_collection)


def create_indexes(db, recologs_collection_name: str, attributed_events_collection_name: str,
                   attributed_recommendations_collection_name: str, unmatched_recommendations_collection: str):
    recologs = db[recologs_collection_name]

    recologs.create_index([("client", 1), ("user_id", 1)], unique=False)
    recologs.create_index([("client", 1), ("date", 1)], unique=False)
    recologs.create_index([("client", 1), ("user_id", 1), ("date", 1)], unique=False)

    attributed_events = db[attributed_events_collection_name]

    attributed_events.create_index([("client", 1), ("event.date", 1)], unique=False)
    attributed_events.create_index([("client", 1), ("event.user_id", 1), ("event.date", 1)], unique=False)

    attributed_recommendations = db[attributed_recommendations_collection_name]

    attributed_recommendations.create_index([("client", 1), ("recommendation.date", 1)], unique=False)
    attributed_recommendations.create_index([("client", 1), ("recommendation.user_id", 1), ("recommendation.date", 1)],
                                            unique=False)

    unmatched_recommendations = db[unmatched_recommendations_collection]

    unmatched_recommendations.create_index([("client", 1), ("date", 1)], unique=False)
    unmatched_recommendations.create_index([("client", 1), ("user_id", 1), ("date", 1)], unique=False)


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_recologs_collection = MongoRecologDao.RECOLOG_COLLECTION_NAME
    _default_attributed_events_collection = MongoRecologDao.ATTRIBUTED_EVENTS_COLLECTION_NAME
    _default_attributed_recommendations_collection = MongoRecologDao.ATTRIBUTED_RECOMMENDATIONS_COLLECTION_NAME
    _default_unmatched_recommendations_collection = MongoRecologDao.SAMPLED_UNMATCHED_RECOMMENDATIONS_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--recologs_collection", required=False, default=_default_recologs_collection,
                        help=f"Recologs collection. Default: {_default_recologs_collection}", type=str)

    parser.add_argument("--attributed_events_collection", required=False, default=_default_attributed_events_collection,
                        help=f"Attributed events collection. Default: {_default_attributed_events_collection}",
                        type=str)

    parser.add_argument("--attributed_recommendations_collection", required=False,
                        default=_default_attributed_recommendations_collection,
                        help=f"Attributed recommendations collection. Default: "
                             f"{_default_attributed_recommendations_collection}", type=str)

    parser.add_argument("--unmatched_recommendations_collection", required=False,
                        default=_default_unmatched_recommendations_collection,
                        help=f"Unmatched recommendations collection. Default: {_default_attributed_events_collection}",
                        type=str)

    return parser.parse_args()


def setup_recologs_dao(host, port, dbname, recologs_collection, attributed_events_collection,
                       attributed_recommendations_collection, unmatched_recommendations_collection):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_collections(db, recologs_collection, attributed_events_collection, attributed_recommendations_collection,
                       unmatched_recommendations_collection)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, recologs_collection, attributed_events_collection, attributed_recommendations_collection,
                   unmatched_recommendations_collection)
    print("Done.")

    client.close()


if __name__ == "__main__":
    args = get_args()
    setup_recologs_dao(args.host, args.port, args.database, args.recologs_collection, args.attributed_events_collection,
                       args.attributed_recommendations_collection, args.unmatched_recommendations_collection)
