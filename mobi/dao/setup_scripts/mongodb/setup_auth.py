import argparse

from pymongo import MongoClient
from pymongo.database import Database

from mobi.dao.auth.impl.mongodb import MongoDbAuthDao


def create_users_collection(db: Database, users_collection: str):
    db.create_collection(users_collection)


def create_sessions_collection(db: Database, sessions_collection: str):
    db.create_collection(sessions_collection)


def create_indexes(db, users_collection, sessions_collection):
    users = db[users_collection]

    users.create_index("email", unique=True)

    sessions = db[sessions_collection]

    sessions.create_index("email", unique=False)
    sessions.create_index([("email", 1), ("token", 1)], unique=False)


def setup_mongodb_auth_dao(host, port, dbname, users_collection, sessions_collection):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_users_collection(db, users_collection)
    create_sessions_collection(db, sessions_collection)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, users_collection, sessions_collection)
    print("Done.")

    client.close()


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_users_collection = MongoDbAuthDao.USERS_COLLECTION_NAME
    _default_sessions_collection = MongoDbAuthDao.SESSIONS_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--users_collection", required=False, default=_default_users_collection,
                        help=f"Users collection. Default: {_default_users_collection}", type=str)

    parser.add_argument("--sessions_collection", required=False, default=_default_sessions_collection,
                        help=f"Sessions collection. Default: {_default_sessions_collection}", type=str)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_auth_dao(args.host, args.port, args.database, args.users_collection, args.sessions_collection)
