import argparse

from pymongo import MongoClient
from pymongo.database import Database

from mobi.dao.audience.impl.mongodb import MongoAudienceDao


def create_audiences_collection(db: Database, audiences_collection: str):
    db.create_collection(audiences_collection)


def create_audiences_unions_collection(db: Database, audiences_unions_collection: str):
    db.create_collection(audiences_unions_collection)


def create_audience_split_trees_collection(db: Database, audience_split_trees_collection: str):
    db.create_collection(audience_split_trees_collection)


def create_indexes(db, audiences_collection, audiences_unions_collection, audience_split_trees_collection):
    audiences = db[audiences_collection]
    audiences.create_index([("client", 1), ("audience_id", 1)], unique=True)

    audiences_unions = db[audiences_unions_collection]
    audiences_unions.create_index([("client", 1), ("audiences_union_id", 1)], unique=False)

    audience_split_trees = db[audience_split_trees_collection]
    audience_split_trees.create_index([("client", 1), ("split_tree_id", 1)], unique=False)


def setup_mongodb_audience_dao(host, port, dbname, audiences_collection, audiences_unions_collection,
                               audience_split_trees_collection):
    client = MongoClient(host, port)
    db = client[dbname]

    print("Creating collections...")
    create_audiences_collection(db, audiences_collection)
    create_audiences_unions_collection(db, audiences_unions_collection)
    create_audience_split_trees_collection(db, audience_split_trees_collection)
    print("Done")

    print("Creating indexes...")
    create_indexes(db, audiences_collection, audiences_unions_collection, audience_split_trees_collection)
    print("Done.")

    client.close()


def get_args() -> argparse.Namespace:
    _default_host = "127.0.0.1"
    _default_port = 27017
    _default_db = "mobi"

    _default_audiences_collection = MongoAudienceDao.AUDIENCES_COLLECTION_NAME
    _default_audiences_unions_collection = MongoAudienceDao.AUDIENCE_UNIONS_COLLECTION_NAME
    _default_audience_split_trees_collection = MongoAudienceDao.AUDIENCE_SPLIT_TREES_COLLECTION_NAME

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"MongoDB host. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"MongoDB port. Default: {_default_port}", type=int)

    parser.add_argument("--database", required=False, default=_default_db,
                        help=f"Database name. Default: {_default_db}", type=str)

    parser.add_argument("--audiences_collection", required=False, default=_default_audiences_collection,
                        help=f"Audiences collection. Default: {_default_audiences_collection}", type=str)

    parser.add_argument("--audiences_unions_collection", required=False, default=_default_audiences_unions_collection,
                        help=f"Audiences Unions collection. Default: {_default_audiences_unions_collection}", type=str)

    parser.add_argument("--audience_split_trees_collection", required=False,
                        default=_default_audience_split_trees_collection,
                        help=f"Audience split trees collection. Default: {_default_audience_split_trees_collection}",
                        type=str)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    setup_mongodb_audience_dao(args.host, args.port, args.database, args.audiences_collection,
                               args.audiences_unions_collection, args.audience_split_trees_collection)
