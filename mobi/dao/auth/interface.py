import re

from abc import ABC, abstractmethod
from datetime import datetime
from typing import Generator, Optional

from mobi.dao.common import MobiDaoException
from mobi.utils.password import generate_hash, generate_salt, generate_token

EMAIL_REGEX = re.compile("[^@]+@[^@]+\\.[^@]+")


class MobiAuthDao(ABC):
    def __init__(self):
        pass

    @classmethod
    def _email_is_valid(cls, email: str):
        return EMAIL_REGEX.match(email)

    @classmethod
    def _assert_email_hash(cls, email_hash: str):
        if not email_hash:
            raise MobiDaoException("Email hash is not provided")

        if type(email_hash) != str:
            raise MobiDaoException(f"Wrong email hash datatype: {type(email_hash)}")

    @classmethod
    def _assert_email(cls, email: str):
        if not email:
            raise MobiDaoException("Email is not provided")

        if type(email) != str:
            raise MobiDaoException(f"Wrong email datatype: {type(email)}")

        if not cls._email_is_valid(email):
            raise MobiDaoException(f"Wrong email format: {email}")

    @classmethod
    def _assert_token(cls, token: str):
        if not token:
            raise MobiDaoException("Token is not provided")

        if type(token) != str:
            raise MobiDaoException(f"Wrong token datatype: {type(token)}")

    @staticmethod
    def _generate_salt(k: int = 10):
        return generate_salt(k)

    @staticmethod
    def _password_hash(password: str, salt: str):
        return generate_hash(password, salt)

    @staticmethod
    def _generate_token(k: int = 32):
        return generate_token(k)

    @abstractmethod
    def create_user(self, email: str, name: str):
        pass

    @abstractmethod
    def delete_user(self, email: str):
        pass

    @abstractmethod
    def read_user(self, email: str) -> dict:
        pass

    @abstractmethod
    def read_users_for_client(self, client: str) -> Generator[str, None, None]:
        pass

    @abstractmethod
    def read_user_by_token(self, token: str) -> Optional[dict]:
        pass

    @abstractmethod
    def add_user_to_client(self, email, client):
        pass

    @abstractmethod
    def update_user_name(self, email, name):
        pass

    @abstractmethod
    def update_user_password(self, email, password):
        pass

    @abstractmethod
    def check_user_exists(self, email) -> bool:
        pass

    @abstractmethod
    def check_user_password(self, email, password) -> bool:
        pass

    @abstractmethod
    def create_session_token(self, email: str, expiration_datetime: datetime) -> str:
        pass

    @abstractmethod
    def renew_session_token(self, token: str, expiration_datetime: datetime):
        pass

    @abstractmethod
    def delete_session_token(self, token: str):
        pass

    @abstractmethod
    def check_session_token(self, token: str) -> bool:
        pass
