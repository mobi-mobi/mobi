from datetime import datetime
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError, WriteError
from typing import Generator, Optional

from mobi.dao.auth.interface import MobiAuthDao
from mobi.dao.common import MobiDaoException


class MongoDbAuthDao(MobiAuthDao):
    USERS_COLLECTION_NAME = "auth_users"
    SESSIONS_COLLECTION_NAME = "auth_sessions"

    def __init__(self, host, port, db, users_collection=None, sessions_collection=None):
        super().__init__()
        self.client = MongoClient(host, port)
        self.db = self.client[db]

        self.users = self.db[users_collection or self.USERS_COLLECTION_NAME]
        self.sessions = self.db[sessions_collection or self.SESSIONS_COLLECTION_NAME]

    def create_user(self, email: str, name: str):
        self._assert_email(email)

        if not name:
            raise MobiDaoException("Name is not provided")

        try:
            self.users.insert_one({
                "email": email,
                "name": name,
                "clients": []
            })
        except DuplicateKeyError:
            raise MobiDaoException("User already exist") from None

    def read_user(self, email: str) -> dict:
        result = self.users.find_one({"email": email})
        if result is None:
            raise MobiDaoException("User not found")
        return result

    def read_users_for_client(self, client: str) -> Generator[str, None, None]:
        for record in self.users.find({"clients": {"$in": [client]}}):
            if record.get("email"):
                yield record.get("email")

    def read_user_by_token(self, token: str) -> Optional[dict]:
        self._assert_token(token)

        session = self.sessions.find_one({
            "token": token
        }, projection={
            "_id": False,
            "email": True
        })

        if not session:
            return None

        return self.read_user(session["email"])

    def delete_user(self, email: str):
        self._assert_email(email)

        self.users.delete_one({
            "email": email
        })

        self.sessions.delete_many({
            "email": email
        })

    def add_user_to_client(self, email, client):
        self._assert_email(email)

        if not client:
            raise MobiDaoException("Client is not provided")

        try:
            result = self.users.update_one({
                "email": email
            }, {
                "$addToSet": {
                    "clients": client
                }
            })
        except WriteError as e:
            raise MobiDaoException(str(e)) from None

        if result.matched_count != 1:
            raise MobiDaoException("User not found")

    def update_user_name(self, email, name: str):
        if not name or not name.strip():
            raise MobiDaoException("Name is not set")

        result = self.users.update_one({
            "email": email
        }, {
            "$set": {
                "name": name.strip(),
            }
        })

        if result.matched_count != 1:
            raise MobiDaoException("User not found")

    def update_user_password(self, email, password):
        if not password:
            raise MobiDaoException("Password is not set")

        if type(password) != str:
            raise MobiDaoException(f"Wrong password type: {type(password)}")

        salt = self._generate_salt()
        password_hash = self._password_hash(password, salt)

        result = self.users.update_one({
            "email": email
        }, {
            "$set": {
                "salt": salt,
                "password": password_hash
            }
        })

        if result.matched_count != 1:
            raise MobiDaoException("User not found")

    def check_user_exists(self, email) -> bool:
        data = self.users.find_one({
            "email": email
        }, projection={
            "_id": True
        })
        if not data:
            return False
        return True

    def check_user_password(self, email: str, password: str) -> bool:
        data = self.users.find_one({
            "email": email
        }, projection={
            "_id": False,
            "salt": True,
            "password": True
        })
        if not data:
            return False
        if not data.get("password") or not data.get("salt"):
            return False
        return self._password_hash(password, data.get("salt")) == data.get("password")

    def create_session_token(self, email: str, expiration_datetime: datetime) -> str:
        self._assert_email(email)

        if datetime.utcnow() > expiration_datetime:
            raise MobiDaoException("Can not create token with expiration date in the past")

        token = self._generate_token()

        self.sessions.insert_one({
            "email": email,
            "token": token,
            "expiration_date": expiration_datetime
        })

        return token

    def renew_session_token(self, token: str, expiration_datetime: datetime):
        self._assert_token(token)

        if datetime.utcnow() > expiration_datetime:
            raise MobiDaoException("Can not update session with token "
                                   "expiration date in the past")

        result = self.sessions.find_one_and_update({
            "token": token
        }, {
            "$set": {
                "expiration_date": expiration_datetime
            }
        })

        if result is None:
            raise MobiDaoException("Can not find session")

    def delete_session_token(self, token: str):
        self._assert_token(token)

        self.sessions.delete_one({
            "token": token
        })

    def check_session_token(self, token: str) -> bool:
        self._assert_token(token)

        data = self.sessions.find_one({
            "token": token
        })

        if not data:
            return False

        if not data.get("expiration_date") or data.get("expiration_date") < datetime.utcnow():
            self.delete_session_token(token)
            return False

        return True
