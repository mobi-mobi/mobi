from abc import ABC, abstractmethod
from dataclasses import dataclass
from pickle import dumps, loads
from typing import Any, Dict, Generator, List, Optional, Tuple

from mobi.core.models import Event


@dataclass(frozen=True, init=True)
class WrappedUserEvent:
    event: Event
    payload: Optional[Dict[str, Any]] = None

    def as_bytes(self) -> bytes:
        return dumps((self.event.as_bytes(), self.payload))

    @classmethod
    def from_bytes(cls, bb: bytes) -> 'WrappedUserEvent':
        event_raw, payload = loads(bb)
        return WrappedUserEvent(Event.from_bytes(event_raw), payload)


class MobiMessageDao(ABC):
    def send_user_event(self, client: str, event: Event):
        self.send_wrapped_user_event(client, WrappedUserEvent(event))

    @abstractmethod
    def send_wrapped_user_event(self, client: str, event: WrappedUserEvent):
        pass

    def consume_user_events(self, consumer_group_id: str, from_timestamp_ms: int = None)\
            -> Generator[Tuple[str, Event], None, None]:
        for client, wrapped_event in self.consume_wrapped_user_events(consumer_group_id, from_timestamp_ms):
            yield wrapped_event.event

    @abstractmethod
    def consume_wrapped_user_events(self, consumer_group_id: str, from_timestamp_ms: int = None)\
            -> Generator[Tuple[str, WrappedUserEvent], None, None]:
        pass

    @abstractmethod
    def user_events_seek_ts(self, consumer_group_id: str, timestamp_ms: int):
        pass

    def poll_user_events(self, consumer_group_id: str, timeout_ms: int = 5000, until_ms: int = None,
                         max_records: int = None) -> List[Tuple[str, Event]]:
        return [
            (client, wrapped_event.event)
            for client, wrapped_event in self.poll_wrapped_user_events(
                consumer_group_id, timeout_ms, until_ms, max_records
            )
        ]

    @abstractmethod
    def poll_wrapped_user_events(self, consumer_group_id: str, timeout_ms: int = 5000, until_ms: int = None,
                                 max_records: int = None) -> List[Tuple[str, WrappedUserEvent]]:
        pass
