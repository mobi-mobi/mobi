import pickle

from kafka import KafkaConsumer, KafkaProducer
from threading import Lock
from typing import Dict, Generator, List, Tuple

from mobi.dao import MobiDaoException
from mobi.dao.message.interface import MobiMessageDao, WrappedUserEvent


def _user_event_to_bytes(client: str, wrapped_event: WrappedUserEvent) -> bytes:
    return pickle.dumps((client, wrapped_event.as_bytes()))


def _user_event_from_bytes(bb: bytes) -> Tuple[str, WrappedUserEvent]:
    try:
        client, raw_event = pickle.loads(bb)

        good_message = True

        if not isinstance(client, str) or not client:
            good_message = False

        if not isinstance(raw_event, bytes):
            good_message = False

        wrapped_event = WrappedUserEvent.from_bytes(raw_event)

        if not good_message:
            raise TypeError()
    except TypeError or ValueError:
        raise MobiDaoException("Can not parse user event received from message dao") from None

    return client, wrapped_event


_CONSUMER_CONSTRUCTION_LOCK = Lock()


class KafkaMessageDao(MobiMessageDao):
    USER_EVENTS_TOPIC = "user-events"

    # These are used in PROD env only
    USER_EVENTS_PRODUCER_USERNAME = "mobi-event-api"
    USER_EVENTS_PRODUCER_PASSWORD = "mobi-event-api"
    USER_EVENTS_CONSUMER_USERNAME = "mobi-event-api"
    USER_EVENTS_CONSUMER_PASSWORD = "mobi-event-api"

    def __init__(self, bootstrap_servers: List[str], security_protocol: str = None, sasl_mechanism: str = None):
        super(KafkaMessageDao, self).__init__()

        self.bootstrap_servers = bootstrap_servers
        self.connection_params = dict()

        if security_protocol is not None and sasl_mechanism is not None:
            self.connection_params["security_protocol"] = security_protocol
            self.connection_params["sasl_mechanism"] = sasl_mechanism
            self.connection_params["sasl_plain_username"] = self.USER_EVENTS_PRODUCER_USERNAME
            self.connection_params["sasl_plain_password"] = self.USER_EVENTS_PRODUCER_PASSWORD

        self.user_events_producer = KafkaProducer(bootstrap_servers=self.bootstrap_servers, **self.connection_params)

        self.consumers: Dict[str, KafkaConsumer] = dict()

    def send_wrapped_user_event(self, client: str, event: WrappedUserEvent):
        self.user_events_producer.send(
            topic=self.USER_EVENTS_TOPIC,
            value=_user_event_to_bytes(client, event)
        )

    def _consumer(self, topic: str, group_id: str):
        global _CONSUMER_CONSTRUCTION_LOCK
        with _CONSUMER_CONSTRUCTION_LOCK:
            try:
                return self.consumers[str((topic, group_id))]
            except KeyError:
                consumer = KafkaConsumer(
                    topic,
                    bootstrap_servers=self.bootstrap_servers,
                    group_id=group_id,
                    **self.connection_params
                )
                self.consumers[str((topic, group_id))] = consumer
                return self.consumers[str((topic, group_id))]

    @classmethod
    def _consumer_seek_ts(cls, consumer: KafkaConsumer, timestamp_ms: int = None):
        partitions = consumer.assignment()
        if not partitions:
            # Just to ensure that topics are assigned
            consumer.poll(timeout_ms=1, max_records=1)

        partitions = consumer.assignment()
        if partitions:
            offset_timestamps = {partition: timestamp_ms for partition in partitions}
            partition_offsets = consumer.offsets_for_times(offset_timestamps)
            for partition, offset_and_timestamp in partition_offsets.items():
                if offset_and_timestamp is not None:
                    offset = offset_and_timestamp.offset
                else:
                    offset = consumer.highwater(partition)
                if offset is not None:
                    consumer.seek(partition, offset)

    def consume_wrapped_user_events(self, consumer_group_id: str, from_timestamp_ms: int = None)\
            -> Generator[Tuple[str, WrappedUserEvent], None, None]:
        consumer = self._consumer(self.USER_EVENTS_TOPIC, consumer_group_id)
        if from_timestamp_ms is not None:
            self._consumer_seek_ts(consumer, from_timestamp_ms)
        for msg in consumer:
            yield _user_event_from_bytes(msg.value)

    def user_events_seek_ts(self, consumer_group_id: str, timestamp_ms: int):
        self._consumer_seek_ts(self._consumer(self.USER_EVENTS_TOPIC, consumer_group_id), timestamp_ms)

    def poll_wrapped_user_events(self, consumer_group_id: str, timeout_ms: int = 5000, until_ms: int = None,
                                 max_records: int = None) -> List[Tuple[str, WrappedUserEvent]]:
        consumer = self._consumer(self.USER_EVENTS_TOPIC, consumer_group_id)
        poll_result = consumer.poll(timeout_ms=timeout_ms, max_records=max_records)
        result = []
        seek_data = []
        for topic_partition in poll_result.values():
            for record in topic_partition:
                if until_ms and until_ms < record.timestamp:
                    seek_data.append((topic_partition, record.offset))
                    break
                result.append(_user_event_from_bytes(record.value))
        for partition, offset in seek_data:
            consumer.seek(partition, offset)
        return result

    def __del__(self):
        for composed_key, consumer in self.consumers.items():
            try:
                consumer.close()
                del self.consumers[composed_key]
            except Exception:
                pass
