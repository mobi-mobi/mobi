from abc import ABC, abstractmethod
from base64 import b85encode
from typing import Any, Dict, List, Optional

from mobi.dao.cache.common import UserHistoryCookie


def cache_key(name, **tags) -> str:
    def hash_val(v: Any) -> str:
        return b85encode(str(v).encode('utf-8')).decode('utf-8')

    return hash_val(name) + "||" + "||".join([f"{hash_val(key)}:{hash_val(val)}"
                                              for key, val in sorted(tags.items(), key=lambda x: x[0])])


def dynamic_counter_key(name: str, client: str, object_id: str):
    return cache_key(
        "__dynamic_counter__",
        counter_name=name,
        client=client,
        object_id=object_id
    )


class MobiCache(ABC):
    @abstractmethod
    def set(self, key: str, value, noreply=None, expire=0, retries: int = 1) -> None:
        pass

    @abstractmethod
    def set_int(self, key: str, value: int, noreply=None, expire=0, retries: int = 1) -> None:
        pass

    @abstractmethod
    def set_float(self, key: str, value: float, noreply=None, expire=0, retries: int = 1) -> None:
        pass

    @abstractmethod
    def set_many(self, values: Dict[str, str], expire=0, noreply=None, retries: int = 1):
        pass

    @abstractmethod
    def set_many_int(self, values: Dict[str, int], expire=0, noreply=None, retries: int = 1):
        pass

    @abstractmethod
    def get(self, key: str, default=None, retries: int = 1) -> Any:
        pass

    @abstractmethod
    def get_int(self, key: str, default: Optional[int] = None, retries: int = 1) -> Optional[int]:
        pass

    @abstractmethod
    def get_float(self, key: str, default: Optional[float] = None, retries: int = 1) -> Optional[float]:
        pass

    @abstractmethod
    def get_many(self, keys: List[str], retries: int = 2) -> Dict[str, Any]:
        pass

    @abstractmethod
    def get_many_int(self, keys: List[str], retries: int = 2) -> Dict[str, int]:
        pass

    @abstractmethod
    def delete(self, key, noreply=None, retries: int = 1):
        pass

    @abstractmethod
    def delete_many(self, keys, noreply=None, retries: int = 1):
        pass

    @abstractmethod
    def inc(self, key: str, value, noreply: bool = False, retries: int = 1):
        pass

    @abstractmethod
    def add(self, key: str, value, expire=0, noreply=None, retries: int = 1) -> Optional[bool]:
        pass

    # User history

    @classmethod
    def _user_history_key(cls, client: str, user_id: str):
        return cache_key("user_history", client=client, user_id=user_id)

    def get_user_history(self, client: str, user_id: str) -> Optional[UserHistoryCookie]:
        val = self.get(self._user_history_key(client, user_id))
        if not val:
            return None
        return UserHistoryCookie.from_bytes(val)

    def set_user_history(self, client: str, user_id: str, history: UserHistoryCookie, noreply: bool = True):
        self.set(self._user_history_key(client, user_id), history.as_bytes(), noreply=noreply, expire=history.ttl_sec())

    # Counters

    @abstractmethod
    def _inc_counter(self, client: str, name: str, tags: Dict[str, Any] = None, value: int = 1, current_ts: int = None,
                     length_sec: int = 60, accuracy_sec: int = 1, noreply: bool = True, retries: int = 2):
        pass

    @abstractmethod
    def _get_counter(self, client: str, name: str, tags: Dict[str, Any] = None, current_ts: int = None,
                     length_sec: int = 60, accuracy_sec: int = 1, noreply: bool = True, retries: int = 2):
        pass

    def inc_counter(self, client: str, name: str, tags: Dict[str, Any] = None, value: int = 1, current_ts: int = None,
                    length_sec: int = 60, accuracy_sec: int = 1, noreply: bool = True, retries: int = 2):
        self._inc_counter(
            client=client,
            name=name,
            tags=tags,
            value=value,
            current_ts=current_ts,
            length_sec=length_sec,
            accuracy_sec=accuracy_sec,
            noreply=noreply,
            retries=retries
        )

    def get_counter(self, client: str, name: str, tags: Dict[str, Any] = None, current_ts: int = None,
                    length_sec: int = 60, accuracy_sec: int = 1, noreply: bool = True, retries: int = 2):
        return self._get_counter(
            client=client,
            name=name,
            tags=tags,
            current_ts=current_ts,
            length_sec=length_sec,
            accuracy_sec=accuracy_sec,
            noreply=noreply,
            retries=retries
        )

    def inc_minute_counter(self, client: str, name: str, tags: Dict[str, Any] = None, value: int = 1,
                           current_ts: int = None, noreply: bool = True, retries: int = 2):
        self.inc_counter(
            client=client,
            name=name,
            tags=tags,
            value=value,
            current_ts=current_ts,
            length_sec=60,
            accuracy_sec=1,
            noreply=noreply,
            retries=retries
        )

    def get_minute_counter(self, client: str, name: str, tags: Dict[str, Any] = None, current_ts: int = None,
                           noreply: bool = True, retries: int = 2):
        self.get_counter(
            client=client,
            name=name,
            tags=tags,
            current_ts=current_ts,
            length_sec=60,
            accuracy_sec=1,
            noreply=noreply,
            retries=retries
        )

    def inc_hourly_counter(self, client: str, name: str, tags: Dict[str, Any] = None, value: int = 1,
                           current_ts: int = None, noreply: bool = True, retries: int = 2):
        self.inc_counter(
            client=client,
            name=name,
            tags=tags,
            value=value,
            current_ts=current_ts,
            length_sec=60 * 60,
            accuracy_sec=60,
            noreply=noreply,
            retries=retries
        )

    def get_hourly_counter(self, client: str, name: str, tags: Dict[str, Any] = None, current_ts: int = None,
                           noreply: bool = True, retries: int = 2):
        self.get_counter(
            client=client,
            name=name,
            tags=tags,
            current_ts=current_ts,
            length_sec=60 * 60,
            accuracy_sec=60,
            noreply=noreply,
            retries=retries
        )

    def inc_daily_counter(self, client: str, name: str, tags: Dict[str, Any] = None, value: int = 1,
                          current_ts: int = None, noreply: bool = True, retries: int = 2):
        self.inc_counter(
            client=client,
            name=name,
            tags=tags,
            value=value,
            current_ts=current_ts,
            length_sec=24 * 60 * 60,
            accuracy_sec=10 * 60,
            noreply=noreply,
            retries=retries
        )

    def get_daily_counter(self, client: str, name: str, tags: Dict[str, Any] = None, current_ts: int = None,
                          noreply: bool = True, retries: int = 2):
        self.get_counter(
            client=client,
            name=name,
            tags=tags,
            current_ts=current_ts,
            length_sec=24 * 60 * 60,
            accuracy_sec=10 * 60,
            noreply=noreply,
            retries=retries
        )


"""
This is historical code that supposed to be added to the repo.
At the end, we decided to move with cache_builder service that calculates everything


class TimecapCounter:
    def __init__(self, cache: MobiCache, name: str, base: TimecapCounterBase, num: int):
        if type(num) != int or num < 2:
            raise ValueError("Wrong num parameter value. A positive integer > 1 was expected.")

        if type(base) != TimecapCounterBase:
            raise ValueError("Wrong base value. An instance of TimecapCounterBase class was expected.")

        if not name:
            raise ValueError(f"Bad name value: {name}")

        self.cache = cache
        self.name = name
        self.base = base
        self.num = num
        self.last_offset = 0

        self.value_key = f"{name}||base:{base.value}||num:{num}||value"

    def _calc_offset(self) -> int:
        return (int(floor(datetime.utcnow().timestamp())) // self.base.value) % (self.num + 2)

    def _expire(self) -> int:
        return self.base.value * self.num

    def _key(self, offset: int):
        return f"{self.name}||base:{self.base.value}||num:{self.num}||offset:{offset}"

    @property
    def offset(self):
        offset = self._calc_offset()
        if self.last_offset < offset:
            self.cache.add(self._key(offset), 0, expire=self._expire(), noreply=False)
            self.last_offset = offset
        return offset

    def inc(self, value: int):
        self.cache.inc(self._key(self.offset), value, noreply=True)

    def value(self, cache: bool = True) -> int:
        v = self.cache.get(self.value_key)
        if v is not None:
            return int(v)

        all_values_keys = [self._key(offset) for offset in range(0, self.num + 2)]
        v = sum(map(int, self.cache.get_many(all_values_keys).values()))

        if cache:
            self.cache.set(self.value_key, v, noreply=True, expire=self.base.value // 3)

        return v


class TimecapCounterFamily(TimecapCounter):
    def __init__(self, cache: MobiCache, name: str, base: TimecapCounterBase, num: int):
        super().__init__(cache, name, base, num)
        self.item_last_offsets = defaultdict(lambda: 0)
        self.value_key = None

    def _item_key(self, item: str, offset: int):
        return super()._key(offset) + f"||item:{item}"

    def item_offset(self, item: str):
        offset = self._calc_offset()
        if self.item_last_offsets[item] < offset:
            self.cache.add(self._item_key(item, offset), 0, expire=self._expire(), noreply=False)
            self.item_last_offsets[item] = offset
        return offset

    def inc(self, value: int):
        raise NotImplemented("This method is not supported in TimecapCounterFamily. Use inc_item instead")

    def inc_item(self, item: str, value: int = 1):
        self.cache.inc(self._item_key(item, self.item_offset(item)), value, noreply=True)

    def item_value_key(self, item: str):
        return f"{self.name}||item:{item}||base:{self.base.value}||num:{self.num}||value"

    def value(self, cache: bool = True) -> int:
        raise NotImplemented("This method is not supported in TimecapCounterFamily. Use item_value instead")

    def known_items(self) -> Set[str]:


    def item_value(self, item: str, cache: bool = True) -> int:
        if item not in self.known_items:
            return 0

        v = self.cache.get(self.item_value_key(item))
        if v is not None:
            return int(v)

        all_values_keys = [self._item_key(item, offset) for offset in range(0, self.num + 2)]

        values = self.cache.get_many(all_values_keys).values()

        if not values:
            try:
                self.known_items.remove(item)
            except KeyError:
                pass

        v = sum(map(int, values))

        if cache:
            self.cache.set(self.item_value_key(item), v, noreply=True, expire=self.base.value // 3)

        return v

    def most_popular(self, k: int = 10) -> List[Tuple[str, int]]:
        most_popular_cache_key = f"{self.name}||base:{self.base.value}||num:{self.num}||k:{k}||most_popular"
        v = self.cache.get(most_popular_cache_key)
        if v is not None:
            return pickle.loads(v)

        result = sorted(
            [(item, self.item_value(item)) for item in list(self.known_items)], key=lambda x: x[1], reverse=True
        )[0:k]

        self.cache.set(most_popular_cache_key, pickle.dumps(result), noreply=True, expire=self.base.value // 3)

        return result
"""
