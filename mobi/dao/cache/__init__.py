from mobi.dao.cache.common import UserHistoryCookie
from mobi.dao.cache.interface import cache_key, dynamic_counter_key, MobiCache
from mobi.dao.cache.impl import MemcachedCache
