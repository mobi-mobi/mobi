from datetime import datetime
from pymemcache.client.base import Client
from typing import Any, Dict, List, Optional

from mobi.dao.cache.interface import cache_key, MobiCache


class MemcachedCache(MobiCache):
    def __init__(self, host='localhost', port=11211):
        self.client = Client((host, port), timeout=0.5)

    def set(self, key, value, noreply=None, expire=0, retries: int = 2) -> None:
        assert retries > 0
        for _ in range(retries):
            try:
                self.client.set(key=key, value=value, noreply=noreply, expire=expire)
                break
            except Exception:
                # IMPROVE: Sometimes it fails in File "site-packages/pymemcache/client/base.py", line 925, in _store_cmd
                # self.sock.sendall(b''.join(cmds))
                # UPD: It was decided to switch to more general exception, as memcached client still throws some strange
                # exceptions:
                # File "/home/mobi/.local/lib/python3.7/site-packages/pymemcache/client/base.py", line 1301, in _recv
                #   return sock.recv(size)
                # AttributeError: 'NoneType' object has no attribute 'recv'
                pass

    def set_int(self, key: str, value: int, noreply=None, expire=0, retries: int = 1) -> None:
        self.set(key, bytes(str(value), "utf-8"), noreply, expire, retries)

    def set_float(self, key: str, value: float, noreply=None, expire=0, retries: int = 1) -> None:
        self.set(key, bytes(str(value), "utf-8"), noreply, expire, retries)

    def set_many(self, values, expire=0, noreply=None, retries: int = 2):
        assert retries > 0
        for _ in range(retries):
            try:
                self.client.set_many(values, expire, noreply)
                break
            except Exception:
                # IMPROVE: Sometimes it fails in File "site-packages/pymemcache/client/base.py", line 925, in _store_cmd
                # self.sock.sendall(b''.join(cmds))
                pass

    def set_many_int(self, values: Dict[str, int], expire=0, noreply=None, retries: int = 1):
        self.set_many({k: bytes(str(v), "utf-8") for k, v in values.items()}, expire, noreply, retries)

    def get(self, key, default=None, retries: int = 2) -> Any:
        assert retries > 0
        for _ in range(retries):
            try:
                return self.client.get(key, default=default)
            except Exception:
                # IMPROVE: Sometimes memcached lib throws KeyError in File "site-packages/pymemcache/client/base.py",
                # line 836, in _extract_value.
                # We have no idea why, but this needs to be investigated
                pass

        return default

    def get_int(self, key: str, default: Optional[int] = None, retries: int = 1) -> Optional[int]:
        value = self.get(key, default, retries)
        if value is None:
            return None
        return int(value)

    def get_float(self, key: str, default: Optional[int] = None, retries: int = 1) -> Optional[float]:
        value = self.get(key, default, retries)
        if value is None:
            return None
        return float(value)

    def get_many(self, keys, retries: int = 2) -> Dict[str, Any]:
        assert retries > 0
        for _ in range(retries):
            try:
                return self.client.get_many(keys)
            except Exception:
                # IMPROVE: Sometimes memcached lib throws KeyError in File "site-packages/pymemcache/client/base.py",
                # line 836, in _extract_value.
                # We have no idea why, but this needs to be investigated
                pass

        return dict()

    def get_many_int(self, keys: List[str], retries: int = 2) -> Dict[str, int]:
        return {k: int(v) for k, v in self.get_many(keys, retries).items()}

    def delete(self, key, noreply=None, retries: int = 2):
        for _ in range(retries):
            try:
                self.client.delete(key, noreply)
                break
            except Exception:
                pass

    def delete_many(self, keys, noreply=None, retries: int = 2):
        for _ in range(retries):
            try:
                self.client.delete_many(keys, noreply)
                break
            except Exception:
                pass

    def inc(self, key: str, value, noreply: bool = False, retries: int = 2):
        for _ in range(retries):
            try:
                self.client.incr(key, value, noreply)
                break
            except Exception:
                pass

    def add(self, key: str, value, expire=0, noreply=None, retries: int = 2) -> Optional[bool]:
        for _ in range(retries):
            try:
                return self.client.add(key, value, expire, noreply)
            except Exception:
                pass

    # Counters

    @classmethod
    def _counter_cache_key(cls, client: str, name: str, timestamp: int, tags: Dict[str, Any] = None,
                           length_sec: int = 60, accuracy_sec: int = 1):
        tags = tags or {}
        return cache_key(f"__counter__{name}", **{**tags, "__ts": timestamp, "__c": client, "__l": length_sec,
                                                  "__a": accuracy_sec})

    def _inc_counter(self, client: str, name: str, tags: Dict[str, Any] = None, value: int = 1, current_ts: int = None,
                     length_sec: int = 60, accuracy_sec: int = 1, noreply: bool = True, retries: int = 2):
        current_ts = current_ts or int(datetime.utcnow().timestamp())
        key = self._counter_cache_key(client, name, accuracy_sec * (current_ts // accuracy_sec), tags, length_sec,
                                      accuracy_sec)
        expire_sec = length_sec + accuracy_sec + 1

        add_result = self.add(key, value, expire_sec, noreply=False, retries=retries)

        if add_result is not None and not add_result:
            # Values already exists - we need to incr
            self.inc(key, value, noreply, retries)

    def _get_counter(self, client: str, name: str, tags: Dict[str, Any] = None, current_ts: int = None,
                     length_sec: int = 60, accuracy_sec: int = 1, noreply: bool = True, retries: int = 2):
        current_ts = current_ts or int(datetime.utcnow().timestamp())
        step = 0
        timestamps = []
        while (current_ts // accuracy_sec) - step >= (current_ts - length_sec) // accuracy_sec:
            timestamps.append(accuracy_sec * ((current_ts // accuracy_sec) - step))
            step += 1

        timestamps.reverse()

        keys = [self._counter_cache_key(client, name, ts, tags, length_sec, accuracy_sec) for ts in timestamps]
        counters = self.get_many(keys, retries)

        smoothing_val = 0

        if accuracy_sec > 1:
            part_to_exclude = 1 + ((current_ts - length_sec) % accuracy_sec)
            ts_to_shrink = accuracy_sec * ((current_ts - length_sec) // accuracy_sec)
            key_to_shrink = self._counter_cache_key(client, name, ts_to_shrink, tags, length_sec, accuracy_sec)
            if part_to_exclude > 0 and counters.get(key_to_shrink):
                smoothing_val = int(part_to_exclude * int(counters[key_to_shrink]) / accuracy_sec)

        return sum(int(v) for _, v in counters.items() if v is not None) - smoothing_val
