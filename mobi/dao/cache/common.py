import pickle

from datetime import datetime, timedelta
from typing import Iterable, List, Optional

from mobi.core.models.event import Event


class UserHistoryCookie:
    EVENTS_LIMIT = 100
    EVENT_MAX_AGE = timedelta(days=30)

    CACHE_TTL = 3 * 24 * 60 * 60  # Store cookies for 3 days

    def __init__(self, events: Optional[List[Event]] = None, enforce_capping: bool = True):
        self.events: List[Event] = events or []
        self.cap(enforce_capping)

    def cap(self, enforce: bool = False, now_dt: Optional[datetime] = None):
        now = now_dt or datetime.utcnow()

        # Here we assume that events are always sorted
        if len(self.events) > 0 and (enforce or now - self.events[0].date > self.EVENT_MAX_AGE):
            self.events = [e for e in self.events if now - e.date < self.EVENT_MAX_AGE]

        if len(self.events) > 1 and (enforce or self.events[-2].date > self.events[-1].date):
            self.events.sort(key=lambda e: e.date)

        self.events = self.events[-self.EVENTS_LIMIT:]

    def last_events(self, num: int):
        return self.events[-num:]

    @classmethod
    def ttl_sec(cls) -> int:
        return cls.CACHE_TTL

    def as_dict(self) -> dict:
        return {
            "events": self.events
        }

    @classmethod
    def from_dict(cls, d: dict):
        inst = super(UserHistoryCookie, cls).__new__(cls)
        inst.__init__(d.get("events"), False)
        return inst

    def as_bytes(self) -> bytes:
        return pickle.dumps(self.as_dict())

    @classmethod
    def from_bytes(cls, b: bytes):
        return cls.from_dict(pickle.loads(b))

    def append(self, event: Event, cap_with_event_dt: bool = False):
        self.events.append(event)
        self.cap(now_dt=None if not cap_with_event_dt else event.date)

    @classmethod
    def all_product_ids_from_events(cls, events: Iterable[Event]) -> List[str]:
        result = set()
        for event in events:
            if event.product_id is not None:
                result.add(event.product_id)
            if event.basket_items is not None:
                for basket_item in event.basket_items:
                    result.add(basket_item.product_id)
        return list(result)

    def all_product_ids(self) -> List[str]:
        return self.all_product_ids_from_events(self.events)

    def __len__(self) -> int:
        return len(self.events)
