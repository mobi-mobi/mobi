from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Dict, Generator, List, Optional, Tuple, Union

from mobi.core.models import Product


@dataclass
class SearchResultItem(ABC):
    score: float


@dataclass
class ProductSearchResultItem(SearchResultItem):
    product_id: str


@dataclass
class BrandSearchResultItem(SearchResultItem):
    brand: str


@dataclass
class CategorySearchResultItem(SearchResultItem):
    category: str


class SearchDaoException(Exception):
    pass


class SearchDaoTimeoutException(SearchDaoException):
    pass


class MobiSearchDao(ABC):
    # WORKING WITH INDICES

    @abstractmethod
    def index_exists(self, client: str) -> bool:
        pass

    @abstractmethod
    def create_index(self, client: str):
        pass

    @abstractmethod
    def delete_index(self, client: str):
        pass

    @abstractmethod
    def refresh_index(self, client: str):
        pass

    # ADDING ELEMENTS TO INDEX

    @abstractmethod
    def index_product(self, client: str, product: Product, updated_ts: int = None, refresh_index: bool = False):
        pass

    @abstractmethod
    def index_brand(self, client: str, brand: str, updated_ts: int = None, refresh_index: bool = False):
        pass

    @abstractmethod
    def index_category(self, client: str, category: str, updated_ts: int = None, refresh_index: bool = False):
        pass

    # DELETING ELEMENTS FROM INDEX

    @abstractmethod
    def delete_product(self, client: str, product_id: str, refresh_index: bool = False):
        pass

    @abstractmethod
    def delete_brand(self, client: str, brand: str, refresh_index: bool = False):
        pass

    @abstractmethod
    def delete_category(self, client: str, category: str, refresh_index: bool = False):
        pass

    @abstractmethod
    def delete_fatigue_data(self, client: str, below_updated_ts: int) -> int:
        pass

    # SEARCH METHODS

    @abstractmethod
    def search_product(
            self,
            client: str,
            query: str,
            brands: List[str] = None,
            categories: List[str] = None,
            offset: int = 0,
            limit: int = 10,
            include_inactive: bool = False,
            include_not_in_stock: bool = False,
            request_timeout: Union[int, float] = 1
            ) -> Tuple[int, List[ProductSearchResultItem]]:
        pass

    @abstractmethod
    def search_brand(
            self,
            client: str,
            query: str,
            offset: int = 0,
            limit: int = 10,
            request_timeout: Union[int, float] = 1
            ) -> Tuple[int, List[BrandSearchResultItem]]:
        pass

    @abstractmethod
    def search_category(
            self,
            client: str,
            query: str,
            offset: int = 0,
            limit: int = 10,
            request_timeout: Union[int, float] = 1
            ) -> Tuple[int, List[CategorySearchResultItem]]:
        pass

    # READING (ALL) BRANDS AND CATEGORIES METHODS

    @abstractmethod
    def read_brands(self, client: str, limit: int = None, offset: int = None, request_timeout: int = 1,
                    _bulk_size: int = 1000) \
            -> Generator[str, None, None]:
        pass

    @abstractmethod
    def read_categories(self, client: str, limit: int = None, offset: int = None, request_timeout: int = 1,
                        _bulk_size: int = 1000) \
            -> Generator[str, None, None]:
        pass
