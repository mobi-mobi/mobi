import base64

from dataclasses import dataclass
from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ConflictError, ConnectionTimeout, NotFoundError
from typing import Generator, List, Tuple, Union

from mobi.core.models import Product
from mobi.dao.search.interface import MobiSearchDao, SearchDaoException, SearchDaoTimeoutException, \
    ProductSearchResultItem, BrandSearchResultItem, CategorySearchResultItem


@dataclass
class ESNode:
    host: str
    port: int


class ElasticDao(MobiSearchDao):
    def __init__(self, es_nodes: List[ESNode]):
        super().__init__()

        if not es_nodes:
            raise ValueError("At least one ES node should be provided")

        self.es = Elasticsearch(
            [f"{node.host}:{node.port}" for node in es_nodes],
            sniff_on_start=True,
            sniff_on_connection_fail=True,
            sniffer_timeout=60
        )

        self.es.cluster.put_settings(
            body={
                "persistent": {
                    "action.auto_create_index": "false"
                }
            }
        )

    # HELPERS - INDICES NAMES

    @classmethod
    def _es_string_to_keyword(cls, s: str) -> str:
        return str(base64.b16encode(bytes(s.strip(), "utf-8")), "utf-8").lower()

    @classmethod
    def _es_index_name(cls, entity: str, client: str) -> str:
        return f"mobi-search--{entity}--{cls._es_string_to_keyword(client)}"

    @classmethod
    def _es_product_index_name(cls, client: str) -> str:
        return cls._es_index_name("products", client)

    @classmethod
    def _es_brands_index_name(cls, client: str) -> str:
        return cls._es_index_name("brands", client)

    @classmethod
    def _es_categories_index_name(cls, client: str) -> str:
        return cls._es_index_name("categories", client)

    # HELPERS - ASSERTIONS

    @classmethod
    def _assert_client(cls, client: str):
        if not isinstance(client, str) or not client:
            raise ValueError("Client should be a non-empty string")

    @classmethod
    def _assert_product(cls, product: Product):
        if not isinstance(product, Product):
            raise ValueError("Can not index something that is not a Product instance")

        try:
            cls._assert_brand(product.brand)
        except ValueError:
            raise ValueError("Product brand is not a valid string or empty") from None

        try:
            cls._assert_title(product.title)
        except ValueError:
            raise ValueError("Product title is not a valid string or empty") from None

    @classmethod
    def _assert_title(cls, title: str):
        if not isinstance(title, str) or not cls.preprocess_title(title):
            raise ValueError("Title should be a non-empty string")

    @classmethod
    def _assert_brand(cls, brand: str):
        if not isinstance(brand, str) or not cls.preprocess_brand(brand):
            raise ValueError("Brand should be a non-empty string")

    @classmethod
    def _assert_category(cls, category: str):
        if not isinstance(category, str) or not cls.preprocess_category(category):
            raise ValueError("Category should be a non-empty string")

    @classmethod
    def _assert_search_query(cls, query: str):
        if not isinstance(query, str) or not cls.preprocess_search_query(query):
            raise ValueError("Query is not a string or empty")

    # HELPERS - TEXT

    @classmethod
    def preprocess_brand(cls, brand: str) -> str:
        return brand.strip()

    @classmethod
    def preprocess_brand_as_keyword(cls, brand: str) -> str:
        return cls._es_string_to_keyword(brand)

    @classmethod
    def preprocess_category(cls, category: str) -> str:
        return category.strip()

    @classmethod
    def preprocess_category_as_keyword(cls, category: str) -> str:
        return cls._es_string_to_keyword(category)

    @classmethod
    def preprocess_title(cls, title: str) -> str:
        return title.strip()

    @classmethod
    def preprocess_search_query(cls, query: str) -> str:
        return query.strip().replace("\"", "")

    @classmethod
    def _product_full_text(cls, product: Product) -> str:
        full_text = " ".join([term.lower() for term in product.brand.split() if term])
        brand_terms = {term for term in full_text.split()}

        new_title = " ".join([term.lower() for term in product.title.split()
                              if term and term.lower() not in brand_terms])

        if new_title:
            full_text += " " + new_title

        return full_text

    @classmethod
    def _product_to_doc(cls, product: Product, updated_ts: int) -> dict:
        return {
            "brand": cls.preprocess_brand(product.brand),
            "brand_as_keyword": cls.preprocess_brand_as_keyword(product.brand),
            "title": cls.preprocess_title(product.title),
            "full_text": cls._product_full_text(product),
            "is_active": product.active,
            "in_stock": product.in_stock,
            "updated_ts": updated_ts
        }

    @classmethod
    def _brand_to_doc(cls, brand: str, updated_ts: int) -> dict:
        return {
            "brand": cls.preprocess_brand(brand),
            "brand_as_keyword": cls.preprocess_brand_as_keyword(brand),
            "updated_ts": updated_ts
        }

    @classmethod
    def _category_to_doc(cls, category: str, updated_ts: int) -> dict:
        return {
            "category": cls.preprocess_category(category),
            "category_as_keyword": cls.preprocess_category_as_keyword(category),
            "updated_ts": updated_ts
        }

    # WORKING WITH INDEXES

    def index_exists(self, client: str) -> bool:
        products_index_exists = len(self.es.indices.get(self._es_product_index_name(client),
                                                        ignore_unavailable=True)) > 0
        brands_index_exists = len(self.es.indices.get(self._es_brands_index_name(client),
                                                      ignore_unavailable=True)) > 0
        categories_index_exists = len(self.es.indices.get(self._es_categories_index_name(client),
                                                          ignore_unavailable=True)) > 0

        return products_index_exists and brands_index_exists and categories_index_exists

    def create_index(self, client: str):
        self._assert_client(client)
        self.es.indices.create(
            index=self._es_product_index_name(client),
            body={
                "mappings": {
                    "dynamic": "strict",
                    "properties": {
                        "brand": {"type": "text", "norms": False},
                        "brand_as_keyword": {"type": "keyword"},
                        "title": {"type": "text", "norms": False},
                        "full_text": {"type": "text", "norms": False},
                        "is_active": {"type": "boolean"},
                        "in_stock": {"type": "boolean"},
                        "updated_ts": {"type": "long"}
                    }
                }
            }
        )

        self.es.indices.create(
            index=self._es_brands_index_name(client),
            body={
                "mappings": {
                    "dynamic": "strict",
                    "properties": {
                        "brand": {"type": "text", "norms": False},
                        "brand_as_keyword": {"type": "keyword"},
                        "updated_ts": {"type": "long"}
                    }
                }
            }
        )

        self.es.indices.create(
            index=self._es_categories_index_name(client),
            body={
                "mappings": {
                    "dynamic": "strict",
                    "properties": {
                        "category": {"type": "text", "norms": False},
                        "category_as_keyword": {"type": "keyword"},
                        "updated_ts": {"type": "long"}
                    }
                }
            }
        )

    def delete_index(self, client: str):
        self._assert_client(client)
        self.es.indices.delete(self._es_product_index_name(client), ignore_unavailable=True)
        self.es.indices.delete(self._es_brands_index_name(client), ignore_unavailable=True)
        self.es.indices.delete(self._es_categories_index_name(client), ignore_unavailable=True)

    def refresh_index(self, client: str):
        self._assert_client(client)
        indexes = f"{self._es_product_index_name(client)},{self._es_brands_index_name(client)}," \
                  f"{self._es_categories_index_name(client)}"
        self.es.indices.refresh(index=indexes, ignore_unavailable=False)

    # INDEXING

    def index_product(self, client: str, product: Product, updated_ts: int = None, refresh_index: bool = False):
        self._assert_client(client)
        self._assert_product(product)

        updated_ts = updated_ts if updated_ts is not None else int(datetime.utcnow().timestamp())

        try:
            self.es.index(
                index=self._es_product_index_name(client),
                id=product.product_id,
                body=self._product_to_doc(product, updated_ts),
                refresh="true" if refresh_index else "false",
                timeout="1s"
            )
        except NotFoundError:
            raise IndexError("Products index does not exist") from None

    def index_brand(self, client: str, brand: str, updated_ts: int = None, refresh_index: bool = False):
        self._assert_client(client)
        self._assert_brand(brand)

        updated_ts = updated_ts if updated_ts is not None else int(datetime.utcnow().timestamp())

        try:
            self.es.index(
                index=self._es_brands_index_name(client),
                id=brand,
                body=self._brand_to_doc(brand, updated_ts),
                refresh="true" if refresh_index else "false"
            )
        except NotFoundError:
            raise IndexError("Brands index does not exist") from None

    def index_category(self, client: str, category: str, updated_ts: int = None, refresh_index: bool = False):
        self._assert_client(client)
        self._assert_brand(category)

        updated_ts = updated_ts if updated_ts is not None else int(datetime.utcnow().timestamp())

        try:
            self.es.index(
                index=self._es_categories_index_name(client),
                id=category,
                body=self._category_to_doc(category, updated_ts),
                refresh="true" if refresh_index else "false"
            )
        except NotFoundError:
            raise IndexError("Categories index does not exist") from None

    def delete_product(self, client: str, product_id: str, refresh_index: bool = False):
        self._assert_client(client)

        self.es.delete(
            index=self._es_product_index_name(client),
            id=product_id,
            refresh="true" if refresh_index else "false",
            ignore=[404]
        )

    def delete_brand(self, client: str, brand: str, refresh_index: bool = False):
        self._assert_client(client)
        self._assert_brand(brand)

        self.es.delete(
            index=self._es_brands_index_name(client),
            id=brand,
            refresh="true" if refresh_index else "false",
            ignore=[404]
        )

    def delete_category(self, client: str, category: str, refresh_index: bool = False):
        self._assert_client(client)
        self._assert_category(category)

        self.es.delete(
            index=self._es_categories_index_name(client),
            id=category,
            refresh="true" if refresh_index else "false",
            ignore=[404]
        )

    def delete_fatigue_data(self, client: str, below_updated_ts: int) -> int:
        max_docs_per_request = 100
        retries_num_if_conflict = 3
        deleted = 0

        self.refresh_index(client)

        for index_name in (self._es_product_index_name(client), self._es_brands_index_name(client),
                           self._es_categories_index_name(client)):
            while True:
                try:
                    deletion_result = self.es.delete_by_query(
                        index=index_name,
                        max_docs=max_docs_per_request,
                        body={
                            "query": {
                                "range": {
                                    "updated_ts": {
                                        "lt": below_updated_ts
                                    }
                                }
                            }
                        }
                    )
                    if not deletion_result["deleted"]:
                        break
                    deleted += deletion_result["deleted"]
                except ConflictError:
                    if retries_num_if_conflict:
                        retries_num_if_conflict -= 1
                    else:
                        break

        return deleted

    # SEARCH METHODS - HELPERS

    @classmethod
    def _assert_es_search_results(cls, es_result: dict):
        if not isinstance(es_result, dict):
            raise ValueError(f"Can not parse elasticsearch results. Dict expected, but {type(es_result)} was given")

        if "hits" not in es_result or "hits" not in es_result["hits"]:
            raise ValueError(f"Can not find hits attribute in elastic search results")

        if "total" not in es_result["hits"] or "value" not in es_result["hits"]["total"]:
            raise ValueError("Can not find total matching documents in elastic search results")

    @classmethod
    def _es_result_to_search_products_results(cls, es_result: dict) -> Tuple[int, List[ProductSearchResultItem]]:
        result = []
        cls._assert_es_search_results(es_result)

        for hit in es_result["hits"]["hits"]:
            result.append(ProductSearchResultItem(score=hit["_score"], product_id=hit["_id"]))

        return es_result["hits"]["total"]["value"], result

    @classmethod
    def _es_result_to_search_brands_results(cls, es_result) -> Tuple[int, List[BrandSearchResultItem]]:
        result = []
        cls._assert_es_search_results(es_result)

        for hit in es_result["hits"]["hits"]:
            result.append(BrandSearchResultItem(score=hit["_score"], brand=hit["_source"]["brand"]))

        return es_result["hits"]["total"]["value"], result

    @classmethod
    def _es_result_to_search_categories_results(cls, es_result) -> Tuple[int, List[CategorySearchResultItem]]:
        result = []
        cls._assert_es_search_results(es_result)

        for hit in es_result["hits"]["hits"]:
            result.append(CategorySearchResultItem(score=hit["_score"], category=hit["_source"]["category"]))

        return es_result["hits"]["total"]["value"], result

    # SEARCH METHODS

    def search_product(
            self,
            client: str,
            query: str,
            brands: List[str] = None,
            categories: List[str] = None,
            offset: int = 0,
            limit: int = 10,
            include_inactive: bool = False,
            include_not_in_stock: bool = False,
            request_timeout: Union[int, float] = 1
            ) -> Tuple[int, List[ProductSearchResultItem]]:
        self._assert_client(client)
        self._assert_search_query(query)

        if (not isinstance(request_timeout, int) and not isinstance(request_timeout, float)) or request_timeout <= 0:
            raise ValueError("Request timeout should be a positive int/float")

        term_filters = []

        if not include_inactive:
            term_filters.append({"term": {"is_active": True}})
        if not include_not_in_stock:
            term_filters.append({"term": {"in_stock": True}})
        if brands:
            term_filters.append({"terms": {"brand_as_keyword": [self.preprocess_brand_as_keyword(brand)
                                                                for brand in brands]}})

        body = {
            "track_total_hits": True,
            "query": {
                "bool": {
                    "must": {
                        "dis_max": {
                            "queries": [
                                {
                                    "match_bool_prefix": {
                                        "full_text": self.preprocess_search_query(query)
                                    }
                                },
                                {
                                    "match": {
                                        "full_text": self.preprocess_search_query(query)
                                    }
                                }
                            ],
                            "tie_breaker": 0.0
                        }
                    },
                    "filter": term_filters
                }
            }
        }

        try:
            result = self.es.search(
                index=self._es_product_index_name(client),
                request_timeout=request_timeout,
                from_=offset,
                size=limit,
                body=body
            )
        except NotFoundError:
            return 0, []
        except ConnectionTimeout:
            raise SearchDaoTimeoutException(f"Read time out ({request_timeout}s)") from None

        try:
            return self._es_result_to_search_products_results(result)
        except Exception as e:
            raise SearchDaoException(f"Exception raised while parsing elasticsearch results: {str(e)}") from None

    def search_brand(
            self,
            client: str,
            query: str,
            offset: int = 0,
            limit: int = 10,
            request_timeout: Union[int, float] = 1
            ) -> Tuple[int, List[BrandSearchResultItem]]:
        self._assert_client(client)
        self._assert_search_query(query)

        if (not isinstance(request_timeout, int) and not isinstance(request_timeout, float)) or request_timeout <= 0:
            raise ValueError("Request timeout should be a positive int/float")

        try:
            result = self.es.search(
                index=self._es_brands_index_name(client),
                request_timeout=request_timeout,
                size=limit,
                from_=offset,
                body={
                    "track_total_hits": True,
                    "query": {
                        "match_bool_prefix": {"brand": self.preprocess_search_query(query)}
                    }
                }
            )
        except NotFoundError:
            return 0, []
        except ConnectionTimeout:
            raise SearchDaoTimeoutException(f"Read time out ({request_timeout}s)") from None

        try:
            return self._es_result_to_search_brands_results(result)
        except Exception as e:
            raise SearchDaoException(f"Exception raised while parsing elasticsearch results: {str(e)}") from None

    def search_category(
            self,
            client: str,
            query: str,
            offset: int = 0,
            limit: int = 10,
            request_timeout: Union[int, float] = 1
            ) -> Tuple[int, List[CategorySearchResultItem]]:
        self._assert_client(client)
        self._assert_search_query(query)

        if (not isinstance(request_timeout, int) and not isinstance(request_timeout, float)) or request_timeout <= 0:
            raise ValueError("Request timeout should be a positive int/float")

        try:
            result = self.es.search(
                index=self._es_categories_index_name(client),
                request_timeout=request_timeout,
                size=limit,
                from_=offset,
                body={
                    "track_total_hits": True,
                    "query": {
                        "match_bool_prefix": {"category": self.preprocess_search_query(query)}
                    }
                }
            )
        except NotFoundError:
            return 0, []
        except ConnectionTimeout:
            raise SearchDaoTimeoutException(f"Read time out ({request_timeout}s)") from None

        try:
            return self._es_result_to_search_categories_results(result)
        except Exception as e:
            raise SearchDaoException(f"Exception raised while parsing elasticsearch results: {str(e)}") from None

    # READING (ALL) BRANDS AND CATEGORIES METHODS

    def read_brands(self, client: str, limit: int = None, offset: int = None, request_timeout: int = 1,
                    _bulk_size: int = 1000) -> Generator[str, None, None]:
        self._assert_client(client)
        if not isinstance(_bulk_size, int) or _bulk_size <= 0:
            raise ValueError("_bulk_size must be a positive integer")

        current_offset = 0 if offset is None else offset
        should_stop = False
        returned = 0

        while not should_stop:
            try:
                _adjusted_bulk_size = _bulk_size if limit is None else min(_bulk_size, limit - returned)

                result = self.es.search(
                    index=self._es_brands_index_name(client),
                    request_timeout=request_timeout,
                    from_=current_offset,
                    size=_adjusted_bulk_size,
                    body={
                        "query": {
                            "match_all": {}
                        }
                    }
                )

                try:
                    _, results = self._es_result_to_search_brands_results(result)
                except Exception as e:
                    raise SearchDaoException(
                        f"Exception raised while parsing elasticsearch results: {str(e)}") from None

                for search_result in results:
                    if limit is None or returned < limit:
                        returned += 1
                        yield search_result.brand

                if returned == limit:
                    should_stop = True

                if results:
                    current_offset += _adjusted_bulk_size
                else:
                    should_stop = True
            except NotFoundError:
                should_stop = True
            except ConnectionTimeout:
                raise SearchDaoTimeoutException(f"Read time out ({request_timeout}s)") from None

    def read_categories(self, client: str, limit: int = None, offset: int = None, request_timeout: int = 1,
                        _bulk_size: int = 1000) \
            -> Generator[str, None, None]:
        self._assert_client(client)
        if not isinstance(_bulk_size, int) or _bulk_size <= 0:
            raise ValueError("_bulk_size must be a positive integer")

        current_offset = 0 if offset is None else offset
        should_stop = False
        returned = 0

        while not should_stop:
            try:
                _adjusted_bulk_size = _bulk_size if limit is None else min(_bulk_size, limit - returned)

                result = self.es.search(
                    index=self._es_categories_index_name(client),
                    request_timeout=request_timeout,
                    from_=current_offset,
                    size=_adjusted_bulk_size,
                    body={
                        "query": {
                            "match_all": {}
                        }
                    }
                )

                try:
                    _, results = self._es_result_to_search_categories_results(result)
                except Exception as e:
                    raise SearchDaoException(
                        f"Exception raised while parsing elasticsearch results: {str(e)}") from None

                for search_result in results:
                    if limit is None or returned < limit:
                        returned += 1
                        yield search_result.category

                if returned == limit:
                    should_stop = True

                if results:
                    current_offset += _bulk_size
                else:
                    should_stop = True
            except NotFoundError:
                should_stop = True
            except ConnectionTimeout:
                raise SearchDaoTimeoutException(f"Read time out ({request_timeout}s)") from None
