from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from typing import Generator, Iterable, List, Optional, Tuple

from mobi.core.models import Event
from mobi.dao import MobiDaoException


class MobiEventDao(ABC):
    MIN_EVENT_FATIGUE = timedelta(days=7)

    def __init__(self):
        pass

    @abstractmethod
    def create_event(self, client: str, event: Event):
        pass

    @abstractmethod
    def read_events(self, client: str, from_date: datetime = None, till_date: datetime = None,
                    sort_by: Iterable[Tuple[str, bool]] = None) -> Generator[Event, None, None]:
        pass

    def read_ordered_events(self, client: str, from_date: datetime = None, till_date: datetime = None) \
            -> Generator[Event, None, None]:
        return self.read_events(client, from_date, till_date, [('user_id', True), ('date', True)])

    def read_unique_user_ids(self, client, from_date: datetime = None, till_date: datetime = None,
                             sorted: bool = False) -> Generator[Tuple[str, datetime, datetime], None, None]:
        pass

    def read_user_sessions(self, client: str, max_time_gap: timedelta = timedelta(minutes=30),
                           from_date: datetime = None, till_date: datetime = None) \
            -> Generator[List[Event], None, None]:
        session: List[Event] = []
        for event in self.read_ordered_events(client, from_date, till_date):
            if session and (session[0].user_id != event.user_id or event.date - session[-1].date > max_time_gap):
                yield session
                session = []

            session.append(event)

        if session:
            yield session

    def get_n_user_ids(self, client: str, from_date: datetime, n: int = 10, till_date: datetime = None) -> List[str]:
        pass

    @abstractmethod
    def read_user_history(self, client: str, user_id: str, from_date: Optional[datetime] = None,
                          limit: int = 10) -> List[Event]:
        pass

    @abstractmethod
    def delete_all_events(self, client: str):
        pass

    def delete_fatigue_events(self, client: str, older_than: datetime) -> Optional[int]:
        if not isinstance(older_than, datetime):
            raise ValueError("Bad date parameter")
        if older_than > datetime.utcnow() - self.MIN_EVENT_FATIGUE:
            raise MobiDaoException(f"Can not delete events younger than {str(older_than)}.")
        return self._delete_fatigue_events(client, older_than)

    @abstractmethod
    def _delete_fatigue_events(self, client: str, older_than_checked: datetime) -> Optional[int]:
        pass
