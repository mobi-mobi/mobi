from datetime import datetime
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from random import choices
from typing import Generator, Iterable, List, Optional, Tuple

from mobi.core.models import Event
from mobi.dao import MobiDaoException
from mobi.dao.event.interface import MobiEventDao


class MongoEventDao(MobiEventDao):
    EVENTS_COLLECTION_NAME = "events"

    def __init__(self, host, port, db, collection=None):
        super().__init__()
        self.client = MongoClient(host, port)
        self.db = self.client[db]
        self.events = self.db[collection or self.EVENTS_COLLECTION_NAME]

    def __del__(self):
        try:
            self.client.close()
        except PyMongoError:
            pass

    def create_event(self, client: str, event: Event):
        self.events.insert_one({"client": client, **event.as_bson_dict()})

    def read_events(self, client: str, from_date: datetime = None, till_date: datetime = None,
                    sort_by: Iterable[Tuple[str, bool]] = None) -> Generator[Event, None, None]:
        filter_clause = {"client": client, "date": {}}
        if from_date is not None:
            filter_clause["date"]["$gte"] = from_date
        if till_date is not None:
            filter_clause["date"]["$lt"] = till_date
        if not filter_clause["date"]:
            del filter_clause["date"]

        sort_clause = None
        if sort_by is not None:
            sort_clause = [(field, int(order) * 2 - 1) for field, order in sort_by]

        for obj in self.events.find(filter=filter_clause, sort=sort_clause):
            yield Event.from_dict(obj)

    def read_unique_user_ids(self, client, from_date: datetime = None, till_date: datetime = None,
                             sorted: bool = False) -> Generator[Tuple[str, datetime, datetime], None, None]:
        filter_clause = {"client": client, "date": {}}
        if from_date is not None:
            filter_clause["date"]["$gte"] = from_date
        if till_date is not None:
            filter_clause["date"]["$lt"] = till_date
        if not filter_clause["date"]:
            del filter_clause["date"]

        pipeline = [
            {"$match": filter_clause},
            {"$group": {
                "_id": "$user_id",
                "min_date": {"$min": "$date"},
                "max_date": {"$max": "$date"}
            }}
        ]

        if sorted:
            pipeline.append({"$sort": {"_id": 1}})

        for obj in self.events.aggregate(pipeline):
            yield obj["_id"], obj["min_date"], obj["max_date"]

    def get_n_user_ids(self, client: str, from_date: datetime, n: int = 10, till_date: datetime = None) -> List[str]:
        filter_clause = {
            "client": client,
            "date": {"$gte": from_date}
        }
        if till_date:
            filter_clause["date"]["$lt"] = till_date

        objects = self.events.find(filter_clause, limit=10 * n, projection={"_id": 0, "user_id": 1})
        result = list({o["user_id"] for o in objects})
        if len(result) > n:
            result = choices(result, k=n)

        return result

    def read_user_history(self, client: str, user_id: str, from_date: Optional[datetime] = None,
                          limit: int = 10) -> List[Event]:
        filter_clause = {"client": client, "user_id": user_id}
        if from_date:
            filter_clause["date"] = {"$gte": from_date}

        result = self.events.find(filter=filter_clause, sort=[("date", -1)], limit=limit)
        return [Event.from_dict(obj) for obj in result]

    def delete_all_events(self, client: str):
        if not client:
            raise MobiDaoException(f"Wrong client value: \"{client}\"")
        self.events.delete_many({"client": client})

    def _delete_fatigue_events(self, client: str, older_than_checked: datetime) -> Optional[int]:
        deletion_result = self.events.delete_many({
            "client": client,
            "date": {
                "$lt": older_than_checked
            }
        })

        try:
            return deletion_result.deleted_count
        except PyMongoError:
            pass
