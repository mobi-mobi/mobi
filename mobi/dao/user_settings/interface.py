import re

from abc import ABC, abstractmethod
from enum import Enum
from typing import List, Optional

from mobi.dao.common import MobiDaoException

EMAIL_REGEX = re.compile("[^@]+@[^@]+\\.[^@]+")


class ReportSubscription(Enum):
    DAILY = "daily"
    WEEKLY = "weekly"


class MobiUserSettingsDao(ABC):
    @classmethod
    def _email_is_valid(cls, email: str):
        return EMAIL_REGEX.match(email)

    @classmethod
    def _assert_email(cls, email: str):
        if not email:
            raise MobiDaoException("Email is not provided")

        if type(email) != str:
            raise MobiDaoException(f"Wrong email datatype: {type(email)}")

        if not cls._email_is_valid(email):
            raise MobiDaoException(f"Wrong email format: {email}")

    # Low-level methods

    @abstractmethod
    def read_str_property(self, user: str, property_name: str, client: Optional[str] = None) -> Optional[str]:
        pass

    @abstractmethod
    def set_str_property(self, user: str, property_name: str, value: str, client: Optional[str] = None):
        pass

    @abstractmethod
    def read_int_property(self, user: str, property_name: str, client: Optional[str] = None) -> Optional[int]:
        pass

    @abstractmethod
    def set_int_property(self, user: str, property_name: str, value: int, client: Optional[str] = None):
        pass

    @abstractmethod
    def read_str_list_property(self, user: str, property_name: str, client: Optional[str] = None) \
            -> Optional[List[str]]:
        pass

    @abstractmethod
    def set_str_list_property(self, user: str, property_name: str, value: List[str], client: Optional[str] = None):
        pass

    @abstractmethod
    def read_int_list_property(self, user: str, property_name: str, client: Optional[str] = None) \
            -> Optional[List[int]]:
        pass

    @abstractmethod
    def set_int_list_property(self, user: str, property_name: str, value: List[int], client: Optional[str] = None):
        pass

    @abstractmethod
    def read_object_property(self, user: str, property_name: str, client: Optional[str] = None) -> Optional[dict]:
        pass

    @abstractmethod
    def set_object_property(self, user: str, property_name: str, value: dict, client: Optional[str] = None):
        pass

    @abstractmethod
    def delete_property(self, user: str, property_name: str, client: Optional[str] = None):
        pass

    @abstractmethod
    def delete_all_properties(self, user: str):
        pass

    # High-level methods

    def read_report_subscriptions_list(self, user: str, client: str) -> List[ReportSubscription]:
        subscriptions = self.read_str_list_property(user, "report_subscriptions", client)
        return [] if subscriptions is None else [ReportSubscription(s) for s in subscriptions]

    def add_report_subscription(self, user: str, client: str, report_subscription: ReportSubscription):
        subscriptions = set(r.value for r in self.read_report_subscriptions_list(user, client))
        subscriptions.add(report_subscription.value)
        self.set_str_list_property(user, "report_subscriptions", sorted(subscriptions), client)

    def remove_report_subscription(self, user: str, client: str, report_subscription: ReportSubscription):
        subscriptions = set(self.read_report_subscriptions_list(user, client))
        subscriptions.discard(report_subscription)
        self.set_str_list_property(user, "report_subscriptions", sorted([r.value for r in subscriptions]), client)

    def get_last_report_email_id(self, user: str, client: str, report_subscription: ReportSubscription) \
            -> Optional[str]:
        last_email_ids = self.read_object_property(user, "last_email_ids", client)
        if last_email_ids is None:
            return None
        return last_email_ids.get("report_" + report_subscription.value)

    def update_last_report_email_id(self, user: str, client: str, report_subscription: ReportSubscription,
                                    email_id: str):
        last_email_ids = self.read_object_property(user, "last_email_ids", client)
        if last_email_ids is None:
            last_email_ids = dict()
        last_email_ids["report_" + report_subscription.value] = email_id
        self.set_object_property(user, "last_email_ids", last_email_ids, client)
