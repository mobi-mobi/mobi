from base64 import b32encode
from pymongo import MongoClient
from typing import Any, Optional, List
from string import ascii_letters, digits

from mobi.dao.user_settings.interface import MobiUserSettingsDao


class MongoUserSettingsDao(MobiUserSettingsDao):
    USER_SETTINGS_COLLECTION_NAME = "user_settings"
    ALLOWED_KEY_SYMBOLS = set(ascii_letters + digits + "_")

    def __init__(self, host, port, db, user_settings_collection=None):
        super().__init__()
        self.client = MongoClient(host, port)
        self.db = self.client[db]
        self.user_settings = self.db[user_settings_collection or self.USER_SETTINGS_COLLECTION_NAME]

    @classmethod
    def _to_key(cls, s: str):
        allowed_key = "".join(ch for ch in s if ch in cls.ALLOWED_KEY_SYMBOLS)
        if s and not s.startswith("_") and allowed_key == s:
            return s
        return "_" + allowed_key + "_" + b32encode(s.encode('utf-8')).decode('utf-8')

    @classmethod
    def _projection(cls, property_name: str, client: Optional[str] = None):
        return cls._to_key(property_name) if not client else (cls._to_key(client) + "." + cls._to_key(property_name))

    def _read_property(self, user: str, property_name: str, client: Optional[str] = None) -> Optional[Any]:
        projection = self._projection(property_name, client)
        find_result = self.user_settings.find_one(
            filter={"user": user},
            projection=[projection]
        )
        if find_result is None:
            return None
        if client is None:
            return find_result.get(self._to_key(property_name))
        else:
            client_dict = find_result.get(self._to_key(client))
            if client_dict is None:
                return None
            return client_dict.get(self._to_key(property_name))

    def _set_property(self, user: str, property_name: str, value: Any, client: Optional[str] = None):
        projection = self._projection(property_name, client)
        self.user_settings.update_one(
            filter={"user": user},
            update={"$set": {projection: value}},
            upsert=True
        )

    def read_str_property(self, user: str, property_name: str, client: Optional[str] = None) -> Optional[str]:
        return self._read_property(user, property_name, client)

    def set_str_property(self, user: str, property_name: str, value: str, client: Optional[str] = None):
        assert isinstance(value, str)
        self._set_property(user, property_name, value, client)

    def read_int_property(self, user: str, property_name: str, client: Optional[str] = None) -> Optional[int]:
        return self._read_property(user, property_name, client)

    def set_int_property(self, user: str, property_name: str, value: int, client: Optional[str] = None):
        assert isinstance(value, int)
        self._set_property(user, property_name, value, client)

    def read_str_list_property(self, user: str, property_name: str, client: Optional[str] = None) \
            -> Optional[List[str]]:
        return self._read_property(user, property_name, client)

    def set_str_list_property(self, user: str, property_name: str, value: List[str], client: Optional[str] = None):
        assert isinstance(value, list)
        for v in value:
            assert isinstance(v, str)
        self._set_property(user, property_name, value, client)

    def read_int_list_property(self, user: str, property_name: str, client: Optional[str] = None) \
            -> Optional[List[int]]:
        return self._read_property(user, property_name, client)

    def set_int_list_property(self, user: str, property_name: str, value: List[int], client: Optional[str] = None):
        assert isinstance(value, list)
        for v in value:
            assert isinstance(v, int)
        self._set_property(user, property_name, value, client)

    def read_object_property(self, user: str, property_name: str, client: Optional[str] = None) -> Optional[dict]:
        return self._read_property(user, property_name, client)

    def set_object_property(self, user: str, property_name: str, value: dict, client: Optional[str] = None):
        assert isinstance(value, dict)
        for key, value_ in value.items():
            assert isinstance(key, str) and len(key)
            assert isinstance(key, str)
            assert value_ is None or isinstance(value_, str) or isinstance(value_, int) or isinstance(value_, float)
        self._set_property(user, property_name, value, client)

    def delete_property(self, user: str, property_name: str, client: Optional[str] = None):
        projection = self._projection(property_name, client)
        self.user_settings.update_one(
            filter={"user": user},
            update={"$unset": {projection: ""}},
            upsert=False
        )

    def delete_all_properties(self, user: str):
        self.user_settings.delete_many({"user": user})
