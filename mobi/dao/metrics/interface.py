from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from typing import Dict, Generator, List, Optional, Tuple


class MobiMetricsDao(ABC):
    MIN_DAYS_DATA_FATIGUE = timedelta(days=35)
    MIN_HOURS_DATA_FATIGUE = timedelta(days=3)
    MIN_MINUTES_DATA_FATIGUE = timedelta(days=1)

    @abstractmethod
    def inc_metrics_point(self, client: str, name: str, tags: Dict[str, str],
                          base: int, timestamp: int, inc: float = 1.0):
        pass

    @abstractmethod
    def set_metrics_point(self, client: str, name: str, tags: Dict[str, str],
                          base: int, timestamp: int, value: float):
        pass

    @abstractmethod
    def get_latest_point_timestamp(self, client: str, name, tags: Optional[Dict[str, str]], base: int) -> Optional[int]:
        pass

    def get_latest_point_value(self, client: str, name, tags: Optional[Dict[str, str]], base: int,
                               no_later_than_bases_num: Optional[int] = 2) -> Optional[float]:
        latest_timestamp = self.get_latest_point_timestamp(client, name, tags, base)
        if latest_timestamp is None:
            return None
        if no_later_than_bases_num is not None \
                and latest_timestamp < int(datetime.utcnow().timestamp()) - (no_later_than_bases_num * base):
            return None
        result = 0.0
        for _, item_value in self.get_metrics(client, name, tags, base, latest_timestamp, latest_timestamp + 1):
            result += item_value

        return result

    @abstractmethod
    def get_metrics_tags(self, client: str, name: str, tags: Dict[str, str], base: int,
                         timestamp_from: int, timestamp_until: int):
        pass

    @abstractmethod
    def get_metrics_tag_values(self, client: str, name: str, tag: str, tags: Dict[str, str], base: int,
                               timestamp_from: int, timestamp_until: int):
        pass

    @abstractmethod
    def get_metrics(self, client: str, name: str, tags: Optional[Dict[str, str]],
                    base: int, timestamp_from: int, timestamp_until: int) \
            -> Generator[Tuple[int, float], None, None]:
        pass

    def get_metrics_last(self, client: str, name: str, tags: Optional[Dict[str, str]], timestamp_from: int,
                         bases: Optional[List[int]] = None):
        pass

    @abstractmethod
    def delete_all(self, client: str):
        pass

    def delete_fatigue_data(self, client: str, base: int, till_ts: int) -> Optional[int]:
        if base == 60:
            till_ts_limit = datetime.utcnow() - self.MIN_MINUTES_DATA_FATIGUE
        elif base == 60 * 60:
            till_ts_limit = datetime.utcnow() - self.MIN_HOURS_DATA_FATIGUE
        elif base == 24 * 60 * 60:
            till_ts_limit = datetime.utcnow() - self.MIN_DAYS_DATA_FATIGUE
        else:
            raise ValueError(f"Base should be one of 60/360/86400")
        if till_ts > till_ts_limit.timestamp():
            raise ValueError(f"Can not delete fresh data")
        return self._delete_fatigue_data(client, base, till_ts)

    @abstractmethod
    def _delete_fatigue_data(self, client: str, base_checked: int, till_ts_checked: int) -> Optional[int]:
        pass
