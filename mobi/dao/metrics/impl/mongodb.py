import bson

from datetime import datetime
from math import floor
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from typing import Optional, Dict, Generator, List, Tuple

from mobi.dao.metrics.interface import MobiMetricsDao


class MongoMetricsDao(MobiMetricsDao):
    METRICS_COLLECTION_NAME = "metrics"

    def __init__(self, host, port, db, collection=None):
        self.client = MongoClient(host, port)
        self.db = self.client[db]
        self.metrics = self.db[collection or self.METRICS_COLLECTION_NAME]

    def __del__(self):
        try:
            self.client.close()
        except PyMongoError:
            pass

    def inc_metrics_point(self, client: str, name: str, tags: Dict[str, str], base: int, timestamp: int,
                          inc: float = 1.0):
        self.metrics.update_one(
            filter={
                "client": client,
                "name": name,
                "tags": tags,
                "base": base,
                "timestamp": bson.Int64(base * (timestamp // base))
            },
            upsert=True,
            update={
                "$inc": {
                    "value": inc
                }
            }
        )

    def set_metrics_point(self, client: str, name: str, tags: Dict[str, str], base: int, timestamp: int, value: float):
        self.metrics.update_one(
            filter={
                "client": client,
                "name": name,
                "tags": tags,
                "base": base,
                "timestamp": bson.Int64(base * (timestamp // base))
            },
            upsert=True,
            update={
                "$set": {
                    "value": value
                }
            }
        )

    def get_latest_point_timestamp(self, client: str, name, tags: Optional[Dict[str, str]], base: int) -> Optional[int]:
        find_clause = {
            "client": client,
            "name": name,
            "base": base
        }

        if tags:
            for key, value in tags.items():
                find_clause[f"tags.{key}"] = value

        point = self.metrics.find_one(
            filter=find_clause,
            projection={"_id": 0, "timestamp": 1},
            sort=[("timestamp", -1)]
        )

        if point is None:
            return None
        return point["timestamp"]

    def get_metrics_tags(self, client: str, name: str, tags: Dict[str, str], base: int, timestamp_from: int,
                         timestamp_until: int):
        find_clause = {
            "client": client,
            "name": name,
            "base": base,
            "timestamp": {
                "$gte": bson.Int64(timestamp_from),
                "$lt": bson.Int64(timestamp_until)
            }
        }

        if tags:
            for key, value in tags.items():
                find_clause[f"tags.{key}"] = value

        pipeline = [
            {"$match": find_clause},
            {"$project": {
                "tag_names_obj": {"$objectToArray": "$tags"}
            }},
            {"$project": {
                "tag_names": "$tag_names_obj.k"
            }},
            {"$group": {
                "_id": None,
                "all_tag_names": {
                    "$addToSet": "$tag_names"
                }
            }}
        ]

        result = list(self.metrics.aggregate(pipeline))

        if not result:
            return []
        else:
            return result[0]["all_tag_names"]

    def get_metrics_tag_values(self, client: str, name: str, tag: str, tags: Dict[str, str], base: int,
                               timestamp_from: int, timestamp_until: int):
        find_clause = {
            "client": client,
            "name": name,
            "base": base,
            "timestamp": {
                "$gte": bson.Int64(timestamp_from),
                "$lt": bson.Int64(timestamp_until)
            }
        }

        if tags:
            for key, value in tags.items():
                find_clause[f"tags.{key}"] = value

        pipeline = [
            {"$match": find_clause},
            {"$project": {
                "tag_names_obj": {"$objectToArray": "$tags"}
            }},
            {"$unwind": "$tag_names_obj"},
            {"$match": {
                "tag_names_obj.k": tag
            }},
            {"$project": {
                "tag_values": "$tag_names_obj.v"
            }},
            {"$group": {
                "_id": None,
                "all_tag_values": {
                    "$addToSet": "$tag_values"
                }
            }}
        ]

        result = list(self.metrics.aggregate(pipeline))

        if not result:
            return []
        else:
            return result[0]["all_tag_values"]

    def get_metrics(self, client: str, name: str, tags: Optional[Dict[str, str]], base: int, timestamp_from: int,
                    timestamp_until: int) -> Generator[Tuple[int, float], None, None]:
        find_clause = {
            "client": client,
            "name": name,
            "base": base,
            "timestamp": {
                "$gte": bson.Int64(timestamp_from),
                "$lt": bson.Int64(timestamp_until)
            }
        }
        if tags:
            for key, value in tags.items():
                find_clause[f"tags.{key}"] = value

        pipeline = [
            {"$match": find_clause},
            {"$group": {
                "_id": {
                    "name": "$name",
                    "timestamp": "$timestamp"
                },
                "value": {
                    "$sum": "$value"
                }
            }},
            {"$sort": {"_id.timestamp": 1}}
        ]

        for item in self.metrics.aggregate(pipeline):
            yield item["_id"]["timestamp"], item["value"]

    @staticmethod
    def _split_timeframe(timestamp_from: int, base: int):
        interval_from = (timestamp_from // base) * base
        if interval_from + base < timestamp_from:
            return None
        return interval_from + base

    def get_metrics_last(self, client: str, name: str, tags: Optional[Dict[str, str]], timestamp_from: int,
                         bases: Optional[List[int]] = None):
        bases = bases or (60, 60 * 60, 24 * 60 * 60)
        timestamp_from = (timestamp_from // 60) * 60
        timestamp_until = int(floor(datetime.utcnow().timestamp()))
        result = .0
        for base in sorted(bases, reverse=True):
            interval_from = self._split_timeframe(timestamp_from, base)
            if interval_from:
                for _, v in self.get_metrics(client, name, tags, base, interval_from, timestamp_until):
                    result += v
                timestamp_until = interval_from
        return result

    def delete_all(self, client: str):
        self.metrics.delete_many({"client": client})

    def _delete_fatigue_data(self, client: str, base_checked: int, till_ts_checked: int) -> Optional[int]:
        deletion_result = self.metrics.delete_many({
            "client": client,
            "base": base_checked,
            "timestamp": {
                "$lt": till_ts_checked
            }
        })

        try:
            return deletion_result.deleted_count
        except PyMongoError:
            pass
