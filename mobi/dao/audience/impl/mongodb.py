from pymongo import MongoClient
from pymongo.errors import PyMongoError
from typing import List, Optional

from mobi.core.models import Audience, AudienceSplitTree, AudiencesUnion
from mobi.dao.audience.interface import MobiAudienceDao


class MongoAudienceDao(MobiAudienceDao):
    AUDIENCES_COLLECTION_NAME = "audiences"
    AUDIENCE_SPLIT_TREES_COLLECTION_NAME = "audience_split_trees"
    AUDIENCE_UNIONS_COLLECTION_NAME = "audience_unions"

    def __init__(self, host, port, db, audiences_collection=None, audience_split_trees_collection=None,
                 audience_unions_collection=None):
        self.client = MongoClient(host, port)
        self.db = self.client[db]
        self.audiences = self.db[audiences_collection or self.AUDIENCES_COLLECTION_NAME]
        self.audience_split_trees = self.db[audience_split_trees_collection
                                            or self.AUDIENCE_SPLIT_TREES_COLLECTION_NAME]
        self.audience_unions = self.db[audience_unions_collection or self.AUDIENCE_UNIONS_COLLECTION_NAME]

    # AUDIENCE SPLIT TREES

    def create_audience_split_tree(self, client: str, audience_split_tree: AudienceSplitTree):
        if audience_split_tree.split_tree_id is None:
            raise ValueError("Audience split tree id is not defined")
        self.audience_split_trees.find_one_and_replace({"client": client,
                                                        "split_tree_id": audience_split_tree.split_tree_id},
                                                       {"client": client, **audience_split_tree.as_bson_dict()},
                                                       upsert=True)

    def delete_audience_split_tree(self, client: str, audience_split_id: str):
        self.audience_split_trees.delete_one({"client": client, "split_tree_id": audience_split_id})

    def read_audience_split_tree(self, client: str, audience_split_id: str) -> Optional[AudienceSplitTree]:
        result = self.audience_split_trees.find_one({"client": client, "split_tree_id": audience_split_id})
        if result is None:
            return None
        return AudienceSplitTree.from_dict(result)

    def read_all_audience_split_trees(self, client: str) -> List[AudienceSplitTree]:
        return [AudienceSplitTree.from_dict(st) for st in self.audience_split_trees.find({"client": client})]

    def delete_all_audience_split_trees(self, client: str):
        self.audience_split_trees.delete_many({"client": client})

    # AUDIENCES

    def create_audience(self, client: str, audience: Audience):
        self.audiences.find_one_and_replace({"client": client, "audience_id": audience.audience_id},
                                            {"client": client, **audience.as_bson_dict()}, upsert=True)

    def read_audience(self, client: str, audience_id: str) -> Optional[Audience]:
        result = self.audiences.find_one({"client": client, "audience_id": audience_id})
        if result is None:
            return None
        return Audience.from_dict(result)

    def read_all_audiences(self, client: str) -> List[Audience]:
        return [Audience.from_dict(a) for a in self.audiences.find({"client": client})]

    def delete_audience(self, client: str, audience_id: str):
        self.audiences.delete_one({"client": client, "audience_id": audience_id})

    def delete_all_audiences(self, client: str):
        self.audiences.delete_many({"client": client})

    # AUDIENCE UNIONS

    def create_audiences_union(self, client: str, audiences_union: AudiencesUnion):
        if audiences_union.audiences_union_id is None:
            raise ValueError("Audiences union id is not defined")
        self.audience_unions.find_one_and_replace({"client": client,
                                                   "audiences_union_id": audiences_union.audiences_union_id},
                                                  {"client": client, **audiences_union.as_bson_dict()}, upsert=True)

    def read_audiences_union(self, client: str, audiences_union_id: str) -> Optional[AudiencesUnion]:
        result = self.audience_unions.find_one({"client": client, "audiences_union_id": audiences_union_id})
        if result is None:
            return None
        return AudiencesUnion.from_dict(result)

    def read_all_audiences_unions(self, client: str) -> List[AudiencesUnion]:
        return [AudiencesUnion.from_dict(au) for au in self.audience_unions.find({"client": client})]

    def delete_audiences_union(self, client: str, audiences_union_id: str):
        self.audience_unions.delete_one({"client": client, "audiences_union_id": audiences_union_id})

    def delete_all_audiences_unions(self, client: str):
        self.audience_unions.delete_many({"client": client})
