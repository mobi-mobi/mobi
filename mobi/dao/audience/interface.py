from abc import ABC, abstractmethod
from typing import List, Optional

from mobi.core.models import Audience, AudienceSplitTree, AudiencesUnion


class MobiAudienceDao(ABC):
    @abstractmethod
    def create_audience_split_tree(self, client: str, audience_split_tree: AudienceSplitTree):
        pass

    @abstractmethod
    def delete_audience_split_tree(self, client: str, audience_split_id: str):
        pass

    @abstractmethod
    def read_audience_split_tree(self, client: str, audience_split_id: str) -> Optional[AudienceSplitTree]:
        pass

    @abstractmethod
    def read_all_audience_split_trees(self, client: str) -> List[AudienceSplitTree]:
        pass

    @abstractmethod
    def delete_all_audience_split_trees(self, client: str):
        pass

    @abstractmethod
    def create_audience(self, client: str, audience: Audience):
        pass

    @abstractmethod
    def read_audience(self, client: str, audience_id: str) -> Optional[Audience]:
        pass

    @abstractmethod
    def read_all_audiences(self, client: str) -> List[Audience]:
        pass

    @abstractmethod
    def delete_audience(self, client: str, audience_id: str):
        pass

    @abstractmethod
    def delete_all_audiences(self, client: str):
        pass

    @abstractmethod
    def create_audiences_union(self, client: str, audiences_union: AudiencesUnion):
        pass

    @abstractmethod
    def read_audiences_union(self, client: str, audiences_union_id: str) -> Optional[AudiencesUnion]:
        pass

    @abstractmethod
    def read_all_audiences_unions(self, client: str) -> List[AudiencesUnion]:
        pass

    @abstractmethod
    def delete_audiences_union(self, client: str, audiences_union_id: str):
        pass

    @abstractmethod
    def delete_all_audiences_unions(self, client: str):
        pass
