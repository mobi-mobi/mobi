from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from typing import Generator, Iterable, Optional, Tuple

from mobi.core.models import AttributedEvent, AttributedRecommendation, AttributionType, Recommendation
from mobi.dao import MobiDaoException


class MobiRecologDao(ABC):
    MIN_ATTRIBUTED_EVENTS_FATIGUE = timedelta(days=7)
    MIN_ATTRIBUTED_RECOMMENDATIONS_FATIGUE = timedelta(days=7)
    MIN_UNMATCHED_RECOMMENDATIONS_FATIGUE = timedelta(days=7)
    MIN_RECOLOG_FATIGUE = timedelta(days=7)

    def __init__(self):
        pass

    # Recommendations

    @abstractmethod
    def log_recommendation(self, client: str, recommendation: Recommendation):
        pass

    @abstractmethod
    def read_recommendations(self, client: str, from_date: datetime = None, till_date: datetime = None,
                             sort_by: Iterable[Tuple[str, bool]] = None) \
            -> Generator[Recommendation, None, None]:
        pass

    def read_ordered_recommendations(self, client: str, from_date: datetime = None, till_date: datetime = None) \
            -> Generator[Recommendation, None, None]:
        return self.read_recommendations(client, from_date, till_date, [('user_id', True), ('date', True)])

    @abstractmethod
    def delete_all_recommendations(self, client: str):
        pass

    @abstractmethod
    def _delete_fatigue_recologs(self, client: str, older_than_checked: datetime) -> Optional[int]:
        pass

    def delete_fatigue_recologs(self, client: str, older_than: datetime) -> Optional[int]:
        if not isinstance(older_than, datetime):
            raise ValueError("Bad date parameter")
        if older_than > datetime.utcnow() - self.MIN_RECOLOG_FATIGUE:
            raise MobiDaoException(f"Can not delete recologs younger than {str(older_than)}.")
        return self._delete_fatigue_recologs(client, older_than)

    # Attributed events

    @abstractmethod
    def log_attributed_event(self, client: str, attributed_event: AttributedEvent):
        pass

    @abstractmethod
    def read_attributed_events(self, client, attribution_type: AttributionType, from_date: datetime = None,
                               till_date: datetime = None, sort_by: Iterable[Tuple[str, bool]] = None) \
            -> Generator[AttributedEvent, None, None]:
        pass

    def read_ordered_attributed_events(self, client: str, attribution_type: AttributionType, from_date: datetime = None,
                                       till_date: datetime = None) -> Generator[AttributedEvent, None, None]:
        return self.read_attributed_events(client, attribution_type, from_date, till_date,
                                           [('user_id', True), ('date', True)])

    def get_latest_attributed_event(self, client, attribution_type: AttributionType) -> Optional[AttributedEvent]:
        pass

    @abstractmethod
    def delete_all_attributed_events(self, client: str, attribution_type: Optional[AttributionType] = None):
        pass

    @abstractmethod
    def _delete_fatigue_attributed_events(self, client: str, older_than_checked: datetime) -> Optional[int]:
        pass

    def delete_fatigue_attributed_events(self, client: str, older_than: datetime) -> Optional[int]:
        if not isinstance(older_than, datetime):
            raise ValueError("Bad date parameter")
        if older_than > datetime.utcnow() - self.MIN_ATTRIBUTED_EVENTS_FATIGUE:
            raise MobiDaoException(f"Can not delete attributed events younger than {str(older_than)}.")
        return self._delete_fatigue_attributed_events(client, older_than)

    # Attributed recommendations

    @abstractmethod
    def log_attributed_recommendation(self, client: str, attributed_recommendation: AttributedRecommendation):
        pass

    @abstractmethod
    def read_attributed_recommendations(self, client, attribution_type: AttributionType, from_date: datetime = None,
                                        till_date: datetime = None, sort_by: Iterable[Tuple[str, bool]] = None) \
            -> Generator[AttributedRecommendation, None, None]:
        pass

    def read_ordered_attributed_recommendations(self, client: str, attribution_type: AttributionType,
                                                from_date: datetime = None, till_date: datetime = None) \
            -> Generator[AttributedRecommendation, None, None]:
        return self.read_attributed_recommendations(client, attribution_type, from_date, till_date,
                                                    [('user_id', True), ('date', True)])

    @abstractmethod
    def delete_all_attributed_recommendations(self, client: str, attribution_type: Optional[AttributionType] = None):
        pass

    @abstractmethod
    def _delete_fatigue_attributed_recommendations(self, client: str, older_than_checked: datetime) -> Optional[int]:
        pass

    def delete_fatigue_attributed_recommendations(self, client: str, older_than: datetime) -> Optional[int]:
        if not isinstance(older_than, datetime):
            raise ValueError("Bad date parameter")
        if older_than > datetime.utcnow() - self.MIN_ATTRIBUTED_RECOMMENDATIONS_FATIGUE:
            raise MobiDaoException(f"Can not delete attributed recommendations younger than {str(older_than)}.")
        return self._delete_fatigue_attributed_recommendations(client, older_than)

    # Unmatched recommendations - TO BE DELETED?

    @abstractmethod
    def log_unmatched_recommendation(self, client: str, attribution_type: AttributionType,
                                     recommendation: Recommendation):
        pass

    @abstractmethod
    def read_unmatched_recommendations(self, client: str, attribution_type: AttributionType, from_date: datetime = None,
                                       till_date: datetime = None, sort_by: Iterable[Tuple[str, bool]] = None) \
            -> Generator[Recommendation, None, None]:
        pass

    def read_ordered_unmatched_recommendations(self, client: str, attribution_type: AttributionType,
                                               from_date: datetime = None, till_date: datetime = None)\
            -> Generator[Recommendation, None, None]:
        return self.read_unmatched_recommendations(client, attribution_type, from_date, till_date,
                                                   [('user_id', True), ('date', True)])

    @abstractmethod
    def delete_all_unmatched_recommendations(self, client: str, attribution_type: Optional[AttributionType] = None):
        pass

    @abstractmethod
    def _delete_fatigue_unmatched_recommendations(self, client: str, older_than_checked: datetime) -> Optional[int]:
        pass

    def delete_fatigue_unmatched_recommendations(self, client: str, older_than: datetime) -> Optional[int]:
        if not isinstance(older_than, datetime):
            raise ValueError("Bad date parameter")
        if older_than > datetime.utcnow() - self.MIN_UNMATCHED_RECOMMENDATIONS_FATIGUE:
            raise MobiDaoException(f"Can not delete unmatched recommendations younger than {str(older_than)}.")
        return self._delete_fatigue_unmatched_recommendations(client, older_than)
