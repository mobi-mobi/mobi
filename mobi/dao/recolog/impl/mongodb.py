from datetime import datetime
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from typing import Generator, Iterable, Optional, Tuple

from mobi.core.models import AttributedEvent, AttributedRecommendation, AttributionType, Recommendation
from mobi.dao.recolog.interface import MobiRecologDao


class MongoRecologDao(MobiRecologDao):
    RECOLOG_COLLECTION_NAME = "recologs"
    ATTRIBUTED_EVENTS_COLLECTION_NAME = "attributed_events"
    ATTRIBUTED_RECOMMENDATIONS_COLLECTION_NAME = "attributed_recommendations"
    SAMPLED_UNMATCHED_RECOMMENDATIONS_COLLECTION_NAME = "sampled_unmatched_recommendations"

    def __init__(self, host, port, db, recolog_collection=None, attributed_events_collection=None,
                 attributed_recommendation_collection=None, sampled_unmatched_recommendations_collection=None):
        super().__init__()
        self.client = MongoClient(host, port)
        self.db = self.client[db]
        self.recologs = self.db[recolog_collection or self.RECOLOG_COLLECTION_NAME]
        self.attributed_events = self.db[attributed_events_collection or self.ATTRIBUTED_EVENTS_COLLECTION_NAME]
        self.attributed_recommendations = self.db[attributed_recommendation_collection
                                                  or self.ATTRIBUTED_RECOMMENDATIONS_COLLECTION_NAME]
        self.sampled_unmatched_recommendations = self.db[sampled_unmatched_recommendations_collection
                                                         or self.SAMPLED_UNMATCHED_RECOMMENDATIONS_COLLECTION_NAME]

    def __del__(self):
        try:
            self.client.close()
        except PyMongoError:
            pass

    # Recommendations

    def log_recommendation(self, client: str, recommendation: Recommendation):
        self.recologs.insert_one({"client": client, **recommendation.as_bson_dict()})

    def read_recommendations(self, client: str, from_date: datetime = None, till_date: datetime = None,
                             sort_by: Iterable[Tuple[str, bool]] = None) \
            -> Generator[Recommendation, None, None]:
        filter_clause = {"client": client, "date": {}}
        if from_date is not None:
            filter_clause["date"]["$gte"] = from_date
        if till_date is not None:
            filter_clause["date"]["$lt"] = till_date
        if not filter_clause["date"]:
            del filter_clause["date"]

        sort_clause = None
        if sort_by is not None:
            sort_clause = [(field, int(order) * 2 - 1) for field, order in sort_by]

        for obj in self.recologs.find(filter=filter_clause, sort=sort_clause):
            yield Recommendation.from_dict(obj)

    def delete_all_recommendations(self, client: str):
        if not client:
            raise ValueError("Client is empty")
        self.recologs.delete_many({"client": client})

    def _delete_fatigue_recologs(self, client: str, older_than_checked: datetime) -> Optional[int]:
        deletion_result = self.recologs.delete_many({
            "client": client,
            "date": {
                "$lt": older_than_checked
            }
        })

        try:
            return deletion_result.deleted_count
        except PyMongoError:
            pass

    # Attributed events

    def log_attributed_event(self, client: str, attributed_event: AttributedEvent):
        obj = attributed_event.as_bson_dict()
        self.attributed_events.insert_one({"client": client, **obj})

    def read_attributed_events(self, client, attribution_type: AttributionType, from_date: datetime = None,
                               till_date: datetime = None, sort_by: Iterable[Tuple[str, bool]] = None) \
            -> Generator[AttributedEvent, None, None]:
        filter_clause = {"client": client, "attribution_type": attribution_type.value, "event.date": {}}
        if from_date is not None:
            filter_clause["event.date"]["$gte"] = from_date
        if till_date is not None:
            filter_clause["event.date"]["$lt"] = till_date
        if not filter_clause["event.date"]:
            del filter_clause["event.date"]

        sort_clause = None
        if sort_by is not None:
            sort_clause = [(f"event.{field}", int(order) * 2 - 1) for field, order in sort_by]

        for obj in self.attributed_events.find(filter=filter_clause, sort=sort_clause):
            yield AttributedEvent.from_dict(obj)

    def get_latest_attributed_event(self, client, attribution_type: AttributionType) -> Optional[AttributedEvent]:
        latest_attributed_event = self.attributed_events.find_one(
            {"client": client, "attribution_type": attribution_type.value},
            sort=[("event.date", -1)]
        )

        if latest_attributed_event is None:
            return None

        return AttributedEvent.from_dict(latest_attributed_event)

    def delete_all_attributed_events(self, client: str, attribution_type: Optional[AttributionType] = None):
        if not client:
            raise ValueError("Client is empty")
        filter_ = {"client": client}
        if attribution_type is not None:
            filter_["attribution_type"] = attribution_type.value
        self.attributed_events.delete_many(filter_)

    def _delete_fatigue_attributed_events(self, client: str, older_than_checked: datetime) -> Optional[int]:
        deletion_result = self.attributed_events.delete_many({
            "client": client,
            "event.date": {
                "$lt": older_than_checked
            }
        })

        try:
            return deletion_result.deleted_count
        except PyMongoError:
            pass

    # Attributed recommendations

    def log_attributed_recommendation(self, client: str, attributed_recommendation: AttributedRecommendation):
        self.attributed_recommendations.insert_one({"client": client, **attributed_recommendation.as_bson_dict()})

    def read_attributed_recommendations(self, client, attribution_type: AttributionType, from_date: datetime = None,
                                        till_date: datetime = None, sort_by: Iterable[Tuple[str, bool]] = None) \
            -> Generator[AttributedRecommendation, None, None]:
        filter_clause = {"client": client, "attribution_type": attribution_type.value, "recommendation.date": {}}
        if from_date is not None:
            filter_clause["recommendation.date"]["$gte"] = from_date
        if till_date is not None:
            filter_clause["recommendation.date"]["$lt"] = till_date
        if not filter_clause["recommendation.date"]:
            del filter_clause["recommendation.date"]

        sort_clause = None
        if sort_by is not None:
            sort_clause = [(f"recommendation.{field}", int(order) * 2 - 1) for field, order in sort_by]

        for obj in self.attributed_recommendations.find(filter=filter_clause, sort=sort_clause):
            yield AttributedRecommendation.from_dict(obj)

    def delete_all_attributed_recommendations(self, client: str, attribution_type: Optional[AttributionType] = None):
        if not client:
            raise ValueError("Client is empty")
        filter_ = {"client": client}
        if attribution_type is not None:
            filter_["attribution_type"] = attribution_type.value
        self.attributed_recommendations.delete_many(filter_)

    def _delete_fatigue_attributed_recommendations(self, client: str, older_than_checked: datetime) -> Optional[int]:
        deletion_result = self.attributed_recommendations.delete_many({
            "client": client,
            "recommendation.date": {
                "$lt": older_than_checked
            }
        })

        try:
            return deletion_result.deleted_count
        except PyMongoError:
            pass

    # Unmatched recommendations - TO DELETE?

    def log_unmatched_recommendation(self, client: str, attribution_type: AttributionType,
                                     recommendation: Recommendation):
        self.sampled_unmatched_recommendations.insert_one({"client": client, "attribution_type": attribution_type.value,
                                                           **recommendation.as_bson_dict()})

    def read_unmatched_recommendations(self, client: str, attribution_type: AttributionType, from_date: datetime = None,
                                       till_date: datetime = None, sort_by: Iterable[Tuple[str, bool]] = None) \
            -> Generator[Recommendation, None, None]:

        filter_clause = {"client": client, "attribution_type": attribution_type.value, "date": {}}
        if from_date is not None:
            filter_clause["date"]["$gte"] = from_date
        if till_date is not None:
            filter_clause["date"]["$lt"] = till_date
        if not filter_clause["date"]:
            del filter_clause["date"]

        sort_clause = None
        if sort_by is not None:
            sort_clause = [(field, int(order) * 2 - 1) for field, order in sort_by]

        for obj in self.sampled_unmatched_recommendations.find(filter=filter_clause, sort=sort_clause):
            yield Recommendation.from_dict(obj)

    def delete_all_unmatched_recommendations(self, client: str, attribution_type: Optional[AttributionType] = None):
        if not client:
            raise ValueError("Client is empty")
        filter_ = {"client": client}
        if attribution_type is not None:
            filter_["attribution_type"] = attribution_type.value
        self.sampled_unmatched_recommendations.delete_many(filter_)

    def _delete_fatigue_unmatched_recommendations(self, client: str, older_than_checked: datetime) -> Optional[int]:
        deletion_result = self.sampled_unmatched_recommendations.delete_many({
            "client": client,
            "date": {
                "$lt": older_than_checked
            }
        })

        try:
            return deletion_result.deleted_count
        except PyMongoError:
            pass
