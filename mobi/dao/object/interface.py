from abc import ABC, abstractmethod
from base64 import b64encode
from typing import Iterable

from mobi.core.models import ModelDump, ModelDumpInfo


class MobiObjectDao(ABC):
    @staticmethod
    def _key(client: str, key: str) -> str:
        def _hash(v: str):
            return b64encode(str(v).encode('utf-8')).decode('utf-8')
        return f"{_hash(client)}__{_hash(key)}"

    @abstractmethod
    def _save(self, key: str, data: bytes):
        pass

    def save_binary(self, client: str, key: str, data: bytes):
        return self._save(self._key(client, key), data)

    @abstractmethod
    def _load(self, key: str) -> bytes:
        pass

    def load_binary(self, client: str, key: str) -> bytes:
        return self._load(self._key(client, key))

    @abstractmethod
    def _delete(self, key: str):
        pass

    def delete_binary(self, client: str, key: str):
        self._delete(self._key(client, key))


class MobiModelDao(MobiObjectDao, ABC):
    @staticmethod
    def _model_key(model_id: str) -> str:
        return f"model_{model_id}"

    @abstractmethod
    def read_model_dump_info(self, client: str, model_id: str) -> ModelDumpInfo:
        pass

    def read_model_dump_binary(self, client: str, model_id: str) -> bytes:
        return self.load_binary(client, self._model_key(model_id))

    def read_model_dump(self, client: str, model_id: str) -> ModelDump:
        return ModelDump(
            info=self.read_model_dump_info(client, model_id),
            binary=self.read_model_dump_binary(client, model_id)
        )

    @abstractmethod
    def _save_model_dump_info(self, client: str, model: ModelDumpInfo):
        pass

    def save_model_dump(self, client: str, model: ModelDump):
        self.save_binary(client, self._model_key(model.info.model_id), model.binary)
        self._save_model_dump_info(client, model.info)

    @abstractmethod
    def read_all_model_dumps(self, client: str, limit: int = 10) -> Iterable[ModelDumpInfo]:
        pass

    @abstractmethod
    def _delete_model_dump_info(self, client: str, model_id: str):
        pass

    def delete_model_dump(self, client: str, model_id: str):
        self._delete_model_dump_info(client, model_id)
        self.delete_binary(client, self._model_key(model_id))

    def delete_all_model_dumps(self, client: str, limit: int = 1000):
        # Anyway we can't delete binaries in one operation
        model_dumps = list(self.read_all_model_dumps(client, limit))
        for model in model_dumps:
            self.delete_model_dump(client, model.model_id)
