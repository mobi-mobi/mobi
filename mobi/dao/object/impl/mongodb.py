import gridfs

from pymongo import MongoClient
from pymongo.errors import PyMongoError
from typing import Iterable

from mobi.core.models import ModelDumpInfo
from mobi.dao.common import MobiDaoException
from mobi.dao.object.interface import MobiObjectDao, MobiModelDao


class MongoObjectDao(MobiObjectDao):
    def __init__(self, host, port, db):
        super().__init__()
        self.client = MongoClient(host, port)
        self.db = self.client[db]
        self.gridfs = gridfs.GridFS(self.db)

    def _save(self, key: str, data: bytes):
        try:
            return self.gridfs.put(data, _id=key)
        except gridfs.errors.FileExists:
            raise MobiDaoException(f"Can not create file \"{key}\": file already exists.") from None

    def _load(self, key: str) -> bytes:
        try:
            return self.gridfs.get(key).read()
        except gridfs.NoFile:
            raise MobiDaoException(f"File \"{key}\" does not exist")

    def _delete(self, key: str):
        self.gridfs.delete(key)


class MongoModelDao(MobiModelDao, MongoObjectDao):
    MODELS_COLLECTION_NAME = "models"

    def __init__(self, host, port, db, collection=None):
        MongoObjectDao.__init__(self, host, port, db)
        self.models = self.db[collection or self.MODELS_COLLECTION_NAME]

    def __del__(self):
        try:
            self.client.close()
        except PyMongoError:
            pass

    def read_model_dump_info(self, client: str, model_id: str) -> ModelDumpInfo:
        model = self.models.find_one({"client": client, "model_id": model_id})
        if model is None:
            raise ValueError(f"Can not find model with id \"{model_id}\"")
        return ModelDumpInfo.from_dict(model)

    def _save_model_dump_info(self, client: str, model: ModelDumpInfo):
        try:
            self.models.find_one_and_replace(
                {"client": client, "model_id": model.model_id},
                {"client": client, **model.as_bson_dict()},
                upsert=True
            )
        except Exception as e:
            raise MobiDaoException(f"Exception raised while saving the model: {str(e)}") from None

    def read_all_model_dumps(self, client: str, limit: int = 10) -> Iterable[ModelDumpInfo]:
        result = []
        for model in self.models.find(
                {"client": client},
                sort=[("date", -1)],
                limit=limit
                ):
            result.append(ModelDumpInfo.from_dict(model))

        return result

    def _delete_model_dump_info(self, client: str, model_id: str):
        try:
            self.models.delete_one({"client": client, "model_id": model_id})
        except Exception as e:
            raise MobiDaoException(f"Exception raised while deleting a model: {str(e)}") from None
