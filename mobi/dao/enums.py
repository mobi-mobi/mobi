from enum import Enum


class MobiObjectCreationStatus(Enum):
    NO_EFFECT = 0
    CREATED = 1
    UPDATED = 2
    DELETED = 3
