from datetime import datetime
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from typing import Optional, Generator, List

from mobi.core.models import Product
from mobi.dao.catalog.interface import MobiCatalogDao
from mobi.dao.common import MobiDaoException
from mobi.dao.enums import MobiObjectCreationStatus


class MongoCatalogDao(MobiCatalogDao):
    PRODUCTS_COLLECTION_NAME = "products"

    def __init__(self, host, port, db, collection=None):
        self.client = MongoClient(host, port)
        self.db = self.client[db]
        self.products = self.db[collection or self.PRODUCTS_COLLECTION_NAME]

    def __del__(self):
        try:
            self.client.close()
        except PyMongoError:
            pass

    def create_product(self, client: str, product: Product) -> MobiObjectCreationStatus:
        try:
            old_product = self.read_product(client, product.product_id)
        except MobiDaoException:
            old_product = None

        # TODO: This is ugly and not thread-safe
        new_version = 1 if old_product is None else old_product.version + 1

        if old_product is None or old_product != product:
            # TODO: Delete "id" once you upgrade MongoDB database
            doc = dict(product.as_bson_dict(), **{"id": product.product_id, "client": client, "outdated": False,
                                                  "version": new_version})
            self.products.insert_one(doc)
            if old_product:
                self.products.find_one_and_update({"client": client,
                                                   "id": product.product_id,
                                                   "version": old_product.version},
                                                  {"$set": {"outdated": True}})

                if old_product.active and not product.active:
                    return MobiObjectCreationStatus.DELETED

                return MobiObjectCreationStatus.UPDATED

            return MobiObjectCreationStatus.CREATED

        return MobiObjectCreationStatus.NO_EFFECT

    def read_product(self, client: str, product_id: str, version: Optional[int] = None) -> Product:
        if version:
            json = self.products.find_one({"client": client, "id": product_id, "version": version})
        else:
            json = self.products.find_one({"client": client, "id": product_id}, sort=[("version", -1)])
        if not json:
            raise MobiDaoException(f"Product \"{product_id}\" does not exist")
        return Product.from_dict(json)

    def _read_products(self, client: str, product_ids: List[str]) -> Generator[Product, None, None]:
        product_ids = list(product_ids)
        cursor = self.products.find({"client": client, "id": {"$in": product_ids}, "outdated": False})
        for json in cursor:
            yield Product.from_dict(json)

    def read_all_products(self, client: str, offset: int = 0, limit: Optional[int] = None,
                          include_inactive: bool = False, include_not_in_stock: bool = True) \
            -> Generator[Product, None, None]:
        _filter = [{"client": client}, {"outdated": False}]
        if not include_inactive:
            _filter.append({"active": True})
        if not include_not_in_stock:
            _filter.append({"$or": [
                {"available_from": None},
                {"available_from": {"$lte": datetime.utcnow()}}
            ]})
            _filter.append({"$or": [
                {"available_until": None},
                {"available_until": {"$gte": datetime.utcnow()}}
            ]})
        kwargs = {
            "skip": offset
        }
        if limit is not None:
            kwargs["limit"] = limit
        for json in self.products.find({"$and": _filter}, **kwargs):
            yield Product.from_dict(json)

    def read_all_brands(self, client) -> Generator[str, None, None]:
        for brand in self.products.distinct("brand", {"client": client, "outdated": False}):
            yield brand

    def read_all_categories(self, client) -> Generator[str, None, None]:
        for brand in self.products.distinct("categories", {"client": client, "outdated": False}):
            yield brand

    def get_products_number(self, client: str, include_inactive: bool = False, text: str = None) -> int:
        _filter = {"client": client, "outdated": False}
        if not include_inactive:
            _filter["active"] = True
        if text is not None and text.strip():
            words = {word.lower() for word in text.split() if word}
            if len(words) == 1:
                _filter["title"] = {"$regex": text.strip(), "$options": "i"}
            elif len(words) > 1:
                _filter = {
                    "$and": [
                        _filter,
                        {"$and": [{"title": {"$regex": word, "$options": "i"}} for word in words]}
                    ]
                }
        return self.products.count(_filter)

    def deactivate_product(self, client: str, product_id: str):
        product = self.read_product(client, product_id)
        self.products.find_one_and_update({"client": client, "id": product_id, "version": product.version},
                                          {"$set": {"active": False}})

    def delete_product(self, client: str, product_id: str):
        if not client:
            raise MobiDaoException(f"Wrong client value: \"{client}\"")
        self.products.delete_many({"client": client, "id": product_id})

    def delete_all_products(self, client: str):
        if not client:
            raise MobiDaoException(f"Wrong client value: \"{client}\"")
        self.products.delete_many({"client": client})

    def delete_fatigue_versions(self, client: str, product_id: str, min_version_to_remain: int = None) -> Optional[int]:
        filter = {
            "client": client,
            "id": product_id,
            "outdated": True
        }

        if min_version_to_remain is not None:
            filter["version"] = {
                "$lt": min_version_to_remain
            }

        deletion_result = self.products.delete_many(filter)

        try:
            return deletion_result.deleted_count
        except PyMongoError:
            pass
