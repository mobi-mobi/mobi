from abc import ABC, abstractmethod
from typing import Generator, Iterable, List, Optional

from mobi.core.models import Product
from mobi.dao.common import MobiDaoException
from mobi.dao.enums import MobiObjectCreationStatus


class MobiCatalogDaoException(MobiDaoException):
    pass


class MobiCatalogDao(ABC):
    @abstractmethod
    def create_product(self, client: str, product: Product) -> MobiObjectCreationStatus:
        pass

    @abstractmethod
    def read_product(self, client: str, product_id: str, version: Optional[int] = None) -> Product:
        pass

    @abstractmethod
    def _read_products(self, client: str, product_ids: List[str]) -> Generator[Product, None, None]:
        pass

    def read_products(self, client: str, product_ids: Iterable[str], respect_order: bool = False) \
            -> Generator[Product, None, None]:
        product_ids = list(product_ids)
        products = self._read_products(client, product_ids)
        if not respect_order:
            for product in products:
                yield product
        else:
            order_dict = dict(zip(product_ids, range(len(product_ids))))
            for product in sorted(products, key=lambda p: order_dict[p.product_id]):
                yield product

    @abstractmethod
    def read_all_products(self, client: str, offset: int = 0, limit: Optional[int] = None,
                          include_inactive: bool = False, include_not_in_stock: bool = True) \
            -> Generator[Product, None, None]:
        pass

    @abstractmethod
    def read_all_brands(self, client) -> Generator[str, None, None]:
        pass

    @abstractmethod
    def read_all_categories(self, client) -> Generator[str, None, None]:
        pass

    @abstractmethod
    def get_products_number(self, client: str, include_inactive: bool = False, text: str = None) -> int:
        pass

    @abstractmethod
    def deactivate_product(self, client: str, product_id: str):
        pass

    @abstractmethod
    def delete_product(self, client: str, product_id: str):
        pass

    @abstractmethod
    def delete_all_products(self, client: str):
        pass

    @abstractmethod
    def delete_fatigue_versions(self, client: str, product_id: str, min_version_to_remain: int = None) -> Optional[int]:
        pass
