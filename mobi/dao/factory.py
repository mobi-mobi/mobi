from threading import Lock
from typing import Optional

from mobi.config._base import MobiConfig
from mobi.dao.auth import MobiAuthDao
from mobi.dao.auth.impl.mongodb import MongoDbAuthDao
from mobi.dao.audience import MobiAudienceDao
from mobi.dao.audience.impl.mongodb import MongoAudienceDao
from mobi.dao.cache import MobiCache
from mobi.dao.cache.impl.memcached import MemcachedCache
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.catalog.impl.mongodb import MongoCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.event.impl.mongodb import MongoEventDao
from mobi.dao.message import MobiMessageDao
from mobi.dao.message.impl.kafka import KafkaMessageDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.metrics.impl.mongodb import MongoMetricsDao
from mobi.dao.object import MobiModelDao, MobiObjectDao
from mobi.dao.object.impl.mongodb import MongoModelDao, MongoObjectDao
from mobi.dao.recolog.interface import MobiRecologDao
from mobi.dao.recolog.impl.mongodb import MongoRecologDao
from mobi.dao.search import MobiSearchDao
from mobi.dao.search.impl.elasticserch import ElasticDao, ESNode
from mobi.dao.settings.interface import MobiSettingsDao
from mobi.dao.settings.impl.mongodb import MongoSettingsDao
from mobi.dao.user_settings.interface import MobiUserSettingsDao
from mobi.dao.user_settings.impl.mongodb import MongoUserSettingsDao

__GET_DAO_LOCK = Lock()

__AUTH_DAO: Optional[MobiAuthDao] = None
__AUDIENCE_DAO: Optional[MobiAudienceDao] = None
__CACHE_DAO: Optional[MobiCache] = None
__CATALOG_DAO: Optional[MobiCatalogDao] = None
__EVENT_DAO: Optional[MobiEventDao] = None
__MESSAGE_DAO: Optional[MobiMessageDao] = None
__METRICS_DAO: Optional[MobiMetricsDao] = None
__MODEL_DAO: Optional[MobiModelDao] = None
__OBJECT_DAO: Optional[MobiObjectDao] = None
__RECOLOG_DAO: Optional[MobiRecologDao] = None
__SEARCH_DAO: Optional[MobiSearchDao] = None
__SETTINGS_DAO: Optional[MobiSettingsDao] = None
__USER_SETTINGS_DAO: Optional[MongoUserSettingsDao] = None


def build_auth_dao(config: MobiConfig) -> MobiAuthDao:
    if config["AUTH_DAO"] == "mongodb":
        return MongoDbAuthDao(
            host=config["AUTH_MONGODB_HOST"],
            port=config["AUTH_MONGODB_PORT"],
            db=config["AUTH_MONGODB_DB"],
            users_collection=config.get("AUTH_MONGODB_USERS_COLLECTION"),
            sessions_collection=config.get("AUTH_MONGODB_SESSIONS_COLLECTION")
        )

    raise ValueError(f"Can not create Auth DAO of an unknown type: {config['AUTH_DAO']}")


def get_auth_dao(config: MobiConfig) -> MobiAuthDao:
    with __GET_DAO_LOCK:
        global __AUTH_DAO
        if __AUTH_DAO is None:
            __AUTH_DAO = build_auth_dao(config)
    return __AUTH_DAO


def build_audience_dao(config: MobiConfig) -> MobiAudienceDao:
    if config["AUDIENCE_DAO"] == "mongodb":
        return MongoAudienceDao(
            host=config["AUDIENCE_MONGODB_HOST"],
            port=config["AUDIENCE_MONGODB_PORT"],
            db=config["AUDIENCE_MONGODB_DB"],
            audiences_collection=config.get("AUDIENCE_MONGODB_AUDIENCES_COLLECTION"),
            audience_split_trees_collection=config.get("AUDIENCE_MONGODB_AUDIENCE_SPLIT_TREE_COLLECTION"),
            audience_unions_collection=config.get("AUDIENCE_MONGODB_AUDIENCES_UNIONS_COLLECTION")
        )

    raise ValueError(f"Can not create Audience DAO of an unknown type: {config['AUDIENCE_DAO']}")


def get_audience_dao(config: MobiConfig) -> MobiAudienceDao:
    with __GET_DAO_LOCK:
        global __AUDIENCE_DAO
        if __AUDIENCE_DAO is None:
            __AUDIENCE_DAO = build_audience_dao(config)
    return __AUDIENCE_DAO


def build_cache_dao(config: MobiConfig) -> MobiCache:
    if config["CACHE"] == "memcached":
        return MemcachedCache(
            host=config["MEMCACHED_HOST"],
            port=config["MEMCACHED_PORT"]
        )

    raise ValueError(f"Can not create Cache DAO of an unknown type: {config['CACHE']}")


def get_cache_dao(config: MobiConfig) -> MobiCache:
    with __GET_DAO_LOCK:
        global __CACHE_DAO
        if __CACHE_DAO is None:
            __CACHE_DAO = build_cache_dao(config)
    return __CACHE_DAO


def build_catalog_dao(config: MobiConfig) -> MobiCatalogDao:
    if config["CATALOG_DAO"] == "mongodb":
        return MongoCatalogDao(
            host=config["CATALOG_MONGODB_HOST"],
            port=config["CATALOG_MONGODB_PORT"],
            db=config["CATALOG_MONGODB_DB"],
            collection=config.get("CATALOG_MONGODB_COLLECTION")
        )

    raise ValueError(f"Can not create Catalog DAO of an unknown type: {config['CATALOG_DAO']}")


def get_catalog_dao(config: MobiConfig) -> MobiCatalogDao:
    with __GET_DAO_LOCK:
        global __CATALOG_DAO
        if __CATALOG_DAO is None:
            __CATALOG_DAO = build_catalog_dao(config)
    return __CATALOG_DAO


def build_event_dao(config: MobiConfig) -> MobiEventDao:
    if config["EVENT_DAO"] == "mongodb":
        return MongoEventDao(
            host=config["EVENT_MONGODB_HOST"],
            port=config["EVENT_MONGODB_PORT"],
            db=config["EVENT_MONGODB_DB"],
            collection=config.get("EVENT_MONGODB_COLLECTION")
        )

    raise ValueError(f"Can not create Event DAO of an unknown type: {config['EVENT_DAO']}")


def get_event_dao(config: MobiConfig) -> MobiEventDao:
    with __GET_DAO_LOCK:
        global __EVENT_DAO
        if __EVENT_DAO is None:
            __EVENT_DAO = build_event_dao(config)
    return __EVENT_DAO


def build_metrics_dao(config: MobiConfig) -> MobiMetricsDao:
    if config["METRICS_DAO"] == "mongodb":
        return MongoMetricsDao(
            host=config["METRICS_MONGODB_HOST"],
            port=config["METRICS_MONGODB_PORT"],
            db=config["METRICS_MONGODB_DB"],
            collection=config.get("METRICS_MONGODB_COLLECTION")
        )

    raise ValueError(f"Can not create Metrics DAO of an unknown type: {config['METRICS_DAO']}")


def get_metrics_dao(config: MobiConfig) -> MobiMetricsDao:
    with __GET_DAO_LOCK:
        global __METRICS_DAO
        if __METRICS_DAO is None:
            __METRICS_DAO = build_metrics_dao(config)
    return __METRICS_DAO


def build_model_dao(config: MobiConfig) -> MobiModelDao:
    if config["MODEL_DAO"] == "mongodb":
        return MongoModelDao(
            host=config["MODEL_MONGODB_HOST"],
            port=config["MODEL_MONGODB_PORT"],
            db=config["MODEL_MONGODB_DB"],
            collection=config.get("MODEL_MONGODB_COLLECTION")
        )

    raise ValueError(f"Can not create Model DAO of an unknown type: {config['MODEL_DAO']}")


def get_model_dao(config: MobiConfig) -> MobiModelDao:
    with __GET_DAO_LOCK:
        global __MODEL_DAO
        if __MODEL_DAO is None:
            __MODEL_DAO = build_model_dao(config)
    return __MODEL_DAO


def build_object_dao(config: MobiConfig) -> MobiObjectDao:
    if config["OBJECT_DAO"] == "mongodb":
        return MongoObjectDao(
            host=config["OBJECT_MONGODB_HOST"],
            port=config["OBJECT_MONGODB_PORT"],
            db=config["OBJECT_MONGODB_DB"]
        )

    raise ValueError(f"Can not create Object DAO of an unknown type: {config['OBJECT_DAO']}")


def get_object_dao(config: MobiConfig) -> MobiObjectDao:
    with __GET_DAO_LOCK:
        global __OBJECT_DAO
        if __OBJECT_DAO is None:
            __OBJECT_DAO = build_object_dao(config)
    return __OBJECT_DAO


def build_recolog_dao(config: MobiConfig) -> MobiRecologDao:
    if config["RECOLOG_DAO"] == "mongodb":
        return MongoRecologDao(
            host=config["RECOLOG_MONGODB_HOST"],
            port=config["RECOLOG_MONGODB_PORT"],
            db=config["RECOLOG_MONGODB_DB"]
        )

    raise ValueError(f"Can not create Recolog DAO of an unknown type: {config['RECOLOG_DAO']}")


def get_recolog_dao(config: MobiConfig) -> MobiRecologDao:
    with __GET_DAO_LOCK:
        global __RECOLOG_DAO
        if __RECOLOG_DAO is None:
            __RECOLOG_DAO = build_recolog_dao(config)
    return __RECOLOG_DAO


def build_search_dao(config: MobiConfig) -> MobiSearchDao:
    if config["SEARCH"] == "elasticsearch":
        return ElasticDao(
            es_nodes=[ESNode(config["ELASTICSEARCH_HOST"], config["ELASTICSEARCH_PORT"])]
        )

    raise ValueError(f"Can not create Search DAO of an unknown type: {config['SEARCH']}")


def get_search_dao(config: MobiConfig) -> MobiSearchDao:
    with __GET_DAO_LOCK:
        global __SEARCH_DAO
        if __SEARCH_DAO is None:
            __SEARCH_DAO = build_search_dao(config)
    return __SEARCH_DAO


def build_settings_dao(config: MobiConfig) -> MobiSettingsDao:
    if config["SETTINGS_DAO"] == "mongodb":
        return MongoSettingsDao(
            host=config["SETTINGS_MONGODB_HOST"],
            port=config["SETTINGS_MONGODB_PORT"],
            db=config["SETTINGS_MONGODB_DB"]
        )

    raise ValueError(f"Can not create Settings DAO of an unknown type: {config['SETTINGS_DAO']}")


def get_settings_dao(config: MobiConfig) -> MobiSettingsDao:
    with __GET_DAO_LOCK:
        global __SETTINGS_DAO
        if __SETTINGS_DAO is None:
            __SETTINGS_DAO = build_settings_dao(config)
    return __SETTINGS_DAO


def build_message_dao(config: MobiConfig) -> MobiMessageDao:
    if config["MESSAGE_DAO"] == "kafka":
        return KafkaMessageDao(
            bootstrap_servers=[config["KAFKA_HOST"] + ":" + str(config["KAFKA_PORT"])],
            security_protocol=config["KAFKA_SECURITY_PROTOCOL"],
            sasl_mechanism=config["KAFKA_SASL_MECHANISM"]
        )

    raise ValueError(f"Can not create Message DAO of an unknown type: {config['MESSAGE_DAO']}")


def get_message_dao(config: MobiConfig) -> MobiMessageDao:
    with __GET_DAO_LOCK:
        global __MESSAGE_DAO
        if __MESSAGE_DAO is None:
            __MESSAGE_DAO = build_message_dao(config)
    return __MESSAGE_DAO


def build_user_settings_dao(config: MobiConfig) -> MobiUserSettingsDao:
    if config["USER_SETTINGS_DAO"] == "mongodb":
        return MongoUserSettingsDao(
            host=config["USER_SETTINGS_MONGODB_HOST"],
            port=config["USER_SETTINGS_MONGODB_PORT"],
            db=config["USER_SETTINGS_MONGODB_DB"],
            user_settings_collection=config.get("USER_SETTINGS_MONGODB_COLLECTION")
        )

    raise ValueError(f"Can not create User Settings DAO of an unknown type: {config['USER_SETTINGS_DAO']}")


def get_user_settings_dao(config: MobiConfig) -> MobiUserSettingsDao:
    with __GET_DAO_LOCK:
        global __USER_SETTINGS_DAO
        if __USER_SETTINGS_DAO is None:
            __USER_SETTINGS_DAO = build_user_settings_dao(config)
    return __USER_SETTINGS_DAO
