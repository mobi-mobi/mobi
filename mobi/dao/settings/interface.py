from abc import ABC, abstractmethod
from typing import Generator, List, Optional

from mobi.core.models import ApiToken
from mobi.core.models.zone import Campaign, CampaignStatus, Zone, ZoneStatus


class MobiSettingsDao(ABC):
    # TOKENS
    def create_api_token(self, token: ApiToken):
        pass

    def read_tokens(self, client: Optional[str] = None) -> List[ApiToken]:
        pass

    def delete_token(self, client: str, token_hash: str):
        pass

    def delete_all_tokens(self, client: str):
        pass

    def delete_expired_tokens(self):
        pass

    # ZONES

    @abstractmethod
    def count_all_zones(self, client: str, include_inactive: bool = False, text: str = None) -> int:
        pass

    @abstractmethod
    def read_all_zones(self, client: str, offset: int = 0, limit: Optional[int] = None,
                       include_inactive: bool = False, text: str = None) -> Generator[Zone, None, None]:
        pass

    @abstractmethod
    def read_zone(self, client: str, zone_id: str) -> Zone:
        pass

    @abstractmethod
    def create_zone(self, client: str, zone: Zone):
        pass

    @abstractmethod
    def set_zone_status(self, client: str, zone_id: str, status: ZoneStatus):
        pass

    @abstractmethod
    def add_campaign_to_zone(self, client: str, zone_id: str, campaign_id: str):
        pass

    @abstractmethod
    def remove_campaign_from_zone(self, client: str, zone_id: str, campaign_id: str):
        pass

    @abstractmethod
    def replace_zone_campaign_ids(self, client: str, zone_id: str, campaign_ids: List[str]):
        pass

    @abstractmethod
    def delete_zone(self, client: str, zone_id: str):
        pass

    @abstractmethod
    def delete_all_zones(self, client: str):
        pass

    # CAMPAIGNS

    @abstractmethod
    def read_all_campaigns(self, client: str, offset: int = 0, limit: Optional[int] = None,
                           include_inactive: bool = False, text: str = None) -> Generator[Campaign, None, None]:
        pass

    def read_zone_campaigns(self, client: str, zone_id: str, include_inactive: bool = False,
                            include_past: bool = False) -> Generator[Campaign, None, None]:
        pass

    @abstractmethod
    def read_campaign(self, client: str, campaign_id: str) -> Campaign:
        pass

    @abstractmethod
    def create_campaign(self, client: str, campaign: Campaign):
        pass

    @abstractmethod
    def set_campaign_status(self, client: str, campaign_id: str, status: CampaignStatus):
        pass

    @abstractmethod
    def delete_campaign(self, client: str, campaign_id: str):
        pass

    def delete_all_campaigns(self, client: str):
        pass
