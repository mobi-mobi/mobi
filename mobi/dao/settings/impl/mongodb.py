from datetime import datetime
from pymongo import MongoClient
from typing import Generator, List, Optional

from mobi.core.models.api_token import ApiToken, hash_token, TokenHashAlgo, TokenPermission, TokenType
from mobi.core.models.zone import Campaign, CampaignStatus, Zone, ZoneStatus
from mobi.dao.settings.interface import MobiSettingsDao


class MongoSettingsDao(MobiSettingsDao):
    TOKENS_COLLECTION_NAME = "tokens"
    ZONES_COLLECTION_NAME = "zones"
    CAMPAIGNS_COLLECTION_NAME = "campaigns"

    def __init__(self, host: str, port: int, db: str, zones_collection_name: str = None,
                 campaigns_collection_name: str = None, tokens_collection_name: str = None):
        self.client = MongoClient(host, port)
        self.db = self.client[db]
        self.zones = self.db[zones_collection_name or self.ZONES_COLLECTION_NAME]
        self.campaigns = self.db[campaigns_collection_name or self.CAMPAIGNS_COLLECTION_NAME]
        self.tokens = self.db[tokens_collection_name or self.TOKENS_COLLECTION_NAME]

    def create_api_token(self, token: ApiToken):
        _filter = {
            "client": token.client,
            "token_hash": token.token_hash
        }
        self.tokens.find_one_and_replace(_filter, token.as_bson_dict(), upsert=True)

    def read_tokens(self, client: Optional[str] = None) -> List[ApiToken]:
        # TO DELETE
        def _eod_datetime(year, month, day):
            return datetime(year, month, day, 23, 59, 59)

        _all_permissions = {permission for permission in TokenPermission}

        _LEGACY_API_TOKENS = [
            ApiToken("testclient", hash_token("testtoken", TokenHashAlgo.SHA256), TokenHashAlgo.SHA256, "test",
                     TokenType.TEST, _eod_datetime(2030, 1, 1), _all_permissions, "Legacy token. Can not be deleted"),
            ApiToken("testclient", "f5f8e8584f62c8cb40a91e52dcf7dab00ef8b13ccb7a220d49dd554cbae49a45",
                     TokenHashAlgo.SHA256,
                     "N/A", TokenType.TEST, _eod_datetime(2030, 1, 1), _all_permissions,
                     "Legacy token. Can not be deleted"),
            ApiToken("testclient", hash_token("testtokenprod", TokenHashAlgo.SHA256), TokenHashAlgo.SHA256, "test",
                     TokenType.PRODUCTION, _eod_datetime(2030, 1, 1), _all_permissions,
                     "Legacy token. Can not be deleted"),
            ApiToken("testclient", hash_token("expiredtesttoken", TokenHashAlgo.SHA256), TokenHashAlgo.SHA256, "test",
                     TokenType.TEST, _eod_datetime(2019, 1, 1), _all_permissions,
                     "Legacy token. Can not be deleted"),
            ApiToken("sephora-test", hash_token("928818E4DB6595E12D613811775D6", TokenHashAlgo.SHA256),
                     TokenHashAlgo.SHA256, "9288", TokenType.TEST, _eod_datetime(2021, 3, 31), _all_permissions,
                     "Legacy token. Can not be deleted"),
            ApiToken("sephora", hash_token("B6C7189A57E29AC6B782E36647A73", TokenHashAlgo.SHA256), TokenHashAlgo.SHA256,
                     "B6C7", TokenType.PRODUCTION, _eod_datetime(2021, 3, 31), _all_permissions,
                     "Legacy token. Can not be deleted"),
            ApiToken("sephora-test", "c132bf536edabe65f96cd8b20a9779f8b9e0dd8339f1775574c3784531e346f3",
                     TokenHashAlgo.SHA256,
                     "N/A", TokenType.TEST, _eod_datetime(2021, 3, 31), _all_permissions,
                     "Legacy token. Can not be deleted"),
            ApiToken("sephora", "eaf15197dd565a051739b11c47aac19913acb4149734c35f917456344db69864",
                     TokenHashAlgo.SHA256,
                     "N/A", TokenType.TEST, _eod_datetime(2021, 3, 31), _all_permissions,
                     "Legacy token. Can not be deleted")
        ]
        # END: TO DELETE

        _filter = {}
        if client:
            _filter["client"] = client

        result = [ApiToken.from_dict(obj) for obj in self.tokens.find(_filter)]
        if client is None:
            result.extend(_LEGACY_API_TOKENS)
        else:
            for token in _LEGACY_API_TOKENS:
                if token.client == client:
                    result.append(token)

        return result

    def delete_token(self, client: str, token_hash: str):
        _filter = {
            "client": client,
            "token_hash": token_hash
        }
        self.tokens.delete_one(_filter)

    def delete_all_tokens(self, client: str):
        _filter = {
            "client": client
        }
        self.tokens.delete_many(_filter)

    def delete_expired_tokens(self):
        _filter = {
            "expiration_date": {"$lt": datetime.utcnow()}
        }
        self.tokens.delete_many(_filter)

    def count_all_zones(self, client: str, include_inactive: bool = False,  text: str = None) -> int:
        _filter = {"client": client}
        if not include_inactive:
            _filter["status"] = ZoneStatus.ACTIVE.value
        if text is not None and text.strip():
            words = {word.lower() for word in text.split() if word}
            if len(words) == 1:
                _filter["name"] = {"$regex": text.strip(), "$options": "i"}
            elif len(words) > 1:
                _filter = {
                    "$and": [
                        _filter,
                        {"$and": [{"name": {"$regex": word, "$options": "i"}} for word in words]}
                    ]
                }
        return self.zones.count_documents(_filter)

    def read_all_zones(self, client: str, offset: int = 0, limit: Optional[int] = None,
                       include_inactive: bool = False, text: str = None) -> Generator[Zone, None, None]:
        _filter = {"client": client}
        if not include_inactive:
            _filter["status"] = ZoneStatus.ACTIVE.value
        if text is not None and text.strip():
            words = {word.lower() for word in text.split() if word}
            if len(words) == 1:
                _filter["name"] = {"$regex": text.strip(), "$options": "i"}
            elif len(words) > 1:
                _filter = {
                    "$and": [
                        _filter,
                        {"$and": [{"name": {"$regex": word, "$options": "i"}} for word in words]}
                    ]
                }
        kwargs = {
            "skip": offset
        }
        if limit is not None:
            kwargs["limit"] = limit
        for obj in self.zones.find(_filter, **kwargs):
            yield Zone.from_dict(obj)

    def read_zone(self, client: str, zone_id: str) -> Zone:
        obj = self.zones.find_one({"client": client, "zone_id": zone_id})
        if obj is None:
            raise KeyError(f"Zone \"{zone_id}\" does not exist")
        return Zone.from_dict(obj)

    def create_zone(self, client: str, zone: Zone):
        bson = {
            **(zone.as_bson_dict()),
            "client": client
        }
        self.zones.find_one_and_replace({"client": client, "zone_id": zone.zone_id}, bson, upsert=True)

    def set_zone_status(self, client: str, zone_id: str, status: ZoneStatus):
        result = self.zones.update_one({"client": client, "zone_id": zone_id}, {"$set": {"status": status.value}})
        if not result.matched_count:
            raise KeyError(f"Zone \"{zone_id}\" does not exist")

    def add_campaign_to_zone(self, client: str, zone_id: str, campaign_id: str):
        zone = self.read_zone(client, zone_id)
        _ = self.read_campaign(client, campaign_id)
        if campaign_id not in zone.campaign_ids:
            zone.campaign_ids += [campaign_id]
        self.zones.update_one({"client": client, "zone_id": zone_id},
                              {"$set": {"campaign_ids": zone.campaign_ids}})

    def remove_campaign_from_zone(self, client: str, zone_id: str, campaign_id: str):
        _ = self.read_zone(client, zone_id)
        self.zones.update_one({"client": client, "zone_id": zone_id}, {"$pull": {"campaign_ids": campaign_id}})

    def replace_zone_campaign_ids(self, client: str, zone_id: str, campaign_ids: List[str]):
        _ = self.read_zone(client, zone_id)
        for campaign_id in campaign_ids:
            _ = self.read_campaign(client, campaign_id)
        self.zones.update_one({"client": client, "zone_id": zone_id}, {"$set": {"campaign_ids": campaign_ids}})

    def delete_zone(self, client: str, zone_id: str):
        self.zones.delete_one({"client": client, "zone_id": zone_id})

    def delete_all_zones(self, client: str):
        self.zones.delete_many({"client": client})

    def read_all_campaigns(self, client: str, offset: int = 0, limit: Optional[int] = None,
                           include_inactive: bool = False, text: str = None) -> Generator[Campaign, None, None]:
        _filter = {
            "client": client
        }
        if not include_inactive:
            _filter["status"] = CampaignStatus.ACTIVE.value
        if text is not None and text.strip():
            words = {word.lower() for word in text.split() if word}
            if len(words) == 1:
                _filter["name"] = {"$regex": text.strip(), "$options": "i"}
            elif len(words) > 1:
                _filter = {
                    "$and": [
                        _filter,
                        {"$and": [{"name": {"$regex": word, "$options": "i"}} for word in words]}
                    ]
                }
        kwargs = {
            "skip": offset
        }
        if limit is not None:
            kwargs["limit"] = limit
        for obj in self.campaigns.find(_filter, **kwargs):
            yield Campaign.from_dict(obj)

    def read_zone_campaigns(self, client: str, zone_id: str, include_inactive: bool = False,
                            include_past: bool = False) -> Generator[Campaign, None, None]:
        zone = self.read_zone(client, zone_id)
        sort_order = {campaign_id: campaign_position for campaign_position, campaign_id in enumerate(zone.campaign_ids)}
        _filter = {
            "client": client,
            "campaign_id": {"$in": zone.campaign_ids}
        }
        if not include_inactive:
            _filter["status"] = CampaignStatus.ACTIVE.value
        if not include_past:
            _filter["end_date"] = {"$not": {"$exists": True, "$lt": datetime.utcnow()}}
        campaigns = list(self.campaigns.find(_filter))
        campaigns.sort(key=lambda campaign: sort_order[campaign["campaign_id"]])
        for campaign in campaigns:
            yield Campaign.from_dict(campaign)

    def read_campaign(self, client: str, campaign_id: str) -> Campaign:
        obj = self.campaigns.find_one({"client": client, "campaign_id": campaign_id})
        if obj is None:
            raise KeyError(f"Campaign \"{campaign_id}\" does not exist")
        return Campaign.from_dict(obj)

    def create_campaign(self, client: str, campaign: Campaign):
        bson = {
            **campaign.as_bson_dict(),
            "client": client
        }
        self.campaigns.find_one_and_replace({"client": client, "campaign_id": campaign.campaign_id}, bson, upsert=True)

    def set_campaign_status(self, client: str, campaign_id: str, status: CampaignStatus):
        result = self.campaigns.update_one({"client": client, "campaign_id": campaign_id},
                                           {"$set": {"status": status.value}})
        if not result.matched_count:
            raise KeyError(f"Campaign \"{campaign_id}\" does not exist")

    def delete_campaign(self, client: str, campaign_id: str):
        self.zones.update_many({"client": client}, {"$pull": {"campaign_ids": campaign_id}})
        self.campaigns.delete_one({"client": client, "campaign_id": campaign_id})

    def delete_all_campaigns(self, client: str):
        self.campaigns.delete_many({"client": client})
