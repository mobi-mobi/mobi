import json
import logging
import time
import traceback

from abc import ABC, abstractmethod
from cachetools.func import ttl_cache
from datetime import datetime
from flask import g, Flask, request
from flask_cors import CORS
from math import floor
from random import random
from werkzeug.exceptions import HTTPException
from typing import Dict, List, Optional, Union

from mobi.api.exceptions import MobiApiException, BadRequestBodyException, UserIsInReferenceException, InternalError,\
    NoTokenException, WrongTokenException, TokenIsExpiredException, TokenHasNoPermissionException,\
    NoClientDefinedException
from mobi.config.client import get_client_config
from mobi.config.client._base import ClientBaseConfig
from mobi.config.env import Env, get_env
from mobi.config.system.api._base import ApiBaseConfig
from mobi.core.logging import get_logger
from mobi.core.models import ApiToken, Product, TokenHashAlgo, TokenPermission, TokenType
from mobi.core.models.api_token import hash_token
from mobi.dao import MobiDaoException, get_cache_dao, get_catalog_dao, get_event_dao, get_metrics_dao, get_model_dao,\
    get_recolog_dao, get_search_dao, get_settings_dao
from mobi.dao.cache import cache_key, MobiCache, UserHistoryCookie
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.object import MobiModelDao
from mobi.dao.recolog import MobiRecologDao
from mobi.dao.search import MobiSearchDao
from mobi.dao.settings import MobiSettingsDao


class TokenizedApi(ABC):
    def __init__(self, name: str):
        self.name = name
        get_logger().setLevel(logging.INFO)

        self.app = Flask(name)
        CORS(self.app)

        self.app.add_url_rule("/status", "status", self.status, methods=["GET"])

        self.app.register_error_handler(MobiApiException, self.error_handler)
        self.app.register_error_handler(HTTPException, self.http_exception_handler)

        self.app.before_request(self.check_token)
        self.app.before_request(self.before_request_hook)

        self.app.after_request(self.after_request_hook)

    @classmethod
    def debug_population_size(cls) -> float:
        return 0.1

    @classmethod
    def before_request_hook(cls):
        g.request_start_time = time.time()
        g.is_debug = random() < cls.debug_population_size()
        g.request_url = request.url

    @classmethod
    def after_request_hook(cls, response):
        if "is_debug" in g and g.is_debug:
            url = "NOT_DEFINED" if "request_url" not in g else g.request_url
            processing_time = time.time() - g.request_start_time
            get_logger().info(f"Request processing time: {processing_time:.3f}s URL: {url}")
        return response

    @classmethod
    def get_post_data(cls, max_content_length=1024 * 1024, throw_exception_if_empty: bool = True) -> Optional[bytes]:
        if request.content_length is not None and request.content_length > max_content_length:
            raise BadRequestBodyException("The content size is too large.")

        data = request.data

        if not data and throw_exception_if_empty:
            raise BadRequestBodyException("Request data is empty.")

        return data

    @classmethod
    def get_post_data_obj(cls) -> dict:
        data = cls.get_post_data(throw_exception_if_empty=True)

        try:
            return json.loads(data)
        except json.JSONDecodeError:
            raise BadRequestBodyException("Request body is not a valid JSON object.") from None

    def status(self):
        return self.format_result()

    def format_result(self, data: Optional[Union[dict, list]] = None,
                      meta: Optional[Union[dict, list]] = None,
                      exception: Optional[Exception] = None):
        # TODO: Delete v1 variable after migrating to v1
        v1 = not request.path.startswith("/v2/")
        code = 200
        body = {
            "status": "OK"
        }
        if meta is not None:
            body["meta"] = meta

        if data is not None:
            body["data"] = data

        if exception:
            if isinstance(exception, UserIsInReferenceException):
                body["status"] = "DENIED"
            else:
                body["status"] = "ERROR"

            log_traceback = isinstance(exception, HTTPException) and exception.code != 404
            log_traceback = log_traceback or isinstance(exception, InternalError)
            log_traceback = log_traceback or (not (isinstance(exception, MobiApiException)
                                                   or isinstance(exception, HTTPException)))

            try:
                code = exception.code
                description = exception.description
            except AttributeError:
                code = InternalError.code
                description = InternalError.description

            if not isinstance(exception, UserIsInReferenceException):
                if log_traceback:
                    tb_str = "".join(traceback.format_tb(exception.__traceback__))
                    get_logger().error(f"{code} {request.url} {exception}\n{tb_str}")
                else:
                    get_logger().error(f"{code} {request.url} {exception}")

            body["error"] = {
                "code": code,
                "description": description
            }
            if v1:
                body["error"]["message"] = str(exception)

        try:
            # We fall into this method also in case when client is not defined. In this case we won't
            # be able to set metrics
            self.inc_metrics_point("api_request", {
                "service": self.__class__.__name__.lower(),
                "code": str(code),
                "status": body["status"].lower(),
            }, int(floor(datetime.utcnow().timestamp())))
        except Exception:
            pass

        return json.dumps(body), code, {'Content-Type': 'application/json; charset=utf-8'}

    def empty_result(self):
        return self.format_result()

    def error_handler(self, e):
        return self.format_result(None, None, e)

    def http_exception_handler(self, e: HTTPException):
        return self.format_result(None, None, e)

    @property
    @ttl_cache(ttl=10*60)
    def tokens_dict(self) -> Dict[TokenHashAlgo, Dict[str, ApiToken]]:
        def _eod_datetime(year, month, day):
            return datetime(year, month, day, 23, 59, 59)

        result = {}
        for token in self.settings_dao.read_tokens():
            if token.token_hash_algo not in result:
                result[token.token_hash_algo] = {}
            result[token.token_hash_algo][token.token_hash] = token

        for token_hash_algo in TokenHashAlgo:
            for client in ("sephora", "sephora-test", "testclient"):
                if token_hash_algo not in result:
                    result[token_hash_algo] = {}
                token = ApiToken(client, hash_token(f"8d636fbbc20fce016c4a4b31910a8c596799af69-"
                                                    f"console-token-for-test-recommendations-{client}",
                                                    token_hash_algo),
                                 token_hash_algo, "8d63", TokenType.PRODUCTION, _eod_datetime(2030, 1, 1),
                                 {TokenPermission.TEST_RECOMMENDATIONS})
                result[token_hash_algo][token.token_hash] = token

        return result

    def check_token(self):
        allow_access_to = (
            "/status",
            "/v2/catalog/status",
            "/v2/user/status",
            "/v2/recommendation/status",
            "/v2/search/status",
            "/v2/metrics/status"
        )

        for url_prefix in allow_access_to:
            if request.path.startswith(url_prefix):
                return

        if not self.raw_token:
            raise NoTokenException()

        if self.token is None:
            raise WrongTokenException()

        if self.token.expired():
            raise TokenIsExpiredException()

        g.client = self.token.client

    @property
    def token(self) -> Optional[ApiToken]:
        token = None
        for token_hash_algo in self.tokens_dict:
            if hash_token(self.raw_token, token_hash_algo) in self.tokens_dict[token_hash_algo]:
                token = self.tokens_dict[token_hash_algo][hash_token(self.raw_token, token_hash_algo)]
                break
        return token

    @property
    def raw_token(self) -> Optional[str]:
        auth_token_header = request.headers.get("Authorization")
        if not auth_token_header:
            raise NoTokenException()

        try:
            authorization_type, auth_token = auth_token_header.split(" ")
        except ValueError:
            raise WrongTokenException() from None

        if authorization_type != "Bearer":
            raise WrongTokenException()

        if not auth_token:
            raise WrongTokenException()

        return auth_token

    @property
    def token_type(self) -> TokenType:
        return self.token.token_type

    def assert_token_permission(self, permission: TokenPermission):
        if permission not in self.token.permissions:
            raise TokenHasNoPermissionException(
                "You don't have permissions to call this API method. Please, ask your administrator to give you "
                f"{permission.name} permission")

    @property
    def client(self) -> str:
        if "client" not in g:
            raise NoClientDefinedException()
        return g.client

    @property
    @abstractmethod
    def config(self) -> ApiBaseConfig:
        pass

    @property
    def client_config(self) -> ClientBaseConfig:
        return get_client_config(self.client)

    @property
    def cache_dao(self) -> MobiCache:
        return get_cache_dao(self.config)

    @property
    def catalog_dao(self) -> MobiCatalogDao:
        return get_catalog_dao(self.config)

    @property
    def event_dao(self) -> MobiEventDao:
        return get_event_dao(self.config)

    @property
    def metrics_dao(self) -> MobiMetricsDao:
        return get_metrics_dao(self.config)

    @property
    def models_dao(self) -> MobiModelDao:
        return get_model_dao(self.config)

    @property
    def recolog_dao(self) -> MobiRecologDao:
        return get_recolog_dao(self.config)

    @property
    def search_dao(self) -> MobiSearchDao:
        return get_search_dao(self.config)

    @property
    def settings_dao(self) -> MobiSettingsDao:
        return get_settings_dao(self.config)

    def set_metrics_point(self, name: str, tags: Optional[Dict[str, str]], timestamp: int, value: float):
        if tags is None:
            tags = {}
        for base in (60, 60 * 60, 24 * 60 * 60):
            self.metrics_dao.set_metrics_point(self.client, name, tags, base, timestamp, value)

    def inc_metrics_point(self, name: str, tags: Optional[Dict[str, str]], timestamp: int,
                          inc: float = 1.0):
        if tags is None:
            tags = {}
        for base in (60, 60 * 60, 24 * 60 * 60):
            self.metrics_dao.inc_metrics_point(self.client, name, tags, base, timestamp, inc)

    def delete_product_from_cache(self, product_id: str):
        if get_env() in (Env.PROD, Env.PREPROD):
            noreply = True
        else:
            noreply = False
        self.cache_dao.delete(cache_key("product", client=self.client, product_id=product_id), retries=3,
                              noreply=noreply)

    def read_products(self, product_ids: List[str], skip_cache: bool = False, create_cache: bool = True,
                      cache_expires: int = 60 * 60, respect_order: bool = False) -> List[Product]:
        def product_key(product_id: str):
            return cache_key("product", client=self.client, product_id=product_id)

        if not skip_cache:
            cached_products = self.cache_dao.get_many([product_key(product_id) for product_id in product_ids],
                                                      retries=3)
        else:
            cached_products = None

        not_cached_products = list(self.catalog_dao.read_products(
            self.client,
            [product_id for product_id in product_ids
             if cached_products is None or product_key(product_id) not in cached_products],
            respect_order=False
        ))

        if create_cache:
            self.cache_dao.set_many(
                {product_key(product.product_id): product.as_bytes() for product in not_cached_products},
                expire=cache_expires,
                noreply=True
            )

        result = not_cached_products

        if cached_products:
            result.extend([Product.from_bytes(b) for b in cached_products.values()])

        if respect_order:
            places = {product_id: place for place, product_id in enumerate(product_ids)}
            result.sort(key=lambda p: places[p.product_id])

        return result

    def read_product(self, product_id: str, skip_cache: bool = False, create_cache: bool = True,
                     cache_ttl: int = 60 * 60) -> Optional[Product]:
        def product_key(product_id: str):
            return cache_key("product", client=self.client, product_id=product_id)

        if not skip_cache:
            product = self.cache_dao.get(product_key(product_id))
            if product is not None:
                return Product.from_bytes(product)

        try:
            product = self.catalog_dao.read_product(self.client, product_id)
        except MobiDaoException:
            return None

        if create_cache:
            self.cache_dao.set(product_key(product_id), product.as_bytes(), noreply=True, expire=cache_ttl)

        return product

    def _get_user_history(self, user_id: str, skip_local_cache: bool = False, skip_cache: bool = False,
                          create_cache: bool = True, db_from_date: datetime = None, db_limit: int = None) \
            -> UserHistoryCookie:
        if not skip_local_cache and "user_histories" in g and isinstance(g.user_histories, dict)\
                and user_id in g.user_histories:
            return g.user_histories[user_id]

        if not skip_cache:
            history = self.cache_dao.get_user_history(self.client, user_id)
            if history is not None:
                if create_cache:
                    if "user_histories" not in g or not isinstance(g.user_histories, dict):
                        g.user_histories = dict()
                    g.user_histories[user_id] = history
                return history

        db_from_date = db_from_date or datetime.utcnow() - UserHistoryCookie.EVENT_MAX_AGE
        db_limit = db_limit or UserHistoryCookie.EVENTS_LIMIT

        events = self.event_dao.read_user_history(self.client, user_id, limit=db_limit, from_date=db_from_date)

        history = UserHistoryCookie(events)

        if create_cache:
            # This is a dirty hack, so unit tests that check browsing history will pass
            if get_env() in (Env.TEST, Env.GITLAB, Env.UNITTEST):
                noreply = False
            else:
                noreply = True
            self.cache_dao.set_user_history(self.client, user_id, history, noreply=noreply)

            if "user_histories" not in g or not isinstance(g.user_histories, dict):
                g.user_histories = dict()
            g.user_histories[user_id] = history

        return history

    def _save_user_history(self, user_id: str, history: UserHistoryCookie):
        self.cache_dao.set_user_history(self.client, user_id, history)
