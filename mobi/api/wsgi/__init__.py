from mobi.api.catalog_api import CatalogApi
from mobi.api.event_api import EventApi
from mobi.api.recommendation_api import RecommendationApi
from mobi.api.metrics_api import MetricsApi


def catalog_api_app():
    api = CatalogApi()
    return api.app


def event_api_app():
    api = EventApi()
    return api.app


def recommendation_api_app():
    api = RecommendationApi()
    return api.app


def metrics_api_app():
    api = MetricsApi()
    return api.app
