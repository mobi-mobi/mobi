import argparse

from datetime import datetime
from flask import request
from math import floor

from mobi.api.exceptions import BadRequestBodyException, InternalError, ObjectNotFoundException,\
    TokenHasNoPermissionException
from mobi.api.parsers import uri
from mobi.api.parsers.model import parse_product, parse_product_partial
from mobi.api.tokenized_api import TokenizedApi
from mobi.config.system.api._base import ApiBaseConfig
from mobi.config.system.api.catalog_api import get_catalog_api_config
from mobi.core.logging import get_logger
from mobi.core.models import TokenPermission, TokenType
from mobi.dao import MobiDaoException


__all__ = [
    "CatalogApi"
]


class CatalogApi(TokenizedApi):
    def __init__(self):
        super().__init__("CatalogApi")

        self.app.add_url_rule("/v2/catalog/products", "create_replace_product", self.create_replace_product,
                              methods=["POST"])
        self.app.add_url_rule("/v2/catalog/products/<product_id>", "get_put_patch_delete_product",
                              self.get_put_patch_delete_product, methods=["GET", "PATCH", "DELETE"])

        self.app.add_url_rule("/v2/catalog/all_products", "delete_all_products", self.delete_all_products,
                              methods=["DELETE"])

        self.app.add_url_rule("/v2/catalog/status", "service_status", self.status, methods=["GET"])

    @property
    def config(self) -> ApiBaseConfig:
        return get_catalog_api_config()

    # Helpers

    def _create_catalog_updates_metrics(self, **tags):
        ts = int(floor(datetime.utcnow().timestamp()))
        self.inc_metrics_point("catalog_updates", tags, ts)

    # High-level API Methods

    def create_replace_product(self):
        self.assert_token_permission(TokenPermission.UPDATE_CATALOG)

        try:
            product = parse_product(self.get_post_data(throw_exception_if_empty=False))
        except Exception as e:
            raise BadRequestBodyException(f"Product schema validation failed: {str(e)}")

        try:
            for product_enricher in self.client_config.get("PRODUCT_ENRICHERS", []):
                product_enricher.enrich_product(product)
        except Exception as e:
            raise InternalError(f"An error occurred while enriching the product: {str(e)}") from None

        try:
            creation_status = self.catalog_dao.create_product(self.client, product)
        except Exception as e:
            raise InternalError(f"An error occurred while saving a product in a database: {str(e)}") from None

        try:
            self._create_catalog_updates_metrics(action="create", brand=product.brand,
                                                 creation_status=creation_status.name.lower())
        except Exception as e:
            get_logger().warning(f"Can not store \"create products\" metric: {e}")

        try:
            self.delete_product_from_cache(product.product_id)
        except Exception as e:
            get_logger().warning(f"Can not delete product from cache: {e}")

        return self.format_result()

    def get_put_patch_delete_product(self, product_id):
        full_info = uri.read_bool("full_info", default=False)

        if request.method == "GET":
            self.assert_token_permission(TokenPermission.READ_CATALOG)

            product = self.read_product(product_id)
            if product is None or not product.active:
                raise ObjectNotFoundException(f"Product \"{product_id}\" does not exist")
            return self.format_result(product.as_json_dict(exclude_ignored_fields=not full_info))

        elif request.method == "PATCH":
            self.assert_token_permission(TokenPermission.UPDATE_CATALOG)

            product = self.read_product(product_id)
            if product is None:
                raise ObjectNotFoundException(f"Product \"{product_id}\" does not exist")

            try:
                product = parse_product_partial(product, self.get_post_data())
            except Exception as e:
                raise BadRequestBodyException(f"Product schema validation failed: {str(e)}") from None

            try:
                for product_enricher in self.client_config.get("PRODUCT_ENRICHERS", []):
                    product_enricher.enrich_product(product)
            except Exception as e:
                raise InternalError(f"An error occurred while enriching the product: {str(e)}") from None

            try:
                creation_status = self.catalog_dao.create_product(self.client, product)
            except Exception as e:
                raise InternalError(f"An error occurred while saving a product in a database: {str(e)}") from None

            try:
                self._create_catalog_updates_metrics(action="patch", brand=product.brand,
                                                     creation_status=creation_status.name.lower())
            except Exception as e:
                get_logger().warning(f"Can not create \"patch product\" metric: {e}")

            try:
                self.delete_product_from_cache(product_id)
            except Exception as e:
                get_logger().warning(f"Can not delete product from cache: {e}")

            return self.format_result()

        elif request.method == "DELETE":
            self.assert_token_permission(TokenPermission.UPDATE_CATALOG)

            try:
                product = self.catalog_dao.read_product(self.client, product_id)
            except MobiDaoException:
                product = None

            if product is not None and product.active:
                try:
                    self.catalog_dao.deactivate_product(self.client, product_id)
                except MobiDaoException:
                    pass

                deletion_status = "deleted"
            else:
                deletion_status = "no_effect"

            try:
                self._create_catalog_updates_metrics(action="delete", brand=product.brand,
                                                     deletion_status=deletion_status)
            except Exception as e:
                get_logger().warning(f"Can not create \"delete product\" metric: {e}")

            try:
                self.delete_product_from_cache(product_id)
            except Exception as e:
                get_logger().warning(f"Can not delete product from cache: {e}")

            return self.format_result()

    def delete_all_products(self):
        if self.token_type != TokenType.TEST:
            raise TokenHasNoPermissionException("Can not perform delete operation for a non-test token")
        self.assert_token_permission(TokenPermission.UPDATE_CATALOG)

        self.catalog_dao.delete_all_products(self.client)
        return self.format_result()


def get_args() -> argparse.Namespace:
    _default_host = "0.0.0.0"
    _default_port = 9393

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"host listen address. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"defines a port to listen. Default: {_default_port}", type=int)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    catalog_api = CatalogApi()
    catalog_api.app.run(host=args.host, port=args.port)
