from werkzeug.exceptions import HTTPException

from mobi.exceptions import MobiException


class MobiApiException(HTTPException, MobiException):
    pass


class BadRequestException(MobiApiException):
    code = 400
    description = "Bad request."


class BadRequestBodyException(MobiApiException):
    code = 400
    description = "Can not parse request body."


class NoClientDefinedException(MobiApiException):
    code = 400
    description = "Can not process a request - client is not defined."


class NoTokenException(MobiApiException):
    code = 401
    description = "No token provided."


class UserIsInReferenceException(MobiApiException):
    code = 403
    description = "User is in reference population."


class WrongTokenException(MobiApiException):
    code = 403
    description = "Token is wrong."


class TokenIsExpiredException(MobiApiException):
    code = 403
    description = "Token is expired."


class NoLoadedModelException(MobiApiException):
    code = 403
    description = "Client has no loaded models. Please, wait until we train one or contact us to get more details."


class NoActiveModelsException(MobiApiException):
    code = 403
    description = "Client has no active models. Please, wait until we train one or contact us to get more details."


class TokenHasNoPermissionException(MobiApiException):
    code = 403
    description = "Token has no permission to call this API method."


class ObjectNotFoundException(MobiApiException):
    code = 404
    description = "Can not find an object."


# class OutOfRequestsLimitException(MobiApiException):
#     code = 429


class InternalError(MobiApiException):
    code = 500
    description = "Unexpected internal error happened. We have already started digging into the problem."
