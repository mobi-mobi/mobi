import argparse
import re

from dataclasses import dataclass
from datetime import datetime, timedelta
from flask import request
from math import floor
from typing import Dict, List, Optional

from mobi.api.exceptions import InternalError, BadRequestException
from mobi.api.tokenized_api import TokenizedApi
from mobi.config.system.api._base import ApiBaseConfig
from mobi.config.system.api.metrics_api import get_metrics_api_config
from mobi.core.models import TokenPermission

__all__ = [
    "MetricsApi"
]


@dataclass
class _MetricRequest:
    request: str
    name: str
    alias: Optional[str]
    tags: Optional[Dict[str, str]]


@dataclass
class _FullMetricsRequest:
    metrics: List[_MetricRequest]
    from_date: datetime
    base_int: int
    human_readable: bool
    points_num: int


class MetricsApi(TokenizedApi):
    BASES = {
        "M": 60,
        "H": 60 * 60,
        "D": 24 * 60 * 60
    }

    DEFAULT_BASE = "M"

    DEFAULT_POINTS = {
        "M": 60,
        "H": 24,
        "D": 10
    }

    MAX_POINTS = {
        "M": 1000,
        "H": 1000,
        "D": 1000
    }

    DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

    NAME_VALIDATOR = re.compile("^[a-zA-Z0-9-._]+$")

    def __init__(self):
        super().__init__("MetricsApi")

        self.app.add_url_rule("/<metrics_str>", "event_metrics", self.event_metrics, methods=["GET"])
        self.app.add_url_rule("/<metrics_str>/tags", "metrics_tags", self.metrics_tags, methods=["GET"])
        self.app.add_url_rule("/<metrics_str>/tag/<tag_str>/values", "metrics_tag_values",
                              self.metrics_tag_values, methods=["GET"])

        self.app.add_url_rule("/v2/metrics/status", "service_status", self.status, methods=["GET"])

    @property
    def config(self) -> ApiBaseConfig:
        return get_metrics_api_config()

    @classmethod
    def _parse_metric_names(cls, metric_names: str) -> List[_MetricRequest]:
        def parse_metric_name(metric_request: str) -> _MetricRequest:
            if not metric_request:
                raise ValueError("Metric name is not specified")
            if metric_request.find(":") != -1:
                try:
                    metric, alias = metric_request.split(":")
                except ValueError:
                    raise ValueError(f"Can not extract metric alias from \"{metric_request}\"")
                if not cls.NAME_VALIDATOR.match(alias):
                    raise ValueError(f"Wring alias name format: \"{alias}\". \"^[a-zA-Z0-9-_.]+&\" string "
                                     f"format was expected.")
            else:
                metric = metric_request
                alias = None

            tags = None
            if metric.find(".") != -1:
                tags = {}
                _split = metric.split(".")
                metric, tags_str = _split[0], _split[1:]
                if len(tags_str) % 2:
                    raise ValueError(f"Wrong length of items in metric name. "
                                     f"Can's split into pairs: \"{metric_request}\"")

                while tags_str:
                    tag, value, tags_str = tags_str[0], tags_str[1], tags_str[2:]
                    if not cls.NAME_VALIDATOR.match(tag):
                        raise ValueError(f"Wrong tag name format: \"{tag}\". \"^[a-zA-Z0-9-_.]+&\" string "
                                         f"format was expected.")
                    if value != "*":
                        if not cls.NAME_VALIDATOR.match(value):
                            raise ValueError(f"Wrong tag value format: \"{value}\". \"^[a-zA-Z0-9-_.]+&\" string "
                                             f"format was expected (or \"*\").")
                    if tag in tags:
                        raise ValueError(f"Tag \"tag\" is duplicated in \"{metric}\"")
                    tags[tag] = value

            if not cls.NAME_VALIDATOR.match(metric):
                raise ValueError(f"Wrong metric name format: \"{metric}\". "
                                 f"\"^[a-zA-Z0-9-_.]+&\" string format was expected.")

            return _MetricRequest(metric_request, metric, alias, tags)

        metric_requests = [parse_metric_name(metric_name) for metric_name in metric_names.split(";")]

        aliases = [r.alias for r in metric_requests if r.alias is not None]
        if len(set(aliases)) != len(aliases):
            raise ValueError(f"Alias duplicates detected: \"{aliases}\"")

        return metric_requests

    @classmethod
    def _parse_from_date(cls, from_date_str: Optional[str]) -> datetime:
        if not from_date_str:
            return datetime.utcnow()

        if from_date_str[-1] in cls.BASES:
            try:
                interval_size = int(from_date_str[:-1])
            except ValueError:
                raise ValueError(f"Wrong interval value: \"{from_date_str}\". Should have the following format: "
                                 f"<int>[M|H|D]") from None
            return datetime.utcnow() + timedelta(seconds=interval_size * cls.BASES[from_date_str[-1]])

        try:
            return datetime.fromtimestamp(int(from_date_str))
        except ValueError:
            try:
                return datetime.strptime(from_date_str, cls.DATE_FORMAT)
            except ValueError:
                raise ValueError(f"Can not parse \"from\" value: \"{from_date_str}\". Make sure "
                                 f"the correct format is used: {cls.DATE_FORMAT}. It is also possible to "
                                 f"use timestamp or offset (<int>[M|H|D])") from None

    @classmethod
    def _parse_base(cls, base_str: str) -> int:
        try:
            base_int = cls.BASES[base_str]
        except KeyError:
            raise ValueError(f"Wrong base value: \"{base_str}\". Should be one of {list(cls.BASES.keys())}") from None
        return base_int

    @classmethod
    def _parse_points(cls, points_str: Optional[str], base_str: str) -> int:
        if not points_str:
            try:
                return cls.DEFAULT_POINTS[base_str]
            except KeyError:
                raise ValueError(f"Can not find default points num for base \"{base_str}\"") from None
        try:
            points = int(points_str)
            if points < 1:
                raise ValueError()
        except ValueError:
            raise ValueError(f"Wrong points number: \"{points_str}\". Should be a valid positive number") from None
        try:
            if points > cls.MAX_POINTS[base_str]:
                raise ValueError(f"Too many points requested: \"{points}\". Max allowed for base {base_str}: "
                                 f"{cls.MAX_POINTS[base_str]}")
        except KeyError:
            raise ValueError(f"Can not find max points number for base \"{base_str}\"") from None
        return points

    @staticmethod
    def _parse_human_readable(human_readable_str: Optional[str]) -> bool:
        return human_readable_str == "on"

    def _get_event_metrics_args(self, metric_names) -> _FullMetricsRequest:
        metric_requests = self._parse_metric_names(metric_names)
        from_date = self._parse_from_date(request.args.get("from"))
        base_str = request.args.get("base", self.DEFAULT_BASE)
        base_int = self._parse_base(base_str)
        human_readable = self._parse_human_readable(request.args.get("hr"))
        points = self._parse_points(request.args.get("points"), base_str)

        return _FullMetricsRequest(
            metric_requests,
            from_date,
            base_int,
            human_readable,
            points
        )

    def fetch_metric(self, metric_request: _MetricRequest, from_ts: int, until_ts: int,
                     base_int: int, human_readable: bool):
        values = []
        for ts, value in self.metrics_dao.get_metrics(self.client, metric_request.name, metric_request.tags,
                                                      base_int, from_ts, until_ts):
            values.append((ts, value))

        full_metrics = []
        for ts in range(from_ts, until_ts, base_int):
            val = 0.0
            try:
                if values and values[0][0] == ts:
                    val = values[0][1]
                    values.pop(0)
            except IndexError:
                raise InternalError(f"Bad data received from database: \"{values[0]}\"")
            if human_readable:
                ts = datetime.fromtimestamp(ts).strftime(self.DATE_FORMAT)
            full_metrics.append((ts, val))

        return full_metrics

    def event_metrics(self, metrics_str: str):
        self.assert_token_permission(TokenPermission.READ_METRICS)

        full_request = self._get_event_metrics_args(metrics_str)

        if not full_request.metrics:
            raise BadRequestException("At least one metric name should be provided")

        db_from_timestamp = full_request.base_int \
            * (int(floor(full_request.from_date.timestamp())) // full_request.base_int)
        db_until_timestamp = db_from_timestamp + full_request.points_num * full_request.base_int

        result_values = {}
        for metric_request in full_request.metrics:
            result_name = metric_request.alias or metric_request.request
            values = self.fetch_metric(metric_request, db_from_timestamp, db_until_timestamp,
                                       full_request.base_int, full_request.human_readable)
            result_values[result_name] = values

        data = {
            "metrics": [metric.request for metric in full_request.metrics],
            "now_ts": int(floor(datetime.utcnow().timestamp())),
            "now": datetime.utcnow().strftime(self.DATE_FORMAT),
            "from_ts": int(floor(full_request.from_date.timestamp())),
            "from": full_request.from_date.strftime(self.DATE_FORMAT),
            "points": full_request.points_num,
            "base": full_request.base_int,
            "values": result_values
        }
        return self.format_result(data)

    def metrics_tags(self, metrics_str: str):
        self.assert_token_permission(TokenPermission.READ_METRICS)

        full_request = self._get_event_metrics_args(metrics_str)

        if not full_request.metrics:
            raise BadRequestException("At least one metric name should be provided")

        db_from_timestamp = full_request.base_int \
            * (int(floor(full_request.from_date.timestamp())) // full_request.base_int)
        db_until_timestamp = db_from_timestamp + full_request.points_num * full_request.base_int

        result_values = {}
        for metric_request in full_request.metrics:
            result_name = metric_request.alias or metric_request.request
            values = self.metrics_dao.get_metrics_tags(self.client, metric_request.name, metric_request.tags,
                                                       full_request.base_int, db_from_timestamp,
                                                       db_until_timestamp)
            result_values[result_name] = values

        return self.format_result({
            "metrics": [metric.request for metric in full_request.metrics],
            "now_ts": int(floor(datetime.utcnow().timestamp())),
            "now": datetime.utcnow().strftime(self.DATE_FORMAT),
            "from_ts": int(floor(full_request.from_date.timestamp())),
            "from": full_request.from_date.strftime(self.DATE_FORMAT),
            "base": full_request.base_int,
            "values": result_values
        })

    def metrics_tag_values(self, metrics_str: str, tag_str: str):
        self.assert_token_permission(TokenPermission.READ_METRICS)

        full_request = self._get_event_metrics_args(metrics_str)

        if not full_request.metrics:
            raise BadRequestException("At least one metric name should be provided")

        db_from_timestamp = full_request.base_int \
            * (int(floor(full_request.from_date.timestamp())) // full_request.base_int)
        db_until_timestamp = db_from_timestamp + full_request.points_num * full_request.base_int

        result_values = {}
        for metric_request in full_request.metrics:
            result_name = metric_request.alias or metric_request.request
            values = self.metrics_dao.get_metrics_tag_values(
                self.client, metric_request.name, tag_str, metric_request.tags,
                full_request.base_int, db_from_timestamp, db_until_timestamp
            )
            result_values[result_name] = values

        return self.format_result({
            "metrics": [metric.request for metric in full_request.metrics],
            "now_ts": int(floor(datetime.utcnow().timestamp())),
            "now": datetime.utcnow().strftime(self.DATE_FORMAT),
            "from_ts": int(floor(full_request.from_date.timestamp())),
            "from": full_request.from_date.strftime(self.DATE_FORMAT),
            "base": full_request.base_int,
            "values": result_values
        })

    # Following methods adds legacy v1 support and have to be deleted after we finish migration

    @property
    def raw_token(self) -> Optional[str]:
        return request.args.get("token") or request.headers.get("Auth-Token")


def get_args() -> argparse.Namespace:
    _default_host = "0.0.0.0"
    _default_port = 9292

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"host listen address. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"defines a port to listen. Default: {_default_port}", type=int)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    event_api = MetricsApi()
    event_api.app.run(host=args.host, port=args.port)
