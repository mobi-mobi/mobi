import argparse

from datetime import datetime
from flask import request
from math import floor
from typing import Optional

from mobi.api.exceptions import InternalError, BadRequestBodyException, ObjectNotFoundException, \
    TokenHasNoPermissionException
from mobi.api.parsers.model import parse_event
from mobi.api.tokenized_api import TokenizedApi
from mobi.config.system.api._base import ApiBaseConfig
from mobi.config.system.api.event_api import get_event_api_config
from mobi.core.common.pricetype import PriceType
from mobi.core.logging import get_logger
from mobi.core.models import Event, EventPlatform, Product, TokenPermission, TokenType
from mobi.dao import MobiDaoException
from mobi.dao.enums import MobiObjectCreationStatus


__all__ = [
    "EventApi"
]


class EventApi(TokenizedApi):
    CREATE_PRODUCT_PARAMS = {"id", "title", "in_stock", "active", "brand", "categories", "color",
                             "is_new", "is_exclusive", "description"}

    CREATE_EVENT_PARAMS = {"user_id", "product_id", "event_type", "referer", "datetime"}

    def __init__(self):
        super().__init__("EventApi")

        # V1 Api Methods - delete them after you finish migration

        self.app.add_url_rule("/product", "create_product", self.create_product, methods=["POST"])
        self.app.add_url_rule("/batch/product", "batch_create_product", self.batch_create_product,
                              methods=["POST"])
        self.app.add_url_rule("/product/<product_id>", "product_info", self.product_info,
                              methods=["GET"])
        self.app.add_url_rule("/product/<product_id>/deactivate", "deactivate_product",
                              self.deactivate_product, methods=["PUT"])
        self.app.add_url_rule("/event", "create_event_v1", self.create_event_v1, methods=["POST"])

        self.app.add_url_rule("/events", "delete_events", self.delete_events, methods=["DELETE"])
        self.app.add_url_rule("/products", "delete_products", self.delete_products,
                              methods=["DELETE"])
        self.app.add_url_rule("/models", "delete_models", self.delete_models, methods=["DELETE"])
        self.app.add_url_rule("/all", "delete_all", self.delete_all, methods=["DELETE"])

        # V2 API Methods

        self.app.add_url_rule("/v2/user/events", "create_event", self.create_event, methods=["POST"])
        self.app.add_url_rule("/v2/user/all_events", "delete_all_events", self.delete_all_events, methods=["DELETE"])

        self.app.add_url_rule("/v2/user/status", "service_status", self.status, methods=["GET"])

    @property
    def config(self) -> ApiBaseConfig:
        return get_event_api_config()

    @classmethod
    def _assert_create_product_data(cls, data):
        # We use InternalError in order to keep capability with v1 version
        if not data.get("id"):
            raise InternalError("Product id missed")
        if not isinstance(data.get("id"), str):
            raise InternalError("Product id is not a string")
        if not data.get("title"):
            raise InternalError("Product title missed")
        if not isinstance(data.get("title"), str):
            raise InternalError("Product title is not a string")
        if not data.get("brand"):
            raise InternalError("Product brand missed")
        if not data.get("color"):
            raise InternalError("Product color missed")
        if not data.get("categories"):
            raise InternalError("Categories set is empty or missing")
        if not isinstance(data.get("categories"), list):
            raise InternalError("Wrong product categories format")
        if any(not isinstance(e, str) for e in data.get("categories")):
            raise InternalError("Wrong product categories format")
        if len(data.get("categories")) != len(set(data.get("categories"))):
            raise InternalError("Product has duplicate categories")
        for key in data.keys():
            if key not in cls.CREATE_PRODUCT_PARAMS:
                raise InternalError(f"Unknown parameter {key}")

    def __create_product_metrics(self, **extra_tags):
        ts = int(floor(datetime.utcnow().timestamp()))
        tags = {
            "action": "create",
            **extra_tags
        }
        self.inc_metrics_point("products", tags, ts)

    def create_product(self):
        self.assert_token_permission(TokenPermission.UPDATE_CATALOG)

        data = self.get_post_data_obj()
        self._assert_create_product_data(data)
        try:
            product = Product(
                product_id=data["id"],
                title=data.get("title"),
                price=PriceType.parse("0.0"),
                currency="USD",
                active=data.get("active", True),
                brand=data.get("brand"),
                categories=data.get("categories"),
                color=data.get("color"),
                description=data.get("description")
            )
            for product_enricher in self.client_config.get("PRODUCT_ENRICHERS", []):
                product_enricher.enrich_product(product)

            status = self.catalog_dao.create_product(self.client, product)
            if status != MobiObjectCreationStatus.NO_EFFECT:
                self.search_dao.index_product(self.client, product, refresh_index=False)
            if not product.active or not product.in_stock:
                # We want to remove product from cache when it's not active or not in stock as
                # it should be immediately removed from recommendations
                self.delete_product_from_cache(product.product_id)
            self.__create_product_metrics(brand=product.brand, status=status.name.lower())
            return self.format_result()
        except Exception as e:
            raise InternalError(f"An error occurred while adding a new product: {str(e)}") from None

    def batch_create_product(self):
        self.assert_token_permission(TokenPermission.UPDATE_CATALOG)

        data_all = self.get_post_data_obj()
        products = []
        for data in data_all:
            self._assert_create_product_data(data)
            product = Product(
                product_id=data["id"],
                title=data.get("title"),
                price=PriceType.parse("0.0"),
                currency="USD",
                active=data.get("active", True),
                brand=data.get("brand"),
                categories=data.get("categories"),
                color=data.get("color"),
                description=data.get("description")
            )
            for product_enricher in self.client_config.get("PRODUCT_ENRICHERS", []):
                product_enricher.enrich_product(product)

            products.append(product)
        try:
            for product in products:
                status = self.catalog_dao.create_product(self.client, product)
                if status != MobiObjectCreationStatus.NO_EFFECT:
                    self.search_dao.index_product(self.client, product, refresh_index=False)
                if not product.active or not product.in_stock:
                    # We want to remove product from cache when it's not active or not in stock as
                    # it should be immediately removed from recommendations
                    self.delete_product_from_cache(product.product_id)
                self.__create_product_metrics(brand=product.brand, status=status.name.lower())
            return self.format_result()
        except Exception as e:
            raise InternalError(f"Error occurred while creating products: {str(e)}") from None

    @classmethod
    def v1_product_json_dict(cls, product: Product) -> dict:
        product_json_dict = product.as_json_dict()
        result = {
            key: product_json_dict[key] for key in [
                "title", "brand", "active", "categories", "color", "version", "description"
            ]
        }
        result["id"] = product.product_id
        result["in_stock"] = product.in_stock
        result["is_new"] = product.tags is not None and "new" in product.tags
        result["is_exclusive"] = product.tags is not None and "exclusive" in product.tags
        return result

    def product_info(self, product_id: str):
        self.assert_token_permission(TokenPermission.READ_CATALOG)

        product = self.read_product(product_id)
        if product is None:
            raise InternalError(f"Can not find product \"{product_id}\"")
        return self.format_result(self.v1_product_json_dict(product))

    def deactivate_product(self, product_id: str):
        self.assert_token_permission(TokenPermission.UPDATE_CATALOG)

        product = self.read_product(product_id, skip_cache=True, create_cache=False)
        if product is None:
            raise InternalError(f"Product {product_id} does not exist")
        try:
            self.catalog_dao.deactivate_product(self.client, product_id)
            self.delete_product_from_cache(product_id)
            self.search_dao.index_product(self.client, product, refresh_index=False)
            return self.format_result()
        except Exception as e:
            raise InternalError(f"Error occurred while deactivating a product: {str(e)}") from None

    @classmethod
    def _assert_create_event_data(cls, data):
        if not data.get("user_id"):
            raise InternalError("User id missed")
        if not data.get("product_id"):
            raise InternalError("Product id missed")
        for key in data.keys():
            if key not in cls.CREATE_EVENT_PARAMS:
                raise InternalError(f"Unknown parameter {key}")

    def __create_event_metrics_v1(self, event: Event, **extra_tags):
        ts = int(floor(event.date.timestamp()))

        tags = {
            "action": "create",
            "type": event.event_type.name.lower(),
            "platform": event.event_platform.name.lower(),
            "population": self.client_config.population_setup().population(event.user_id).name.lower(),
            **extra_tags
        }
        self.inc_metrics_point("events", tags, ts)

        if "product_id" in tags:
            del tags["product_id"]

        if "brand" in tags:
            self.inc_metrics_point("events__brand_total", tags, ts)
            del tags["brand"]

        self.inc_metrics_point("events__total", tags, ts)

    def create_event_v1(self):
        self.assert_token_permission(TokenPermission.SEND_EVENTS)

        data = self.get_post_data_obj()
        self._assert_create_event_data(data)
        product = self.read_product(data.get("product_id"))
        if product is None:
            raise InternalError(f"Product \"{data.get('product_id')}\" does not exist")
        date = None
        if data.get("datetime"):
            date_format = "%Y-%m-%d %H:%M:%S"
            try:
                date = datetime.strptime(data.get("datetime"), date_format)
            except ValueError:
                raise InternalError(f"Can not parse datetime {data.get('datetime')}. Make sure "
                                       f"the correct format is used: {date_format}") from None
        try:
            event_type = data.get("event_type", "UNKNOWN")
            event = Event(
                user_id=data.get("user_id"),
                user_population=self.client_config.population_setup().population(data.get("user_id")),
                product_id=data.get("product_id"),
                product_version=product.version,
                event_type=event_type,
                event_platform=EventPlatform.OTHER,
                date=date or datetime.utcnow()
            )
            self.event_dao.create_event(self.client, event)
            self.__create_event_metrics_v1(event, brand=product.brand, product_id=product.product_id)
            # try:
            #     self.send_event_to_message_queue(event)
            # except MobiDaoException as e:
            #     get_logger().warning(f"Can not submit event to a message queue: {e}")
            history = self.cache_dao.get_user_history(self.client, data.get("user_id"))
            if history is not None:
                history.append(event)
                self.cache_dao.set_user_history(self.client, data.get("user_id"), history)
            return self.format_result()
        except MobiDaoException as e:
            raise InternalError(str(e))

    def delete_events(self):
        if self.token_type != TokenType.TEST:
            raise InternalError("Can not perform delete operation for a non-test token")
        self.event_dao.delete_all_events(self.client)
        return self.format_result()

    def delete_products(self):
        if self.token_type != TokenType.TEST:
            raise InternalError("Can not perform delete operation for a non-test token")
        self.catalog_dao.delete_all_products(self.client)
        return self.format_result()

    def delete_models(self):
        if self.token_type != TokenType.TEST:
            raise InternalError("Can not perform delete operation for a non-test token")
        # self.models_dao.delete_models()
        return self.format_result()

    def delete_all(self):
        if self.token_type != TokenType.TEST:
            raise InternalError("Can not perform delete operation for a non-test token")
        self.event_dao.delete_all_events(self.client)
        self.catalog_dao.delete_all_products(self.client)
        return self.format_result()

    # V2 Methods

    # def send_event_to_message_queue(self, event: Event):
    #     self.message_dao.send_user_event(self.client, event)

    def _create_event_metrics(self, event: Event):
        ts = int(floor(event.date.timestamp()))

        tags = {
            "event_type": event.event_type.name.lower(),
            "population": event.user_population.name.lower(),
            "platform": event.event_platform.name.lower()
        }
        if event.product_id is not None:
            tags["product_id"] = event.product_id
            product = self.read_product(event.product_id)
            if product is not None:
                tags["brand"] = product.brand

            quantity = 1.0
            if event.product_quantity is not None and event.product_quantity > 1:
                quantity = event.product_quantity
            self.inc_metrics_point("events_created", tags, ts, inc=quantity)

            del tags["product_id"]
            self.inc_metrics_point("events_created__brand_total", tags, ts, inc=quantity)

            if "brand" in tags:
                del tags["brand"]
            self.inc_metrics_point("events_created__total", tags, ts, inc=quantity)
        elif event.basket_items is not None:
            product_ids = {bi.product_id for bi in event.basket_items}
            products = self.read_products(list(product_ids))
            product_id_to_brand_map = {product.product_id: product.brand for product in products}

            for basket_item in event.basket_items:
                tags["product_id"] = basket_item.product_id
                if basket_item.product_id in product_id_to_brand_map:
                    tags["brand"] = product_id_to_brand_map[basket_item.product_id]

                quantity = 1.0
                if basket_item.product_quantity is not None and basket_item.product_quantity > 1:
                    quantity = basket_item.product_quantity
                self.inc_metrics_point("events_created", tags, ts, inc=quantity)

                del tags["product_id"]
                self.inc_metrics_point("events_created__brand_total", tags, ts, inc=quantity)

                if "brand" in tags:
                    del tags["brand"]
                self.inc_metrics_point("events_created__total", tags, ts, inc=quantity)

    def _create_product_events_metrics(self, event: Event, product: Product):
        ts = int(floor(event.date.timestamp()))

        tags = {
            "event_type": event.event_type.name.lower(),
            "population": event.user_population.name.lower(),
            "product_id": product.product_id,
            "brand": product.brand
        }
        self.inc_metrics_point("product_events_created", tags, ts)

        del tags["product_id"]
        self.inc_metrics_point("product_events_created__brand_total", tags, ts)

        del tags["brand"]
        self.inc_metrics_point("product_events_created__total", tags, ts)

    def create_event(self):
        self.assert_token_permission(TokenPermission.SEND_EVENTS)

        try:
            event = parse_event(self.get_post_data(throw_exception_if_empty=False))
        except Exception as e:
            raise BadRequestBodyException(f"Event schema validation failed: {str(e)}") from None

        products = []

        event.user_population = self.client_config.population_setup().population(event.user_id)

        if event.product_id is not None:
            product = self.read_product(event.product_id)
            if product is None:
                raise ObjectNotFoundException(f"Product \"{event.product_id}\" does not exist")
            products.append(product)
            event.product_version = product.version

        if event.basket_items is not None:
            for event_basket_item in event.basket_items:
                product = self.read_product(event_basket_item.product_id)
                if product is None:
                    raise ObjectNotFoundException(f"Product \"{event_basket_item.product_id}\" does not exist")
                products.append(product)

        self.event_dao.create_event(self.client, event)

        history = self.cache_dao.get_user_history(self.client, event.user_id)
        if history is not None:
            history.append(event)
            self.cache_dao.set_user_history(self.client, event.user_id, history)

        try:
            self._create_event_metrics(event)
        except Exception as e:
            get_logger().warning(f"Can not store \"events_created\" metric: {e}")

        # try:
        #     self.send_event_to_message_queue(event)
        # except MobiDaoException as e:
        #     get_logger().warning(f"Can not submit event to a message queue: {e}")

        for product in products:
            try:
                self._create_product_events_metrics(event, product)
            except Exception as e:
                get_logger().warning(f"Can not store \"product_events_created\" metric: {e}")

        # TODO: Remove "super" after migrating to V2
        return self.format_result()

    def delete_all_events(self):
        if self.token_type != TokenType.TEST:
            raise TokenHasNoPermissionException("Can not perform delete operation for a non-test token")
        self.assert_token_permission(TokenPermission.SEND_EVENTS)

        self.event_dao.delete_all_events(self.client)
        return self.format_result()

    # Following methods adds legacy v1 support and have to be deleted after we finish migration

    @property
    def raw_token(self) -> Optional[str]:
        authorization_header = request.headers.get("Authorization")
        try:
            authorization_type, authorization_header_token = authorization_header.split(" ")
            if authorization_type != "Bearer":
                authorization_header_token = None
        except:
            authorization_header_token = None
        return authorization_header_token or request.args.get("token") or request.headers.get("Auth-Token")


def get_args() -> argparse.Namespace:
    _default_host = "0.0.0.0"
    _default_port = 9090

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"host listen address. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"defines a port to listen. Default: {_default_port}", type=int)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    event_api = EventApi()
    event_api.app.run(host=args.host, port=args.port)
