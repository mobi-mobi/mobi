from datetime import datetime
from jsonschema import validate, ValidationError, SchemaError
from typing import Any, Union

from mobi.api.parsers.common import parse_raw_json
from mobi.api.parsers.iso4217 import codes as iso4217codes
from mobi.api.parsers.schemas import CRITEO_PRODUCT_SCHEMA, EVENT_SCHEMA, FB_PRODUCT_SCHEMA, PARTIAL_PRODUCT_SCHEMA,\
    PRODUCT_SCHEMA, AUDIENCE_SCHEMA, AUDIENCES_UNION_SCHEMA, AUDIENCE_SPLIT_TREE_SCHEMA
from mobi.core.models._base import MobiModel
from mobi.core.models import Audience, AudiencesUnion, AudienceSplitTree, Event, EventPlatform, EventType, Product
from mobi.exceptions import MobiSchemaValidationException


def validate_fb_product_schema(fb_product_json: Any):
    err_prefix = "Can not parse facebook parameters: "

    try:
        validate(fb_product_json, FB_PRODUCT_SCHEMA)
    except ValidationError as e:
        raise MobiSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise MobiSchemaValidationException(err_prefix + e.message) from None

    fb_enums = {
        "availability": ["in stock", "available for order", "out of stock", "discontinued"],
        "condition": ["new", "refurbished", "used"],
        "age_group": ["adult", "all ages", "teen", "kids", "toddler", "infant", "newborn"],
        "gender": ["female", "male", "unisex"],
        "material": ["cotton", "denim", "leather"],
        "visibility": ["published", "staging", "hidden", "whitelist_only"]
    }

    for enum_name, enum_values in fb_enums.items():
        if fb_product_json.get(enum_name) is not None:
            if fb_product_json[enum_name] not in enum_values:
                raise MobiSchemaValidationException(err_prefix + f"{enum_name} must be one of "
                                                    + ", ".join(enum_values))


def validate_criteo_product_schema(criteo_product_json: Any):
    err_prefix = "Can not parse criteo parameters: "

    try:
        validate(criteo_product_json, CRITEO_PRODUCT_SCHEMA)
    except ValidationError as e:
        raise MobiSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        raise MobiSchemaValidationException(err_prefix + e.message) from None


def validate_product_json(product_json: Any, product_schema: dict = None):
    product_schema = product_schema or PRODUCT_SCHEMA

    fb_properties_name = "facebook_properties"
    criteo_properties_name = "criteo_properties"

    err_prefix = "Can not parse product parameters: "

    try:
        validate(product_json, product_schema)
    except ValidationError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None

    mm_enums = {
        "age_group": ["all_ages", "adult", "teen", "kids", "newborn"],
        "gender": ["female", "male", "unisex"],
    }

    for enum_name, enum_values in mm_enums.items():
        if product_json.get(enum_name) is not None:
            if product_json[enum_name] not in enum_values:
                raise MobiSchemaValidationException(err_prefix + f"{enum_name} must be one of "
                                                    + ", ".join(enum_values))

    mm_date_fields = ["available_from", "available_until", "on_sale_from", "on_sale_until"]

    for field_name in mm_date_fields:
        if product_json.get(field_name) is not None:
            try:
                datetime.strptime(product_json[field_name], MobiModel.DATE_FORMAT)
            except ValueError:
                raise MobiSchemaValidationException(err_prefix + f"Can not parse datetime "
                                                                 f"from \"{field_name}\" field") from None

    currency = product_json.get("currency")
    if currency is not None and currency not in iso4217codes:
        raise MobiSchemaValidationException(err_prefix + f"Bad currency value. Please, use valid ISO-4217 value")

    if fb_properties_name in product_json:
        validate_fb_product_schema(product_json[fb_properties_name])

    if criteo_properties_name in product_json:
        validate_criteo_product_schema(product_json[criteo_properties_name])


def parse_product(raw_json: Union[bytes, str]) -> Product:
    json_obj = parse_raw_json(raw_json)
    validate_product_json(json_obj, PRODUCT_SCHEMA)

    return Product.from_dict(json_obj)


def parse_product_partial(old_product: Product, raw_json: Union[bytes, str, dict]) -> Product:
    json_obj = parse_raw_json(raw_json)
    validate_product_json(json_obj, PARTIAL_PRODUCT_SCHEMA)

    return Product.from_dict({**old_product.as_json_dict(), **json_obj})


def validate_event_json(event_json: Any, event_schema: dict = None):
    event_schema = event_schema or EVENT_SCHEMA
    err_prefix = "Can not parse event parameters: "

    try:
        validate(event_json, event_schema)
    except ValidationError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None

    try:
        event_type = EventType.parse(event_json.get("event_type"))
    except Exception:
        raise MobiSchemaValidationException(err_prefix + f"Can not parse event_type value "
                                                         f"\"{event_json.get('event_type')}\"") from None

    try:
        _ = EventPlatform.parse(event_json.get("event_platform"))
    except Exception:
        raise MobiSchemaValidationException(err_prefix + f"Can not parse event_platform value "
                                                         f"\"{event_json.get('event_platform')}\"") from None

    null_values = {
        EventType.PRODUCT_VIEW: ["product_quantity", "basket_items"],
        EventType.PRODUCT_TO_WISHLIST: ["product_quantity", "basket_items"],
        EventType.PRODUCT_TO_BASKET: ["basket_items"],
        EventType.PRODUCT_SALE: ["product_id", "product_quantity"]
    }

    non_null_values = {
        EventType.PRODUCT_VIEW: ["product_id"],
        EventType.PRODUCT_TO_WISHLIST: ["product_id"],
        EventType.PRODUCT_TO_BASKET: ["product_id", "product_quantity"],
        EventType.PRODUCT_SALE: ["basket_items"]
    }

    if event_type == EventType.PRODUCT_TO_BASKET and "product_quantity" not in event_json:
        event_json["product_quantity"] = 1

    if event_type == EventType.UNKNOWN:
        raise MobiSchemaValidationException(err_prefix + f"Unknown event type: {event_json.get('event_type')}")
    else:
        for null_value in null_values[event_type]:
            if event_json.get(null_value) is not None:
                raise MobiSchemaValidationException(err_prefix + f"Event field \"{null_value}\" must not be set for an "
                                                                 f"event of type {event_type.name.lower()}")
        for non_null_value in non_null_values[event_type]:
            if event_json.get(non_null_value) is None:
                raise MobiSchemaValidationException(err_prefix + f"\"{non_null_value}\" must be defined for an "
                                                                 f"event type of type {event_type.name.lower()}")


def parse_event(raw_json: Union[str, bytes]) -> Event:
    json_obj = parse_raw_json(raw_json)
    validate_event_json(json_obj, EVENT_SCHEMA)

    return Event.from_dict(json_obj)


def validate_audience_json(audience_json: Any, audience_schema: dict = None):
    audience_schema = audience_schema or AUDIENCE_SCHEMA
    err_prefix = "Can not parse audience parameters: "

    try:
        validate(audience_json, audience_schema)
    except ValidationError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None


def parse_audience(raw_json: Union[str, bytes]) -> Audience:
    json_obj = parse_raw_json(raw_json)
    validate_audience_json(json_obj, AUDIENCE_SCHEMA)

    return Audience.from_dict(json_obj)


def validate_audiences_union_json(audiences_union_json: Any, audiences_union_schema: dict = None):
    audiences_union_schema = audiences_union_schema or AUDIENCES_UNION_SCHEMA
    err_prefix = "Can not parse audiences union parameters: "

    try:
        validate(audiences_union_json, audiences_union_schema)
    except ValidationError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None


def parse_audiences_union(raw_json: Union[str, bytes]) -> AudiencesUnion:
    json_obj = parse_raw_json(raw_json)
    validate_audiences_union_json(json_obj, AUDIENCES_UNION_SCHEMA)

    return AudiencesUnion.from_dict(json_obj)


def validate_audience_split_tree_json(audience_split_tree_json: Any, audience_split_tree_schema: dict = None):
    audience_split_tree_schema = audience_split_tree_schema or AUDIENCE_SPLIT_TREE_SCHEMA
    err_prefix = "Can not parse audience split tree parameters: "

    try:
        validate(audience_split_tree_json, audience_split_tree_schema)
    except ValidationError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None
    except SchemaError as e:
        if len(e.absolute_path) > 0:
            err_prefix += f"Parameter {e.absolute_path[0]}: "
        raise MobiSchemaValidationException(err_prefix + e.message) from None


def parse_audience_split_tree(raw_json: Union[str, bytes]) -> AudienceSplitTree:
    json_obj = parse_raw_json(raw_json)
    validate_audience_split_tree_json(json_obj, AUDIENCE_SPLIT_TREE_SCHEMA)

    split_tree = AudienceSplitTree.from_dict(json_obj)

    try:
        split_tree.assert_audience_split_tree()
    except ValueError as e:
        raise MobiSchemaValidationException("Bad structure of the split tree: " + str(e))

    return split_tree
