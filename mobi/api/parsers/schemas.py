def _str_schema(max_len: int = None, nullable: bool = True, pattern: str = None):
    _schema = {"type": "string" if not nullable else ["string", "null"]}
    if max_len is not None:
        _schema["maxLength"] = max_len
    if pattern is not None:
        _schema["pattern"] = pattern
    return _schema


def _mobi_price_schema(nullable: bool = True):
    return _str_schema(max_len=20, nullable=nullable, pattern="^[0-9]+\\.[0-9]{2,2}$")


def _mobi_currency_schema(nullable: bool = True):
    return _str_schema(max_len=10, nullable=nullable, pattern="^[A-Z]{3}$")


def _mobi_date_schema(nullable: bool = True):
    return _str_schema(nullable=nullable, pattern="^[0-9]{4}\\-[0-9]{2}\\-[0-9]{2}\\ [0-9]{2}\\:[0-9]{2}\\:[0-9]{2}$")


def _fb_price_schema(nullable: bool = True):
    return _str_schema(nullable=nullable, pattern="^[0-9]+\\.[0-9]{2}[ ]{1}[A-Z]{3}$")


FB_PRODUCT_SCHEMA = {
    "type": "object",
    "properties": {
        "id": _str_schema(100),
        "title": _str_schema(150),  # Localizable
        "description": _str_schema(5000),  # Localizable
        "availability": _str_schema(),  # Localizable
        "inventory": {"type": "integer", "minimum": 0},
        "condition": _str_schema(),
        "link": _str_schema(1024),  # Localizable
        "image_link": _str_schema(1024),
        "additional_image_link": _str_schema(2000),
        "brand": _str_schema(100),  # Localizable
        "price": _fb_price_schema(),  # Localizable
        "age_group": _str_schema(),
        "color": _str_schema(200),  # Localizable
        "gender": _str_schema(),
        "item_group_id": _str_schema(100),
        "google_product_category": _str_schema(1000),
        "commerce_tax_category": _str_schema(1000),
        "material": _str_schema(),
        "pattern": _str_schema(100),
        "product_type": _str_schema(750),
        "sale_price": _fb_price_schema(),  # Localizable
        "sale_price_effective_date": _str_schema(100),  # Localizable
        "shipping": _str_schema(4096),
        "shipping_weight": _str_schema(20),
        "size": _str_schema(200),  # Localizable
        "custom_label_0": _str_schema(100),  # Localizable
        "custom_label_1": _str_schema(100),  # Localizable
        "custom_label_2": _str_schema(100),  # Localizable
        "custom_label_3": _str_schema(100),  # Localizable
        "custom_label_4": _str_schema(100),  # Localizable
        "rich_text_description": _str_schema(5000),
        "gtin": _str_schema(20),
        "mpn": _str_schema(100),
        "return_policy_info": _str_schema(100),
        "launch_date": _str_schema(100),
        "expiration_date": _str_schema(100),
        "visibility": _str_schema(),
        "mobile_link": _str_schema(1024),
        "additional_variant_attribute": _str_schema(1024),
        # "short_description": {},  # Localizable
        "applink.ios_url": _str_schema(1024),  # Localizable
        "applink.ios_app_store_id": _str_schema(1024),  # Localizable
        "applink.ios_app_name": _str_schema(1024),  # Localizable
        "applink.android_url": _str_schema(1024),  # Localizable
        "applink.android_package": _str_schema(1024),  # Localizable
        "applink.android_app_name": _str_schema(1024),  # Localizable
        "applink.windows_phone_url": _str_schema(1024),  # Localizable
        "applink.windows_phone_app_id": _str_schema(1024),  # Localizable
        "applink.windows_phone_app_name": _str_schema(1024),  # Localizable
        "applink.ipad_url": _str_schema(1024),  # Localizable
        "applink.ipad_app_store_id": _str_schema(1024),  # Localizable
        "applink.ipad_app_name": _str_schema(1024),  # Localizable
    },
    "additionalProperties": False,
}

CRITEO_PRODUCT_SCHEMA = {
    "type": "object"
}


PRODUCT_SCHEMA = {
    "type": "object",
    "properties": {
        "product_id": _str_schema(100, nullable=False),
        "product_group_id": _str_schema(100),
        "title": _str_schema(150, nullable=False),
        "brand": _str_schema(100, nullable=False),
        "categories": {"type": ["array", "null"], "uniqueItems": True, "minItems": 0, "maxItems": 10,
                       "items": _str_schema(100, nullable=False)},
        "color": _str_schema(200),
        "size": _str_schema(200),
        "available_from": _mobi_date_schema(),
        "available_until": _mobi_date_schema(),
        "description": _str_schema(5000),
        "price": _mobi_price_schema(nullable=False),
        "currency": _mobi_currency_schema(nullable=False),
        "sale_price": _mobi_price_schema(),
        "on_sale_from": _mobi_date_schema(),
        "on_sale_until": _mobi_date_schema(),
        "tags": {"type": ["array", "null"], "uniqueItems": True, "minItems": 0, "maxItems": 10,
                 "items": _str_schema(100, nullable=False)},
        "age_group": _str_schema(),
        "gender": _str_schema(),

        "is_searchable": {"type": ["boolean"]},
        "is_recommendable": {"type": ["boolean"]},

        "export_to_facebook": {"type": ["boolean"]},
        "export_to_criteo": {"type": ["boolean"]},
        "facebook_properties": {
            "type": ["object", "null"]
        },
        "criteo_properties": {
            "type": ["object", "null"]
        }
    },
    "additionalProperties": False,
    "required": [
        "product_id", "title", "brand", "price", "currency"
    ]
}

PARTIAL_PRODUCT_SCHEMA = PRODUCT_SCHEMA.copy()
PARTIAL_PRODUCT_SCHEMA["required"] = ["product_id"]


EVENT_SCHEMA = {
    "type": "object",
    "properties": {
        "user_id": _str_schema(100),
        "product_id": _str_schema(100),
        "product_quantity": {"type": ["integer", "null"], "minimum": 1, "maximum": 1000000},
        "event_type": _str_schema(100),
        "event_platform": _str_schema(100),
        "basket_items": {"type": ["array", "null"], "minItems": 1, "maxItems": 1000,
                         "items": {"type": "object", "properties": {
                             "product_id": _str_schema(100),
                             "product_quantity": {"type": ["integer", "null"], "minimum": 1, "maximum": 1000000}
                         }}},
        "date": _mobi_date_schema()
    },
    "additionalProperties": False,
    "required": [
        "user_id", "event_type", "event_platform"
    ]
}


AUDIENCE_SCHEMA = {
    "type": "object",
    "properties": {
        "audience_id": _str_schema(100, nullable=False),
        "audience_name": _str_schema(100, nullable=False),
        "audience_description": _str_schema(1000, nullable=False)
    },
    "additionalProperties": False,
    "required": [
        "audience_id", "audience_name", "audience_description"
    ]
}


AUDIENCES_UNION_SCHEMA = {
    "type": "object",
    "properties": {
        "audiences_union_id": _str_schema(100, nullable=False),
        "audiences_union_name": _str_schema(100, nullable=False),
        "audiences_union_description": _str_schema(1000, nullable=False),
        "audience_ids": {
            "type": "array",
            "minItems": 1,
            "maxItems": 100,
            "items": _str_schema(100, nullable=True)
        }
    },
    "additionalProperties": False,
    "required": [
        "audiences_union_id", "audiences_union_name", "audiences_union_description", "audience_ids"
    ]
}


AUDIENCE_SPLIT_SCHEMA = {
    "type": "object",
    "properties": {
        "split_type": _str_schema(100, nullable=False),
        "split_id": _str_schema(100, nullable=False),
        "children_split_ids": {
            "type": ["null", "array"],
            "minItems": 1,
            "maxItems": 100,
            "items": _str_schema(100, nullable=True)
        },
        "percentiles": {
            "type": ["null", "array"],
            "minItems": 1,
            "maxItems": 100,
            "items": {"type": "number"}
        },
        "quantities": {
            "type": ["null", "array"],
            "minItems": 1,
            "maxItems": 100,
            "items": {"type": "integer"}
        },
        "audience_ids": {
            "type": ["null", "array"],
            "minItems": 1,
            "maxItems": 100,
            "items": _str_schema(100, nullable=False),
        },
        "brands": {
            "type": ["null", "array"],
            "minItems": 1,
            "maxItems": 100,
            "items": _str_schema(100, nullable=False),
        },
        "categories": {
            "type": ["null", "array"],
            "minItems": 1,
            "maxItems": 100,
            "items": _str_schema(100, nullable=False),
        },
        "product_ids": {
            "type": ["null", "array"],
            "minItems": 1,
            "maxItems": 100,
            "items": _str_schema(100, nullable=False),
        },
        "session_length_min": {
            "type": ["null", "integer"],
            "minimum": 1,
            "maximum": 1051201  # 2 years in minutes
        }
    }
}

AUDIENCE_SPLIT_TREE_SCHEMA = {
    "type": "object",
    "properties": {
        "split_tree_id": _str_schema(100, nullable=False),
        "name": _str_schema(100, nullable=False),
        "description": _str_schema(1000, nullable=False),
        "root_split_id": _str_schema(100, nullable=True),
        "audience_splits": {
            "type": "array",
            "minItems": 0,
            "maxItems": 100,
            "items": AUDIENCE_SPLIT_SCHEMA
        }
    },
    "additionalProperties": False,
    "required": [
        "split_tree_id", "name", "description"
    ]
}
