from flask import request
from typing import Iterable, List, Optional, Type

from mobi.core.common import MobiEnum


def _read_single(name: str, required: bool = False, default: str = None) -> Optional[str]:
    try:
        value = request.args.get(name)
    except Exception:
        raise ValueError(f"Can not get a value of a parameter \"{name}\"") from None
    if value is None:
        if required:
            raise ValueError(f"Parameter \"{name}\" must be set")
        return default
    return value


def read_int(name: str, required: bool = False, default: int = None, min_allowed: int = None, max_allowed: int = None) \
        -> Optional[int]:
    str_value = _read_single(name, required)
    if str_value is None:
        return default
    try:
        value = int(str_value)
    except (ValueError, TypeError):
        raise ValueError(f"Wrong parameter value \"{str_value}\" provided for parameter \"{name}\"")
    if min_allowed is not None and value < min_allowed:
        raise ValueError(f"Parameter \"{name}\" must be no lower than \"{min_allowed}\"")
    if max_allowed is not None and value > max_allowed:
        raise ValueError(f"Parameter \"{name}\" must be no bigger than \"{max_allowed}\"")
    return value


def read_str(name: str, required: bool = False, default: str = None, min_length: int = None, max_length: int = None) \
        -> Optional[str]:
    value = _read_single(name, required)
    if value is None:
        return default
    if min_length is not None and len(value) < min_length:
        raise ValueError(f"Parameter \"{name}\" must be no shorter than {min_length} chars")
    if max_length is not None and len(value) > max_length:
        raise ValueError(f"Parameter \"{name}\" must be no longer than {max_length} chars")
    return value


def read_bool(name: str, required: bool = False, default: bool = None, true_values=("yes", "true", "on")) \
        -> Optional[bool]:
    value = _read_single(name, required)
    if value is None:
        return default
    if value.lower() in true_values:
        return True
    return False


def read_enum(name: str, possible_values: Iterable[str], required: bool = False, default: bool = None,
              case_sensitive: bool = False) -> Optional[str]:
    value = _read_single(name, required)
    if value is None:
        return default
    possible_values = [x if case_sensitive else x.lower() for x in possible_values]
    if not case_sensitive:
        value = value.lower()
    if value not in possible_values:
        raise ValueError(f"Unexpected value (\"{value}\") of parameter \"{name}\". Possible values are:"
                         f" {', '.join(possible_values)}")
    return value


def read_mobi_enum(name: str, enum: Type[MobiEnum], required: bool = False, default: MobiEnum = None) \
        -> Optional[MobiEnum]:
    value = _read_single(name, required)
    if value is None:
        return default
    try:
        return enum.parse(value)
    except ValueError:
        raise ValueError(f"Can not parse parameter \"{name}\"") from None


def _read_multiple(name: str, min_items: int = None, max_items: int = None) -> List[str]:
    try:
        values = request.args.getlist(name)
    except Exception:
        raise ValueError(f"Can not get a value of a parameter \"{name}\"") from None
    if min_items is not None and len(values) < min_items:
        raise ValueError(f"Multi-parameter \"{name}\" has to has to consist of at least {min_items} values")
    if max_items is not None and len(values) > max_items:
        raise ValueError(f"Multi-parameter \"{name}\" has to has to consist of at most {max_items} values")
    return values


def read_multiple_str(name, min_items: int = None, max_items: int = None, min_item_length: int = None,
                      max_item_length: int = None) -> List[str]:
    items = _read_multiple(name, min_items, max_items)
    for item in items:
        if min_item_length is not None and len(item) < min_item_length:
            raise ValueError(f"Parameter \"{name}\" values must be no shorter than {min_item_length} chars")
        if max_item_length is not None and len(item) > max_item_length:
            raise ValueError(f"Parameter \"{name}\" values must be no longer than {max_item_length} chars")
    return items
