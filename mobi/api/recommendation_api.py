import argparse
import itertools
import json
import pickle
import random

from collections import Counter
from dataclasses import dataclass
from datetime import datetime, timedelta
from flask import request
from math import floor
from threading import Lock
from typing import Dict, List, Tuple, Optional, Set

from mobi.api.exceptions import BadRequestException, NoLoadedModelException, UserIsInReferenceException, \
    ObjectNotFoundException, InternalError, NoActiveModelsException, NoClientDefinedException
from mobi.api.helpers.ranking import rank_products
from mobi.api.helpers.zone import calc_active_zone_campaign
from mobi.api.parsers import uri
from mobi.api.tokenized_api import TokenizedApi
from mobi.core.reco import merge_organic_and_extra_products
from mobi.config.enums import ModelPickingStrategy
from mobi.config.system.api import ApiBaseConfig
from mobi.config.system.api.recommendation_api import get_recommendation_api_config
from mobi.core.models import Event, EventPlatform, EventType, ModelStatus, Product, Recommendation,\
    RecommendationSource, RecommendationType, TokenPermission
from mobi.core.models.zone import ZoneExtraProductsPosition, ZoneStatus, ZoneType
from mobi.core.population import Population, PopulationSetup
from mobi.dao.cache import cache_key, UserHistoryCookie
from mobi.ml.model.interface import MobiModel
from mobi.ml.model.embedding.impl.static import StaticEmbeddingModel


__all__ = [
    "RecommendationApi"
]


@dataclass
class SearchProductResult:
    product_id: str
    product: Optional[Product]
    index_score: float
    model_score: Optional[float]
    position_after_index: Optional[int]
    position_after_ranking: Optional[int]

    def as_dict(self):
        return {
            "product_id": self.product_id,
            "index_score": self.index_score,
            "model_score": self.model_score,
            "position_after_index": self.position_after_index,
            "position_after_ranking": self.position_after_ranking
        }


class RecommendationApi(TokenizedApi):
    def __init__(self):
        self.__SIMILAR_PRODUCTS_NUM = 10
        self.__MODEL_LOADER_LOCK = Lock()

        self.models: Dict[str, MobiModel] = {}
        self.models_updated_datetime: Dict[str, datetime] = {}
        self.population_setups: Dict[str, PopulationSetup] = {}

        super().__init__("RecommendationApi")

        self.app.before_request(self.ensure_model_is_loaded)
        self.app.before_request(self.ensure_populations_are_loaded)

        # V2 Status method

        self.app.add_url_rule("/v2/recommendation/status", "status", self.status, methods=["GET"])
        self.app.add_url_rule("/v2/search/status", "status", self.status, methods=["GET"])

        # V2 Recommendation methods
        self.app.add_url_rule("/v2/recommendation/products/similar", "recommendations_similar_products",
                              self.recommendations_similar_products)
        self.app.add_url_rule("/v2/recommendation/products/most_popular", "recommendations_most_popular_products",
                              self.recommendations_most_popular_products)
        self.app.add_url_rule("/v2/recommendation/products/personalized", "recommendations_personalized_products",
                              self.recommendations_personalized_products)
        self.app.add_url_rule("/v2/recommendation/products/basket", "recommendations_basket_products",
                              self.recommendations_basket_products)
        self.app.add_url_rule("/v2/recommendation/products/recently_viewed", "recommendations_recently_viewed_products",
                              self.recommendations_recently_viewed_products)
        self.app.add_url_rule("/v2/recommendation/products/zoned", "recommendations_zoned_products",
                              self.recommendations_zoned_products)
        self.app.add_url_rule("/v2/recommendation/products/rank", "recommendations_rank_products",
                              self.recommendations_rank_products)

        # V2 Search methods
        self.app.add_url_rule("/v2/search", "recommendations_search", self.recommendations_search)

        # V2 Test methods

        self.app.add_url_rule("/v2/recommendation/model", "model_info", self.model_info, methods=["GET"])
        self.app.add_url_rule("/v2/recommendation/check/recommendations", "check_recommendations",
                              self.check_recommendations, methods=["GET"])
        self.app.add_url_rule("/v2/recommendation/check/features", "check_features", self.check_features,
                              methods=["GET"])
        self.app.add_url_rule("/v2/recommendation/products/test", "test_recommendations",
                              self.test_recommendation)
        self.app.add_url_rule("/v2/search/test", "test_search", self.test_search)

        # V1 Recommendation methods
        # TODO: Delete all of them after finishing v1 to v2 migration
        self.app.add_url_rule("/user/<user_id>/product/<product_id>/similar", "similar_products_v1",
                              self.similar_products)
        self.app.add_url_rule("/user/<user_id>/recommendations", "personal_recommendations",
                              self.personal_recommendations)
        self.app.add_url_rule("/user/<user_id>/basket_recommendations", "basket_recommendations",
                              self.basket_recommendations)

        # V1 Test methods
        # TODO: Delete these mappings after patching console application
        self.app.add_url_rule("/model", "model_info", self.model_info, methods=["GET"])
        self.app.add_url_rule("/check/recommendations", "check_recommendations", self.check_recommendations,
                              methods=["GET"])
        self.app.add_url_rule("/test/recommendation", "test_recommendations",
                              self.test_recommendation)
        self.app.add_url_rule("/test/search", "test_search", self.test_search)

    @property
    def config(self) -> ApiBaseConfig:
        return get_recommendation_api_config()

    @classmethod
    def debug_population_size(cls) -> float:
        return 0.02

    def __create_recommendation_metric(self, method: str, zone_id: str = None, campaign_id: str = None, **extra_tags):
        ts = int(floor(datetime.utcnow().timestamp()))
        tags = {
            "method": method,
            **extra_tags
        }
        if zone_id is not None:
            tags["zone_id"] = zone_id
        if campaign_id is not None:
            tags["campaign_id"] = campaign_id
        self.inc_metrics_point("recommendations", tags, ts)

    @classmethod
    def v1_product_json_dict(cls, product: Product) -> dict:
        product_json_dict = product.as_json_dict()
        result = {
            key: product_json_dict[key] for key in [
                "title", "brand", "active", "categories", "color", "version", "description"
            ]
        }
        result["id"] = product.product_id
        result["in_stock"] = product.in_stock
        result["is_new"] = product.tags is not None and "new" in product.tags
        result["is_exclusive"] = product.tags is not None and "exclusive" in product.tags
        return result

    # ARGUMENTS RETRIEVAL METHODS

    @classmethod
    def _get_argument__browsing_history(cls, required: bool = True, max_events_num: int = 30) -> UserHistoryCookie:
        browsing_history_raw = request.args.get("browsing_history")

        if browsing_history_raw is None and not required:
            browsing_history_raw = "[]"

        try:
            browsing_history_obj = json.loads(browsing_history_raw)
        except Exception:
            raise BadRequestException(f"Can not parse browsing history") from None

        if not isinstance(browsing_history_obj, list):
            raise BadRequestException(f"User history should be a list")

        if len(browsing_history_obj) > max_events_num:
            raise BadRequestException(f"Too big user browsing history. Max {max_events_num} elements allowed")

        initial_date = datetime.utcnow() - timedelta(hours=1)
        browsing_history = UserHistoryCookie()

        for event_obj_num, event_obj in enumerate(browsing_history_obj):
            if not isinstance(event_obj, dict):
                raise BadRequestException(f"Can not parse event \"{str(event_obj)}\". It should be a valid object.")

            if len(event_obj) != 2:
                raise BadRequestException(f"Each event has to contain exactly two parameters: "
                                          f"product_id and event_type")

            if not event_obj.get("product_id"):
                raise BadRequestException(f"One of the passed events does not have product id")

            if not event_obj.get("event_type"):
                raise BadRequestException(f"One of the passed events does not have event type")

            try:
                event_type = EventType.parse(event_obj["event_type"])
            except Exception:
                raise BadRequestException(f"Can not parse event type \"{event_obj['event_type']}\"") from None

            if event_type == EventType.UNKNOWN:
                raise BadRequestException(f"Unknown event type: \"{event_obj['event_type']}\"")

            event = Event(
                user_id="__test_user_id__",  # Will be ignored, so we can use any dumb user id (so far),
                user_population=Population.TARGET,
                product_id=event_obj["product_id"],
                product_version=0,  # Will be ignored, so we can use any dumb version (so far),
                event_type=event_type,
                event_platform=EventPlatform.OTHER,
                date=initial_date + timedelta(minutes=event_obj_num)
            )

            browsing_history.append(event)

        return browsing_history

    # LOW LEVEL DATA RETRIEVAL METHODS

    def _find_product_candidates(self, recommendation_type: RecommendationType,
                                 source_product: Optional[Product] = None,
                                 user_history: Optional[UserHistoryCookie] = None, brands: Optional[List[str]] = None,
                                 categories: Optional[List[str]] = None, num_per_source: int = 20,
                                 most_popular_brands: Optional[List[str]] = None) \
            -> Tuple[List[Product], Optional[str]]:
        candidate_product_ids = set()

        most_visited_brand = None
        second_most_visited_brand = None
        most_visited_categories = None
        last_visited_brand = None
        last_visited_categories = None
        if user_history is not None:
            last_10_visited_product_ids = []
            for event in reversed(user_history.events):
                if event.event_type == EventType.PRODUCT_VIEW:
                    last_10_visited_product_ids.append(event.product_id)
                if len(last_10_visited_product_ids) == 10:
                    break

            last_10_visited_products = None
            if last_10_visited_product_ids:
                last_10_visited_products = self.read_products(last_10_visited_product_ids, respect_order=True)

            if last_10_visited_products:
                for product in last_10_visited_products:
                    if source_product is None or source_product.product_id != product.product_id:
                        if last_visited_brand is None:
                            last_visited_brand = product.brand
                        if product.categories is not None and last_visited_categories is not None:
                            last_visited_categories = product.categories

                last_10_visited_brands = [product.brand for product in last_10_visited_products]
                last_10_visited_brands_counter = Counter(last_10_visited_brands)
                most_visited_brand = last_10_visited_brands_counter.most_common(1)[0][0]
                if len(last_10_visited_brands_counter) > 1:
                    second_most_visited_brand = last_10_visited_brands_counter.most_common(2)[1][0]

                last_10_visited_categories = Counter()
                for product in last_10_visited_products:
                    if product.categories:
                        for category in product.categories:
                            last_10_visited_categories[category] += 1
                most_visited_categories = [category for category, _ in last_10_visited_categories.most_common(3)]

        extra_brands: Set[str] = set()
        if not brands:
            if most_visited_brand is not None:
                extra_brands.add(most_visited_brand)
            if second_most_visited_brand is not None:
                extra_brands.add(second_most_visited_brand)
            if last_visited_brand is not None:
                extra_brands.add(last_visited_brand)
            if most_popular_brands:
                extra_brands.update(most_popular_brands)

        extra_categories_lists: List[List[str]] = []
        if not categories:
            if most_visited_categories is not None:
                extra_categories_lists.append(most_visited_categories)
            if last_visited_categories is not None:
                extra_categories_lists.append(last_visited_categories)

        if recommendation_type not in (RecommendationType.SIMILAR_PRODUCTS,
                                       RecommendationType.PERSONAL_RECOMMENDATIONS):
            raise ValueError(f"Can not find candidates for unsupported recommendation type {recommendation_type.name}")

        params_sets = {
            "brands": [brands],
            # We should probably replace it with
            # "brands": [None] if not brands else [[brand] for brand in brands]
            "categories": [categories]
        }
        num_to_retrieve = 2 * num_per_source
        if extra_brands:
            num_to_retrieve = num_per_source
            params_sets["brands"].extend([[extra_brand] for extra_brand in extra_brands])
        if extra_categories_lists:
            num_to_retrieve = num_per_source
            params_sets["categories"].extend(extra_categories_lists)

        if recommendation_type == RecommendationType.SIMILAR_PRODUCTS:
            if source_product is None:
                raise ValueError("Source product is not set")

            # for brands, categories in itertools.product(params_sets["brands"], params_sets["categories"]):
            #     candidate_product_ids.update(
            #         self.model.similar_products(source_product, brands=brands, categories=categories,
            #                                     k=num_to_retrieve)
            #     )
            product_ids = self.model.similar_products(source_product, brands=brands, categories=categories,
                                                      k=10*num_to_retrieve)
            products = self.read_products(product_ids, respect_order=True)
            brands_added = Counter()
            for product in products:
                if len(candidate_product_ids) >= 3 * num_to_retrieve:
                    break
                if product.brand == source_product.brand:
                    if brands_added[product.brand] > num_to_retrieve:
                        continue
                else:
                    if brands_added[product.brand] > num_to_retrieve // 2:
                        continue
                brands_added[product.brand] += 1
                candidate_product_ids.add(product.product_id)

            if (not brands or source_product.brand in brands) and brands_added[source_product.brand] \
                    < num_to_retrieve:
                product_ids = self.model.similar_products(source_product, brands=[source_product.brand],
                                                          categories=categories, k=num_to_retrieve)
                candidate_product_ids.update(product_ids)

        if recommendation_type == RecommendationType.PERSONAL_RECOMMENDATIONS:
            if user_history is None or not user_history.events:
                for brands, categories in itertools.product(params_sets["brands"], params_sets["categories"]):
                    candidate_product_ids.update(
                        map(lambda x: x[0], self._most_popular_ids(5*num_to_retrieve, brands=brands,
                                                                   categories=categories))
                    )
            else:
                for brands, categories in itertools.product(params_sets["brands"], params_sets["categories"]):
                    candidate_product_ids.update(
                        self.model.personal_recommendations(user_history.events, brands=brands, categories=categories,
                                                            k=num_to_retrieve)
                    )
                candidate_product_ids.update(
                    self.model.personal_recommendations(user_history.events, brands=brands, categories=categories,
                                                        k=5*num_to_retrieve)
                )

        return self.read_products(list(candidate_product_ids)), most_visited_brand

    def _score_products(self, products: List[Product], recommendation_type: RecommendationType,
                        user_history: UserHistoryCookie, source_product: Optional[Product] = None,
                        ignore_user_product_features: bool = False) -> List[Tuple[Product, float]]:
        if not products:
            return []

        classification_result = self.model.classify_products(self.client, recommendation_type, user_history, products,
                                                             self.cache_dao, source_product,
                                                             ignore_user_product_features)
        if classification_result is not None:
            return [(product, score) for product, score in zip(products, classification_result)]

        if recommendation_type == RecommendationType.SIMILAR_PRODUCTS:
            if source_product is None:
                raise ValueError("Source product should be provided for scoring similar products")

            scores = self.model.rank_products_by_source_product(products, source_product)
            if scores is not None:
                return [(product, score) for product, score in zip(products, scores)]
            else:
                # Hmm.... Is that OKEY?
                return [(product, 0.1 * (len(products) - pos)) for pos, product in enumerate(products)]

        if recommendation_type == RecommendationType.PERSONAL_RECOMMENDATIONS \
                or recommendation_type == RecommendationType.RANK_PRODUCTS:
            scores = self.model.rank_products_by_user_history(products, user_history)
            if scores is not None:
                return [(product, score) for product, score in zip(products, scores)]
            else:
                # Hmm.... Is that OKEY?
                return [(product, 0.1 * (len(products) - pos)) for pos, product in enumerate(products)]

        raise ValueError(f"Can not score products for recommendation type {recommendation_type.name}")

    @classmethod
    def _rank_products(cls, scored_products: List[Tuple[Product, float]], user_history: UserHistoryCookie,
                       source_product: Optional[Product] = None, most_visited_brand: Optional[str] = None,
                       num: int = 20) -> List[Tuple[Product, float]]:
        return rank_products(scored_products, user_history, source_product, most_visited_brand, num)

    def _retrieve_recommended_products(self, product_ids: List[str], recommendations_num: int,
                                       exclude_non_recommended_products: bool = True,
                                       exclude_non_recommended_brands: bool = True,
                                       excluded_product_ids: Optional[Set[str]] = None) -> List[Product]:
        products = self.read_products(product_ids, respect_order=True)
        products = [product for product in products if product.active and product.in_stock]
        if exclude_non_recommended_products:
            try:
                non_recommended_product_ids = self.model.non_recommendable_product_ids
            except NoLoadedModelException:
                non_recommended_product_ids = None
            if non_recommended_product_ids:
                products = [product for product in products if product.product_id not in non_recommended_product_ids]
        if exclude_non_recommended_brands:
            try:
                non_recommended_brands = self.model.non_recommendable_brands
            except NoLoadedModelException:
                non_recommended_brands = None
            if non_recommended_brands:
                products = [product for product in products if product.brand not in non_recommended_brands]
        if excluded_product_ids:
            products = [product for product in products if product.product_id not in excluded_product_ids]
        return products[0:recommendations_num]

    def _most_popular_brands(self, num: int = 10) -> List[str]:
        most_popular_brands = self.cache_dao.get(cache_key("most_popular_brands", client=self.client, interval="8h"),
                                                 retries=3)
        if most_popular_brands:
            most_popular_brands = pickle.loads(most_popular_brands)
        else:
            most_popular_brands = []

        return [brand for brand, _ in most_popular_brands][0:num]

    def _most_popular_ids(self, recommendations_num: int, brands: Optional[List[str]] = None,
                          categories: Optional[List[str]] = None) -> List[Tuple[str, int]]:
        most_popular: Dict[str, int] = dict()

        if brands and categories:
            for brand in set(brands):
                for category in set(categories):
                    most_popular_in_bytes = self.cache_dao.get(
                        cache_key("most_popular", client=self.client, brand=brand, category=category, interval="8h")
                    )
                    if most_popular_in_bytes:
                        for product_id, popularity in pickle.loads(most_popular_in_bytes):
                            most_popular[product_id] = popularity
        elif brands:
            for brand in set(brands):
                most_popular_in_bytes = self.cache_dao.get(
                    cache_key("most_popular", client=self.client, brand=brand, interval="8h")
                )
                if most_popular_in_bytes:
                    for product_id, popularity in pickle.loads(most_popular_in_bytes):
                        most_popular[product_id] = popularity
        elif categories:
            for category in set(categories):
                most_popular_in_bytes = self.cache_dao.get(
                    cache_key("most_popular", client=self.client, category=category, interval="8h")
                )
                if most_popular_in_bytes:
                    for product_id, popularity in pickle.loads(most_popular_in_bytes):
                        most_popular[product_id] = popularity
        else:
            most_popular_in_bytes = self.cache_dao.get(cache_key("most_popular", client=self.client, interval="8h"))
            if most_popular_in_bytes:
                for product_id, popularity in pickle.loads(most_popular_in_bytes):
                    most_popular[product_id] = popularity

        try:
            # As model might not exist
            most_popular_list = filter(lambda x: x[0] not in self.model.non_recommendable_product_ids,
                                       most_popular.items())
        except NoLoadedModelException:
            most_popular_list = most_popular.items()
        most_popular_list = sorted([(k, v) for k, v in most_popular_list], key=lambda x: x[1], reverse=True)

        return most_popular_list[0:recommendations_num]

    def _most_popular(self, recommendations_num: int, brands: List[str] = None, categories: Optional[List[str]] = None,
                      excluded_product_ids: Optional[Set[str]] = None)\
            -> List[Product]:
        product_ids = [product_id for product_id, _ in
                       self._most_popular_ids(2 * recommendations_num, brands=brands, categories=categories)]

        return self._retrieve_recommended_products(product_ids, recommendations_num,
                                                   exclude_non_recommended_products=True,
                                                   exclude_non_recommended_brands=not brands,
                                                   excluded_product_ids=excluded_product_ids)

    def _personal_recommendations_ids(self, user_id: str, recommendations_num: int, brands: List[str] = None,
                                      categories: List[str] = None, historical_events_to_retrieve: int = 10) \
            -> Optional[List[str]]:
        browsing_history = self._get_user_history(user_id).last_events(historical_events_to_retrieve)
        if not browsing_history:
            return None

        product_ids = self.model.personal_recommendations(
            user_session=browsing_history,
            brands=brands,
            categories=categories,
            k=recommendations_num
        )

        return product_ids

    def _personal_recommendations(self, user_history: UserHistoryCookie, recommendations_num: int,
                                  brands: List[str] = None, categories: List[str] = None,
                                  excluded_product_ids: Optional[Set[str]] = None) -> Optional[List[Product]]:
        candidates, most_visited_brand = self._find_product_candidates(RecommendationType.PERSONAL_RECOMMENDATIONS,
                                                                       user_history=user_history,
                                                                       brands=brands, categories=categories)

        datetime_now = datetime.utcnow()
        filtered_candidates = [product for product in candidates
                               if product.product_id not in (excluded_product_ids or {})
                               and product.product_id not in self.model.non_recommendable_product_ids
                               and product.brand not in self.model.non_recommendable_brands
                               and product.is_recommendable
                               and (product.available_from is None or product.available_from < datetime_now)
                               and (product.available_until is None or datetime_now < product.available_until)]

        scored_products = self._score_products(filtered_candidates, RecommendationType.PERSONAL_RECOMMENDATIONS,
                                               user_history)

        ranked_products = self._rank_products(scored_products, user_history,
                                              most_visited_brand=most_visited_brand, num=recommendations_num)
        return [product for product, _ in ranked_products]

    def _similar_products(self, user_history: UserHistoryCookie, product: Product, recommendations_num: int,
                          brands: Optional[List[str]] = None, categories: Optional[List[str]] = None,
                          excluded_product_ids: Optional[Set[str]] = None,
                          ignore_user_product_features: bool = False) -> List[Product]:
        most_popular_brands = None  # self._most_popular_brands(num=5)
        candidates, most_visited_brand = self._find_product_candidates(RecommendationType.SIMILAR_PRODUCTS,
                                                                       source_product=product,
                                                                       user_history=user_history,
                                                                       brands=brands, categories=categories,
                                                                       most_popular_brands=most_popular_brands)
        if not candidates:
            candidates = self._most_popular(recommendations_num * 5, brands=brands, categories=categories,
                                            excluded_product_ids=excluded_product_ids)
            most_visited_brand = None

        datetime_now = datetime.utcnow()
        filtered_candidates = [product for product in candidates
                               if product.product_id not in (excluded_product_ids or {})
                               and product.product_id not in self.model.non_recommendable_product_ids
                               and product.brand not in self.model.non_recommendable_brands
                               and product.is_recommendable
                               and (product.available_from is None or product.available_from < datetime_now)
                               and (product.available_until is None or datetime_now < product.available_until)]

        scored_products = self._score_products(filtered_candidates, RecommendationType.PERSONAL_RECOMMENDATIONS,
                                               user_history, source_product=product,
                                               ignore_user_product_features=ignore_user_product_features)

        ranked_products = self._rank_products(scored_products, user_history,
                                              most_visited_brand=most_visited_brand, num=recommendations_num,
                                              source_product=product)
        return [product for product, _ in ranked_products]

    def _basket_recommendations(self, product_ids: List[str], recommendations_num: int,
                                excluded_product_ids: Optional[Set[str]] = None) -> List[Product]:
        product_ids = self.model.basket_recommendations(product_ids, 2 * recommendations_num)
        products = self._retrieve_recommended_products(product_ids, recommendations_num,
                                                       exclude_non_recommended_products=True,
                                                       exclude_non_recommended_brands=True,
                                                       excluded_product_ids=excluded_product_ids)

        datetime_now = datetime.utcnow()
        filtered_products = [product for product in products
                             if product.product_id not in (excluded_product_ids or {})
                             and product.product_id not in self.model.non_recommendable_product_ids
                             and product.brand not in self.model.non_recommendable_brands
                             and product.is_recommendable
                             and (product.available_from is None or product.available_from < datetime_now)
                             and (product.available_until is None or datetime_now < product.available_until)]

        return filtered_products[0:recommendations_num]

    # HELPERS

    def get_excluded_product_ids(self, user_id: str, exclude_sold: bool, exclude_added_to_basket: bool,
                                 exclude_added_to_wishlist: bool) -> Optional[Set[str]]:
        if not (exclude_sold or exclude_added_to_basket or exclude_added_to_wishlist):
            return None

        exclusion_interval_from = datetime.utcnow() - timedelta(hours=25)

        excluded_product_ids = set()
        for event in self._get_user_history(user_id).events:
            if event.date < exclusion_interval_from:
                continue
            if exclude_sold and event.event_type == EventType.PRODUCT_SALE and event.basket_items:
                for basket_item in event.basket_items:
                    excluded_product_ids.add(basket_item.product_id)
            if exclude_added_to_basket and event.event_type == EventType.PRODUCT_TO_BASKET:
                excluded_product_ids.add(event.product_id)
            if exclude_added_to_wishlist and event.event_type == EventType.PRODUCT_TO_WISHLIST:
                excluded_product_ids.add(event.product_id)

        return excluded_product_ids

    # HIGH LEVEL API METHODS - V2

    def recommendations_similar_products(self):
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            num = uri.read_int("num", default=10, min_allowed=1, max_allowed=20)
            user_id = uri.read_str("user_id", required=True, min_length=1, max_length=100)
            product_id = uri.read_str("product_id", required=True, min_length=1, max_length=100)
            same_brand = uri.read_bool("same_brand", default=False)
            brands = uri.read_multiple_str("brands", max_items=10, min_item_length=1, max_item_length=100)
            categories = uri.read_multiple_str("categories", max_items=10, min_item_length=1, max_item_length=100)
            full_product_info = uri.read_bool("full_product_info", default=False)
            exclude_sold = uri.read_bool("exclude_sold", default=True)
            exclude_added_to_basket = uri.read_bool("exclude_added_to_basket", default=False)
            exclude_added_to_wishlist = uri.read_bool("exclude_added_to_wishlist", default=False)
            ignore_user_product_features = uri.read_bool("disable_user_product_history_features", required=False,
                                                         default=True)
        except ValueError as e:
            raise BadRequestException(str(e)) from None

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        product = self.read_product(product_id)
        if product is None:
            raise ObjectNotFoundException(f"Product \"{product_id}\" does not exist")

        if same_brand:
            if not brands:
                brands = []
            brands.append(product.brand)

        excluded_product_ids = self.get_excluded_product_ids(user_id, exclude_sold, exclude_added_to_basket,
                                                             exclude_added_to_wishlist)

        result = self._similar_products(self._get_user_history(user_id), product, num, brands, categories,
                                        excluded_product_ids, ignore_user_product_features=ignore_user_product_features)

        self.__create_recommendation_metric(RecommendationType.SIMILAR_PRODUCTS.name.lower())

        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.SIMILAR_PRODUCTS,
            source=RecommendationSource.MODEL,
            product_ids=[product.product_id for product in result],
            source_product_id=product_id
        ))

        return self.format_result(
            [product.as_json_dict(exclude_ignored_fields=not full_product_info) for product in result],
            meta={"source": RecommendationSource.MODEL.name.lower()}
        )

    def recommendations_most_popular_products(self):
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            user_id = uri.read_str("user_id", required=True, min_length=1, max_length=100)
            num = uri.read_int("num", default=10, min_allowed=1, max_allowed=20)
            brands = uri.read_multiple_str("brands", max_items=10, min_item_length=1, max_item_length=100)
            categories = uri.read_multiple_str("categories", max_items=10, min_item_length=1, max_item_length=100)
            full_product_info = uri.read_bool("full_product_info", default=False)
            exclude_sold = uri.read_bool("exclude_sold", default=True)
            exclude_added_to_basket = uri.read_bool("exclude_added_to_basket", default=False)
            exclude_added_to_wishlist = uri.read_bool("exclude_added_to_wishlist", default=False)
        except ValueError as e:
            raise BadRequestException(str(e)) from None

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        excluded_product_ids = self.get_excluded_product_ids(user_id, exclude_sold, exclude_added_to_basket,
                                                             exclude_added_to_wishlist)

        products = self._most_popular(num, brands=brands, categories=categories,
                                      excluded_product_ids=excluded_product_ids)

        self.__create_recommendation_metric(RecommendationType.MOST_POPULAR.name.lower())

        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.MOST_POPULAR,
            source=RecommendationSource.MOST_POPULAR_PRODUCTS,
            product_ids=[product.product_id for product in products]
        ))

        return self.format_result([product.as_json_dict(exclude_ignored_fields=not full_product_info)
                                   for product in products])

    def recommendations_personalized_products(self):
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            num = uri.read_int("num", default=10, min_allowed=1, max_allowed=20)
            user_id = uri.read_str("user_id", required=True, min_length=1, max_length=100)
            brands = uri.read_multiple_str("brands", max_items=10, min_item_length=1, max_item_length=100)
            categories = uri.read_multiple_str("categories", max_items=10, min_item_length=1, max_item_length=100)
            full_product_info = uri.read_bool("full_product_info", default=False)
            complete_with_most_popular = uri.read_bool("complete_with_most_popular", default=True)
            exclude_sold = uri.read_bool("exclude_sold", default=True)
            exclude_added_to_basket = uri.read_bool("exclude_added_to_basket", default=False)
            exclude_added_to_wishlist = uri.read_bool("exclude_added_to_wishlist", default=False)
        except ValueError as e:
            raise BadRequestException(str(e)) from None

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        excluded_product_ids = self.get_excluded_product_ids(user_id, exclude_sold, exclude_added_to_basket,
                                                             exclude_added_to_wishlist)

        source = RecommendationSource.MODEL
        products = self._personal_recommendations(self._get_user_history(user_id), num, brands=brands,
                                                  categories=categories, excluded_product_ids=excluded_product_ids)\
            or []

        if complete_with_most_popular:
            if not products:
                source = RecommendationSource.MOST_POPULAR_PRODUCTS

            if len(products) < num:
                seen_product_ids = {product.product_id for product in products}
                for product in self._most_popular(num, brands=brands, categories=categories,
                                                  excluded_product_ids=excluded_product_ids):
                    if product.product_id in seen_product_ids:
                        continue
                    products.append(product)
                    if len(products) == num:
                        break

        self.__create_recommendation_metric(RecommendationType.PERSONAL_RECOMMENDATIONS.name.lower())

        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.PERSONAL_RECOMMENDATIONS,
            source=source,
            product_ids=[product.product_id for product in products]
        ))

        return self.format_result(
            [product.as_json_dict(exclude_ignored_fields=not full_product_info) for product in products],
            meta={"source": source.name.lower()}
        )

    def recommendations_basket_products(self):
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            num = uri.read_int("num", default=10, min_allowed=1, max_allowed=20)
            user_id = uri.read_str("user_id", required=True, min_length=1, max_length=100)
            full_product_info = uri.read_bool("full_product_info", default=False)
            product_ids = uri.read_multiple_str("product_ids", max_items=100, min_item_length=1)
            complete_with_most_popular = uri.read_bool("complete_with_most_popular", default=True)
            exclude_sold = uri.read_bool("exclude_sold", default=True)
            exclude_added_to_basket = uri.read_bool("exclude_added_to_basket", default=False)
            exclude_added_to_wishlist = uri.read_bool("exclude_added_to_wishlist", default=False)
        except ValueError as e:
            raise BadRequestException(str(e)) from None

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        excluded_product_ids = self.get_excluded_product_ids(user_id, exclude_sold, exclude_added_to_basket,
                                                             exclude_added_to_wishlist)

        products = [] if not product_ids else self._basket_recommendations(
            list(set(product_ids)), recommendations_num=num, excluded_product_ids=excluded_product_ids
        )
        source = RecommendationSource.MOST_POPULAR_PRODUCTS

        if len(products) < num and complete_with_most_popular:
            exclude = {product.product_id for product in products}
            exclude.update(product_ids)
            for product in self._most_popular(num, excluded_product_ids=excluded_product_ids):
                if product.product_id not in exclude:
                    exclude.add(product.product_id)
                    products.append(product)

        self.__create_recommendation_metric(RecommendationType.BASKET_RECOMMENDATIONS.name.lower())
        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.BASKET_RECOMMENDATIONS,
            source=source,
            product_ids=[product.product_id for product in products]
        ))

        return self.format_result(
            [product.as_json_dict(exclude_ignored_fields=not full_product_info) for product in products],
            meta={"source": source.name.lower()}
        )

    def recommendations_recently_viewed_products(self):
        self.assert_token_permission(TokenPermission.TEST_RECOMMENDATIONS)

        try:
            user_id = uri.read_str("user_id", required=True, min_length=1, max_length=100)
            full_product_info = uri.read_bool("full_product_info", default=False)
            num = uri.read_int("num", default=10, min_allowed=1, max_allowed=20)
            exclude_sold = uri.read_bool("exclude_sold", default=True)
            exclude_added_to_basket = uri.read_bool("exclude_added_to_basket", default=False)
            exclude_added_to_wishlist = uri.read_bool("exclude_added_to_wishlist", default=False)
        except ValueError as e:
            raise BadRequestException(str(e)) from None

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        browsing_history = self._get_user_history(user_id)

        excluded_product_ids = self.get_excluded_product_ids(user_id, exclude_sold, exclude_added_to_basket,
                                                             exclude_added_to_wishlist) or set()

        product_ids = []

        for event in reversed(browsing_history.events):
            if event.event_type != EventType.PRODUCT_VIEW:
                continue
            if event.product_id not in excluded_product_ids:
                product_ids.append(event.product_id)
                excluded_product_ids.add(event.product_id)
            if len(product_ids) >= 3 * num:
                break

        products = self._retrieve_recommended_products(product_ids, num)

        self.__create_recommendation_metric(RecommendationType.RECENTLY_VIEWED.name.lower())
        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.RECENTLY_VIEWED,
            source=RecommendationSource.MODEL,
            product_ids=[product.product_id for product in products]
        ))

        return self.format_result(
            [product.as_json_dict(exclude_ignored_fields=not full_product_info) for product in products]
        )

    def recommendations_zoned_products(self):
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            num = uri.read_int("num", default=10, min_allowed=1, max_allowed=20)
            user_id = uri.read_str("user_id", required=True, min_length=1, max_length=100)
            zone_id = uri.read_str("zone_id", required=True, min_length=1, max_length=100)
            full_product_info = uri.read_bool("full_product_info", default=False)
            complete_with_most_popular = uri.read_bool("complete_with_most_popular", default=True)
            exclude_sold = uri.read_bool("exclude_sold", default=True)
            exclude_added_to_basket = uri.read_bool("exclude_added_to_basket", default=False)
            exclude_added_to_wishlist = uri.read_bool("exclude_added_to_wishlist", default=False)
        except ValueError as e:
            raise BadRequestException(str(e)) from None

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        try:
            zone = self.settings_dao.read_zone(self.client, zone_id)
        except KeyError:
            raise ObjectNotFoundException(f"Zone \"{zone_id}\" does not exist") from None

        if zone.status == ZoneStatus.DISABLED:
            raise ObjectNotFoundException(f"Zone \"{zone_id}\" does not exist")

        zone_settings = zone.zone_settings

        campaigns = list(self.settings_dao.read_zone_campaigns(self.client, zone_id, include_inactive=False))
        active_campaign = calc_active_zone_campaign(campaigns)

        if active_campaign is not None:
            zone_settings.apply_other(active_campaign.zone_settings)

        num = zone_settings.recommendations_num or num

        excluded_product_ids = self.get_excluded_product_ids(user_id, exclude_sold, exclude_added_to_basket,
                                                             exclude_added_to_wishlist)

        excluded_most_popular_ids = set()

        extra_products = []
        if zone_settings.extra_product_ids:
            extra_products = self.read_products(zone_settings.extra_product_ids, respect_order=True)
            extra_products = [product for product in extra_products if product.active and product.in_stock]
            if excluded_product_ids:
                extra_products = [product for product in extra_products
                                  if product.product_id not in excluded_product_ids]
            extra_products = extra_products[0:num]

        if zone_settings.dont_show_organic_products or len(extra_products) >= num:
            # There is not need in doing anything - we just need to return predefined products
            if extra_products and zone_settings.extra_products_position == ZoneExtraProductsPosition.RANDOM:
                random.shuffle(extra_products)

            organic_products = []
            recommendation_source = "predefined"
            recommendation_metric_type = "static_recommendations"
        elif zone.zone_type == ZoneType.MOST_POPULAR:
            recommendation_source = "most_popular"
            recommendation_metric_type = "most_popular"
            organic_products = self._most_popular(num, zone_settings.brands, zone_settings.categories,
                                                  excluded_product_ids=excluded_product_ids)
        elif zone.zone_type == ZoneType.PERSONAL_RECOMMENDATIONS:
            recommendation_source = "model"
            recommendation_metric_type = "personal_recommendations"
            organic_products = self._personal_recommendations(self._get_user_history(user_id), num,
                                                              zone_settings.brands,
                                                              excluded_product_ids=excluded_product_ids)
            if organic_products is None:
                recommendation_source = "most_popular"
                organic_products = self._most_popular(num, zone_settings.brands, zone_settings.categories,
                                                      excluded_product_ids=excluded_product_ids)
        elif zone.zone_type == ZoneType.SIMILAR_PRODUCTS:
            recommendation_source = "model"
            recommendation_metric_type = "similar_products"
            try:
                product_id = uri.read_str("product_id", required=True, min_length=1, max_length=100)
            except ValueError as e:
                raise BadRequestException(str(e)) from None
            excluded_most_popular_ids.add(product_id)
            product = self.read_product(product_id)
            if product is None:
                raise ObjectNotFoundException(f"Product \"{product_id}\" does not exist")
            brands = None if zone_settings.brands is None else list(zone_settings.brands)
            if zone_settings.same_brand:
                if brands is None:
                    brands = [product.brand]
                else:
                    brands.append(product.brand)
                    brands = list(set(brands))
            categories = None if zone_settings.categories is None else list(zone_settings.categories)
            organic_products = self._similar_products(self._get_user_history(user_id), product, num, brands, categories,
                                                      excluded_product_ids=excluded_product_ids)
        elif zone.zone_type == ZoneType.BASKET_RECOMMENDATIONS:
            recommendation_source = "model"
            recommendation_metric_type = "basket_recommendations"
            product_ids = uri.read_multiple_str("product_ids", min_item_length=1, max_item_length=100, max_items=100)
            excluded_most_popular_ids.update(product_ids)
            organic_products = self._basket_recommendations(product_ids, num, excluded_product_ids=excluded_product_ids)
        else:
            raise InternalError(f"Unknown zone type: {zone.zone_type.name}")

        if extra_products:
            if organic_products:
                organic_products = merge_organic_and_extra_products(organic_products, extra_products,
                                                                    num,
                                                                    zone_settings.save_organic_extra_products_positions,
                                                                    zone_settings.extra_products_position)
            else:
                organic_products = extra_products

        if len(organic_products) < num and complete_with_most_popular:
            excluded_most_popular_ids.update({product.product_id for product in organic_products})
            brands = None
            if zone_settings.brands is not None:
                brands = list(zone_settings.brands)
            categories = None
            if zone_settings.categories is not None:
                categories = list(zone_settings.categories)
            for product in self._most_popular(num, brands=brands, categories=categories):
                if product.product_id not in excluded_most_popular_ids:
                    organic_products.append(product)
                    excluded_most_popular_ids.add(product.product_id)
                if len(organic_products) == num:
                    break

        self.__create_recommendation_metric(
            recommendation_metric_type,
            zone_id=zone_id,
            campaign_id=None if active_campaign is None else active_campaign.campaign_id
        )
        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.STATIC_RECOMMENDATIONS,
            source=RecommendationSource.PREDEFINED,
            product_ids=[product.product_id for product in extra_products],
            zone_id=zone_id,
            campaign_id=None if active_campaign is None else active_campaign.campaign_id
        ))

        return self.format_result(
            [product.as_json_dict(exclude_ignored_fields=not full_product_info) for product in organic_products],
            meta={"source": recommendation_source, "zone_settings": zone_settings.as_json_dict()}
        )

    def recommendations_rank_products(self):
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            user_id = uri.read_str("user_id", required=False, min_length=1, max_length=100)
            full_product_info = uri.read_bool("full_product_info", default=False)
            product_ids = uri.read_multiple_str("product_ids", min_items=1, max_items=50, min_item_length=1,
                                                max_item_length=100)
        except ValueError as e:
            raise BadRequestException(str(e))

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        user_history = self._get_user_history(user_id)

        products = self.read_products(product_ids)
        scored_products = self._score_products(products, RecommendationType.RANK_PRODUCTS, user_history)
        ranked_products = self._rank_products(scored_products, user_history, num=len(products))

        return self.format_result(
            [product.as_json_dict(exclude_ignored_fields=not full_product_info) for product, _ in ranked_products]
        )

    # HIGH LEVEL SEARCH METHODS - V2

    def recommendations_search(self):
        self.assert_token_permission(TokenPermission.SEARCH)

        try:
            num = uri.read_int("num", default=10, max_allowed=20)
            user_id = uri.read_str("user_id", required=False, min_length=1, max_length=100)
            full_product_info = uri.read_bool("full_product_info", default=False)
            query = uri.read_str("q", required=True, min_length=1, max_length=100)
            brands = uri.read_multiple_str("brands", min_item_length=1, max_item_length=100)
        except ValueError as e:
            raise BadRequestException(str(e)) from None

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        result = {}

        # Build user vector with only 10 last events
        user_history = self._get_user_history(user_id)

        search_results = self._find_products(query, 2 * num, user_history, brands=brands)

        datetime_now = datetime.utcnow()
        filtered_search_results = [sr for sr in search_results if
                                   sr.product.is_searchable
                                   and (sr.product.available_from is None or sr.product.available_from < datetime_now)
                                   and (sr.product.available_until is None or datetime_now < sr.product.available_until)
                                   ]

        result["products"] = [{"meta": sr.as_dict(),
                               "product": sr.product.as_json_dict(exclude_ignored_fields=not full_product_info)}
                              for sr in filtered_search_results][0:num]

        self.__create_recommendation_metric("smart_search")

        return self.format_result(result)

    # HIGH LEVEL API METHODS - V1

    def similar_products(self, user_id: str, product_id: str):
        # TODO: Delete this method after migration from V1 to V2 is done
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            num = uri.read_int("num", min_allowed=1, max_allowed=20, default=10)
            brands = uri.read_multiple_str("brands", min_item_length=1, max_item_length=100, max_items=10)
            categories = uri.read_multiple_str("categories", max_items=10, min_item_length=1, max_item_length=100)
            ignore_user_product_features = uri.read_bool("disable_user_product_history_features", required=False,
                                                         default=True)
        except ValueError as e:
            raise BadRequestException(str(e))

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        product = self.read_product(product_id)
        if product is None:
            raise ObjectNotFoundException(f"Product \"{product_id}\" does not exist")

        same_brand = request.args.get("same_brand")

        if same_brand:
            if not brands:
                brands = []
            brands.append(product.brand)

        result = self._similar_products(self._get_user_history(user_id), product, num, brands, categories,
                                        ignore_user_product_features=ignore_user_product_features)

        self.__create_recommendation_metric(RecommendationType.SIMILAR_PRODUCTS.name.lower())

        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.SIMILAR_PRODUCTS,
            source=RecommendationSource.MODEL,
            product_ids=[product.product_id for product in result],
            source_product_id=product_id
        ))

        return self.format_result([self.v1_product_json_dict(product) for product in result])

    def personal_recommendations(self, user_id: str):
        # TODO: Delete this method after migration from V1 to V2 is done
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            num = uri.read_int("num", min_allowed=1, max_allowed=20, default=10)
            brands = uri.read_multiple_str("brands", min_item_length=1, max_item_length=100, max_items=10)
        except ValueError as e:
            raise BadRequestException(str(e))

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        products = self._personal_recommendations(self._get_user_history(user_id), num, brands)

        if products is not None:
            source = "model"
        else:
            source = "most_popular"
            products = self._most_popular(num, brands)

        self.__create_recommendation_metric(RecommendationType.PERSONAL_RECOMMENDATIONS.name.lower())

        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.PERSONAL_RECOMMENDATIONS,
            source=RecommendationSource.MODEL if source == "model" else RecommendationSource.MOST_POPULAR_PRODUCTS,
            product_ids=[product.product_id for product in products]
        ))

        return self.format_result(
            [self.v1_product_json_dict(product) for product in products],
            meta={"source": source}
        )

    def basket_recommendations(self, user_id: str):
        # TODO: Delete this method after migration from V1 to V2 is done
        self.assert_token_permission(TokenPermission.GET_RECOMMENDATIONS)

        try:
            num = uri.read_int("num", min_allowed=1, max_allowed=20, default=10)
            product_ids = uri.read_multiple_str("product_ids", min_item_length=1, max_item_length=100, max_items=100)
        except ValueError as e:
            raise BadRequestException(str(e))

        if self.population(user_id) == Population.REFERENCE:
            raise UserIsInReferenceException("User is in REFERENCE population")

        products = self._basket_recommendations(product_ids, num)

        self.__create_recommendation_metric(RecommendationType.BASKET_RECOMMENDATIONS.name.lower())

        self.recolog_dao.log_recommendation(self.client, Recommendation(
            user_id=user_id,
            date=datetime.utcnow(),
            type=RecommendationType.BASKET_RECOMMENDATIONS,
            source=RecommendationSource.MODEL,
            product_ids=[product.product_id for product in products]
        ))

        return self.format_result([self.v1_product_json_dict(product) for product in products])

    def _find_products(self, query: str, num: int, user_history: UserHistoryCookie,
                       brands: Optional[List[str]] = None) -> List[SearchProductResult]:
        _, products = self.search_dao.search_product(self.client, query, brands=brands, limit=1000, request_timeout=10)

        found_product_items = [
            SearchProductResult(
                product_id=item.product_id,
                product=None,
                index_score=item.score,
                model_score=None,
                position_after_index=position,
                position_after_ranking=None
            )
            for position, item in enumerate(products)
        ]

        products = self.read_products([e.product_id for e in found_product_items], respect_order=True)
        scored_products = self._score_products(products, RecommendationType.PERSONAL_RECOMMENDATIONS, user_history)
        model_scores = {product.product_id: score for product, score in scored_products}

        for position, item in enumerate(found_product_items):
            item.model_score = model_scores[item.product_id]

        search_results = []
        current_group = []
        last_score = None
        max_score_delta = 0.01

        def sort_group(group: List[SearchProductResult]):
            # First, replace None with previous values
            for left, right in zip(group, group[1:]):
                if right.model_score is None and left.model_score is not None:
                    right.model_score = left.model_score
            for left, right in zip(reversed(group[0:-1]), reversed(group)):
                if left.model_score is None and right.model_score is not None:
                    left.model_score = right.model_score

            group.sort(key=lambda x: x.model_score, reverse=True)

        for i, item in enumerate(found_product_items):
            if last_score is not None and abs(last_score - item.index_score) > max_score_delta:
                # Sort current group
                sort_group(current_group)
                # Extend result with current group ids
                search_results.extend(current_group)
                current_group = []

                if len(search_results) >= 2 * num:
                    break

            last_score = item.index_score
            current_group.append(item)

        if current_group:
            sort_group(current_group)
            search_results.extend(current_group)

        search_results = search_results[0:2*num]
        for position, item in enumerate(search_results):
            item.position_after_ranking = position
        products = self.read_products([sr.product_id for sr in search_results], respect_order=True)

        result = []
        for position, item in enumerate(search_results):
            if products[position].active and products[position].in_stock:
                item.product = products[position]
                result.append(item)

        return result

    # TEST METHODS

    def test_recommendation(self):
        self.assert_token_permission(TokenPermission.TEST_RECOMMENDATIONS)

        try:
            recommendation_type = uri.read_enum("recommendation_type", required=True, possible_values=[
                RecommendationType.SIMILAR_PRODUCTS.name,
                RecommendationType.PERSONAL_RECOMMENDATIONS.name,
                RecommendationType.BASKET_RECOMMENDATIONS.name
            ])
            recommendation_type = RecommendationType.parse(recommendation_type)

            recommendations_num = uri.read_int("num", min_allowed=1, max_allowed=20, default=10)
            brands = uri.read_multiple_str("brands", min_item_length=1, max_item_length=100, max_items=10)
            categories = uri.read_multiple_str("categories", min_item_length=1, max_item_length=100, max_items=10)
        except ValueError as e:
            raise BadRequestException(str(e))

        browsing_history = self._get_argument__browsing_history(required=False)

        if recommendation_type == RecommendationType.SIMILAR_PRODUCTS:
            try:
                product_id = uri.read_str("product_id", required=True, min_length=1, max_length=100)
            except ValueError as e:
                raise BadRequestException(str(e))

            product = self.read_product(product_id)
            if product is None:
                raise ObjectNotFoundException(f"Product \"{product_id}\" does not exist")

            return self.format_result(
                [product.as_json_dict() for product in self._similar_products(browsing_history, product,
                                                                              recommendations_num,
                                                                              brands=brands, categories=categories)]
            )

        if recommendation_type == RecommendationType.PERSONAL_RECOMMENDATIONS:
            if browsing_history.events:
                product_ids = self.model.personal_recommendations(
                    user_session=browsing_history.events,
                    brands=brands,
                    k=int(1.5 * recommendations_num)
                )
            else:
                product_ids = [product_id for product_id, _ in self._most_popular_ids(2 * recommendations_num, brands)]

            return self.format_result(
                [product.as_json_dict()
                 for product in self._retrieve_recommended_products(product_ids, recommendations_num)]
            )

        if recommendation_type == RecommendationType.BASKET_RECOMMENDATIONS:
            try:
                basket_product_ids = uri.read_multiple_str("basket_product_ids", max_items=50, min_item_length=1)
            except ValueError as e:
                raise BadRequestException(str(e))

            products = []
            if len(basket_product_ids) > 0:
                products = self._basket_recommendations(basket_product_ids, recommendations_num)

            return self.format_result([product.as_json_dict() for product in products])

    def test_search(self):
        self.assert_token_permission(TokenPermission.TEST_RECOMMENDATIONS)

        user_history = self._get_argument__browsing_history(required=False)

        try:
            brands = uri.read_multiple_str("brands", min_item_length=1, max_item_length=100, max_items=10)
            recommendations_num = uri.read_int("num", min_allowed=1, max_allowed=30, default=10)
            query = uri.read_str("q", required=True, min_length=1, max_length=100)
        except ValueError as e:
            raise BadRequestException(str(e))

        search_results = self._find_products(query, recommendations_num, user_history, brands=brands)

        result = [{"meta": sr.as_dict(), "product": sr.product.as_json_dict(exclude_ignored_fields=True)}
                  for sr in search_results][0:recommendations_num]

        return self.format_result(result)

    def check_recommendations(self):
        self.assert_token_permission(TokenPermission.TEST_RECOMMENDATIONS)

        user_ids = ["unexisting_user_id"] \
                   + self.event_dao.get_n_user_ids(self.client, from_date=datetime.utcnow() - timedelta(hours=1), n=10)

        try:
            brands = uri.read_multiple_str("brands", min_item_length=1, max_item_length=100, max_items=10)
            categories = uri.read_multiple_str("categories", min_item_length=1, max_item_length=100, max_items=10)
            num = uri.read_int("num", min_allowed=1, max_allowed=20, default=10)
            ignore_user_product_features = uri.read_bool("ignore_user_product_features", required=False, default=False)
        except ValueError as e:
            raise BadRequestException(str(e))

        result = []

        for user_id in user_ids:
            browsing_history = self._get_user_history(user_id).last_events(20)
            historical_product_ids = []
            for event in browsing_history:
                if event.product_id is not None:
                    historical_product_ids.append(event.product_id)
                if event.basket_items is not None:
                    for basket_item in event.basket_items:
                        if basket_item.product_id is not None:
                            historical_product_ids.append(basket_item.product_id)

            browsing_products = {product.product_id: product for product in self.read_products(historical_product_ids)}

            browsing_history_as_str = []
            for event in browsing_history:
                s = f"{event.date.strftime('%Y-%m-%d %H:%M:%S')}: "
                if event.product_id:
                    s += browsing_products[event.product_id].brand + ": "
                    s += browsing_products[event.product_id].title
                if event.basket_items:
                    s += "[ "
                    for basket_item in event.basket_items:
                        s += str(basket_item.product_quantity) + " * "
                        s += browsing_products[basket_item.product_id].brand + ": "
                        s += browsing_products[basket_item.product_id].title + " ||| "
                    s += " ]"
                s += f" ({event.event_type.name.lower()})"
                browsing_history_as_str.append(s)

            if not historical_product_ids or random.random() > 0.5:
                source_product = None
                products = self._personal_recommendations(self._get_user_history(user_id), num, brands=brands,
                                                          categories=categories)
            else:
                source_product = self.read_product(random.choice(historical_product_ids))
                products = self._similar_products(self._get_user_history(user_id), source_product, num, brands=brands,
                                                  categories=categories,
                                                  ignore_user_product_features=ignore_user_product_features)

            if products is not None:
                source = "model"
            else:
                source = "most_popular"
                products = self._most_popular(num, brands=brands, categories=categories)

            result.append({
                "user_id": user_id,
                "source": source,
                "recommendation": "similar products" if source_product is not None else "personalized recommendations",
                "source_product": None if source_product is None else source_product.as_json_dict(),
                "browsing_history": browsing_history_as_str,
                "recommendations": [product.brand + ": " + product.title for product in products]
            })

        return self.format_result(result)

    def check_features(self):
        user_id = uri.read_str("user_id", required=False, min_length=1)
        product_id = uri.read_str("product_id", required=True, min_length=1)

        feature_data = self.model.calc_feature_data(self.client, RecommendationType.PERSONAL_RECOMMENDATIONS,
                                                    self._get_user_history(user_id), [self.read_product(product_id)],
                                                    self.cache_dao)

        return self.format_result({
            "float_features": feature_data[product_id].float_features,
            "int_features": feature_data[product_id].int_features,
            "categorical_features": feature_data[product_id].categorical_features
        })

    def _refresh_model(self):
        config = self.config
        client = self.client

        if config.MODEL_PICKING_STRATEGY == ModelPickingStrategy.PREDEFINED:
            if client in self.models and self.models[client].model_id == config.ACTIVE_MODEL_ID:
                return

            model_info = self.models_dao.read_model_dump_info(self.client, config.ACTIVE_MODEL_ID)
            if model_info is None:
                raise InternalError(f"Can not find model {config.ACTIVE_MODEL_ID}")
        elif config.MODEL_PICKING_STRATEGY == ModelPickingStrategy.LATEST:
            model_infos = self.models_dao.read_all_model_dumps(self.client)

            model_info = filter(lambda x: x.status == ModelStatus.READY, model_infos)
            try:
                model_info = max(model_info, key=lambda x: x.date)
            except ValueError:
                raise NoActiveModelsException(description=f"Client \"{client}\" has no active models. "
                                                          f"Please, wait until we train one or contact us "
                                                          f"to get more details.")

            if client in self.models and self.models[client].model_id == model_info.model_id:
                return
        else:
            raise InternalError(f"Unknown picking strategy: {config.MODEL_PICKING_STRATEGY}")
        model = self._load_model(client, model_info.model_type, model_info.model_id)
        self.models[client] = model
        self.models_updated_datetime[client] = datetime.utcnow()

    def _refresh_population_setups(self):
        self.population_setups[self.client] = PopulationSetup(self.client_config.POPULATIONS)

    def ensure_model_is_loaded(self):
        try:
            _ = self.client
        except NoClientDefinedException:
            return

        # This is ugly. Loading model should be moved to the separate thread
        acq_result = False
        try:
            blocking_acquire = self.client not in self.models
            acq_result = self.__MODEL_LOADER_LOCK.acquire(blocking=blocking_acquire)
            if acq_result:
                need_to_load = False
                if self.client in self.models:
                    if self.client not in self.models_updated_datetime:
                        need_to_load = True
                    elif self.models_updated_datetime[self.client] < datetime.utcnow() - timedelta(hours=2):
                        need_to_load = True
                else:
                    need_to_load = True

                if need_to_load:
                    try:
                        self._refresh_model()
                    except Exception as e:
                        raise InternalError(f"Can not refresh model: {str(e)}")
        finally:
            if acq_result:
                self.__MODEL_LOADER_LOCK.release()

    def ensure_populations_are_loaded(self):
        try:
            _ = self.client
        except NoClientDefinedException:
            return

        if self.client not in self.population_setups:
            self._refresh_population_setups()

    def available_models(self):
        self.assert_token_permission(TokenPermission.GET_MODEL_INFO)

        info = [m.as_dict() for m in self.models_dao.read_all_model_dumps(self.client)]
        for i in info:
            i["date"] = i["date"].isoformat()
            i["status"] = ModelStatus.parse(i["status"]).name
        result = {"models": info}
        return self.format_result(result)

    def model_info(self):
        self.assert_token_permission(TokenPermission.GET_MODEL_INFO)

        return self.format_result(self.model.meta_info())

    def populations_info(self):
        self.assert_token_permission(TokenPermission.GET_MODEL_INFO)

        result = {population.name: size for population, size in
                  self.population_setups[self.client].populations.items()}
        return self.format_result(result)

    def _load_model(self, client: str, model_type: str, model_id: str) -> MobiModel:
        model_dump = self.models_dao.read_model_dump(client, model_id)
        if model_type == "STATIC_EMBEDDING_MODEL":
            model = StaticEmbeddingModel.from_dump(model_dump)
            model.compile(list(self.catalog_dao.read_all_products(client)))
            return model
        else:
            raise InternalError(f"Unknown model type: {model_type}")

    @property
    def model(self) -> MobiModel:
        try:
            return self.models[self.client]
        except KeyError:
            raise NoLoadedModelException() from None

    def population(self, uid: str) -> Population:
        return self.population_setups[self.client].population(uid)

    # Following methods adds legacy v1 support and have to be deleted after we finish migration

    @property
    def raw_token(self) -> Optional[str]:
        authorization_header = request.headers.get("Authorization")
        try:
            authorization_type, authorization_header_token = authorization_header.split(" ")
            if authorization_type != "Bearer":
                authorization_header_token = None
        except:
            authorization_header_token = None
        return authorization_header_token or request.args.get("token") or request.headers.get("Auth-Token")


def get_args() -> argparse.Namespace:
    _default_version = 0
    _default_host = "0.0.0.0"
    _default_port = 9191

    parser = argparse.ArgumentParser()

    parser.add_argument("--host", required=False, type=str, default=_default_host,
                        help=f"host listen address. Default: {_default_host}")

    parser.add_argument("-p", "--port", required=False, default=_default_port,
                        help=f"defines a port to listen. Default: {_default_port}", type=int)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    recommendation_api = RecommendationApi()
    recommendation_api.app.run(host=args.host, port=args.port)
