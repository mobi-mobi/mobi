from collections import Counter
from datetime import datetime, timedelta
from typing import List, Optional, Tuple, Set

from mobi.dao.cache import UserHistoryCookie
from mobi.core.models import EventType, Product


def rank_products(scored_products: List[Tuple[Product, float]], user_history: UserHistoryCookie,
                  source_product: Optional[Product] = None, most_visited_brand: Optional[str] = None,
                  num: int = 20) -> List[Tuple[Product, float]]:

    def pop_next_best(*sources: List[Tuple[Product, float]], _ignored_product_ids: Optional[Set[str]] = None,
                      _ignored_brands: Optional[Set[str]] = None) -> Optional[Tuple[Product, float]]:
        def best_candidate_in_source(_source: List[Tuple[Product, float]],
                                     _ignored_product_ids: Optional[Set[str]] = None,
                                     _ignored_brands: Optional[Set[str]] = None) \
                -> Optional[Tuple[Product, float, int]]:
            for _position, _scored_product in enumerate(_source):
                _product, _score = _scored_product
                if _ignored_product_ids is not None and _product.product_id in _ignored_product_ids:
                    continue
                if _ignored_brands is not None and _product.brand in _ignored_brands:
                    continue
                return _product, _score, _position
            return None

        best_result = None
        best_position = None
        best_position_in_source = None

        for source_position, source in enumerate(sources):
            if source:
                _best_candidate = best_candidate_in_source(source, _ignored_product_ids, _ignored_brands)
                if _best_candidate is not None:
                    product_candidate, product_score, product_position = _best_candidate
                    if best_result is None or product_score > best_result[1]:
                        best_result = (product_candidate, product_score)
                        best_position = source_position
                        best_position_in_source = product_position

        if best_position is not None:
            sources[best_position].pop(best_position_in_source)

        return best_result

    products_visited_earlier = {event.product_id for event in user_history.events
                                if event.event_type == EventType.PRODUCT_VIEW
                                and event.date > datetime.utcnow() - timedelta(days=7)}

    products_added_to_basket_or_wishlist_earlier = {event.product_id for event in user_history.events
                                                    if (event.event_type == EventType.PRODUCT_TO_BASKET
                                                        or event.event_type == EventType.PRODUCT_TO_WISHLIST)
                                                    and event.date > datetime.utcnow() - timedelta(days=14)}

    unique_brands_num = len({product.brand for product, _ in scored_products})

    _unique_product_ids = set()
    brand_to_focus_products = []
    other_brand_products = []

    brand_to_focus = None
    if source_product is not None:
        brand_to_focus = source_product.brand
    elif most_visited_brand is not None:
        brand_to_focus = most_visited_brand

    for product, score in scored_products:
        if product.product_id in _unique_product_ids:
            continue
        if source_product is not None and product.product_id == source_product.product_id:
            continue
        _unique_product_ids.add(product.product_id)

        if brand_to_focus is not None and product.brand == brand_to_focus:
            brand_to_focus_products.append((product, score))
        else:
            other_brand_products.append((product, score))

    del _unique_product_ids
    brand_to_focus_products.sort(key=lambda x: x[1], reverse=True)
    other_brand_products.sort(key=lambda x: x[1], reverse=True)

    result: List[Tuple[Product, float]] = []

    while (brand_to_focus_products or other_brand_products) and len(result) < num:
        ignored_product_ids: Set[str] = set()
        ignored_brands: Set[str] = set()

        # RULES DEFINITION

        # The list of rules we currently apply:
        # 1. Don't show two previously visited products next to each other. Starting from the second position
        # 2. Don't show previously added to basket/wishlist products more than once in 4 in-a-row products.
        #    We also don't want to show these products in the top-3
        # 3. Want to ensure that at least two products from top-3 belong to the brand we're focusing on
        #    Also, they should be placed together - this way we try to ensure that at least one was never visited before
        # 4. Want to ensure that two products of the brand that we're focusing on will be in top-5
        # 5. We don't want to show two products with same brands next to each other (except that the brand corresponds
        #    to the brand of the source product)
        # 6. We don't want to show two products with same brands next to each other (rule applies from the
        #    4-th position)
        # 7. We don't want to show tree products of the same brand next to each other
        # 8. No more than 4 product of the brand we're focusing in results
        # 9. No more than 2 product of the same brand (except brand we're focusing on) in results
        # 10.There will be three different brands in every 4 products group. Starting from the 4-th product

        # IMPLEMENTATION

        # 1. Don't show two previously visited products next to each other. Starting from the second position
        skip_visited_before = bool(len(result) > 0 and result[-1][0].product_id in products_visited_earlier)
        if skip_visited_before and products_visited_earlier:
            ignored_product_ids.update(products_visited_earlier)

        # 2. Don't show previously added to basket/wishlist products more than once in 4 in-a-row products.
        #    We also don't want to shoe these products in the top-3
        _once_in = 4
        if len(result) < _once_in - 1 or sum(1 for product, _ in result[-(_once_in-1):] if product.product_id
                                             in products_added_to_basket_or_wishlist_earlier) > 0:
            if products_added_to_basket_or_wishlist_earlier:
                ignored_product_ids.update(products_added_to_basket_or_wishlist_earlier)

        # 3. Want to ensure that at least two products from top-3 belong to the brand we're focusing on
        #    Also, they should be placed together - this way we try to ensure that at least one was never visited before
        if len(result) == 1:
            scored_product = pop_next_best(brand_to_focus_products, _ignored_product_ids=ignored_product_ids)
            if scored_product is not None:
                result.append(scored_product)
                continue
        if len(result) == 2 and brand_to_focus is not None and (result[0][0].brand != brand_to_focus
                                                                or result[1][0].brand != brand_to_focus):
            scored_product = pop_next_best(brand_to_focus_products, _ignored_product_ids=ignored_product_ids)
            if scored_product is not None:
                result.append(scored_product)
                continue

        # 4. Want to ensure that two products of the brand that we're focusing on will be in top-5
        if len(result) == 4 and sum(1 for product, _ in result if product.brand == brand_to_focus) == 2:
            scored_product = pop_next_best(brand_to_focus_products, _ignored_product_ids=ignored_product_ids)
            if scored_product is not None:
                result.append(scored_product)
                continue

        # 5. We don't want to show two products with same brands next to each other (except the focus brand)
        if result and (brand_to_focus is None or result[-1][0].brand != brand_to_focus):
            ignored_brands.add(result[-1][0].brand)

        # 6. We don't want to show two products with same brands next to each other (rule applies from the
        #    4-th position)
        if len(result) > 2:
            ignored_brands.add(result[-1][0].brand)

        # 7. We don't want to show tree products of the same brand next to each other
        if len(result) > 1 and result[-1][0].brand == result[-2][0].brand:
            ignored_brands.add(result[-1][0].brand)

        # 10.There will be three different brands in every 4 products group. Starting from the 4-th product
        if len(result) >= 2:
            last_three_brands = set(product.brand for product, _ in result[-3:])
            if len(last_three_brands) <= 2:
                if unique_brands_num - len(ignored_brands.union(last_three_brands)) >= 1:
                    # We leave at least two brands to choose from
                    ignored_brands.update(last_three_brands)

        # 8. No more than 4 product of the brand we're focusing in results
        # 9. No more than 2 product of the same brand (except brand we're focusing on) in results
        if len(result) > 1:
            brands_in_result = Counter(product.brand for product, _ in result)
            for _added_brand, _num in sorted(brands_in_result.items(), key=lambda x: x[1], reverse=True):
                if unique_brands_num - len(ignored_brands) <= 1:
                    # We leave at least one brand to choose from
                    break
                if brand_to_focus is not None and _added_brand == brand_to_focus:
                    if _num >= int(0.4 * num):
                        ignored_brands.add(_added_brand)
                else:
                    if _num >= int(0.2 * num):
                        ignored_brands.add(_added_brand)

        scored_product = pop_next_best(brand_to_focus_products, other_brand_products,
                                       _ignored_product_ids=ignored_product_ids, _ignored_brands=ignored_brands)
        if scored_product is not None:
            result.append(scored_product)
            continue

        # If we're still in the loop, it means we applied too strict rules.
        # We will first try to ignore ignore_brands one

        scored_product = pop_next_best(brand_to_focus_products, other_brand_products,
                                       _ignored_product_ids=ignored_product_ids)
        if scored_product is not None:
            result.append(scored_product)
            continue

        # If it doesn't help, we will ignore ignored_product_ids rule also

        scored_product = pop_next_best(brand_to_focus_products, other_brand_products)
        if scored_product is not None:
            result.append(scored_product)
            continue

        # We should never be here
        raise ValueError("An internal error happened in ranking function")

    return result
