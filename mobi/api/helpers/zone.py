from datetime import datetime
from typing import Any, Dict, List, Optional

from mobi.core.models._base import MobiModel
from mobi.core.models.zone import Campaign, CampaignStatus, ZoneExtraProductsPosition, ZoneStatus, ZoneType


def calc_next_zone_campaign(campaigns: List[Campaign]) -> Optional[Campaign]:
    next_campaign = None

    for campaign in reversed(campaigns):
        if campaign.status != CampaignStatus.ACTIVE:
            continue
        if campaign.end_date is not None and campaign.end_date < datetime.utcnow():
            continue
        if campaign.start_date is not None and campaign.start_date > datetime.utcnow():
            if next_campaign is None or next_campaign.start_date > campaign.start_date:
                next_campaign = campaign
            continue

    return next_campaign


def calc_active_zone_campaign(campaigns: List[Campaign]) -> Optional[Campaign]:
    active_campaign = None

    for campaign in reversed(campaigns):
        if campaign.end_date is not None and campaign.end_date < datetime.utcnow():
            continue
        if campaign.start_date is not None and campaign.start_date > datetime.utcnow():
            continue

        active_campaign = campaign
        break

    return active_campaign


def calc_closest_zone_campaign(campaigns: List[Campaign]) -> Optional[Campaign]:
    return calc_active_zone_campaign(campaigns) or calc_next_zone_campaign(campaigns)


def validate_zone_settings_json(zone_settings: Dict[str, Any]):
    if not isinstance(zone_settings, dict):
        raise ValueError("zone_settings should be a dict")
    for key in zone_settings.keys():
        if not isinstance(key, str) or not key:
            raise ValueError("parameters of a zone settings object should be valid strings")

    if zone_settings.get("recommendations_num") is None \
            or not isinstance(zone_settings["recommendations_num"], int) or zone_settings["recommendations_num"] < 1:
        raise ValueError("recommendations_num should be a positive integer")

    if zone_settings.get("same_brand") is not None and not isinstance(zone_settings["same_brand"], bool):
        raise ValueError("same_brand should be a valid boolean")

    if zone_settings.get("brands") is not None and not isinstance(zone_settings["brands"], list):
        raise ValueError("brands should be an array")
    if zone_settings.get("brands") is not None:
        if len(zone_settings["brands"]) > 100:
            raise ValueError("Too many brands given. Max allowed size is 100")
        if any(not (isinstance(brand, str) and brand.strip()) for brand in zone_settings.get("brands")):
            raise ValueError("brands should be an array of non-empty strings")
        if any(len(brand) > 256 for brand in zone_settings.get("brands")):
            raise ValueError("One of the brands is loo long. 256 symbols are allowed")

    if zone_settings.get("categories") is not None and not isinstance(zone_settings["categories"], list):
        raise ValueError("categories should be an array")
    if zone_settings.get("categories") is not None:
        if len(zone_settings["categories"]) > 100:
            raise ValueError("Too many categories given. Max allowed size is 100")
        if any(not (isinstance(category, str) and category.strip()) for category in zone_settings.get("categories")):
            raise ValueError("categories should be an array of non-empty strings")
        if any(len(category) > 256 for category in zone_settings.get("categories")):
            raise ValueError("One of the brands is loo long. 256 symbols are allowed")

    if zone_settings.get("extra_product_ids") is not None:
        if not isinstance(zone_settings["extra_product_ids"], list):
            raise ValueError("extra_product_ids should be an array")
        if any(not (isinstance(extra_product_id, str) and extra_product_id.strip())
               for extra_product_id in zone_settings.get("extra_product_ids")):
            raise ValueError("extra_product_ids should be an array of non-empty strings")

    if zone_settings.get("extra_products_position") is not None:
        try:
            ZoneExtraProductsPosition.parse(zone_settings["extra_products_position"])
        except Exception:
            raise ValueError("Can not parse extra_products_position") from None

    if zone_settings.get("save_organic_extra_products_positions") is not None \
            and not isinstance(zone_settings["save_organic_extra_products_positions"], bool):
        raise ValueError("save_organic_extra_products_positions should be a valid boolean")

    if zone_settings.get("dont_show_organic_products") is not None \
            and not isinstance(zone_settings["dont_show_organic_products"], bool):
        raise ValueError("dont_show_organic_products should be a valid boolean")


def validate_zone_json(zone: Dict[str, Any]):
    if not isinstance(zone, dict):
        raise ValueError("zone should be a dict")
    for key in zone.keys():
        if not isinstance(key, str) or not key:
            raise ValueError("parameters of a zone object should be valid strings")

    if zone.get("zone_id") is not None:
        if not (isinstance(zone["zone_id"], str) and zone["zone_id"].strip()):
            raise ValueError("zone_id must be a non-empty string")
        if len(zone["zone_id"]) > 256:
            raise ValueError("zone_id is loo long. 256 symbols are allowed")

    if zone.get("name") is None or not (isinstance(zone["name"], str) and zone["name"].strip()):
        raise ValueError("name must be a non-empty string")
    if len(zone["name"]) > 256:
        raise ValueError("name is loo long. 256 symbols are allowed")

    if zone.get("zone_type") is None:
        raise ValueError("zone_type must be set")
    try:
        ZoneType.parse(zone["zone_type"])
    except Exception:
        raise ValueError("Can not parse zone_type") from None

    if zone.get("description") is not None:
        if not (isinstance(zone["description"], str) and zone["description"].strip()):
            raise ValueError("description should be a valid string")
        if len(zone["description"]) > 500:
            raise ValueError("description is too long. Max 500 symbols are allowed")

    if zone.get("status") is not None:
        try:
            ZoneStatus.parse(zone["status"])
        except Exception:
            raise ValueError("Can not parse status") from None

    if zone.get("campaign_ids") is not None:
        if not isinstance(zone["campaign_ids"], list):
            raise ValueError("campaign_ids must be an array")
        if any(not isinstance(campaign_id, str) for campaign_id in zone["campaign_ids"]):
            raise ValueError("campaign_ids must contain valid strings")
        if any(len(campaign_id) > 256 for campaign_id in zone["campaign_ids"]):
            raise ValueError("One of the campaign ids is too long. 256 symbols ar allowed")

    if zone.get("zone_settings") is None:
        raise ValueError("zone_settings should be defined")
    try:
        validate_zone_settings_json(zone["zone_settings"])
    except ValueError as e:
        raise ValueError(f"Can not parse zone_settings: {str(e)}") from None


def validate_campaign_json(campaign: Dict[str, Any]):
    if not isinstance(campaign, dict):
        raise ValueError("campaign should be a dict")
    for key in campaign.keys():
        if not isinstance(key, str) or not key:
            raise ValueError("parameters of a campaign object should be valid strings")

    if campaign.get("campaign_id") is not None:
        if not (isinstance(campaign["campaign_id"], str) and campaign["campaign_id"].strip()):
            raise ValueError("campaign_id must be a non-empty string")
        if len(campaign["campaign_id"]) > 256:
            raise ValueError("campaign_id is loo long. 256 symbols are allowed")

    if campaign.get("name") is None or not (isinstance(campaign["name"], str) and campaign["name"].strip()):
        raise ValueError("name must be a non-empty string")
    if len(campaign["name"]) > 256:
        raise ValueError("name is loo long. 256 symbols are allowed")

    if campaign.get("description") is not None:
        if not (isinstance(campaign["description"], str) and campaign["description"].strip()):
            raise ValueError("description should be a valid string")
        if len(campaign["description"]) > 500:
            raise ValueError("description is too long. Max 500 symbols are allowed")

    if campaign.get("status") is not None:
        try:
            CampaignStatus.parse(campaign["status"])
        except Exception:
            raise ValueError("Can not parse status") from None

    if campaign.get("start_date") is not None and not isinstance(campaign["start_date"], datetime):
        try:
            MobiModel._parse_datetime(campaign["start_date"])
        except Exception as e:
            raise ValueError(f"Can not parse start_date: {str(e)}") from None

    if campaign.get("end_date") is not None and not isinstance(campaign["end_date"], datetime):
        try:
            MobiModel._parse_datetime(campaign["end_date"])
        except Exception as e:
            raise ValueError(f"Can not parse end_date: {str(e)}") from None

    if campaign.get("zone_settings") is None:
        raise ValueError("zone_settings should be defined")
    try:
        validate_zone_settings_json(campaign["zone_settings"])
    except ValueError as e:
        raise ValueError(f"Can not parse zone_settings: {str(e)}") from None
