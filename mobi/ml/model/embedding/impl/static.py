from abc import ABC, abstractmethod
from collections import Counter
from datetime import datetime
from typing import Any, Dict, Iterable, Optional, List, Tuple, Counter as CounterType

from mobi.core.models import ModelStatus, Product, RecommendationType
from mobi.dao.cache import MobiCache, UserHistoryCookie
from mobi.ml.feature_data import FeatureData
from mobi.ml.model.embedding.interface import EmbeddingModel, EmbeddingModelFactory
from mobi.ml.model.interface import MobiModel
from mobi.ml.model.embedding.impl.factories.catboost import calc_inference_feature_data, CatboostClassifierModel


class StaticEmbeddingModel(EmbeddingModel):
    _DUMP_KEY_PRODUCT_ID_EMBEDDINGS_MAP = "product_id_embeddings_map"
    _DUMP_KEY_BASKET_LINKS = "basket_links"
    _DUMP_KEY_CATBOOST_CLASSIFIER = "catboost_classifier"

    @property
    def model_type(self) -> str:
        return "STATIC_EMBEDDING_MODEL"

    def __init__(self, model_id: str = None, date: datetime = None, status: ModelStatus = ModelStatus.NOT_READY):
        super().__init__(model_id, date, status)
        self.product_id_embeddings_map: Optional[Dict[str, List[float]]] = None
        self.basket_links: Dict[str, CounterType[str]] = dict()
        self.catboost_classifier: Optional[CatboostClassifierModel] = None

    def model_specific_meta_info(self) -> Dict[str, Any]:
        return {
            "product_id_embeddings_map_size": None if self.product_id_embeddings_map is None
            else len(self.product_id_embeddings_map),
            "basket_links_size": None if self.basket_links is None else len(self.basket_links),
            **super(StaticEmbeddingModel, self).model_specific_meta_info()
        }

    def set_product_id_embeddings(self, data: List[Tuple[str, List[float]]]):
        self.product_id_embeddings_map = dict()

        dimensionality = None
        if not data:
            raise ValueError("Embedding data is empty")
        for product_id, product_embedding in data:
            if not product_embedding or not len(product_embedding):
                raise ValueError(f"Product embedding is empty for product \"{product_id}\"")

            if dimensionality is None:
                dimensionality = len(product_embedding)
            elif len(product_embedding) != dimensionality:
                raise ValueError("Products have embeddings of different lengths")

            self.product_id_embeddings_map[product_id] = product_embedding

    def set_basket_links(self, basket_links: Dict[str, CounterType[str]]):
        self.basket_links = basket_links

    def set_catboost_classifier(self, catboost_classifier: CatboostClassifierModel):
        self.catboost_classifier = catboost_classifier

    def basket_recommendations(self, product_ids: Iterable[str], num: int = 10) -> List[str]:
        product_ids = set(product_ids)

        candidates: CounterType[str] = Counter()

        for product_id in product_ids:
            if product_id in self.basket_links:
                for suggested_product_id, count in self.basket_links[product_id].most_common(10 + 3 * num):
                    candidates[suggested_product_id] += count

        result = [p_id
                  for p_id, _ in candidates.most_common(3 * num + len(product_ids))
                  if p_id not in product_ids and p_id not in self.non_recommendable_product_ids
                  ]

        return result[0:num]

    def calc_feature_data(self, client: str, recommendation_type: RecommendationType, user_history: UserHistoryCookie,
                          products: List[Product], cache_dao: MobiCache, source_product: Optional[Product] = None) \
            -> Dict[str, FeatureData]:
        return calc_inference_feature_data(
            client,
            products,
            self.product_to_embedding,
            user_history,
            self.products_map,
            recommendation_type,
            self.categories_idf,
            self.tags_idf,
            self.enriched_tags_idf,
            self.words_idf,
            source_product,
            cache_dao
        )

    def classify_products(self, client: str, recommendation_type: RecommendationType, user_history: UserHistoryCookie,
                          products: List[Product], cache_dao: MobiCache, source_product: Optional[Product] = None,
                          ignore_user_product_features: bool = False) -> Optional[List[float]]:
        if not products:
            return []

        if self.catboost_classifier is None:
            return None

        feature_data = calc_inference_feature_data(
            client,
            products,
            self.product_to_embedding,
            user_history,
            self.products_map,
            recommendation_type,
            self.categories_idf,
            self.tags_idf,
            self.enriched_tags_idf,
            self.words_idf,
            source_product,
            cache_dao
        )

        if ignore_user_product_features:
            for feature_data_item in feature_data.values():
                # Float features
                feature_data_item.float_features["user_product_visits_in_history"] = 0.0
                feature_data_item.float_features["user_product_visits_in_last_1_day"] = 0.0
                feature_data_item.float_features["user_product_visits_in_last_30_min"] = 0.0

                feature_data_item.float_features["user_product_distance"] = 999.9
                feature_data_item.float_features["user_product_distance_30m"] = 999.9
                feature_data_item.float_features["user_product_distance_last_3"] = 999.9

                feature_data_item.float_features["words_tf_idf"] = 0.0
                feature_data_item.float_features["categories_tf_idf"] = 0.0
                feature_data_item.float_features["tags_tf_idf"] = 0.0
                feature_data_item.float_features["enriched_tags_tf_idf"] = 0.0

                # Categorical
                feature_data_item.categorical_features["product_added_to_wishlist_in_history"] = 0
                feature_data_item.categorical_features["product_added_to_basket_in_history"] = 0
                feature_data_item.categorical_features["product_added_to_wishlist_in_history"] = 0

                feature_data_item.categorical_features["user_product_distance_exists"] = 0
                feature_data_item.categorical_features["user_product_distance_last_3_exists"] = 0
                feature_data_item.categorical_features["user_product_distance_30m_exists"] = 0

        return self.catboost_classifier.predict_many([feature_data[p.product_id] for p in products])

    def compile(self, products: List[Product]):
        if self.product_id_embeddings_map is None:
            raise ValueError("Can not compile static model with no static data passed in advance")
        super().compile(products)
        self.status = ModelStatus.READY

    def product_to_embedding(self, product: Product) -> Optional[List[float]]:
        return self.product_id_embeddings_map.get(product.product_id)

    def _dump_binary_as_dict(self) -> Dict[str, Any]:
        return {
            self._DUMP_KEY_PRODUCT_ID_EMBEDDINGS_MAP: self.product_id_embeddings_map,
            self._DUMP_KEY_BASKET_LINKS: self.basket_links,
            self._DUMP_KEY_CATBOOST_CLASSIFIER: self.catboost_classifier,
            **super()._dump_binary_as_dict()
        }

    def _process_dump_binary_as_dict(self, dump: Dict[str, Any]):
        self.product_id_embeddings_map = dump[self._DUMP_KEY_PRODUCT_ID_EMBEDDINGS_MAP]
        self.basket_links = dump[self._DUMP_KEY_BASKET_LINKS]
        del dump[self._DUMP_KEY_PRODUCT_ID_EMBEDDINGS_MAP]
        del dump[self._DUMP_KEY_BASKET_LINKS]
        if self._DUMP_KEY_CATBOOST_CLASSIFIER in dump:
            self.catboost_classifier = dump[self._DUMP_KEY_CATBOOST_CLASSIFIER]
            del dump[self._DUMP_KEY_CATBOOST_CLASSIFIER]
        super()._process_dump_binary_as_dict(dump)


class AbstractStaticEmbeddingModelFactory(EmbeddingModelFactory, ABC):
    @abstractmethod
    def calc_product_embeddings(self, client: str, model: EmbeddingModel) -> List[Tuple[str, List[float]]]:
        pass

    @abstractmethod
    def calc_basket_links(self, client: str, model: EmbeddingModel) -> Dict[str, CounterType[str]]:
        pass

    @abstractmethod
    def calc_catboost_classifier(self, client: str, model: EmbeddingModel) -> CatboostClassifierModel:
        pass

    @abstractmethod
    def calc_idf_data(self, client: str, model: EmbeddingModel) \
            -> Tuple[Dict[str, float], Dict[str, float], Dict[str, float], Dict[str, float]]:
        pass

    def train(self, client: str) -> MobiModel:
        model = StaticEmbeddingModel()
        model.set_product_id_embeddings(self.calc_product_embeddings(client, model))
        model.set_basket_links(self.calc_basket_links(client, model))
        model.set_idf_data(*self.calc_idf_data(client, model))
        model.set_catboost_classifier(self.calc_catboost_classifier(client, model))
        return model
