import catboost

from abc import ABC, abstractmethod
from collections import Counter, defaultdict
from dataclasses import dataclass
from datetime import datetime, timedelta
from itertools import chain
from math import sqrt
from random import choices, shuffle
from typing import Callable, Dict, Generator, Iterable, List, Optional, Set, Tuple, Union

from mobi.core.counters import create_brand_counters, create_brand_total_counters, create_product_counters,\
    create_product_total_counters, PRODUCT_EVENTS_COUNTERS_SPECS, BRAND_EVENTS_COUNTERS_SPECS
from mobi.core.counters import ComplexDynamicCounter, DynamicCounter
from mobi.core.logging import get_logger
from mobi.core.models import AttributionType, AttributedRecommendation, Event, EventType, Product, Recommendation,\
    RecommendationType
from mobi.dao.cache import cache_key, MobiCache
from mobi.dao.event import MobiEventDao
from mobi.dao.recolog import MobiRecologDao
from mobi.dao.cache.common import UserHistoryCookie
from mobi.ml.feature_data import FeatureData


_last_event: Optional[Event] = None
_user_histories: Dict[str, UserHistoryCookie] = defaultdict(lambda: UserHistoryCookie())
_pos_weights: List[int] = [0 for _ in range(1000)]
_product_id_to_brand_map: Optional[Dict[str, str]] = None
_product_counters: Optional[Dict[EventType, Dict[str, ComplexDynamicCounter]]] = None
_brand_counters: Optional[Dict[EventType, Dict[str, ComplexDynamicCounter]]] = None
_total_product_counters: Optional[Dict[EventType, Dict[str, DynamicCounter]]] = None
_total_brand_counters: Optional[Dict[EventType, Dict[str, DynamicCounter]]] = None


@dataclass
class LabeledRecommendation:
    recommendation: Recommendation
    labeled_product_positions: List[Tuple[int, int]]

    @property
    def labeled_product_ids(self) -> List[Tuple[str, int]]:
        return [(self.recommendation.product_ids[product_position], label)
                for product_position, label in self.labeled_product_positions]


def init_counters():
    global _product_counters
    _product_counters = create_product_counters()
    global _total_product_counters
    _total_product_counters = create_product_total_counters()

    global _brand_counters
    _brand_counters = create_brand_counters()
    global _total_brand_counters
    _total_brand_counters = create_brand_total_counters()


def inc_training_counters(event: Event, products_map: Dict[str, Product]):
    def value_from_event(event_: Event) -> Dict[str, int]:
        result = {}
        if event_.product_id is not None:
            result[event_.product_id] = event_.product_quantity if event_.product_quantity is not None else 1
        if event_.basket_items is not None:
            for basket_item in event_.basket_items:
                result[basket_item.product_id] = basket_item.product_quantity
        return result

    global _product_counters
    global _total_product_counters
    global _brand_counters
    global _total_brand_counters

    for product_id, value in value_from_event(event):
        for counter in _product_counters.get(event.event_type, dict()).values():
            counter.inc(key=product_id, value=value, dt=event.date)
        for counter in _total_product_counters.get(event.event_type, dict()).values():
            counter.inc(value=value, dt=event.date)

        product = products_map.get(product_id)
        if product is not None:
            brand = product.brand
        else:
            brand = None

        if brand is not None:
            for counter in _brand_counters.get(event.event_type, dict()).values():
                counter.inc(key=brand, value=value, dt=event.date)
            for counter in _total_brand_counters.get(event.event_type, dict()).values():
                counter.inc(value=value, dt=event.date)


def get_prepared_for_training():
    global _last_event
    _last_event = None
    global _user_histories
    _user_histories = defaultdict(lambda: UserHistoryCookie())
    global _pos_weights
    _pos_weights = [0 for _ in range(1000)]
    init_counters()


def process_events_until(events: Generator[Event, None, None], dt: datetime, products_map: Dict[str, Product]):
    global _last_event
    global _product_counters
    global _total_product_counters
    global _brand_counters
    global _total_brand_counters
    global _user_histories

    while True:
        if _last_event is not None:
            event, _last_event = _last_event, None
        else:
            try:
                event = next(events)
            except StopIteration:
                event = None

        if event is None:
            break

        if event.date > dt - timedelta(seconds=3):
            _last_event = event
            break

        product_ids_to_inc = Counter()
        brand_ids_to_inc = Counter()

        if event.product_id:
            product_ids_to_inc[event.product_id] += (event.product_quantity or 1)
            if event.product_id in products_map:
                brand_ids_to_inc[products_map[event.product_id].brand] += (event.product_quantity or 1)
        if event.basket_items:
            for basket_item in event.basket_items:
                product_ids_to_inc[basket_item.product_id] += basket_item.product_quantity
                if basket_item.product_id in products_map:
                    brand_ids_to_inc[products_map[basket_item.product_id].brand] += basket_item.product_quantity

        for counter_name, counter in _product_counters.get(event.event_type, dict()).items():
            for product_id, value in product_ids_to_inc.items():
                counter.inc(product_id, value)
                _total_product_counters[event.event_type][counter_name].inc(value)

        for counter_name, counter in _brand_counters.get(event.event_type, dict()).items():
            for brand, value in brand_ids_to_inc.items():
                counter.inc(brand, value)
                _total_brand_counters[event.event_type][counter_name].inc(value)

        # inc_training_counters(event, products_map)
        _user_histories[event.user_id].append(event, cap_with_event_dt=True)


def generate_training_labeled_attr_recommendations(events: Generator[Event, None, None],
                                                   attributed_recommendations: Generator[AttributedRecommendation,
                                                                                         None, None],
                                                   ignore_recommendations_before: datetime,
                                                   products_map: Dict[str, Product]) \
        -> Generator[LabeledRecommendation, None, None]:
    global _pos_weights

    for attributed_recommendation in attributed_recommendations:
        recommendation = attributed_recommendation.recommendation
        process_events_until(events, recommendation.date, products_map)

        if recommendation.date < ignore_recommendations_before:
            continue

        labeled_recommendation = LabeledRecommendation(recommendation, [])

        clicked_product_ids = {event.product_id for event in attributed_recommendation.attributed_events
                               if event.product_id is not None}

        for product_id in clicked_product_ids:
            position = recommendation.product_ids.index(product_id)
            _pos_weights[position] += 1

            labeled_recommendation.labeled_product_positions.append((position, 1))

        negatives_added = 0

        for negative_product_id_position in choices(
            list(range(len(recommendation.product_ids))),
            weights=_pos_weights[0:len(recommendation.product_ids)],
            k=min(2 * len(clicked_product_ids), len(recommendation.product_ids))
        ):
            if negatives_added == len(clicked_product_ids):
                break

            negative_product_id = recommendation.product_ids[negative_product_id_position]
            if negative_product_id in clicked_product_ids:
                continue

            labeled_recommendation.labeled_product_positions.append((negative_product_id_position, 0))
            negatives_added += 1

        yield labeled_recommendation


def _spike(metric_1h: Optional[Union[float, int]], metric_24h: Optional[Union[float, int]]) -> float:
    metric_1h = metric_1h if metric_1h is not None else 0
    metric_24h = metric_24h if metric_24h is not None else 0
    return max(1, metric_1h) / max(23.0, (metric_24h - metric_1h) / 23)


def calc_inference_product_brand_counter_features(client: str, products: List[Product], cache_dao: MobiCache,
                                                  date: datetime) -> Dict[str, FeatureData]:
    product_feature_cache_keys: Dict[str, Dict[str, str]] = defaultdict(dict)
    total_product_counters_keys: Dict[str, str] = dict()
    all_cache_keys: Set[str] = set()

    for event_type in PRODUCT_EVENTS_COUNTERS_SPECS:
        for counter_name, _ in PRODUCT_EVENTS_COUNTERS_SPECS[event_type].items():
            for product in products:
                _cache_key = cache_key(counter_name, client=client, product_id=product.product_id)
                product_feature_cache_keys[counter_name][product.product_id] = _cache_key
                all_cache_keys.add(_cache_key)
            _total_cache_key = cache_key(counter_name, client=client)
            total_product_counters_keys[counter_name] = _total_cache_key
            all_cache_keys.add(_total_cache_key)

    for event_type in BRAND_EVENTS_COUNTERS_SPECS:
        for counter_name in BRAND_EVENTS_COUNTERS_SPECS[event_type]:
            for product in products:
                _cache_key = cache_key(counter_name, client=client, brand=product.brand)
                product_feature_cache_keys[counter_name][product.product_id] = _cache_key
                all_cache_keys.add(_cache_key)
            _total_cache_key = cache_key(counter_name, client=client)
            total_product_counters_keys[counter_name] = _total_cache_key
            all_cache_keys.add(_total_cache_key)

    all_cache_keys_as_list = list(all_cache_keys)

    batch_size = 50
    cache_data: Dict[str, int] = dict()

    while all_cache_keys_as_list:
        keys_to_send, all_cache_keys_as_list = all_cache_keys_as_list[0:batch_size], all_cache_keys_as_list[batch_size:]
        cache_data.update(cache_dao.get_many_int(keys_to_send, retries=3))

    def float_features(_product_id: str) -> Dict[str, float]:
        _result: Dict[str, float] = dict()
        for _counter_name in product_feature_cache_keys:
            _result[_counter_name] = cache_data.get(product_feature_cache_keys[_counter_name][_product_id], 0.0)
            total_value = cache_data.get(total_product_counters_keys[_counter_name], 0)
            if total_value > 0:
                _result[_counter_name] /= total_value
            else:
                _result[_counter_name] = 0.0
            _result[_counter_name] = max(_result[_counter_name], 0.0)
        _result["product_views_spike"] = \
            _spike(cache_data.get(product_feature_cache_keys["product_views_1h"][_product_id], 0.0),
                   cache_data.get(product_feature_cache_keys["product_views_24h"][_product_id], 0.0))
        _result["brand_views_spike"] = \
            _spike(cache_data.get(product_feature_cache_keys["brand_views_1h"][_product_id], 0.0),
                   cache_data.get(product_feature_cache_keys["brand_views_24h"][_product_id], 0.0))

        return _result

    return {
        product.product_id: FeatureData(
            categorical_features=None,
            int_features=None,
            float_features=float_features(product.product_id),
            date=date
        ) for product in products
    }


def calc_training_product_counter_features(product_ids: Iterable[str], now_dt: datetime = None) \
        -> Dict[str, FeatureData]:
    global _product_counters
    global _total_product_counters
    now_dt = now_dt or datetime.utcnow()

    return {
        product_id: FeatureData(
            categorical_features=None,
            int_features=None,
            float_features={
                counter_name: _product_counters[_event_type][counter_name].value(product_id, now_dt=now_dt)
                / (10 + _total_product_counters[_event_type][counter_name].value(now_dt=now_dt))
                for _event_type in _product_counters.keys()
                for counter_name in _product_counters[_event_type].keys()
            },
            date=now_dt
        ).add_float("product_views_spike", _spike(_product_counters[EventType.PRODUCT_VIEW]["product_views_1h"]
                                                  .value(product_id, now_dt=now_dt),
                                                  _product_counters[EventType.PRODUCT_VIEW]["product_views_24h"]
                                                  .value(product_id, now_dt=now_dt)))
        for product_id in product_ids
    }


def calc_training_brand_counter_features(brands: Iterable[str], now_dt: datetime = None) -> Dict[str, FeatureData]:
    global _brand_counters
    global _total_brand_counters
    now_dt = now_dt or datetime.utcnow()

    return {
        brand: FeatureData(
            categorical_features=None,
            int_features=None,
            float_features={
                counter_name: _brand_counters[_event_type][counter_name].value(brand, now_dt=now_dt)
                / (10 + _total_brand_counters[_event_type][counter_name].value(now_dt=now_dt))
                for _event_type in _brand_counters.keys()
                for counter_name in _brand_counters[_event_type].keys()
            },
            date=now_dt
        ).add_float("brand_views_spike", _spike(_brand_counters[EventType.PRODUCT_VIEW]["brand_views_1h"]
                                                .value(brand, now_dt=now_dt),
                                                _brand_counters[EventType.PRODUCT_VIEW]["brand_views_24h"]
                                                .value(brand, now_dt=now_dt)))
        for brand in brands
    }


def calc_user_history_features(products: List[Product], user_history: UserHistoryCookie,
                               products_map: Dict[str, Product], categories_idf: Dict[str, float],
                               tags_idf: Dict[str, float], enriched_tags_idf: Dict[str, float],
                               words_idf: Dict[str, float], source_product: Optional[Product] = None,
                               date: datetime = None) -> Dict[str, FeatureData]:
    date = date or datetime.utcnow()
    event_views_num = 0

    visited_brands_last_3 = []
    visited_brands_last_10 = []
    visited_brands_last_1_day = []
    visited_brands_all = []
    visited_products_last_30_min = []
    visited_products_last_1_day = []
    visited_products_all = []

    product_added_to_wishlist_in_last_1_day = set()
    product_added_to_wishlist_in_history = set()
    product_added_to_basket_in_last_1_day = set()
    product_added_to_basket_in_history = set()
    brand_added_to_wishlist_in_last_1_day = set()
    brand_added_to_wishlist_in_history = set()
    brand_added_to_basket_in_last_1_day = set()
    brand_added_to_basket_in_history = set()

    # -- TF-IDF-COUNTERS --
    tf_idf_documents = 0
    categories_counter = Counter()
    tags_counter = Counter()
    enriched_tags_counter = Counter()
    words_counter = Counter()
    # -- END OF TF-IDF-COUNTERS --

    for event in reversed(user_history.events):
        if date < event.date:
            continue

        if event.event_type == EventType.PRODUCT_VIEW:
            event_views_num += 1

        event_product = products_map.get(event.product_id)

        if event_views_num <= 3:
            if event.event_type == EventType.PRODUCT_VIEW and event_product is not None:
                visited_brands_last_3.append(event_product.brand)
        if event_views_num <= 10:
            if event.event_type == EventType.PRODUCT_VIEW and event_product is not None:
                visited_brands_last_10.append(event_product.brand)
        if date - timedelta(minutes=30) < event.date:
            if event.event_type == EventType.PRODUCT_VIEW:
                visited_products_last_30_min.append(event.product_id)
        if date - timedelta(days=1) < event.date:
            if event.event_type == EventType.PRODUCT_VIEW:
                visited_products_last_1_day.append(event.product_id)
                if event_product is not None:
                    visited_brands_last_1_day.append(event_product.brand)
            if event.event_type == EventType.PRODUCT_TO_WISHLIST:
                product_added_to_wishlist_in_last_1_day.add(event.product_id)
                if event_product is not None:
                    brand_added_to_wishlist_in_last_1_day.add(event_product.brand)
            if event.event_type == EventType.PRODUCT_TO_BASKET:
                product_added_to_basket_in_last_1_day.add(event.product_id)
                if event_product is not None:
                    brand_added_to_basket_in_last_1_day.add(event_product.brand)

        if event.event_type == EventType.PRODUCT_VIEW:
            visited_products_all.append(event.product_id)
            if event_product is not None:
                visited_brands_all.append(event_product.brand)
        if event.event_type == EventType.PRODUCT_TO_WISHLIST:
            product_added_to_wishlist_in_history.add(event.product_id)
            if event_product is not None:
                brand_added_to_wishlist_in_history.add(event_product.brand)
        if event.event_type == EventType.PRODUCT_TO_BASKET:
            product_added_to_basket_in_history.add(event.product_id)
            if event_product is not None:
                brand_added_to_basket_in_history.add(event_product.brand)

        if event.event_type == EventType.PRODUCT_VIEW and event_product is not None:
            tf_idf_documents += 1
            if event_product.categories:
                for category in event_product.categories:
                    categories_counter[category] += 1
            if event_product.tags:
                for tag in event_product.tags:
                    tags_counter[tag] += 1
            if event_product.enriched_tags:
                for enriched_tag in event_product.enriched_tags:
                    enriched_tags_counter[enriched_tag] += 1
            for word in event_product.title.split():
                if word:
                    words_counter[word.lower()] += 1

    visited_brands_last_3_counter = Counter(visited_brands_last_3)
    visited_brands_last_10_counter = Counter(visited_brands_last_10)
    visited_brands_last_1_day_counter = Counter(visited_brands_last_1_day)
    visited_products_last_30_min_counter = Counter(visited_products_last_30_min)
    visited_products_last_1_day_counter = Counter(visited_products_last_1_day)
    visited_products_all_counter = Counter(visited_products_all)
    visited_brands_all_counter = Counter(visited_brands_all)

    def _calc_tf_idf(_product_parameter_value: Optional[Iterable[str]], _parameter_idf: Dict[str, float],
                     _parameter_counter: Dict[str, int], _tf_idf_documents: int) -> float:
        if not _product_parameter_value or not _tf_idf_documents:
            return 0.0

        return sum(
            _parameter_idf.get(_param, 0.0) * _parameter_counter.get(_param, 0) / _tf_idf_documents
            for _param in _product_parameter_value
        ) or 0.0

    def _calc_same_product_tf_idf(_product_parameter_value: Optional[Iterable[str]],
                                  _source_product_parameter_value: Optional[Iterable[str]],
                                  _parameter_idf: Dict[str, float]) -> float:
        if not _product_parameter_value or not _source_product_parameter_value:
            return 0.0
        return sum(
            _parameter_idf.get(_param, 0.0) for _param in _product_parameter_value
            if _param in _source_product_parameter_value
        ) or 0.0

    source_product_categories = None if source_product is None or not source_product.categories \
        else set(source_product.categories)
    source_product_tags = None if source_product is None or not source_product.tags else set(source_product.tags)
    source_product_enriched_tags = None if source_product is None or not source_product.enriched_tags \
        else set(source_product.enriched_tags)
    source_product_words = None if source_product is None else set(w.lower() for w in source_product.title.split() if w)

    return {
        product.product_id: FeatureData(
            categorical_features={
                "product_added_to_wishlist_in_last_1_day": int(product.product_id
                                                               in product_added_to_wishlist_in_last_1_day),
                "product_added_to_basket_in_last_1_day": int(product.product_id
                                                             in product_added_to_basket_in_last_1_day),
                "product_added_to_wishlist_in_history": int(product.product_id in product_added_to_wishlist_in_history),
                "product_added_to_basket_in_history": int(product.product_id in product_added_to_basket_in_history),
                "brand_added_to_wishlist_in_last_1_day": int(product.brand in brand_added_to_wishlist_in_last_1_day),
                "brand_added_to_basket_in_last_1_day": int(product.brand in brand_added_to_basket_in_last_1_day),
                "brand_added_to_wishlist_in_history": int(product.brand in brand_added_to_wishlist_in_history),
                "brand_added_to_basket_in_history": int(product.brand in brand_added_to_basket_in_history),
                "is_source_product_exists": int(source_product is not None),
                "is_source_brand_same": -1 if source_product is None else int(source_product.brand == product.brand)
            },
            float_features={
                "user_brand_visits_in_last_1_day": 0.0 if not visited_brands_last_1_day
                else visited_brands_last_1_day_counter[product.brand] / len(visited_brands_last_1_day),
                "user_brand_visits_in_history": 0.0 if not visited_brands_all
                else visited_brands_all_counter[product.brand] / len(visited_brands_all),
                "user_product_visits_in_last_30_min": 0.0 if not visited_products_last_30_min
                else visited_products_last_30_min_counter[product.product_id] / len(visited_products_last_30_min),
                "user_product_visits_in_last_1_day": 0.0 if not visited_products_last_1_day
                else visited_products_last_1_day_counter[product.product_id] / len(visited_products_last_1_day),
                "user_product_visits_in_history": 0.0 if not visited_products_all
                else visited_products_all_counter[product.product_id] / len(visited_products_all),
                "categories_tf_idf": _calc_tf_idf(product.categories, categories_idf, categories_counter,
                                                  tf_idf_documents),
                "tags_tf_idf": _calc_tf_idf(product.tags, tags_idf, tags_counter, tf_idf_documents),
                "enriched_tags_tf_idf": _calc_tf_idf(product.enriched_tags, enriched_tags_idf, enriched_tags_counter,
                                                     tf_idf_documents),
                "words_tf_idf": _calc_tf_idf([w.lower() for w in product.title.split()], words_idf, words_counter,
                                             tf_idf_documents),
                "source_product_categories_tf_idf": 0.0 if not source_product
                else _calc_same_product_tf_idf(product.categories, source_product_categories, categories_idf),
                "source_product_tags_tf_idf": 0.0 if not source_product
                else _calc_same_product_tf_idf(product.tags, source_product_tags, tags_idf),
                "source_product_enriched_tags_tf_idf": 0.0 if not source_product
                else _calc_same_product_tf_idf(product.enriched_tags, source_product_enriched_tags, enriched_tags_idf),
                "source_product_words_tf_idf": 0.0 if not source_product
                else _calc_same_product_tf_idf([w.lower() for w in product.title.split() if w], source_product_words,
                                               words_idf)
            },
            int_features={
                "user_brand_visits_in_last_3": visited_brands_last_3_counter[product.brand],
                "user_brand_visits_in_last_10": visited_brands_last_10_counter[product.brand]
            },
            date=date
        )
        for product in products
    }


def calc_embedding_features(user_history: Optional[UserHistoryCookie],
                            source_product_embedding: Optional[List[float]],
                            products: List[Product], products_map: Dict[str, Product],
                            product_embeddings_func: Callable[[Product], Optional[List[float]]],
                            date: datetime, non_existing_distance: float = 999.9) -> Dict[str, FeatureData]:
    def _distance(v1: List[float], v2: List[float]) -> float:
        return sqrt(sum(map(lambda x1, x2: (x1 - x2) ** 2, v1, v2)))

    def average_embedding(embeddings: List[List[float]]) -> List[float]:
        return list(map(lambda x: x / len(embeddings), map(sum, zip(*embeddings))))

    def user_history_as_embedding(_user_history: UserHistoryCookie, _no_older_than: Optional[timedelta] = None,
                                  max_events: Optional[int] = None) \
            -> Optional[List[float]]:
        _embeddings = []
        for _event in _user_history.events:
            if _no_older_than is None or _event.date > date - _no_older_than:
                if _event.event_type == EventType.PRODUCT_VIEW and _event.product_id in products_map:
                    emb = product_embeddings_func(products_map[_event.product_id])
                    if emb is not None:
                        _embeddings.append(emb)
                        if max_events is not None and len(_embeddings) == max_events:
                            break
        if not _embeddings:
            return None
        return average_embedding(_embeddings)

    user_history_embedding = user_history_as_embedding(user_history)
    user_history_embedding_30m = user_history_as_embedding(user_history, timedelta(minutes=30))
    user_history_embedding_last_3 = user_history_as_embedding(user_history, timedelta(minutes=30), max_events=3)

    distances: Dict[str, float] = dict()
    distances_30m: Dict[str, float] = dict()
    distances_last_3: Dict[str, float] = dict()
    source_product_distances: Dict[str, float] = dict()

    for product in products:
        product_embedding = product_embeddings_func(product)

        if user_history_embedding is not None and product_embedding is not None:
            distances[product.product_id] = _distance(user_history_embedding, product_embedding)

        if user_history_embedding_30m is not None and product_embedding is not None:
            distances_30m[product.product_id] = _distance(user_history_embedding_30m, product_embedding)

        if user_history_embedding_last_3 is not None and product_embedding is not None:
            distances_last_3[product.product_id] = _distance(user_history_embedding_last_3, product_embedding)

        if source_product_embedding is not None and product_embedding is not None:
            source_product_distances[product.product_id] = _distance(source_product_embedding, product_embedding)

    return {
        product.product_id: FeatureData(
            categorical_features={
                "user_product_distance_exists": int(product.product_id in distances),
                "user_product_distance_30m_exists": int(product.product_id in distances_30m),
                "user_product_distance_last_3_exists": int(product.product_id in distances_last_3),
                "source_product_product_distance_exists": int(product.product_id in source_product_distances)
            },
            float_features={
                "user_product_distance": distances.get(product.product_id, non_existing_distance),
                "user_product_distance_30m": distances_30m.get(product.product_id, non_existing_distance),
                "user_product_distance_last_3": distances_last_3.get(product.product_id, non_existing_distance),
                "source_product_product_distance": source_product_distances.get(product.product_id,
                                                                                non_existing_distance)
            },
            int_features=None,
            date=date
        )
        for product in products
    }


def calc_training_feature_data(products: List[Product],
                               product_embeddings_func: Callable[[Product], List[float]],
                               products_map: Dict[str, Product],
                               recommendation_type: RecommendationType,
                               user_id: str, categories_idf: Dict[str, float],
                               tags_idf: Dict[str, float], enriched_tags_idf: Dict[str, float],
                               words_idf: Dict[str, float], source_product: Optional[Product],
                               now_dt: datetime) -> Dict[str, FeatureData]:
    global _user_histories

    user_history_features = calc_user_history_features(products, _user_histories[user_id], products_map, categories_idf,
                                                       tags_idf, enriched_tags_idf, words_idf, source_product, now_dt)
    product_counters_features = calc_training_product_counter_features([product.product_id for product in products],
                                                                       now_dt)
    brand_counters_features = calc_training_brand_counter_features(set(product.brand for product in products), now_dt)

    source_product_embedding = None
    if source_product:
        source_product_embedding = product_embeddings_func(source_product)
    product_embedding_features = calc_embedding_features(_user_histories.get(user_id), source_product_embedding,
                                                         products, products_map, product_embeddings_func, now_dt)

    return {
        product.product_id: user_history_features[product.product_id]
        .update(product_counters_features[product.product_id])
        .update(brand_counters_features[product.brand])
        .update(product_embedding_features[product.product_id])
        .add_categorical("recommendation_type", recommendation_type.value)
        .add_categorical("recommendation_type_ohe_similar",
                         int(recommendation_type == RecommendationType.SIMILAR_PRODUCTS))
        .add_categorical("recommendation_type_ohe_personalized",
                         int(recommendation_type == RecommendationType.PERSONAL_RECOMMENDATIONS))
        .add_categorical("recommendation_type_ohe_most_popular",
                         int(recommendation_type == RecommendationType.MOST_POPULAR))
        .add_categorical("brand", product.brand)
        for product in products
    }


def calc_inference_feature_data(client: str, products: List[Product],
                                product_embeddings_func: Callable[[Product], List[float]],
                                user_history: UserHistoryCookie, products_map: Dict[str, Product],
                                recommendation_type: RecommendationType,
                                categories_idf: Dict[str, float],
                                tags_idf: Dict[str, float], enriched_tags_idf: Dict[str, float],
                                words_idf: Dict[str, float], source_product: Optional[Product],
                                cache_dao: MobiCache)\
        -> Dict[str, FeatureData]:
    date = datetime.utcnow()

    user_history_features = calc_user_history_features(products, user_history, products_map,
                                                       categories_idf, tags_idf, enriched_tags_idf, words_idf,
                                                       source_product, date)
    product_brand_counters_features = calc_inference_product_brand_counter_features(
        client, products, cache_dao, date
    )

    source_product_embedding = None
    if source_product:
        source_product_embedding = product_embeddings_func(source_product)
    product_embedding_features = calc_embedding_features(user_history, source_product_embedding, products, products_map,
                                                         product_embeddings_func, date)

    return {
        product.product_id: user_history_features[product.product_id]
        .update(product_brand_counters_features[product.product_id])
        .update(product_embedding_features[product.product_id])
        .add_categorical("recommendation_type", recommendation_type.value)
        .add_categorical("recommendation_type_ohe_similar",
                         int(recommendation_type == RecommendationType.SIMILAR_PRODUCTS))
        .add_categorical("recommendation_type_ohe_personalized",
                         int(recommendation_type == RecommendationType.PERSONAL_RECOMMENDATIONS))
        .add_categorical("recommendation_type_ohe_most_popular",
                         int(recommendation_type == RecommendationType.MOST_POPULAR))
        .add_categorical("brand", product.brand)
        for product in products
    }


def calc_training_data(
        client: str,
        from_date: datetime,
        event_dao: MobiEventDao,
        recolog_dao: MobiRecologDao,
        products_map: Dict[str, Product],
        product_embeddings_func: Callable[[Product], Optional[List[float]]],
        categories_idf: Dict[str, float],
        tags_idf: Dict[str, float],
        enriched_tags_idf: Dict[str, float],
        words_idf: Dict[str, float]

        ) -> Generator[Tuple[FeatureData, int], None, None]:

    get_prepared_for_training()
    till_date = datetime.utcnow()

    events_generator = event_dao.read_events(client, from_date=from_date - timedelta(days=1, minutes=1),
                                             till_date=till_date, sort_by=[('date', True)])
    attributed_recommendations_generator = recolog_dao.read_attributed_recommendations(
                client, AttributionType.EMPIRICAL, from_date, till_date, sort_by=[('date', True)]
            )

    for labeled_recommendation in generate_training_labeled_attr_recommendations(
            events_generator, attributed_recommendations_generator, from_date, products_map
    ):
        labeled_product_ids = labeled_recommendation.labeled_product_ids
        products = [products_map[lp[0]] for lp in labeled_product_ids]
        recommendation = labeled_recommendation.recommendation
        source_product = products_map.get(recommendation.source_product_id)

        feature_data = calc_training_feature_data(
            products,
            product_embeddings_func,
            products_map,
            recommendation.type,
            recommendation.user_id,
            categories_idf,
            tags_idf,
            enriched_tags_idf,
            words_idf,
            source_product,
            recommendation.date
        )

        for product_id, label in labeled_recommendation.labeled_product_ids:
            yield feature_data[product_id], label


class BinaryClassifierModel(ABC):
    def __init__(self, categorical_feature_names: List[str], int_feature_names: List[str],
                 float_feature_names: List[str]):
        self.categorical_feature_names = sorted(categorical_feature_names)
        self.int_feature_names = sorted(int_feature_names)
        self.float_feature_names = sorted(float_feature_names)
        self._features_len = len(self.categorical_feature_names) + len(self.int_feature_names) \
            + len(self.float_feature_names)

    @abstractmethod
    def train(self, data_and_labels: Iterable[Tuple[FeatureData, int]]):
        pass

    @abstractmethod
    def post_training_log_text(self) -> str:
        pass


class CatboostClassifierModel(BinaryClassifierModel):
    def __init__(self, categorical_feature_names: List[str], int_feature_names: List[str],
                 float_feature_names: List[str], iterations: int = 1000, depth: int = 7):
        super(CatboostClassifierModel, self).__init__(categorical_feature_names, int_feature_names, float_feature_names)
        self.iterations = iterations
        self.depth = depth
        self.model = None
        self.classifier = None
        self._feature_importance = None

    @classmethod
    def create_model(cls, iterations: int = 1000, depth: int = 7) -> catboost.CatBoostClassifier:
        return catboost.CatBoostClassifier(
            iterations=iterations,
            depth=depth,
            verbose=False,
            early_stopping_rounds=iterations // 10,
            loss_function="Logloss",
            custom_metric=['Logloss', 'AUC', 'Accuracy', 'Precision', 'Recall', 'F1'],
            eval_metric='AUC',
            l2_leaf_reg=6.0
        )

    def _feature_data_to_vec(self, feature_data: FeatureData) -> List[Union[float, int, str]]:
        vec: List[Optional[Union[int, float, str]]] = [None] * self._features_len
        pos = 0
        for categorical_feature in self.categorical_feature_names:
            vec[pos] = feature_data.categorical_features[categorical_feature]
            pos += 1
        for int_feature in self.int_feature_names:
            vec[pos] = feature_data.int_features[int_feature]
            pos += 1
        for float_feature in self.float_feature_names:
            vec[pos] = feature_data.float_features[float_feature]
            pos += 1
        return vec

    def _cb_cat_feature_names(self) -> List[str]:
        return ["cat__" + feature_name for feature_name in self.categorical_feature_names]

    def _cb_int_feature_names(self) -> List[str]:
        return ["int__" + feature_name for feature_name in self.int_feature_names]

    def _cb_float_feature_names(self) -> List[str]:
        return ["float__" + feature_name for feature_name in self.float_feature_names]

    def _cb_all_feature_names(self) -> List[str]:
        return self._cb_cat_feature_names() + self._cb_int_feature_names() + self._cb_float_feature_names()

    def _catboost_pool(self, data: Iterable[FeatureData], labels: Iterable[int]) -> catboost.Pool:
        return catboost.Pool(
            data=[self._feature_data_to_vec(feature_data) for feature_data in data],
            label=labels if isinstance(labels, list) else list(labels),
            cat_features=self._cb_cat_feature_names(),
            feature_names=self._cb_all_feature_names()
        )

    def train(self, data_and_labels: Iterable[Tuple[FeatureData, int]]):
        self.model = self.create_model(self.iterations, self.depth)

        positive_data = []
        negative_data = []

        for feature_data, label in data_and_labels:
            feature_vec_and_date = (self._feature_data_to_vec(feature_data), feature_data.date)

            if label:
                positive_data.append(feature_vec_and_date)
            else:
                negative_data.append(feature_vec_and_date)

        get_logger().info(f"Catboost positive examples size: {len(positive_data)}")
        get_logger().info(f"Catboost negative examples size: {len(negative_data)}")

        if len(positive_data) < 100 or len(negative_data) < 100:
            get_logger().warning("We don't have data to train regression model")
            return

        get_logger().info(f"Catboost training data start date: {positive_data[0][1]}")
        get_logger().info(f"Catboost training data end date: {positive_data[-1][1]}")

        split_date = positive_data[int(len(positive_data) * 0.8)][1]

        get_logger().info(f"Catboost split date: {split_date}")

        shuffle(positive_data)
        shuffle(negative_data)

        train_positive_data = []
        train_negative_data = []
        eval_positive_data = []
        eval_negative_data = []

        for feature_vec, date in positive_data:
            if date < split_date:
                train_positive_data.append(feature_vec)
            else:
                eval_positive_data.append(feature_vec)

        for feature_vec, date in negative_data:
            if date < split_date:
                train_negative_data.append(feature_vec)
            else:
                eval_negative_data.append(feature_vec)

        get_logger().info(f"Catboost positive TRAIN examples size: {len(train_positive_data)}")
        get_logger().info(f"Catboost negative TRAIN examples size: {len(train_negative_data)}")

        get_logger().info(f"Catboost positive EVAL examples size: {len(eval_positive_data)}")
        get_logger().info(f"Catboost negative EVAL examples size: {len(eval_negative_data)}")

        if len(train_positive_data) > len(train_negative_data):
            train_positive_data = train_positive_data[0:len(train_negative_data)]
        elif len(train_positive_data) < len(train_negative_data):
            train_negative_data = train_negative_data[0:len(train_positive_data)]

        if len(eval_positive_data) > len(eval_negative_data):
            eval_positive_data = eval_positive_data[0:len(eval_negative_data)]
        elif len(eval_positive_data) < len(eval_negative_data):
            eval_negative_data = eval_negative_data[0:len(eval_positive_data)]

        get_logger().info(f"After adjusting sizes of data:")

        get_logger().info(f"Catboost positive TRAIN examples size: {len(train_positive_data)}")
        get_logger().info(f"Catboost negative TRAIN examples size: {len(train_negative_data)}")

        get_logger().info(f"Catboost positive EVAL examples size: {len(eval_positive_data)}")
        get_logger().info(f"Catboost negative EVAL examples size: {len(eval_negative_data)}")

        train_full_data = list(zip(chain(train_positive_data, train_negative_data),
                                   chain([1] * len(train_positive_data), [0] * len(train_negative_data))))
        shuffle(train_full_data)

        get_logger().info(f"Train FULL data size: {len(train_full_data)}")

        eval_full_data = list(zip(chain(eval_positive_data, eval_negative_data),
                                  chain([1] * len(eval_positive_data), [0] * len(eval_negative_data))))
        shuffle(eval_full_data)

        get_logger().info(f"Eval FULL data size: {len(eval_full_data)}")

        train_data = []
        train_labels = []
        eval_data = []
        eval_labels = []

        for feature_vec, label in train_full_data:
            train_data.append(feature_vec)
            train_labels.append(label)

        for feature_vec, label in eval_full_data:
            eval_data.append(feature_vec)
            eval_labels.append(label)

        training_pool = catboost.Pool(
            data=train_data,
            label=train_labels,
            cat_features=self._cb_cat_feature_names(),
            feature_names=self._cb_all_feature_names()
        )

        eval_pool = catboost.Pool(
            data=eval_data,
            label=eval_labels,
            cat_features=self._cb_cat_feature_names(),
            feature_names=self._cb_all_feature_names()
        )

        self.classifier = self.model.fit(
            training_pool,
            eval_set=eval_pool,
            plot=False
        )

        get_logger().info(f"Catboost best iteration: {str(self.classifier.best_iteration_)}")
        get_logger().info(f"Catboost best score: {str(self.classifier.best_score_)}")

        self._feature_importance = self.classifier.get_feature_importance(training_pool)

    def predict_one(self, feature_data: FeatureData) -> float:
        return self.classifier.predict(self._feature_data_to_vec(feature_data), prediction_type='Probability')[1]

    def predict_many(self, feature_data_list: List[FeatureData]) -> Optional[List[float]]:
        if self.classifier is None:
            return None

        return self.classifier.predict(
            [self._feature_data_to_vec(feature_data) for feature_data in feature_data_list],
            prediction_type='Probability'
        )[:, 1].tolist()

    def post_training_log_text(self) -> str:
        if self._feature_importance is not None:
            text = "FEATURES IMPORTANCE:\n"
            text += "\n".join(map(lambda x: f"{x[0]}: {x[1]}",
                                  sorted(zip(self._cb_all_feature_names(), self._feature_importance),
                                         key=lambda x: x[1], reverse=True)))
        else:
            text = "WARNING: Feature importances are missing"
        return text
