import keras
import numpy as np

from bisect import bisect
from collections import Counter, defaultdict
from datetime import datetime, timedelta
from itertools import accumulate, permutations
from random import choices, random
from typing import Any, Counter as CounterType, Dict, Generator, Hashable, Iterable, List, Optional, Tuple, Union

from mobi.core.logging import get_logger
from mobi.core.models import Event, EventType, Product
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.recolog import MobiRecologDao
from mobi.ml.common import is_train_user
from mobi.ml.common.keras import AverageLayer
from mobi.ml.model.embedding.interface import EmbeddingModel
from mobi.ml.model.embedding.impl.factories.catboost import CatboostClassifierModel, calc_training_data
from mobi.ml.model.embedding.impl.static import AbstractStaticEmbeddingModelFactory
from mobi.ml.utils.vocabs import Vocabulary

_cb_categorical_features = [
    "brand",
    "recommendation_type",
    "recommendation_type_ohe_similar",
    "recommendation_type_ohe_personalized",
    "recommendation_type_ohe_most_popular",

    "user_product_distance_exists",
    "user_product_distance_30m_exists",
    "user_product_distance_last_3_exists",
    "source_product_product_distance_exists",

    "product_added_to_wishlist_in_last_1_day",
    "product_added_to_basket_in_last_1_day",
    "product_added_to_wishlist_in_history",
    "product_added_to_basket_in_history",
    "brand_added_to_wishlist_in_last_1_day",
    "brand_added_to_basket_in_last_1_day",
    "brand_added_to_wishlist_in_history",
    "brand_added_to_basket_in_history",
    "is_source_product_exists",
    "is_source_brand_same"
]

_cb_int_features = [
    "user_brand_visits_in_last_3",
    "user_brand_visits_in_last_10",
]

_cb_float_features = [
    "product_views_1h",
    "product_views_24h",
    "product_to_basket_1h",
    "product_to_basket_24h",
    "product_to_wishlist_1h",
    "product_to_wishlist_24h",

    "brand_views_1h",
    "brand_views_24h",
    "brand_to_basket_1h",
    "brand_to_basket_24h",
    "brand_to_wishlist_1h",
    "brand_to_wishlist_24h",

    "product_views_spike",
    "brand_views_spike",

    "user_brand_visits_in_last_1_day",
    "user_brand_visits_in_history",

    "user_product_visits_in_last_30_min",
    "user_product_visits_in_last_1_day",
    "user_product_visits_in_history",

    "user_product_distance",
    "user_product_distance_30m",
    "user_product_distance_last_3",
    "source_product_product_distance",

    "categories_tf_idf",
    "tags_tf_idf",
    "enriched_tags_tf_idf",
    "words_tf_idf",

    "source_product_categories_tf_idf",
    "source_product_tags_tf_idf",
    "source_product_enriched_tags_tf_idf",
    "source_product_words_tf_idf"
]


class DeepwalkStaticEmbeddingModelFactory(AbstractStaticEmbeddingModelFactory):
    def __init__(self, catalog_dao: MobiCatalogDao, event_dao: MobiEventDao, recolog_dao: MobiRecologDao,
                 from_date: datetime, till_date: datetime, dimensions: int = 10):
        super().__init__()
        self.catalog_dao = catalog_dao
        self.event_dao = event_dao
        self.recolog_dao = recolog_dao
        self.from_date = from_date
        self.till_date = till_date
        self.dimensions = dimensions

    def calc_basket_links(self, client: str, model: EmbeddingModel) -> Dict[str, CounterType[str]]:
        logger = get_logger()
        logger.info(f"Calculating basket links for model \"{model.model_id}\" for client {client}...")
        return build_basket_links(
            self.event_dao,
            client,
            self.from_date,
            self.till_date,
            max_time_delta=timedelta(minutes=30),
            max_basket_size=10
        )

    def calc_idf_data(self, client: str, model: EmbeddingModel) \
            -> Tuple[Dict[str, float], Dict[str, float], Dict[str, float], Dict[str, float]]:
        return model.calc_idf_data(list(self.catalog_dao.read_all_products(client, include_inactive=True,
                                                                           include_not_in_stock=True)))

    def calc_catboost_classifier(self, client: str, model: EmbeddingModel) -> CatboostClassifierModel:
        logger = get_logger()
        logger.info(f"Calculating catboost classifier for model \"{model.model_id}\" for client {client}...")
        catboost_classifier = CatboostClassifierModel(
            categorical_feature_names=_cb_categorical_features,
            int_feature_names=_cb_int_features,
            float_feature_names=_cb_float_features
        )
        training_data_generator = calc_training_data(
            client,
            self.from_date,
            self.event_dao,
            self.recolog_dao,
            {p.product_id: p for p in self.catalog_dao.read_all_products(client, include_inactive=True,
                                                                         include_not_in_stock=True)},
            model.product_to_embedding,
            model.categories_idf,
            model.tags_idf,
            model.enriched_tags_idf,
            model.words_idf
        )
        catboost_classifier.train(training_data_generator)
        logger.info(catboost_classifier.post_training_log_text())
        return catboost_classifier

    def calc_product_embeddings(self, client: str, model: EmbeddingModel) -> List[Tuple[str, List[float]]]:
        logger = get_logger()
        logger.info(f"Training \"{model.model_id}\" model of type \"{model.model_type}\" for client {client}...")
        return train_keras_embeddings(
            client=client,
            catalog_dao=self.catalog_dao,
            event_dao=self.event_dao,
            from_date=self.from_date,
            till_date=self.till_date,
            dimensions=self.dimensions
        )


def train_keras_embeddings(client: str, catalog_dao: MobiCatalogDao, event_dao: MobiEventDao,
                           from_date: datetime, till_date: datetime, dimensions: int = 10) \
        -> List[Tuple[str, List[float]]]:
    vocab_id = Vocabulary("product_id", null_is_zero=False, build_inverted=True)
    vocab_brand = Vocabulary("product_brand", null_is_zero=False)
    vocab_category = Vocabulary("product_category", null_is_zero=True)
    vocab_color = Vocabulary("product_color", null_is_zero=True)
    vocab_tags = Vocabulary("product_tags", null_is_zero=True)
    vocab_enriched_tags = Vocabulary("product_enriched_tags", null_is_zero=True)

    products_map: Dict[str, Product] = {product.product_id: product
                                        for product in catalog_dao.read_all_products(client)}

    for product in products_map.values():
        vocab_id.add(product.product_id)
        if product.brand is not None:
            vocab_brand.add(product.brand)
        if product.categories is not None:
            vocab_category.add_multiple(product.categories)
        if product.color is not None:
            vocab_color.add(product.color)
        if product.tags is not None:
            vocab_tags.add_multiple(product.tags)
        if product.enriched_tags is not None:
            vocab_enriched_tags.add_multiple(product.enriched_tags)

    vocab_id.stop_training()
    vocab_brand.stop_training()
    vocab_category.stop_training()
    vocab_color.stop_training()
    vocab_tags.stop_training()
    vocab_enriched_tags.stop_training()

    keras_model = build_keras_model(
        len(vocab_id),
        len(vocab_brand),
        len(vocab_category),
        len(vocab_color),
        len(vocab_tags),
        len(vocab_enriched_tags),
        dimensions
    )

    train_graph, val_graph = build_train_val_graphs(event_dao, client, from_date, till_date, timedelta(minutes=30))

    train_data = [[] for _ in range(12)]
    valid_data = [[] for _ in range(12)]
    train_labels = []
    valid_labels = []

    max_categories = 10
    max_tags = 10
    max_enriched_tags = 10

    def add_products_to_dataset(dataset: List[List], p1: Product, p2: Product):
        def _build_vector_feature(value: Optional[List[Any]], vocab: Vocabulary, max_len: int):
            if value is None:
                return [0] * max_len
            _result = vocab.get_multiple(value)[0:max_len]
            if len(_result) < max_len:
                _result = _result + ([0] * (max_len - len(_result)))
            return _result

        dataset[0].append(vocab_id[p1.product_id])
        dataset[1].append(vocab_brand[p1.brand])
        dataset[2].append(_build_vector_feature(p1.categories, vocab_category, max_categories))
        dataset[3].append(vocab_color[p1.color])
        dataset[4].append(_build_vector_feature(p1.tags, vocab_tags, max_tags))
        dataset[5].append(_build_vector_feature(p1.enriched_tags, vocab_enriched_tags, max_enriched_tags))

        dataset[6].append(vocab_id[p2.product_id])
        dataset[7].append(vocab_brand[p2.brand])
        dataset[8].append(_build_vector_feature(p2.categories, vocab_category, max_categories))
        dataset[9].append(vocab_color[p2.color])
        dataset[10].append(_build_vector_feature(p2.tags, vocab_tags, max_tags))
        dataset[11].append(_build_vector_feature(p2.enriched_tags, vocab_enriched_tags, max_enriched_tags))

    train_pos = 0
    train_neg = 0

    logger = get_logger()

    cycles = max(10, int(train_graph.edges / (10 * len(train_graph.verticies))))
    logger.info(f"CYCLES INFORMATION: Edges num: {train_graph.edges}. Verticies num: "
                f"{len(train_graph.verticies)}. Cycles num: {cycles}")

    for v1, v2 in generate_pairs(train_graph, min_chain_len=3, max_chain_len=7,
                                 cycles=min(max(1, cycles), 10)):
        if v1 not in vocab_id or v2 not in vocab_id:
            continue

        if v1 not in products_map or v2 not in products_map:
            continue

        p1: Product = products_map[str(v1)]
        p2: Product = products_map[str(v2)]

        add_products_to_dataset(train_data, p1, p2)
        train_labels.append(1)
        train_pos += 1

    non_existing_edges = train_graph.random_non_existing_edges(k=train_pos)

    if non_existing_edges:
        for n_p1, n_p2 in non_existing_edges:
            if n_p1 in products_map and n_p2 in products_map:
                n_p1_product: Product = products_map[n_p1]
                n_p2_product: Product = products_map[n_p2]

                add_products_to_dataset(train_data, n_p1_product, n_p2_product)
                train_labels.append(0)
                train_neg += 1

    logger.info(f"Number of train examples: {len(train_labels)}")
    logger.info(f"Positives: {train_pos}")
    logger.info(f"Negatives: {train_neg}")
    assert len(train_labels)

    valid_pos = 0
    valid_neg = 0

    for v1, v2 in generate_pairs(val_graph, min_chain_len=3, max_chain_len=7,
                                 cycles=min(max(1, cycles), 10)):
        if v1 not in vocab_id or v2 not in vocab_id:
            continue

        if v1 not in products_map or v2 not in products_map:
            continue

        p1: Product = products_map[str(v1)]
        p2: Product = products_map[str(v2)]

        add_products_to_dataset(valid_data, p1, p2)
        valid_labels.append(1)
        valid_pos += 1

    non_existing_edges = val_graph.random_non_existing_edges(k=valid_pos)

    if non_existing_edges:
        for n_p1, n_p2 in non_existing_edges:
            if n_p1 in products_map and n_p2 in products_map:
                n_p1_product: Product = products_map[n_p1]
                n_p2_product: Product = products_map[n_p2]

                add_products_to_dataset(valid_data, n_p1_product, n_p2_product)
                valid_labels.append(0)
                valid_neg += 1

    logger.info(f"Number of validation examples: {len(valid_labels)}")
    logger.info(f"Positives: {valid_pos}")
    logger.info(f"Negatives: {valid_neg}")
    assert len(valid_labels)

    train_data = [np.vstack(a) for a in train_data]
    valid_data = [np.vstack(a) for a in valid_data]

    callbacks = [
        keras.callbacks.EarlyStopping("val_loss", min_delta=0.0001, patience=5)
    ]
    keras_model.fit(train_data, np.vstack(train_labels), epochs=20, batch_size=32,
                    validation_data=(valid_data, np.vstack(valid_labels)), shuffle=True,
                    callbacks=callbacks, verbose=2)

    products = list(products_map.values())

    w1 = keras_model.get_layer("product_embedding_layer").get_weights()[0]
    w2 = keras_model.get_layer("brand_embedding_layer").get_weights()[0]
    w3 = keras_model.get_layer("category_embedding_layer").get_weights()[0]
    w4 = keras_model.get_layer("color_embedding_layer").get_weights()[0]
    w5 = keras_model.get_layer("tags_embedding_layer").get_weights()[0]
    w6 = keras_model.get_layer("enriched_tags_embedding_layer").get_weights()[0]
    result = []

    def _get_vector_feature_value(layer_weights, product_value, vocab: Vocabulary, max_len: int):
        if not product_value:
            return [0.0] * max_len
        return (sum(layer_weights[vocab[c]] for c in product_value) / len(product_value)).tolist()

    for i, product in enumerate(products):
        e1 = w1[vocab_id[product.product_id]].tolist()
        e2 = w2[vocab_brand[product.brand]].tolist()
        e3 = _get_vector_feature_value(w3, product.categories, vocab_category, max_categories)
        e4 = w4[vocab_color[product.color]].tolist()
        e5 = _get_vector_feature_value(w5, product.tags, vocab_tags, max_tags)
        e6 = _get_vector_feature_value(w6, product.enriched_tags, vocab_enriched_tags, max_enriched_tags)

        result.append((product.product_id, e1 + e2 + e3 + e4 + e5 + e6))

    return result


def build_basket_links(event_dao: MobiEventDao, client: str, from_date: datetime, till_date: datetime,
                       max_time_delta: timedelta = timedelta(minutes=10), max_basket_size: int = 10):
    basket_links: Dict[str, CounterType[str]] = dict()

    for session in event_dao.read_user_sessions(client, max_time_delta, from_date, till_date):
        basket = [event.product_id for event in session if event.event_type == EventType.PRODUCT_TO_BASKET]
        if len(basket) <= 1 or len(basket) > max_basket_size:
            continue

        for product_a_id, product_b_id in permutations(basket, r=2):
            if product_a_id not in basket_links:
                basket_links[product_a_id] = Counter()

            basket_links[product_a_id][product_b_id] += 1

    return basket_links


def event_pairs(event_dao: MobiEventDao, client: str, from_date: datetime, till_date: datetime,
                max_timedelta_within_session: timedelta) \
        -> Generator[Tuple[Event, Event], None, None]:
    for session in event_dao.read_user_sessions(client, max_timedelta_within_session, from_date, till_date):
        for e1, e2 in zip(session, session[1:]):
            yield e1, e2


def train_val_event_pairs(event_dao: MobiEventDao, client: str, from_date: datetime, till_date: datetime,
                          max_timedelta_within_session: timedelta, train_population: float = 0.8) \
        -> Generator[Tuple[Event, Event, bool], None, None]:
    for event1, event2 in event_pairs(event_dao, client, from_date, till_date, max_timedelta_within_session):
        yield event1, event2, is_train_user(event1.user_id, train_population)


def build_train_val_graphs(event_dao: MobiEventDao, client: str, from_date: datetime,
                           till_date: datetime, max_timedelta_within_session: timedelta,
                           train_population: float = 0.8) -> Tuple['Graph', 'Graph']:
    train_graph = Graph()
    val_graph = Graph()

    for event1, event2, is_train in train_val_event_pairs(event_dao, client, from_date, till_date,
                                                          max_timedelta_within_session, train_population):
        if is_train:
            train_graph.add_edge(event1.product_id, event2.product_id)
        else:
            val_graph.add_edge(event1.product_id, event2.product_id)

    return train_graph, val_graph


def generate_random_paths(graph: 'Graph', min_len: int, max_len: int) \
        -> Generator[List[int], None, None]:
    for v in graph.verticies:
        path = graph.random_path(v, max_hops=max_len)
        if len(path) >= min_len:
            yield path


def generate_pairs(graph: 'Graph', min_chain_len: int = 3, max_chain_len: int = 7,
                   cycles: int = 1) -> Generator[Tuple[Hashable, Hashable], None, None]:
    for _ in range(cycles):
        for path in generate_random_paths(graph, min_chain_len, max_chain_len):
            for pair in zip(path, path[1:]):
                yield pair


class Graph:
    def __init__(self):
        self.verticies = defaultdict(set)
        self.weights = dict()

        # Inverted weights defines probability of a specific vertex to be chosen as
        # "v from" vertex for the random-non-existing edge
        self.v_from_inv_weights = []

        self.min_weight = None
        self.max_weight = None

        self.lists_are_ready = False
        self.l_all_verticies = []
        self.l_verticies: Dict[Hashable, List[Hashable]] = defaultdict(list)
        self.l_cum_weights: Dict[Hashable, List[float]] = defaultdict(list)

        self.edges = 0
        self.total_weight = 0.0

    def add_verticies(self, vv: Iterable[Hashable]):
        if vv:
            self.lists_are_ready = False
            for v in vv:
                if v not in self.verticies:
                    self.verticies[v] = set()

    def add_edge(self, v_from: Hashable, v_to: Hashable, weight: Union[float, int] = 1):
        if v_from != v_to:
            self.lists_are_ready = False

            self.verticies[v_from].add(v_to)
            if v_to not in self.verticies:
                self.verticies[v_to] = set()

            if (v_from, v_to) not in self.weights:
                self.weights[(v_from, v_to)] = 0.0

            self.weights[(v_from, v_to)] += weight
            self.total_weight += weight
            self.edges += 1

    def _build_lists(self):
        for v in self.verticies:
            self.l_verticies[v] = list(self.verticies[v])
            self.l_cum_weights[v] = \
                list(accumulate(
                    map(lambda _v_to: 0 if (v, _v_to) not in self.weights else self.weights[(v, _v_to)],
                        self.l_verticies[v])
                ))

        self.max_weight = -1
        self.min_weight = 10 ** 10

        for v1 in self.verticies:
            for v2 in self.l_verticies:
                if v1 != v2:
                    if (v1, v2) in self.weights:
                        w = self.weights[(v1, v2)]
                    else:
                        w = 0.0
                    if self.max_weight < w:
                        self.max_weight = w
                    if self.min_weight > w:
                        self.min_weight = w

        if self.max_weight < self.min_weight:
            raise ValueError("Can not find proper max and min values")

        mami_dist = max(1.0, float(self.max_weight - self.min_weight))

        _v_from_weights = []

        self.l_all_verticies = list(self.verticies)

        for v in self.l_all_verticies:
            inv_weight = 0.0
            for v_to in self.l_verticies[v]:
                inv_weight += ((self.max_weight - self.weights[(v, v_to)]) / mami_dist) ** 4

            _to_add = len(self.verticies) - len(self.l_verticies[v])
            if v not in self.verticies[v]:
                _to_add -= 1
            _to_add *= (self.max_weight / mami_dist) ** 4
            _v_from_weights.append(inv_weight + _to_add)

        self.v_from_inv_weights = list(accumulate(_v_from_weights))
        del _v_from_weights

        self.lists_are_ready = True

        # for i, pair in enumerate(self.l_i_edges):
        #     print(f"PAIR: {pair}, original weights: {unnormalized_weights[i]}, "
        #           f"inv weight: {self.l_i_weights[i]}")

    def random_vertex(self, v: Hashable) -> Optional[Hashable]:
        if not self.lists_are_ready:
            self._build_lists()

        if not self.l_verticies[v]:
            return None

        if len(self.l_verticies[v]) == 1:
            return self.l_verticies[v][0]

        return self.l_verticies[v][
            bisect(
                self.l_cum_weights[v],
                random() * self.l_cum_weights[v][-1],
                0,
                len(self.l_cum_weights[v]) - 1
            )]

    def random_path(self, v: Hashable, max_hops: int = 5) -> List[Hashable]:
        path = [v]
        while len(path) <= max_hops:
            next_v = self.random_vertex(path[-1])
            if next_v is None:
                break

            path.append(next_v)

        return path

    def random_non_existing_edges(self, k=1) -> Optional[List[Tuple[str, str]]]:
        if not self.lists_are_ready:
            self._build_lists()

        if len(self.verticies) < 2:
            raise ValueError("Too few verticies in the graph")

        vv_from = Counter()

        for v in choices(self.l_all_verticies, cum_weights=self.v_from_inv_weights, k=k):
            vv_from[v] += 1

        inv_weights = [0.0 for _ in range(len(self.l_all_verticies))]
        cum_weights = [0.0 for _ in range(len(self.l_all_verticies))]

        mami_dist = max(1.0, float(self.max_weight - self.min_weight))
        zero_inv_weight = (self.max_weight / mami_dist) ** 4

        for v_from, num in vv_from.items():
            for pos, v_to in enumerate(self.l_all_verticies):
                if v_from == v_to:
                    inv_weights[pos] = 0.0
                elif v_to not in self.verticies[v_from]:
                    inv_weights[pos] = zero_inv_weight
                else:
                    inv_weights[pos] = ((self.max_weight - self.weights[(v_from, v_to)]) / mami_dist) ** 4

            for pos, s in enumerate(accumulate(inv_weights)):
                cum_weights[pos] = s

            for v_to in choices(self.l_all_verticies, cum_weights=cum_weights, k=num):
                yield v_from, v_to


def build_keras_model(
        products_num: int,
        brands_vocab_size: int,
        categories_vocab_size: int,
        colors_vocab_size: int,
        tags_vocab_size: int,
        enriched_tags_vocab_size: int,
        dimensions: int
        ):
    def _build_product_input(vertical_id: int):
        product_id_input = keras.Input((1,), name=f"product_id_{vertical_id}_input")
        brand_id_input = keras.Input((1,), name=f"brand_id_{vertical_id}_input")
        category_ids_input = keras.Input((10,), name=f"category_ids_{vertical_id}_input")
        color_id_input = keras.Input((1,), name=f"color_id_{vertical_id}_input")
        tags_ids_input = keras.Input((10,), name=f"tags_ids_{vertical_id}_input")
        enriched_ids_input = keras.Input((10,), name=f"enriched_tags_ids_{vertical_id}_input")
        """
        is_new_input = keras.Input((1,), name=f"is_new_{vertical_id}_input")
        is_exclusive_input = keras.Input((1,), name=f"is_exclusive_{vertical_id}_input")
        """
        return product_id_input, brand_id_input, category_ids_input, color_id_input, tags_ids_input, enriched_ids_input

    product_embedding_layer = keras.layers.Embedding(products_num, dimensions,
                                                     name=f"product_embedding_layer")
    brand_embedding_layer = keras.layers.Embedding(brands_vocab_size, dimensions,
                                                   name=f"brand_embedding_layer")

    category_embedding_layer = keras.layers.Embedding(categories_vocab_size, dimensions,
                                                      name=f"category_embedding_layer",
                                                      mask_zero=True, input_length=10)

    color_embedding_layer = keras.layers.Embedding(colors_vocab_size, dimensions,
                                                   name=f"color_embedding_layer")

    tags_embedding_layer = keras.layers.Embedding(tags_vocab_size, dimensions,
                                                  name=f"tags_embedding_layer",
                                                  mask_zero=True, input_length=10)

    enriched_tags_embedding_layer = keras.layers.Embedding(enriched_tags_vocab_size, dimensions,
                                                           name=f"enriched_tags_embedding_layer",
                                                           mask_zero=True, input_length=10)
    """
    is_new_embedding_layer = keras.layers.Embedding(2, dimensions, name="is_new_embedding_layer")

    is_exclusive_embedding_layer = keras.layers.Embedding(2, dimensions,
                                                          name="is_exclusive_embedding_layer")
    """

    product_id_1_input, brand_id_1_input, category_ids_1_input, color_id_1_input, tags_ids_1_input, \
        enriched_tags_ids_1_input = _build_product_input(1)

    product_id_2_input, brand_id_2_input, category_ids_2_input, color_id_2_input, tags_ids_2_input, \
        enriched_tags_ids_2_input = _build_product_input(2)

    product_1_embedding = product_embedding_layer(product_id_1_input)
    product_1_embedding = keras.layers.Reshape(target_shape=(dimensions, ))(product_1_embedding)
    brand_1_embedding = brand_embedding_layer(brand_id_1_input)
    brand_1_embedding = keras.layers.Reshape(target_shape=(dimensions, ))(brand_1_embedding)
    category_1_embedding = category_embedding_layer(category_ids_1_input)
    category_1_embedding = AverageLayer()(category_1_embedding)
    color_1_embedding = color_embedding_layer(color_id_1_input)
    color_1_embedding = keras.layers.Reshape(target_shape=(dimensions, ))(color_1_embedding)
    tags_1_embedding = tags_embedding_layer(tags_ids_1_input)
    tags_1_embedding = AverageLayer()(tags_1_embedding)
    enriched_tags_1_embedding = enriched_tags_embedding_layer(enriched_tags_ids_1_input)
    enriched_tags_1_embedding = AverageLayer()(enriched_tags_1_embedding)
    """
    is_new_1_embedding = is_new_embedding_layer(is_new_1_input)
    is_new_1_embedding = keras.layers.Reshape(target_shape=(dimensions, ))(is_new_1_embedding)
    is_exclusive_1_embedding = is_exclusive_embedding_layer(is_exclusive_1_input)
    is_exclusive_1_embedding = keras.layers.Reshape(target_shape=(dimensions, ))(is_exclusive_1_embedding)
    """

    product_2_embedding = product_embedding_layer(product_id_2_input)
    product_2_embedding = keras.layers.Reshape(target_shape=(dimensions, ))(product_2_embedding)
    brand_2_embedding = product_embedding_layer(brand_id_2_input)
    brand_2_embedding = keras.layers.Reshape(target_shape=(dimensions, ))(brand_2_embedding)
    category_2_embedding = product_embedding_layer(category_ids_2_input)
    category_2_embedding = AverageLayer()(category_2_embedding)
    color_2_embedding = product_embedding_layer(color_id_2_input)
    color_2_embedding = keras.layers.Reshape(target_shape=(dimensions, ))(color_2_embedding)
    tags_2_embedding = tags_embedding_layer(tags_ids_2_input)
    tags_2_embedding = AverageLayer()(tags_2_embedding)
    enriched_tags_2_embedding = enriched_tags_embedding_layer(enriched_tags_ids_2_input)
    enriched_tags_2_embedding = AverageLayer()(enriched_tags_2_embedding)

    product_1_vector = keras.layers.Concatenate(name=f"final_vector_1_layer")([
        product_1_embedding,
        brand_1_embedding,
        category_1_embedding,
        color_1_embedding,
        tags_1_embedding,
        enriched_tags_1_embedding
    ])

    product_2_vector = keras.layers.Concatenate(name=f"final_vector_2_layer")([
        product_2_embedding,
        brand_2_embedding,
        category_2_embedding,
        color_2_embedding,
        tags_2_embedding,
        enriched_tags_2_embedding
    ])

    output = keras.layers.Dot(axes=1)([product_1_vector, product_2_vector])
    output = keras.layers.Activation('sigmoid')(output)

    inputs = [product_id_1_input, brand_id_1_input, category_ids_1_input, color_id_1_input, tags_ids_1_input,
              enriched_tags_ids_1_input,
              product_id_2_input, brand_id_2_input, category_ids_2_input, color_id_2_input, tags_ids_2_input,
              enriched_tags_ids_2_input]

    model = keras.models.Model(inputs=inputs, outputs=[output])
    model.compile(optimizer=keras.optimizers.Adam(lr=0.001), loss=keras.losses.MAE)

    return model
