from abc import ABC, abstractmethod
from datetime import datetime
from itertools import chain, product
from typing import Any, Dict, Iterable, List, Optional

from mobi.core.models import Event, ModelStatus, Product
from mobi.dao.cache import UserHistoryCookie
from mobi.ml.common import average_embedding, embeddings_dist
from mobi.ml.common.knn_index import BallTreeKnnIndex, KnnMultiParametrizedIndex, KnnParametrizedIndex
from mobi.ml.model.interface import MobiModel, MobiModelException, MobiModelFactory


class EmbeddingModel(MobiModel, ABC):
    def __init__(self, model_id: str = None, date: datetime = None, status: ModelStatus = ModelStatus.NOT_READY):
        super().__init__(model_id, date, status)
        self.knn_tree: Optional[BallTreeKnnIndex] = None
        self.brand_isolated_knn_tree: Optional[KnnParametrizedIndex] = None
        self.category_isolated_knn_tree: Optional[KnnParametrizedIndex] = None
        self.brand_category_isolated_knn_tree: Optional[KnnMultiParametrizedIndex] = None

    def model_specific_meta_info(self) -> Dict[str, Any]:
        return {
            "knn_tree_size": None if self.knn_tree is None else len(self.knn_tree),
            "brand_isolated_knn_tree_size": None if self.brand_isolated_knn_tree is None
            else len(self.brand_isolated_knn_tree),
            "category_isolated_knn_tree": None if self.category_isolated_knn_tree is None
            else len(self.category_isolated_knn_tree),
            "brand_category_isolated_knn_tree": None if self.brand_category_isolated_knn_tree is None
            else len(self.brand_category_isolated_knn_tree),
            **super(EmbeddingModel, self).model_specific_meta_info()
        }

    def _dump_binary_as_dict(self) -> Dict[str, Any]:
        return {
            **super()._dump_binary_as_dict()
        }

    def _process_dump_binary_as_dict(self, dump: Dict[str, Any]):
        super()._process_dump_binary_as_dict(dump)

    @abstractmethod
    def product_to_embedding(self, product: Product) -> Optional[List[float]]:
        pass

    def events_history_to_embedding(self, events_history: List[Event]) \
            -> Optional[List[float]]:
        products = map(self.products_map.get, map(lambda event: event.product_id, events_history))
        products = filter(lambda x: x is not None, products)
        embeddings = list(filter(lambda x: x is not None, map(self.product_to_embedding, products)))
        if not embeddings:
            return None
        return average_embedding(embeddings)

    def user_history_to_embedding(self, user_history: Optional[UserHistoryCookie]) -> Optional[List[float]]:
        if user_history is None:
            return None
        return self.events_history_to_embedding(user_history.events)

    def assert_ready(self):
        if self.status != ModelStatus.READY:
            raise MobiModelException(f"The model \"{self.model_id}\" is not ready. "
                                     f"Current status is: \"{self.status.name}\"")

    def _build_knn_trees(self, products: Iterable[Product]):
        self.knn_tree = BallTreeKnnIndex(
            [(product.product_id, self.product_to_embedding(product)) for product in products]
        )
        self.brand_isolated_knn_tree = KnnParametrizedIndex(
            [(product.product_id, product.brand, self.product_to_embedding(product)) for product in products],
            index_cls=BallTreeKnnIndex
        )
        self.category_isolated_knn_tree = KnnParametrizedIndex(
            [(product.product_id, category, self.product_to_embedding(product))
             for product in products if product.categories for category in product.categories],
            index_cls=BallTreeKnnIndex
        )
        self.brand_category_isolated_knn_tree = KnnMultiParametrizedIndex(
            [(product.product_id, [product.brand, category], self.product_to_embedding(product))
             for product in products if product.categories for category in product.categories],
            index_cls=BallTreeKnnIndex, parameters_num=2
        )

    def compile(self, products: List[Product]):
        super().compile(products)
        products_to_index = [
            product for product in products
            if self.product_to_embedding(product) is not None
               and product.product_id not in self.non_recommendable_product_ids
               and product.brand not in self.non_recommendable_brands
        ]
        self._build_knn_trees(products_to_index)

    def _find_in_index(self, embedding: List[float], brands: Optional[List[str]] = None,
                       categories: Optional[List[str]] = None, skip_first: bool = False, k: int = 10) -> List[str]:
        to_skip = int(bool(skip_first))

        if brands and categories:
            k = min(len(self.brand_category_isolated_knn_tree), k + to_skip)
            results = chain(
                *map(lambda brand, category: self.brand_category_isolated_knn_tree.query(
                    embedding, [brand, category], k
                ), *zip(*product(set(brands), set(categories))))
            )
            results = sorted(results, key=lambda x: x[0])
            product_ids = []
            seen = set()
            for _, product_id in results:
                if product_id not in seen:
                    product_ids.append(str(product_id))
                    seen.add(product_id)
            return product_ids[to_skip:to_skip+k]

        if brands:
            k = min(len(self.brand_isolated_knn_tree), k + to_skip)
            results = chain(
                *map(lambda brand: self.brand_isolated_knn_tree.query(embedding, brand, k), brands)
            )
            return [str(product_id) for _, product_id in sorted(results, key=lambda x: x[0])][to_skip:to_skip+k]

        if categories:
            k = min(len(self.category_isolated_knn_tree), k + to_skip)
            results = chain(
                *map(lambda category: self.category_isolated_knn_tree.query(embedding, category, k), categories)
            )
            results = sorted(results, key=lambda x: x[0])
            product_ids = []
            seen = set()
            for _, product_id in results:
                if product_id not in seen:
                    product_ids.append(str(product_id))
                    seen.add(product_id)
            return product_ids[to_skip:to_skip+k]

        k = min(len(self.knn_tree), k + to_skip)
        return [str(product_id) for _, product_id in self.knn_tree.query(embedding, k)[to_skip:]]

    def similar_products(self, product: Product, brands: Optional[List[str]] = None,
                         categories: Optional[List[str]] = None, k: int = 10) -> List[str]:
        self.assert_ready()
        product_embedding = self.product_to_embedding(product)
        if product_embedding is None:
            return []
        return self._find_in_index(product_embedding, brands=brands, categories=categories, skip_first=True, k=k)

    def personal_recommendations(self, user_session: List[Event], brands: List[str] = None,
                                 categories: List[str] = None, k: int = 10) -> List[str]:
        self.assert_ready()
        session_embedding = self.events_history_to_embedding(user_session)
        if session_embedding is None:
            return []
        return self._find_in_index(session_embedding, brands=brands, categories=categories, skip_first=False, k=k)

    @abstractmethod
    def basket_recommendations(self, product_ids: Iterable[str], num: int = 10) -> List[str]:
        pass

    def _rank_products_by_embedding(self, products: List[Product], embedding: List[float]) -> List[float]:
        product_embeddings = map(self.product_to_embedding, products)
        distances = [None if product_embedding is None else embeddings_dist(embedding, product_embedding)
                     for product_embedding in product_embeddings]
        try:
            max_dist = max(filter(lambda x: x is not None, distances))
        except ValueError:
            max_dist = 0.0

        return [0.0 if distance is None else max_dist - distance for distance in distances]

    def rank_products_by_user_history(self, products: List[Product], user_history: UserHistoryCookie) \
            -> Optional[List[float]]:
        self.assert_ready()
        user_embedding = self.user_history_to_embedding(user_history)
        if user_embedding is None:
            return None
        return self._rank_products_by_embedding(products, user_embedding)

    def rank_products_by_source_product(self, products: List[Product], source_product: Product) \
            -> Optional[List[float]]:
        self.assert_ready()
        source_product_embedding = self.product_to_embedding(source_product)
        if source_product_embedding is None:
            return None
        return self._rank_products_by_embedding(products, source_product_embedding)


class EmbeddingModelFactory(MobiModelFactory, ABC):
    pass
