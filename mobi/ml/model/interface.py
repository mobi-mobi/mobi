import math
import pickle

from abc import ABC, abstractmethod
from collections import Counter
from datetime import datetime
from typing import Any, Dict, Iterable, List, Optional, Set, Tuple

from mobi.core.models import Event, ModelDump, ModelDumpInfo, ModelStatus, Product, RecommendationType
from mobi.dao.cache import MobiCache, UserHistoryCookie
from mobi.ml.common import generate_model_id
from mobi.ml.feature_data import FeatureData


class MobiModelException(Exception):
    pass


class MobiModel(ABC):
    _DUMP_KEY_NON_RECOMMENDABLE_PRODUCT_IDS = "non_recommendable_product_ids"
    _DUMP_KEY_NON_RECOMMENDABLE_BRANDS = "non_recommendable_brands"

    @property
    @abstractmethod
    def model_type(self) -> str:
        pass

    def __init__(self, model_id: str = None, date: datetime = None, status: ModelStatus = ModelStatus.NOT_READY):
        self.model_id = model_id or generate_model_id()
        self.date = date or datetime.utcnow()
        self.status = status

        self.non_recommendable_product_ids: Set[str] = set()
        self.non_recommendable_brands: Set[str] = set()
        self.products_map: Dict[str, Product] = dict()

        self.categories_idf: Dict[str, float] = dict()
        self.tags_idf: Dict[str, float] = dict()
        self.enriched_tags_idf: Dict[str, float] = dict()
        self.words_idf: Dict[str, float] = dict()

    def set_non_recommendable_product_ids(self, product_ids: Iterable[str]):
        self.non_recommendable_product_ids = set(product_ids)
        self.status = ModelStatus.NOT_READY

    def set_non_recommendable_brands(self, brands: Iterable[str]):
        self.non_recommendable_brands = set(brands)
        self.status = ModelStatus.NOT_READY

    @classmethod
    def calc_idf_data(cls, products: List[Product]) \
            -> Tuple[Dict[str, float], Dict[str, float], Dict[str, float], Dict[str, float]]:
        categories_counter = Counter()
        tags_counter = Counter()
        enriched_tags_counter = Counter()
        words_counter = Counter()

        products_len = 0

        for product in products:
            products_len += 1

            if product.categories:
                for category in product.categories:
                    categories_counter[category] += 1
            if product.tags:
                for tag in product.tags:
                    tags_counter[tag] += 1
            if product.enriched_tags:
                for enriched_tag in product.enriched_tags:
                    enriched_tags_counter[enriched_tag] += 1
            for word in product.title.split():
                if word:
                    words_counter[word.lower()] += 1

        categories_idf = {category: math.log(products_len / categories_counter[category])
                          for category in categories_counter}

        tags_idf = {tag: math.log(products_len / tags_counter[tag]) for tag in tags_counter}

        enriched_tags_idf = {enriched_tag: math.log(products_len / enriched_tags_counter[enriched_tag])
                             for enriched_tag in enriched_tags_counter}

        words_idf = {word: math.log(products_len / words_counter[word]) for word in words_counter if len(word) > 1}

        return categories_idf, tags_idf, enriched_tags_idf, words_idf

    def set_idf_data(self, categories_idf: Dict[str, float], tags_idf: Dict[str, float],
                     enriched_tags_idf: Dict[str, float], words_idf: Dict[str, float]):
        self.categories_idf, self.tags_idf, self.enriched_tags_idf, self.words_idf \
            = categories_idf, tags_idf, enriched_tags_idf, words_idf

    def compile(self, products: List[Product]):
        self.products_map = {product.product_id: product for product in products}
        self.set_idf_data(*self.calc_idf_data(products))

    def _dump_info(self) -> ModelDumpInfo:
        return ModelDumpInfo(
            model_id=self.model_id,
            model_type=self.model_type,
            date=self.date,
            status=self.status
        )

    def model_specific_meta_info(self) -> Dict[str, Any]:
        return {
            "non_recommendable_products": len(self.non_recommendable_product_ids),
            "non_recommendable_brands": len(self.non_recommendable_brands)
        }

    def meta_info(self) -> Dict[str, Dict[str, Any]]:
        return {
            "model_dump_info": self._dump_info().as_json_dict(),
            "model_specific_info": self.model_specific_meta_info()
        }

    def _dump_binary_as_dict(self) -> Dict[str, Any]:
        return {
            self._DUMP_KEY_NON_RECOMMENDABLE_PRODUCT_IDS: self.non_recommendable_product_ids,
            self._DUMP_KEY_NON_RECOMMENDABLE_BRANDS: self.non_recommendable_brands
        }

    def _dump_binary(self) -> bytes:
        return pickle.dumps(self._dump_binary_as_dict())

    def _process_dump_binary(self, binary: bytes):
        self._process_dump_binary_as_dict(pickle.loads(binary))

    def _process_dump_binary_as_dict(self, dump: Dict[str, Any]):
        self.non_recommendable_product_ids = dump[self._DUMP_KEY_NON_RECOMMENDABLE_PRODUCT_IDS]
        self.non_recommendable_brands = dump[self._DUMP_KEY_NON_RECOMMENDABLE_BRANDS]
        self.status = ModelStatus.NOT_READY

    def dump(self) -> ModelDump:
        return ModelDump(
            info=self._dump_info(),
            binary=self._dump_binary()
        )

    @classmethod
    def from_dump(cls, dump: ModelDump) -> 'MobiModel':
        model = super(MobiModel, cls).__new__(cls)
        if model.model_type != dump.info.model_type:
            raise MobiModelException(f"Class \"{cls.__name__}\" supports model type \"{model.model_type}\", but "
                                     f"a model dump of type \"{dump.info.model_type}\" was passed.")
        model.__init__(
            model_id=dump.info.model_id,
            date=dump.info.date,
            status=dump.info.status
        )
        model._process_dump_binary(dump.binary)
        return model

    @abstractmethod
    def similar_products(self, product: Product, brands: Optional[List[str]] = None,
                         categories: Optional[List[str]] = None, k: int = 10) -> List[str]:
        pass

    @abstractmethod
    def personal_recommendations(self, user_session: List[Event], brands: List[str] = None,
                                 categories: List[str] = None, k: int = 10) -> List[str]:
        pass

    @abstractmethod
    def basket_recommendations(self, product_ids: Iterable[str], num: int = 10) -> List[str]:
        pass

    @abstractmethod
    def rank_products_by_user_history(self, products: List[Product], user_history: UserHistoryCookie) \
            -> Optional[List[float]]:
        pass

    @abstractmethod
    def rank_products_by_source_product(self, products: List[Product], source_product: Product) \
            -> Optional[List[float]]:
        pass

    @abstractmethod
    def calc_feature_data(self, client: str, recommendation_type: RecommendationType, user_history: UserHistoryCookie,
                          products: List[Product], cache_dao: MobiCache, source_product: Optional[Product] = None) \
            -> Dict[str, FeatureData]:
        pass

    @abstractmethod
    def classify_products(self, client: str, recommendation_type: RecommendationType, user_history: UserHistoryCookie,
                          products: List[Product], cache_dao: MobiCache, source_product: Optional[Product] = None,
                          ignore_user_product_features: bool = False) -> Optional[List[float]]:
        pass


class MobiModelFactory(ABC):
    @abstractmethod
    def train(self, client: str) -> MobiModel:
        pass
