from datetime import datetime
from dataclasses import dataclass
from typing import Dict, Optional, Union


@dataclass
class FeatureData:
    categorical_features: Optional[Dict[str, Union[str, int]]]
    float_features: Optional[Dict[str, float]]
    int_features: Optional[Dict[str, int]]
    date: datetime

    def update(self, other_feature_data: 'FeatureData') -> 'FeatureData':
        if other_feature_data.categorical_features:
            if not self.categorical_features:
                self.categorical_features = other_feature_data.categorical_features
            else:
                self.categorical_features.update(other_feature_data.categorical_features)

        if other_feature_data.float_features:
            if not self.float_features:
                self.float_features = other_feature_data.float_features
            else:
                self.float_features.update(other_feature_data.float_features)

        if other_feature_data.int_features:
            if not self.int_features:
                self.int_features = other_feature_data.int_features
            else:
                self.int_features.update(other_feature_data.int_features)

        return self

    def add_categorical(self, feature_name, feature_value) -> 'FeatureData':
        if self.categorical_features is None:
            self.categorical_features = dict()
        self.categorical_features[feature_name] = feature_value
        return self

    def add_float(self, feature_name, feature_value) -> 'FeatureData':
        if self.float_features is None:
            self.float_features = dict()
        self.float_features[feature_name] = feature_value
        return self

    def add_int(self, feature_name, feature_value) -> 'FeatureData':
        if self.int_features is None:
            self.int_features = dict()
        self.int_features[feature_name] = feature_value
        return self

    def __str__(self):
        return f"CAT: {self.categorical_features}, FLOAT: {self.float_features}, INT: {self.int_features}"
