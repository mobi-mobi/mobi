from enum import Enum
from typing import Dict, Hashable, List, Optional


class VocabMode(Enum):
    TRAIN = 1
    INFERENCE = 2


class Vocabulary:
    def __init__(self, name: str, null_is_zero: bool = True, mode=VocabMode.TRAIN, build_inverted: bool = False):
        self.name = name
        self.null_is_zero = null_is_zero
        self.mode = mode
        self.map: Dict[Hashable, int] = dict()
        self.inverted_map: Optional[Dict[int, Hashable]] = None
        if build_inverted:
            self.inverted_map = dict()

    def size(self):
        return len(self.map) + int(self.null_is_zero)

    def __len__(self):
        return self.size()

    def _add_new(self, key: Hashable) -> int:
        # Not thread safe!
        value = self.size()  # As we count from 0
        self.map[key] = value
        if self.inverted_map is not None:
            self.inverted_map[value] = key
        return value

    def get(self, key: Hashable) -> int:
        if key is None and self.null_is_zero:
            return 0

        try:
            return self.map[key]
        except KeyError:
            if self.mode == VocabMode.TRAIN:
                return self._add_new(key)
            else:
                raise KeyError(f"Can not find \"{key}\" key in vocabulary \"{self.name}\"") from None

    def __getitem__(self, key: Hashable) -> int:
        return self.get(key)

    def add(self, key: Hashable):
        self.get(key)

    def get_multiple(self, keys: List[Hashable]) -> List[int]:
        return [self.get(key) for key in keys]

    def add_multiple(self, keys: List[Hashable]):
        for key in keys:
            self.get(key)

    def get_original(self, value: int) -> Optional[Hashable]:
        if self.inverted_map is None:
            raise KeyError(f"Vocabulary \"{self.name}\" is not aware of original values")

        if self.null_is_zero and value == 0:
            return None

        try:
            return self.inverted_map[value]
        except KeyError:
            raise KeyError(f"Can not find original key for value \"{value}\"") from None

    def get_original_multiple(self, values: List[int]) -> List[Hashable]:
        return [self.get_original(value) for value in values]

    def __contains__(self, item: Hashable) -> bool:
        if item is None and self.null_is_zero:
            return True

        return item in self.map

    def stop_training(self):
        self.mode = VocabMode.INFERENCE
