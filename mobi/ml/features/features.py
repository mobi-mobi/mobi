from abc import ABC, abstractmethod
from enum import Enum
from typing import Any, Dict, List, Optional, Union


class FeatureMode(Enum):
    TRAINING = 0
    INFERENCE = 1


class MobiFeature(ABC):
    def __init__(self, name: str, optional: bool = False, mode: FeatureMode = FeatureMode.TRAINING):
        self.name = name
        self.optional = optional
        self.mode = mode

    def set_mode(self, mode: FeatureMode):
        self.mode = mode

    @abstractmethod
    def assert_raw_not_none_value(self, value: Any):
        pass

    def assert_raw_value(self, value: Any):
        if value is None:
            if not self.optional:
                raise ValueError(f"\"None\" is a wrong value for the non-optional feature {self.name}")
        else:
            self.assert_raw_not_none_value(value)

    def as_dict(self) -> Dict[str, Any]:
        return {
            "name": self.name,
            "optional": self.optional,
        }

    @classmethod
    def from_dict(cls, d: dict):
        inst = super(MobiFeature, cls).__new__(cls)
        inst.__init__(
            name=d["name"],
            optional=d["optional"]
        )
        return inst

    def as_json_dict(self):
        return self.as_dict()

    @classmethod
    def from_json_dict(cls, d: dict):
        return cls.from_dict(d)


class MobiTextFeature(MobiFeature):
    def assert_raw_not_none_value(self, value: str):
        if not isinstance(value, str):
            raise ValueError("Text string expected")


class MobiCategoricalFeature(MobiFeature):
    def __init__(self, name: str, optional: bool = False, mode: FeatureMode = FeatureMode.TRAINING):
        super().__init__(name, optional, mode)
        self.vocabulary: Dict[Any, int] = dict()
        self.inverted_vocab: Dict[int, Any] = dict()

    def int_value(self, value: Optional[Any]) -> int:
        """ Transforms raw value into unique int value

        @param value: Any - raw value
        @return: int - unique integer representation
        """
        self.assert_raw_value(value)

        if value is None:
            return 0

        # Not thread safe
        if value not in self.vocabulary:
            if self.mode != FeatureMode.TRAINING:
                raise ValueError("Value is not in the vocabulary. Can not extend vocab "
                                 "while being not in the training mode")
            self.vocabulary[value] = len(self.vocabulary) + int(self.optional)
            self.inverted_vocab[self.vocabulary[value]] = value

        return self.vocabulary[value]

    def raw_value(self, value: int) -> Any:
        """ Retrieves original feature value

        @param value: int - categorical representation
        @return:
        """
        if value == 0 and self.optional:
            return None
        return self.inverted_vocab[value]

    @classmethod
    def value_as_str(cls, value: Any) -> str:
        return str(value)

    @classmethod
    @abstractmethod
    def value_from_str(cls, s: str) -> Any:
        pass

    def as_dict(self):
        result = super(MobiCategoricalFeature, self).as_dict()
        result["vocabulary"] = self.vocabulary
        result["inverted_vocab"] = self.inverted_vocab
        return result

    def as_json_dict(self):
        result = self.as_dict()
        result["vocabulary"] = {self.value_as_str(k): v for k, v in result["inverted_vocabulary"].items()}
        result["inverted_vocabulary"] = {str(k): v for k, v in result["inverted_vocabulary"].items()}

    @classmethod
    def from_dict(cls, d: dict):
        inst = super(MobiCategoricalFeature, cls).from_dict(d)
        inst.vocabulary = d["vocabulary"]
        inst.inverted_vocab = d["inverted_vocab"]
        return inst

    @classmethod
    def from_json_dict(cls, d: dict):
        inst = cls.from_dict(d)
        inst.vocabulary = {cls.value_from_str(k): v for k, v in d["vocabulary"].items()}
        inst.inverted_vocab = {int(k): v for k, v in d["inverted_vocab"].items()}
        return inst


class MobiCategoricalStrFeature(MobiCategoricalFeature):
    @classmethod
    def value_from_str(cls, s: str) -> Any:
        return s

    def assert_raw_not_none_value(self, value: str):
        if not isinstance(value, str):
            raise ValueError(f"Can not use non-string value for categorical string feature \"{self.name}\"")


class MobiCategoricalIntFeature(MobiCategoricalFeature):
    @classmethod
    def value_from_str(cls, s: str) -> Any:
        return int(s)

    def assert_raw_not_none_value(self, value: Any):
        if not isinstance(value, int):
            raise ValueError(f"Can not use non-int value for categorical int feature \"{self.name}\"")


class MobiCategoricalBoolFeature(MobiCategoricalFeature):
    def __init__(self, name: str, optional: bool = False, mode: FeatureMode = FeatureMode.TRAINING):
        super().__init__(name, optional, mode)

        # Initialize values as we know them
        self.int_value(False)
        self.int_value(True)
        if self.optional:
            self.int_value(None)

    @classmethod
    def value_from_str(cls, s: str) -> Any:
        return s == "True"

    def assert_raw_not_none_value(self, value: Any):
        if not isinstance(value, bool):
            raise ValueError(f"Can not use non-bool value for categorical bool feature \"{self.name}\"")


class MobiCategoricalNumericIntervalFeature(MobiCategoricalFeature):
    def __init__(self, name: str, bounds: List[Union[int, float]], optional: bool = False,
                 mode: FeatureMode = FeatureMode.TRAINING):
        super(MobiCategoricalNumericIntervalFeature, self).__init__(name, optional, mode)

        if not isinstance(bounds, list):
            raise ValueError("\"bounds\" parameter should be a list")
        if any(map(lambda x: not(isinstance(x, int) or isinstance(x, float)), bounds)):
            raise ValueError("One of the bounds is not a numeric value")
        if any(map(lambda left, right: left >= right, *zip(*zip(bounds, bounds[1:])))):
            raise ValueError("list of bounds should be sorted and contain unique values")

        self.bounds = bounds

        # Initiate ordered int values
        for i in range(0, len(self.bounds) + 1):
            self.int_value(i)
        if self.optional:
            self.int_value(None)

    def int_value(self, value: Optional[Any]) -> int:
        if value is not None:
            for i, bound in enumerate(self.bounds):
                if bound > value:
                    value = i
                    break
            else:
                value = len(self.bounds)

        return super(MobiCategoricalNumericIntervalFeature, self).int_value(value)

    @classmethod
    def value_from_str(cls, s: str) -> int:
        return int(s)

    def assert_raw_not_none_value(self, value: Any):
        if not (isinstance(value, int) or isinstance(value, float)):
            raise ValueError(f"Numeric value expected for numeric interval feature")


class MobiMultiCategoricalFeature(MobiFeature):
    def __init__(self, name: str, categorical_feature: MobiCategoricalFeature, optional: bool = False,
                 pad_to: Optional[int] = None, mode: FeatureMode = FeatureMode.TRAINING):
        super().__init__(name, optional, mode)
        if not isinstance(categorical_feature, MobiCategoricalFeature):
            raise ValueError("feature_class should be an instance of MobiCategoricalFeature class")
        self.categorical_feature = categorical_feature
        if pad_to is not None:
            if not isinstance(pad_to, int) or pad_to < 1:
                raise ValueError("pad_to should be a valid positive integer")
            if not categorical_feature.optional:
                raise ValueError("Categorical feature has to be optional in order to use padding")
        self.pad_to = pad_to

    def assert_raw_not_none_value(self, value: List[Any]):
        if not isinstance(value, list):
            raise ValueError("Value should be a valid list")
        if not value:
            if not self.optional:
                raise ValueError("Empty array is not a valid feature value for a non-optional feature")
        for v in value:
            self.categorical_feature.assert_raw_value(v)

    def raw_values(self, value: List[int]) -> List[Any]:
        return [self.categorical_feature.raw_value(v) for v in value]

    def int_values(self, value: Optional[List[Any]]) -> List[int]:
        self.assert_raw_value(value)

        if not value:
            if self.pad_to:
                if value is None:
                    value = []
            else:
                return value

        result = [self.categorical_feature.int_value(v) for v in value]

        if self.pad_to:
            if len(result) > self.pad_to:
                result = result[0:self.pad_to]
            if len(result) < self.pad_to:
                result.extend([self.categorical_feature.int_value(None)] * (self.pad_to - len(result)))

        return result
