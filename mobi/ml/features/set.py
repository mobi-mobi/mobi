from enum import Enum
from typing import Dict, List

from mobi.ml.features.features import MobiFeature


class FeatureSource(Enum):
    PRODUCT = 1
    EVENT = 2
    USER = 3


class FeatureSet:
    def __init__(self, features: Dict[FeatureSource, List[MobiFeature]]):
        self.features = features

    def sources(self):
        return list(self.features.keys())

    def __getitem__(self, item: FeatureSource):
        return self.features[item]
