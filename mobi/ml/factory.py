from datetime import datetime, timedelta

from mobi.config.client._base import ClientBaseConfig
from mobi.config.system._base import SystemBaseConfig
from mobi.dao import get_catalog_dao, get_event_dao, get_recolog_dao
from mobi.ml.model.interface import MobiModelFactory
from mobi.ml.model.embedding.impl.factories.deepwalk import DeepwalkStaticEmbeddingModelFactory


def get_model_factory(client_config: ClientBaseConfig, config: SystemBaseConfig) -> MobiModelFactory:
    if client_config.MODEL_TYPE == "deepwalk":
        return DeepwalkStaticEmbeddingModelFactory(
            catalog_dao=get_catalog_dao(config),
            event_dao=get_event_dao(config),
            recolog_dao=get_recolog_dao(config),
            from_date=datetime.utcnow() - timedelta(days=30),
            till_date=datetime.utcnow(),
            dimensions=10
        )

    raise ValueError(f"Can not find model factory for the value \"{client_config.MODEL_TYPE}\"")
