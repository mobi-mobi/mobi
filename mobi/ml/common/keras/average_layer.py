import keras
import tensorflow as tf


class AverageLayer(keras.layers.Layer):
    def __init__(self, **kwargs):
        self.supports_masking = True
        super(AverageLayer, self).__init__(**kwargs)

    def compute_mask(self, _, input_mask=None):
        return None

    def call(self, inputs, mask=None):
        if mask is not None:
            # mask (batch, time)
            mask = keras.backend.cast(mask, keras.backend.floatx())

            mask_non_zero = keras.backend.sum(mask)

            # From: https://stackoverflow.com/questions/39510809/mean-or-max-pooling-with-
            # masking-support-in-keras

            # mask (batch, x_dim, time)
            mask = keras.backend.repeat(mask, inputs.shape[-1])

            # mask (batch, time, x_dim)
            mask = tf.transpose(mask, [0, 2, 1])

            return tf.cond(
                keras.backend.less(mask_non_zero, 0.5),
                lambda: keras.backend.mean(inputs * 0.0, axis=1),
                lambda: keras.backend.sum(inputs * mask, axis=1) / mask_non_zero
            )

        return keras.backend.mean(inputs, axis=1)

    def compute_output_shape(self, input_shape):
        t = list(input_shape)
        t.pop(1)
        return tuple(t)
