import numpy as np

from sklearn.neighbors import BallTree

from abc import ABC, abstractmethod
from typing import Dict, Hashable, Iterable, List, Set, Sized, Tuple, Type


class MobiKnnIndex(ABC):
    def __init__(self, items: Iterable[Tuple[Hashable, List[float]]]):
        self.items = list(items)

        if not len(self.items):
            raise ValueError("Need at least one element in the index")

        self.dimensions = len(self.items[0][1])

        processed: Set[Hashable] = set()
        self.inverted_vocabulary: List[Hashable] = []

        for item_id, embedding in self.items:
            if len(embedding) != self.dimensions:
                raise ValueError(f"Embeddings have different dimensions: {self.dimensions} and {len(embedding)}")

            # if item_id in processed:
            #     raise ValueError(f"Element \"{item_id}\" is duplicated")

            self.inverted_vocabulary.append(item_id)
            processed.add(item_id)

        del processed

    def __len__(self):
        return len(self.items)

    @abstractmethod
    def query(self, embedding: List[float], k: int) -> List[Tuple[float, Hashable]]:
        pass


class KnnParametrizedIndex:
    _DIST = float(10 ** 6)

    def __init__(self, items: Iterable[Tuple[Hashable, Hashable, List[float]]], index_cls: Type[MobiKnnIndex]):
        self.vocab: Dict[Hashable, int] = dict()
        embeddings = []
        for item_id, parameter, embedding in items:
            if parameter not in self.vocab:
                self.vocab[parameter] = len(self.vocab)
            embeddings.append((item_id, embedding + [self.vocab[parameter] * self._DIST]))

        self.index = index_cls(embeddings)
        del embeddings

    def __len__(self):
        return len(self.index)

    def query(self, embedding: List[float], parameter: Hashable, k: int) -> List[Tuple[float, Hashable]]:
        if parameter not in self.vocab:
            return []

        return [
            (dist, item_id)
            for dist, item_id in self.index.query(embedding + [self.vocab[parameter] * self._DIST], k)
            if dist < self._DIST
        ]


class KnnMultiParametrizedIndex:
    _DIST = float(10 ** 6)

    def __init__(self, items: Iterable[Tuple[Hashable, Iterable[Hashable], List[float]]], index_cls: Type[MobiKnnIndex],
                 parameters_num: int):
        assert parameters_num >= 1
        self.vocabs: List[Dict[Hashable, int]] = [dict() for _ in range(parameters_num)]
        embeddings = []
        for item_id, parameters, embedding in items:
            extra_embedding = []

            for parameter_position, parameter in enumerate(parameters):
                if parameter not in self.vocabs[parameter_position]:
                    self.vocabs[parameter_position][parameter] = len(self.vocabs[parameter_position])
                extra_embedding.append(self._DIST * self.vocabs[parameter_position][parameter])

            assert len(extra_embedding) == parameters_num
            embeddings.append((item_id, embedding + extra_embedding))
        self.index = index_cls(embeddings)

    def __len__(self):
        return len(self.index)

    def query(self, embedding: List[float], parameters: Iterable[Hashable], k: int) -> List[Tuple[float, Hashable]]:
        extra_embedding = []
        return_empty = False
        for parameter_position, parameter in enumerate(parameters):
            if parameter_position == len(self.vocabs):
                raise ValueError("Too many parameters given")
            if parameter not in self.vocabs[parameter_position]:
                return_empty = True
                continue
            extra_embedding.append(self._DIST * self.vocabs[parameter_position][parameter])
        if return_empty:
            return []
        return [
            (dist, item_id)
            for dist, item_id in self.index.query(embedding + extra_embedding, k)
            if dist < self._DIST
        ]


class BallTreeKnnIndex(MobiKnnIndex):
    def __init__(self, items: Iterable[Tuple[Hashable, List[float]]]):
        super().__init__(items)
        self.ball_tree = BallTree(np.vstack([embedding for _, embedding in self.items]))

    def query(self, embedding: List[float], k: int) -> List[Tuple[float, Hashable]]:
        if k > len(self.items):
            raise ValueError("Parameter k can not be larger than number of items in the index")
        if len(embedding) != self.dimensions:
            raise ValueError("Embedding dimensionality differs from the dimensionality of index space")
        distances, positions = self.ball_tree.query([embedding], k=k)
        distances = distances[0]
        positions = positions[0]
        return [(dist, self.inverted_vocabulary[position]) for dist, position in zip(distances, positions)]
