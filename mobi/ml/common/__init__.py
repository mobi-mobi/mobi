from datetime import datetime
from hashlib import sha1
from math import sqrt
from random import randrange
from typing import List


def is_train_user(uid: str, train_population: float) -> bool:
    k = 10 ** 9
    salt = "SOME_SALT"
    return int(sha1(bytes(salt + uid, "utf-8")).hexdigest(), 16) % k < train_population * k


def generate_model_id() -> str:
    return f"model_{int(datetime.utcnow().timestamp())}_{randrange(1000000)}"


def average_embedding(embeddings: List[List[float]]) -> List[float]:
    return list(map(lambda x: x / len(embeddings), map(sum, zip(*embeddings))))


def embeddings_dist(e1: List[float], e2: List[float]) -> float:
    return sqrt(sum(map(lambda x1, x2: (x1 - x2) ** 2, e1, e2)))
