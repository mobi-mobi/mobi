class MobiConfig:
    def __getitem__(self, item):
        try:
            return getattr(self, item)
        except AttributeError:
            raise KeyError(f"Property \"{item}\" is not defined in \"{self.__class__.__name__}\" config") from None

    def get(self, item, default=None):
        try:
            return self[item]
        except KeyError:
            return default
