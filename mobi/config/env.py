import os

from enum import Enum

from mobi.core.logging import get_logger


logger = get_logger()
MOBI_ENV_VAR = "MOBI_ENV"

__WARN_SHOWN = False


class Env(Enum):
    GITLAB = 0  # Used to run unit tests in gitlab environment

    UNITTEST = 1  # Used to run unit tests with mock services

    TEST = 2  # Used to run unit tests with real services

    PREPROD = 3

    PROD = 4


def get_env(default=Env.TEST) -> Env:
    global __WARN_SHOWN

    env = os.environ.get(MOBI_ENV_VAR, "").upper()
    if not env:
        if not __WARN_SHOWN:
            __WARN_SHOWN = True
            logger.warning(f"Can not find \"{MOBI_ENV_VAR}\" environment variable. Using \"{default.name}\"")

        return default

    try:
        return Env[env]
    except KeyError:
        raise KeyError(f"Wrong environment: \"{env}\"")
