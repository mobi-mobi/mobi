import threading

from time import time
from typing import Callable


__import_lock = threading.Lock()


def memoize_60(func: Callable) -> Callable:
    global __import_lock
    results = {}

    def helper(*args, **kwargs):
        key = str(args) + '+++' + str(kwargs)
        with __import_lock:
            if key not in results or results[key][1] < time() - 60:
                results[key] = (func(*args, **kwargs), time())
        return results[key][0]
    return helper
