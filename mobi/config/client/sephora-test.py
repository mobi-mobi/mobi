from mobi.config.client._base import ClientBaseConfig
from mobi.enrich import StaticCategorizer


class Config(ClientBaseConfig):
    PRODUCT_ENRICHERS = [
        StaticCategorizer()
    ]
