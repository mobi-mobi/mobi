import importlib.util
import os

from mobi.config.client._active import ACTIVE_CLIENTS
from mobi.config.client._base import ClientBaseConfig
from mobi.config.common import memoize_60


@memoize_60
def get_client_config(client: str) -> ClientBaseConfig:
    if client not in ACTIVE_CLIENTS:
        raise ValueError(f"Can not find client \"{client}\" in the list of active clients. Can not load config.")
    file = os.path.join(os.path.dirname(os.path.realpath(__file__)), f"{client}.py")
    spec = importlib.util.spec_from_file_location(f"mobi.config.client.{client}", file)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    config = module.Config()
    return config
