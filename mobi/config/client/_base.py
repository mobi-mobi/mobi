from typing import List

from mobi.config._base import MobiConfig
from mobi.core.population import Population, PopulationSetup
from mobi.enrich.enricher import ProductEnricher


class ClientBaseConfig(MobiConfig):
    POPULATIONS = {
        Population.REFERENCE: 20,
        Population.TARGET: 70,
        Population.TEST: 10
    }

    __POPULATION_SETUP = None

    @classmethod
    def population_setup(cls) -> PopulationSetup:
        # Not thread safe, but should not be a problem
        if cls.__POPULATION_SETUP is None:
            cls.__POPULATION_SETUP = PopulationSetup(cls.POPULATIONS)
        return cls.__POPULATION_SETUP

    MODEL_TYPE = "deepwalk"

    PRODUCT_ENRICHERS: List[ProductEnricher] = []

    BRAND_EVENT_COUNTERS = []
    PRODUCT_ID_EVENT_COUNTERS = []

    EVENTS_RETENTION_DAYS = 60
    RECOLOGS_RETENTION_DAYS = 60

    METRICS_DAILY_RETENTION_DAYS = 365
    METRICS_HOURS_RETENTION_DAYS = 35
    METRICS_MINUTES_RETENTION_DAYS = 7

    """
    PRODUCT_ID_EVENT_COUNTERS: List[EventCounterParams] = [
        EventCounterParams(name="product_id_views_1h", event_type=EventType.PRODUCT_VIEW,
                           time_window=timedelta(hours=1), preciseness=timedelta(minutes=1),
                           cache_for=timedelta(minutes=1)),
        EventCounterParams(name="product_id_to_basket_1h", event_type=EventType.PRODUCT_TO_BASKET,
                           time_window=timedelta(hours=1), preciseness=timedelta(minutes=1),
                           cache_for=timedelta(minutes=1)),

        EventCounterParams(name="product_id_views_8h", event_type=EventType.PRODUCT_VIEW,
                           time_window=timedelta(hours=8), preciseness=timedelta(minutes=5),
                           cache_for=timedelta(minutes=1)),
        EventCounterParams(name="product_id_to_basket_8h", event_type=EventType.PRODUCT_TO_BASKET,
                           time_window=timedelta(hours=8), preciseness=timedelta(minutes=5),
                           cache_for=timedelta(minutes=1)),

        EventCounterParams(name="product_id_views_24h", event_type=EventType.PRODUCT_VIEW,
                           time_window=timedelta(hours=24), preciseness=timedelta(minutes=20),
                           cache_for=timedelta(minutes=1)),
        EventCounterParams(name="product_id_to_basket_24h", event_type=EventType.PRODUCT_TO_BASKET,
                           time_window=timedelta(hours=24), preciseness=timedelta(minutes=20),
                           cache_for=timedelta(minutes=1)),
    ]

    BRAND_EVENT_COUNTERS: List[EventCounterParams] = [
        EventCounterParams(name="brand_views_1h", event_type=EventType.PRODUCT_VIEW,
                           time_window=timedelta(hours=1), preciseness=timedelta(minutes=1),
                           cache_for=timedelta(minutes=1)),
        EventCounterParams(name="brand_to_basket_1h", event_type=EventType.PRODUCT_TO_BASKET,
                           time_window=timedelta(hours=1), preciseness=timedelta(minutes=1),
                           cache_for=timedelta(minutes=1)),

        EventCounterParams(name="brand_views_8h", event_type=EventType.PRODUCT_VIEW,
                           time_window=timedelta(hours=8), preciseness=timedelta(minutes=5),
                           cache_for=timedelta(minutes=1)),
        EventCounterParams(name="brand_to_basket_8h", event_type=EventType.PRODUCT_TO_BASKET,
                           time_window=timedelta(hours=8), preciseness=timedelta(minutes=5),
                           cache_for=timedelta(minutes=1)),

        EventCounterParams(name="brand_views_24h", event_type=EventType.PRODUCT_VIEW,
                           time_window=timedelta(hours=24), preciseness=timedelta(minutes=20),
                           cache_for=timedelta(minutes=1)),
        EventCounterParams(name="brand_to_basket_24h", event_type=EventType.PRODUCT_TO_BASKET,
                           time_window=timedelta(hours=24), preciseness=timedelta(minutes=20),
                           cache_for=timedelta(minutes=1)),
    ]
    """
