import os

from mobi.config._base import MobiConfig
from mobi.config.enums import ModelPickingStrategy


class SystemBaseConfig(MobiConfig):
    AUTH_DAO = "mongodb"
    AUTH_MONGODB_HOST = os.environ.get("AUTH_MONGODB_HOST", "127.0.0.1")
    AUTH_MONGODB_PORT = os.environ.get("AUTH_MONGODB_PORT", 27017)
    AUTH_MONGODB_DB = os.environ.get("AUTH_MONGODB_DB", "mobi")

    AUDIENCE_DAO = "mongodb"
    AUDIENCE_MONGODB_HOST = os.environ.get("AUDIENCE_MONGODB_HOST", "127.0.0.1")
    AUDIENCE_MONGODB_PORT = os.environ.get("AUDIENCE_MONGODB_PORT", 27017)
    AUDIENCE_MONGODB_DB = os.environ.get("AUDIENCE_MONGODB_DB", "mobi")

    EMAIL_DAO = "mongodb"
    EMAIL_MONGODB_HOST = os.environ.get("EMAIL_MONGODB_HOST", "127.0.0.1")
    EMAIL_MONGODB_PORT = os.environ.get("EMAIL_MONGODB_PORT", 27017)
    EMAIL_MONGODB_DB = os.environ.get("EMAIL_MONGODB_DB", "mobi")

    EVENT_DAO = "mongodb"
    EVENT_MONGODB_HOST = os.environ.get("EVENT_MONGODB_HOST", "127.0.0.1")
    EVENT_MONGODB_PORT = os.environ.get("EVENT_MONGODB_PORT", 27017)
    EVENT_MONGODB_DB = os.environ.get("EVENT_MONGODB_DB", "mobi")

    METRICS_DAO = "mongodb"
    METRICS_MONGODB_HOST = os.environ.get("METRICS_MONGODB_HOST", "127.0.0.1")
    METRICS_MONGODB_PORT = os.environ.get("METRICS_MONGODB_PORT", 27017)
    METRICS_MONGODB_DB = os.environ.get("METRICS_MONGODB_DB", "mobi")

    MODEL_DAO = "mongodb"
    MODEL_MONGODB_HOST = os.environ.get("MODEL_MONGODB_HOST", "127.0.0.1")
    MODEL_MONGODB_PORT = os.environ.get("MODEL_MONGODB_PORT", 27017)
    MODEL_MONGODB_DB = os.environ.get("MODEL_MONGODB_DB", "mobi")

    CATALOG_DAO = "mongodb"
    CATALOG_MONGODB_HOST = os.environ.get("CATALOG_MONGODB_HOST", "127.0.0.1")
    CATALOG_MONGODB_PORT = os.environ.get("CATALOG_MONGODB_PORT", 27017)
    CATALOG_MONGODB_DB = os.environ.get("CATALOG_MONGODB_DB", "mobi")

    OBJECT_DAO = "mongodb"
    OBJECT_MONGODB_HOST = os.environ.get("OBJECT_MONGODB_HOST", "127.0.0.1")
    OBJECT_MONGODB_PORT = os.environ.get("OBJECT_MONGODB_PORT", 27017)
    OBJECT_MONGODB_DB = os.environ.get("OBJECT_MONGODB_DB", "mobi")

    RECOLOG_DAO = "mongodb"
    RECOLOG_MONGODB_HOST = os.environ.get("RECOLOG_MONGODB_HOST", "127.0.0.1")
    RECOLOG_MONGODB_PORT = os.environ.get("RECOLOG_MONGODB_PORT", 27017)
    RECOLOG_MONGODB_DB = os.environ.get("RECOLOG_MONGODB_DB", "mobi")

    SETTINGS_DAO = "mongodb"
    SETTINGS_MONGODB_HOST = os.environ.get("SETTINGS_MONGODB_HOST", "127.0.0.1")
    SETTINGS_MONGODB_PORT = os.environ.get("SETTINGS_MONGODB_PORT", 27017)
    SETTINGS_MONGODB_DB = os.environ.get("SETTINGS_MONGODB_DB", "mobi")

    USER_SETTINGS_DAO = "mongodb"
    USER_SETTINGS_MONGODB_HOST = os.environ.get("USER_SETTINGS_MONGODB_HOST", "127.0.0.1")
    USER_SETTINGS_MONGODB_PORT = os.environ.get("USER_SETTINGS_MONGODB_PORT", 27017)
    USER_SETTINGS_MONGODB_DB = os.environ.get("USER_SETTINGS_MONGODB_DB", "mobi")

    CACHE = "memcached"
    MEMCACHED_HOST = os.environ.get("MEMCACHED_HOST", "127.0.0.1")
    MEMCACHED_PORT = os.environ.get("MEMCACHED_PORT", 11211)

    SEARCH = "elasticsearch"
    ELASTICSEARCH_HOST = os.environ.get("ELASTICSEARCH_HOST", "127.0.0.1")
    ELASTICSEARCH_PORT = os.environ.get("ELASTICSEARCH_PORT", 9200)

    MODEL_TYPE = "deepwalk"
    MODEL_PICKING_STRATEGY = ModelPickingStrategy.LATEST
    # Should be set when ModelPickingStrategy.PREDEFINED is in use
    ACTIVE_MODEL_ID = None

    MESSAGE_DAO = "kafka"
    KAFKA_HOST = "localhost"
    KAFKA_PORT = 9092
    KAFKA_SECURITY_PROTOCOL = None
    KAFKA_SASL_MECHANISM = None

    SMTP_USER = os.environ.get("SMTP_USER", "no-reply@mobimobi.tech")
    SMTP_PASSWORD = os.environ.get("SMTP_PASSWORD", "")
    SMTP_SENDER_NAME = "Mobimobi.tech"
