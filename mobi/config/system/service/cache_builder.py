from mobi.config.system.service._base import ServiceBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class CacheBuilderBaseConfig(ServiceBaseConfig):
    pass


class CacheBuilderGitlabConfig(CacheBuilderBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class CacheBuilderUnitTestConfig(CacheBuilderBaseConfig):
    pass


class CacheBuilderTestConfig(CacheBuilderBaseConfig):
    pass


class CacheBuilderProdConfig(CacheBuilderBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(CacheBuilderGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(CacheBuilderUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(CacheBuilderTestConfig):
        pass
elif env == Env.PROD:
    class Config(CacheBuilderProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_cache_builder_config() -> CacheBuilderBaseConfig:
    return Config()
