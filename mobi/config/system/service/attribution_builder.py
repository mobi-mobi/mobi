from mobi.config.system.service._base import ServiceBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class AttributionBuilderBaseConfig(ServiceBaseConfig):
    pass


class AttributionBuilderGitlabConfig(AttributionBuilderBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class AttributionBuilderUnitTestConfig(AttributionBuilderBaseConfig):
    pass


class AttributionBuilderTestConfig(AttributionBuilderBaseConfig):
    pass


class AttributionBuilderProdConfig(AttributionBuilderBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(AttributionBuilderGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(AttributionBuilderUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(AttributionBuilderTestConfig):
        pass
elif env == Env.PROD:
    class Config(AttributionBuilderProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_attribution_builder_config() -> AttributionBuilderBaseConfig:
    return Config()
