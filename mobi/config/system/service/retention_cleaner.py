from mobi.config.system.service._base import ServiceBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class RetentionCleanerBaseConfig(ServiceBaseConfig):
    pass


class RetentionCleanerGitlabConfig(RetentionCleanerBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class RetentionCleanerUnitTestConfig(RetentionCleanerBaseConfig):
    pass


class RetentionCleanerTestConfig(RetentionCleanerBaseConfig):
    pass


class RetentionCleanerProdConfig(RetentionCleanerBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(RetentionCleanerGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(RetentionCleanerUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(RetentionCleanerTestConfig):
        pass
elif env == Env.PROD:
    class Config(RetentionCleanerProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_retention_cleaner_config() -> RetentionCleanerBaseConfig:
    return Config()
