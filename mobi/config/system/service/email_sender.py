from mobi.config.system.service._base import ServiceBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class EmailSenderBaseConfig(ServiceBaseConfig):
    pass


class EmailSenderGitlabConfig(EmailSenderBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class EmailSenderUnitTestConfig(EmailSenderBaseConfig):
    pass


class EmailSenderTestConfig(EmailSenderBaseConfig):
    pass


class EmailSenderProdConfig(EmailSenderBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(EmailSenderGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(EmailSenderUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(EmailSenderTestConfig):
        pass
elif env == Env.PROD:
    class Config(EmailSenderProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_email_sender_config() -> EmailSenderBaseConfig:
    return Config()
