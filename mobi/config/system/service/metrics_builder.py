from mobi.config.system.service._base import ServiceBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class MetricsBuilderBaseConfig(ServiceBaseConfig):
    pass


class MetricsBuilderGitlabConfig(MetricsBuilderBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class MetricsBuilderUnitTestConfig(MetricsBuilderBaseConfig):
    pass


class MetricsBuilderTestConfig(MetricsBuilderBaseConfig):
    pass


class MetricsBuilderProdConfig(MetricsBuilderBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(MetricsBuilderGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(MetricsBuilderUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(MetricsBuilderTestConfig):
        pass
elif env == Env.PROD:
    class Config(MetricsBuilderProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_metrics_builder_config() -> MetricsBuilderBaseConfig:
    return Config()
