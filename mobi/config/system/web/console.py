from mobi.config.system.web._base import WebBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class ConsoleWebBaseConfig(WebBaseConfig):
    pass


class ConsoleWebGitlabConfig(ConsoleWebBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUTH_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class ConsoleWebUnitTestConfig(ConsoleWebBaseConfig):
    pass


class ConsoleWebTestConfig(ConsoleWebBaseConfig):
    pass


class ConsoleWebProdConfig(ConsoleWebBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(ConsoleWebGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(ConsoleWebUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(ConsoleWebTestConfig):
        pass
elif env == Env.PROD:
    class Config(ConsoleWebProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_console_web_config() -> ConsoleWebBaseConfig:
    return Config()
