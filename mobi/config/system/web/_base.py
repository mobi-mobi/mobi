import os

from mobi.config.system._base import SystemBaseConfig


class WebBaseConfig(SystemBaseConfig):
    AUTH_DAO = "mongodb"

    AUTH_MONGODB_HOST = os.environ.get("AUTH_MONGODB_HOST", "127.0.0.1")
    AUTH_MONGODB_PORT = os.environ.get("AUTH_MONGODB_PORT", 27017)
    AUTH_MONGODB_DB = os.environ.get("AUTH_MONGODB_DB", "mobi")
