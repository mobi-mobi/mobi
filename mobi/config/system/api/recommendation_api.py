from mobi.config.system.api._base import ApiBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class RecommendationApiBaseConfig(ApiBaseConfig):
    pass


class RecommendationApiGitlabConfig(RecommendationApiBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class RecommendationApiUnitTestConfig(RecommendationApiBaseConfig):
    pass


class RecommendationApiTestConfig(RecommendationApiBaseConfig):
    pass


class RecommendationApiProdConfig(RecommendationApiBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(RecommendationApiGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(RecommendationApiUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(RecommendationApiTestConfig):
        pass
elif env == Env.PROD:
    class Config(RecommendationApiProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_recommendation_api_config() -> RecommendationApiBaseConfig:
    return Config()
