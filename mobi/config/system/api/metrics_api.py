from mobi.config.system.api._base import ApiBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class MetricsApiBaseConfig(ApiBaseConfig):
    pass


class MetricsApiGitlabConfig(MetricsApiBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class MetricsApiUnitTestConfig(MetricsApiBaseConfig):
    pass


class MetricsApiTestConfig(MetricsApiBaseConfig):
    pass


class MetricsApiProdConfig(MetricsApiBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(MetricsApiGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(MetricsApiUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(MetricsApiTestConfig):
        pass
elif env == Env.PROD:
    class Config(MetricsApiProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_metrics_api_config() -> MetricsApiBaseConfig:
    return Config()
