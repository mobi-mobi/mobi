from mobi.config.system.api._base import ApiBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class EventApiBaseConfig(ApiBaseConfig):
    pass


class EventApiGitlabConfig(EventApiBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class EventApiUnitTestConfig(EventApiBaseConfig):
    pass


class EventApiTestConfig(EventApiBaseConfig):
    pass


class EventApiProdConfig(EventApiBaseConfig):
    KAFKA_HOST = "rc1c-83eui276g1ghr6np.mdb.yandexcloud.net"
    KAFKA_PORT = 9092
    KAFKA_SECURITY_PROTOCOL = "SASL_PLAINTEXT"
    KAFKA_SASL_MECHANISM = "SCRAM-SHA-512"


env = get_env()

if env == Env.GITLAB:
    class Config(EventApiGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(EventApiUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(EventApiTestConfig):
        pass
elif env == Env.PROD:
    class Config(EventApiProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_event_api_config() -> ApiBaseConfig:
    return Config()
