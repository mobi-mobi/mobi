from mobi.config.system.api._base import ApiBaseConfig

from mobi.config.common import memoize_60
from mobi.config.env import Env, get_env


class CatalogApiBaseConfig(ApiBaseConfig):
    pass


class CatalogApiGitlabConfig(CatalogApiBaseConfig):
    MEMCACHED_HOST = "memcached"
    ELASTICSEARCH_HOST = "elasticsearch"
    AUDIENCE_MONGODB_HOST = "mongo"
    EVENT_MONGODB_HOST = "mongo"
    METRICS_MONGODB_HOST = "mongo"
    MODEL_MONGODB_HOST = "mongo"
    CATALOG_MONGODB_HOST = "mongo"
    OBJECT_MONGODB_HOST = "mongo"
    RECOLOG_MONGODB_HOST = "mongo"
    SETTINGS_MONGODB_HOST = "mongo"
    USER_SETTINGS_MONGODB_HOST = "mongo"


class CatalogApiUnitTestConfig(CatalogApiBaseConfig):
    pass


class CatalogApiTestConfig(CatalogApiBaseConfig):
    pass


class CatalogApiProdConfig(CatalogApiBaseConfig):
    pass


env = get_env()

if env == Env.GITLAB:
    class Config(CatalogApiGitlabConfig):
        pass
elif env == Env.UNITTEST:
    class Config(CatalogApiUnitTestConfig):
        pass
elif env == Env.TEST:
    class Config(CatalogApiTestConfig):
        pass
elif env == Env.PROD:
    class Config(CatalogApiProdConfig):
        pass
else:
    raise ValueError(f"Can not find config for environment \"{env.name}\"")


@memoize_60
def get_catalog_api_config() -> ApiBaseConfig:
    return Config()
