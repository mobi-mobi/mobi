from enum import Enum


class ModelPickingStrategy(Enum):
    LATEST = 0
    PREDEFINED = 1
