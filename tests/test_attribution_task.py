import pytest

from datetime import datetime, timedelta

from mobi.config.system.service.attribution_builder import get_attribution_builder_config
from mobi.core.models import AttributionType, EventType, RecommendationType
from mobi.dao import get_catalog_dao, get_event_dao, get_metrics_dao, get_recolog_dao
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.recolog import MobiRecologDao
from mobi.service.task.impl.attribution_builder import AttributionBuilderTask
from mobi.utils.tests import random_event, random_product, random_recommendation


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def catalog_dao() -> MobiCatalogDao:
    yield get_catalog_dao(get_attribution_builder_config())


@pytest.fixture(scope="session")
def event_dao() -> MobiEventDao:
    yield get_event_dao(get_attribution_builder_config())


@pytest.fixture(scope="session")
def metrics_dao() -> MobiMetricsDao:
    yield get_metrics_dao(get_attribution_builder_config())


@pytest.fixture(scope="session")
def recolog_dao() -> MobiRecologDao:
    yield get_recolog_dao(get_attribution_builder_config())


def test_attribution_builder(client, catalog_dao, event_dao, metrics_dao, recolog_dao):
    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)
    metrics_dao.delete_all(client)
    recolog_dao.delete_all_recommendations(client)
    recolog_dao.delete_all_attributed_events(client)
    recolog_dao.delete_all_attributed_recommendations(client)
    recolog_dao.delete_all_unmatched_recommendations(client)

    from_date = datetime.utcnow() - timedelta(hours=10)

    task = AttributionBuilderTask("AttributionBuilderTask", clients=[client])
    global_context = dict()
    client_context = {"last_till_date_empirical": from_date}

    recommended_product = "test product"
    recommended_brand = "test brand"

    catalog_dao.create_product(client, random_product(product_id=recommended_product, brand=recommended_brand))

    product_with_no_display = "no display product"
    product_with_no_display_2 = "no display product 2"

    event1 = random_event(date=from_date)
    recommendation1 = random_recommendation(user_id=event1.user_id, date=event1.date + timedelta(seconds=1),
                                            product_ids=[recommended_product], type=RecommendationType.SIMILAR_PRODUCTS)

    recommendation2 = random_recommendation(user_id=event1.user_id, date=event1.date + timedelta(seconds=2),
                                            product_ids=["not" + recommended_product, product_with_no_display])

    event2 = random_event(user_id=event1.user_id, date=event1.date + timedelta(minutes=1),
                          product_id=recommended_product)

    event3 = random_event(user_id=event1.user_id, date=event1.date + timedelta(minutes=2),
                          product_id=recommended_product, event_type=EventType.PRODUCT_TO_BASKET)

    event4 = random_event(user_id=event1.user_id, date=event1.date + timedelta(minutes=2),
                          product_id=product_with_no_display, event_type=EventType.PRODUCT_TO_WISHLIST)

    event5 = random_event(user_id=event1.user_id, date=event1.date + timedelta(minutes=18),
                          product_id=recommended_product, event_type=EventType.PRODUCT_TO_BASKET)

    event6 = random_event(user_id=event1.user_id, date=event1.date + timedelta(minutes=4),
                          product_id=product_with_no_display_2, event_type=EventType.PRODUCT_TO_WISHLIST)

    recommendation3 = random_recommendation(user_id=event1.user_id, date=event1.date + timedelta(minutes=18),
                                            product_ids=[recommended_product])

    event_dao.create_event(client, event1)
    event_dao.create_event(client, event2)  # Should be matched
    event_dao.create_event(client, event3)  # Should be matched
    event_dao.create_event(client, event4)  # Should be matched
    event_dao.create_event(client, event5)
    event_dao.create_event(client, event6)
    recolog_dao.log_recommendation(client, recommendation1)
    recolog_dao.log_recommendation(client, recommendation2)
    recolog_dao.log_recommendation(client, recommendation3)

    task.process_client(client, global_context, client_context)

    # Checking matched events

    attributed_events = list(recolog_dao.read_attributed_events(client, AttributionType.EMPIRICAL))
    assert attributed_events
    assert len(attributed_events) == 3
    assert len(attributed_events[0].attributed_recommendations) == 1
    assert len(attributed_events[1].attributed_recommendations) == 1
    assert len(attributed_events[2].attributed_recommendations) == 1

    assert len([ae for ae in attributed_events if not ae.has_matched_display]) == 1

    # Checking attributed recommendations

    attributed_recommendations = list(recolog_dao.read_attributed_recommendations(client, AttributionType.EMPIRICAL))
    assert attributed_recommendations
    assert len(attributed_recommendations) == 2
    assert len(attributed_recommendations[0].attributed_events) == 2
    assert len(attributed_recommendations[1].attributed_events) == 1

    # Checking metrics

    for base in (60, 60 * 60, 24 * 60 * 60):
        event_2_ts = (int(event2.date.timestamp()) // base) * base

        metrics = list(metrics_dao.get_metrics(client, "attributed_events", tags=None, base=base,
                                               timestamp_from=event_2_ts,
                                               timestamp_until=event_2_ts + 1))

        assert len(metrics) == 1

        event_3_ts = (int(event3.date.timestamp()) // base) * base

        tags = {
            "event_type": EventType.PRODUCT_TO_BASKET.name.lower(),
            "attribution_type": AttributionType.EMPIRICAL.name.lower(),
            "recommendation_type": RecommendationType.SIMILAR_PRODUCTS.name.lower(),
            "from_zone": "no",
            "from_campaign": "no",
            "product_id": recommended_product,
            "brand": recommended_brand
        }

        metrics = list(metrics_dao.get_metrics(client, "attributed_events", tags=tags, base=base,
                                               timestamp_from=event_3_ts,
                                               timestamp_until=event_3_ts + 1))

        assert len(metrics) == 1

        tags["from_zone"] = "yes"

        metrics = list(metrics_dao.get_metrics(client, "attributed_events", tags=tags, base=base,
                                               timestamp_from=event_3_ts,
                                               timestamp_until=event_3_ts + 1))

        assert not metrics

    # Checking unmatched recommendations

    unmatched_recommendations = list(recolog_dao.read_unmatched_recommendations(client, AttributionType.EMPIRICAL))
    assert len(unmatched_recommendations) == 1
    assert recommendation3 in unmatched_recommendations
