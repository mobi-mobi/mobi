import random

from locust import HttpLocust, TaskSet, task, between
from random import choice, randrange
from typing import Generator
from threading import Lock

from mobi.core import Product
from mobi.mock import random_product, random_id


def random_products(num: int = 10) -> Generator[Product, None, None]:
    for _ in range(num):
        yield random_product()


CR_P_LOCK = Lock()
PRODUCTS = []


class WebsiteTasks(TaskSet):
    def on_start(self):
        with CR_P_LOCK:
            if not PRODUCTS:
                _products = list(random_products(10))

                self.client.delete("/event/all?token=testtoken")
                for p in _products:
                    self.client.post("/event/product?token=testtoken", json=p.asdict())

                PRODUCTS.extend(_products)

    @task
    def event(self):
        user_id = random_id()
        product = random.choice(PRODUCTS)

        body = {
            "user_id": user_id,
            "product_id": product.product_id
        }

        self.client.post("/event/event?token=testtoken", json=body)


USERS = []


class RecommendationTasks(TaskSet):
    def on_start(self):
        global USERS
        USERS = [randrange(1000000) for _ in range(1000)]

    @task
    def personal_recommendations(self):
        global USERS
        user_id = choice(USERS)
        self.client.get(f"/recommendation/user/{user_id}/recommendations?token=B6C7189A57E29AC6B782E36647A73")


class WebsiteUser(HttpLocust):
    task_set = RecommendationTasks
    wait_time = between(1, 1)
