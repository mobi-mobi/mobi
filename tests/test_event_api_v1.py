import json
import pytest
from flask import testing
from mobi.api.event_api import EventApi
from mobi.config.system.api.event_api import get_event_api_config
from mobi.core.models import Product
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao
from mobi.dao.cache import cache_key
from mobi.utils.tests import random_product, random_string


class TestEventApiClient(testing.FlaskClient):
    MOBI_TOKEN = "testtoken"

    def open(self, *args, **kwargs):
        if self.MOBI_TOKEN is not None:
            headers = kwargs.get('headers', {})
            headers['Auth-Token'] = self.MOBI_TOKEN
            kwargs['headers'] = headers
        return super().open(*args, **kwargs)


class TestEventApiClientWithHashedToken(TestEventApiClient):
    MOBI_TOKEN = "testtokenhashed"


class TestEventApiClientWithNoToken(TestEventApiClient):
    MOBI_TOKEN = None


class TestEventApiClientWithExpiredToken(TestEventApiClient):
    MOBI_TOKEN = "expiredtesttoken"


class TestEventApiClientWithWrongToken(TestEventApiClient):
    MOBI_TOKEN = "__testnonexistingtoken__"


class TestEventApiClientWithProdToken(TestEventApiClient):
    MOBI_TOKEN = "testtokenprod"


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def event_api_client():
    api = EventApi()
    api.app.test_client_class = TestEventApiClient

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_expired_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithExpiredToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_hashed_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithHashedToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_no_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithNoToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_wrong_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithWrongToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_prod_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithProdToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def event_dao():
    return get_event_dao(get_event_api_config())


@pytest.fixture(scope="session")
def catalog_dao():
    return get_catalog_dao(get_event_api_config())


@pytest.fixture(scope="session")
def cache_dao():
    return get_cache_dao(get_event_api_config())


@pytest.fixture
def random_non_existing_product(client, catalog_dao):
    product = random_product()
    try:
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass

    yield product

    try:
        # Who knows, maybe it was already deleted in the test body
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass


@pytest.fixture
def random_existing_product(client, catalog_dao, random_non_existing_product):
    catalog_dao.create_product(client, random_non_existing_product)
    yield random_non_existing_product

    try:
        # Who knows, maybe it was already deleted in the test body
        catalog_dao.delete_product(client, random_non_existing_product.product_id)
    except Exception:
        pass


def test_event_api_init(event_api_client):
    pass


def test_product(random_non_existing_product):
    new_product = Product.from_bytes(random_non_existing_product.as_bytes())
    assert random_non_existing_product == new_product


def test_status(event_api_client):
    rv = event_api_client.get("/status")
    assert rv.data == b'{"status": "OK"}'


def test_empty_request_body(event_api_client):
    rv = event_api_client.post("/event", data="{}")
    assert b'Can not parse request body' not in rv.data


def test_wrong_request_body(event_api_client):
    rv = event_api_client.post("/event", data=None)
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 400' in rv.data
    assert b'Request data is empty' in rv.data

    rv = event_api_client.post("/event", data="{{")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 400' in rv.data
    assert b'Request body is not a valid JSON object' in rv.data
    assert b'400 Bad Request' in rv.data


def test_status_with_no_token(client_with_no_token):
    rv = client_with_no_token.get("/status")
    assert rv.data == b'{"status": "OK"}'


def test_no_tokens(client_with_no_token):
    rv = client_with_no_token.get("/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 401' in rv.data
    assert b'No token provided' in rv.data
    assert b'401 Unauthorized' in rv.data


def test_wrong_token(client_with_wrong_token):
    rv = client_with_wrong_token.get("/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data
    assert b'403 Forbidden' in rv.data


def test_wrong_token_in_uri(client_with_no_token):
    rv = client_with_no_token.get("/some/url?token=__testnonexistingtoken__")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data
    assert b'403 Forbidden' in rv.data


def test_ok_token_in_header_but_wrong_token_in_uri(event_api_client):
    rv = event_api_client.get("/some/url?token=__testnonexistingtoken__")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data
    assert b'403 Forbidden' in rv.data


def test_with_expired_token(client_with_expired_token):
    rv = client_with_expired_token.get("/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is expired' in rv.data
    assert b'403 Forbidden' in rv.data


def test_with_expired_token_in_uri(event_api_client):
    rv = event_api_client.get("/some/url?token=expiredtesttoken")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is expired' in rv.data
    assert b'403 Forbidden' in rv.data


def test_wrong_url(event_api_client):
    rv = event_api_client.get("/somewrongurl")
    assert b'"status": "ERROR"' in rv.data
    assert b'The requested URL was not found on the server' in rv.data


def v1_product_dict(product: Product):
    result = EventApi.v1_product_json_dict(product)
    del result["version"]
    return result


def test_create_product(event_api_client):
    product_dict = v1_product_dict(random_product())

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    response = json.loads(event_api_client.get(f"/product/{product_dict['id']}").data)
    assert response["status"] == "OK"

    received_data = response["data"]
    for k in product_dict.keys():
        assert product_dict[k] == received_data[k]


def test_create_product_with_hashed_token(client_with_hashed_token):
    product = random_product()
    product_dict = v1_product_dict(product)

    rv = client_with_hashed_token.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    response = json.loads(client_with_hashed_token.get(f"/product/{product.product_id}").data)
    assert response["status"] == "OK"

    received_data = response["data"]
    for k in product_dict.keys():
        assert product_dict[k] == received_data[k]


def test_create_enriched_product(event_api_client):
    product = random_product(title=random_string() + " eye balm")
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    response = json.loads(event_api_client.get(f"/product/{product.product_id}").data)
    assert response["status"] == "OK"

    received_data = response["data"]
    for k in product_dict.keys():
        if k != "enriched_tags":
            assert product_dict[k] == received_data[k]
        else:
            assert set(received_data[k]) == set(product_dict[k]).union(["eyes", "eye_balm"])


def test_create_product_without_categories(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict.pop("categories")

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Categories set is empty or missing' in rv.data


def test_create_product_with_empty_categories_list(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict["categories"] = []

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Categories set is empty or missing' in rv.data


def test_create_product_with_invalid_category(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict["categories"] = "bad data"

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Wrong product categories format' in rv.data

    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict["categories"] = [1]

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Wrong product categories format' in rv.data


def test_create_product_with_multiple_categories(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict["categories"] = ["1", "2"]

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'


def test_create_product_with_repeated_categories(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict["categories"] = ["1", "1"]

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Product has duplicate categories' in rv.data


def test_create_product_no_id(event_api_client):
    rv = event_api_client.post("/product", data="{\"id\": \"\"}")
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Product id missed' in rv.data

    rv = event_api_client.post("/product", data="{}")
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Product id missed' in rv.data


def test_create_product_no_title(event_api_client):
    rv = event_api_client.post("/product", data="{\"id\": \"testproductid\"}")
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Product title missed' in rv.data


def test_create_product_wrong_data(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict["id"] = True

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Product id is not a string' in rv.data

    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict["title"] = True
    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Internal Server Error' in rv.data
    assert b'Product title is not a string' in rv.data


def test_create_update_read_product(client, event_api_client, cache_dao):
    product = random_product()
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'
    response = json.loads(event_api_client.get(f"/product/{product.product_id}").data)
    assert response["status"] == "OK"

    data = response["data"]
    assert data["id"] == product.product_id
    for property in ("brand", "color", "categories", "title", "in_stock", "active", "is_new", "is_exclusive"):
        assert product_dict[property] == data[property]

    cache_dao.delete(cache_key("product", client=client, product_id=product.product_id), noreply=False, retries=10)

    product.brand = "New " + product.brand
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'
    response = json.loads(event_api_client.get(f"/product/{product.product_id}").data)
    assert response["status"] == "OK"

    data = response["data"]
    assert data["id"] == product.product_id
    for property in ("brand", "color", "categories", "title", "in_stock", "active", "is_new", "is_exclusive"):
        assert product_dict[property] == data[property]


def test_create_product_with_description(event_api_client):
    product = random_product(description=random_string())
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    response = json.loads(event_api_client.get(f"/product/{product.product_id}").data)
    assert response["status"] == "OK"

    for k in product_dict.keys():
        assert product_dict[k] == response["data"][k]


def test_batch_create_product(event_api_client):
    rv = event_api_client.post("/batch/product", data=json.dumps([v1_product_dict(random_product()),
                                                                  v1_product_dict(random_product())]))
    assert rv.data == b'{"status": "OK"}'


def test_create_product_unknown_parameter(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)
    product_dict["wrong_parameter"] = "Some value"
    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Unknown parameter wrong_parameter' in rv.data

    data = [v1_product_dict(random_product()), v1_product_dict(random_product())]
    data[0]["category"] = "Some Value"

    rv = event_api_client.post("/batch/product", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'Unknown parameter category' in rv.data


def test_batch_create_wrong_product(event_api_client):
    data = [v1_product_dict(random_product()), v1_product_dict(random_product())]
    data[0]["id"] = random_string()
    data[1]["id"] = random_string()
    data[1].pop("title")

    rv = event_api_client.post("/batch/product", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'Product title missed' in rv.data

    rv = event_api_client.get(f"/product/{data[0]['id']}")
    assert b'Internal Server Error' in rv.data
    assert b'Can not find product' in rv.data

    rv = event_api_client.get(f"/product/{data[1]['id']}")
    assert b'Internal Server Error' in rv.data
    assert b'Can not find product' in rv.data


def test_read_unknown_product(event_api_client):
    rv = event_api_client.get("/product/testwrongproductid")
    assert b'Internal Server Error' in rv.data
    assert b'Can not find product' in rv.data


def test_deactivate_product(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'
    rv = event_api_client.put(f"/product/{product.product_id}/deactivate")
    assert rv.data == b'{"status": "OK"}'
    rv = event_api_client.get(f"/product/{product.product_id}")
    assert b'"active": false' in rv.data


def test_deactivate_product_twice(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'
    rv = event_api_client.put(f"/product/{product.product_id}/deactivate")
    assert rv.data == b'{"status": "OK"}'
    rv = event_api_client.get(f"/product/{product.product_id}")
    assert b'"active": false' in rv.data
    rv = event_api_client.put(f"/product/{product.product_id}/deactivate")
    assert rv.data == b'{"status": "OK"}'
    rv = event_api_client.get(f"/product/{product.product_id}")
    assert b'"active": false' in rv.data


def test_deactivate_unexisting_product(event_api_client):
    rv = event_api_client.put("/product/wrongtestproductid/deactivate")
    assert b'"status": "ERROR"' in rv.data
    assert b'Product wrongtestproductid does not exist' in rv.data


def test_create_event(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    rv = event_api_client.post("/event", data="{\"user_id\": \"userid\", \"product_id\": \""
                                              + product.product_id + "\", \"referer\": \"UNKNOWN\"}")
    assert rv.data == b'{"status": "OK"}'


def test_create_event_wrong_data(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    rv = event_api_client.post("/event", data="{\"user_id\": \"userid\"}")
    assert b'"status": "ERROR"' in rv.data
    assert b'Product id missed' in rv.data
    rv = event_api_client.post("/event", data="{\"product_id\": \"" + product.product_id + "\"}")
    assert b'"status": "ERROR"' in rv.data
    assert b'User id missed' in rv.data

    rv = event_api_client.post("/event", data="{\"user_id\": \"userid\", "
                                              "\"product_id\": \"" + product.product_id + "\", "
                                              "\"event_type\": \"WRONGEVENTTYPE\"}")
    assert rv.data == b'{"status": "OK"}'


def test_create_event_custom_datetime(client, event_api_client, event_dao):
    product = random_product()
    product_dict = v1_product_dict(product)

    data = {
        "user_id": "userid",
        "product_id": product.product_id,
        "event_type": "PRODUCT_VIEW",
        "datetime": "2001-02-03 04:05:06"
    }

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'
    rv = event_api_client.post("/event", data=json.dumps(data))
    assert rv.data == b'{"status": "OK"}'

    for event in event_dao.read_events(client):
        if event.product_id == product.product_id:
            assert event.date.strftime("%Y-%m-%d %H:%M:%S") == "2001-02-03 04:05:06"
            return

    assert False, "Product is not found"


def test_create_event_wrong_datetime_format(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)

    data = {
        "user_id": "userid",
        "product_id": product.product_id,
        "event_type": "PRODUCT_VIEW",
    }

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    for dt in ["2001-02-03", "2001-02-03 04:05", "2001-02-03 04:05:61", "04:05:06"]:
        data["datetime"] = dt
        rv = event_api_client.post("/event", data=json.dumps(data))
        assert b'"status": "ERROR"' in rv.data
        assert b'Can not parse datetime' in rv.data


def test_create_event_unknown_parameter(event_api_client):
    product = random_product()
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    data = {
        "user_id": random_string(),
        "product_id": product.product_id,
        "wrong_parameter": random_string()
    }

    rv = event_api_client.post("/event", data=json.dumps(data))
    assert b'Unknown parameter wrong_parameter' in rv.data


def test_create_event_for_non_existing_product(event_api_client, random_non_existing_product):
    data = {
        "user_id": random_string(),
        "product_id": random_non_existing_product.product_id,
        "event_type": "PRODUCT_VIEW"
    }

    rv = event_api_client.post("/event", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'does not exist' in rv.data


def test_delete_for_prod_token(client_with_prod_token):
    rv = client_with_prod_token.delete("/products")
    assert b'Can not perform delete operation for a non-test token' in rv.data
    assert b'"code": 500' in rv.data

    rv = client_with_prod_token.delete("/events")
    assert b'Can not perform delete operation for a non-test token' in rv.data
    assert b'"code": 500' in rv.data

    rv = client_with_prod_token.delete("/all")
    assert b'Can not perform delete operation for a non-test token' in rv.data
    assert b'"code": 500' in rv.data


def test_delete_products(event_api_client, random_existing_product):
    product = random_product()
    product_dict = v1_product_dict(product)

    rv = event_api_client.post("/product", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    rv = event_api_client.delete("/products")
    assert rv.data == b'{"status": "OK"}'

    rv = event_api_client.get(f"/product/{product.product_id}")
    assert b'{"code": 500' in rv.data
    assert b'500 Internal Server Error' in rv.data
    assert b'Can not find product' in rv.data
