import pytest

from datetime import datetime, timedelta
from typing import Generator, Tuple

from mobi.config.system.web.console import get_console_web_config
from mobi.dao import get_auth_dao, MobiDaoException
from mobi.dao.auth import MobiAuthDao
from mobi.utils.tests import random_string


@pytest.fixture(scope="session")
def auth_dao() -> Generator[MobiAuthDao, None, None]:
    yield get_auth_dao(get_console_web_config())


@pytest.fixture(scope="function")
def random_non_existing_user() -> Generator[Tuple[str, str], None, None]:
    email = random_string(10) + "@" + random_string(5) + ".com"
    name = "Random Name " + random_string(10)

    try:
        auth_dao.delete_user(email)
    except Exception:
        pass

    yield email, name

    try:
        auth_dao.delete_user(email)
    except Exception:
        pass


@pytest.fixture(scope="function")
def random_existing_user(random_non_existing_user, auth_dao) -> Generator[Tuple[str, str], None, None]:
    email, name = random_non_existing_user
    auth_dao.create_user(email, name)

    yield email, name

    try:
        auth_dao.delete_user(email)
    except Exception:
        pass


@pytest.fixture(scope="function")
def random_user_token(random_existing_user, auth_dao):
    email, name = random_existing_user
    token = auth_dao.create_session_token(email, datetime.utcnow() + timedelta(hours=1))
    yield email, name, token

    try:
        auth_dao.delete_session_token(token)
    except Exception:
        pass

    try:
        auth_dao.delete_user(email)
    except Exception:
        pass


def test_get_dao(auth_dao):
    pass


def test_create_user(auth_dao):
    email = f"{random_string(20)}@email.com"
    name = "Test Name"

    try:
        auth_dao.delete_user(email)
    except Exception:
        pass

    auth_dao.create_user(email, name)
    user = auth_dao.read_user(email)
    assert user["email"] == email
    assert user["name"] == name

    try:
        auth_dao.delete_user(email)
    except Exception:
        pass


def test_create_user_twice(random_existing_user, auth_dao):
    email, name = random_existing_user
    name2 = "Test Name 2"

    with pytest.raises(MobiDaoException):
        auth_dao.create_user(email, name2)

    user = auth_dao.read_user(email)
    assert user["email"] == email
    assert user["name"] == name
    assert user["name"] != name2


def test_create_user_with_incorrect_data(random_non_existing_user, auth_dao):
    email, name = random_non_existing_user

    wrong_email = "wrong_email"
    wrong_name = 100

    with pytest.raises(MobiDaoException):
        auth_dao.create_user(wrong_email, name)

    with pytest.raises(MobiDaoException):
        auth_dao.create_user(wrong_email, wrong_name)

    with pytest.raises(MobiDaoException):
        auth_dao.create_user("", name)

    with pytest.raises(MobiDaoException):
        auth_dao.create_user(wrong_email, "")

    with pytest.raises(MobiDaoException):
        auth_dao.create_user("", "")


def test_delete_user(random_existing_user, auth_dao):
    email, name = random_existing_user

    auth_dao.read_user(email)
    auth_dao.delete_user(email)

    with pytest.raises(MobiDaoException):
        auth_dao.read_user(email)


def test_add_user_to_client(random_existing_user, auth_dao):
    email, name = random_existing_user
    client = "client"

    user = auth_dao.read_user(email)
    assert client not in user["clients"]

    auth_dao.add_user_to_client(email, client)
    user = auth_dao.read_user(email)

    assert client in user["clients"]


def test_add_user_to_client_wrong_data(random_existing_user, auth_dao):
    email, name = random_existing_user
    client = "client"

    user = auth_dao.read_user(email)
    assert client not in user["clients"]

    with pytest.raises(MobiDaoException):
        auth_dao.add_user_to_client("", client)

    with pytest.raises(MobiDaoException):
        auth_dao.add_user_to_client(email, "")


def test_update_user_password(random_existing_user, auth_dao):
    email, name = random_existing_user
    password = "password"

    user = auth_dao.read_user(email)
    assert user.get("password") is None
    assert user.get("salt") is None

    auth_dao.update_user_password(email, password)
    user = auth_dao.read_user(email)
    assert user.get("password") is not None
    assert user.get("salt") is not None


def test_update_user_wrong_password(random_existing_user, auth_dao):
    email, name = random_existing_user
    wrong_password = 123

    with pytest.raises(MobiDaoException):
        auth_dao.update_user_password(email, "")

    with pytest.raises(MobiDaoException):
        auth_dao.update_user_password(email, wrong_password)


def test_check_user_exists(random_existing_user, auth_dao):
    email, name = random_existing_user
    assert auth_dao.check_user_exists(email)


def test_check_user_doesnt_exist(random_non_existing_user, auth_dao):
    email, name = random_non_existing_user
    assert not auth_dao.check_user_exists(email)


def test_check_user_password(random_existing_user, auth_dao):
    email, name = random_existing_user
    password = "123password123"

    to_check = [
        "",
        "password",
        "123password",
        "password123",
        "123password123 ",
        " 123password123"
    ]

    auth_dao.update_user_password(email, password)
    user = auth_dao.read_user(email)
    assert user.get("password") is not None
    assert user.get("salt") is not None

    assert auth_dao.check_user_password(email, password)

    for wrong_password in to_check:
        assert not auth_dao.check_user_password(email, wrong_password)


def test_create_session_token(random_existing_user, auth_dao):
    email, _ = random_existing_user

    auth_dao.create_session_token(email, datetime.utcnow() + timedelta(hours=1))


def test_create_expired_session_token(random_existing_user, auth_dao):
    email, _ = random_existing_user

    with pytest.raises(MobiDaoException):
        auth_dao.create_session_token(email, datetime.utcnow() - timedelta(hours=1))


def test_check_session_token(random_user_token, auth_dao):
    _, _, token = random_user_token

    to_check = [
        " " + token,
        token + " ",
        " "
    ]

    assert auth_dao.check_session_token(token)

    with pytest.raises(MobiDaoException):
        auth_dao.check_session_token("")

    with pytest.raises(MobiDaoException):
        auth_dao.check_session_token(123)

    for wrong_token in to_check:
        assert not auth_dao.check_session_token(wrong_token)


def test_renew_session_token(random_user_token, auth_dao):
    _, _, token = random_user_token

    auth_dao.renew_session_token(token, datetime.utcnow() + timedelta(hours=1))

    with pytest.raises(MobiDaoException):
        auth_dao.renew_session_token(token, datetime.utcnow() - timedelta(hours=1))


def test_renew_non_existing_session(auth_dao):
    token = "NON EXISTING TOKEN"

    with pytest.raises(MobiDaoException):
        auth_dao.renew_session_token(token, datetime.utcnow() + timedelta(hours=1))
