import pickle
import pytest

from datetime import datetime
from typing import Generator

from mobi.config.system.api.recommendation_api import get_recommendation_api_config
from mobi.core.models import ModelDump, ModelDumpInfo, ModelStatus
from mobi.dao.object import MobiModelDao
from mobi.dao import get_model_dao, MobiDaoException
from mobi.utils.tests import random_string


@pytest.fixture(scope="session")
def models_dao() -> Generator[MobiModelDao, None, None]:
    yield get_model_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def client():
    yield "testclient"


def test_get_dao(models_dao):
    pass


def test_load_save_delete_model_dump(models_dao, client):
    model_id = random_string()
    model_type = "test_model_type"
    model_date = datetime.utcnow()
    model_status = ModelStatus.NOT_READY
    model_binary = pickle.dumps({"TEST_KEY": "TEST_VAL", 11: 22})

    dump = ModelDump(
        ModelDumpInfo(model_id, model_type, model_date, model_status),
        model_binary
    )
    models_dao.save_model_dump(client, dump)

    loaded_dump = models_dao.read_model_dump(client, dump.info.model_id)

    assert dump.info.model_id == loaded_dump.info.model_id
    assert dump.info.model_type == loaded_dump.info.model_type
    assert dump.info.date.isoformat()[0:-3] == loaded_dump.info.date.isoformat()[0:-3]
    assert dump.info.status == loaded_dump.info.status
    assert dump.binary == loaded_dump.binary

    assert loaded_dump.info.model_id == model_id
    assert loaded_dump.info.model_type == model_type
    assert loaded_dump.info.date.isoformat()[0:-3] == model_date.isoformat()[0:-3]
    assert loaded_dump.info.status == model_status
    assert loaded_dump.binary == model_binary

    assert pickle.loads(loaded_dump.binary).get("TEST_KEY") == "TEST_VAL"
    assert pickle.loads(loaded_dump.binary).get(11) == 22
    assert pickle.loads(loaded_dump.binary).get("__NON_EXISTING__") is None

    models_dao.delete_model_dump(client, dump.info.model_id)

    with pytest.raises(ValueError):
        models_dao.read_model_dump(client, dump.info.model_id)

    with pytest.raises(MobiDaoException):
        models_dao.read_model_dump_binary(client, dump.info.model_id)
