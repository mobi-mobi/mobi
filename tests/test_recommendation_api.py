import json
import pytest
import werkzeug

from datetime import datetime, timedelta
from flask import testing
from typing import List, Tuple

from mobi.api.recommendation_api import RecommendationApi
from mobi.config.client import get_client_config
from mobi.config.system.api.recommendation_api import get_recommendation_api_config
from mobi.core.models import Event, EventPlatform, EventType, ModelStatus, Product, RecommendationSource, RecommendationType
from mobi.core.models.zone import CampaignStatus, ZoneSettings, ZoneType, ZoneExtraProductsPosition
from mobi.core.population import Population
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao, get_model_dao, get_recolog_dao, get_search_dao,\
    get_settings_dao
from mobi.dao.cache import MobiCache
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.cache import UserHistoryCookie
from mobi.dao.event import MobiEventDao
from mobi.dao.object import MobiModelDao
from mobi.dao.recolog import MobiRecologDao
from mobi.dao.search import MobiSearchDao
from mobi.dao.settings import MobiSettingsDao
from mobi.ml.model.embedding.impl.static import StaticEmbeddingModel
from mobi.ml.model.embedding.impl.factories.deepwalk import build_basket_links
from mobi.service.task.impl.cache_builder_task import CacheBuilderTask
from mobi.utils.tests import random_campaign, random_event, random_product, random_string, random_zone


class TestRecommendationApiClient(testing.FlaskClient):
    MOBI_TOKEN = "testtoken"

    def open(self, *args, **kwargs):
        if self.MOBI_TOKEN is not None:
            headers = kwargs.get('headers', {})
            headers['Authorization'] = "Bearer " + self.MOBI_TOKEN
            kwargs['headers'] = headers
        return super().open(*args, **kwargs)


class TestRecommendationApiClientWithNoToken(TestRecommendationApiClient):
    MOBI_TOKEN = None


class TestRecommendationApiClientWithExpiredToken(TestRecommendationApiClient):
    MOBI_TOKEN = "expiredtesttoken"


class TestRecommendationApiClientWithWrongToken(TestRecommendationApiClient):
    MOBI_TOKEN = "__testnonexistingtoken__"


@pytest.fixture(scope="session")
def client() -> str:
    yield "testclient"


@pytest.fixture(scope="session")
def recommendation_api() -> RecommendationApi:
    return RecommendationApi()


@pytest.fixture(scope="session")
def recommendation_api_client(recommendation_api):
    recommendation_api.app.test_client_class = TestRecommendationApiClient

    client = recommendation_api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_expired_token(recommendation_api):
    recommendation_api.app.test_client_class = TestRecommendationApiClientWithExpiredToken
    client = recommendation_api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_no_token(recommendation_api):
    recommendation_api.app.test_client_class = TestRecommendationApiClientWithNoToken
    client = recommendation_api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_wrong_token(recommendation_api):
    recommendation_api.app.test_client_class = TestRecommendationApiClientWithWrongToken

    client = recommendation_api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def catalog_dao() -> MobiCatalogDao:
    yield get_catalog_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def cache_dao() -> MobiCache:
    yield get_cache_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def event_dao() -> MobiEventDao:
    yield get_event_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def models_dao() -> MobiModelDao:
    yield get_model_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def recolog_dao() -> MobiRecologDao:
    yield get_recolog_dao(get_recommendation_api_config())


@pytest.fixture
def search_dao(client) -> MobiSearchDao:
    dao = get_search_dao(get_recommendation_api_config())
    dao.delete_index(client)
    dao.create_index(client)
    yield dao
    dao.delete_index(client)


@pytest.fixture(scope="session")
def settings_dao() -> MobiSettingsDao:
    yield get_settings_dao(get_recommendation_api_config())


@pytest.fixture
def random_non_existing_product(catalog_dao, client) -> Product:
    product = random_product()
    try:
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass

    yield product

    try:
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass


@pytest.fixture
def random_existing_product(catalog_dao, client, random_non_existing_product) -> Product:
    catalog_dao.create_product(client, random_non_existing_product)

    yield random_non_existing_product

    try:
        catalog_dao.delete_product(client, random_non_existing_product.product_id)
    except Exception:
        pass


def get_random_user_id_with_population(client: str, population: Population, iterations: int = 500):
    population_setup = get_client_config(client).population_setup()
    for _ in range(iterations):
        user_id = random_string()
        if population_setup.population(user_id) == population:
            return user_id
    raise Exception(f"Can not find proper user id for population {population.name}")


@pytest.fixture
def reference_random_user_id(client):
    yield get_random_user_id_with_population(client, Population.REFERENCE)


@pytest.fixture
def target_random_user_id(client):
    yield get_random_user_id_with_population(client, Population.TARGET)


@pytest.fixture
def static_embedding_model_and_products(event_dao, recommendation_api, client, catalog_dao, models_dao) \
        -> Tuple[StaticEmbeddingModel, List[Product]]:
    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)
    models_dao.delete_all_model_dumps(client)

    product_1 = random_product()
    product_1.brand = "brand_1"
    product_1.categories = ["category_1", "category_3"]
    catalog_dao.create_product(client, product_1)
    product_2 = random_product()
    product_2.brand = "brand_2"
    product_2.categories = ["category_2"]
    catalog_dao.create_product(client, product_2)
    product_3 = random_product()
    product_3.brand = "brand_1"
    product_3.categories = ["category_3"]
    catalog_dao.create_product(client, product_3)
    product_4 = random_product()
    product_4.brand = "brand_4"
    product_4.categories = ["category_4"]
    catalog_dao.create_product(client, product_4)
    product_5 = random_product()
    product_5.brand = "brand_5"
    product_5.categories = ["category_4", "category_5"]
    catalog_dao.create_product(client, product_5)

    user_id = random_string()
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_2.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)

    user_id = random_string()
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_2.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)

    user_id = random_string()
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_3.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_3.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)

    model = StaticEmbeddingModel()
    non_recommendable_products = [random_string() + "#ne" for _ in range(1000)]
    non_recommendable_products.append(product_4.product_id)
    model.set_non_recommendable_product_ids(non_recommendable_products)
    non_recommendable_brands = [random_string() + "#ne" for _ in range(100)]
    non_recommendable_brands.append(product_5.brand)
    model.set_non_recommendable_brands(non_recommendable_brands)
    model.status = ModelStatus.READY
    model.set_product_id_embeddings([(product.product_id, [float(i)])
                                     for i, product in enumerate([product_1, product_2, product_3,
                                                                  product_4, product_5])])
    model.compile([product_1, product_2, product_3, product_4, product_5])
    model.set_basket_links(build_basket_links(
        event_dao,
        client,
        from_date=datetime.utcnow() - timedelta(hours=1),
        till_date=datetime.utcnow()
    ))

    models_dao.save_model_dump(client, model.dump())
    recommendation_api.models_updated_datetime[client] = datetime.utcnow() - timedelta(days=1)

    yield model, [product_1, product_2, product_3]

    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)
    models_dao.delete_all_model_dumps(client)


@pytest.fixture
def static_embedding_model(static_embedding_model_and_products) -> StaticEmbeddingModel:
    model, _ = static_embedding_model_and_products
    yield model


def test_recommendation_api_init(client):
    pass


def test_recommendation_status(recommendation_api_client):
    rv = recommendation_api_client.get("/v2/recommendation/status")
    assert rv.data == b'{"status": "OK"}'


def test_search_status(recommendation_api_client):
    rv = recommendation_api_client.get("/v2/search/status")
    assert rv.data == b'{"status": "OK"}'


def test_no_tokens(client_with_no_token):
    rv = client_with_no_token.get("/v2/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 401' in rv.data
    assert b'No token provided' in rv.data


def test_wrong_token(client_with_wrong_token):
    rv = client_with_wrong_token.get("/v2/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data


def test_wrong_token_in_uri(client_with_no_token):
    rv = client_with_no_token.get("/v2/some/url?token=__testnonexistingtoken__")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data


def test_with_expired_token(client_with_expired_token):
    rv = client_with_expired_token.get("/v2/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is expired' in rv.data


def test_wrong_url(recommendation_api_client, static_embedding_model):
    rv = recommendation_api_client.get("/v2/somewrongurl")
    assert b'"status": "ERROR"' in rv.data
    assert b'The requested URL was not found on the server' in rv.data


def test_get_model_meta(recommendation_api_client, static_embedding_model):
    rv = recommendation_api_client.get("/v2/recommendation/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' in rv.data
    assert b'"model_type": "' + bytes(static_embedding_model.model_type, encoding="utf-8") + b'"' in rv.data
    assert b'"status": "ready"' in rv.data
    assert b'"product_id_embeddings_map_size": 5' in rv.data
    assert b'"basket_links_size": 3' in rv.data
    assert b'"knn_tree_size": 3' in rv.data
    assert b'"brand_isolated_knn_tree_size": 3' in rv.data
    assert b'"non_recommendable_products": 1001' in rv.data
    assert b'"non_recommendable_brands": 101' in rv.data


def test_model_reload_after_2h_1s(recommendation_api, recommendation_api_client, client, catalog_dao,
                                  models_dao, static_embedding_model):
    rv = recommendation_api_client.get("/v2/recommendation/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' in rv.data
    old_update_time = recommendation_api.models_updated_datetime[client]

    recommendation_api.models_updated_datetime[client] = \
        datetime.utcnow() - timedelta(hours=2, seconds=1)

    new_model = StaticEmbeddingModel()
    new_model.status = ModelStatus.READY
    new_product = random_product()
    catalog_dao.create_product(client, new_product)
    new_model.set_product_id_embeddings([(new_product.product_id, [0.0])])

    models_dao.save_model_dump(client, new_model.dump())

    rv = recommendation_api_client.get("/v2/recommendation/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(new_model.model_id, encoding="utf-8") + b'"' in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' not in rv.data
    recommendation_api.models_updated_datetime[client] = old_update_time
    recommendation_api.models[client] = static_embedding_model


def test_model_reload_after_1h(recommendation_api, recommendation_api_client, client,  catalog_dao,
                               models_dao, static_embedding_model):
    rv = recommendation_api_client.get("/v2/recommendation/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' in rv.data
    old_update_time = recommendation_api.models_updated_datetime[client]

    recommendation_api.models_updated_datetime[client] = \
        datetime.utcnow() - timedelta(hours=1)

    new_model = StaticEmbeddingModel()
    new_model.status = ModelStatus.READY
    new_product = random_product()
    catalog_dao.create_product(client, new_product)
    new_model.set_product_id_embeddings([(new_product.product_id, [0.0])])

    models_dao.save_model_dump(client, new_model.dump())

    rv = recommendation_api_client.get("/v2/recommendation/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(new_model.model_id, encoding="utf-8") + b'"' not in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' in rv.data
    recommendation_api.models_updated_datetime[client] = old_update_time
    recommendation_api.models[client] = static_embedding_model


def test_recently_viewed(client, target_random_user_id, cache_dao, catalog_dao, recommendation_api_client,
                         static_embedding_model):
    user_id = target_random_user_id
    product = random_product()
    catalog_dao.create_product(client, product)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/recently_viewed?user_id={user_id}")
    assert b'"status": "OK"' in rv.data
    assert b'"data": []' in rv.data

    event = Event(
        user_id=user_id,
        user_population=Population.TARGET,
        product_id=product.product_id,
        product_version=1,
        event_type=EventType.PRODUCT_VIEW,
        event_platform=EventPlatform.OTHER,
        date=datetime.utcnow()
    )

    history = UserHistoryCookie([event])
    cache_dao.set_user_history(client, user_id, history, noreply=False)
    rv = recommendation_api_client.get(f"/v2/recommendation/products/recently_viewed?user_id={user_id}")
    assert b'"status": "OK"' in rv.data
    assert bytes(product.product_id, encoding="utf-8") in rv.data


def test_reference_population_is_denied(recommendation_api_client, reference_random_user_id, static_embedding_model):
    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={reference_random_user_id}"
                                       f"&product_id=1")
    assert b'"status": "DENIED"' in rv.data

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={reference_random_user_id}"
                                       f"&product_id=1")
    assert b'"status": "DENIED"' in rv.data


def test_categories_filters_similar_products(client, recommendation_api_client, target_random_user_id,
                                             static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products
    user_id = target_random_user_id

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()

    cache_builder.process_client(client, global_context, client_context)

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=unknown&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=unknown&categories=unknown&categories=unknown42&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_1&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_1&categories=category_3&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 1
    assert result["data"][0]["product_id"] == product_3.product_id

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_3&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 1
    assert result["data"][0]["product_id"] == product_3.product_id

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_2&categories=category_3&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"]])

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_2&categories=category_3&categories=category_4&categories=category_4"
                             f"&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"]])


def test_categories_filters_personalized_recommendations(client, event_dao, recommendation_api_client,
                                                         static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products
    user_id = get_random_user_id_with_population(client, Population.TARGET)

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()

    cache_builder.process_client(client, global_context, client_context)

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"],
                      result["data"][2]["product_id"]])

    # ---

    user_id = get_random_user_id_with_population(client, Population.TARGET)

    event_dao.create_event(client, random_event(
        user_id=user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_1.product_id
    ))

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"],
                      result["data"][2]["product_id"]])

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_1")
                        .data.decode("utf-8"))
    assert len(result["data"]) == 1
    assert result["data"][0]["product_id"] == product_1.product_id

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_3")
                        .data.decode("utf-8"))
    assert len(result["data"]) == 2
    assert sorted([product_1.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"]])

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_3&categories=category_1")
                        .data.decode("utf-8"))
    assert len(result["data"]) == 2
    assert sorted([product_1.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"]])

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_3&categories=category_1&categories=category_2")
                        .data.decode("utf-8"))
    assert len(result["data"]) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"],
                      result["data"][2]["product_id"]])


def test_brand_categories_filters_similar_products(client, recommendation_api_client, target_random_user_id,
                                                   static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products
    user_id = target_random_user_id

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()

    cache_builder.process_client(client, global_context, client_context)

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=unknown&brands=unknown&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=unknown&brands=brand_1&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_1&brands=unknown&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_1&brands=brand_1&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_3&brands=brand_1&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 1
    assert result["data"][0]["product_id"] == product_3.product_id

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_1&categories=category_2&categories=category_3"
                             f"&brands=brand_1&brands=brand_2&brands=brand_3&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"]])

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/similar?user_id={user_id}&product_id={product_1.product_id}"
                             f"&categories=category_1&categories=category_2&categories=category_3&categories=unknown"
                             f"&brands=brand_1&brands=brand_2&brands=brand_3&brands=unknown&min_same_brand_products=0")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"]])


def test_categories_brands_filters_personalized_recommendations(client, event_dao, recommendation_api_client,
                                                                static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products
    user_id = get_random_user_id_with_population(client, Population.TARGET)

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()

    cache_builder.process_client(client, global_context, client_context)

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"],
                      result["data"][2]["product_id"]])

    # ---

    user_id = get_random_user_id_with_population(client, Population.TARGET)

    event_dao.create_event(client, random_event(
        user_id=user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_1.product_id
    ))

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"],
                      result["data"][2]["product_id"]])

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"],
                      result["data"][2]["product_id"]])

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=unknown&brands=unknown")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=unknown&brands=brand_1")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_1&brands=unknown")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 0

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_1&brands=brand_1")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 1
    assert result["data"][0]["product_id"] == product_1.product_id

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_3&brands=brand_1")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 2
    assert sorted([product_1.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"]])

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_1&categories=category_2&categories=category_3"
                             f"&brands=brand_1&brands=brand_2&brands=brand_3")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"],
                      result["data"][2]["product_id"]])

    # ---

    result = json.loads(recommendation_api_client
                        .get(f"/v2/recommendation/products/personalized?user_id={user_id}"
                             f"&categories=category_1&categories=category_2&categories=category_3&categories=unknown"
                             f"&brands=brand_1&brands=brand_2&brands=brand_3&brands=unknown")
                        .data.decode("utf-8"))
    assert result["status"] == "OK"
    assert len(result["data"]) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([result["data"][0]["product_id"], result["data"][1]["product_id"],
                      result["data"][2]["product_id"]])


def test_most_popular_products(client, recommendation_api_client, static_embedding_model_and_products,
                               target_random_user_id):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products
    user_id = target_random_user_id

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()

    cache_builder.process_client(client, global_context, client_context)
    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}&num=-1")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}&num=0")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}&num=21")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}&num=num")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(11))
    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}&{brands_params}")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(10))
    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}&{brands_params}")
    assert b'"status": "ERROR"' not in rv.data
    categories_params = "&".join(f"categories={random_string()}" for _ in range(11))
    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}"
                                       f"&{categories_params}")
    assert b'"status": "ERROR"' in rv.data
    categories_params = "&".join(f"categories={random_string()}" for _ in range(10))
    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}"
                                       f"&{categories_params}")
    assert b'"status": "ERROR"' not in rv.data

    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}")

    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 3
    assert data[0]["product_id"] == product_1.product_id
    assert data[1]["product_id"] == product_2.product_id
    assert data[2]["product_id"] == product_3.product_id

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}&brands=brand_1")

    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 2
    assert data[0]["product_id"] == product_1.product_id
    assert data[1]["product_id"] == product_3.product_id

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}"
                                       "&categories=category_1")

    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_1.product_id

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}&brands=brand_1"
                                       "&categories=category_1")

    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_1.product_id

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/most_popular?user_id={user_id}"
                                       "&brands=brand_1&brands=brand_2&brands=brand_3&brands=brand_4&brands=brand_5"
                                       "&categories=category_1&categories=category_2&categories=category_3")

    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 3
    assert data[0]["product_id"] == product_1.product_id
    assert data[1]["product_id"] == product_2.product_id
    assert data[2]["product_id"] == product_3.product_id


def test_recommendations(event_dao, recolog_dao,  client, recommendation_api_client,
                         static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()
    cache_builder.process_client(client, global_context, client_context)

    # Testing bad data
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&num=-1")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&num=0")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&num=21")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&num=num")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(11))
    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&brands={brands_params}")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(10))
    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&brands={brands_params}")
    assert b'"status": "ERROR"' not in rv.data

    # Testing "most_popular" source when user has no browsing history

    recolog_dao.delete_all_recommendations(client)
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    # meta = data["meta"]
    # assert meta["source"] == "most_popular_products"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 3
    assert sorted(reco.product_ids) == sorted([product_1.product_id, product_2.product_id, product_3.product_id])
    # assert reco.source == RecommendationSource.MOST_POPULAR_PRODUCTS
    assert reco.type == RecommendationType.PERSONAL_RECOMMENDATIONS
    assert reco.source_product_id is None

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&brands=brand_1&brands=brand_2&num=5"
                                       f"&categories=category_2&categories=category_3")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    # meta = data["meta"]
    # assert meta["source"] == "most_popular_products"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    # meta = data["meta"]
    # assert meta["source"] == "most_popular_products"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_2.product_id

    # Testing non-recommendable product (by brand)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&brands=brand_4&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    # meta = data["meta"]
    # assert meta["source"] == "most_popular_products"
    data = data["data"]
    assert len(data) == 0

    # Testing non-recommendable brand

    recolog_dao.delete_all_recommendations(client)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&brands=brand_5&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    # meta = data["meta"]
    # assert meta["source"] == "most_popular_products"
    data = data["data"]
    assert len(data) == 0

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 0
    # assert reco.source == RecommendationSource.MOST_POPULAR_PRODUCTS
    assert reco.type == RecommendationType.PERSONAL_RECOMMENDATIONS
    assert reco.source_product_id is None

    # Testing "model" source when user has a browsing history

    recolog_dao.delete_all_recommendations(client)
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_1.product_id
    ))

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}"
                                       f"&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 3
    assert sorted(reco.product_ids) == sorted([product_1.product_id, product_2.product_id, product_3.product_id])
    assert reco.source == RecommendationSource.MODEL
    assert reco.type == RecommendationType.PERSONAL_RECOMMENDATIONS
    assert reco.source_product_id is None

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}&"
                                       f"brands=brand_1&brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}&"
                                       f"brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_2.product_id

    # Testing another session

    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_3.product_id
    ))

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}&"
                                       f"brands=brand_1&brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])

    # Testing another session

    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_2.product_id
    ))
    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_2.product_id
    ))
    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_3.product_id
    ))

    rv = recommendation_api_client.get(f"/v2/recommendation/products/personalized?user_id={target_random_user_id}&"
                                       f"brands=brand_1&brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])


def test_similar_products(client, recolog_dao, recommendation_api_client, static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    # Testing bad data
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}&num=-1")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}&num=0")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}&num=21")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}&num=num")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(11))
    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}"
                                       f"&brands={brands_params}&product_id={product_1.product_id}")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(10))
    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}"
                                       f"&brands={brands_params}&product_id={product_1.product_id}")
    assert b'"status": "ERROR"' not in rv.data

    recolog_dao.delete_all_recommendations(client)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}"
                                       f"&brands=brand_1&brands=brand_2&num=5&product_id={product_1.product_id}")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 2
    assert sorted(reco.product_ids) == sorted([product_2.product_id, product_3.product_id])
    assert reco.source == RecommendationSource.MODEL
    assert reco.type == RecommendationType.SIMILAR_PRODUCTS
    assert reco.source_product_id == product_1.product_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}"
                                       f"&brands=brand_1&num=5&product_id={product_1.product_id}")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_3.product_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}&"
                                       f"same_brand=on&num=5&product_id={product_1.product_id}")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_3.product_id

    recolog_dao.delete_all_recommendations(client)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}&"
                                       f"brands=brand_na&num=5&product_id={product_1.product_id}")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 0

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 0
    assert reco.source == RecommendationSource.MODEL
    assert reco.type == RecommendationType.SIMILAR_PRODUCTS
    assert reco.source_product_id == product_1.product_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/similar?user_id={target_random_user_id}&num=5"
                                       f"&product_id={product_1.product_id}")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])


def test_basket_recommendations(client, recommendation_api_client, static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    # Test with no products passed

    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&complete_with_most_popular=no")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)
    assert data["meta"]["source"] == "most_popular_products"
    assert not data["data"]

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)
    assert data["meta"]["source"] == "most_popular_products"
    assert len(data["data"])

    # Test "good" data

    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_1.product_id}&complete_with_most_popular=no")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_1.product_id}&complete_with_most_popular=yes")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 5

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_2.product_id}&complete_with_most_popular=no")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_1.product_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_2.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 4

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_3.product_id}&complete_with_most_popular=no")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_1.product_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_3.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 4
    assert data[0]["product_id"] == product_1.product_id

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_3.product_id}&product_ids={product_2.product_id}"
                                       f"&complete_with_most_popular=no")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_1.product_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_3.product_id}&product_ids={product_2.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 4
    assert data[0]["product_id"] == product_1.product_id

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_3.product_id}&product_ids={product_2.product_id}"
                                       f"&product_ids={product_1.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 3

    rv = recommendation_api_client.get(f"/v2/recommendation/products/basket?user_id={target_random_user_id}"
                                       f"&product_ids={product_3.product_id}&product_ids={product_2.product_id}"
                                       f"&product_ids={product_1.product_id}&complete_with_most_popular=no")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 0


def test_search(client, search_dao, recommendation_api_client, static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    search_dao.index_product(client, product_1, refresh_index=False)
    search_dao.index_product(client, product_2, refresh_index=False)
    search_dao.index_product(client, product_3, refresh_index=False)
    search_dao.refresh_index(client)

    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/v2/search?user_id={target_random_user_id}&q={product_1.title}")
    assert b'"status": "OK"' in rv.data


def test_zone_api(client, catalog_dao, settings_dao, event_dao, recommendation_api_client,
                  static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    extra_product = random_product()
    catalog_dao.create_product(client, extra_product)

    settings_dao.delete_all_campaigns(client)
    settings_dao.delete_all_zones(client)

    zone_1 = random_zone(zone_id="zone_1", zone_type=ZoneType.SIMILAR_PRODUCTS)
    zone_2 = random_zone(zone_id="zone_2", zone_type=ZoneType.PERSONAL_RECOMMENDATIONS)
    zone_3 = random_zone(zone_id="zone_3", zone_type=ZoneType.MOST_POPULAR)
    zone_4 = random_zone(zone_id="zone_4", zone_type=ZoneType.BASKET_RECOMMENDATIONS)
    settings_dao.create_zone(client, zone_1)
    settings_dao.create_zone(client, zone_2)
    settings_dao.create_zone(client, zone_3)
    settings_dao.create_zone(client, zone_4)

    campaign = random_campaign()
    settings_dao.create_campaign(client, campaign)

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()
    cache_builder.process_client(client, global_context, client_context)

    user_id = get_random_user_id_with_population(client, Population.TARGET)

    # As there is no browsing history, the API should return most popular products
    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}&zone_id={zone_2.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    # assert data["meta"]["source"] == "most_popular"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone_2.zone_settings
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])

    while True:
        user_id_2 = get_random_user_id_with_population(client, Population.TARGET)
        if user_id_2 != user_id:
            break

    event_dao.create_event(client, random_event(user_id=user_id_2, product_id=product_1.product_id,
                                                event_type=EventType.PRODUCT_VIEW))

    # Testing zone 2 - personal recommendations

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_2.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone_2.zone_settings
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])

    zone_2.zone_settings.brands = ["brand_1"]
    settings_dao.create_zone(client, zone_2)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_2.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone_2.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_1.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # Testing zone 1 - similar products

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_1.zone_id}")
    assert b'"status": "ERROR"' in rv.data

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_1.zone_id}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone_1.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # ---

    zone_1.zone_settings.same_brand = True
    settings_dao.create_zone(client, zone_1)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_1.zone_id}&product_id={product_1.product_id}"
                                       f"&complete_with_most_popular=no")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone_1.zone_settings
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_3.product_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_1.zone_id}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone_1.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert data[0]["product_id"] == product_3.product_id

    # ---

    zone_1.zone_settings.same_brand = True
    zone_1.zone_settings.brands = [product_2.brand]
    settings_dao.create_zone(client, zone_1)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_1.zone_id}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone_1.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # Testing zone 3 - most popular

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_3.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"]])

    zone_3.zone_settings.brands = [product_3.brand]
    settings_dao.create_zone(client, zone_3)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_3.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_1.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # Testing zone 4 - basket recommendations

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_4.zone_id}&complete_with_most_popular=no")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert not data

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_4.zone_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 3

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_4.zone_id}&product_ids={product_1.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # ---

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_4.zone_id}&product_ids={product_3.product_id}"
                                       f"&product_ids={product_3.product_id}&complete_with_most_popular=no")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 1
    assert data[0]["product_id"] == product_1.product_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id_2}"
                                       f"&zone_id={zone_4.zone_id}&product_ids={product_3.product_id}"
                                       f"&product_ids={product_3.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 2


def test_campaigns(client, catalog_dao, settings_dao, event_dao, recommendation_api_client,
                   static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    extra_product = random_product()
    catalog_dao.create_product(client, extra_product)

    settings_dao.delete_all_campaigns(client)
    settings_dao.delete_all_zones(client)

    zone = random_zone(zone_type=ZoneType.SIMILAR_PRODUCTS)
    settings_dao.create_zone(client, zone)

    user_id = get_random_user_id_with_population(client, Population.TARGET)

    campaign = random_campaign()

    # No campaign assigned

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}&product_id={product_1.product_id}"
                                       f"&complete_with_most_popular=no")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # Campaign in the past

    campaign.start_date = datetime.utcnow() - timedelta(days=10)
    campaign.end_date = datetime.utcnow() - timedelta(days=1)
    campaign.zone_settings.recommendations_num = 1
    settings_dao.create_campaign(client, campaign)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign.campaign_id)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}&product_id={product_1.product_id}"
                                       f"&complete_with_most_popular=no")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # Campaign in the future

    campaign.start_date = datetime.utcnow() + timedelta(days=1)
    campaign.end_date = datetime.utcnow() + timedelta(days=10)
    campaign.zone_settings.recommendations_num = 1
    settings_dao.create_campaign(client, campaign)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign.campaign_id)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}&product_id={product_1.product_id}"
                                       f"&complete_with_most_popular=no")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # Campaign with good start and end dates

    campaign.start_date = datetime.utcnow() - timedelta(days=1)
    campaign.end_date = datetime.utcnow() + timedelta(days=1)
    campaign.zone_settings.recommendations_num = 1
    settings_dao.create_campaign(client, campaign)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign.campaign_id)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) != zone.zone_settings
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] in [product_1.product_id, product_2.product_id, product_3.product_id]

    # Inactive campaign

    campaign.status = CampaignStatus.DISABLED
    campaign.zone_settings.recommendations_num = 1
    settings_dao.create_campaign(client, campaign)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign.campaign_id)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}&product_id={product_1.product_id}"
                                       f"&complete_with_most_popular=no")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) == zone.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])

    # Active + Inactive campaigns (+ Active)

    settings_dao.replace_zone_campaign_ids(client, zone_id=zone.zone_id, campaign_ids=[])
    assert not settings_dao.read_zone(client, zone.zone_id).campaign_ids

    new_campaign = random_campaign()
    new_campaign.zone_settings.recommendations_num = 1
    settings_dao.create_campaign(client, new_campaign)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, new_campaign.campaign_id)

    campaign.status = CampaignStatus.DISABLED
    campaign.zone_settings.recommendations_num = 2
    settings_dao.create_campaign(client, campaign)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign.campaign_id)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) != zone.zone_settings
    data = data["data"]
    assert len(data) == 1
    assert data[0]["product_id"] in [product_1.product_id, product_2.product_id, product_3.product_id]

    super_new_campaign = random_campaign()
    super_new_campaign.zone_settings.recommendations_num = 2
    settings_dao.create_campaign(client, super_new_campaign)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, super_new_campaign.campaign_id)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "model"
    assert ZoneSettings.from_dict(data["meta"]["zone_settings"]) != zone.zone_settings
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["product_id"], data[1]["product_id"]])


def test_extra_products(client, catalog_dao, settings_dao, event_dao, recommendation_api_client,
                        static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()
    cache_builder.process_client(client, global_context, client_context)

    extra_product_ids = [random_string() + "-extra" for _ in range(10)]
    for extra_product_id in extra_product_ids:
        extra_product = random_product(product_id=extra_product_id)
        catalog_dao.create_product(client, extra_product)

    settings_dao.delete_all_campaigns(client)
    settings_dao.delete_all_zones(client)

    zone = random_zone(zone_type=ZoneType.MOST_POPULAR)
    settings_dao.create_zone(client, zone)

    user_id = get_random_user_id_with_population(client, Population.TARGET)

    # No extra proudcts

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 3
    assert data[0]["product_id"] == product_1.product_id
    assert data[1]["product_id"] == product_2.product_id
    assert data[2]["product_id"] == product_3.product_id

    # Extra products in the beginning with no space for organic products

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.BEGINNING
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1], extra_product_ids[2]]
    zone.zone_settings.recommendations_num = 2
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "predefined"
    data = data["data"]
    assert len(data) == 2
    assert data[0]["product_id"] == extra_product_ids[0]
    assert data[1]["product_id"] == extra_product_ids[1]

    # Extra products in the beginning with space for organic products

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.BEGINNING
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1]]
    zone.zone_settings.recommendations_num = 4
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 4
    assert data[0]["product_id"] == extra_product_ids[0]
    assert data[1]["product_id"] == extra_product_ids[1]
    assert data[2]["product_id"] == product_1.product_id
    assert data[3]["product_id"] == product_2.product_id

    # Extra products in the beginning with space for organic products + saving organic products positions

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.BEGINNING
    zone.zone_settings.extra_product_ids = [product_2.product_id]
    zone.zone_settings.recommendations_num = 3
    zone.zone_settings.save_organic_extra_products_positions = True
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 3
    assert data[0]["product_id"] == product_1.product_id
    assert data[1]["product_id"] == product_2.product_id
    assert data[2]["product_id"] == product_3.product_id

    # Extra products in the end with no space for organic products

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.END
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1], extra_product_ids[2]]
    zone.zone_settings.recommendations_num = 2
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "predefined"
    data = data["data"]
    assert len(data) == 2
    assert data[0]["product_id"] == extra_product_ids[0]
    assert data[1]["product_id"] == extra_product_ids[1]

    # Extra products in the beginning with space for organic products

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.END
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1]]
    zone.zone_settings.recommendations_num = 4
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 4
    assert data[0]["product_id"] == product_1.product_id
    assert data[1]["product_id"] == product_2.product_id
    assert data[2]["product_id"] == extra_product_ids[0]
    assert data[3]["product_id"] == extra_product_ids[1]

    # Extra products evenly distributed

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.EVENLY
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1], extra_product_ids[2]]
    zone.zone_settings.recommendations_num = 5
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 5
    assert data[0]["product_id"] == extra_product_ids[0]
    assert data[1]["product_id"] == product_1.product_id
    assert data[2]["product_id"] == extra_product_ids[1]
    assert data[3]["product_id"] == product_2.product_id
    assert data[4]["product_id"] == extra_product_ids[2]

    # More extra products evenly distributed

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.EVENLY
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1], extra_product_ids[2],
                                            extra_product_ids[3]]
    zone.zone_settings.recommendations_num = 5
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 5
    assert data[0]["product_id"] == extra_product_ids[0]
    assert data[1]["product_id"] == extra_product_ids[1]
    assert data[2]["product_id"] == product_1.product_id
    assert data[3]["product_id"] == extra_product_ids[2]
    assert data[4]["product_id"] == extra_product_ids[3]

    # Extra products randomly distributed

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.RANDOM
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1], extra_product_ids[2]]
    zone.zone_settings.recommendations_num = 5
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 5
    assert sorted([data[0]["product_id"], data[2]["product_id"], data[4]["product_id"]]) \
        == sorted([extra_product_ids[0], extra_product_ids[1], extra_product_ids[2]])
    assert data[1]["product_id"] == product_1.product_id
    assert data[3]["product_id"] == product_2.product_id

    # More extra products randomly distributed

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.EVENLY
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1], extra_product_ids[2],
                                            extra_product_ids[3]]
    zone.zone_settings.recommendations_num = 5
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 5
    assert sorted([data[0]["product_id"], data[1]["product_id"], data[3]["product_id"], data[4]["product_id"]]) \
        == sorted([extra_product_ids[0], extra_product_ids[1], extra_product_ids[2], extra_product_ids[3]])
    assert data[2]["product_id"] == product_1.product_id

    # Even more extra products randomly distributed

    zone.zone_settings.extra_products_position = ZoneExtraProductsPosition.EVENLY
    zone.zone_settings.extra_product_ids = [extra_product_ids[0], extra_product_ids[1], extra_product_ids[2],
                                            extra_product_ids[3], extra_product_ids[4]]
    zone.zone_settings.recommendations_num = 5
    settings_dao.create_zone(client, zone)

    rv = recommendation_api_client.get(f"/v2/recommendation/products/zoned?user_id={user_id}"
                                       f"&zone_id={zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["meta"]["source"] == "predefined"
    data = data["data"]
    assert len(data) == 5
    assert sorted([data[0]["product_id"], data[1]["product_id"], data[2]["product_id"], data[3]["product_id"],
                   data[4]["product_id"]]) \
        == sorted([extra_product_ids[0], extra_product_ids[1], extra_product_ids[2], extra_product_ids[3],
                   extra_product_ids[4]])


def test_test_recommendation(client, catalog_dao, settings_dao, event_dao, recommendation_api_client,
                             static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()
    cache_builder.process_client(client, global_context, client_context)

    json_events = [
        {"product_id": product_1.product_id, "event_type": EventType.PRODUCT_VIEW.name.lower()},
        {"product_id": product_2.product_id, "event_type": EventType.PRODUCT_VIEW.name.lower()}
    ]

    json_events_str = werkzeug.urls.url_quote(json.dumps(json_events))

    json_empty_events = []
    empty_json_events_str = werkzeug.urls.url_quote(json.dumps(json_empty_events))

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=wrong_reco_type")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history=bad_value")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id=__non_existing__")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}&"
                                       f"num=-1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}&"
                                       f"num=21")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history={empty_json_events_str}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}"
                                       f"&brands=brand_1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}&num=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=personal_recommendations&"
                                       f"browsing_history={json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=personal_recommendations&"
                                       f"browsing_history={empty_json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=personal_recommendations&"
                                       f"browsing_history={empty_json_events_str}&num=2")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=basket_recommendations&"
                                       f"browsing_history={json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 0

    rv = recommendation_api_client.get(f"/v2/recommendation/products/test?recommendation_type=basket_recommendations&"
                                       f"browsing_history={json_events_str}&basket_product_ids={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2


def test_test_search(client, search_dao, recommendation_api_client, static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    search_dao.index_product(client, product_1, refresh_index=False)
    search_dao.index_product(client, product_2, refresh_index=False)
    search_dao.index_product(client, product_3, refresh_index=False)
    search_dao.refresh_index(client)

    rv = recommendation_api_client.get(f"/v2/search/test?q={product_1.title}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) > 0

    json_events = [
        {"product_id": product_1.product_id, "event_type": EventType.PRODUCT_VIEW.name.lower()},
        {"product_id": product_3.product_id, "event_type": EventType.PRODUCT_VIEW.name.lower()}
    ]

    json_events_str = werkzeug.urls.url_quote(json.dumps(json_events))

    rv = recommendation_api_client.get(f"/v2/search/test?q={product_1.title}&browsing_history={json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) > 0


def test_check_features(client, event_dao, recommendation_api_client, static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    user_id = random_string()

    event_dao.create_event(client, random_event(
        user_id=user_id,
        product_id=product_1.product_id,
        event_type=EventType.PRODUCT_VIEW
    ))

    rv = recommendation_api_client.get(f"/v2/recommendation/check/features?product_id={product_1.product_id}"
                                       f"&user_id={user_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_rank_products(client, recommendation_api_client, static_embedding_model_and_products,
                       target_random_user_id):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    user_id = target_random_user_id

    rv = recommendation_api_client.get(f"/v2/recommendation/products/rank?user_id={user_id}"
                                       f"&product_ids={product_1.product_id}&product_ids={product_2.product_id}"
                                       f"&product_ids={product_3.product_id}&product_ids=unknown_product_id")

    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

