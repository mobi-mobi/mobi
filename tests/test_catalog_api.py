import json
import pytest

from flask import testing

from mobi.api.catalog_api import CatalogApi
from mobi.config.system.api.catalog_api import get_catalog_api_config
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao
from mobi.utils.tests import random_product, random_string


class TestCatalogApiClient(testing.FlaskClient):
    MOBI_TOKEN = "testtoken"

    def open(self, *args, **kwargs):
        if self.MOBI_TOKEN is not None:
            headers = kwargs.get('headers', {})
            headers['Authorization'] = "Bearer " + self.MOBI_TOKEN
            kwargs['headers'] = headers
        return super().open(*args, **kwargs)


class TestCatalogApiClientWithHashedToken(TestCatalogApiClient):
    MOBI_TOKEN = "testtokenhashed"


class TestCatalogApiClientWithNoToken(TestCatalogApiClient):
    MOBI_TOKEN = None


class TestCatalogApiClientWithExpiredToken(TestCatalogApiClient):
    MOBI_TOKEN = "expiredtesttoken"


class TestCatalogApiClientWithWrongToken(TestCatalogApiClient):
    MOBI_TOKEN = "__testnonexistingtoken__"


class TestCatalogApiClientWithProdToken(TestCatalogApiClient):
    MOBI_TOKEN = "testtokenprod"


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def catalog_api_client():
    api = CatalogApi()
    api.app.test_client_class = TestCatalogApiClient

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_expired_token():
    api = CatalogApi()
    api.app.test_client_class = TestCatalogApiClientWithExpiredToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_hashed_token():
    api = CatalogApi()
    api.app.test_client_class = TestCatalogApiClientWithHashedToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_no_token():
    api = CatalogApi()
    api.app.test_client_class = TestCatalogApiClientWithNoToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_wrong_token():
    api = CatalogApi()
    api.app.test_client_class = TestCatalogApiClientWithWrongToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_prod_token():
    api = CatalogApi()
    api.app.test_client_class = TestCatalogApiClientWithProdToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def event_dao():
    return get_event_dao(get_catalog_api_config())


@pytest.fixture(scope="session")
def catalog_dao():
    return get_catalog_dao(get_catalog_api_config())


@pytest.fixture(scope="session")
def cache_dao():
    return get_cache_dao(get_catalog_api_config())


@pytest.fixture
def random_non_existing_product(client, catalog_dao):
    product = random_product()
    try:
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass

    yield product

    try:
        # Who knows, maybe it was already deleted in the test body
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass


@pytest.fixture
def random_existing_product(client, catalog_dao, random_non_existing_product):
    catalog_dao.create_product(client, random_non_existing_product)
    yield random_non_existing_product

    try:
        # Who knows, maybe it was already deleted in the test body
        catalog_dao.delete_product(client, random_non_existing_product.product_id)
    except Exception:
        pass


def test_catalog_api_init(catalog_api_client):
    pass


def test_status(client_with_no_token):
    rv = client_with_no_token.get("/v2/catalog/status")
    assert rv.data == b'{"status": "OK"}'


def test_empty_request_body(catalog_api_client):
    rv = catalog_api_client.post("/v2/catalog/products", data="")
    assert b'Product schema validation failed' in rv.data
    assert b'Can not parse JSON string' in rv.data

    rv = catalog_api_client.post("/v2/catalog/products", data=None)
    assert b'Product schema validation failed' in rv.data
    assert b'Can not parse JSON string' in rv.data


def test_wrong_json_body(catalog_api_client):
    rv = catalog_api_client.post("/v2/catalog/products", data="{{{")
    assert b'Product schema validation failed' in rv.data
    assert b'Can not parse JSON string' in rv.data


def test_empty_json(catalog_api_client):
    rv = catalog_api_client.post("/v2/catalog/products", data="{}")
    assert b'Product schema validation failed' in rv.data
    assert b'Can not parse product parameters' in rv.data
    assert b'is a required property' in rv.data


def test_status_with_no_token(client_with_no_token):
    rv = client_with_no_token.get("/status")
    assert rv.data == b'{"status": "OK"}'


def test_no_tokens(client_with_no_token):
    rv = client_with_no_token.get("/v2/some/url")
    assert b'"code": 401' in rv.data
    assert b'"status": "ERROR"' in rv.data
    assert b'No token provided' in rv.data


def test_wrong_token(client_with_wrong_token):
    rv = client_with_wrong_token.get("/v2/some/url")
    assert b'"code": 403' in rv.data
    assert b'"status": "ERROR"' in rv.data
    assert b'Token is wrong' in rv.data


def test_wrong_token_in_uri(client_with_no_token):
    rv = client_with_no_token.get("/v2/some/url?token=__testnonexistingtoken__")
    assert b'"code": 401' in rv.data
    assert b'"status": "ERROR"' in rv.data
    assert b'No token provided' in rv.data


def test_ok_token_in_header_but_wrong_token_in_uri(client_with_no_token):
    rv = client_with_no_token.get("/v2/some/url?token=__testnonexistingtoken__")
    assert b'"code": 401' in rv.data
    assert b'"status": "ERROR"' in rv.data
    assert b'No token provided' in rv.data


def test_with_expired_token(client_with_expired_token):
    rv = client_with_expired_token.get("/v2/some/url")
    assert b'"code": 403' in rv.data
    assert b'"status": "ERROR"' in rv.data
    assert b'Token is expired' in rv.data


def test_with_expired_token_in_uri(client_with_no_token):
    rv = client_with_no_token.get("/v2/some/url?token=expiredtesttoken")
    assert b'"code": 401' in rv.data
    assert b'"status": "ERROR"' in rv.data
    assert b'No token provided' in rv.data


def test_wrong_url(catalog_api_client):
    rv = catalog_api_client.get("/v2/somewrongurl")
    assert b'"status": "ERROR"' in rv.data
    assert b'The requested URL was not found on the server' in rv.data


def test_create_product(catalog_api_client, random_non_existing_product):
    rv = catalog_api_client.post(
        "/v2/catalog/products",
        data=json.dumps(random_non_existing_product.as_json_dict(exclude_ignored_fields=True))
    )
    assert b'"status": "OK"' in rv.data


def test_create_product_with_hashed_token(client_with_hashed_token, random_non_existing_product):
    rv = client_with_hashed_token.post(
        "/v2/catalog/products",
        data=json.dumps(random_non_existing_product.as_json_dict(exclude_ignored_fields=True))
    )
    assert b'"status": "OK"' in rv.data


def test_create_product_with_wrong_parameter(catalog_api_client, random_non_existing_product):
    product_dict = random_non_existing_product.as_json_dict(exclude_ignored_fields=True)
    product_dict["__unknown_parameter__"] = "some value"

    rv = catalog_api_client.post("/v2/catalog/products", data=json.dumps(product_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'Product schema validation failed' in rv.data
    assert b'Additional properties are not allowed' in rv.data


def test_create_enriched_product(catalog_api_client, random_non_existing_product):
    random_non_existing_product.title += " eye balm"

    rv = catalog_api_client.post(
        "/v2/catalog/products",
        data=json.dumps(random_non_existing_product.as_json_dict(exclude_ignored_fields=True))
    )
    assert b'"status": "OK"' in rv.data

    response = json.loads(catalog_api_client.get(f"/v2/catalog/products/{random_non_existing_product.product_id}").data)
    assert response["status"] == "OK"

    product_dict = random_non_existing_product.as_json_dict(exclude_ignored_fields=True)
    received_data = response["data"]
    for k in product_dict.keys():
        if k != "enriched_tags":
            assert product_dict[k] == received_data[k]
        else:
            assert set(received_data[k]) == set(product_dict[k]).union(["eyes", "eye_balm"])


def test_create_product_without_categories(catalog_api_client):
    product = random_product()
    product.categories = None

    rv = catalog_api_client.post("/v2/catalog/products",
                                 data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert b'"status": "OK"' in rv.data


def test_create_product_with_empty_categories_list(catalog_api_client):
    product = random_product()
    product.categories = []

    rv = catalog_api_client.post("/v2/catalog/products",
                                 data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert b'"status": "OK"' in rv.data


def test_create_product_with_invalid_category(catalog_api_client):
    product = random_product()
    product.categories = "bad data"

    rv = catalog_api_client.post("/v2/catalog/products",
                                 data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert b'"status": "ERROR"' in rv.data
    assert b'Product schema validation failed' in rv.data
    assert b'Can not parse product parameters' in rv.data

    product = random_product()
    product.categories = [1]

    rv = catalog_api_client.post("/v2/catalog/products",
                                 data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert b'"status": "ERROR"' in rv.data
    assert b'Product schema validation failed' in rv.data
    assert b'Can not parse product parameters' in rv.data


def test_create_product_with_multiple_categories(catalog_api_client):
    product = random_product()
    product.categories = ["1", "2"]

    rv = catalog_api_client.post("/v2/catalog/products",
                                 data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert rv.data == b'{"status": "OK"}'


def test_create_product_with_repeated_categories(catalog_api_client):
    product = random_product()
    product.categories = ["1", "1"]

    rv = catalog_api_client.post("/v2/catalog/products",
                                 data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert b'"status": "ERROR"' in rv.data
    assert b'Product schema validation failed' in rv.data
    assert b'has non-unique elements' in rv.data


def test_create_product_with_no_required_field(catalog_api_client):
    for field in ("product_id", "brand", "title", "price", "currency"):
        product = random_product()
        product_dict = product.as_json_dict(exclude_ignored_fields=True)
        del product_dict[field]

        rv = catalog_api_client.post("/v2/catalog/products", data=json.dumps(product_dict))
        assert b'"status": "ERROR"' in rv.data
        assert b'Product schema validation failed' in rv.data
        assert b'is a required property' in rv.data

        product = random_product()
        product_dict = product.as_json_dict(exclude_ignored_fields=True)
        product_dict[field] = None

        rv = catalog_api_client.post("/v2/catalog/products", data=json.dumps(product_dict))
        assert b'"status": "ERROR"' in rv.data
        assert b'Product schema validation failed' in rv.data
        assert b'None is not of type' in rv.data


def test_create_product_with_wrong_required_field(catalog_api_client):
    for field in ("product_id", "brand", "title", "price", "currency"):
        product = random_product()
        product_dict = product.as_json_dict(exclude_ignored_fields=True)
        product_dict[field] = 42

        rv = catalog_api_client.post("/v2/catalog/products", data=json.dumps(product_dict))
        assert b'"status": "ERROR"' in rv.data
        assert b'Product schema validation failed' in rv.data
        assert b'Can not parse product parameters' in rv.data

        product = random_product()
        product_dict = product.as_json_dict(exclude_ignored_fields=True)
        product_dict[field] = [42]

        rv = catalog_api_client.post("/v2/catalog/products", data=json.dumps(product_dict))
        assert b'"status": "ERROR"' in rv.data
        assert b'Product schema validation failed' in rv.data
        assert b'Can not parse product parameters' in rv.data


def test_create_replace_read_product(catalog_api_client):
    product = random_product()

    rv = catalog_api_client.post("/v2/catalog/products",
                                 data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert b'"status": "OK"' in rv.data

    new_product = random_product(product_id=product.product_id)
    rv = catalog_api_client.post(f"/v2/catalog/products",
                                 data=json.dumps(new_product.as_json_dict(exclude_ignored_fields=True)))
    assert b'"status": "OK"' in rv.data

    response = json.loads(catalog_api_client.get(f"/v2/catalog/products/{product.product_id}").data)
    assert response["status"] == "OK"

    product_dict = new_product.as_json_dict(exclude_ignored_fields=True)
    received_data = response["data"]
    for k in product_dict.keys():
        assert product_dict[k] == received_data[k]


def test_create_patch_read_product(catalog_api_client, random_non_existing_product):
    product = random_non_existing_product

    rv = catalog_api_client.post("/v2/catalog/products",
                                 data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert b'"status": "OK"' in rv.data

    new_product = random_product()
    for name, value in new_product.as_json_dict(exclude_ignored_fields=True).items():
        if name == "product_id":
            continue

        patch_dict = {"product_id": product.product_id, name: value}
        rv = catalog_api_client.patch(f"/v2/catalog/products/{product.product_id}", data=json.dumps(patch_dict))
        assert b'"status": "OK"' in rv.data

        response = json.loads(catalog_api_client.get(f"/v2/catalog/products/{product.product_id}").data)
        assert response["status"] == "OK"
        assert response["data"][name] == value


def test_patch_unexisting_product(catalog_api_client, random_non_existing_product):
    product = random_non_existing_product

    patch_dict = {"product_id": product.product_id, "title": "Some new random title " + random_string()}
    rv = catalog_api_client.patch(f"/v2/catalog/products/{product.product_id}", data=json.dumps(patch_dict))
    assert b'"status": "ERROR"' in rv.data
    assert b'does not exist' in rv.data


def test_read_unknown_product(catalog_api_client):
    rv = catalog_api_client.get("/v2/catalog/products/testwrongproductid")
    assert b'"status": "ERROR"' in rv.data
    assert b'does not exist' in rv.data


def test_delete_product(catalog_api_client, random_existing_product):
    rv = catalog_api_client.get(f"/v2/catalog/products/{random_existing_product.product_id}")
    assert b'"status": "OK"' in rv.data

    rv = catalog_api_client.delete(f"/v2/catalog/products/{random_existing_product.product_id}")
    assert b'"status": "OK"' in rv.data

    rv = catalog_api_client.get(f"/v2/catalog/products/{random_existing_product.product_id}")
    assert b'"status": "ERROR"' in rv.data


def test_delete_product_twice(catalog_api_client, random_existing_product):
    rv = catalog_api_client.get(f"/v2/catalog/products/{random_existing_product.product_id}")
    assert b'"status": "OK"' in rv.data

    rv = catalog_api_client.delete(f"/v2/catalog/products/{random_existing_product.product_id}")
    assert b'"status": "OK"' in rv.data

    rv = catalog_api_client.get(f"/v2/catalog/products/{random_existing_product.product_id}")
    assert b'"status": "ERROR"' in rv.data

    rv = catalog_api_client.delete(f"/v2/catalog/products/{random_existing_product.product_id}")
    assert b'"status": "OK"' in rv.data

    rv = catalog_api_client.get(f"/v2/catalog/products/{random_existing_product.product_id}")
    assert b'"status": "ERROR"' in rv.data


def test_delete_all_products_for_prod_token(client_with_prod_token):
    rv = client_with_prod_token.delete("/v2/catalog/all_products")
    assert b'"code": 403' in rv.data
    assert b'Can not perform delete operation for a non-test token' in rv.data


def test_delete_all_products(catalog_api_client, random_existing_product):
    product = random_product()
    product_dict = product.as_json_dict(exclude_ignored_fields=True)

    rv = catalog_api_client.post("/v2/catalog/products", data=json.dumps(product_dict))
    assert rv.data == b'{"status": "OK"}'

    rv = catalog_api_client.delete("/v2/catalog/all_products")
    assert rv.data == b'{"status": "OK"}'

    rv = catalog_api_client.get(f"/v2/catalog/products/{product.product_id}")
    assert b'"status": "ERROR"' in rv.data
    assert b'does not exist' in rv.data


def test_create_product_after_deletion(client, catalog_api_client, catalog_dao):
    product = random_product()

    rv = catalog_api_client.post("/v2/catalog/products", data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert rv.data == b'{"status": "OK"}'

    rv = catalog_api_client.delete(f"/v2/catalog/products/{product.product_id}")
    assert b'"status": "OK"' in rv.data

    rv = catalog_api_client.get(f"/v2/catalog/products/{product.product_id}")
    assert b'"status": "ERROR"' in rv.data

    product = random_product(product_id=product.product_id)

    rv = catalog_api_client.post("/v2/catalog/products", data=json.dumps(product.as_json_dict(exclude_ignored_fields=True)))
    assert rv.data == b'{"status": "OK"}'

    response = json.loads(catalog_api_client.get(f"/v2/catalog/products/{product.product_id}").data)
    assert response["status"] == "OK"

    product_dict = product.as_json_dict(exclude_ignored_fields=True)
    received_data = response["data"]
    for k in product_dict.keys():
        assert product_dict[k] == received_data[k]

    product = catalog_dao.read_product(client, product.product_id)
    assert product.version == 2
