import pytest

"""
from mobi.core.data import *


def test_interval_feature():
    f = BoundedCategoricalFeature("name")
    assert f.int_value(0) == 0
    assert f.int_value(0.999) == 0
    assert f.int_value(1) == 1
    assert f.int_value(1.001) == 1
    assert f.int_value(1.999) == 1
    assert f.int_value(2) == 2
    assert f.int_value(2.001) == 2
    assert f.int_value(2.999) == 2

    with pytest.raises(ValueError):
        f.int_value(None)

    f = MobiCategoricalNumericIntervalFeature("name", [1, 2])
    f.set_mode(FeatureMode.INFERENCE)
    assert f.int_value(0) == 0
    assert f.int_value(0.999) == 0
    assert f.int_value(1) == 1
    assert f.int_value(1.001) == 1
    assert f.int_value(1.999) == 1
    assert f.int_value(2) == 2
    assert f.int_value(2.001) == 2
    assert f.int_value(2.999) == 2

    f = MobiCategoricalNumericIntervalFeature("name", [1, 2])
    assert f.int_value(2) == 2
    assert f.int_value(2.001) == 2
    assert f.int_value(2.999) == 2
    assert f.int_value(0) == 0
    assert f.int_value(0.999) == 0
    assert f.int_value(1) == 1
    assert f.int_value(1.001) == 1
    assert f.int_value(1.999) == 1

    f = MobiCategoricalNumericIntervalFeature("name", [])
    assert f.int_value(2) == 0
    assert f.int_value(2.001) == 0
    assert f.int_value(2.999) == 0
    assert f.int_value(0) == 0
    assert f.int_value(0.999) == 0
    assert f.int_value(1) == 0
    assert f.int_value(1.001) == 0
    assert f.int_value(1.999) == 0


def test_interval_optional_feature():
    f = MobiCategoricalNumericIntervalFeature("name", [1, 2], optional=True)
    assert f.int_value(None) == 0
    assert f.int_value(0) == 1
    assert f.int_value(0.999) == 1
    assert f.int_value(1) == 2
    assert f.int_value(1.001) == 2
    assert f.int_value(1.999) == 2
    assert f.int_value(2) == 3
    assert f.int_value(2.001) == 3
    assert f.int_value(2.999) == 3

    f = MobiCategoricalNumericIntervalFeature("name", [1, 2], optional=True)
    f.set_mode(FeatureMode.INFERENCE)
    assert f.int_value(None) == 0
    assert f.int_value(-10**10) == 1
    assert f.int_value(0) == 1
    assert f.int_value(0.999) == 1
    assert f.int_value(1) == 2
    assert f.int_value(1.001) == 2
    assert f.int_value(1.999) == 2
    assert f.int_value(2) == 3
    assert f.int_value(2.001) == 3
    assert f.int_value(2.999) == 3
    assert f.int_value(10**10) == 3

    f = MobiCategoricalNumericIntervalFeature("name", [1, 2], optional=True)
    assert f.int_value(2) == 3
    assert f.int_value(2.001) == 3
    assert f.int_value(2.999) == 3
    assert f.int_value(0) == 1
    assert f.int_value(0.999) == 1
    assert f.int_value(1) == 2
    assert f.int_value(1.001) == 2
    assert f.int_value(1.999) == 2
    assert f.int_value(None) == 0

    f = MobiCategoricalNumericIntervalFeature("name", [], optional=True)
    assert f.int_value(2) == 1
    assert f.int_value(2.001) == 1
    assert f.int_value(2.999) == 1
    assert f.int_value(0) == 1
    assert f.int_value(0.999) == 1
    assert f.int_value(1) == 1
    assert f.int_value(1.001) == 1
    assert f.int_value(1.999) == 1
    assert f.int_value(None) == 0


def test_bad_numeric_interval_bounds():
    with pytest.raises(ValueError):
        MobiCategoricalNumericIntervalFeature("name", [None], optional=True)

    with pytest.raises(ValueError):
        MobiCategoricalNumericIntervalFeature("name", [1, "a"], optional=True)

    with pytest.raises(ValueError):
        MobiCategoricalNumericIntervalFeature("name", [1, "1"], optional=True)

    with pytest.raises(ValueError):
        MobiCategoricalNumericIntervalFeature("name", [1, "1.1"], optional=True)


def test_categorical_string_feature():
    f = MobiCategoricalStrFeature("name", optional=False)
    assert f.int_value("a") == 0
    assert f.int_value("b") == 1
    assert f.int_value("c") == 2
    assert f.int_value("a") == 0
    assert f.int_value("b") == 1
    assert f.int_value("") == 3

    f = MobiCategoricalStrFeature("name", optional=True)
    assert f.int_value(None) == 0
    assert f.int_value("a") == 1
    assert f.int_value("b") == 2
    assert f.int_value("c") == 3
    assert f.int_value("a") == 1
    assert f.int_value("b") == 2

    with pytest.raises(ValueError):
        f.int_value(1)

    with pytest.raises(ValueError):
        f.int_value(1.1)

    with pytest.raises(ValueError):
        f.int_value(bool)

    with pytest.raises(ValueError):
        f.int_value(["a"])

    f.set_mode(FeatureMode.INFERENCE)
    with pytest.raises(ValueError):
        f.int_value("d")

    with pytest.raises(ValueError):
        f.int_value(1.1)


def test_categorical_int_feature():
    f = MobiCategoricalIntFeature("name", optional=False)
    assert f.int_value(1) == 0
    assert f.int_value(2) == 1
    assert f.int_value(3) == 2
    assert f.int_value(1) == 0
    assert f.int_value(2) == 1

    f = MobiCategoricalIntFeature("name", optional=True)
    assert f.int_value(None) == 0
    assert f.int_value(1) == 1
    assert f.int_value(2) == 2
    assert f.int_value(3) == 3
    assert f.int_value(1) == 1
    assert f.int_value(2) == 2

    with pytest.raises(ValueError):
        f.int_value("a")

    with pytest.raises(ValueError):
        f.int_value(1.1)

    with pytest.raises(ValueError):
        f.int_value(bool)

    with pytest.raises(ValueError):
        f.int_value([1])

    f.set_mode(FeatureMode.INFERENCE)
    with pytest.raises(ValueError):
        f.int_value(4)

    assert f.int_value(None) == 0
    assert f.int_value(1) == 1
    assert f.int_value(2) == 2
    assert f.int_value(3) == 3
    assert f.int_value(1) == 1
    assert f.int_value(2) == 2


def test_categorical_bool_feature():
    f = MobiCategoricalBoolFeature("name", optional=False)
    assert f.int_value(False) == 0
    assert f.int_value(True) == 1
    with pytest.raises(ValueError):
        f.int_value(None)

    f = MobiCategoricalBoolFeature("name", optional=True)
    assert f.int_value(None) == 0
    assert f.int_value(False) == 1
    assert f.int_value(True) == 2

    with pytest.raises(ValueError):
        f.int_value("a")

    with pytest.raises(ValueError):
        f.int_value(1.1)

    with pytest.raises(ValueError):
        f.int_value(2)

    with pytest.raises(ValueError):
        f.int_value([1])

    f.set_mode(FeatureMode.INFERENCE)

    assert f.int_value(None) == 0
    assert f.int_value(False) == 1
    assert f.int_value(True) == 2
"""
