from collections import Counter
from datetime import datetime, timedelta
from random import random

from mobi.api.helpers.ranking import rank_products
from mobi.core.models import EventType
from mobi.dao.cache import UserHistoryCookie
from mobi.utils.tests import random_event, random_product


def test_empty_ranking():
    result = rank_products([], UserHistoryCookie())

    assert isinstance(result, list)
    assert not result


def test_one_product():
    product = random_product()

    result = rank_products([(product, 1.0)], UserHistoryCookie())
    assert isinstance(result, list)
    assert len(result) == 1
    assert result[0][0] == product

    result = rank_products([(random_product(product_id="a"), random()) for _ in range(100)], UserHistoryCookie())
    assert isinstance(result, list)
    assert len(result) == 1
    assert result[0][0].product_id == "a"


def test_many_random_products():
    scored_products = [(random_product(brand=str(i), product_id=str(i)), random()) for i in range(100)]

    result = rank_products(scored_products, UserHistoryCookie(), num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20
    for sp1, sp2 in zip(result, result[1:]):
        assert sp1[1] >= sp2[1]


def test_same_brand_as_source_product_in_top():
    same_brand_position = 50
    scored_products = [(random_product(brand=str(i), product_id=str(i)), random()) for i in range(100)]
    source_product = random_product(brand=scored_products[same_brand_position][0].brand)

    # Random score
    result = rank_products(scored_products, UserHistoryCookie(), source_product=source_product, num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    assert result[0][0].brand == source_product.brand or result[1][0].brand == source_product.brand

    # Highest score
    scored_products[same_brand_position] = (scored_products[same_brand_position][0], 2.0)
    result = rank_products(scored_products, UserHistoryCookie(), source_product=source_product, num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    assert result[0][0].brand == source_product.brand

    # Lowest score
    scored_products[same_brand_position] = (scored_products[same_brand_position][0], -1.0)
    result = rank_products(scored_products, UserHistoryCookie(), source_product=source_product, num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    assert result[0][0].brand != source_product.brand
    assert result[1][0].brand == source_product.brand


def test_no_2_visited_products_in_a_row():
    scored_products = [(random_product(brand=str(i), product_id=str(i)), float(100-i)) for i in range(100)]
    events = []
    visited_products = set()
    for i in range(50):
        events.append(random_event(
            product_id=scored_products[i][0].product_id,
            event_type=EventType.PRODUCT_VIEW,
            date=datetime.utcnow() - timedelta(minutes=15)
        ))
        visited_products.add(scored_products[i][0].product_id)

    result = rank_products(scored_products, UserHistoryCookie(events), num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    for sp1, sp2 in zip(result, result[1:]):
        assert sp1[0].product_id not in visited_products or sp2[0].product_id not in visited_products
        assert sp1[0].product_id in visited_products or sp2[0].product_id in visited_products


def test_no_2_same_brands_in_4():
    scored_products = [(random_product(brand=str(i // 40), product_id=str(i)), float(100-i)) for i in range(100)]

    result = rank_products(scored_products, UserHistoryCookie(), num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    for four_products in zip(result, result[1:], result[2:], result[3:]):
        brands_counter = Counter(product.brand for product, _ in four_products)
        assert brands_counter.most_common(1)[0][1] == 2


def test_2_same_as_source_brands_in_top_3_and_3_in_top_5():
    scored_products_cases = [
        [(random_product(brand=str(i // 40), product_id=str(i)), float(100 - i)) for i in range(100)],
        [(random_product(brand=str(i // 40), product_id=str(i)), float(i)) for i in range(100)],
        [(random_product(brand=str(i // 40), product_id=str(i)), random()) for i in range(100)],
    ]

    source_product = random_product(brand=str(1))

    for scored_products in scored_products_cases:
        result = rank_products(scored_products, UserHistoryCookie(), source_product=source_product, num=20)

        assert isinstance(result, list)
        assert len(result) == 20
        assert len({sp[0].product_id for sp in result}) == 20

        brands_in_top_3 = Counter(product.brand for product, _ in result[0:3])
        assert brands_in_top_3[source_product.brand] == 2

        brands_in_top_5 = Counter(product.brand for product, _ in result[0:5])
        assert brands_in_top_5[source_product.brand] == 3


def test_impossible_cases():
    # All products have same brand
    scored_products = [(random_product(brand="a", product_id=str(i)), random()) for i in range(100)]

    result = rank_products(scored_products, UserHistoryCookie(), num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    for sp1, sp2 in zip(result, result[1:]):
        assert sp1[1] >= sp2[1]

    # All products have same brand, except one
    scored_products = [(random_product(brand="a", product_id=str(i)), random()) for i in range(100)]
    scored_products[50] = (random_product(brand="b", product_id="b"), -1.0)

    result = rank_products(scored_products, UserHistoryCookie(), num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    assert result[1][0].brand == "b"
    assert result[1][0].product_id == "b"

    # All products have the same brand as the source product
    scored_products = [(random_product(brand="a", product_id=str(i)), random()) for i in range(100)]
    source_product = random_product(brand="a")

    result = rank_products(scored_products, UserHistoryCookie(), num=20, source_product=source_product)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    # All products already visited
    scored_products = [(random_product(product_id=str(i)), random()) for i in range(100)]
    events = []
    for product, _ in scored_products:
        events.append(random_event(
            event_type=EventType.PRODUCT_VIEW,
            product_id=product.product_id,
            date=datetime.utcnow() - timedelta(minutes=15))
        )

    result = rank_products(scored_products, UserHistoryCookie(events), num=20)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20

    # All together
    scored_products = [(random_product(brand="a", product_id=str(i)), random()) for i in range(100)]
    source_product = random_product(brand="a")
    events = []
    for product, _ in scored_products:
        events.append(random_event(
            event_type=EventType.PRODUCT_VIEW,
            product_id=product.product_id,
            date=datetime.utcnow() - timedelta(minutes=15))
        )

    result = rank_products(scored_products, UserHistoryCookie(events), num=20, source_product=source_product)
    assert isinstance(result, list)
    assert len(result) == 20
    assert len({sp[0].product_id for sp in result}) == 20
