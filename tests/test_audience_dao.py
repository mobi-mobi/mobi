import pytest

from datetime import datetime, timedelta

from mobi.config.system.api.event_api import get_event_api_config
from mobi.core.models import AudienceSplitTree, AudienceSplit, AudienceSplitType
from mobi.dao.audience import MobiAudienceDao
from mobi.dao import get_audience_dao
from mobi.utils.tests import random_audience, random_audiences_union, random_audiences_split_tree


@pytest.fixture(scope="session")
def audience_dao() -> MobiAudienceDao:
    yield get_audience_dao(get_event_api_config())


@pytest.fixture(scope="session")
def client() -> str:
    yield "testclient"


def test_get_dao(audience_dao):
    pass


def test_audiences(audience_dao, client):
    audience_dao.delete_all_audiences(client)
    assert not audience_dao.read_all_audiences(client)

    audience = random_audience()
    audience_dao.create_audience(client, audience)

    assert len(audience_dao.read_all_audiences(client)) == 1

    for _ in range(100):
        audience_dao.create_audience(client, random_audience())

    assert len(list(filter(lambda x: x == audience, audience_dao.read_all_audiences(client)))) == 1

    audience_dao.create_audience(client, random_audience(audience_id=audience.audience_id,
                                                         audience_name="NEW" + audience.audience_name))

    assert len(list(filter(lambda x: x == audience, audience_dao.read_all_audiences(client)))) == 0
    assert len(list(filter(lambda x: x.audience_id == audience.audience_id,
                           audience_dao.read_all_audiences(client)))) == 1

    audience_dao.delete_all_audiences(client)
    assert not audience_dao.read_all_audiences(client)


def test_audiences_unions(audience_dao, client):
    audience_dao.delete_all_audiences_unions(client)
    assert not audience_dao.read_all_audiences_unions(client)

    audiences_union = random_audiences_union()
    audience_dao.create_audiences_union(client, audiences_union)

    assert len(audience_dao.read_all_audiences_unions(client)) == 1
    assert audience_dao.read_all_audiences_unions(client)[0] == audiences_union

    audience_dao.delete_all_audiences_unions(client)
    assert not audience_dao.read_all_audiences_unions(client)

    audiences_union = random_audiences_union(
        audience_ids=[str(i) for i in range(10)]
    )
    audience_dao.create_audiences_union(client, audiences_union)

    assert len(audience_dao.read_all_audiences_unions(client)) == 1
    assert audience_dao.read_all_audiences_unions(client)[0] == audiences_union

    audience_dao.create_audiences_union(client,
                                        random_audiences_union(audiences_union_id=audiences_union.audiences_union_id))

    assert len(audience_dao.read_all_audiences_unions(client)) == 1
    assert audience_dao.read_all_audiences_unions(client)[0] != audiences_union
    assert audience_dao.read_all_audiences_unions(client)[0].audiences_union_id == audiences_union.audiences_union_id
    assert len(audience_dao.read_all_audiences_unions(client)[0].audience_ids) == 2


def test_audiences_split_trees(audience_dao, client):
    audience_dao.delete_all_audience_split_trees(client)
    assert not audience_dao.read_all_audience_split_trees(client)

    split_tree = random_audiences_split_tree()
    AudienceSplitTree.assert_audience_split_tree(split_tree)

    audience_dao.create_audience_split_tree(client, split_tree)
    assert len(audience_dao.read_all_audience_split_trees(client)) == 1
    assert audience_dao.read_all_audience_split_trees(client)[0] == split_tree

    audience_dao.create_audience_split_tree(client, split_tree)
    assert len(audience_dao.read_all_audience_split_trees(client)) == 1
    assert audience_dao.read_all_audience_split_trees(client)[0] == split_tree

    AudienceSplitTree.assert_audience_split_tree(audience_dao.read_all_audience_split_trees(client)[0])
