import pytest
from random import randrange
from mobi.core.population import Population, PopulationSetup


def test_reference_population():
    assert Population.REFERENCE.value == 1
    assert Population["REFERENCE"].value == 1
    assert Population(1).value == 1


def test_bad_populations():
    with pytest.raises(ValueError):
        _ = PopulationSetup({})

    with pytest.raises(ValueError):
        _ = PopulationSetup({Population.REFERENCE: -0.1})

    with pytest.raises(ValueError):
        _ = PopulationSetup({Population.REFERENCE: 0, Population.TARGET: 0.0})


def test_one_population():
    ps = PopulationSetup({Population.REFERENCE: 1})
    for _ in range(0, 1000):
        uid = str(randrange(0, 10 ** 7))
        assert ps[uid] == Population.REFERENCE


def test_two_populations():
    ps = PopulationSetup({Population.REFERENCE: 0.5, Population.TARGET: 0.5})
    for _ in range(0, 1000):
        assert ps[str(randrange(0, 10 ** 7))] in (Population.TARGET, Population.REFERENCE)


def test_total_population_size():
    ps = PopulationSetup({Population.REFERENCE: 0.1, Population.TARGET: 0.2})
    assert abs(ps.total_size - 0.3) < 0.001


def test_same_uid():
    ps = PopulationSetup({Population.REFERENCE: 0.5, Population.TARGET: 0.5})
    for _ in range(0, 1000):
        uid = str(randrange(0, 10 ** 7))
        assert ps[uid] == ps[uid]


def test_population_order():
    ps1 = PopulationSetup({Population.REFERENCE: 0.1, Population.TARGET: 0.9})
    ps2 = PopulationSetup({Population.TARGET: 0.9, Population.REFERENCE: 0.1})

    for _ in range(0, 1000):
        uid = str(randrange(0, 10 ** 7))
        assert ps1[uid] == ps2[uid]
