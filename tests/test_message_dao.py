import pytest
import time

from typing import Generator

from mobi.config.system.api.event_api import get_event_api_config
from mobi.dao.message import MobiMessageDao, WrappedUserEvent
from mobi.dao import get_message_dao
from mobi.utils.tests import random_event, random_string


@pytest.fixture(scope="session")
def message_dao() -> Generator[MobiMessageDao, None, None]:
    yield get_message_dao(get_event_api_config())


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.mark.skip("Skipping as kafka is disabled for now")
def test_get_dao(message_dao):
    pass


@pytest.mark.skip("Skipping as kafka is disabled for now")
def test_send_poll_user_event(client, message_dao):
    time.sleep(0.1)

    consumer_group_id = "TEST_GROUP__" + random_string()

    message_dao.user_events_seek_ts(consumer_group_id, int(time.time() * 1000))

    events = [random_event() for _ in range(100)]

    for event in events:
        message_dao.send_wrapped_user_event(client, WrappedUserEvent(event))

    polled_events = message_dao.poll_wrapped_user_events(consumer_group_id, max_records=10)

    assert polled_events is not None
    assert len(polled_events) == 10

    assert [event.product_id for event in events][0:10] \
           == [wrapped_event.event.product_id for _, wrapped_event in polled_events]
    assert all(map(lambda polled_event: polled_event[0] == client, polled_events))


@pytest.mark.skip("Skipping as kafka is disabled for now")
@pytest.mark.timeout(10, method="thread")
def test_send_subscribe_user_event(client, message_dao):
    time.sleep(0.1)
    current_timestamp = int(time.time() * 1000)

    consumer_group_id = "TEST_GROUP__" + random_string()
    events = [random_event() for _ in range(100)]

    for event in events:
        message_dao.send_wrapped_user_event(client, WrappedUserEvent(event))

    message_dao.user_events_seek_ts(consumer_group_id, current_timestamp)

    polled_events = []
    for polled_client, wrapped_event in message_dao.consume_wrapped_user_events(consumer_group_id):
        assert client == polled_client
        polled_events.append(wrapped_event.event)
        if len(polled_events) == 5:
            break

    assert [event.product_id for event in events][0:5] == [event.product_id for event in polled_events]

    polled_events = []
    for polled_client, wrapped_event in message_dao.consume_wrapped_user_events(consumer_group_id):
        assert client == polled_client
        polled_events.append(wrapped_event.event)
        if len(polled_events) == 5:
            break

    assert [event.product_id for event in events][5:10] == [event.product_id for event in polled_events]


@pytest.mark.skip("Skipping as kafka is disabled for now")
@pytest.mark.timeout(10, method="thread")
def test_send_subscribe_poll_user_event(client, message_dao):
    time.sleep(0.1)
    current_timestamp = int(time.time() * 1000)

    consumer_group_id = "TEST_GROUP__" + random_string()
    events = [random_event() for _ in range(100)]

    for event in events:
        message_dao.send_wrapped_user_event(client, WrappedUserEvent(event))

    message_dao.user_events_seek_ts(consumer_group_id, current_timestamp)

    polled_events = []
    for polled_client, wrapped_event in message_dao.consume_wrapped_user_events(consumer_group_id):
        assert client == polled_client
        polled_events.append(wrapped_event.event)
        if len(polled_events) == 5:
            break

    assert [event.product_id for event in events][0:5] == [event.product_id for event in polled_events]

    polled_events = message_dao.poll_user_events(consumer_group_id, timeout_ms=1000, max_records=5)

    # As polling and subscribing do not work at the same time
    assert not polled_events

    message_dao.user_events_seek_ts(consumer_group_id, current_timestamp)
    polled_events = message_dao.poll_user_events(consumer_group_id, timeout_ms=1000, max_records=7)

    assert [event.product_id for event in events][0:7] == [event.product_id for _, event in polled_events]
