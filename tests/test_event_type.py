import pytest
from mobi.core.models import EventType


def test_product_view():
    assert EventType.PRODUCT_VIEW.value == 1
    assert EventType["PRODUCT_VIEW"].value == 1


def test_wrong_event_type():
    with pytest.raises(KeyError):
        _ = EventType["SOME_UNKNOWN_EVENT_TYPE"]


def test_parse_event_type():
    assert EventType.parse(1) == EventType.PRODUCT_VIEW
    assert EventType.parse("1") == EventType.PRODUCT_VIEW
    assert EventType.parse("VIEW") == EventType.UNKNOWN
    assert EventType.parse("view") == EventType.UNKNOWN
    assert EventType.parse("PRODUCT_VIEW") == EventType.PRODUCT_VIEW
    assert EventType.parse("PRODUCT_view") == EventType.PRODUCT_VIEW

    assert EventType.parse(123) == EventType.UNKNOWN
    assert EventType.parse("123") == EventType.UNKNOWN

    assert EventType.parse("some_unknown_status") == EventType.UNKNOWN
