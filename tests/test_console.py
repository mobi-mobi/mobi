import json
import pytest

from datetime import datetime, timedelta
from flask import testing
from hashlib import sha1
from typing import List, Optional

from mobi.config.system.web.console import get_console_web_config
from mobi.core.models import AudienceSplitType, TokenType, TokenPermission, TokenHashAlgo
from mobi.core.models.api_token import hash_token
from mobi.core.models.zone import Campaign, CampaignStatus, Zone, ZoneStatus
from mobi.dao.audience import MobiAudienceDao
from mobi.dao.auth import MobiAuthDao
from mobi.dao.cache import MobiCache
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.search import MobiSearchDao
from mobi.dao.settings import MobiSettingsDao
from mobi.dao.user_settings import MobiUserSettingsDao, ReportSubscription
from mobi.dao import get_audience_dao, get_auth_dao, get_cache_dao, get_catalog_dao, get_search_dao, get_settings_dao, \
    get_user_settings_dao, MobiDaoException
from mobi.service.task.impl.serch_index_build_task import SearchIndexBuildTask
from mobi.utils.tests import random_audience, random_audiences_union, random_audience_split,\
    random_audiences_split_tree, random_campaign, random_product, random_string, random_zone, random_zone_settings
from mobi.web.console.console import ConsoleApplication


_TEST_EMAIL = "test@e.mail"
_TEST_TOKEN = None


class TestConsoleApplicationClient(testing.FlaskClient):
    @classmethod
    def token(cls) -> Optional[str]:
        global _TEST_TOKEN
        return _TEST_TOKEN

    def open(self, *args, **kwargs):
        if self.token() is not None:
            headers = kwargs.get('headers', {})
            headers['Auth-Token'] = self.token()
            kwargs['headers'] = headers
        return super().open(*args, **kwargs)


@pytest.fixture(scope="session")
def client() -> str:
    yield "testclient"


@pytest.fixture(scope="session")
def user() -> str:
    yield "testuser"


@pytest.fixture(scope="session")
def audience_dao() -> MobiAudienceDao:
    yield get_audience_dao(get_console_web_config())


@pytest.fixture(scope="session")
def auth_dao() -> MobiAuthDao:
    yield get_auth_dao(get_console_web_config())


@pytest.fixture(scope="session")
def cache_dao() -> MobiCache:
    yield get_cache_dao(get_console_web_config())


@pytest.fixture(scope="session")
def catalog_dao() -> MobiCatalogDao:
    yield get_catalog_dao(get_console_web_config())


@pytest.fixture
def search_dao(client) -> MobiSearchDao:
    dao = get_search_dao(get_console_web_config())
    dao.delete_index(client)
    dao.create_index(client)
    yield dao
    dao.delete_index(client)


@pytest.fixture(scope="session")
def user_settings_dao() -> MobiUserSettingsDao:
    yield get_user_settings_dao(get_console_web_config())


@pytest.fixture(scope="session")
def settings_dao() -> MobiSettingsDao:
    yield get_settings_dao(get_console_web_config())


@pytest.fixture(scope="session")
def console_application() -> ConsoleApplication:
    return ConsoleApplication("app", "host", port=0)


@pytest.fixture(scope="session")
def console_application_client(client, console_application, auth_dao):
    global _TEST_TOKEN

    console_application.app.test_client_class = TestConsoleApplicationClient

    try:
        auth_dao.create_user(_TEST_EMAIL, client)
    except MobiDaoException:
        pass

    auth_dao.add_user_to_client(_TEST_EMAIL, client)
    token = auth_dao.create_session_token(_TEST_EMAIL, datetime.utcnow() + timedelta(hours=1))
    _TEST_TOKEN = token

    console_client = console_application.app.test_client()
    yield console_client

    auth_dao.delete_session_token(token)
    auth_dao.delete_user(_TEST_EMAIL)


def test_get_zones(client, settings_dao, console_application_client):
    settings_dao.delete_all_zones(client)
    rv = console_application_client.get(f"/api/{client}/zones")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert not data["data"]

    zone_1 = random_zone(name="Zone Zone1")
    zone_2 = random_zone(name="Zone Zone2")
    settings_dao.create_zone(client, zone_1)
    settings_dao.create_zone(client, zone_2)

    rv = console_application_client.get(f"/api/{client}/zones/num")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["value"] == 2

    rv = console_application_client.get(f"/api/{client}/zones")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    def sorted_zones(zones: List[Zone]):
        return sorted(zones, key=lambda zone: zone.zone_id)

    assert sorted_zones([zone_1, zone_2])[0] \
        == sorted_zones([Zone.from_dict(data["data"][0]), Zone.from_dict(data["data"][1])])[0]
    assert sorted_zones([zone_1, zone_2])[1] \
        == sorted_zones([Zone.from_dict(data["data"][0]), Zone.from_dict(data["data"][1])])[1]

    # Testing search

    rv = console_application_client.get(f"/api/{client}/zones?q=someunexistingzonename")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 0

    rv = console_application_client.get(f"/api/{client}/zones?q=zone1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = console_application_client.get(f"/api/{client}/zones?q=zone")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    # Testing offsets

    rv = console_application_client.get(f"/api/{client}/zones?offset=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = console_application_client.get(f"/api/{client}/zones?limit=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    zone_1.status = ZoneStatus.DISABLED
    settings_dao.create_zone(client, zone_1)

    rv = console_application_client.get(f"/api/{client}/zones/num")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["value"] == 1

    rv = console_application_client.get(f"/api/{client}/zones/num?include_inactive=yes")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["value"] == 2

    rv = console_application_client.get(f"/api/{client}/zones/num?include_inactive=yes?q=zone1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["value"] == 1

    rv = console_application_client.get(f"/api/{client}/zones")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = console_application_client.get(f"/api/{client}/zones?include_inactive=yes")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    assert sorted_zones([zone_1, zone_2])[0] \
        == sorted_zones([Zone.from_dict(data["data"][0]), Zone.from_dict(data["data"][1])])[0]
    assert sorted_zones([zone_1, zone_2])[1] \
        == sorted_zones([Zone.from_dict(data["data"][0]), Zone.from_dict(data["data"][1])])[1]

    settings_dao.delete_all_zones(client)

    rv = console_application_client.get(f"/api/{client}/zones")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert not data["data"]


def test_create_zone(client, settings_dao, console_application_client):
    settings_dao.delete_all_zones(client)
    zone = random_zone()
    zone.zone_id = None

    data = zone.as_json_dict()
    rv = console_application_client.post(f"/api/{client}/zone", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    zones = list(settings_dao.read_all_zones(client))
    assert len(zones) == 1
    assert zones[0] != zone
    assert zones[0].zone_id is not None

    d1 = zones[0].as_dict()
    del d1["zone_id"]
    d2 = zone.as_dict()
    del d2["zone_id"]
    assert d1 == d2


def test_get_zone_metrics(client, settings_dao, console_application_client):
    settings_dao.delete_all_zones(client)

    rv = console_application_client.get(f"/api/{client}/zone/{random_string()}/metrics")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    zone = random_zone()
    settings_dao.create_zone(client, zone)

    rv = console_application_client.get(f"/api/{client}/zone/{zone.zone_id}/metrics")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/zone/{zone.zone_id}/metrics?metrics_from=28d")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_get_campaign_metrics(client, settings_dao, console_application_client):
    settings_dao.delete_all_campaigns(client)

    rv = console_application_client.get(f"/api/{client}/campaign/{random_string()}/metrics")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    campaign = random_campaign()
    settings_dao.create_campaign(client, campaign)

    rv = console_application_client.get(f"/api/{client}/campaign/{campaign.campaign_id}/metrics")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/campaign/{campaign.campaign_id}/metrics?metrics_from=28d")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_update_zone(client, settings_dao, console_application_client):
    settings_dao.delete_all_zones(client)
    zone = random_zone()
    data = zone.as_json_dict()
    rv = console_application_client.post(f"/api/{client}/zone", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    zones = list(settings_dao.read_all_zones(client))
    assert len(zones) == 1
    assert zones[0] == zone

    zone.status = ZoneStatus.DISABLED
    zone.description = "Some Description"
    zone.name = "Some Name"

    data = zone.as_json_dict()
    rv = console_application_client.post(f"/api/{client}/zone", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    zones = list(settings_dao.read_all_zones(client, include_inactive=True))
    assert len(zones) == 1
    assert zones[0] == zone


def test_delete_zone(client, settings_dao, console_application_client):
    settings_dao.delete_all_zones(client)
    zone = random_zone()
    settings_dao.create_zone(client, zone)
    assert len(list(settings_dao.read_all_zones(client))) == 1

    rv = console_application_client.delete(f"/api/{client}/zone/{zone.zone_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    assert not list(settings_dao.read_all_zones(client))


def test_get_campaigns(client, settings_dao, console_application_client):
    settings_dao.delete_all_campaigns(client)
    rv = console_application_client.get(f"/api/{client}/campaigns")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert not data["data"]

    campaign_1 = random_campaign()
    campaign_2 = random_campaign()
    settings_dao.create_campaign(client, campaign_1)
    settings_dao.create_campaign(client, campaign_2)

    rv = console_application_client.get(f"/api/{client}/campaigns")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    def sorted_campaigns(campaigns: List[Campaign]):
        return sorted(campaigns, key=lambda campaign: campaign.campaign_id)

    assert sorted_campaigns([campaign_1, campaign_2])[0] \
        == sorted_campaigns([Campaign.from_dict(data["data"][0]), Campaign.from_dict(data["data"][1])])[0]
    assert sorted_campaigns([campaign_1, campaign_2])[1] \
        == sorted_campaigns([Campaign.from_dict(data["data"][0]), Campaign.from_dict(data["data"][1])])[1]

    campaign_1.status = CampaignStatus.DISABLED
    settings_dao.create_campaign(client, campaign_1)

    rv = console_application_client.get(f"/api/{client}/campaigns")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    assert sorted_campaigns([campaign_1, campaign_2])[0] \
        == sorted_campaigns([Campaign.from_dict(data["data"][0]), Campaign.from_dict(data["data"][1])])[0]
    assert sorted_campaigns([campaign_1, campaign_2])[1] \
        == sorted_campaigns([Campaign.from_dict(data["data"][0]), Campaign.from_dict(data["data"][1])])[1]

    settings_dao.delete_all_campaigns(client)

    rv = console_application_client.get(f"/api/{client}/campaigns")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert not data["data"]


def test_get_campaigns_full(client, settings_dao, console_application_client):
    settings_dao.delete_all_campaigns(client)
    rv = console_application_client.get(f"/api/{client}/campaigns/full")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert not data["data"]

    campaign_1 = random_campaign(name="campaign1")
    campaign_2 = random_campaign(name="campaign2")
    settings_dao.create_campaign(client, campaign_1)
    settings_dao.create_campaign(client, campaign_2)

    rv = console_application_client.get(f"/api/{client}/campaigns/full")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    # Testing search

    rv = console_application_client.get(f"/api/{client}/campaigns/full?q=campaign")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = console_application_client.get(f"/api/{client}/campaigns/full?q=campaign1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    # Testing offset

    rv = console_application_client.get(f"/api/{client}/campaigns/full?q=campaign&offset=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = console_application_client.get(f"/api/{client}/campaigns/full?q=campaign1&offset=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 0

    # Testing limit

    rv = console_application_client.get(f"/api/{client}/campaigns/full?q=campaign&limit=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = console_application_client.get(f"/api/{client}/campaigns/full?q=campaign1&limit=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1


def test_create_campaign(client, settings_dao, console_application_client):
    settings_dao.delete_all_campaigns(client)
    campaign = random_campaign(
        start_date=datetime.utcnow() + timedelta(days=1),
        end_date=datetime.utcnow() + timedelta(days=2)
    )
    campaign.campaign_id = None

    data = campaign.as_json_dict()
    rv = console_application_client.post(f"/api/{client}/campaign", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    campaigns = list(settings_dao.read_all_campaigns(client))
    assert len(campaigns) == 1
    assert campaigns[0] != campaign
    assert campaigns[0].campaign_id is not None

    d1 = campaigns[0].as_json_dict()
    del d1["campaign_id"]
    d2 = campaign.as_json_dict()
    del d2["campaign_id"]
    assert d1 == d2


def test_update_campaign(client, settings_dao, console_application_client):
    settings_dao.delete_all_campaigns(client)
    campaign = random_campaign()
    data = campaign.as_json_dict()
    rv = console_application_client.post(f"/api/{client}/campaign", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    campaigns = list(settings_dao.read_all_campaigns(client))
    assert len(campaigns) == 1
    assert campaigns[0] == campaign

    campaign.status = CampaignStatus.DISABLED
    campaign.description = "Some Description"
    campaign.name = "Some Name"

    data = campaign.as_json_dict()
    rv = console_application_client.post(f"/api/{client}/campaign", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    campaigns = list(settings_dao.read_all_campaigns(client, include_inactive=True))
    assert len(campaigns) == 1
    assert campaigns[0] == campaign


def test_delete_campaign(client, settings_dao, console_application_client):
    settings_dao.delete_all_campaigns(client)
    campaign = random_campaign()
    settings_dao.create_campaign(client, campaign)
    assert len(list(settings_dao.read_all_campaigns(client))) == 1

    settings_dao.delete_all_zones(client)
    zone = random_zone()
    settings_dao.create_zone(client, zone)
    assert len(list(settings_dao.read_all_zones(client))) == 1

    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign.campaign_id)
    zone = settings_dao.read_zone(client, zone.zone_id)
    assert len(zone.campaign_ids) == 1
    assert zone.campaign_ids[0] == campaign.campaign_id

    rv = console_application_client.delete(f"/api/{client}/campaign/{campaign.campaign_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    assert not list(settings_dao.read_all_campaigns(client))

    zone = settings_dao.read_zone(client, zone.zone_id)
    assert not zone.campaign_ids


def test_assignment(client, settings_dao, console_application_client):
    settings_dao.delete_all_campaigns(client)
    campaign = random_campaign()
    settings_dao.create_campaign(client, campaign)
    assert len(list(settings_dao.read_all_campaigns(client))) == 1

    settings_dao.delete_all_zones(client)
    zone = random_zone()
    settings_dao.create_zone(client, zone)
    assert len(list(settings_dao.read_all_zones(client))) == 1

    rv = console_application_client.post(f"/api/{client}/zone/{zone.zone_id}/add_campaign/{campaign.campaign_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    zone = settings_dao.read_zone(client, zone.zone_id)
    assert len(zone.campaign_ids) == 1
    assert zone.campaign_ids[0] == campaign.campaign_id

    rv = console_application_client.delete(f"/api/{client}/zone/{zone.zone_id}/remove_campaign/{campaign.campaign_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    zone = settings_dao.read_zone(client, zone.zone_id)
    assert not zone.campaign_ids


def test_closest_campaign(client, settings_dao, console_application_client):
    settings_dao.delete_all_campaigns(client)
    current_campaign = random_campaign(name="Current", start_date=datetime.utcnow() - timedelta(hours=1),
                                       end_date=datetime.utcnow() + timedelta(hours=1))
    settings_dao.create_campaign(client, current_campaign)
    assert len(list(settings_dao.read_all_campaigns(client))) == 1

    next_campaign = random_campaign(name="Next", start_date=datetime.utcnow() + timedelta(minutes=1),
                                    end_date=datetime.utcnow() + timedelta(hours=1))
    settings_dao.create_campaign(client, next_campaign)
    assert len(list(settings_dao.read_all_campaigns(client))) == 2

    settings_dao.delete_all_zones(client)
    zone = random_zone()
    settings_dao.create_zone(client, zone)
    assert len(list(settings_dao.read_all_zones(client))) == 1

    rv = console_application_client.get(f"/api/{client}/zone/{zone.zone_id}/campaign/closest")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"
    assert "does not have active campaigns" in data["error"]["description"]

    rv = console_application_client.get(f"/api/{client}/zones/full")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]
    assert data["data"][0]["closest_campaign"] is None
    assert data["data"][0]["active_campaign"] is None
    assert data["data"][0]["next_campaign"] is None

    settings_dao.add_campaign_to_zone(client, zone.zone_id, next_campaign.campaign_id)

    rv = console_application_client.get(f"/api/{client}/zone/{zone.zone_id}/campaign/closest")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"] == next_campaign.as_json_dict()

    rv = console_application_client.get(f"/api/{client}/zones/full")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]
    assert data["data"][0]["closest_campaign"] == next_campaign.as_json_dict()
    assert data["data"][0]["active_campaign"] is None
    assert data["data"][0]["next_campaign"] == next_campaign.as_json_dict()

    settings_dao.add_campaign_to_zone(client, zone.zone_id, current_campaign.campaign_id)

    rv = console_application_client.get(f"/api/{client}/zone/{zone.zone_id}/campaign/closest")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"] == current_campaign.as_json_dict()

    rv = console_application_client.get(f"/api/{client}/zones/full")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]
    assert data["data"][0]["closest_campaign"] == current_campaign.as_json_dict()
    assert data["data"][0]["active_campaign"] == current_campaign.as_json_dict()
    assert data["data"][0]["next_campaign"] == next_campaign.as_json_dict()


def test_zone_applied_settings(client, settings_dao, console_application_client):
    settings_dao.delete_all_zones(client)
    zone = random_zone(
        zone_settings=random_zone_settings(
            recommendations_num=10,
            same_brand=True,
            brands=["b1"],
            categories=["c1"],
            extra_product_ids=None
        )
    )
    settings_dao.create_zone(client, zone)
    assert len(list(settings_dao.read_all_zones(client))) == 1

    settings_dao.delete_all_campaigns(client)
    campaign = random_campaign(
        zone_settings=random_zone_settings(
            recommendations_num=20,
            same_brand=False,
            brands=["b2"],
            categories=["c2"],
            extra_product_ids=["p2"]
        )
    )
    settings_dao.create_campaign(client, campaign)
    assert len(list(settings_dao.read_all_campaigns(client))) == 1

    rv = console_application_client.get(f"/api/{client}/zone/{zone.zone_id}/apply_campaign/{campaign.campaign_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    zone_meta_returned = data["data"]
    zone_settings_returned = data["data"]["zone_settings"]
    del zone_meta_returned["zone_settings"]

    zone_meta_original = zone.as_json_dict()
    zone_settings_original = zone.as_json_dict()["zone_settings"]
    del zone_meta_original["zone_settings"]

    assert zone_meta_original == zone_meta_returned
    assert zone_settings_original != zone_settings_returned
    for param in ["recommendations_num", "same_brand", "brands", "categories", "extra_product_ids"]:
        assert zone_settings_original[param] != zone_settings_returned[param]
    assert zone_settings_returned == campaign.zone_settings.as_json_dict()


def test_get_zone_stats(client, settings_dao, console_application_client):
    settings_dao.delete_all_zones(client)
    zone = random_zone()
    settings_dao.create_zone(client, zone)
    assert len(list(settings_dao.read_all_zones(client))) == 1

    rv = console_application_client.get(f"/api/{client}/zone/{zone.zone_id}/stats")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    # As it's just a stub for now
    assert data["data"]["displays"] == 10
    assert data["data"]["ctr"] == 4.3
    assert data["data"]["converted"] == 55.1


def test_get_brands(client, cache_dao, search_dao, console_application_client):
    search_dao.index_brand(client, "brand1")
    search_dao.index_brand(client, "brand2")
    search_dao.index_brand(client, "brand3")
    search_dao.refresh_index(client)

    rv = console_application_client.get(f"/api/{client}/catalog/brands")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["brands"]) == ["brand1", "brand2", "brand3"]

    search_dao.index_brand(client, "brand4")
    search_dao.refresh_index(client)

    # Nothing changed as cache is not invalidated

    rv = console_application_client.get(f"/api/{client}/catalog/brands")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["brands"]) == ["brand1", "brand2", "brand3"]

    # But changes if we use search query

    rv = console_application_client.get(f"/api/{client}/catalog/brands?q=brand")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["brands"]) == ["brand1", "brand2", "brand3", "brand4"]

    # Different prefixes

    search_dao.index_brand(client, "notabrand")
    search_dao.refresh_index(client)

    rv = console_application_client.get(f"/api/{client}/catalog/brands?q=brand")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["brands"]) == ["brand1", "brand2", "brand3", "brand4"]

    rv = console_application_client.get(f"/api/{client}/catalog/brands?q=not")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["brands"]) == ["notabrand"]


def test_get_categories(client, cache_dao, search_dao, console_application_client):
    search_dao.index_category(client, "category1")
    search_dao.index_category(client, "category2")
    search_dao.index_category(client, "category3")
    search_dao.refresh_index(client)

    rv = console_application_client.get(f"/api/{client}/catalog/categories")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["categories"]) == ["category1", "category2", "category3"]

    search_dao.index_category(client, "category4")
    search_dao.refresh_index(client)

    # Nothing changed as cache is not invalidated

    rv = console_application_client.get(f"/api/{client}/catalog/categories")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["categories"]) == ["category1", "category2", "category3"]

    # But changes if we use search query

    rv = console_application_client.get(f"/api/{client}/catalog/categories?q=category")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["categories"]) == ["category1", "category2", "category3", "category4"]

    # Different prefixes

    search_dao.index_category(client, "notacategory")
    search_dao.refresh_index(client)

    rv = console_application_client.get(f"/api/{client}/catalog/categories?q=category")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["categories"]) == ["category1", "category2", "category3", "category4"]

    rv = console_application_client.get(f"/api/{client}/catalog/categories?q=not")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert sorted(data["data"]["categories"]) == ["notacategory"]


def test_search_products(client, catalog_dao, console_application_client):
    catalog_dao.delete_all_products(client)

    product = random_product(title="Test Product")
    inactive_product = random_product(title="Also test product", active=False)
    catalog_dao.create_product(client, product)
    catalog_dao.create_product(client, inactive_product)

    global_context = dict()
    client_context = dict()
    search_builder_task = SearchIndexBuildTask("SearchIndexBuildTask", [client])
    search_builder_task.process_client(client, global_context, client_context)

    # Test search

    rv = console_application_client.get(f"/api/{client}/catalog/products")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] is None
    assert len(data["data"]["products"]) == 1
    rv = console_application_client.get(f"/api/{client}/catalog/products?include_inactive=yes")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] is None
    assert len(data["data"]["products"]) == 2

    rv = console_application_client.get(f"/api/{client}/catalog/products?q=test")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 1
    assert len(data["data"]["products"]) == 1
    rv = console_application_client.get(f"/api/{client}/catalog/products?q=test&include_inactive=yes")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 2
    assert len(data["data"]["products"]) == 2

    rv = console_application_client.get(f"/api/{client}/catalog/products?q=te")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 1
    assert len(data["data"]["products"]) == 1
    rv = console_application_client.get(f"/api/{client}/catalog/products?q=te&include_inactive=yes")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 2
    assert len(data["data"]["products"]) == 2

    rv = console_application_client.get(f"/api/{client}/catalog/products?q=LSO")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 0
    assert len(data["data"]["products"]) == 0
    rv = console_application_client.get(f"/api/{client}/catalog/products?q=LSO&include_inactive=yes")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 0
    assert len(data["data"]["products"]) == 0

    rv = console_application_client.get(f"/api/{client}/catalog/products?q=test  product")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 1
    assert len(data["data"]["products"]) == 1
    rv = console_application_client.get(f"/api/{client}/catalog/products?q=test  product&include_inactive=yes")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 2
    assert len(data["data"]["products"]) == 2
    rv = console_application_client.get(f"/api/{client}/catalog/products?q=test  product&include_inactive=yes&offset=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 2
    assert len(data["data"]["products"]) == 1
    rv = console_application_client.get(f"/api/{client}/catalog/products?q=test  product&include_inactive=yes&limit=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 2
    assert len(data["data"]["products"]) == 1

    rv = console_application_client.get(f"/api/{client}/catalog/products?q=also  product")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 1
    assert len(data["data"]["products"]) == 1
    rv = console_application_client.get(f"/api/{client}/catalog/products?q=also  product&include_inactive=yes")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 2
    assert len(data["data"]["products"]) == 2
    rv = console_application_client.get(f"/api/{client}/catalog/products?q=also  product&include_inactive=yes&offset=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert data["data"]["total"] == 2
    assert len(data["data"]["products"]) == 1


def test_get_ab_test_metrics(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/abtest/metric/to_basket_num?period=14d"
                                        f"&pop_a=reference&pop_b=target&days=25")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_main_page__real_time(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/main/real-time")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_main_page__audience_info(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/main/audience-info")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_main_page__reco_conversions(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/main/recommendations-conversion")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_main_page__traffic_info(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/main/traffic-info")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_main_page__traffic_source(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/main/traffic-source")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_main_page__incremental_stats(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/main/incremental-stats")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_main_page__integration_stats(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/main/integration-stats")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_main_page__conversion_stats(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/main/conversion-stats")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_get_catalog_brands_page(client, console_application_client):
    rv = console_application_client.get(f"/api/{client}/pages/catalog/brands")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_unsubscribe(client, console_application_client, user, user_settings_dao):
    rv = console_application_client.get(f"/unsubscribe")
    assert rv.status == "200 OK"
    response = rv.data.decode("utf-8")
    assert "Unsubscribed" in response

    user_settings_dao.delete_all_properties(user)
    assert user_settings_dao.read_report_subscriptions_list(user, client) == []
    user_settings_dao.add_report_subscription(user, client, ReportSubscription("daily"))
    assert user_settings_dao.read_report_subscriptions_list(user, client) == [ReportSubscription("daily")]

    checksum_str = "mobimobi" \
                   + "<user>" + user + "</user>" \
                   + "<subscription>" + "report" + "</subscription>" \
                   + "<subscription_type>" + "daily" + "</subscription_type>" \
                   + "<subscription_name>" + "subscription name" + "</subscription_name>" \
                   + "<client>" + client + "</client>" \
                   + "tech"

    checksum = sha1(bytes(checksum_str, "utf-8")).hexdigest()

    rv = console_application_client.get(f"/unsubscribe?user={user}&client={client}&subscription=report"
                                        f"&subscription_type=daily&checksum=wrong_{checksum}"
                                        f"&subscription_name=subscription+name")
    assert rv.status == "200 OK"
    response = rv.data.decode("utf-8")
    assert "link is not valid or expired" in response

    assert user_settings_dao.read_report_subscriptions_list(user, client) == [ReportSubscription("daily")]

    rv = console_application_client.get(f"/unsubscribe?user={user}&client={client}&subscription=report"
                                        f"&subscription_type=daily&checksum={checksum}"
                                        f"&subscription_name=subscription+name")
    assert rv.status == "200 OK"
    response = rv.data.decode("utf-8")
    assert "Unsubscribed" in response

    assert user_settings_dao.read_report_subscriptions_list(user, client) == []


def test_get_update_user_data(client, console_application_client, auth_dao):
    user = auth_dao.read_user(_TEST_EMAIL)

    rv = console_application_client.get("/api/user/settings/account")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    assert data["data"]["name"] == user["name"]
    assert data["data"]["email"] == user["email"]

    rv = console_application_client.put("/api/user/settings/account")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"
    assert data["error"]["description"] == "Can not parse post data"

    rv = console_application_client.put("/api/user/settings/account", data=json.dumps([]))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"
    assert data["error"]["description"] == "Can not parse post data"

    rv = console_application_client.put("/api/user/settings/account", data=json.dumps({"name": ""}))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"
    assert data["error"]["description"] == "Name should not be empty and should contain no more than 100 symbols"

    rv = console_application_client.put("/api/user/settings/account", data=json.dumps({"name": "testclient"}))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"


def test_change_password(console_application_client, auth_dao):
    auth_dao.update_user_password(_TEST_EMAIL, "old_password")
    assert not auth_dao.check_user_password(_TEST_EMAIL, "12345678")

    rv = console_application_client.put(f"/api/user/settings/password", data=json.dumps([]))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"
    assert data["error"]["description"] == "Can not parse post data"

    data = {}

    rv = console_application_client.put(f"/api/user/settings/password", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"
    assert data["error"]["description"] == "New password is not set"

    data = {"new_password": "a"}

    rv = console_application_client.put(f"/api/user/settings/password", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"
    assert data["error"]["description"] == "New passwords should match"

    data = {"new_password": "a", "new_password_repeated": "a"}

    rv = console_application_client.put(f"/api/user/settings/password", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"
    assert data["error"]["description"] == "New password should be at least 8 symbols long"

    data = {"new_password": "12345678", "new_password_repeated": "12345678"}

    rv = console_application_client.put(f"/api/user/settings/password", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    assert auth_dao.check_user_password(_TEST_EMAIL, "12345678")


def test_get_delete_add_subscriptions(client, console_application_client, user_settings_dao):
    for report in user_settings_dao.read_report_subscriptions_list(_TEST_EMAIL, client):
        user_settings_dao.remove_report_subscription(_TEST_EMAIL, client, report)

    assert not user_settings_dao.read_report_subscriptions_list(_TEST_EMAIL, client)

    rv = console_application_client.get(f"/api/user/settings/subscriptions")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert client in data["data"]["current_subscriptions"]
    assert client in data["data"]["can_subscribe_to"]
    assert not data["data"]["current_subscriptions"][client]
    assert data["data"]["can_subscribe_to"][client]

    parameters = data["data"]["can_subscribe_to"][client][0]["subscribe_parameters"]
    query_str = "&".join(f"{key}={value}" for key, value in parameters.items())

    # Delete unexisting usbscription

    rv = console_application_client.delete(f"/api/user/settings/subscriptions?{query_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    # Subscribe

    rv = console_application_client.post(f"/api/user/settings/subscriptions?{query_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/user/settings/subscriptions")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert client in data["data"]["current_subscriptions"]
    assert client in data["data"]["can_subscribe_to"]
    assert data["data"]["current_subscriptions"][client]
    assert data["data"]["can_subscribe_to"][client]

    # Delete existing subscription

    rv = console_application_client.delete(f"/api/user/settings/subscriptions?{query_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/user/settings/subscriptions")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert client in data["data"]["current_subscriptions"]
    assert client in data["data"]["can_subscribe_to"]
    assert not data["data"]["current_subscriptions"][client]
    assert data["data"]["can_subscribe_to"][client]


def test_tokens(client, console_application_client, user, settings_dao):
    settings_dao.delete_all_tokens(client)
    legacy_tokens = [token for token in settings_dao.read_tokens(client) if token.description is not None
                     and token.description == "Legacy token. Can not be deleted"]
    assert len(settings_dao.read_tokens(client)) == len(legacy_tokens)

    rv = console_application_client.get(f"/api/user/settings/tokens")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert isinstance(data["data"][client], list)
    assert len(data["data"][client]) == len(legacy_tokens)

    # Deleting non-existing

    rv = console_application_client.delete(f"/api/user/settings/tokens?client={client}&token_hash=__non_existing__")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    # Creating new token

    token_description = random_string()
    data = {
        "token_type": TokenType.TEST.name.lower(),
        "token_permissions": [TokenPermission.GET_RECOMMENDATIONS.name.lower(),
                              TokenPermission.SEND_EVENTS.name.lower()],
        "description": token_description
    }
    rv = console_application_client.post(f"/api/user/settings/tokens?client={client}", data=json.dumps(data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    token_str = data["data"]["token_str"]

    rv = console_application_client.get(f"/api/user/settings/tokens")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"][client]) == 1 + len(legacy_tokens)
    created_token = None
    for token in data["data"][client]:
        if token["description"] != "Legacy token. Can not be deleted":
            created_token = token
            break
    assert created_token is not None
    assert created_token["first_letters"] == token_str[0:4]
    assert created_token["token_type"] == "test"
    assert created_token["token_hash"] == hash_token(token_str, TokenHashAlgo.SHA256)
    assert created_token["description"] == token_description
    assert len(data["data"][client][0]["permissions"]) == 2

    # Deleting existing token

    rv = console_application_client.delete(f"/api/user/settings/tokens?client={client}&token_hash="
                                           f"{hash_token(token_str, TokenHashAlgo.SHA256)}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/user/settings/tokens")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert isinstance(data["data"][client], list)
    assert len(data["data"][client]) == len(legacy_tokens)


def test_audience(client, console_application_client, audience_dao):
    audience_dao.delete_all_audiences(client)

    rv = console_application_client.get(f"/api/{client}/audiences")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    audience_id = random_string()

    rv = console_application_client.get(f"/api/{client}/audiences?audience_id={audience_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    audience = random_audience(audience_id=audience_id)

    rv = console_application_client.post(f"/api/{client}/audiences", data=json.dumps(audience.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audiences?audience_id={audience_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.post(f"/api/{client}/audiences", data=json.dumps(audience.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    audience_id = "other" + audience_id
    audience = random_audience(audience_id=audience_id)

    rv = console_application_client.put(f"/api/{client}/audiences", data=json.dumps(audience.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = console_application_client.post(f"/api/{client}/audiences", data=json.dumps(audience.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.put(f"/api/{client}/audiences", data=json.dumps(audience.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    # Creating with wrong data

    audience_id = "one more" + audience_id
    audience = random_audience(audience_id=audience_id)

    request_data = audience.as_json_dict()
    del request_data["audience_id"]

    rv = console_application_client.post(f"/api/{client}/audiences", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audience.as_json_dict()
    del request_data["audience_name"]

    rv = console_application_client.post(f"/api/{client}/audiences", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audience.as_json_dict()
    del request_data["audience_description"]

    rv = console_application_client.post(f"/api/{client}/audiences", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audience.as_json_dict()
    request_data["unknown_parameter"] = "unknown value"

    rv = console_application_client.post(f"/api/{client}/audiences", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    # Reading all audiences

    rv = console_application_client.get(f"/api/{client}/audiences/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = console_application_client.post(f"/api/{client}/audiences", data=json.dumps(audience.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audiences/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    # Deleting audience

    rv = console_application_client.delete(f"/api/{client}/audiences?audience_id=__unknown_audience_id__")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audiences/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    rv = console_application_client.delete(f"/api/{client}/audiences?audience_id={audience_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audiences/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = console_application_client.get(f"/api/{client}/audiences?audience_id={audience_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"


def test_audiences_union(client, console_application_client, audience_dao):
    audience_dao.delete_all_audiences_unions(client)

    rv = console_application_client.get(f"/api/{client}/audiences_unions")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    audiences_union_id = random_string()

    rv = console_application_client.get(f"/api/{client}/audiences_unions?audience_id={audiences_union_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    audiences_union = random_audiences_union(audiences_union_id=audiences_union_id)

    rv = console_application_client.post(f"/api/{client}/audiences_unions",
                                         data=json.dumps(audiences_union.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audiences_unions?audiences_union_id={audiences_union_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.post(f"/api/{client}/audiences_unions",
                                         data=json.dumps(audiences_union.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    audiences_union_id = "other" + audiences_union_id
    audiences_union = random_audiences_union(audiences_union_id=audiences_union_id)

    rv = console_application_client.put(f"/api/{client}/audiences_unions",
                                        data=json.dumps(audiences_union.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = console_application_client.post(f"/api/{client}/audiences_unions",
                                         data=json.dumps(audiences_union.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.put(f"/api/{client}/audiences_unions",
                                        data=json.dumps(audiences_union.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    # Creating with wrong data

    audiences_union_id = "one more" + audiences_union_id
    audiences_union = random_audiences_union(audiences_union_id=audiences_union_id)

    request_data = audiences_union.as_json_dict()
    del request_data["audiences_union_id"]

    rv = console_application_client.post(f"/api/{client}/audiences_unions", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audiences_union.as_json_dict()
    del request_data["audiences_union_name"]

    rv = console_application_client.post(f"/api/{client}/audiences_unions", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audiences_union.as_json_dict()
    del request_data["audiences_union_description"]

    rv = console_application_client.post(f"/api/{client}/audiences_unions", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audiences_union.as_json_dict()
    request_data["unknown_parameter"] = "unknown value"

    rv = console_application_client.post(f"/api/{client}/audiences_unions", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    # Reading all audiences

    rv = console_application_client.get(f"/api/{client}/audiences_unions/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = console_application_client.post(f"/api/{client}/audiences_unions",
                                         data=json.dumps(audiences_union.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audiences_unions/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    # Deleting audience

    rv = console_application_client.delete(f"/api/{client}/audiences_unions?audiences_union_id=__unknown_audience_id__")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audiences_unions/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    rv = console_application_client.delete(f"/api/{client}/audiences_unions?audiences_union_id={audiences_union_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audiences_unions/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = console_application_client.get(f"/api/{client}/audiences_unions?audiences_union_id={audiences_union_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"


def test_audience_split_tree(client, console_application_client, audience_dao):
    audience_dao.delete_all_audience_split_trees(client)

    rv = console_application_client.get(f"/api/{client}/audience_split_trees")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    split_tree_id = random_string()

    rv = console_application_client.get(f"/api/{client}/audience_split_trees?split_tree_id={split_tree_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    audience_split_tree = random_audiences_split_tree(split_tree_id=split_tree_id)

    _ff = audience_split_tree.as_json_dict()
    rv = console_application_client.post(f"/api/{client}/audience_split_trees",
                                         data=json.dumps(audience_split_tree.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audience_split_trees?split_tree_id={split_tree_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.post(f"/api/{client}/audience_split_trees",
                                         data=json.dumps(audience_split_tree.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    split_tree_id = "other" + split_tree_id
    audience_split_tree = random_audiences_split_tree(split_tree_id=split_tree_id)

    rv = console_application_client.put(f"/api/{client}/audience_split_trees",
                                        data=json.dumps(audience_split_tree.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = console_application_client.post(f"/api/{client}/audience_split_trees",
                                         data=json.dumps(audience_split_tree.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.put(f"/api/{client}/audience_split_trees",
                                        data=json.dumps(audience_split_tree.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    # Creating with wrong data

    split_tree_id = "one more" + split_tree_id
    audience_split_tree = random_audiences_split_tree(split_tree_id=split_tree_id)

    request_data = audience_split_tree.as_json_dict()
    del request_data["split_tree_id"]

    rv = console_application_client.post(f"/api/{client}/audience_split_trees", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audience_split_tree.as_json_dict()
    del request_data["name"]

    rv = console_application_client.post(f"/api/{client}/audience_split_trees", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audience_split_tree.as_json_dict()
    del request_data["description"]

    rv = console_application_client.post(f"/api/{client}/audience_split_trees", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    request_data = audience_split_tree.as_json_dict()
    request_data["unknown_parameter"] = "unknown value"

    rv = console_application_client.post(f"/api/{client}/audience_split_trees", data=json.dumps(request_data))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    # Reading all audiences

    rv = console_application_client.get(f"/api/{client}/audience_split_trees/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = console_application_client.post(f"/api/{client}/audience_split_trees",
                                         data=json.dumps(audience_split_tree.as_json_dict()))
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audience_split_trees/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    # Deleting audience

    rv = console_application_client.delete(f"/api/{client}/audience_split_trees?split_tree_id=__unknown_audience_id__")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audience_split_trees/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    rv = console_application_client.delete(f"/api/{client}/audience_split_trees?split_tree_id={split_tree_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"

    rv = console_application_client.get(f"/api/{client}/audience_split_trees/all")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = console_application_client.get(f"/api/{client}/audience_split_trees?split_tree_id={split_tree_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"


def test_audiences_page(client, console_application_client, audience_dao):
    audience_dao.delete_all_audience_split_trees(client)
    audience_dao.delete_all_audiences(client)
    audience_dao.delete_all_audiences_unions(client)

    rv = console_application_client.get(f"/api/{client}/pages/audiences")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert not len(data["data"]["audiences"])
    assert not len(data["data"]["audiences_unions"])
    assert not len(data["data"]["split_trees"])

    audience_1 = random_audience(audience_id="aud1")
    audience_2 = random_audience(audience_id="aud2")
    audience_dao.create_audience(client, audience_1)
    audience_dao.create_audience(client, audience_2)
    assert len(audience_dao.read_all_audiences(client)) == 2

    rv = console_application_client.get(f"/api/{client}/pages/audiences")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["audiences"]) == 2
    assert not len(data["data"]["audiences"][0]["presented_in_audiences_unions"])
    assert not len(data["data"]["audiences"][0]["presented_in_split_trees"])
    assert not len(data["data"]["audiences"][1]["presented_in_audiences_unions"])
    assert not len(data["data"]["audiences"][1]["presented_in_split_trees"])
    assert not len(data["data"]["audiences_unions"])
    assert not len(data["data"]["split_trees"])

    audiences_union_1 = random_audiences_union(audiences_union_id="aud_uni_1")
    audiences_union_2 = random_audiences_union(audiences_union_id="aud_uni_2")
    audience_dao.create_audiences_union(client, audiences_union_1)
    audience_dao.create_audiences_union(client, audiences_union_2)
    assert len(audience_dao.read_all_audiences_unions(client)) == 2

    split_tree_1 = random_audiences_split_tree(split_tree_id="split_tree_1")
    split_tree_2 = random_audiences_split_tree(split_tree_id="split_tree_2")
    audience_dao.create_audience_split_tree(client, split_tree_1)
    audience_dao.create_audience_split_tree(client, split_tree_2)
    assert len(audience_dao.read_all_audience_split_trees(client)) == 2

    rv = console_application_client.get(f"/api/{client}/pages/audiences")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["audiences"]) == 2
    assert not len(data["data"]["audiences"][0]["presented_in_audiences_unions"])
    assert not len(data["data"]["audiences"][0]["presented_in_split_trees"])
    assert not len(data["data"]["audiences"][1]["presented_in_audiences_unions"])
    assert not len(data["data"]["audiences"][1]["presented_in_split_trees"])
    assert len(data["data"]["audiences_unions"]) == 2
    assert len(data["data"]["split_trees"]) == 2

    audiences_union_1.audience_ids = [audience_1.audience_id, audience_2.audience_id]
    audiences_union_2.audience_ids = [audience_1.audience_id, audience_2.audience_id]
    audience_dao.create_audiences_union(client, audiences_union_1)
    audience_dao.create_audiences_union(client, audiences_union_2)
    assert len(audience_dao.read_all_audiences_unions(client)) == 2

    rv = console_application_client.get(f"/api/{client}/pages/audiences")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["audiences"]) == 2
    assert len(data["data"]["audiences"][0]["presented_in_audiences_unions"]) == 2
    assert not len(data["data"]["audiences"][0]["presented_in_split_trees"])
    assert len(data["data"]["audiences"][1]["presented_in_audiences_unions"]) == 2
    assert not len(data["data"]["audiences"][1]["presented_in_split_trees"])
    assert len(data["data"]["audiences_unions"]) == 2
    assert len(data["data"]["split_trees"]) == 2

    split_tree_1.audience_splits = [random_audience_split(audience_ids=[[audience_1.audience_id]])]
    split_tree_2.audience_splits = [random_audience_split(audience_ids=[[audience_2.audience_id]])]
    audience_dao.create_audience_split_tree(client, split_tree_1)
    audience_dao.create_audience_split_tree(client, split_tree_2)
    _ff = audience_dao.read_all_audience_split_trees(client)
    assert len(audience_dao.read_all_audience_split_trees(client)) == 2

    rv = console_application_client.get(f"/api/{client}/pages/audiences")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["audiences"]) == 2
    assert len(data["data"]["audiences"][0]["presented_in_audiences_unions"]) == 2
    assert len(data["data"]["audiences"][0]["presented_in_split_trees"]) == 1
    assert len(data["data"]["audiences"][1]["presented_in_audiences_unions"]) == 2
    assert len(data["data"]["audiences"][1]["presented_in_split_trees"]) == 1
    assert len(data["data"]["audiences_unions"]) == 2
    assert len(data["data"]["split_trees"]) == 2


def test_get_split_trees_page(client, console_application_client, audience_dao):
    audience_dao.delete_all_audience_split_trees(client)
    audience_dao.delete_all_audiences(client)
    audience_dao.delete_all_audiences_unions(client)

    rv = console_application_client.get(f"/api/{client}/pages/split_trees")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert not data["data"]["split_trees"]
    assert not data["data"]["audiences"]

    audience_1 = random_audience(audience_id="aud1")
    audience_2 = random_audience(audience_id="aud2")
    audience_dao.create_audience(client, audience_1)
    audience_dao.create_audience(client, audience_2)
    assert len(audience_dao.read_all_audiences(client)) == 2
    rv = console_application_client.get(f"/api/{client}/pages/split_trees")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert not data["data"]["split_trees"]
    assert len(data["data"]["audiences"]) == 2

    split_tree = random_audiences_split_tree(
        split_tree_id="split_tree_id_1",
        root_split_id="root_split_id_1",
        audience_splits=[
            random_audience_split(
                split_type=AudienceSplitType.RANDOM,
                split_id="root_split_id_1",
                percentiles=[10.0, 20.0, 70.0]
            )
        ]
    )

    split_tree.assert_audience_split_tree()
    audience_dao.create_audience_split_tree(client, split_tree)

    rv = console_application_client.get(f"/api/{client}/pages/split_trees")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["split_trees"]) == 1
    assert not data["data"]["split_trees"][0]["defines_audiences"]
    assert len(data["data"]["audiences"]) == 2

    split_tree.audience_splits[0].audience_ids = [["aud1", "aud2"], None, None, None]
    split_tree.assert_audience_split_tree()
    audience_dao.create_audience_split_tree(client, split_tree)

    rv = console_application_client.get(f"/api/{client}/pages/split_trees")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["split_trees"]) == 1
    assert sorted(data["data"]["split_trees"][0]["defines_audiences"]) == sorted(["aud1", "aud2"])
    assert len(data["data"]["audiences"]) == 2

    split_tree.audience_splits[0].audience_ids = [["aud1"], None, None, None]
    split_tree.assert_audience_split_tree()
    audience_dao.create_audience_split_tree(client, split_tree)

    rv = console_application_client.get(f"/api/{client}/pages/split_trees")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["split_trees"]) == 1
    assert sorted(data["data"]["split_trees"][0]["defines_audiences"]) == sorted(["aud1"])
    assert len(data["data"]["audiences"]) == 2

    split_tree = random_audiences_split_tree(
        split_tree_id="split_tree_id_1",
        root_split_id="root_split_id_1",
        audience_splits=[
            random_audience_split(
                split_type=AudienceSplitType.RANDOM,
                split_id="root_split_id_1",
                percentiles=[10.0, 20.0, 70.0],
                children_split_ids=[None, "children_split_id_1", None]
            ),
            random_audience_split(
                split_type=AudienceSplitType.RANDOM,
                split_id="children_split_id_1",
                percentiles=[10.0, 20.0, 30.0],
            )
        ]
    )
    split_tree.assert_audience_split_tree()
    audience_dao.create_audience_split_tree(client, split_tree)

    rv = console_application_client.get(f"/api/{client}/pages/split_trees")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["split_trees"]) == 1
    assert not data["data"]["split_trees"][0]["defines_audiences"]
    assert len(data["data"]["audiences"]) == 2

    split_tree.audience_splits[0].audience_ids = [["aud1", "aud2"], None, None, None]
    split_tree.assert_audience_split_tree()
    audience_dao.create_audience_split_tree(client, split_tree)

    rv = console_application_client.get(f"/api/{client}/pages/split_trees")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]["split_trees"]) == 1
    assert sorted(data["data"]["split_trees"][0]["defines_audiences"]) == sorted(["aud1", "aud2"])
    assert len(data["data"]["audiences"]) == 2
