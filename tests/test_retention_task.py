import pytest

from datetime import datetime, timedelta

from mobi.config.client import get_client_config
from mobi.config.system.service import get_retention_cleaner_config
from mobi.dao import get_catalog_dao, get_event_dao, get_metrics_dao, get_recolog_dao, MobiDaoException
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.recolog import MobiRecologDao
from mobi.service.task.impl.retention_cleaner import RetentionCleanerTask
from mobi.utils.tests import random_event, random_product, random_recommendation, random_string


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def catalog_dao() -> MobiCatalogDao:
    yield get_catalog_dao(get_retention_cleaner_config())


@pytest.fixture(scope="session")
def event_dao() -> MobiEventDao:
    yield get_event_dao(get_retention_cleaner_config())


@pytest.fixture(scope="session")
def metrics_dao() -> MobiMetricsDao:
    yield get_metrics_dao(get_retention_cleaner_config())


@pytest.fixture(scope="session")
def recolog_dao() -> MobiRecologDao:
    yield get_recolog_dao(get_retention_cleaner_config())


def test_events_products_retention(client, catalog_dao, event_dao):
    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)
    retention_cleaner = RetentionCleanerTask("RetentionCleaner", clients=[client])
    global_context = dict()
    client_context = dict()
    client_config = get_client_config(client)

    assert not list(catalog_dao.read_all_products(client))
    assert not list(event_dao.read_events(client))

    retention_cleaner.process_client(client, global_context, client_context)

    assert not list(catalog_dao.read_all_products(client))
    assert not list(event_dao.read_events(client))

    user_id = random_string()
    product = random_product(title="Test Product")

    catalog_dao.create_product(client, product)

    assert len(list(catalog_dao.read_all_products(client))) == 1
    old_product_version = next(catalog_dao.read_all_products(client)).version
    assert old_product_version == 1

    event_1 = random_event(
        user_id=user_id,
        product_id=product.product_id,
        date=datetime.utcnow() - timedelta(days=client_config.EVENTS_RETENTION_DAYS + 1)
    )
    event_dao.create_event(client, event_1)

    assert len(list(event_dao.read_events(client))) == 1

    product.title = "New Test Product"
    catalog_dao.create_product(client, product)

    assert len(list(catalog_dao.read_all_products(client))) == 1
    new_product_version = next(catalog_dao.read_all_products(client)).version
    assert new_product_version == 2

    # Executing retention cleaner task

    retention_cleaner.process_client(client, global_context, client_context)

    assert not list(event_dao.read_events(client))

    with pytest.raises(MobiDaoException):
        catalog_dao.read_product(client, product.product_id, version=old_product_version)

    assert catalog_dao.read_product(client, product.product_id).version == new_product_version

    # Creating fresh event for the old version, creating new version of this product

    event_2 = random_event(
        user_id=user_id,
        product_id=product.product_id,
        date=datetime.utcnow() - timedelta(days=client_config.EVENTS_RETENTION_DAYS - 1)
    )
    event_dao.create_event(client, event_2)
    assert len(list(event_dao.read_events(client))) == 1

    product.title = "New New Test Product"
    catalog_dao.create_product(client, product)

    assert len(list(catalog_dao.read_all_products(client))) == 1
    old_product_version = new_product_version
    new_product_version = next(catalog_dao.read_all_products(client)).version
    assert new_product_version == 3

    retention_cleaner.process_client(client, global_context, client_context)

    assert len(list(event_dao.read_events(client))) == 1
    old_product_from_db = catalog_dao.read_product(client, product.product_id, version=old_product_version)
    new_product_from_db = catalog_dao.read_product(client, product.product_id, version=new_product_version)
    assert old_product_from_db.version == old_product_version
    assert new_product_from_db.version == new_product_version
    assert old_product_from_db != new_product_from_db

    # Test wrong parameters

    old_events_retention_days = client_config.EVENTS_RETENTION_DAYS

    assert event_dao.MIN_EVENT_FATIGUE.days > 1
    client_config.EVENTS_RETENTION_DAYS = event_dao.MIN_EVENT_FATIGUE.days - 1
    with pytest.raises(MobiDaoException):
        retention_cleaner.delete_old_events(client, client_config)

    client_config.EVENTS_RETENTION_DAYS = old_events_retention_days


def test_recologs_retention(client, recolog_dao):
    recolog_dao.delete_all_recommendations(client)
    retention_cleaner = RetentionCleanerTask("RetentionCleaner", clients=[client])
    global_context = dict()
    client_context = dict()
    client_config = get_client_config(client)

    assert not(list(recolog_dao.read_recommendations(client)))

    retention_cleaner.process_client(client, global_context, client_context)

    assert not(list(recolog_dao.read_recommendations(client)))

    # Test no recologs after task execution

    recommendation_1 = random_recommendation(
        date=datetime.utcnow() - timedelta(days=client_config.EVENTS_RETENTION_DAYS + 1)
    )
    recolog_dao.log_recommendation(client, recommendation_1)

    assert len(list(recolog_dao.read_recommendations(client))) == 1

    retention_cleaner.process_client(client, global_context, client_context)

    assert not list(recolog_dao.read_recommendations(client))

    # Test recologs after task execution

    recommendation_1 = random_recommendation(
        date=datetime.utcnow() - timedelta(days=client_config.EVENTS_RETENTION_DAYS - 1)
    )
    recolog_dao.log_recommendation(client, recommendation_1)

    assert len(list(recolog_dao.read_recommendations(client))) == 1

    retention_cleaner.process_client(client, global_context, client_context)

    assert len(list(recolog_dao.read_recommendations(client))) == 1

    # Test wrong parameters

    old_recologs_retention_days = client_config.RECOLOGS_RETENTION_DAYS

    assert recolog_dao.MIN_RECOLOG_FATIGUE.days > 1
    client_config.RECOLOGS_RETENTION_DAYS = recolog_dao.MIN_RECOLOG_FATIGUE.days - 1
    with pytest.raises(MobiDaoException):
        retention_cleaner.delete_old_recologs(client, client_config)

    client_config.RECOLOGS_RETENTION_DAYS = old_recologs_retention_days


def test_metrics_retention(client, metrics_dao):
    metrics_dao.delete_all(client)
    retention_cleaner = RetentionCleanerTask("RetentionCleaner", clients=[client])
    global_context = dict()
    client_context = dict()
    client_config = get_client_config(client)
    metric_name = "test_metric"

    retention_date_minutes = (datetime.utcnow() - timedelta(client_config.METRICS_MINUTES_RETENTION_DAYS))
    retention_date_hours = (datetime.utcnow() - timedelta(client_config.METRICS_HOURS_RETENTION_DAYS))
    retention_date_days = (datetime.utcnow() - timedelta(client_config.METRICS_DAILY_RETENTION_DAYS))

    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=60,
        tags=None,
        timestamp_from=int((retention_date_minutes - timedelta(minutes=10)).timestamp()),
        timestamp_until=int((retention_date_minutes + timedelta(minutes=10)).timestamp()),
    ))
    assert not metrics
    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=60 * 60,
        tags=None,
        timestamp_from=int((retention_date_hours - timedelta(hours=10)).timestamp()),
        timestamp_until=int((retention_date_hours + timedelta(hours=10)).timestamp()),
    ))
    assert not metrics
    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=24 * 60 * 60,
        tags=None,
        timestamp_from=int((retention_date_days - timedelta(days=10)).timestamp()),
        timestamp_until=int((retention_date_days + timedelta(days=10)).timestamp()),
    ))
    assert not metrics

    retention_cleaner.process_client(client, global_context, client_context)

    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=60,
        tags=None,
        timestamp_from=int((retention_date_minutes - timedelta(minutes=10)).timestamp()),
        timestamp_until=int((retention_date_minutes + timedelta(minutes=10)).timestamp()),
    ))
    assert not metrics
    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=60 * 60,
        tags=None,
        timestamp_from=int((retention_date_hours - timedelta(hours=10)).timestamp()),
        timestamp_until=int((retention_date_hours + timedelta(hours=10)).timestamp()),
    ))
    assert not metrics
    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=24 * 60 * 60,
        tags=None,
        timestamp_from=int((retention_date_days - timedelta(days=10)).timestamp()),
        timestamp_until=int((retention_date_days + timedelta(days=10)).timestamp()),
    ))
    assert not metrics

    # Adding six points

    metrics_dao.inc_metrics_point(client, metric_name, tags=dict(), base=60,
                                  timestamp=int((retention_date_minutes + timedelta(minutes=2)).timestamp()))

    metrics_dao.inc_metrics_point(client, metric_name, tags=dict(), base=60,
                                  timestamp=int((retention_date_minutes - timedelta(minutes=2)).timestamp()))

    metrics_dao.inc_metrics_point(client, metric_name, tags=dict(), base=60 * 60,
                                  timestamp=int((retention_date_hours + timedelta(hours=2)).timestamp()))

    metrics_dao.inc_metrics_point(client, metric_name, tags=dict(), base=60 * 60,
                                  timestamp=int((retention_date_hours - timedelta(hours=2)).timestamp()))

    metrics_dao.inc_metrics_point(client, metric_name, tags=dict(), base=24 * 60 * 60,
                                  timestamp=int((retention_date_days + timedelta(days=2)).timestamp()))

    metrics_dao.inc_metrics_point(client, metric_name, tags=dict(), base=24 * 60 * 60,
                                  timestamp=int((retention_date_days - timedelta(days=2)).timestamp()))

    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=60,
        tags=None,
        timestamp_from=int((retention_date_minutes - timedelta(minutes=10)).timestamp()),
        timestamp_until=int((retention_date_minutes + timedelta(minutes=10)).timestamp()),
    ))
    assert len(metrics) == 2
    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=60 * 60,
        tags=None,
        timestamp_from=int((retention_date_hours - timedelta(hours=10)).timestamp()),
        timestamp_until=int((retention_date_hours + timedelta(hours=10)).timestamp()),
    ))
    assert len(metrics) == 2
    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=24 * 60 * 60,
        tags=None,
        timestamp_from=int((retention_date_days - timedelta(days=10)).timestamp()),
        timestamp_until=int((retention_date_days + timedelta(days=10)).timestamp()),
    ))
    assert len(metrics) == 2

    retention_cleaner.process_client(client, global_context, client_context)

    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=60,
        tags=None,
        timestamp_from=int((retention_date_minutes - timedelta(minutes=10)).timestamp()),
        timestamp_until=int((retention_date_minutes + timedelta(minutes=10)).timestamp()),
    ))
    assert len(metrics) == 1
    assert metrics[0][0] > int(retention_date_minutes.timestamp())

    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=60 * 60,
        tags=None,
        timestamp_from=int((retention_date_hours - timedelta(hours=10)).timestamp()),
        timestamp_until=int((retention_date_hours + timedelta(hours=10)).timestamp()),
    ))
    assert len(metrics) == 1
    assert metrics[0][0] > int(retention_date_hours.timestamp())

    metrics = list(metrics_dao.get_metrics(
        client,
        metric_name,
        base=24 * 60 * 60,
        tags=None,
        timestamp_from=int((retention_date_days - timedelta(days=10)).timestamp()),
        timestamp_until=int((retention_date_days + timedelta(days=10)).timestamp()),
    ))
    assert len(metrics) == 1
    assert metrics[0][0] > int(retention_date_days.timestamp())


