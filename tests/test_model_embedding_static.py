import pytest

from datetime import datetime
from typing import List

from mobi.core.models import Event, EventPlatform, EventType, Product
from mobi.core.population import Population
from mobi.dao.cache import UserHistoryCookie
from mobi.ml.model.embedding.impl.static import StaticEmbeddingModel
from mobi.utils.tests import random_string, random_product


@pytest.fixture(scope="session")
def model() -> StaticEmbeddingModel:
    yield StaticEmbeddingModel()


@pytest.fixture(scope="session")
def test_products() -> List[Product]:
    product_id_1, product_id_2, product_id_3 = "product_1", "product_2", "product_3"
    product_1 = random_product(product_id_1)
    product_2 = random_product(product_id_2)
    product_3 = random_product(product_id_3)
    yield [product_1, product_2, product_3]


@pytest.fixture(scope="session")
def compiled_model(model, test_products) -> StaticEmbeddingModel:
    product_1, product_2, product_3 = test_products
    model.set_product_id_embeddings(
        [(product_1.product_id, [0., 0., 0.]), (product_2.product_id, [1., 0., 0.]), (product_3.product_id, [1., 1., 1.])],
    )
    model.compile([product_1, product_2, product_3])
    yield model


def test_create_model():
    StaticEmbeddingModel()


def test_compile_not_ready(model):
    with pytest.raises(ValueError):
        model.compile([random_product()])


def test_pass_data_to_model(model):
    product_id = random_string()
    product = random_product(product_id)
    model.set_product_id_embeddings([(product_id, [0., 0., 0.])])
    model.compile([product])


def test_prediction_non_existing_product(compiled_model):
    product = random_product()
    assert not compiled_model.similar_products(product, k=1)


def test_model_predictions(compiled_model, test_products):
    product = random_product("product_1")
    similar_products = compiled_model.similar_products(product, k=1)
    assert len(similar_products) == 1
    assert similar_products[0] == "product_2"

    product = random_product("product_2")
    similar_products = compiled_model.similar_products(product, k=1)
    assert len(similar_products) == 1
    assert similar_products[0] == "product_1"

    product = random_product("product_3")
    similar_products = compiled_model.similar_products(product, k=2)
    assert len(similar_products) == 2
    assert similar_products[0] == "product_2"
    assert similar_products[1] == "product_1"

    # We removed this logic from the model
    # with pytest.raises(ValueError):
    #     compiled_model.similar_products(product, k=11)

    event_1 = Event(
        user_id="some_user_id",
        user_population=Population.UNKNOWN,
        product_id="product_1",
        product_version=1,
        event_type=EventType.PRODUCT_VIEW,
        event_platform=EventPlatform.OTHER,
        date=datetime.utcnow()
    )
    recommendations = compiled_model.personal_recommendations([event_1], k=1)
    assert len(recommendations) == 1
    assert recommendations[0] == "product_1"

    event_2 = Event(
        user_id="some_user_id",
        user_population=Population.UNKNOWN,
        product_id="product_2",
        product_version=1,
        event_type=EventType.PRODUCT_VIEW,
        event_platform=EventPlatform.OTHER,
        date=datetime.utcnow()
    )

    recommendations = compiled_model.personal_recommendations([event_1, event_2], k=2)
    assert len(recommendations) == 2
    assert "product_1" in recommendations
    assert "product_2" in recommendations

    assert compiled_model.personal_recommendations([event_1, event_2], k=3)

    # We removed this logic from the model
    # with pytest.raises(ValueError):
    #     compiled_model.personal_recommendations([event_1, event_2], k=4)

    scores = compiled_model.rank_products_by_user_history(test_products, UserHistoryCookie([event_1, event_2]))
    assert len(scores) == 3
