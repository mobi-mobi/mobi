import pytest

from datetime import datetime
from random import randrange

from mobi.config.system.api.recommendation_api import get_recommendation_api_config
from mobi.core.models import Product
from mobi.dao.cache import cache_key, MobiCache
from mobi.dao.factory import get_cache_dao
from mobi.utils.tests import random_product, random_string


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def cache() -> MobiCache:
    return get_cache_dao(get_recommendation_api_config())


def product_key(product_id: str):
    return cache_key("product", context="test", client="testclient", product_id=product_id)


def test_set_delete(cache: MobiCache):
    product = random_product()
    assert cache.get(product_key(product.product_id)) is None

    cache.set(product_key(product.product_id), product.as_bytes(), noreply=False, expire=10)
    assert cache.get(product_key(product.product_id)) is not None
    assert product == Product.from_bytes(cache.get(product_key(product.product_id)))

    cache.delete(product_key(product.product_id), noreply=False)
    assert cache.get(product_key(product.product_id)) is None

    cache.delete(product_key(product.product_id), noreply=False)
    assert cache.get(product_key(product.product_id)) is None


def test_int(cache: MobiCache):
    key = cache_key("test_int_key", client="testclient")

    cache.set_int(key, 1, noreply=False, expire=10)
    assert isinstance(cache.get_int(key), int)
    assert cache.get_int(key) == 1

    cache.inc(key, 10, noreply=False)
    assert isinstance(cache.get_int(key), int)
    assert cache.get_int(key) == 11


def test_float(cache: MobiCache):
    key = cache_key("test_float_key", client="testclient")

    cache.set_float(key, 1.2, noreply=False, expire=10)
    assert isinstance(cache.get_float(key), float)
    assert cache.get_float(key) == 1.2

    cache.inc(key, 10, noreply=False)
    assert isinstance(cache.get_float(key), float)
    assert cache.get_float(key) == 1.2  # As it wasn't incremented

    cache.set_float(key, 11.2, noreply=False, expire=10)
    assert isinstance(cache.get_float(key), float)
    assert cache.get_float(key) == 11.2


def test_many(cache: MobiCache):
    products = [random_product() for _ in range(100)]

    for product in products:
        assert cache.get(product_key(product.product_id)) is None

    assert not cache.get_many([product_key(product.product_id) for product in products])

    cache.set_many({product_key(product.product_id): product.as_bytes() for product in products}, noreply=False,
                   expire=10)

    for product in products:
        assert cache.get(product_key(product.product_id)) is not None

    assert cache.get_many([product_key(product.product_id) for product in products])
    assert len(cache.get_many([product_key(product.product_id) for product in products])) == len(products)

    cache.delete(product_key(products[0].product_id), noreply=False)

    assert cache.get_many([product_key(product.product_id) for product in products])
    assert len(cache.get_many([product_key(product.product_id) for product in products])) == len(products) - 1

    cache.delete_many([product_key(product.product_id) for product in products], noreply=False)

    for product in products:
        assert cache.get(product_key(product.product_id)) is None

    assert not cache.get_many([product_key(product.product_id) for product in products])


def test_set_get_main_int(client: str, cache: MobiCache):
    data = {
        cache_key(f"__test_many_int__{str(i)}", client=client): randrange(10 ** 9, 10 ** 10) for i in range(100)
    }
    cache.set_many_int(data, expire=10, noreply=False, retries=3)

    cache_data = cache.get_many_int(list(data.keys()), retries=3)

    for key, value in data.items():
        assert key in cache_data
        assert isinstance(cache_data[key], int)
        assert value == cache_data[key]


def test_add(cache: MobiCache):
    product = random_product()
    assert cache.get(product_key(product.product_id)) is None

    cache.add(product_key(product.product_id), product.as_bytes(), expire=10, noreply=False)

    assert cache.get(product_key(product.product_id)) is not None
    assert product == Product.from_bytes(cache.get(product_key(product.product_id)))

    assert not cache.add(product_key(product.product_id), product.as_bytes(), noreply=False)


def test_counter(client, cache):
    cache_name = random_string()

    cache.inc_counter(client, cache_name)
    assert cache.get_counter(client, cache_name) == 1

    cache.inc_counter(client, cache_name, value=2)
    assert cache.get_counter(client, cache_name) == 3

    cache.inc_counter(client, cache_name, current_ts=int(datetime.utcnow().timestamp()) - 100)
    assert cache.get_counter(client, cache_name) == 3

    smoothed_counter = random_string()
    for i in range(60):
        current_ts = int(datetime.utcnow().timestamp()) + i
        cache.inc_counter(client, smoothed_counter, current_ts=current_ts, value=1, length_sec=20, accuracy_sec=10,
                          noreply=False)
        assert cache.get_counter(client, smoothed_counter, current_ts=current_ts, length_sec=20, accuracy_sec=10) <= 21
