import pytest

from datetime import datetime, timedelta
from typing import Generator

from mobi.config.system.api.event_api import get_event_api_config
from mobi.core.models import Event, EventPlatform, EventType
from mobi.core.population import Population
from mobi.dao.event import MobiEventDao
from mobi.dao import get_event_dao, MobiDaoException
from mobi.utils.tests import random_event, random_string


@pytest.fixture(scope="session")
def event_dao() -> Generator[MobiEventDao, None, None]:
    yield get_event_dao(get_event_api_config())


@pytest.fixture(scope="session")
def client() -> str:
    yield "testclient"


def test_get_dao(event_dao):
    pass


def test_read_events(event_dao, client):
    for _ in event_dao.read_events(client):
        return


def test_no_future_events(event_dao, client):
    assert not len([_ for _ in event_dao.read_events(client, datetime.utcnow() + timedelta(seconds=1))])


def test_create_read_event(event_dao, client):
    user_id = random_string()
    product_id = random_string()
    event_dao.create_event(client, Event(
        user_id=user_id,
        user_population=Population.UNKNOWN,
        product_id=product_id,
        product_version=1,
        event_type=EventType.PRODUCT_VIEW,
        event_platform=EventPlatform.OTHER,
        date=datetime.utcnow()
    ))
    found_event = None
    for event in event_dao.read_events(client):
        if event.product_id == product_id and event.user_id == user_id:
            found_event = event
    assert found_event is not None
    assert found_event.event_type == EventType.PRODUCT_VIEW

    found_event = None
    for event in event_dao.read_events(client, from_date=datetime.utcnow() - timedelta(seconds=10)):
        if event.product_id == product_id and event.user_id == user_id:
            found_event = event
    assert found_event is not None
    assert found_event.event_type == EventType.PRODUCT_VIEW


def test_delete_events(event_dao, client):
    user_id = random_string()
    product_id = random_string()
    event_dao.create_event(client, Event(
        user_id=user_id,
        user_population=Population.UNKNOWN,
        product_id=product_id,
        product_version=1,
        event_type=EventType.PRODUCT_VIEW,
        event_platform=EventPlatform.OTHER,
        date=datetime.utcnow()
    ))
    found_event = None
    for event in event_dao.read_events(client):
        if event.product_id == product_id and event.user_id == user_id:
            found_event = event
    assert found_event is not None
    assert found_event.event_type == EventType.PRODUCT_VIEW

    event_dao.delete_all_events(client)

    found_event = None
    for event in event_dao.read_events(client):
        if event.product_id == product_id and event.user_id == user_id:
            found_event = event
    assert found_event is None


def test_delete_fatigue(event_dao, client):
    event_dao.delete_all_events(client)
    assert len(list(event_dao.read_events(client))) == 0

    date_1 = datetime.utcnow() - timedelta(days=8)
    date_2 = datetime.utcnow() - timedelta(days=9)
    bad_date = (datetime.utcnow() - event_dao.MIN_EVENT_FATIGUE) + timedelta(hours=1)
    not_a_bad_date = (datetime.utcnow() - event_dao.MIN_EVENT_FATIGUE) - timedelta(hours=1)

    event_dao.create_event(client, random_event(date=not_a_bad_date))

    # Should raise an exception
    assert len(list(event_dao.read_events(client))) == 1
    with pytest.raises(MobiDaoException):
        event_dao.delete_fatigue_events(client, bad_date)
    assert len(list(event_dao.read_events(client))) == 1

    # Should not raise an exception
    assert len(list(event_dao.read_events(client))) == 1
    event_dao.delete_fatigue_events(client, not_a_bad_date)
    assert len(list(event_dao.read_events(client))) == 1

    event_dao.create_event(client, random_event(date=date_1))
    event_dao.create_event(client, random_event(date=date_2))

    assert len(list(event_dao.read_events(client))) == 3

    event_dao.delete_fatigue_events(client, date_2)

    assert len(list(event_dao.read_events(client))) == 3

    event_dao.delete_fatigue_events(client, date_1)

    assert len(list(event_dao.read_events(client))) == 2

    event_dao.delete_fatigue_events(client, date_1 + timedelta(minutes=1))

    assert len(list(event_dao.read_events(client))) == 1


def test_read_unique_user_ids(event_dao, client):
    event_dao.delete_all_events(client)

    event_dao.create_event(client, random_event(user_id="user_2"))
    event_dao.create_event(client, random_event(user_id="user_2"))
    event_dao.create_event(client, random_event(user_id="user_2"))
    event_dao.create_event(client, random_event(user_id="user_1"))
    event_dao.create_event(client, random_event(user_id="user_1"))
    event_dao.create_event(client, random_event(user_id="user_2"))
    event_dao.create_event(client, random_event(user_id="user_3"))
    event_dao.create_event(client, random_event(user_id="user_3"))
    event_dao.create_event(client, random_event(user_id="user_4"))

    user_ids = [user_id for user_id, _, _ in event_dao.read_unique_user_ids(client, sorted=True)]
    assert user_ids == ["user_1", "user_2", "user_3", "user_4"]
