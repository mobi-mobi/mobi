import pytest

from mobi.config.system.api.recommendation_api import get_recommendation_api_config
from mobi.dao import get_object_dao, MobiDaoException
from mobi.dao.object import MobiObjectDao
from mobi.utils.tests import random_string


@pytest.fixture(scope="session")
def object_dao() -> MobiObjectDao:
    return get_object_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def client():
    return "testclient"


def test_object_dao(object_dao):
    pass


def test_save_get_del_bin(object_dao, client):
    key = random_string()
    s = random_string(100)
    s_bytes = bytes(s, "utf-8")
    object_dao.save_binary(client, key, s_bytes)

    p_bytes = object_dao.load_binary(client, key)
    p = str(p_bytes, "utf-8")

    assert s_bytes == p_bytes
    assert s == p

    object_dao.delete_binary(client, key)

    with pytest.raises(MobiDaoException):
        object_dao.load_binary(client, key)
