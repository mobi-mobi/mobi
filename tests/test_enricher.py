import pytest

from copy import deepcopy

from mobi.enrich import StaticCategorizer
from mobi.utils.tests import random_product, random_string


@pytest.fixture(scope="session")
def static_categorizer():
    return StaticCategorizer()


def test_static_categorizer(static_categorizer):
    product = random_product()
    random_category = random_string()
    product.categories.append(random_category)
    product.title += " eye balm"
    new_product = deepcopy(product)
    static_categorizer.enrich_product(new_product)
    assert new_product is not None
    assert "eyes" in new_product.enriched_tags
    assert "eye_balm" in new_product.enriched_tags
    assert random_category in product.categories
