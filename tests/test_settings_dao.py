import pytest

from datetime import datetime, timedelta

from mobi.config.system.web.console import get_console_web_config
from mobi.core.models import TokenPermission
from mobi.core.models.zone import ZoneStatus
from mobi.dao import get_settings_dao
from mobi.dao.settings.interface import MobiSettingsDao
from mobi.utils.tests import random_api_token, random_campaign, random_zone


@pytest.fixture(scope="session")
def client():
    return "testclient"


@pytest.fixture(scope="session")
def settings_dao() -> MobiSettingsDao:
    yield get_settings_dao(get_console_web_config())


def test_create_dao(settings_dao):
    pass


def test_create_delete_zone(settings_dao, client):
    settings_dao.delete_all_zones(client)
    assert not len(list(settings_dao.read_all_zones(client)))

    zone = random_zone(name="Test Zone")
    settings_dao.create_zone(client, zone)
    assert settings_dao.count_all_zones(client) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert settings_dao.read_zone(client, zone.zone_id).as_dict() == zone.as_dict()

    assert len(list(settings_dao.read_all_zones(client, text="Test"))) == 1
    assert len(list(settings_dao.read_all_zones(client, text="Zone"))) == 1
    assert len(list(settings_dao.read_all_zones(client, text="zone TEST"))) == 1
    assert not len(list(settings_dao.read_all_zones(client, text="Prefix " + zone.name)))
    assert not list(settings_dao.read_all_zones(client, text="somewrongnonexistingname"))

    settings_dao.delete_zone(client, zone.zone_id)
    assert not len(list(settings_dao.read_all_zones(client)))


def test_update_zone(settings_dao, client):
    settings_dao.delete_all_zones(client)
    assert not list(settings_dao.read_all_zones(client))

    zone = random_zone()
    settings_dao.create_zone(client, zone)
    assert settings_dao.count_all_zones(client) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert list(settings_dao.read_all_zones(client))[0].as_dict() == zone.as_dict()
    assert settings_dao.read_zone(client, zone.zone_id).as_dict() == zone.as_dict()

    zone.name = zone.name + " and soomething new"
    assert list(settings_dao.read_all_zones(client))[0].as_dict() != zone.as_dict()
    settings_dao.create_zone(client, zone)
    assert settings_dao.count_all_zones(client) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert list(settings_dao.read_all_zones(client))[0].as_dict() == zone.as_dict()


def test_disable_zone(settings_dao, client):
    settings_dao.delete_all_zones(client)
    assert not list(settings_dao.read_all_zones(client))

    zone = random_zone()
    settings_dao.create_zone(client, zone)
    assert settings_dao.count_all_zones(client) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert list(settings_dao.read_all_zones(client))[0].as_dict() == zone.as_dict()
    assert settings_dao.read_zone(client, zone.zone_id).as_dict() == zone.as_dict()

    zone.status = ZoneStatus.DISABLED
    assert list(settings_dao.read_all_zones(client))[0].as_dict() != zone.as_dict()
    settings_dao.create_zone(client, zone)
    assert settings_dao.count_all_zones(client) == 0
    assert settings_dao.count_all_zones(client, include_inactive=True) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 0
    assert len(list(settings_dao.read_all_zones(client, include_inactive=True))) == 1
    assert list(settings_dao.read_all_zones(client, include_inactive=True))[0].as_dict() == zone.as_dict()

    zone.status = ZoneStatus.ACTIVE
    settings_dao.create_zone(client, zone)
    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert list(settings_dao.read_all_zones(client))[0].as_dict() == zone.as_dict()


def test_read_all_reco_campaigns(settings_dao, client):
    settings_dao.delete_all_zones(client)
    settings_dao.delete_all_campaigns(client)
    assert not len(list(settings_dao.read_all_zones(client)))
    assert not len(list(settings_dao.read_all_campaigns(client)))

    zone = random_zone()
    settings_dao.create_zone(client, zone)

    campaign_1 = random_campaign(name="random campaign 1")
    campaign_2 = random_campaign(name="random campaign 2")
    settings_dao.create_campaign(client, campaign_1)
    settings_dao.create_campaign(client, campaign_2)

    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert len(list(settings_dao.read_all_campaigns(client))) == 2

    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign_1.campaign_id)
    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == [campaign_1.campaign_id]
    assert len(list(settings_dao.read_zone_campaigns(client, zone.zone_id))) == 1
    assert list(settings_dao.read_zone_campaigns(client, zone.zone_id))[0].campaign_id == campaign_1.campaign_id

    with pytest.raises(KeyError):
        list(settings_dao.read_zone_campaigns(client, "__nonexisting_zone_id__"))


def test_change_zone_status(settings_dao, client):
    settings_dao.delete_all_zones(client)
    assert not len(list(settings_dao.read_all_zones(client)))

    zone = random_zone()
    settings_dao.create_zone(client, zone)
    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert list(settings_dao.read_all_zones(client))[0].as_dict() == zone.as_dict()
    assert settings_dao.read_zone(client, zone.zone_id).as_dict() == zone.as_dict()

    settings_dao.set_zone_status(client, zone.zone_id, ZoneStatus.ACTIVE)
    assert settings_dao.read_zone(client, zone.zone_id).as_dict() == zone.as_dict()

    settings_dao.set_zone_status(client, zone.zone_id, ZoneStatus.DISABLED)
    assert settings_dao.read_zone(client, zone.zone_id).as_dict() != zone.as_dict()
    assert settings_dao.read_zone(client, zone.zone_id).status == ZoneStatus.DISABLED

    settings_dao.set_zone_status(client, zone.zone_id, ZoneStatus.ACTIVE)
    assert settings_dao.read_zone(client, zone.zone_id).as_dict() == zone.as_dict()


def test_adding_campaigns_to_zone(settings_dao, client):
    settings_dao.delete_all_zones(client)
    settings_dao.delete_all_campaigns(client)

    assert not len(list(settings_dao.read_all_campaigns(client)))
    assert not len(list(settings_dao.read_all_zones(client)))

    zone = random_zone()
    campaign = random_campaign()

    settings_dao.create_zone(client, zone)
    settings_dao.create_campaign(client, campaign)

    assert len(list(settings_dao.read_all_campaigns(client))) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 1

    assert not zone.campaign_ids

    settings_dao.add_campaign_to_zone(client, zone_id=zone.zone_id, campaign_id=campaign.campaign_id)

    assert len(list(settings_dao.read_all_campaigns(client))) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 1

    new_zone = list(settings_dao.read_all_zones(client))[0]

    assert zone != new_zone
    assert len(new_zone.campaign_ids) == 1
    assert new_zone.campaign_ids[0] == campaign.campaign_id

    # Add one more time
    settings_dao.add_campaign_to_zone(client, zone_id=zone.zone_id, campaign_id=campaign.campaign_id)

    assert len(list(settings_dao.read_all_campaigns(client))) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 1

    new_zone = list(settings_dao.read_all_zones(client))[0]

    assert zone != new_zone
    assert len(new_zone.campaign_ids) == 1
    assert new_zone.campaign_ids[0] == campaign.campaign_id

    # Deleting campaign
    settings_dao.delete_campaign(client, campaign.campaign_id)

    assert not len(list(settings_dao.read_all_campaigns(client)))
    assert len(list(settings_dao.read_all_zones(client))) == 1

    new_zone = list(settings_dao.read_all_zones(client))[0]

    assert not len(new_zone.campaign_ids)

    # One again, but with remove_campaign method
    settings_dao.create_campaign(client, campaign)
    assert len(list(settings_dao.read_all_campaigns(client))) == 1

    settings_dao.add_campaign_to_zone(client, zone_id=zone.zone_id, campaign_id=campaign.campaign_id)

    assert len(list(settings_dao.read_all_campaigns(client))) == 1
    assert len(list(settings_dao.read_all_zones(client))) == 1

    new_zone = list(settings_dao.read_all_zones(client))[0]

    assert zone != new_zone
    assert len(new_zone.campaign_ids) == 1
    assert new_zone.campaign_ids[0] == campaign.campaign_id

    settings_dao.remove_campaign_from_zone(client, zone.zone_id, campaign.campaign_id)

    new_zone = list(settings_dao.read_all_zones(client))[0]

    assert not len(new_zone.campaign_ids)


def test_create_delete_campaign(settings_dao, client):
    settings_dao.delete_all_campaigns(client)
    assert not list(settings_dao.read_all_campaigns(client))

    campaign = random_campaign(name="Test Campaign")
    settings_dao.create_campaign(client, campaign)
    assert len(list(settings_dao.read_all_campaigns(client))) == 1

    assert len(list(settings_dao.read_all_campaigns(client, text="Test"))) == 1
    assert len(list(settings_dao.read_all_campaigns(client, text="Campaign"))) == 1
    assert len(list(settings_dao.read_all_campaigns(client, text="TEST campaign"))) == 1
    assert not len(list(settings_dao.read_all_campaigns(client, text="Prefix " + campaign.name)))
    assert not list(settings_dao.read_all_campaigns(client, text="somewrongnonexistingname"))

    settings_dao.delete_campaign(client, campaign.campaign_id)
    assert not list(settings_dao.read_all_campaigns(client))


def test_campaigns_order(settings_dao, client):
    settings_dao.delete_all_campaigns(client)
    settings_dao.delete_all_zones(client)

    assert not list(settings_dao.read_all_zones(client))
    assert not list(settings_dao.read_all_campaigns(client))

    zone = random_zone()
    settings_dao.create_zone(client, zone)

    campaigns = []
    for _ in range(100):
        campaign = random_campaign()
        campaigns.append(campaign)
        settings_dao.create_campaign(client, campaign)

    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert len(list(settings_dao.read_all_campaigns(client, limit=100))) == 100

    for campaign in campaigns:
        settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign.campaign_id)

    campaign_ids = [campaign.campaign_id for campaign in campaigns]

    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == campaign_ids

    campaign_3 = random_campaign(campaign_id="random campaign 3")
    settings_dao.create_campaign(client, campaign_3)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign_3.campaign_id)

    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == campaign_ids + [campaign_3.campaign_id]

    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign_3.campaign_id)

    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == campaign_ids + [campaign_3.campaign_id]

    settings_dao.remove_campaign_from_zone(client, zone.zone_id, campaigns[0].campaign_id)

    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == campaign_ids[1:] + [campaign_3.campaign_id]

    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaigns[0].campaign_id)

    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == campaign_ids[1:] + [campaign_3.campaign_id] \
        + [campaigns[0].campaign_id]


def test_read_past_campaigns(settings_dao, client):
    settings_dao.delete_all_campaigns(client)
    settings_dao.delete_all_zones(client)

    past_campaign = random_campaign(end_date=datetime.utcnow() - timedelta(minutes=1))
    good_campaign = random_campaign(end_date=datetime.utcnow() + timedelta(minutes=1))
    another_good_campaign = random_campaign()

    settings_dao.create_campaign(client, past_campaign)
    settings_dao.create_campaign(client, good_campaign)
    settings_dao.create_campaign(client, another_good_campaign)

    zone = random_zone(campaign_ids=[past_campaign.campaign_id, good_campaign.campaign_id,
                                     another_good_campaign.campaign_id])
    settings_dao.create_zone(client, zone)

    not_passed_campaigns = list(settings_dao.read_zone_campaigns(client, zone.zone_id, include_past=False))
    assert len(not_passed_campaigns) == 2
    assert not_passed_campaigns[0] == good_campaign
    assert not_passed_campaigns[1] == another_good_campaign


def test_replace_campaign_ids(settings_dao, client):
    settings_dao.delete_all_campaigns(client)
    settings_dao.delete_all_zones(client)

    assert not list(settings_dao.read_all_zones(client))
    assert not list(settings_dao.read_all_campaigns(client))

    zone = random_zone()
    settings_dao.create_zone(client, zone)

    campaign_1 = random_campaign(campaign_id="random campaign 1")
    campaign_2 = random_campaign(campaign_id="random campaign 2")
    settings_dao.create_campaign(client, campaign_1)
    settings_dao.create_campaign(client, campaign_2)

    assert len(list(settings_dao.read_all_zones(client))) == 1
    assert len(list(settings_dao.read_all_campaigns(client))) == 2

    with pytest.raises(KeyError):
        settings_dao.replace_zone_campaign_ids(client, zone.zone_id, ["__nonexisting_campaign_id__"])

    with pytest.raises(KeyError):
        settings_dao.replace_zone_campaign_ids(client, zone.zone_id, [campaign_1.campaign_id,
                                                                      "__nonexisting_campaign_id__"])

    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == []

    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign_1.campaign_id)
    settings_dao.add_campaign_to_zone(client, zone.zone_id, campaign_2.campaign_id)

    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == [campaign_1.campaign_id, campaign_2.campaign_id]

    settings_dao.replace_zone_campaign_ids(client, zone.zone_id, [])
    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == []

    settings_dao.replace_zone_campaign_ids(client, zone.zone_id, [campaign_1.campaign_id])
    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == [campaign_1.campaign_id]

    settings_dao.replace_zone_campaign_ids(client, zone.zone_id, [campaign_1.campaign_id, campaign_2.campaign_id])
    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == [campaign_1.campaign_id, campaign_2.campaign_id]

    settings_dao.replace_zone_campaign_ids(client, zone.zone_id, [campaign_2.campaign_id, campaign_1.campaign_id])
    assert settings_dao.read_zone(client, zone.zone_id).campaign_ids == [campaign_2.campaign_id, campaign_1.campaign_id]


def test_tokens(settings_dao, client):
    settings_dao.delete_all_tokens(client)
    legacy_tokens = [token for token in settings_dao.read_tokens(client) if token.description is not None
                     and token.description == "Legacy token. Can not be deleted"]
    assert len(settings_dao.read_tokens(client)) == len(legacy_tokens)

    token = random_api_token(client=client, permissions=[TokenPermission.TEST_RECOMMENDATIONS])
    settings_dao.create_api_token(token)

    assert len(settings_dao.read_tokens(client)) == 1 + len(legacy_tokens)
    created_token = None
    for token in settings_dao.read_tokens(client):
        if token.description != "Legacy token. Can not be deleted":
            created_token = token
            break
    assert created_token is not None
    assert created_token == token

    settings_dao.delete_all_tokens(client)
    assert len(settings_dao.read_tokens(client)) == len(legacy_tokens)


def test_delete_one_token(settings_dao, client):
    settings_dao.delete_all_tokens(client)
    legacy_tokens = [token for token in settings_dao.read_tokens(client) if token.description is not None
                     and token.description == "Legacy token. Can not be deleted"]
    assert len(settings_dao.read_tokens(client)) == len(legacy_tokens)

    token = random_api_token(client=client, permissions=[TokenPermission.TEST_RECOMMENDATIONS])
    settings_dao.create_api_token(token)

    assert len(settings_dao.read_tokens(client)) == 1 + len(legacy_tokens)
    created_token = None
    for token in settings_dao.read_tokens(client):
        if token.description != "Legacy token. Can not be deleted":
            created_token = token
            break
    assert created_token is not None
    assert created_token == token

    settings_dao.delete_token(client, token.token_hash)
    assert len(settings_dao.read_tokens(client)) == len(legacy_tokens)


def test_expire_token_deletion(settings_dao, client):
    settings_dao.delete_all_tokens(client)
    legacy_tokens = [token for token in settings_dao.read_tokens(client) if token.description is not None
                     and token.description == "Legacy token. Can not be deleted"]
    assert len(settings_dao.read_tokens(client)) == len(legacy_tokens)

    token = random_api_token(client=client, expiration_date=datetime.utcnow() - timedelta(minutes=1),
                             permissions=[TokenPermission.TEST_RECOMMENDATIONS])
    settings_dao.create_api_token(token)

    assert len(settings_dao.read_tokens(client)) == 1 + len(legacy_tokens)
    created_token = None
    for token in settings_dao.read_tokens(client):
        if token.description != "Legacy token. Can not be deleted":
            created_token = token
            break
    assert created_token is not None
    assert created_token == token

    settings_dao.delete_expired_tokens()
    assert len(settings_dao.read_tokens(client)) == len(legacy_tokens)
