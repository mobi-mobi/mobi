import pytest
import random
import string

from datetime import datetime, timedelta
from typing import List

from mobi.config.client import get_client_config
from mobi.config.system.api.recommendation_api import get_recommendation_api_config
from mobi.core.models import AttributedRecommendation, AttributionType, Event, EventPlatform, EventType, Product,\
    Recommendation, RecommendationSource, RecommendationType
from mobi.core.population import Population
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao, get_recolog_dao
from mobi.dao.cache import UserHistoryCookie
from mobi.ml.model.embedding.impl.factories.deepwalk import DeepwalkStaticEmbeddingModelFactory
from mobi.utils.tests import random_event, random_product
from mobi.utils.tests.train_data import simple_train_data, simple_train_data_with_null_values


def random_string(len: int = 10):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(len))


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def config():
    return get_recommendation_api_config()


@pytest.fixture(scope="session")
def client_config(client):
    return get_client_config(client)


@pytest.fixture(scope="session")
def cache_dao(config):
    return get_cache_dao(config)


@pytest.fixture(scope="session")
def catalog_dao(config):
    return get_catalog_dao(config)


@pytest.fixture(scope="session")
def event_dao(config):
    return get_event_dao(config)


@pytest.fixture(scope="session")
def recolog_dao(config):
    return get_recolog_dao(config)


@pytest.fixture
def training_products(catalog_dao, event_dao, recolog_dao, client) -> List[Product]:
    products, events = simple_train_data()

    catalog_dao.delete_all_products(client)
    for product in products:
        catalog_dao.create_product(client, product)

    event_dao.delete_all_events(client)
    for event in events:
        event_dao.create_event(client, event)

    recolog_dao.delete_all_attributed_recommendations(client)
    for event in events:
        reco_products = {random.choice(products).product_id for _ in range(10)}
        reco_products.add(event.product_id)

        recolog_dao.log_attributed_recommendation(client, AttributedRecommendation(
            recommendation=Recommendation(
                user_id=event.user_id,
                date=event.date - timedelta(seconds=10),
                type=RecommendationType.PERSONAL_RECOMMENDATIONS,
                source=RecommendationSource.UNKNOWN,
                product_ids=list(reco_products),
            ),
            attribution_type=AttributionType.EMPIRICAL,
            attributed_events=[event]
        ))

    yield products

    event_dao.delete_all_events(client)
    catalog_dao.delete_all_products(client)
    recolog_dao.delete_all_attributed_recommendations(client)


@pytest.fixture
def training_products_with_empty_parameters(catalog_dao, event_dao, recolog_dao, client) -> List[Product]:
    products, events = simple_train_data_with_null_values()

    catalog_dao.delete_all_products(client)
    for product in products:
        catalog_dao.create_product(client, product)

    event_dao.delete_all_events(client)
    for event in events:
        event_dao.create_event(client, event)

    recolog_dao.delete_all_attributed_recommendations(client)
    for event in events:
        reco_products = {random.choice(products).product_id for _ in range(10)}
        reco_products.add(event.product_id)

        if random.random() > 0.1:
            reco_type = RecommendationType.SIMILAR_PRODUCTS
            reco_products = [event.product_id]
        else:
            reco_type = random.choice((RecommendationType.PERSONAL_RECOMMENDATIONS,
                                       RecommendationType.SIMILAR_PRODUCTS,
                                       RecommendationType.STATIC_RECOMMENDATIONS,
                                       RecommendationType.BASKET_RECOMMENDATIONS,
                                       RecommendationType.MOST_POPULAR,
                                       RecommendationType.RECENTLY_VIEWED))

        recolog_dao.log_attributed_recommendation(client, AttributedRecommendation(
            recommendation=Recommendation(
                user_id=event.user_id,
                date=event.date - timedelta(seconds=10),
                type=reco_type,
                source=RecommendationSource.UNKNOWN,
                product_ids=list(reco_products),
            ),
            attribution_type=AttributionType.EMPIRICAL,
            attributed_events=[event]
        ))

    yield products

    event_dao.delete_all_events(client)
    catalog_dao.delete_all_products(client)
    recolog_dao.delete_all_attributed_recommendations(client)


@pytest.fixture
def training_products_with_empty_parameters_with_no_attr_recos(catalog_dao, event_dao, recolog_dao, client) \
        -> List[Product]:
    products, events = simple_train_data_with_null_values()

    catalog_dao.delete_all_products(client)
    for product in products:
        catalog_dao.create_product(client, product)

    event_dao.delete_all_events(client)
    for event in events:
        event_dao.create_event(client, event)

    recolog_dao.delete_all_attributed_recommendations(client)

    for event in events:
        reco_products = {random.choice(products).product_id for _ in range(10)}
        reco_products.add(event.product_id)

        if random.random() > 0.5:
            reco_type = RecommendationType.SIMILAR_PRODUCTS
            reco_products = [event.product_id]
        else:
            reco_type = random.choice((RecommendationType.PERSONAL_RECOMMENDATIONS,
                                       RecommendationType.SIMILAR_PRODUCTS,
                                       RecommendationType.STATIC_RECOMMENDATIONS,
                                       RecommendationType.BASKET_RECOMMENDATIONS,
                                       RecommendationType.MOST_POPULAR,
                                       RecommendationType.RECENTLY_VIEWED))

        recolog_dao.log_attributed_recommendation(client, AttributedRecommendation(
            recommendation=Recommendation(
                user_id=event.user_id,
                date=event.date - timedelta(seconds=10),
                type=reco_type,
                source=RecommendationSource.UNKNOWN,
                product_ids=list(reco_products),
            ),
            attribution_type=AttributionType.EMPIRICAL,
            attributed_events=[event]
        ))

    yield products

    event_dao.delete_all_events(client)
    catalog_dao.delete_all_products(client)
    recolog_dao.delete_all_attributed_recommendations(client)


def test_deepwalk_factory(training_products, cache_dao, catalog_dao, event_dao, recolog_dao, client, config):
    factory = DeepwalkStaticEmbeddingModelFactory(
        catalog_dao,
        event_dao,
        recolog_dao,
        datetime.utcnow() - timedelta(days=1),
        datetime.utcnow()
    )
    model = factory.train(client)
    all_products = list(catalog_dao.read_all_products(client))
    model.set_non_recommendable_product_ids([product.product_id for product in all_products
                                             if not product.active or not product.in_stock])
    model.compile(all_products)
    assert len(model.__getattribute__("knn_tree")) == len(training_products)

    for product in training_products:
        model.similar_products(product)

    for product in training_products:
        model.basket_recommendations([product.product_id])

    for product in training_products:
        model.personal_recommendations([Event(
            user_id="user_id",
            user_population=Population.TARGET,
            event_type=EventType.PRODUCT_VIEW,
            event_platform=EventPlatform.ANDROID,
            product_id=product.product_id,
            product_version=0,
            date=datetime.utcnow()
        )])


def test_deepwalk_factory_empty_product_parameters(training_products_with_empty_parameters, catalog_dao, event_dao,
                                                   recolog_dao, client, config):
    factory = DeepwalkStaticEmbeddingModelFactory(
        catalog_dao,
        event_dao,
        recolog_dao,
        datetime.utcnow() - timedelta(days=1),
        datetime.utcnow()
    )
    model = factory.train(client)
    all_products = list(catalog_dao.read_all_products(client))
    model.set_non_recommendable_product_ids([product.product_id for product in all_products
                                             if not product.active or not product.in_stock])
    model.compile(all_products)
    assert len(model.__getattribute__("knn_tree")) == len(training_products_with_empty_parameters)

    for product in training_products_with_empty_parameters:
        model.similar_products(product)

    for product in training_products_with_empty_parameters:
        model.basket_recommendations([product.product_id])

    for product in training_products_with_empty_parameters:
        model.personal_recommendations([Event(
            user_id="user_id",
            user_population=Population.TARGET,
            event_type=EventType.PRODUCT_VIEW,
            event_platform=EventPlatform.ANDROID,
            product_id=product.product_id,
            product_version=0,
            date=datetime.utcnow()
        )])


def test_deepwalk_factory_empty_product_parameters_and_empty_attr_recos(
        training_products_with_empty_parameters_with_no_attr_recos, catalog_dao, event_dao, recolog_dao, client,
        config):
    factory = DeepwalkStaticEmbeddingModelFactory(
        catalog_dao,
        event_dao,
        recolog_dao,
        datetime.utcnow() - timedelta(days=1),
        datetime.utcnow()
    )
    model = factory.train(client)
    all_products = list(catalog_dao.read_all_products(client))
    model.set_non_recommendable_product_ids([product.product_id for product in all_products
                                             if not product.active or not product.in_stock])
    model.compile(all_products)
    assert len(model.__getattribute__("knn_tree")) == len(training_products_with_empty_parameters_with_no_attr_recos)

    for product in training_products_with_empty_parameters_with_no_attr_recos:
        model.similar_products(product)

    for product in training_products_with_empty_parameters_with_no_attr_recos:
        model.basket_recommendations([product.product_id])

    for product in training_products_with_empty_parameters_with_no_attr_recos:
        model.personal_recommendations([Event(
            user_id="user_id",
            user_population=Population.TARGET,
            event_type=EventType.PRODUCT_VIEW,
            event_platform=EventPlatform.ANDROID,
            product_id=product.product_id,
            product_version=0,
            date=datetime.utcnow()
        )])


def test_catboost_classifier(training_products_with_empty_parameters_with_no_attr_recos, cache_dao, catalog_dao,
                             event_dao, recolog_dao, client, config):

    factory = DeepwalkStaticEmbeddingModelFactory(
        catalog_dao,
        event_dao,
        recolog_dao,
        datetime.utcnow() - timedelta(days=1),
        datetime.utcnow()
    )
    model = factory.train(client)
    all_products = list(catalog_dao.read_all_products(client))
    model.set_non_recommendable_product_ids([product.product_id for product in all_products
                                             if not product.active or not product.in_stock])
    model.compile(all_products)
    assert len(model.__getattribute__("knn_tree")) == len(training_products_with_empty_parameters_with_no_attr_recos)

    user_id = random_string()
    user_history = UserHistoryCookie()

    for i in range(5):
        product = random.choice(all_products)
        event_type = random.choice((EventType.PRODUCT_VIEW, EventType.PRODUCT_TO_WISHLIST, EventType.PRODUCT_TO_BASKET))
        event = random_event(
            user_id=user_id,
            product_id=product.product_id,
            event_type=event_type,
            date=datetime.utcnow() - timedelta(hours=6 * i)
        )
        user_history.append(event)

    # Scoring similar

    scores = model.classify_products(client, RecommendationType.SIMILAR_PRODUCTS, user_history, all_products,
                                     cache_dao, source_product=all_products[0])

    assert isinstance(scores, list)
    assert len(scores) == len(all_products)
    assert all(map(lambda x: isinstance(x, float), scores))
    assert all(map(lambda x: x > 0, scores))

    # Scoring other recommendations

    for reco_type in RecommendationType:
        scores = model.classify_products(client, reco_type, user_history, all_products, cache_dao)

        assert isinstance(scores, list)
        assert len(scores) == len(all_products)
        assert all(map(lambda x: isinstance(x, float), scores))
        assert all(map(lambda x: x > 0, scores))

    # Scoring unexisting data

    bad_products = []

    product = random_product()
    product.categories = []
    bad_products.append(product)

    product = random_product()
    product.categories = None
    bad_products.append(product)

    product = random_product()
    product.categories = ["__SOME_UNKNOWN_CATEGORY__", "__SOME_UNKNOWN_CATEGORY__2__"]
    bad_products.append(product)

    product = random_product()
    product.brand = "__UNKNOWN_BRAND___"
    bad_products.append(product)

    product = random_product()
    product.title = "____SOME____NEW__WORDS____ _____HERE__________"
    bad_products.append(product)

    product = random_product()
    product.enriched_tags = None
    bad_products.append(product)

    product = random_product()
    product.enriched_tags = []
    bad_products.append(product)

    product = random_product()
    product.enriched_tags = ["__SOME_UNKNOWN_ENRICHED_TAG__", "__SOME_UNKNOWN_ENRICHED_TAG__2__"]
    bad_products.append(product)

    product = random_product()
    product.tags = None
    bad_products.append(product)

    product = random_product()
    product.tags = []
    bad_products.append(product)

    product = random_product()
    product.tags = ["__SOME_UNKNOWN_ENRICHED_TAG__", "__SOME_UNKNOWN_ENRICHED_TAG__2__"]
    bad_products.append(product)

    for product in bad_products:
        scores = model.classify_products(client, RecommendationType.PERSONAL_RECOMMENDATIONS, user_history, [product],
                                         cache_dao)

        assert isinstance(scores, list)
        assert len(scores) == 1
        assert scores[0] > 0


