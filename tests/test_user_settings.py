import pytest

from mobi.config.system.api.event_api import get_event_api_config
from mobi.dao.user_settings import MobiUserSettingsDao, ReportSubscription
from mobi.dao import get_user_settings_dao


@pytest.fixture(scope="session")
def user_settings_dao() -> MobiUserSettingsDao:
    yield get_user_settings_dao(get_event_api_config())


@pytest.fixture(scope="session")
def client() -> str:
    yield "testclient"


@pytest.fixture(scope="session")
def user() -> str:
    yield "testuser"


def test_create_user_settings_dao(user_settings_dao):
    pass


def test_delete_property(client, user, user_settings_dao):
    str_property = "str_property"
    int_property = "int_property"
    object_property = "object_property"

    user_settings_dao.delete_all_properties(user)
    assert user_settings_dao.read_str_property(user, str_property) is None
    assert user_settings_dao.read_str_property(user, int_property) is None
    assert user_settings_dao.read_str_property(user, object_property) is None

    user_settings_dao.set_str_property(user, str_property, "test_value")
    assert user_settings_dao.read_str_property(user, str_property) == "test_value"

    user_settings_dao.delete_property(user, str_property)
    assert user_settings_dao.read_str_property(user, str_property) is None

    user_settings_dao.set_int_property(user, int_property, 42)
    assert user_settings_dao.read_int_property(user, int_property) == 42

    user_settings_dao.delete_property(user, int_property)
    assert user_settings_dao.read_int_property(user, int_property) is None

    user_settings_dao.set_object_property(user, object_property, {"a": 42})
    assert user_settings_dao.read_object_property(user, object_property) == {"a": 42}

    user_settings_dao.delete_property(user, object_property)
    assert user_settings_dao.read_object_property(user, object_property) is None


def test_set_read_str_property(client, user, user_settings_dao):
    str_property = "str_property"

    user_settings_dao.delete_all_properties(user)
    assert user_settings_dao.read_str_property(user, str_property) is None

    user_settings_dao.set_str_property(user, str_property, "some_value")
    assert user_settings_dao.read_str_property(user, str_property) == "some_value"

    assert user_settings_dao.read_str_property(user, str_property, client=client) is None
    user_settings_dao.set_str_property(user, str_property, "some_other_value", client=client)
    assert user_settings_dao.read_str_property(user, str_property, client=client) == "some_other_value"


def test_set_read_int_property(client, user, user_settings_dao):
    int_property = "int_property"

    user_settings_dao.delete_all_properties(user)
    assert user_settings_dao.read_int_property(user, int_property) is None

    user_settings_dao.set_int_property(user, int_property, 42)
    assert user_settings_dao.read_int_property(user, int_property) == 42

    assert user_settings_dao.read_int_property(user, int_property, client=client) is None
    user_settings_dao.set_int_property(user, int_property, 43, client=client)
    assert user_settings_dao.read_int_property(user, int_property, client=client) == 43


def test_set_read_str_list_property(client, user, user_settings_dao):
    str_property = "str_list_property"

    user_settings_dao.delete_all_properties(user)
    assert user_settings_dao.read_str_list_property(user, str_property) is None

    user_settings_dao.set_str_list_property(user, str_property, ["some_value_1", "some_value_2"])
    assert user_settings_dao.read_str_list_property(user, str_property) == ["some_value_1", "some_value_2"]
    user_settings_dao.set_str_list_property(user, str_property, [])
    assert user_settings_dao.read_str_list_property(user, str_property) == []

    assert user_settings_dao.read_str_list_property(user, str_property, client=client) is None
    user_settings_dao.set_str_list_property(user, str_property, ["some_other_value_1", "some_other_value_2"],
                                            client=client)
    assert user_settings_dao.read_str_list_property(user, str_property, client=client) \
           == ["some_other_value_1", "some_other_value_2"]


def test_set_read_int_list_property(client, user, user_settings_dao):
    int_property = "int_list_property"

    user_settings_dao.delete_all_properties(user)
    assert user_settings_dao.read_int_list_property(user, int_property) is None

    user_settings_dao.set_int_list_property(user, int_property, [42, 43])
    assert user_settings_dao.read_int_list_property(user, int_property) == [42, 43]
    user_settings_dao.set_int_list_property(user, int_property, [])
    assert user_settings_dao.read_int_list_property(user, int_property) == []

    assert user_settings_dao.read_int_list_property(user, int_property, client=client) is None
    user_settings_dao.set_int_list_property(user, int_property, [42, 43],
                                            client=client)
    assert user_settings_dao.read_int_list_property(user, int_property, client=client) \
           == [42, 43]


def test_set_read_object_property(client, user, user_settings_dao):
    object_property = "object_property"

    user_settings_dao.delete_all_properties(user)
    assert user_settings_dao.read_object_property(user, object_property) is None

    user_settings_dao.set_object_property(user, object_property, {}, client)
    assert user_settings_dao.read_object_property(user, object_property, client) == {}

    user_settings_dao.set_object_property(user, object_property, {"a": "b"}, client)
    assert user_settings_dao.read_object_property(user, object_property, client) == {"a": "b"}

    for bad_key in [None, 1, 1.1, ""]:
        with pytest.raises(AssertionError):
            user_settings_dao.set_object_property(user, object_property, {bad_key: "b"}, client)

    for bad_value in [{}, []]:
        with pytest.raises(AssertionError):
            user_settings_dao.set_object_property(user, object_property, {"a": bad_value}, client)

    user_settings_dao.set_object_property(user, object_property, {"a a": "b"}, client)
    assert user_settings_dao.read_object_property(user, object_property, client) == {"a a": "b"}

    user_settings_dao.set_object_property(user, object_property, {"a a ": "b"}, client)
    assert user_settings_dao.read_object_property(user, object_property, client) == {"a a ": "b"}


def test_user_report_subscriptions(client, user, user_settings_dao):
    user_settings_dao.delete_all_properties(user)
    assert user_settings_dao.read_report_subscriptions_list(user, client) == []

    user_settings_dao.add_report_subscription(user, client, ReportSubscription.WEEKLY)
    assert user_settings_dao.read_report_subscriptions_list(user, client) == [ReportSubscription.WEEKLY]

    user_settings_dao.add_report_subscription(user, client, ReportSubscription.WEEKLY)
    assert user_settings_dao.read_report_subscriptions_list(user, client) == [ReportSubscription.WEEKLY]

    user_settings_dao.add_report_subscription(user, client, ReportSubscription.DAILY)
    assert user_settings_dao.read_report_subscriptions_list(user, client) == [ReportSubscription.DAILY,
                                                                              ReportSubscription.WEEKLY]

    user_settings_dao.add_report_subscription(user, client, ReportSubscription.DAILY)
    assert user_settings_dao.read_report_subscriptions_list(user, client) == [ReportSubscription.DAILY,
                                                                              ReportSubscription.WEEKLY]

    user_settings_dao.remove_report_subscription(user, client, ReportSubscription.WEEKLY)
    assert user_settings_dao.read_report_subscriptions_list(user, client) == [ReportSubscription.DAILY]

    user_settings_dao.remove_report_subscription(user, client, ReportSubscription.DAILY)
    assert user_settings_dao.read_report_subscriptions_list(user, client) == []


def test_get_update_last_report_email_id(client, user, user_settings_dao):
    user_settings_dao.delete_all_properties(user)
    email_id = "some_email_id"
    other_email_id = "other_email_id"

    assert user_settings_dao.get_last_report_email_id(user, client, ReportSubscription.DAILY) is None
    assert user_settings_dao.get_last_report_email_id(user, client, ReportSubscription.WEEKLY) is None

    user_settings_dao.update_last_report_email_id(user, client, ReportSubscription.WEEKLY, email_id)

    assert user_settings_dao.get_last_report_email_id(user, client, ReportSubscription.DAILY) is None
    assert user_settings_dao.get_last_report_email_id(user, client, ReportSubscription.WEEKLY) == email_id

    user_settings_dao.update_last_report_email_id(user, client, ReportSubscription.WEEKLY, other_email_id)

    assert user_settings_dao.get_last_report_email_id(user, client, ReportSubscription.DAILY) is None
    assert user_settings_dao.get_last_report_email_id(user, client, ReportSubscription.WEEKLY) == other_email_id

    user_settings_dao.update_last_report_email_id(user, client, ReportSubscription.DAILY, email_id)

    assert user_settings_dao.get_last_report_email_id(user, client, ReportSubscription.DAILY) == email_id
    assert user_settings_dao.get_last_report_email_id(user, client, ReportSubscription.WEEKLY) == other_email_id
