import pytest

from datetime import datetime, timedelta

from mobi.config.system.api.event_api import get_event_api_config
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao import get_catalog_dao, get_event_dao
from mobi.utils.tests import random_event, random_product, random_string


@pytest.fixture(scope="session")
def catalog_dao() -> MobiCatalogDao:
    yield get_catalog_dao(get_event_api_config())


@pytest.fixture(scope="session")
def event_dao() -> MobiEventDao:
    yield get_event_dao(get_event_api_config())


@pytest.fixture(scope="session")
def client() -> str:
    yield "testclient"


def test_get_dao(catalog_dao):
    pass


def test_same_version_on_update(client, catalog_dao):
    product_id = random_string()
    product = random_product(product_id)

    catalog_dao.create_product(client, product)

    read_product = catalog_dao.read_product(client, product_id)

    for key in product.as_dict().keys():
        if key != "version":
            assert product.as_dict()[key] == read_product.as_dict()[key]
    assert product.version is None
    assert read_product.version == 1

    catalog_dao.create_product(client, product)

    new_read_product = catalog_dao.read_product(client, product_id)
    for key in read_product.as_dict().keys():
        if key != "version":
            assert read_product.as_dict()[key] == new_read_product.as_dict()[key]

    assert read_product.version == new_read_product.version


def test_different_version_on_update(client, catalog_dao):
    product_id = random_string()
    product = random_product(product_id, brand="brand1")

    catalog_dao.create_product(client, product)

    read_product = catalog_dao.read_product(client, product_id)

    for key in product.as_dict().keys():
        if key != "version":
            assert product.as_dict()[key] == read_product.as_dict()[key]
    assert product.version is None
    assert read_product.version == 1

    product.brand = "brand2"
    catalog_dao.create_product(client, product)

    new_read_product = catalog_dao.read_product(client, product_id)
    assert read_product != new_read_product
    assert read_product.version == new_read_product.version - 1


def test_read_products(client, catalog_dao):
    product_id_1 = random_string()
    product_id_2 = random_string()

    product_1 = random_product(product_id_1)
    product_2 = random_product(product_id_2)

    catalog_dao.create_product(client, product_1)
    catalog_dao.create_product(client, product_2)

    products = list(catalog_dao.read_products(client, [product_id_1, product_id_2]))
    assert len(products) == 2

    products = list(catalog_dao.read_products(client, [product_id_1, product_id_2], respect_order=True))
    assert len(products) == 2
    assert products[0].product_id == product_id_1
    assert products[1].product_id == product_id_2

    products = list(catalog_dao.read_products(client, [product_id_2, product_id_1], respect_order=True))
    assert len(products) == 2
    assert products[0].product_id == product_id_2
    assert products[1].product_id == product_id_1


def test_count_products(client, catalog_dao):
    product_id_1 = random_string()
    product_id_2 = random_string()

    product_1 = random_product(product_id_1)
    product_2 = random_product(product_id_2)

    catalog_dao.create_product(client, product_1)
    c = catalog_dao.get_products_number(client)
    assert c > 0

    catalog_dao.create_product(client, product_2)
    assert catalog_dao.get_products_number(client) == c + 1


def test_delete_all_products(client, catalog_dao):
    product_id_1 = random_string()
    product_id_2 = random_string()

    product_1 = random_product(product_id_1)
    product_2 = random_product(product_id_2)

    catalog_dao.create_product(client, product_1)
    catalog_dao.create_product(client, product_2)

    assert len(list(catalog_dao.read_all_products(client))) >= 2

    catalog_dao.delete_all_products(client)

    assert not len(list(catalog_dao.read_all_products(client)))


def test_delete_fatigue_versions(client, catalog_dao, event_dao):
    event_dao.delete_all_events(client)
    catalog_dao.delete_all_products(client)

    product = random_product()
    catalog_dao.create_product(client, product)

    assert len(list(catalog_dao.read_all_products(client))) == 1

    event_1 = random_event(product_id=product.product_id, date=datetime.utcnow() - timedelta(days=42))
    event_dao.create_event(client, event_1)

    assert len(list(event_dao.read_events(client))) == 1

    product.title = f"New title of product{product.product_id}"
    catalog_dao.create_product(client, product)

    assert len(list(catalog_dao.read_all_products(client))) == 1

    event_2 = random_event(product_id=product.product_id, date=datetime.utcnow() - timedelta(days=38))
    event_dao.create_event(client, event_2)

    assert len(list(event_dao.read_events(client))) == 2

    event_dao.delete_fatigue_events(client, datetime.utcnow() - timedelta(days=40))

    assert len(list(event_dao.read_events(client))) == 1

    catalog_dao.delete_fatigue_versions(client, product.product_id)

    assert len(list(catalog_dao.read_all_products(client))) == 1
    new_product = catalog_dao.read_product(client, product.product_id)
    assert product == new_product


def test_search_products(client, catalog_dao):
    catalog_dao.delete_all_products(client)

    product = random_product(title="Test Product")
    inactive_product = random_product(title="Also test product", active=False)
    catalog_dao.create_product(client, product)
    catalog_dao.create_product(client, inactive_product)

    active_products = list(catalog_dao.read_all_products(client))
    assert len(active_products) == 1
    all_products = list(catalog_dao.read_all_products(client, include_inactive=True))
    assert len(all_products) == 2


def test_read_all_brands(client, catalog_dao):
    catalog_dao.delete_all_products(client)

    product = random_product(brand="test brand 1")
    catalog_dao.create_product(client, product)

    all_brands = list(catalog_dao.read_all_brands(client))
    assert len(all_brands) == 1

    product = random_product(brand="test brand 2")
    catalog_dao.create_product(client, product)

    all_brands = list(catalog_dao.read_all_brands(client))
    assert len(all_brands) == 2

    product = random_product(brand="test brand 2")
    catalog_dao.create_product(client, product)

    all_brands = list(catalog_dao.read_all_brands(client))
    assert len(all_brands) == 2


def test_read_all_categories(client, catalog_dao):
    catalog_dao.delete_all_products(client)

    product = random_product(categories=["test category 1", "test category 2"])
    catalog_dao.create_product(client, product)

    all_brands = list(catalog_dao.read_all_categories(client))
    assert len(all_brands) == 2

    product = random_product(categories=["test category 1", "test category 3"])
    catalog_dao.create_product(client, product)

    all_brands = list(catalog_dao.read_all_categories(client))
    assert len(all_brands) == 3

    product = random_product(categories=["test category 1", "test category 3"])
    catalog_dao.create_product(client, product)

    all_brands = list(catalog_dao.read_all_categories(client))
    assert len(all_brands) == 3
