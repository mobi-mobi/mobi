import pytest

from typing import Generator

from mobi.config.system.api.event_api import get_event_api_config
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao import get_metrics_dao


@pytest.fixture(scope="session")
def metrics_dao() -> Generator[MobiMetricsDao, None, None]:
    yield get_metrics_dao(get_event_api_config())


def test_get_dao(metrics_dao):
    pass
