import pytest
from mobi.config._base import MobiConfig
from mobi.config.client import get_client_config


def test_unexisting_key():
    c = MobiConfig()
    with pytest.raises(KeyError):
        _ = c["__UNEXISTING_KEY__"]


def test_existing_key():
    class TestConfig(MobiConfig):
        SOME_KEY = "SOME_VALUE"

    conf = TestConfig()

    assert conf.get("SOME_KEY") == "SOME_VALUE"
    assert conf.get("SOME_WRONG_KEY") is None
    assert conf.get("SOME_WRONG_KEY", "DEFAULT_VALUE") == "DEFAULT_VALUE"


def test_wrong_config():
    with pytest.raises(ValueError):
        _ = get_client_config("wrongtestclient")


def test_memoization():
    config = get_client_config("testclient")
    with pytest.raises(AttributeError):
        _ = config.SOMETESTPARAMETER
    config.SOMETESTPARAMETER = 1
    config = get_client_config("testclient")
    try:
        config.SOMETESTPARAMETER
    except AttributeError:
        # Maybe we were "lucky" enough to execute this test right on the edge of a 60 second interval
        config.SOMETESTPARAMETER = 1
        config = get_client_config("testclient")
    assert config.SOMETESTPARAMETER == 1
