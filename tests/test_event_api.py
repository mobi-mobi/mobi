import json
import pytest
from flask import testing
from mobi.api.event_api import EventApi
from mobi.config.system.api.event_api import get_event_api_config
from mobi.core.models import EventPlatform, EventType
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao
from mobi.utils.tests import random_product, random_string


class TestEventApiClient(testing.FlaskClient):
    MOBI_TOKEN = "testtoken"

    def open(self, *args, **kwargs):
        if self.MOBI_TOKEN is not None:
            headers = kwargs.get('headers', {})
            headers['Authorization'] = "Bearer " + self.MOBI_TOKEN
            kwargs['headers'] = headers
        return super().open(*args, **kwargs)


class TestEventApiClientWithHashedToken(TestEventApiClient):
    MOBI_TOKEN = "testtokenhashed"


class TestEventApiClientWithNoToken(TestEventApiClient):
    MOBI_TOKEN = None


class TestEventApiClientWithExpiredToken(TestEventApiClient):
    MOBI_TOKEN = "expiredtesttoken"


class TestEventApiClientWithWrongToken(TestEventApiClient):
    MOBI_TOKEN = "__testnonexistingtoken__"


class TestEventApiClientWithProdToken(TestEventApiClient):
    MOBI_TOKEN = "testtokenprod"


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def event_api_client():
    api = EventApi()
    api.app.test_client_class = TestEventApiClient

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_expired_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithExpiredToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_hashed_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithHashedToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_no_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithNoToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_wrong_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithWrongToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_prod_token():
    api = EventApi()
    api.app.test_client_class = TestEventApiClientWithProdToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def event_dao():
    return get_event_dao(get_event_api_config())


@pytest.fixture(scope="session")
def catalog_dao():
    return get_catalog_dao(get_event_api_config())


@pytest.fixture(scope="session")
def cache_dao():
    return get_cache_dao(get_event_api_config())


@pytest.fixture
def random_non_existing_product(client, catalog_dao):
    product = random_product()
    try:
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass

    yield product

    try:
        # Who knows, maybe it was already deleted in the test body
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass


@pytest.fixture
def random_existing_product(client, catalog_dao, random_non_existing_product):
    catalog_dao.create_product(client, random_non_existing_product)
    yield random_non_existing_product

    try:
        # Who knows, maybe it was already deleted in the test body
        catalog_dao.delete_product(client, random_non_existing_product.product_id)
    except Exception:
        pass


def test_event_api_init(event_api_client):
    pass


def test_status(client_with_no_token):
    rv = client_with_no_token.get("/v2/user/status")
    assert rv.data == b'{"status": "OK"}'


def test_empty_request_body(event_api_client):
    rv = event_api_client.post("/v2/user/events", data="{}")
    assert b'Can not parse request body' not in rv.data


def test_wrong_request_body(event_api_client):
    rv = event_api_client.post("/v2/user/events", data=None)
    assert b'Event schema validation failed' in rv.data
    assert b'Can not parse JSON string' in rv.data

    rv = event_api_client.post("/v2/user/events", data="{{")
    assert b'Event schema validation failed' in rv.data
    assert b'Can not parse JSON string' in rv.data


def test_status_with_no_token(client_with_no_token):
    rv = client_with_no_token.get("/v2/user/status")
    assert rv.data == b'{"status": "OK"}'


def test_no_tokens(client_with_no_token):
    rv = client_with_no_token.get("/v2/user/non_existing_url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 401' in rv.data
    assert b'No token provided' in rv.data


def test_wrong_token(client_with_wrong_token):
    rv = client_with_wrong_token.get("/v2/user/non_existing_url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data


def test_wrong_token_in_uri(client_with_no_token):
    rv = client_with_no_token.get("/v2/user/non_existing_url?token=__testnonexistingtoken__")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data


def test_with_expired_token(client_with_expired_token):
    rv = client_with_expired_token.get("/v2/user/non_existing_url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is expired' in rv.data


def test_wrong_url(event_api_client):
    rv = event_api_client.get("/v2/user/non_existing_url")
    assert b'"status": "ERROR"' in rv.data
    assert b'The requested URL was not found on the server' in rv.data


def test_create_event(client, event_api_client, event_dao, random_existing_product):
    event_dao.delete_all_events(client)
    assert len(list(event_dao.read_events(client))) == 0

    data = {
        "user_id": "user_id",
        "product_id": random_existing_product.product_id,
        "event_type": "product_view",
        "event_platform": "ios"
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert rv.data == b'{"status": "OK"}'
    assert len(list(event_dao.read_events(client))) == 1


def test_create_product_view_event(event_api_client, random_existing_product):
    request_dict = {
        "user_id": "user_id",
        "event_type": EventType.PRODUCT_VIEW.name.lower(),
        "event_platform": EventPlatform.ANDROID.name.lower()
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'\\"product_id\\" must be defined for an event type of type product_view' in rv.data

    request_dict["product_id"] = random_existing_product.product_id
    request_dict["product_quantity"] = 1

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'Event field \\"product_quantity\\" must not be set for an event of type product_view' in rv.data

    del request_dict["product_quantity"]
    request_dict["basket_items"] = [{"product_id": random_existing_product.product_id, "product_quantity": 1}]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'Event field \\"basket_items\\" must not be set for an event of type product_view' in rv.data

    del request_dict["basket_items"]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert rv.data == b'{"status": "OK"}'


def test_create_product_to_whishlist_event(event_api_client, random_existing_product):
    request_dict = {
        "user_id": "user_id",
        "event_type": EventType.PRODUCT_TO_WISHLIST.name.lower(),
        "event_platform": EventPlatform.ANDROID.name.lower()
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'\\"product_id\\" must be defined for an event type of type product_to_wishlist' in rv.data

    request_dict["product_id"] = random_existing_product.product_id
    request_dict["product_quantity"] = 1

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'Event field \\"product_quantity\\" must not be set for an event of type product_to_wishlist' in rv.data

    del request_dict["product_quantity"]
    request_dict["basket_items"] = [{"product_id": random_existing_product.product_id, "product_quantity": 1}]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'Event field \\"basket_items\\" must not be set for an event of type product_to_wishlist' in rv.data

    del request_dict["basket_items"]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert rv.data == b'{"status": "OK"}'


def test_create_product_to_basket_event(event_api_client, random_existing_product):
    request_dict = {
        "user_id": "user_id",
        "event_type": EventType.PRODUCT_TO_BASKET.name.lower(),
        "event_platform": EventPlatform.ANDROID.name.lower()
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'\\"product_id\\" must be defined for an event type of type product_to_basket' in rv.data

    request_dict["product_id"] = random_existing_product.product_id
    request_dict["basket_items"] = [{"product_id": random_existing_product.product_id, "product_quantity": 1}]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'Event field \\"basket_items\\" must not be set for an event of type product_to_basket' in rv.data

    del request_dict["basket_items"]
    request_dict["product_quantity"] = 0

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'Parameter product_quantity: 0 is less than the minimum of 1' in rv.data

    request_dict["product_quantity"] = 1

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert rv.data == b'{"status": "OK"}'


def test_create_product_sale_event(event_api_client, random_existing_product):
    request_dict = {
        "user_id": "user_id",
        "event_type": EventType.PRODUCT_SALE.name.lower(),
        "event_platform": EventPlatform.ANDROID.name.lower()
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'\\"basket_items\\" must be defined for an event type of type product_sale' in rv.data

    request_dict["product_id"] = random_existing_product.product_id
    request_dict["basket_items"] = [{"product_id": random_existing_product.product_id, "product_quantity": 1}]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'Event field \\"product_id\\" must not be set for an event of type product_sale' in rv.data

    del request_dict["product_id"]
    request_dict["product_quantity"] = 1

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert b'Can not parse event parameters' in rv.data
    assert b'Event field \\"product_quantity\\" must not be set for an event of type product_sale' in rv.data

    del request_dict["product_quantity"]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(request_dict))
    assert rv.data == b'{"status": "OK"}'


def test_create_event_wrong_data(event_api_client):
    data = {
        "user_id": "user_id",
        "event_platform": EventPlatform.ANDROID.name.lower()
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'Can not parse event parameters' in rv.data
    assert b'\'event_type\' is a required property' in rv.data

    data["event_type"] = EventType.UNKNOWN.name.lower()

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'Can not parse event parameters' in rv.data
    assert b'Unknown event type: unknown' in rv.data

    data["event_type"] = 42

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'Can not parse event parameters' in rv.data
    assert b'Parameter event_type: 42 is not of type \'string\'' in rv.data

    data["event_type"] = EventType.PRODUCT_VIEW.name.lower()
    del data["event_platform"]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'Can not parse event parameters' in rv.data
    assert b'\'event_platform\' is a required property' in rv.data

    data["event_platform"] = EventPlatform.ANDROID.name.lower()
    del data["user_id"]

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'Can not parse event parameters' in rv.data
    assert b'\'user_id\' is a required property' in rv.data

    data["user_id"] = "user_id"

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'Can not parse event parameters' in rv.data
    assert b'\\"product_id\\" must be defined for an event type of type product_view' in rv.data


def test_create_event_custom_datetime(client, event_api_client, event_dao, random_existing_product):
    product = random_existing_product

    data = {
        "user_id": "userid",
        "product_id": product.product_id,
        "event_type": "PRODUCT_VIEW",
        "event_platform": EventPlatform.ANDROID.name.lower(),
        "date": "2001-02-03 04:05:06"
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert rv.data == b'{"status": "OK"}'

    for event in event_dao.read_events(client):
        if event.product_id == product.product_id:
            assert event.date.strftime("%Y-%m-%d %H:%M:%S") == "2001-02-03 04:05:06"
            return

    assert False, "Product is not found"


def test_create_event_wrong_datetime_format(event_api_client, random_existing_product):
    product = random_existing_product

    data = {
        "user_id": "userid",
        "product_id": product.product_id,
        "event_type": "PRODUCT_VIEW",
        "event_platform": EventPlatform.ANDROID.name.lower()
    }

    for dt in ["2001-02-03", "2001-02-03 04:05", "04:05:06"]:
        data["date"] = dt
        rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
        assert b'"status": "ERROR"' in rv.data
        assert b'Event schema validation failed' in rv.data
        assert b'Can not parse event parameters' in rv.data
        assert b'does not match' in rv.data

    for dt in ["2001-02-03 04:05:61", "2001-02-32 04:05:59", "2001-02-03 25:05:59"]:
        data["date"] = dt
        rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
        assert b'"status": "ERROR"' in rv.data
        assert b'Event schema validation failed' in rv.data
        assert b'Can not parse datetime' in rv.data


def test_create_event_unknown_parameter(event_api_client, random_existing_product):
    product = random_existing_product

    data = {
        "user_id": random_string(),
        "product_id": product.product_id,
        "event_type": EventType.PRODUCT_VIEW.name.lower(),
        "event_platform": EventPlatform.ANDROID.name.lower(),
        "wrong_parameter": random_string()
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert b'Event schema validation failed' in rv.data
    assert b'Additional properties are not allowed' in rv.data
    assert b'\'wrong_parameter\' was unexpected' in rv.data


def test_create_event_for_non_existing_product(event_api_client, random_non_existing_product):
    data = {
        "user_id": random_string(),
        "product_id": random_non_existing_product.product_id,
        "event_type": "PRODUCT_VIEW",
        "event_platform": EventPlatform.ANDROID.name.lower()
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert b'"status": "ERROR"' in rv.data
    assert b'does not exist' in rv.data


def test_delete_for_prod_token(client_with_prod_token):
    rv = client_with_prod_token.delete("/v2/user/all_events")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Can not perform delete operation for a non-test token' in rv.data


def test_delete_all_events(client, event_api_client, event_dao, random_existing_product):
    event_dao.delete_all_events(client)

    data = {
        "user_id": random_string(),
        "product_id": random_existing_product.product_id,
        "event_type": "PRODUCT_VIEW",
        "event_platform": EventPlatform.ANDROID.name.lower()
    }

    rv = event_api_client.post("/v2/user/events", data=json.dumps(data))
    assert rv.data == b'{"status": "OK"}'

    assert len(list(event_dao.read_events(client))) == 1

    rv = event_api_client.delete("/v2/user/all_events")
    assert rv.data == b'{"status": "OK"}'

    assert len(list(event_dao.read_events(client))) == 0


