import pytest

from datetime import datetime, timedelta
from elasticsearch.exceptions import RequestError
from time import sleep

from mobi.config.system.api.recommendation_api import get_recommendation_api_config
from mobi.core.models import Product
from mobi.dao import get_search_dao
from mobi.dao.search.impl.elasticserch import ElasticDao
from mobi.utils.tests import random_product, random_string


@pytest.fixture(scope="session")
def client():
    return "testclient"


@pytest.fixture
def search_dao(client):
    dao = get_search_dao(get_recommendation_api_config())
    dao.delete_index(client)
    dao.create_index(client)
    yield dao
    dao.delete_index(client)


@pytest.fixture
def random_indexed_product(search_dao, client) -> Product:
    product = random_product()
    search_dao.index_product(client, product, refresh_index=True)
    yield product
    search_dao.delete_product(client, product.product_id)


def test_create_index_twice(client, search_dao):
    with pytest.raises(RequestError):
        search_dao.create_index(client)


def test_delete_index_twice(client, search_dao):
    search_dao.delete_index(client)
    search_dao.delete_index(client)


def test_add_product_with_no_index(client, search_dao):
    search_dao.delete_index(client)
    product = random_product()
    with pytest.raises(IndexError):
        search_dao.index_product(client, product)

    with pytest.raises(IndexError):
        search_dao.index_brand(client, product.brand)

    with pytest.raises(IndexError):
        search_dao.index_category(client, "some category")


def test_deleting_fatigue_products(client, search_dao):
    product = random_product(product_id="product_1", title="product_1")

    _, results = search_dao.search_product(client, "product_1")
    assert not results

    search_dao.index_product(client, product, updated_ts=99)
    search_dao.refresh_index(client)

    _, results = search_dao.search_product(client, "product_1")
    assert len(results) == 1

    search_dao.delete_fatigue_data(client, 99)
    search_dao.refresh_index(client)

    _, results = search_dao.search_product(client, "product_1")
    assert len(results) == 1

    search_dao.delete_fatigue_data(client, 100)
    search_dao.refresh_index(client)

    _, results = search_dao.search_product(client, "product_1")
    assert not results


def test_deleting_fatigue_brands(client, search_dao):
    _, results = search_dao.search_brand(client, "brands")
    assert not results

    search_dao.index_brand(client, "brand", updated_ts=99)
    search_dao.refresh_index(client)

    _, results = search_dao.search_brand(client, "brand")
    assert len(results) == 1

    search_dao.delete_fatigue_data(client, 99)
    search_dao.refresh_index(client)

    _, results = search_dao.search_brand(client, "brand")
    assert len(results) == 1

    search_dao.delete_fatigue_data(client, 100)
    search_dao.refresh_index(client)

    _, results = search_dao.search_brand(client, "brand")
    assert not results


def test_deleting_fatigue_categories(client, search_dao):
    _, results = search_dao.search_product(client, "category")
    assert not results

    search_dao.index_category(client, "category", updated_ts=99)
    search_dao.refresh_index(client)

    _, results = search_dao.search_category(client, "category")
    assert len(results) == 1

    search_dao.delete_fatigue_data(client, 99)
    search_dao.refresh_index(client)

    _, results = search_dao.search_category(client, "category")
    assert len(results) == 1

    search_dao.delete_fatigue_data(client, 100)
    search_dao.refresh_index(client)

    _, results = search_dao.search_category(client, "category")
    assert not results


def test_index_product(search_dao, client):
    product = random_product()
    search_dao.index_product(client, product)


def test_find_product(search_dao, client):
    not_in_stock_avail_date = datetime.utcnow() - timedelta(days=1)

    product1 = random_product(title="Test Product")
    product2 = random_product(title="Also Test Product", active=False)
    product3 = random_product(title="Also Test Product", available_until=not_in_stock_avail_date)
    product4 = random_product(title="Also Test Product", available_until=not_in_stock_avail_date, active=False)

    for product in [product1, product2, product3, product4]:
        search_dao.index_product(client, product, refresh_index=True)

    n, results = search_dao.search_product(client, "Test Product", include_inactive=False, include_not_in_stock=False)
    assert n == 1
    assert len(results) == 1
    assert results[0].product_id == product1.product_id

    n, results = search_dao.search_product(client, "Test Product")
    assert n == 1
    assert len(results) == 1
    assert results[0].product_id == product1.product_id

    n, results = search_dao.search_product(client, "Test Product", include_inactive=True)
    assert n == 2
    assert len(results) == 2

    n, results = search_dao.search_product(client, "Test Product", include_not_in_stock=True)
    assert n == 2
    assert len(results) == 2

    n, results = search_dao.search_product(client, "Test Product", include_inactive=True, include_not_in_stock=True)
    assert n == 4
    assert len(results) == 4

    n, results = search_dao.search_product(client, "Test Product", include_inactive=True, include_not_in_stock=True,
                                           limit=2, offset=1)
    assert n == 4
    assert len(results) == 2

    n, results = search_dao.search_product(client, "Test Product", include_inactive=True, include_not_in_stock=True,
                                           limit=4, offset=1)
    assert n == 4
    assert len(results) == 3

    n, results = search_dao.search_product(client, "Test Product", include_inactive=True, offset=1)
    assert n == 2
    assert len(results) == 1

    n, results = search_dao.search_product(client, "Test Product", include_inactive=True, offset=2)
    assert n == 2
    assert len(results) == 0

    n, results = search_dao.search_product(client, "Test Product", include_inactive=True, limit=1)
    assert n == 2
    assert len(results) == 1


def test_find_product_with_brands_filtering(search_dao, client):
    not_in_stock_avail_date = datetime.utcnow() - timedelta(days=1)

    product1 = random_product(title="Test Product", brand="Brand A")
    product2 = random_product(title="Also Test Product", brand="Brand A", active=False)
    product3 = random_product(title="Also Test Product", brand="Brand B", available_until=not_in_stock_avail_date)
    product4 = random_product(title="Also Test Product", brand="Brand B", available_until=not_in_stock_avail_date,
                              active=False)

    for product in [product1, product2, product3, product4]:
        search_dao.index_product(client, product, refresh_index=True)

    n, results = search_dao.search_product(client, "Test", brands=["Brand A"], include_inactive=True,
                                           include_not_in_stock=True)
    assert n == 2
    assert len(results) == 2

    n, results = search_dao.search_product(client, "Test", brands=[" Brand A  "], include_inactive=True,
                                           include_not_in_stock=True)
    assert n == 2
    assert len(results) == 2

    n, results = search_dao.search_product(client, "Test", brands=["Brand A", "Brand B"], include_inactive=True,
                                           include_not_in_stock=True)
    assert n == 4
    assert len(results) == 4

    n, results = search_dao.search_product(client, "Test", brands=["Brand A", "Brand B"], include_inactive=False,
                                           include_not_in_stock=False)
    assert n == 1
    assert len(results) == 1


def test_index_brand(search_dao, client):
    brand = random_string()
    search_dao.index_brand(client, brand)


def test_index_category(search_dao, client):
    brand = random_string()
    search_dao.index_category(client, brand)


def test_with_no_indexes_creatd(search_dao, client):
    assert not search_dao.search_product(client, "product")[1]
    assert not search_dao.search_brand(client, "brand")[1]
    assert not search_dao.search_category(client, "category")[1]


def test_index_bad_brand(search_dao, client):
    for brand in ["", " ", "\t", " \t", 123, [], 1.1, b"bin"]:
        with pytest.raises(ValueError):
            search_dao.index_brand(client, brand)


def test_find_brand(search_dao, client):
    brand = random_string()
    search_dao.index_brand(client, brand, refresh_index=True)

    n, results = search_dao.search_brand(client, brand)
    assert n == 1
    assert len(results) == 1

    assert results[0].brand == brand


def test_rand_brands(search_dao, client):
    brand_1 = "111 222"
    brand_2 = "111"
    brand_3 = "111 222 333"

    for brand in [brand_1, brand_2, brand_3]:
        search_dao.index_brand(client, brand, refresh_index=True)

    n, results = search_dao.search_brand(client, "111 222 333")
    assert n == 3
    assert len(results) == 3

    assert results[0].brand == brand_3
    assert results[1].brand == brand_1
    assert results[2].brand == brand_2

    n, results = search_dao.search_brand(client, "111 222 33")
    assert n == 3
    assert len(results) == 3

    assert results[0].brand == brand_3
    assert results[1].brand == brand_1
    assert results[2].brand == brand_2

    n, results = search_dao.search_brand(client, "11")
    assert n == 3
    assert len(results) == 3


def test_rand_categories(search_dao, client):
    category_1 = "111 222"
    category_2 = "111"
    category_3 = "111 222 333"

    for category in [category_1, category_2, category_3]:
        search_dao.index_category(client, category, refresh_index=True)

    n, results = search_dao.search_category(client, "111 222 333")
    assert n == 3
    assert len(results) == 3

    assert results[0].category == category_3
    assert results[1].category == category_1
    assert results[2].category == category_2

    n, results = search_dao.search_category(client, "111 222 33")
    assert n == 3
    assert len(results) == 3

    assert results[0].category == category_3
    assert results[1].category == category_1
    assert results[2].category == category_2

    n, results = search_dao.search_category(client, "11")
    assert n == 3
    assert len(results) == 3


def test_refresh_index(search_dao, client):
    product = random_product()
    product.title = random_string()
    search_dao.index_product(client, product, refresh_index=False)

    n, results = search_dao.search_product(client, query=product.title)
    assert n == 0
    assert len(results) == 0

    search_dao.refresh_index(client)

    n, results = search_dao.search_product(client, query=product.title)
    assert n == 1
    assert len(results) == 1

    search_dao.delete_product(client, product.product_id, refresh_index=False)

    n, results = search_dao.search_product(client, query=product.title)
    assert n == 1
    assert len(results) == 1

    search_dao.refresh_index(client)

    n, results = search_dao.search_product(client, query=product.title)
    assert n == 0
    assert len(results) == 0


def test_es_automatic_refresh(search_dao, client):
    """ From ES Docs: By default, Elasticsearch periodically refreshes indices every second,
    but only on indices that have received one search request or more in the last 30 seconds
    """
    if not isinstance(search_dao, ElasticDao):
        return

    product = random_product()
    product.title = random_string()

    search_dao.index_product(client, product, refresh_index=False)
    assert not search_dao.search_product(client, query=product.title)[1]

    sleep(2)

    assert search_dao.search_product(client, query=product.title)[1]


def test_es_full_text(search_dao: ElasticDao):
    if not isinstance(search_dao, ElasticDao):
        return

    product = random_product(
        title="111 222 333 444",
        brand="111 333"
    )

    assert search_dao._product_full_text(product) == "111 333 222 444"

    product = random_product(
        title="   word1  BrAnd       word2  ",
        brand=" brand  "
    )

    assert search_dao._product_full_text(product) == "brand word1 word2"

    product = random_product(
        title="   word1  BrAndword2  ",
        brand=" brand  "
    )

    assert search_dao._product_full_text(product) == "brand word1 brandword2"


def test_read_all_brands(search_dao, client):
    brands = {random_string(10) for _ in range(100)}

    for brand in brands:
        search_dao.index_brand(client, brand, refresh_index=False)

    search_dao.refresh_index(client)

    all_brands = list(search_dao.read_brands(client, _bulk_size=10))

    assert len(brands) == len(all_brands)
    assert sorted(list(brands)) == sorted(all_brands)


def test_read_all_categories(search_dao, client):
    categories = {random_string(10) for _ in range(100)}

    for category in categories:
        search_dao.index_category(client, category, refresh_index=False)

    search_dao.refresh_index(client)

    all_categories = list(search_dao.read_categories(client, _bulk_size=10))

    assert len(categories) == len(all_categories)
    assert sorted(list(categories)) == sorted(all_categories)
