import pytest

from mobi.ml.common.knn_index import BallTreeKnnIndex, KnnMultiParametrizedIndex, KnnParametrizedIndex


ELEMENTS = [
    ("zero", [0., 0.]),
    ("one", [1., 1.]),
    ("two", [2., 2.])
]

BAD_ELEMENTS = [
    ("zero", [0.]),
    ("one", [1., 1.])
]

REPEATED_ELEMENTS = [
    ("zero", [0.]),
    ("on1", [1.]),
    ("zero", [0.])
]

PARAMETRIZED_ELEMENTS = [
    ("zero", "red", [0., 0.]),
    ("one", "green", [1., 1.]),
    ("two", "red", [2., 2.])
]

MULTI_PARAMETRIZED_ELEMENTS = [
    ("zero", ["red", "big"], [0., 0.]),
    ("one", ["green", "big"], [1., 1.]),
    ("two", ["red", "small"], [2., 2.]),
    ("three", ["a", "b"], [3., 3.]),
    ("three", ["a", "b"], [4., 4.]),
    ("three", ["a", "b"], [5., 5.])
]


def test_wrong_data():
    global ELEMENTS

    knn_index = BallTreeKnnIndex(ELEMENTS)

    with pytest.raises(ValueError):
        knn_index.query([0.], k=1)

    with pytest.raises(ValueError):
        knn_index.query([0., 0.], k=4)

    with pytest.raises(ValueError):
        BallTreeKnnIndex(BAD_ELEMENTS)

    with pytest.raises(ValueError):
        BallTreeKnnIndex([])


def test_ball_tree():
    global ELEMENTS
    eps = 0.1 ** 6

    knn_index = BallTreeKnnIndex(ELEMENTS)

    assert len(knn_index.items) == len(ELEMENTS)
    assert len(knn_index.query([0., 0.], k=3)) == 3

    assert abs(knn_index.query([0., 0.], k=3)[0][0] - 0.) < eps
    assert knn_index.query([0., 0.], k=3)[0][1] == "zero"

    assert len(knn_index.query([1.2, 1.0], k=2)) == 2
    assert knn_index.query([1.2, 1.0], k=2)[0][1] == "one"
    assert knn_index.query([1.2, 1.0], k=2)[1][1] == "two"

    knn_index = BallTreeKnnIndex(REPEATED_ELEMENTS)

    assert len(knn_index.items) == len(REPEATED_ELEMENTS)


def test_parametrized_index():
    global PARAMETRIZED_ELEMENTS
    eps = 0.1 ** 6

    knn_index = KnnParametrizedIndex(PARAMETRIZED_ELEMENTS, BallTreeKnnIndex)

    assert len(knn_index.query([0., 0.], "blue", k=3)) == 0
    assert len(knn_index.query([0., 0.], "red", k=3)) == 2
    assert len(knn_index.query([0., 0.], "green", k=3)) == 1

    assert abs(knn_index.query([0., 0.], "green", k=3)[0][0] - 1.4142135623) < eps
    assert knn_index.query([0., 0.], "green", k=3)[0][1] == "one"


def test_multi_parametrized_index():
    global MULTI_PARAMETRIZED_ELEMENTS
    eps = 0.1 ** 6

    knn_index = KnnMultiParametrizedIndex(MULTI_PARAMETRIZED_ELEMENTS, BallTreeKnnIndex, parameters_num=2)

    # -- red

    assert len(knn_index.query([0., 0.], ["red", "big"], k=3)) == 1
    assert abs(knn_index.query([0., 0.], ["red", "big"], k=3)[0][0]) < eps

    assert len(knn_index.query([0., 0.], ["red", "small"], k=3)) == 1
    assert abs(knn_index.query([0., 0.], ["red", "small"], k=3)[0][0] - 2.8284271247) < eps

    assert len(knn_index.query([0., 0.], ["red", "unknown"], k=3)) == 0
    assert len(knn_index.query([0., 0.], ["unknown", "small"], k=3)) == 0

    # -- green

    assert len(knn_index.query([0., 0.], ["green", "big"], k=3)) == 1
    assert abs(knn_index.query([0., 0.], ["green", "big"], k=3)[0][0] - 1.4142135623) < eps

    assert len(knn_index.query([0., 0.], ["green", "small"], k=3)) == 0

    assert len(knn_index.query([0., 0.], ["green", "unknown"], k=3)) == 0
    assert len(knn_index.query([0., 0.], ["unknown", "small"], k=3)) == 0

    # -- three

    assert len(knn_index.query([0., 0.], ["a", "b"], k=3)) == 3
    assert abs(knn_index.query([0., 0.], ["a", "b"], k=3)[0][0] - 4.2426406871192) < eps
    assert abs(knn_index.query([0., 0.], ["a", "b"], k=3)[1][0] - 5.6568542494923) < eps
    assert abs(knn_index.query([0., 0.], ["a", "b"], k=3)[2][0] - 7.0710678118654) < eps

    # exception

    with pytest.raises(ValueError):
        knn_index.query([0., 0.], ["a"], k=1)

    with pytest.raises(ValueError):
        knn_index.query([0., 0.], ["a", "b", "c"], k=1)

    with pytest.raises(ValueError):
        knn_index.query([0., 0.], ["a", "b", "b"], k=1)
