import pytest

from datetime import datetime, timedelta
from time import sleep
from typing import List

from mobi.config.client import get_client_config
from mobi.config.system.service.cache_builder import get_cache_builder_config
from mobi.config.system.service.model_builder import get_model_builder_config
from mobi.core.models import EventType, Product
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao, get_metrics_dao, get_model_dao
from mobi.dao.cache import cache_key, MobiCache
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.event import MobiEventDao
from mobi.dao.metrics import MobiMetricsDao
from mobi.dao.object import MobiModelDao
from mobi.service.task.impl.cache_builder_task import CacheBuilderTask
from mobi.service.task.impl.catalog_cache_builder_task import CatalogCacheBuilderTask
from mobi.service.task.impl.churn_rate_task import ChurnRateTask
from mobi.service.task.impl.dynamic_counters_task import DynamicCountersTask
from mobi.service.task.impl.model_builder import ModelBuilderTask
from mobi.service.task.impl.new_users_task import NewUsersTask
from mobi.service.task.impl.retention_cleaner import RetentionCleanerTask
from mobi.utils.tests import random_event, random_product
from mobi.utils.tests.train_data import simple_train_data


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def cache_dao() -> MobiCache:
    return get_cache_dao(get_cache_builder_config())


@pytest.fixture(scope="session")
def catalog_dao() -> MobiCatalogDao:
    return get_catalog_dao(get_cache_builder_config())


@pytest.fixture(scope="session")
def event_dao() -> MobiEventDao:
    yield get_event_dao(get_cache_builder_config())


@pytest.fixture(scope="session")
def metrics_dao() -> MobiMetricsDao:
    yield get_metrics_dao(get_cache_builder_config())


@pytest.fixture(scope="session")
def model_dao() -> MobiModelDao:
    yield get_model_dao(get_model_builder_config())


@pytest.fixture
def training_products(catalog_dao, event_dao, client) -> List[Product]:
    products, events = simple_train_data()

    catalog_dao.delete_all_products(client)
    for product in products:
        catalog_dao.create_product(client, product)

    event_dao.delete_all_events(client)
    for event in events:
        event_dao.create_event(client, event)

    yield products

    event_dao.delete_all_events(client)
    catalog_dao.delete_all_products(client)


def test_cache_builder(client, catalog_dao, event_dao):
    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()

    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)

    product_1 = random_product(brand="brand_1", categories=["category_1"])
    catalog_dao.create_product(client, product_1)
    product_2 = random_product(brand="brand_2", categories=["category_2", "category_1"])
    catalog_dao.create_product(client, product_2)
    product_3 = random_product(brand="brand_1", categories=["category_2", "category_1", "category_3"])
    catalog_dao.create_product(client, product_3)

    event = random_event(product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)

    cache_builder.process_client(client, global_context, client_context)

    assert len(client_context["counters"]) == 2
    assert client_context["counters"][product_1.product_id] == 2
    assert client_context["counters"][product_2.product_id] == 1
    assert client_context["counters"][product_3.product_id] == 0

    assert len(client_context["brand_counters"]) == 2

    assert len(client_context["brand_counters"]["brand_1"]) == 1
    assert client_context["brand_counters"]["brand_1"][product_1.product_id] == 2
    assert client_context["brand_counters"]["brand_1"][product_2.product_id] == 0
    assert client_context["brand_counters"]["brand_1"][product_3.product_id] == 0

    assert len(client_context["brand_counters"]["brand_2"]) == 1
    assert client_context["brand_counters"]["brand_2"][product_1.product_id] == 0
    assert client_context["brand_counters"]["brand_2"][product_2.product_id] == 1
    assert client_context["brand_counters"]["brand_2"][product_3.product_id] == 0

    assert len(client_context["product_brand_map"]) == 3
    assert client_context["product_brand_map"][product_1.product_id] == "brand_1"

    assert len(client_context["categories_counters"]) == 2
    assert len(client_context["categories_counters"]["category_1"]) == 2
    assert client_context["categories_counters"]["category_1"][product_1.product_id] == 2
    assert client_context["categories_counters"]["category_1"][product_2.product_id] == 1
    assert len(client_context["categories_counters"]["category_2"]) == 1
    assert client_context["categories_counters"]["category_2"][product_1.product_id] == 0
    assert client_context["categories_counters"]["category_2"][product_2.product_id] == 1

    assert len(client_context["global_brands_counter"]) == 2
    assert client_context["global_brands_counter"]["brand_1"] == 2
    assert client_context["global_brands_counter"]["brand_2"] == 1

    assert len(client_context["global_categories_counter"]) == 2
    assert client_context["global_categories_counter"]["category_1"] == 3
    assert client_context["global_categories_counter"]["category_2"] == 1

    cache_builder.process_client(client, global_context, client_context)

    assert len(client_context["counters"]) == 2
    assert client_context["counters"][product_1.product_id] == 2
    assert client_context["counters"][product_2.product_id] == 1
    assert client_context["counters"][product_3.product_id] == 0

    assert len(client_context["brand_counters"]) == 2

    assert len(client_context["brand_counters"]["brand_1"]) == 1
    assert client_context["brand_counters"]["brand_1"][product_1.product_id] == 2
    assert client_context["brand_counters"]["brand_1"][product_2.product_id] == 0
    assert client_context["brand_counters"]["brand_1"][product_3.product_id] == 0

    assert len(client_context["brand_counters"]["brand_2"]) == 1
    assert client_context["brand_counters"]["brand_2"][product_1.product_id] == 0
    assert client_context["brand_counters"]["brand_2"][product_2.product_id] == 1
    assert client_context["brand_counters"]["brand_2"][product_3.product_id] == 0

    assert len(client_context["product_brand_map"]) == 3
    assert client_context["product_brand_map"][product_1.product_id] == "brand_1"

    assert len(client_context["categories_counters"]) == 2
    assert len(client_context["categories_counters"]["category_1"]) == 2
    assert client_context["categories_counters"]["category_1"][product_1.product_id] == 2
    assert client_context["categories_counters"]["category_1"][product_2.product_id] == 1
    assert len(client_context["categories_counters"]["category_2"]) == 1
    assert client_context["categories_counters"]["category_2"][product_1.product_id] == 0
    assert client_context["categories_counters"]["category_2"][product_2.product_id] == 1

    assert len(client_context["global_brands_counter"]) == 2
    assert client_context["global_brands_counter"]["brand_1"] == 2
    assert client_context["global_brands_counter"]["brand_2"] == 1

    assert len(client_context["global_categories_counter"]) == 2
    assert client_context["global_categories_counter"]["category_1"] == 3
    assert client_context["global_categories_counter"]["category_2"] == 1

    sleep(2)

    event = random_event(product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(seconds=1))
    event_dao.create_event(client, event)
    event_dao.create_event(client, event)

    event = random_event(product_id=product_3.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(seconds=1))
    event_dao.create_event(client, event)

    cache_builder.process_client(client, global_context, client_context)

    assert len(client_context["counters"]) == 3
    assert client_context["counters"][product_1.product_id] == 2
    assert client_context["counters"][product_2.product_id] == 3
    assert client_context["counters"][product_3.product_id] == 1

    assert len(client_context["brand_counters"]) == 2

    assert len(client_context["brand_counters"]["brand_1"]) == 2
    assert client_context["brand_counters"]["brand_1"][product_1.product_id] == 2
    assert client_context["brand_counters"]["brand_1"][product_2.product_id] == 0
    assert client_context["brand_counters"]["brand_1"][product_3.product_id] == 1

    assert len(client_context["brand_counters"]["brand_2"]) == 1
    assert client_context["brand_counters"]["brand_2"][product_1.product_id] == 0
    assert client_context["brand_counters"]["brand_2"][product_2.product_id] == 3
    assert client_context["brand_counters"]["brand_2"][product_3.product_id] == 0

    assert len(client_context["categories_counters"]) == 3
    assert len(client_context["categories_counters"]["category_1"]) == 3
    assert client_context["categories_counters"]["category_1"][product_1.product_id] == 2
    assert client_context["categories_counters"]["category_1"][product_2.product_id] == 3
    assert client_context["categories_counters"]["category_1"][product_3.product_id] == 1
    assert len(client_context["categories_counters"]["category_2"]) == 2
    assert client_context["categories_counters"]["category_2"][product_1.product_id] == 0
    assert client_context["categories_counters"]["category_2"][product_2.product_id] == 3
    assert client_context["categories_counters"]["category_2"][product_3.product_id] == 1
    assert len(client_context["categories_counters"]["category_3"]) == 1
    assert client_context["categories_counters"]["category_3"][product_1.product_id] == 0
    assert client_context["categories_counters"]["category_3"][product_2.product_id] == 0
    assert client_context["categories_counters"]["category_3"][product_3.product_id] == 1

    assert len(client_context["global_brands_counter"]) == 2
    assert client_context["global_brands_counter"]["brand_1"] == 3
    assert client_context["global_brands_counter"]["brand_2"] == 3

    assert len(client_context["global_categories_counter"]) == 3
    assert client_context["global_categories_counter"]["category_1"] == 6
    assert client_context["global_categories_counter"]["category_2"] == 4
    assert client_context["global_categories_counter"]["category_3"] == 1


def test_catalog_cache_builder(client, catalog_dao, metrics_dao):
    catalog_dao.delete_all_products(client)
    metrics_dao.delete_all(client)

    for base in (60, 60 * 60, 24 * 60 * 60):
        assert metrics_dao.get_latest_point_value(client, "products_num", {}, base) is None

    in_stock_dt = datetime.utcnow() + timedelta(days=1)
    not_in_stock_dt = datetime.utcnow() - timedelta(days=1)

    catalog_dao.create_product(client, random_product(active=True, available_until=in_stock_dt))
    catalog_dao.create_product(client, random_product(active=True, available_until=in_stock_dt))
    catalog_dao.create_product(client, random_product(active=True, available_until=in_stock_dt))
    catalog_dao.create_product(client, random_product(active=True, available_until=in_stock_dt))

    catalog_dao.create_product(client, random_product(active=True, available_until=not_in_stock_dt))
    catalog_dao.create_product(client, random_product(active=True, available_until=not_in_stock_dt))
    catalog_dao.create_product(client, random_product(active=True, available_until=not_in_stock_dt))

    catalog_dao.create_product(client, random_product(active=False, available_until=in_stock_dt))
    catalog_dao.create_product(client, random_product(active=False, available_until=in_stock_dt))

    catalog_dao.create_product(client, random_product(active=False, available_until=not_in_stock_dt))

    cache_builder = CatalogCacheBuilderTask("Test Catalog Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()
    cache_builder.process_client(client, global_context, client_context)

    for base in (60, 60 * 60, 24 * 60 * 60):
        assert metrics_dao.get_latest_point_value(client, "products_num", {}, base) == 10
        assert metrics_dao.get_latest_point_value(client, "products_num", {"active": "yes"}, base) == 7
        assert metrics_dao.get_latest_point_value(client, "products_num", {"in_stock": "yes"}, base) == 6
        assert metrics_dao.get_latest_point_value(client, "products_num", {"active": "yes", "in_stock": "yes"},
                                                  base) == 4

        assert metrics_dao.get_latest_point_value(client, "products_num", {"active": "no"}, base) == 3
        assert metrics_dao.get_latest_point_value(client, "products_num", {"in_stock": "no"}, base) == 4
        assert metrics_dao.get_latest_point_value(client, "products_num", {"active": "no", "in_stock": "no"}, base) == 1

        assert metrics_dao.get_latest_point_value(client, "products_num", {"active": "yes", "in_stock": "no"},
                                                  base) == 3
        assert metrics_dao.get_latest_point_value(client, "products_num", {"active": "no", "in_stock": "yes"},
                                                  base) == 2


def test_catalog_stats_cache_builder(client, cache_dao, catalog_dao, event_dao, metrics_dao):
    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)
    metrics_dao.delete_all(client)

    for base in (60, 60 * 60, 24 * 60 * 60):
        assert metrics_dao.get_latest_point_value(client, "g_product_stats", {}, base) is None

    product_1 = random_product(product_id="product_1")
    product_2 = random_product(product_id="product_2")
    product_3 = random_product(product_id="product_3")
    product_4 = random_product(product_id="product_4")
    catalog_dao.create_product(client, product_1)
    catalog_dao.create_product(client, product_2)
    catalog_dao.create_product(client, product_3)
    catalog_dao.create_product(client, product_4)

    event_dao.create_event(client, random_event(product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW))
    event_dao.create_event(client, random_event(product_id=product_1.product_id,
                                                event_type=EventType.PRODUCT_TO_WISHLIST))
    event_dao.create_event(client, random_event(product_id=product_1.product_id,
                                                event_type=EventType.PRODUCT_TO_BASKET))
    event_dao.create_event(client, random_event(product_id=product_1.product_id, event_type=EventType.PRODUCT_SALE))
    event_dao.create_event(client, random_event(product_id=product_1.product_id, event_type=EventType.PRODUCT_SALE))
    event_dao.create_event(client, random_event(product_id=product_1.product_id, event_type=EventType.PRODUCT_SALE))

    event_dao.create_event(client, random_event(product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW))
    event_dao.create_event(client, random_event(product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW))

    cache_builder = CatalogCacheBuilderTask("Test Catalog Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()
    cache_builder.process_client(client, global_context, client_context)

    assert client_context

    event_dao.create_event(client, random_event(product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW))

    cache_builder.process_client(client, global_context, client_context)

    def get_cache_key(event_type: EventType, product_id: str) -> str:
        return cache_key("product_events", client=client, event_type=event_type.name.lower(), product_id=product_id,
                         period="24h")

    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_VIEW, product_1.product_id)) == 1
    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_TO_BASKET, product_1.product_id)) == 1
    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_SALE, product_1.product_id)) == 3
    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_TO_WISHLIST, product_1.product_id)) == 1

    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_VIEW, product_2.product_id)) == 3
    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_TO_BASKET, product_2.product_id)) == 0
    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_SALE, product_2.product_id)) == 0
    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_TO_WISHLIST, product_2.product_id)) == 0

    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_VIEW, product_3.product_id)) == 0
    assert cache_dao.get_int(get_cache_key(EventType.PRODUCT_VIEW, product_4.product_id)) == 0


def test_model_builder(client, model_dao, training_products):
    model_dao.delete_all_model_dumps(client)
    assert len(list(model_dao.read_all_model_dumps(client))) == 0

    model_builder = ModelBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()

    model_builder.process_client(client, global_context, client_context)

    last_attempt = client_context.get("last_attempt")
    assert last_attempt is not None
    assert len(list(model_dao.read_all_model_dumps(client))) == 1

    model_builder.process_client(client, global_context, client_context)

    assert client_context.get("last_attempt") is not None
    assert client_context.get("last_attempt") == last_attempt
    assert len(list(model_dao.read_all_model_dumps(client))) == 1

    model_dao.delete_all_model_dumps(client)


def test_retention_cleaner(client, event_dao):
    event_dao.delete_all_events(client)

    assert not len(list(event_dao.read_events(client)))
    events_retention = get_client_config(client).EVENTS_RETENTION_DAYS

    event_1 = random_event(date=datetime.utcnow() - timedelta(days=events_retention, hours=1))
    event_dao.create_event(client, event_1)
    event_2 = random_event(date=datetime.utcnow() - timedelta(days=events_retention-1, hours=23))
    event_dao.create_event(client, event_2)

    assert len(list(event_dao.read_events(client))) == 2

    retention_cleaner = RetentionCleanerTask("Retention Cleaner Task", [client])
    global_context = dict()
    client_context = dict()
    retention_cleaner.process_client(client, global_context, client_context)

    assert len(list(event_dao.read_events(client))) == 1
    assert list(event_dao.read_events(client))[0].product_id == event_2.product_id

    retention_cleaner.process_client(client, global_context, client_context)

    assert len(list(event_dao.read_events(client))) == 1
    assert list(event_dao.read_events(client))[0].product_id == event_2.product_id


def test_new_users(client, event_dao, metrics_dao):
    event_dao.delete_all_events(client)
    metrics_dao.delete_all(client)

    new_users_task = NewUsersTask("New Users Task", [client])
    new_users_task.ON_INIT_RECALC_FROM = {
        60: timedelta(days=1),
        60 * 60: timedelta(days=1),
        24 * 60 * 60: timedelta(days=1)
    }

    now = datetime.utcnow() - timedelta(minutes=1)

    global_context = dict()
    client_context = dict()
    metrics_dao.delete_all(client)
    new_users_task.process_client(client, global_context, client_context)

    event_dao.create_event(client, random_event("user_1", date=now - timedelta(days=20)))
    event_dao.create_event(client, random_event("user_1", date=now))

    global_context = dict()
    client_context = dict()
    metrics_dao.delete_all(client)
    new_users_task.process_client(client, global_context, client_context)

    for base in (60, 60 * 60, 24 * 60 * 60):
        metric = metrics_dao.get_metrics_last(client, new_users_task.METRIC_NAME, None, int(now.timestamp()) - base)
        assert not int(metric)

    event_dao.create_event(client, random_event("user_2", date=now))

    global_context = dict()
    client_context = dict()
    metrics_dao.delete_all(client)
    new_users_task.process_client(client, global_context, client_context)

    for base in (60, 60 * 60, 24 * 60 * 60):
        metric = metrics_dao.get_metrics_last(client, new_users_task.METRIC_NAME, None, int(now.timestamp()) - base)
        assert int(metric) == 1

    event_dao.create_event(client, random_event("user_2", date=now))
    event_dao.create_event(client, random_event("user_2", date=now))
    event_dao.create_event(client, random_event("user_3", date=now - timedelta(days=50)))
    event_dao.create_event(client, random_event("user_3", date=now))
    event_dao.create_event(client, random_event("user_4", date=now - timedelta(days=20)))
    event_dao.create_event(client, random_event("user_4", date=now))

    global_context = dict()
    client_context = dict()
    metrics_dao.delete_all(client)
    new_users_task.process_client(client, global_context, client_context)

    for base in (60, 60 * 60, 24 * 60 * 60):
        metric = metrics_dao.get_metrics_last(client, new_users_task.METRIC_NAME, None, int(now.timestamp()) - base)
        assert int(metric) == 2


def test_dynamic_counters_task(client, catalog_dao, event_dao):
    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)
    now_dt = datetime.utcnow()

    for _ in range(100):
        product = random_product()
        catalog_dao.create_product(client, product)
        event_dao.create_event(client, random_event("user_1", product_id=product.product_id,
                                                    date=now_dt - timedelta(minutes=1)))

    task = DynamicCountersTask("DynamicCountersTask", [client])
    global_context = dict()
    client_context = dict()
    task.process_client(client, global_context, client_context)


def test_churn_rate_task(client, event_dao, metrics_dao):
    datetime_now = datetime.utcnow()

    event_dao.delete_all_events(client)
    metrics_dao.delete_all(client)
    metrics_base = 24 * 60 * 60

    assert metrics_dao.get_latest_point_timestamp(client, "churned_users", None, base=metrics_base) is None

    churn_rate_task = ChurnRateTask("ChurnRateTask", [client])
    global_context = dict()
    client_context = dict()
    churn_rate_task.process_client(client, global_context, client_context)
    assert client_context

    event = random_event(user_id="churned_user", date=datetime_now - timedelta(days=7))
    event_dao.create_event(client, event)
    event = random_event(user_id="another_churned_user", date=datetime_now - timedelta(days=7))
    event_dao.create_event(client, event)
    event = random_event(user_id="another_churned_user", date=datetime_now + timedelta(days=1))
    event_dao.create_event(client, event)
    event = random_event(user_id="not_churned_user", date=datetime_now - timedelta(days=7))
    event_dao.create_event(client, event)
    event = random_event(user_id="not_churned_user", date=datetime_now - timedelta(days=6))
    event_dao.create_event(client, event)
    event = random_event(user_id="not_churned_user_2", date=datetime_now - timedelta(days=7))
    event_dao.create_event(client, event)
    event = random_event(user_id="not_churned_user_2", date=datetime_now)
    event_dao.create_event(client, event)
    global_context = dict()
    client_context = dict()

    # Testing backfilling
    for i in range(9):
        churn_rate_task.process_client(client, global_context, client_context)

    for metric in ("churned_users_w7d_a7d_abs", "churned_users_w7d_a7d_rel",
                   "churned_users_w7d_a35d_abs", "churned_users_w7d_a35d_rel"):
        assert metrics_dao.get_latest_point_timestamp(client, metric, None, base=metrics_base) is not None
        latest_value = metrics_dao.get_latest_point_value(client, metric, None, base=metrics_base,
                                                          no_later_than_bases_num=None)
        assert latest_value is not None
        assert abs(latest_value) < 0.001

    for i in range(9):
        churn_rate_task.process_client(client, global_context, client_context)

    assert metrics_dao.get_latest_point_timestamp(client, "churned_users_w7d_a7d_abs", None,
                                                  base=metrics_base) is not None

    ts_from = metrics_base * (int(datetime_now.timestamp()) // metrics_base)
    ts_until = ts_from + metrics_base

    churned_users = list(metrics_dao.get_metrics(client, "churned_users_w7d_a7d_abs", None, base=metrics_base,
                                                 timestamp_from=ts_from, timestamp_until=ts_until))

    assert churned_users
    assert len(churned_users) == 1
    assert abs(churned_users[0][1] - 2.0) < 0.001

    churned_users = list(metrics_dao.get_metrics(client, "churned_users_w7d_a7d_rel", None, base=metrics_base,
                                                 timestamp_from=ts_from, timestamp_until=ts_until))

    assert churned_users
    assert len(churned_users) == 1
    assert abs(churned_users[0][1] - 0.5) < 0.001
