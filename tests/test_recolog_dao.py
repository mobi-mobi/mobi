import pytest

from datetime import datetime, timedelta
from random import randrange, shuffle
from typing import Generator

from mobi.config.system.api.event_api import get_event_api_config
from mobi.core.models import AttributionType, Recommendation, RecommendationSource, RecommendationType
from mobi.dao import get_recolog_dao
from mobi.dao.recolog import MobiRecologDao
from mobi.utils.tests import random_attributed_event, random_recommendation, random_string


@pytest.fixture(scope="session")
def recolog_dao() -> Generator[MobiRecologDao, None, None]:
    yield get_recolog_dao(get_event_api_config())


@pytest.fixture(scope="session")
def client() -> str:
    yield "testclient"


def test_get_dao(recolog_dao):
    pass


def test_read_events(recolog_dao, client):
    for _ in recolog_dao.read_recommendations(client):
        return


def test_no_future_logs(recolog_dao, client):
    assert not len([_ for _ in recolog_dao.read_recommendations(client, datetime.utcnow() + timedelta(seconds=1))])


def test_create_empty_recolog(recolog_dao, client):
    recolog_dao.delete_all_recommendations(client)
    assert not len(list(recolog_dao.read_recommendations(client)))

    recolog_dao.log_recommendation(client, Recommendation(
        user_id=random_string(),
        date=datetime.utcnow(),
        type=RecommendationType.UNKNOWN,
        source=RecommendationSource.UNKNOWN,
        product_ids=[]
    ))

    assert len(list(recolog_dao.read_recommendations(client))) == 1


def test_create_read_recommendation(recolog_dao, client):
    recolog_dao.delete_all_recommendations(client)
    assert not len(list(recolog_dao.read_recommendations(client)))

    recommendation = Recommendation(
        user_id=random_string(20),
        date=datetime.utcnow() - timedelta(minutes=1),
        type=RecommendationType.SIMILAR_PRODUCTS,
        source=RecommendationSource.MODEL,
        product_ids=[random_string(), random_string()],
        source_product_id=random_string(),
        zone_id=random_string(),
        campaign_id=random_string()
    )

    recolog_dao.log_recommendation(client, recommendation)

    found = False

    recommendations = list(recolog_dao.read_recommendations(client))

    assert len(recommendations) == 1

    for read_reco in recommendations:
        if read_reco.user_id == recommendation.user_id:
            if found:
                assert False, "It's the second time we find this user"
            found = True
            assert read_reco.date.isoformat()[0:-3] == recommendation.date.isoformat()[0:-3]
            assert read_reco.type == recommendation.type
            assert read_reco.source == recommendation.source
            assert read_reco.product_ids == recommendation.product_ids
            assert read_reco.source_product_id == recommendation.source_product_id
            assert read_reco.zone_id == recommendation.zone_id
            assert read_reco.campaign_id == recommendation.campaign_id

    if not found:
        assert False, "Reco log not found"


def test_ordered_read(recolog_dao, client):
    recolog_dao.delete_all_recommendations(client)
    assert not len(list(recolog_dao.read_recommendations(client)))

    recommendations = []

    for _ in range(10):
        user_id = random_string()

        for __ in range(10):
            date = datetime.utcnow() - timedelta(minutes=randrange(0, 60), seconds=randrange(0, 60))
            recommendations.append(random_recommendation(user_id=user_id, date=date))

    shuffle(recommendations)

    for recommendation in recommendations:
        recolog_dao.log_recommendation(client, recommendation)

    read_recommendations = list(recolog_dao.read_ordered_recommendations(client))

    for reco_1, reco_2 in zip(read_recommendations, read_recommendations[1:]):
        assert reco_1.user_id <= reco_2.user_id
        if reco_1.user_id == reco_2.user_id:
            assert reco_1.date <= reco_2.date

    recolog_dao.delete_all_recommendations(client)


def test_create_read_attribution_event(client, recolog_dao):
    recolog_dao.delete_all_attributed_events(client)

    attributed_event = random_attributed_event(attribution_type=AttributionType.EMPIRICAL)
    recolog_dao.log_attributed_event(client, attributed_event)

    all_attributed_events = list(recolog_dao.read_attributed_events(client, AttributionType.EMPIRICAL))
    assert all_attributed_events
    assert len(all_attributed_events) == 1
    assert all_attributed_events[0] == attributed_event


def test_create_unmatched_recommendation(client, recolog_dao):
    recolog_dao.delete_all_unmatched_recommendations(client)

    assert not list(recolog_dao.read_unmatched_recommendations(client, AttributionType.EMPIRICAL))

    recommendation = random_recommendation(date=datetime.utcnow())
    recolog_dao.log_unmatched_recommendation(client, AttributionType.EMPIRICAL, recommendation)

    assert len(list(recolog_dao.read_unmatched_recommendations(client, AttributionType.EMPIRICAL))) == 1
    assert list(recolog_dao.read_unmatched_recommendations(client, AttributionType.EMPIRICAL))[0] == recommendation

    new_recommendation = random_recommendation(user_id=recommendation.user_id,
                                               date=recommendation.date - timedelta(hours=10))
    recolog_dao.log_unmatched_recommendation(client, AttributionType.EMPIRICAL, new_recommendation)

    all_unmatched_recommendations = list(recolog_dao.read_ordered_unmatched_recommendations(client,
                                                                                            AttributionType.EMPIRICAL))

    assert len(all_unmatched_recommendations) == 2
    assert all_unmatched_recommendations[0] == new_recommendation
    assert all_unmatched_recommendations[1] == recommendation
