import pytest

from collections import Counter
from datetime import datetime
from math import sqrt

from mobi.core.models import Event, EventPlatform, EventType
from mobi.core.population import Population
from mobi.dao.cache import UserHistoryCookie
from mobi.ml.model.embedding.impl.static import StaticEmbeddingModel
from mobi.utils.tests import random_product, random_string

PRODUCTS = [
    random_product("product_1"),
    random_product("product_2"),
    random_product("product_3")
]

PRODUCTS[0].brand = "red"
PRODUCTS[1].brand = "green"
PRODUCTS[2].brand = "red"


@pytest.fixture(scope="function")
def model() -> StaticEmbeddingModel:
    global PRODUCTS

    model_data = [
        (PRODUCTS[0].product_id, [1., 1.]),
        (PRODUCTS[1].product_id, [2., 2.]),
        (PRODUCTS[2].product_id, [3., 3.])
    ]

    model = StaticEmbeddingModel()
    model.set_product_id_embeddings(model_data)
    model.compile(PRODUCTS)

    yield model


def test_wrong_data():
    model = StaticEmbeddingModel()

    with pytest.raises(ValueError):
        model.compile([random_product()])

    with pytest.raises(ValueError):
        model.set_product_id_embeddings([])

    with pytest.raises(ValueError):
        model.set_product_id_embeddings([("1", [])])

    with pytest.raises(ValueError):
        model.set_product_id_embeddings([("1", [1.]), ("2", [2., 2.])])


def test_similar_products(model):
    result = model.similar_products(PRODUCTS[0], k=1)
    assert len(result) == 1
    assert result[0] == "product_2"

    result = model.similar_products(PRODUCTS[0], k=2)
    assert len(result) == 2
    assert result[0] == "product_2"
    assert result[1] == "product_3"

    result = model.similar_products(PRODUCTS[0], brands=["red"], k=1)
    assert len(result) == 1
    assert result[0] == "product_3"

    result = model.similar_products(PRODUCTS[0], brands=["red"], k=2)
    assert len(result) == 1
    assert result[0] == "product_3"


def test_personal_recommendations(model):
    events = [
        Event(
            user_id="test_user",
            user_population=Population.UNKNOWN,
            product_id="product_3",
            product_version=0,
            event_type=EventType.UNKNOWN,
            event_platform=EventPlatform.OTHER,
            date=datetime.utcnow()
        )
    ]

    result = model.personal_recommendations(user_session=events, k=3)
    assert len(result) == 3
    assert result[0] == "product_3"
    assert result[1] == "product_2"
    assert result[2] == "product_1"

    result = model.personal_recommendations(user_session=events, brands=["red"], k=3)
    assert len(result) == 2
    assert result[0] == "product_3"
    assert result[1] == "product_1"

    result = model.personal_recommendations(user_session=events, brands=["red", "green"], k=3)
    assert len(result) == 3
    assert result[0] == "product_3"
    assert result[1] == "product_2"
    assert result[2] == "product_1"

    result = model.personal_recommendations(user_session=events, brands=["green"], k=3)
    assert len(result) == 1
    assert result[0] == "product_2"

    result = model.personal_recommendations(user_session=events, brands=["non_existing", "green"], k=3)
    assert len(result) == 1
    assert result[0] == "product_2"

    result = model.personal_recommendations(user_session=events, brands=["non_existing"], k=3)
    assert len(result) == 0


def test_score_products(model):
    global PRODUCTS
    eps = 0.1 ** 6

    events = [
        Event(
            user_id="test_user",
            user_population=Population.UNKNOWN,
            product_id="product_3",
            product_version=0,
            event_type=EventType.UNKNOWN,
            event_platform=EventPlatform.OTHER,
            date=datetime.utcnow()
        )
    ]

    result = model.rank_products_by_user_history([PRODUCTS[2]], UserHistoryCookie(events))
    assert len(result) == 1
    assert result[0] is not None
    assert abs(result[0]) < eps

    result = model.rank_products_by_user_history([PRODUCTS[2], PRODUCTS[1], PRODUCTS[0]], UserHistoryCookie(events))
    assert len(result) == 3
    assert result[0] is not None
    assert result[1] is not None
    assert result[2] is not None
    # assert abs(result[0]) < eps
    # assert abs(result[1] - sqrt(2)) < eps
    # assert abs(result[2] - sqrt(8)) < eps

    events.append(
        Event(
            user_id="test_user",
            user_population=Population.UNKNOWN,
            product_id="product_1",
            product_version=0,
            event_type=EventType.UNKNOWN,
            event_platform=EventPlatform.OTHER,
            date=datetime.utcnow()
        )
    )

    events.append(
        Event(
            user_id="test_user",
            user_population=Population.UNKNOWN,
            product_id="product_1",
            product_version=0,
            event_type=EventType.UNKNOWN,
            event_platform=EventPlatform.OTHER,
            date=datetime.utcnow()
        )
    )

    result = model.rank_products_by_user_history([PRODUCTS[2], PRODUCTS[1], PRODUCTS[0]], UserHistoryCookie(events))
    assert len(result) == 3
    assert result[0] is not None
    assert result[1] is not None
    assert result[2] is not None
    # assert abs(result[0] - 1.8856180831641) < eps
    # assert abs(result[1] - 0.4714045207908) < eps
    # assert abs(result[2] - 0.9428090415820) < eps


def test_basket_recommendations(model):
    product_1_id = random_string()
    product_2_id = random_string()
    product_3_id = random_string()

    assert not len(model.basket_recommendations([product_1_id]))

    model.set_basket_links(
        {
            product_1_id: Counter({product_2_id: 1}),
            product_2_id: Counter({product_1_id: 100, product_3_id: 1}),
            product_3_id: Counter({product_1_id: 1, product_2_id: 2})
        }
    )

    recommendations = model.basket_recommendations([product_1_id])

    assert len(recommendations) == 1
    assert recommendations[0] == product_2_id

    recommendations = model.basket_recommendations([product_1_id, product_1_id])

    assert len(recommendations) == 1
    assert recommendations[0] == product_2_id

    recommendations = model.basket_recommendations([product_2_id, product_3_id])

    assert len(recommendations) == 1
    assert recommendations[0] == product_1_id

    model.set_non_recommendable_product_ids([product_1_id])

    recommendations = model.basket_recommendations([product_2_id, product_3_id])

    assert len(recommendations) == 0
