import pytest

from flask import testing

from mobi.api.metrics_api import MetricsApi
from mobi.config.system.api.metrics_api import get_metrics_api_config
from mobi.dao import get_metrics_dao


class TestMetricsApiClient(testing.FlaskClient):
    MOBI_TOKEN = "testtoken"

    def open(self, *args, **kwargs):
        if self.MOBI_TOKEN is not None:
            headers = kwargs.get('headers', {})
            headers['Auth-Token'] = self.MOBI_TOKEN
            kwargs['headers'] = headers
        return super().open(*args, **kwargs)


class TestMetricsApiClientWithHashedToken(TestMetricsApiClient):
    MOBI_TOKEN = "testtokenhashed"


class TestMetricsApiClientWithNoToken(TestMetricsApiClient):
    MOBI_TOKEN = None


class TestMetricsApiClientWithExpiredToken(TestMetricsApiClient):
    MOBI_TOKEN = "expiredtesttoken"


class TestMetricsApiClientWithWrongToken(TestMetricsApiClient):
    MOBI_TOKEN = "__testnonexistingtoken__"


class TestMetricsApiClientWithProdToken(TestMetricsApiClient):
    MOBI_TOKEN = "testtokenprod"


@pytest.fixture(scope="session")
def client() -> str:
    return "testclient"


@pytest.fixture(scope="session")
def metrics_api_client():
    api = MetricsApi()
    api.app.test_client_class = TestMetricsApiClient

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_expired_token():
    api = MetricsApi()
    api.app.test_client_class = TestMetricsApiClientWithExpiredToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_hashed_token():
    api = MetricsApi()
    api.app.test_client_class = TestMetricsApiClientWithHashedToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_no_token():
    api = MetricsApi()
    api.app.test_client_class = TestMetricsApiClientWithNoToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_wrong_token():
    api = MetricsApi()
    api.app.test_client_class = TestMetricsApiClientWithWrongToken

    client = api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def metrics_dao():
    return get_metrics_dao(get_metrics_api_config())


def test_metrics_api_init(metrics_api_client):
    pass


def test_metrics_api_status(metrics_api_client):
    rv = metrics_api_client.get("/status")
    assert rv.data == b'{"status": "OK"}'


def test_get_random_metric(metrics_api_client):
    rv = metrics_api_client.get("/abtest.bound.lower.metric.session_length.period.daily.popA.target.popB.reference"
                                "?base=D&points=22&hr=on&from=-21D")
    assert b'"status": "OK"' in rv.data
