import pytest

from mobi.core.common.mobienum import MobiEnum


class TestEnum(MobiEnum):
    A_KEY = 0
    B_KEY = 1


class TestEnumWithUnknown(MobiEnum):
    A_KEY = 0
    B_KEY = 1
    UNKNOWN = 2

    @classmethod
    def _unknown_value(cls):
        return TestEnumWithUnknown.UNKNOWN


def test_enum():
    assert TestEnum.parse(0) == TestEnum.A_KEY
    assert TestEnum.parse(1) == TestEnum.B_KEY

    assert TestEnum.parse("0") == TestEnum.A_KEY
    assert TestEnum.parse("1") == TestEnum.B_KEY

    assert TestEnum.parse("A_KEY") == TestEnum.A_KEY
    assert TestEnum.parse("B_KEY") == TestEnum.B_KEY

    assert TestEnum.parse("a_key") == TestEnum.A_KEY
    assert TestEnum.parse("b_key") == TestEnum.B_KEY

    with pytest.raises(ValueError):
        assert TestEnum.parse("unknown_key") == TestEnum.B_KEY


def test_enum_with_unknown():
    assert TestEnumWithUnknown.parse("unknown_key") == TestEnumWithUnknown.UNKNOWN
