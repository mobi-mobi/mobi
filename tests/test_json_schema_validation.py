import pytest

from functools import partial

from mobi.exceptions import MobiSchemaValidationException
from mobi.api.parsers.schemas import PARTIAL_PRODUCT_SCHEMA
from mobi.api.parsers.model import validate_event_json, validate_product_json, validate_audience_json, \
    validate_audiences_union_json, validate_audience_split_tree_json
from mobi.utils.tests import random_string


validate_partial_product_json = partial(validate_product_json, product_schema=PARTIAL_PRODUCT_SCHEMA)


def test_product_missing_fields():
    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({"product_id": "1"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({"product_id": "1", "title": "title"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({"product_id": "1", "title": "title", "brand": "brand"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({"product_id": "1", "title": "title", "brand": "brand", "price": "11.22"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({"product_id": "1", "title": "title", "brand": "brand", "price": "11.22",
                               "__unknown_field__": "1"})

    validate_product_json({"product_id": "1", "title": "title", "brand": "brand",
                           "price": "11.22", "currency": "USD"})


def test_partial_product_missing_fields():
    with pytest.raises(MobiSchemaValidationException):
        validate_partial_product_json({})

    with pytest.raises(MobiSchemaValidationException):
        validate_partial_product_json({"product_id": "1", "__unknown_field__": "1"})

    validate_partial_product_json({"product_id": "1"})


def test_product_bad_values():
    product = {"product_id": "1", "title": "title", "brand": "brand", "price": "11.22", "currency": "USD"}

    # Testing simple string parameter

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "color": 1})

    validate_product_json({**product, "color": random_string(200)})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "color": random_string(201)})

    # Testing array of strings

    validate_product_json({**product, "categories": []})
    validate_product_json({**product, "categories": ["1"]})
    validate_product_json({**product, "categories": [str(i) for i in range(10)]})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "categories": [str(i) for i in range(11)]})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "categories": ["1", 1]})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "categories": ["1", None]})

    # Testing currency

    validate_product_json({**product, "price": "1.11"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "price": None})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "price": "1,11"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "price": "1.1"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "price": "1.123"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "price": "a1.12"})

    # Testing currency

    validate_product_json({**product, "currency": "EUR"})
    validate_product_json({**product, "currency": "USD"})
    validate_product_json({**product, "currency": "RUB"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "currency": "ABC"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "currency": "abc"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "currency": "ABCD"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "currency": "AB"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "currency": "AB9"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "currency": "1AB"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "currency": None})


def test_product_dates():
    product = {"product_id": "1", "title": "title", "brand": "brand", "price": "11.22", "currency": "USD"}

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "available_from": 1})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "available_from": "2021-05-112 14:12:42"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "available_from": "2021-05-11  14:12:42"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "available_from": "2021-13-12 14:12:42"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "available_from": "2021-11-31 14:12:42"})

    with pytest.raises(MobiSchemaValidationException):
        validate_product_json({**product, "available_from": "2021-11-11 25:12:42"})

    validate_product_json({**product, "available_from": "2021-12-12 14:12:42"})


def test_event_missing_fields():
    with pytest.raises(MobiSchemaValidationException):
        validate_event_json({})

    with pytest.raises(MobiSchemaValidationException):
        validate_event_json({"user_id": "user_id"})

    with pytest.raises(MobiSchemaValidationException):
        validate_event_json({"user_id": "user_id", "product_id": "product_id"})

    with pytest.raises(MobiSchemaValidationException):
        validate_event_json({"user_id": "user_id", "product_id": "product_id", "__unknown_field__": "1"})

    with pytest.raises(MobiSchemaValidationException):
        validate_event_json({"user_id": "user_id", "product_id": "product_id", "event_type": "unexisting_event_type"})

    with pytest.raises(MobiSchemaValidationException):
        validate_event_json({"user_id": "user_id", "product_id": "product_id", "event_type": "product_view",
                             "event_platform": "unexisting_event_platform"})

    validate_event_json({"user_id": "user_id", "product_id": "product_id", "event_type": "product_view",
                         "event_platform": "android"})


def test_event_bad_values():
    event = {"user_id": "user_id", "product_id": "product_id", "event_type": "product_view",
             "event_platform": "android"}

    # Testing simple string parameter

    with pytest.raises(MobiSchemaValidationException):
        validate_event_json({**event, "user_id": 1})

    validate_event_json({**event, "user_id": random_string(100)})

    with pytest.raises(MobiSchemaValidationException):
        validate_event_json({**event, "user_id": random_string(101)})


def test_audience_parsing():
    audience = {}

    with pytest.raises(MobiSchemaValidationException):
        validate_audience_json(audience)

    audience = {
        "audience_id": random_string(),
    }

    with pytest.raises(MobiSchemaValidationException):
        validate_audience_json(audience)

    audience = {
        "audience_id": random_string(),
        "audience_name": random_string()
    }

    with pytest.raises(MobiSchemaValidationException):
        validate_audience_json(audience)

    audience = {
        "audience_id": random_string(),
        "audience_name": random_string(),
        "audience_description": random_string()
    }

    validate_audience_json(audience)

    audience = {
        "audience_id": 42,
        "audience_name": random_string(),
        "audience_description": random_string()
    }

    with pytest.raises(MobiSchemaValidationException):
        validate_audience_json(audience)


def test_audiences_union_parsing():
    audiences_union = {}

    with pytest.raises(MobiSchemaValidationException):
        validate_audiences_union_json(audiences_union)
