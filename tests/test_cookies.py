import pytest

from datetime import datetime, timedelta

from mobi.config.system.api import get_recommendation_api_config
from mobi.core.models import Event, EventPlatform, EventType
from mobi.dao.cache import MobiCache, UserHistoryCookie
from mobi.dao.factory import get_cache_dao
from mobi.utils.tests import random_string


@pytest.fixture(scope="session")
def cache() -> MobiCache:
    return get_cache_dao(get_recommendation_api_config())


def test_user_history(cache: MobiCache):
    client_id = random_string()
    user_id = random_string()

    history = cache.get_user_history(client_id, user_id)
    assert history is None

    history = UserHistoryCookie()
    cache.set_user_history(client_id, user_id, history, noreply=False)

    history = cache.get_user_history(client_id, user_id)
    assert history is not None
    assert type(history.events) == list
    assert len(history.events) == 0

    event = Event(
        user_id=user_id,
        user_population=1,
        product_id="product",
        product_version=0,
        event_type=EventType.PRODUCT_VIEW,
        event_platform=EventPlatform.OTHER,
        date=datetime.utcnow()
    )

    history.append(event)
    cache.set_user_history(client_id, user_id, history, noreply=False)

    history = cache.get_user_history(client_id, user_id)
    assert history is not None
    assert type(history.events) == list
    assert len(history.events) == 1
    assert history.events[0] == event

    for i in range(UserHistoryCookie.EVENTS_LIMIT + 10):
        history.append(
            Event(
                user_id=user_id,
                user_population=1,
                product_id=f"product_{i}",
                product_version=0,
                event_type=EventType.PRODUCT_VIEW,
                event_platform=EventPlatform.OTHER,
                date=datetime.utcnow() + timedelta(hours=i)
            )
        )

    assert len(history.events) == UserHistoryCookie.EVENTS_LIMIT
    cache.set_user_history(client_id, user_id, history, noreply=False)

    history = cache.get_user_history(client_id, user_id)
    assert type(history.events) == list
    assert len(history.events) == UserHistoryCookie.EVENTS_LIMIT

    products = [e.product_id for e in history.events]

    for i in range(10):
        assert f"product_{i}" not in products

    history.events = []

    for i in range(UserHistoryCookie.EVENTS_LIMIT + 10):
        history.append(
            Event(
                user_id=user_id,
                user_population=1,
                product_id=f"product_{i}",
                product_version=0,
                event_type=EventType.PRODUCT_VIEW,
                event_platform=EventPlatform.OTHER,
                date=datetime.utcnow() - timedelta(days=30, hours=i)
            )
        )

    assert len(history.events) == 0
    assert len(history) == 0

    cache.set_user_history(client_id, user_id, history, noreply=False)
    history = cache.get_user_history(client_id, user_id)

    assert history is not None
    assert type(history.events) == list
    assert len(history.events) == 0
