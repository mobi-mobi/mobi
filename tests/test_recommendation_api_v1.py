import json
import pytest
import werkzeug

from datetime import datetime, timedelta
from flask import testing
from typing import List, Tuple

from mobi.api.recommendation_api import RecommendationApi
from mobi.config.client import get_client_config
from mobi.config.system.api.recommendation_api import get_recommendation_api_config
from mobi.core.models import Event, EventPlatform, EventType, ModelStatus, Product, RecommendationSource, RecommendationType
from mobi.core.models.zone import CampaignStatus, ZoneSettings, ZoneType, ZoneExtraProductsPosition
from mobi.core.population import Population
from mobi.dao import get_cache_dao, get_catalog_dao, get_event_dao, get_model_dao, get_recolog_dao, get_search_dao,\
    get_settings_dao
from mobi.dao.cache import MobiCache
from mobi.dao.catalog import MobiCatalogDao
from mobi.dao.cache import UserHistoryCookie
from mobi.dao.event import MobiEventDao
from mobi.dao.object import MobiModelDao
from mobi.dao.recolog import MobiRecologDao
from mobi.dao.search import MobiSearchDao
from mobi.dao.settings import MobiSettingsDao
from mobi.ml.model.embedding.impl.static import StaticEmbeddingModel
from mobi.ml.model.embedding.impl.factories.deepwalk import build_basket_links
from mobi.service.task.impl.cache_builder_task import CacheBuilderTask
from mobi.utils.tests import random_campaign, random_event, random_product, random_string, random_zone


class TestRecommendationApiClient(testing.FlaskClient):
    MOBI_TOKEN = "testtoken"

    def open(self, *args, **kwargs):
        if self.MOBI_TOKEN is not None:
            headers = kwargs.get('headers', {})
            headers['Auth-Token'] = self.MOBI_TOKEN
            kwargs['headers'] = headers
        return super().open(*args, **kwargs)


class TestRecommendationApiClientWithNoToken(TestRecommendationApiClient):
    MOBI_TOKEN = None


class TestRecommendationApiClientWithExpiredToken(TestRecommendationApiClient):
    MOBI_TOKEN = "expiredtesttoken"


class TestRecommendationApiClientWithWrongToken(TestRecommendationApiClient):
    MOBI_TOKEN = "__testnonexistingtoken__"


@pytest.fixture(scope="session")
def client() -> str:
    yield "testclient"


@pytest.fixture(scope="session")
def recommendation_api() -> RecommendationApi:
    return RecommendationApi()


@pytest.fixture(scope="session")
def recommendation_api_client(recommendation_api):
    recommendation_api.app.test_client_class = TestRecommendationApiClient

    client = recommendation_api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_expired_token(recommendation_api):
    recommendation_api.app.test_client_class = TestRecommendationApiClientWithExpiredToken
    client = recommendation_api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_no_token(recommendation_api):
    recommendation_api.app.test_client_class = TestRecommendationApiClientWithNoToken
    client = recommendation_api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def client_with_wrong_token(recommendation_api):
    recommendation_api.app.test_client_class = TestRecommendationApiClientWithWrongToken

    client = recommendation_api.app.test_client()

    yield client


@pytest.fixture(scope="session")
def catalog_dao() -> MobiCatalogDao:
    yield get_catalog_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def cache_dao() -> MobiCache:
    yield get_cache_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def event_dao() -> MobiEventDao:
    yield get_event_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def models_dao() -> MobiModelDao:
    yield get_model_dao(get_recommendation_api_config())


@pytest.fixture(scope="session")
def recolog_dao() -> MobiRecologDao:
    yield get_recolog_dao(get_recommendation_api_config())


@pytest.fixture
def search_dao(client) -> MobiSearchDao:
    dao = get_search_dao(get_recommendation_api_config())
    dao.delete_index(client)
    dao.create_index(client)
    yield dao
    dao.delete_index(client)


@pytest.fixture(scope="session")
def settings_dao() -> MobiSettingsDao:
    yield get_settings_dao(get_recommendation_api_config())


@pytest.fixture
def random_non_existing_product(catalog_dao, client) -> Product:
    product = random_product()
    try:
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass

    yield product

    try:
        catalog_dao.delete_product(client, product.product_id)
    except Exception:
        pass


@pytest.fixture
def random_existing_product(catalog_dao, client, random_non_existing_product) -> Product:
    catalog_dao.create_product(client, random_non_existing_product)

    yield random_non_existing_product

    try:
        catalog_dao.delete_product(client, random_non_existing_product.product_id)
    except Exception:
        pass


def get_random_user_id_with_population(client: str, population: Population, iterations: int = 500):
    population_setup = get_client_config(client).population_setup()
    for _ in range(iterations):
        user_id = random_string()
        if population_setup.population(user_id) == population:
            return user_id
    raise Exception(f"Can not find proper user id for population {population.name}")


@pytest.fixture
def reference_random_user_id(client):
    yield get_random_user_id_with_population(client, Population.REFERENCE)


@pytest.fixture
def target_random_user_id(client):
    yield get_random_user_id_with_population(client, Population.TARGET)


@pytest.fixture
def static_embedding_model_and_products(event_dao, recommendation_api, client, catalog_dao, models_dao) \
        -> Tuple[StaticEmbeddingModel, List[Product]]:
    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)
    models_dao.delete_all_model_dumps(client)

    product_1 = random_product()
    product_1.brand = "brand_1"
    catalog_dao.create_product(client, product_1)
    product_2 = random_product()
    product_2.brand = "brand_2"
    catalog_dao.create_product(client, product_2)
    product_3 = random_product()
    product_3.brand = "brand_1"
    catalog_dao.create_product(client, product_3)
    product_4 = random_product()
    product_4.brand = "brand_4"
    catalog_dao.create_product(client, product_4)
    product_5 = random_product()
    product_5.brand = "brand_5"
    catalog_dao.create_product(client, product_5)

    user_id = random_string()
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_2.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)

    user_id = random_string()
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_2.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_2.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)

    user_id = random_string()
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_1.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_3.product_id, event_type=EventType.PRODUCT_VIEW,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)
    event = random_event(user_id=user_id, product_id=product_3.product_id, event_type=EventType.PRODUCT_TO_BASKET,
                         date=datetime.utcnow() - timedelta(minutes=1))
    event_dao.create_event(client, event)

    model = StaticEmbeddingModel()
    non_recommendable_products = [random_string() + "#ne" for _ in range(1000)]
    non_recommendable_products.append(product_4.product_id)
    model.set_non_recommendable_product_ids(non_recommendable_products)
    non_recommendable_brands = [random_string() + "#ne" for _ in range(100)]
    non_recommendable_brands.append(product_5.brand)
    model.set_non_recommendable_brands(non_recommendable_brands)
    model.status = ModelStatus.READY
    model.set_product_id_embeddings([(product.product_id, [float(i)])
                                     for i, product in enumerate([product_1, product_2, product_3,
                                                                  product_4, product_5])])
    model.compile([product_1, product_2, product_3, product_4, product_5])
    model.set_basket_links(build_basket_links(
        event_dao,
        client,
        from_date=datetime.utcnow() - timedelta(hours=1),
        till_date=datetime.utcnow()
    ))

    models_dao.save_model_dump(client, model.dump())
    recommendation_api.models_updated_datetime[client] = datetime.utcnow() - timedelta(days=1)

    yield model, [product_1, product_2, product_3]

    catalog_dao.delete_all_products(client)
    event_dao.delete_all_events(client)
    models_dao.delete_all_model_dumps(client)


@pytest.fixture
def static_embedding_model(static_embedding_model_and_products) -> StaticEmbeddingModel:
    model, _ = static_embedding_model_and_products
    yield model


def test_recommendation_api_init(client):
    pass


def test_status(recommendation_api_client):
    rv = recommendation_api_client.get("/status")
    assert rv.data == b'{"status": "OK"}'


def test_no_tokens(client_with_no_token):
    rv = client_with_no_token.get("/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 401' in rv.data
    assert b'No token provided' in rv.data
    assert b'401 Unauthorized' in rv.data


def test_wrong_token(client_with_wrong_token):
    rv = client_with_wrong_token.get("/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data
    assert b'403 Forbidden' in rv.data


def test_wrong_token_in_uri(client_with_no_token):
    rv = client_with_no_token.get("/some/url?token=__testnonexistingtoken__")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data
    assert b'403 Forbidden' in rv.data


def test_ok_token_in_header_but_wrong_token_in_uri(recommendation_api_client):
    rv = recommendation_api_client.get("/some/url?token=__testnonexistingtoken__")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is wrong' in rv.data
    assert b'403 Forbidden' in rv.data


def test_with_expired_token(client_with_expired_token):
    rv = client_with_expired_token.get("/some/url")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is expired' in rv.data
    assert b'403 Forbidden' in rv.data


def test_with_expired_token_in_uri(recommendation_api_client):
    rv = recommendation_api_client.get("/some/url?token=expiredtesttoken")
    assert b'"status": "ERROR"' in rv.data
    assert b'"code": 403' in rv.data
    assert b'Token is expired' in rv.data
    assert b'403 Forbidden' in rv.data


def test_wrong_url(recommendation_api_client, static_embedding_model):
    rv = recommendation_api_client.get("/somewrongurl")
    assert b'"status": "ERROR"' in rv.data
    assert b'The requested URL was not found on the server' in rv.data


def test_get_model_meta(recommendation_api_client, static_embedding_model):
    rv = recommendation_api_client.get("/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' in rv.data
    assert b'"model_type": "' + bytes(static_embedding_model.model_type, encoding="utf-8") + b'"' in rv.data
    assert b'"status": "ready"' in rv.data
    assert b'"product_id_embeddings_map_size": 5' in rv.data
    assert b'"basket_links_size": 3' in rv.data
    assert b'"knn_tree_size": 3' in rv.data
    assert b'"brand_isolated_knn_tree_size": 3' in rv.data
    assert b'"non_recommendable_products": 1001' in rv.data
    assert b'"non_recommendable_brands": 101' in rv.data


def test_model_reload_after_2h_1s(recommendation_api, recommendation_api_client, client, catalog_dao,
                                  models_dao, static_embedding_model):
    rv = recommendation_api_client.get("/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' in rv.data
    old_update_time = recommendation_api.models_updated_datetime[client]

    recommendation_api.models_updated_datetime[client] = \
        datetime.utcnow() - timedelta(hours=2, seconds=1)

    new_model = StaticEmbeddingModel()
    new_model.status = ModelStatus.READY
    new_product = random_product()
    catalog_dao.create_product(client, new_product)
    new_model.set_product_id_embeddings([(new_product.product_id, [0.0])])

    models_dao.save_model_dump(client, new_model.dump())

    rv = recommendation_api_client.get("/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(new_model.model_id, encoding="utf-8") + b'"' in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' not in rv.data
    recommendation_api.models_updated_datetime[client] = old_update_time
    recommendation_api.models[client] = static_embedding_model


def test_model_reload_after_1h(recommendation_api, recommendation_api_client, client,  catalog_dao,
                               models_dao, static_embedding_model):
    rv = recommendation_api_client.get("/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' in rv.data
    old_update_time = recommendation_api.models_updated_datetime[client]

    recommendation_api.models_updated_datetime[client] = \
        datetime.utcnow() - timedelta(hours=1)

    new_model = StaticEmbeddingModel()
    new_model.status = ModelStatus.READY
    new_product = random_product()
    catalog_dao.create_product(client, new_product)
    new_model.set_product_id_embeddings([(new_product.product_id, [0.0])])

    models_dao.save_model_dump(client, new_model.dump())

    rv = recommendation_api_client.get("/model")
    assert b'"status": "OK"' in rv.data
    assert b'"model_id": "' + bytes(new_model.model_id, encoding="utf-8") + b'"' not in rv.data
    assert b'"model_id": "' + bytes(static_embedding_model.model_id, encoding="utf-8") + b'"' in rv.data
    recommendation_api.models_updated_datetime[client] = old_update_time
    recommendation_api.models[client] = static_embedding_model


def test_reference_population_is_denied(recommendation_api_client, reference_random_user_id):
    rv = recommendation_api_client.get(f"/user/{reference_random_user_id}/product/testproduct/similar")
    assert b'"status": "DENIED"' in rv.data

    rv = recommendation_api_client.get(f"/user/{reference_random_user_id}/recommendations")
    assert b'"status": "DENIED"' in rv.data


def test_recommendations(event_dao, recolog_dao,  client, recommendation_api_client,
                         static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()
    cache_builder.process_client(client, global_context, client_context)

    # Testing bad data
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?num=-1")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?num=0")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?num=21")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?num=num")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(11))
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?brands={brands_params}")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(10))
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?brands={brands_params}")
    assert b'"status": "ERROR"' not in rv.data

    # Testing "most_popular" source when user has no browsing history

    recolog_dao.delete_all_recommendations(client)
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    # assert meta["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["id"], data[1]["id"], data[2]["id"]])

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 3
    # assert reco.product_ids == [product_1.product_id, product_2.product_id, product_3.product_id]
    # assert reco.source == RecommendationSource.MOST_POPULAR_PRODUCTS
    assert reco.type == RecommendationType.PERSONAL_RECOMMENDATIONS
    assert reco.source_product_id is None

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}"
                                       f"/recommendations?brands=brand_1&brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    # assert meta["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["id"], data[1]["id"], data[2]["id"]])

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}"
                                       f"/recommendations?brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    # assert meta["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["id"] == product_2.product_id

    # Testing non-recommendable product (by brand)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}"
                                       f"/recommendations?brands=brand_4&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    # assert meta["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 0

    # Testing non-recommendable brand

    recolog_dao.delete_all_recommendations(client)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}"
                                       f"/recommendations?brands=brand_5&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    # assert meta["source"] == "most_popular"
    data = data["data"]
    assert len(data) == 0

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 0
    # assert reco.source == RecommendationSource.MOST_POPULAR_PRODUCTS
    assert reco.type == RecommendationType.PERSONAL_RECOMMENDATIONS
    assert reco.source_product_id is None

    # Testing "model" source when user has a browsing history

    recolog_dao.delete_all_recommendations(client)
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_1.product_id
    ))

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["id"], data[1]["id"], data[2]["id"]])

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 3
    assert sorted(reco.product_ids) == sorted([product_1.product_id, product_2.product_id, product_3.product_id])
    assert reco.source == RecommendationSource.MODEL
    assert reco.type == RecommendationType.PERSONAL_RECOMMENDATIONS
    assert reco.source_product_id is None

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?"
                                       f"brands=brand_1&brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["id"], data[1]["id"], data[2]["id"]])

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?"
                                       f"brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["id"] == product_2.product_id

    # Testing another session

    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_3.product_id
    ))

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?"
                                       f"brands=brand_1&brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["id"], data[1]["id"], data[2]["id"]])

    # Testing another session

    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_2.product_id
    ))
    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_2.product_id
    ))
    event_dao.create_event(client, random_event(
        user_id=target_random_user_id,
        event_type=EventType.PRODUCT_VIEW,
        product_id=product_3.product_id
    ))

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/recommendations?"
                                       f"brands=brand_1&brands=brand_2&num=5")
    data = json.loads(rv.data)
    assert data["status"] == "OK"
    meta = data["meta"]
    assert meta["source"] == "model"
    data = data["data"]
    assert len(data) == 3
    assert sorted([product_1.product_id, product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["id"], data[1]["id"], data[2]["id"]])


def test_similar_products(client, recolog_dao, recommendation_api_client, static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    # Testing bad data
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?num=-1")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?num=0")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?num=21")
    assert b'"status": "ERROR"' in rv.data
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?num=num")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(11))
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar"
                                       f"?brands={brands_params}")
    assert b'"status": "ERROR"' in rv.data
    brands_params = "&".join(f"brands={random_string()}" for _ in range(10))
    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar"
                                       f"?brands={brands_params}")
    assert b'"status": "ERROR"' not in rv.data

    recolog_dao.delete_all_recommendations(client)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?"
                                       f"brands=brand_1&brands=brand_2&num=5")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["id"], data[1]["id"]])

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 2
    assert sorted(reco.product_ids) == sorted([product_2.product_id, product_3.product_id])
    assert reco.source == RecommendationSource.MODEL
    assert reco.type == RecommendationType.SIMILAR_PRODUCTS
    assert reco.source_product_id == product_1.product_id

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?"
                                       f"brands=brand_1&num=5")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["id"] == product_3.product_id

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?"
                                       f"same_brand=1&num=5")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 1
    assert data[0]["id"] == product_3.product_id

    recolog_dao.delete_all_recommendations(client)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?"
                                       f"brands=brand_na&num=5")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 0

    assert len(list(recolog_dao.read_recommendations(client))) == 1
    reco = list(recolog_dao.read_recommendations(client))[0]
    assert len(reco.product_ids) == 0
    assert reco.source == RecommendationSource.MODEL
    assert reco.type == RecommendationType.SIMILAR_PRODUCTS
    assert reco.source_product_id == product_1.product_id

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/product/{product_1.product_id}/similar?num=5")

    data = json.loads(rv.data)
    assert data["status"] == "OK"
    data = data["data"]
    assert len(data) == 2
    assert sorted([product_2.product_id, product_3.product_id]) \
           == sorted([data[0]["id"], data[1]["id"]])


def test_basket_recommendations(client, recommendation_api_client, static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    # Test bad data
    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/basket_recommendations")
    assert rv.data == b'{"status": "OK", "data": []}'

    # Test good data

    target_random_user_id = get_random_user_id_with_population(client, Population.TARGET)

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/basket_recommendations"
                                       f"?product_ids={product_1.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 2
    assert data[0]["id"] == product_2.product_id
    assert data[1]["id"] == product_3.product_id

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/basket_recommendations"
                                       f"?product_ids={product_2.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 1
    assert data[0]["id"] == product_1.product_id

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/basket_recommendations"
                                       f"?product_ids={product_3.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 1
    assert data[0]["id"] == product_1.product_id

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/basket_recommendations"
                                       f"?product_ids={product_3.product_id}&product_ids={product_2.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 1
    assert data[0]["id"] == product_1.product_id

    rv = recommendation_api_client.get(f"/user/{target_random_user_id}/basket_recommendations"
                                       f"?product_ids={product_3.product_id}&product_ids={product_2.product_id}"
                                       f"&product_ids={product_1.product_id}")
    assert b'"status": "OK"' in rv.data
    data = json.loads(rv.data)["data"]
    assert len(data) == 0


def test_test_recommendation(client, catalog_dao, settings_dao, event_dao, recommendation_api_client,
                             static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    cache_builder = CacheBuilderTask("Test Cache Builder Task", [client])
    global_context = dict()
    client_context = dict()
    cache_builder.process_client(client, global_context, client_context)

    json_events = [
        {"product_id": product_1.product_id, "event_type": EventType.PRODUCT_VIEW.name.lower()},
        {"product_id": product_2.product_id, "event_type": EventType.PRODUCT_VIEW.name.lower()}
    ]

    json_events_str = werkzeug.urls.url_quote(json.dumps(json_events))

    json_empty_events = []
    empty_json_events_str = werkzeug.urls.url_quote(json.dumps(json_empty_events))

    rv = recommendation_api_client.get(f"/test/recommendation")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=wrong_reco_type")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history=bad_value")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id=__non_existing__")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}&"
                                       f"num=-1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}&"
                                       f"num=21")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "ERROR"

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history={empty_json_events_str}&product_id={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}&brands=brand_1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=similar_products&"
                                       f"browsing_history={json_events_str}&product_id={product_1.product_id}&num=1")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 1

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=personal_recommendations&"
                                       f"browsing_history={json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=personal_recommendations&"
                                       f"browsing_history={empty_json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 3

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=personal_recommendations&"
                                       f"browsing_history={empty_json_events_str}&num=2")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=basket_recommendations&"
                                       f"browsing_history={json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 0

    rv = recommendation_api_client.get(f"/test/recommendation?recommendation_type=basket_recommendations&"
                                       f"browsing_history={json_events_str}&basket_product_ids={product_1.product_id}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) == 2


def test_test_search(client, search_dao, recommendation_api_client, static_embedding_model_and_products):
    model, products = static_embedding_model_and_products
    product_1, product_2, product_3 = products

    search_dao.index_product(client, product_1, refresh_index=False)
    search_dao.index_product(client, product_2, refresh_index=False)
    search_dao.index_product(client, product_3, refresh_index=False)
    search_dao.refresh_index(client)

    rv = recommendation_api_client.get(f"/test/search?q={product_1.title}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) > 0

    json_events = [
        {"product_id": product_1.product_id, "event_type": EventType.PRODUCT_VIEW.name.lower()},
        {"product_id": product_3.product_id, "event_type": EventType.PRODUCT_VIEW.name.lower()}
    ]

    json_events_str = werkzeug.urls.url_quote(json.dumps(json_events))

    rv = recommendation_api_client.get(f"/test/search?q={product_1.title}&browsing_history={json_events_str}")
    data = json.loads(rv.data.decode("utf-8"))
    assert data["status"] == "OK"
    assert len(data["data"]) > 0
