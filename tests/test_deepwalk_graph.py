from datetime import datetime, timedelta
from mobi.core.population import Population
from mobi.core.models import Event, EventPlatform, EventType
from mobi.ml.model.embedding.impl.factories.deepwalk import Graph, generate_pairs
from mobi.utils.tests import random_string


def test_create_graph():
    user_id = random_string(20)
    graph = Graph()

    e1 = Event(
        user_id=user_id,
        user_population=Population.UNKNOWN,
        product_id=random_string(20),
        product_version=1,
        event_type=EventType.PRODUCT_VIEW,
        event_platform=EventPlatform.OTHER,
        date=datetime.utcnow() - timedelta(minutes=2)
    )

    e2 = Event(
        user_id=user_id,
        user_population=Population.UNKNOWN,
        product_id=random_string(20),
        product_version=1,
        event_type=EventType.PRODUCT_VIEW,
        event_platform=EventPlatform.OTHER,
        date=datetime.utcnow() - timedelta(minutes=1)
    )

    graph.add_edge(e1.product_id, e2.product_id)

    assert (e1.product_id, e2.product_id) in list(generate_pairs(graph, 2))
    assert (e1.product_id, e2.product_id) not in list(generate_pairs(graph, 3))


def test_inverted_weights():
    graph = Graph()

    product_1_id = random_string()
    product_2_id = random_string()
    product_3_id = random_string()

    graph.add_edge(product_1_id, product_2_id)
    graph.add_verticies([product_3_id])

    assert (product_1_id, product_2_id) not in graph.random_non_existing_edges(k=100)
    assert (product_2_id, product_2_id) not in graph.random_non_existing_edges(k=1000)
    assert (product_1_id, product_1_id) not in graph.random_non_existing_edges(k=1000)
    assert (product_3_id, product_3_id) not in graph.random_non_existing_edges(k=1000)

    assert (product_2_id, product_1_id) in graph.random_non_existing_edges(k=1000)
    assert (product_2_id, product_3_id) in graph.random_non_existing_edges(k=1000)

    assert (product_1_id, product_3_id) in graph.random_non_existing_edges(k=1000)

    assert (product_3_id, product_1_id) in graph.random_non_existing_edges(k=1000)
    assert (product_3_id, product_2_id) in graph.random_non_existing_edges(k=1000)
